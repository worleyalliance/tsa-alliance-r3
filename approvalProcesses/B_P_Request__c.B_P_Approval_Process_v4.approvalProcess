<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>true</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>creator</type>
    </allowedSubmitters>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Name</field>
        <field>Owner</field>
        <field>Client__c</field>
        <field>Opportunity__c</field>
        <field>Selling_Unit__c</field>
        <field>Status__c</field>
        <field>BP_Budget__c</field>
        <field>Approval_Level_Required__c</field>
        <field>Description__c</field>
        <field>Work_Hours__c</field>
        <field>Previously_Approved_B_P__c</field>
        <field>Sales_Lead__c</field>
        <field>Level_1_Sales_Manager__c</field>
        <field>Level_1_Ops_Manager__c</field>
        <field>Regional_VP_of_Sales__c</field>
        <field>Regional_VP_of_Ops__c</field>
        <field>SVP_Sales__c</field>
        <field>SVP_GM_Ops__c</field>
        <field>LOB_President__c</field>
        <field>Chairman_CEO__c</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>B_P_Coordinator__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <label>B&amp;P Coordinator</label>
        <name>B_P_Coordinator</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Sales_Lead__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <formula>OwnerId  &lt;&gt;  Opportunity__r.OwnerId</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Sales Lead</label>
        <name>Sales_Lead</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Level_6_Sales_Approver_Local__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>Added in version 4 of the approval process</description>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Level_6_Sales_Approver_Local__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 6 Sales Approver (Local)</label>
        <name>Level_6_Sales_Approver_Local</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Level_6_Ops_Approver_Local__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>Added in version 4 of the approval process</description>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Level_6_Ops_Approver_Local__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 6 Ops Approver (Local)</label>
        <name>Level_6_Ops_Approver_Local</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Level_1_Sales_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Level_1_Sales_Manager__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 5 Sales Approver (Local)</label>
        <name>Level_5_Sales_Approver_Local</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Level_1_Ops_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Level_1_Ops_Manager__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 5 Ops Approver (Local)</label>
        <name>Level_5_Ops_Approver_Local</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Regional_VP_of_Sales__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Regional_VP_of_Sales__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 4 Sales Approver</label>
        <name>Level_4_Sales_Approver</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Regional_VP_of_Ops__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Regional_VP_of_Ops__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 4 Ops Approver</label>
        <name>Level_4_Ops_Approver</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>SVP_Sales__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.SVP_Sales__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 3 Sales Approver</label>
        <name>Level_3_Sales_Approver</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>SVP_GM_Ops__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.SVP_GM_Ops__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 3 Ops Approver</label>
        <name>Level_3_Ops_Approver</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>LOB_President__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.LOB_President__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Level 2 LOB President</label>
        <name>Level_2_LOB_President</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Chairman_CEO__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <criteriaItems>
                <field>B_P_Request__c.Chairman_CEO__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
        </entryCriteria>
        <label>Level 1 Chairman &amp; CEO</label>
        <name>Level_1_Chairman_CEO</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Approval Process where approvers are manually selected. The process goes through all approvers and approves the record if the last approver is blank or approves the request.</description>
    <emailTemplate>B_P_Requests/B_P_Approval_Request</emailTemplate>
    <enableMobileDeviceAccess>true</enableMobileDeviceAccess>
    <entryCriteria>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>B_P_Request__c.Manual_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Approved,Active,Pending Close,Closed</value>
        </criteriaItems>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>Account_Verification_UnCheck</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Update_Original_B_P_Budget_USD</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Update_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>true</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>Account_Verification_UnCheck</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Update_Status_to_Rejected</name>
            <type>FieldUpdate</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>Change_B_P_status_to_Submitted</name>
            <type>FieldUpdate</type>
        </action>
    </initialSubmissionActions>
    <label>B&amp;P Approval Process v4</label>
    <recallActions>
        <action>
            <name>Account_Verification_UnCheck</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Change_B_P_status_to_Draft</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOnly</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
