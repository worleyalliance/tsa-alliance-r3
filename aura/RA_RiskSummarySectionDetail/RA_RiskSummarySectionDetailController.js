({ 
    init: function (cmp, evt, h) {
        var sectionName = cmp.get('v.sectionName');
        console.log('sectionName: ' + sectionName);
        
        var columns = [
            {
                type: 'text',
                fieldName: 'question',
                label: 'Question'
            },
            {
                type: 'text',
                fieldName: 'response',
                label: 'Response',
                cellAttributes: { alignment: 'left'}
            },
            {
                type: 'text',
                fieldName: 'riskScore',
                label: 'Risk Assessment',
                cellAttributes: { alignment: 'left' }
            },
            {
                type: 'button-icon',
                label: 'Flag',
                initialWidth: 50,
                typeAttributes: { iconName: 'utility:lower_flag', variant: 'bare', iconClass: 'flagIcon', disabled: {fieldName: 'notFlagged'}}
            }
        ];        
        cmp.set('v.columns', columns);
        
        var theReview = cmp.get('v.theReview');
        console.log('summary review: ', theReview);
        if(sectionName !== 'Opportunity Risk'){
            var summaryDataAction = cmp.get('c.getSummaryData');
            summaryDataAction.setParams({
                theReview: theReview,
                sectionName: sectionName
            });
            summaryDataAction.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){
                    console.log('response: ', response.getReturnValue());
                    var theReview = cmp.get('v.theReview');
                    var flagged;
                    switch (cmp.get('v.sectionName')){
                        case 'Execution Risk':
                            flagged = theReview.Flagged_Execution_Questions__c;
                            break;
                        case 'Client Risk':
                            flagged = theReview.Flagged_Client_Questions__c;
                            break;
                        case 'Geography Risk':
                            flagged = theReview.Flagged_Geography_Questions__c;
                            break;
                        case 'Commercial Risk':
                            flagged = theReview.Flagged_Commercial_Questions__c;
                            break;
                        case 'Contractual Risk':
                            flagged = theReview.Flagged_Contractual_Questions__c;
                            break;
                        default:
                            flagged = '';
                            break;
                    }
                    if(flagged == null){
                        flagged = '';
                    }
                    var returnedData = response.getReturnValue();
                    var data = [];
                    for(var i = 0; i < returnedData.length; i++){
                        if(returnedData[i].response !== '') {
                            var isFlagged = flagged.includes(returnedData[i].apiName);
                            var newData = {
                                id: i + 1,
                                question: returnedData[i].question,
                                response: returnedData[i].response,
                                riskScore: returnedData[i].riskScore,
                                notFlagged: !isFlagged
                            };
                        }
                        data.push(newData);
                    }
                    cmp.set('v.data', data);
                    cmp.set('v.hasData', data.length > 0);
                } else if(response.getState() === 'ERROR'){
                    console.log('data ERROR: ', response.getError());
                }
            });
            $A.enqueueAction(summaryDataAction);
        } else {
            
            var columns = [
                {
                    type: 'text',
                    fieldName: 'question',
                    label: 'Question'
                },
                {
                    type: 'text',
                    fieldName: 'response',
                    label: 'Response',
                    cellAttributes: { alignment: 'right'}
                },
                {
                    type: 'text',
                    fieldName: 'riskScore',
                    label: 'Risk Assessment',
                    cellAttributes: { alignment: 'left' }
                }
            ];        
            cmp.set('v.columns', columns);
            
            console.log('review margin: ' + theReview.Gross_Margin__c.toString());
            var tic = '';
            if(!$A.util.isEmpty(theReview.Total_Installed_Cost_TIC__c)){
                tic = theReview.Total_Installed_Cost_TIC__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
            }
            var rev = '';
            if(!$A.util.isEmpty(theReview.Revenue__c)){
                rev = theReview.Revenue__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
            }
            var margin = '';
            if(!$A.util.isEmpty(theReview.Gross_Margin__c)){
                margin = theReview.Gross_Margin__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
            }
            var bp = '';
            if(!$A.util.isEmpty(theReview.Total_Requested_B_P__c)){
                bp = theReview.Total_Requested_B_P__c.toLocaleString('en-US', {style: 'currency', currency: 'USD'});
            }
            var data = [
                /*{
                    id: '1',
                    question: 'Account',
                    response: theReview.Account__r.Name,
                    riskScore: theReview.Opportunity_Risk_Score__c + ' - ' + theReview.Opportunity_Risk_Category__c
                },
                {
                    id: '2',
                    question: 'Opportunity',
                    response: theReview.Opportunity__r.Name
                },
                {
                    id: '3',
                    question: 'Line of Business',
                    response: theReview.Line_of_Business__c
                },*/
                {
                    id: '4',
                    question: 'Scope of Services',
                    response: theReview.Scope_of_Services__c,
                    riskScore: theReview.Opportunity_Risk_Score__c + ' - ' + theReview.Opportunity_Risk_Category__c
                },/*
                {
                    id: '5',
                    question: 'Service Type Code',
                    response: theReview.Service_Type_Code__c
                },*/
                {
                    id: '6',
                    question: 'Contract Award Type',
                    response: theReview.Contract_Award_Type__c
                },
                {
                    id: '7',
                    question: 'Total Project Estimated Cost (TIC)',
                    response: tic
                },
                {
                    id: '8',
                    question: 'Total Contract Value (Revenue)',
                    response: rev
                }/*,
                {
                    id: '9',
                    question: 'Gross Margin',
                    response: margin
                },
                {
                    id: '10',
                    question: 'Total Requested B&P',
                    response: bp
                }*/
            ];
            
            console.log('data: ' + data);
        	cmp.set('v.data', data);
        	cmp.set('v.hasData', data.length > 0);
        }
       
    }
});