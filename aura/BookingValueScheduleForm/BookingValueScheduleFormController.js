/**************************************************************************************
Name: aura.BookingValueScheduleForm
Version: 1.0 
Created Date: 03/20/2018
Function: Controller for the Booking Value Schedule Form component

Modification Log:
**************************************************************************************
* Developer         Date           Description
**************************************************************************************
* Pradeep Shetty   09/08/2018      Original Version
*************************************************************************************/
({
	doInit : function(cmp, event, helper) {

    //Instantiate the bookingValue attribute
    var bookingValue = {};
    cmp.set("v.bookingValue", bookingValue);		
			
		//Set the Overall duration to the value of Parent Opp's duration
		cmp.set("v.overallDuration", cmp.get("v.bookingValueParentOpp.Duration_Months__c"));

    //Callback when the server call fails
    var onFail = function(errors){
      if(errors) {
        cmp.set('v.newBookingValueError', errors[0].message);
      }
      
      //Send message to tell footer to hide spinner
      helper.sendMessage(cmp, event);
    }; 


    if(  $A.util.isUndefinedOrNull( cmp.get("v.bookingValueParentOpp").Sales_Plan_Unit__c )){
      cmp.set('v.bookingValueError',"WARNING: The Booking Schedule cannot be created if the Opportunity does not have a Sales Plan Unit assigned. Please go back to the Opportunity and add Sales Plan Unit if it is blank."
 
      );
    }

		//Get help text map
    helper.loadHelpText(cmp, function(helpTextMap){
      cmp.set('v.fieldHelpMap', helpTextMap);
    }, onFail);     

		//Get Field Label map
    helper.loadFieldLabel(cmp, function(fieldLabelMap){
      cmp.set('v.fieldLabelMap', fieldLabelMap);
    }, onFail);

    //Get Picklist values for Scope of Services field
    helper.loadScopeOfServices(cmp, function (scopeOfServices) {
      cmp.set('v.scopeOfServicesOptions', scopeOfServices);
    }, onFail);

    //Get Picklist values for Selling Unit field
    helper.loadSellingUnits(cmp, function (sellingUnits) {
      cmp.set('v.sellingUnitOptions', sellingUnits);
      if(helper.setSUDisabledAttribute(cmp)){
        cmp.set("v.bookingValue.Selling_Unit__c", cmp.get("v.bookingValueParentOpp.Selling_Unit__c"));
      }            
    }, onFail); 

    //Get Picklist values for Stage field
    helper.loadStages(cmp, function (stages) {
      cmp.set('v.stageOptions', stages);
    }, onFail);

    //Get Picklist values for Pursuit Type field
    helper.loadPursuitTypes(cmp, function (pursuitTypes) {
      cmp.set('v.pursuitTypeOptions', pursuitTypes);
    }, onFail); 

    //Set Performance Unit value to that of the parent opp
    helper.setPerformanceUnitValue(cmp,event);       
	},
	doNext:function(cmp,event,helper){

		var currentScreen = cmp.get("v.currentScreen");
		//If the preview button is hit call the showPreview method
		if(currentScreen === "form"){
			cmp.showPreview();
		}
		else if(currentScreen === "preview"){
			cmp.saveBV();
		}
	},	
	/*Function called when Preview button is clicked*/
	onPreview: function(cmp,event,helper){

		//Clear the error message
		cmp.set("v.bookingValueError", []);  

	  //Callback when validation is successful
	  var onValidationSuccess = function(){
			//Call the helper method to build and show preview
			helper.buildPreview(cmp, event, helper);			
	  };
	  
	  //Callback when validation fails
	  var onValidationFail = function(error){
      if(error){                
        //Call the helper method to save        
        cmp.set("v.bookingValueError", "These required fields must be completed: " + error);                
      }
      
      //Send message to tell footer to hide spinner
      helper.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");
	  };     		

    //Call helper method to Perform validation
    helper.performInputValidation(cmp, onValidationSuccess, onValidationFail);	

	},
	/*Function called when Back button is clicked*/
	onBack: function(cmp,event,helper){

		//Call the helper method to build and show preview
		helper.goBack(cmp, event);
	},
	/*Function called when Save button is clicked*/
	onSave: function(cmp,event,helper){
    //Callback when the save is successful
    var onSuccess = function(){
      console.log('sudeep-onSuccess');
    	//Send a message to refresh ChildOpportunityRelatedList component
    	helper.sendMessage(cmp, event, "reload", "BookingValueCreateChannel");

    	//Close the modal
      cmp.find("overlayLib").notifyClose();
    };
    
    //Callback when the save fails
    var onFail = function(errors){
      
      if(errors) {
          cmp.set('v.bookingValueError', errors[0].message);
      }
      
      //Send message to tell footer to hide spinner
      helper.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");
    }; 

    

		//Call the helper method to build and show preview
		helper.saveBookingValues(cmp, event, onSuccess, onFail);    
	}		
})