/**************************************************************************************
Name: aura.BookingValueScheduleForm
Version: 1.0 
Created Date: 03/20/2018
Function: Helper class for the Booking Value Schedule form component

Modification Log:
**************************************************************************************
* Developer         Date           Description
**************************************************************************************
* Pradeep Shetty   09/08/2018      Original Version
*************************************************************************************/
({
    /*Function to load help text for Opportunity Fields*/
    loadHelpText: function(cmp, onSuccess, onFail){
      //Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.getOpportunityFieldsHelpText", null, true, onSuccess, onFail);        
    },
    /*Function to load Field Label for Opportunity Fields*/
    loadFieldLabel: function(cmp, onSuccess, onFail){
      //Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.getOpportunityFieldsLabel", null, true, onSuccess, onFail);        
    },
    /*Function to load picklist values for Scope of Services field*/
		loadScopeOfServices: function(cmp, onSuccess, onFail) {
      //Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.getScopeOfServices", null, true, onSuccess, onFail);         
    },
    /*Function to load picklist values for Selling Unit field*/
    loadSellingUnits: function(cmp, onSuccess, onFail) {        
	    //Call server side method to create booking value(Opportunity)        
	    this.callServer(cmp, "c.getSellingUnits", null, true, onSuccess, onFail);         
    },
    /*Function to determine if Selling Unit should be disabled or not
    	If Parent Opp's selling unit is an active value in the Selling Unit global picklist,
    	then set the field is disabled, else the field is enabled for users to select an SU */
    setSUDisabledAttribute: function(cmp){
      //Get the array of Selling Units
      var sellingUnitList = cmp.get("v.sellingUnitOptions");

      //Get the Parent Selling Unit
      var parentSellingUnit = cmp.get("v.bookingValueParentOpp.Selling_Unit__c");

      //Set the value based on the check
      var disabled = this.checkPickListExistence(sellingUnitList, parentSellingUnit) > 0 ? true : false;
      cmp.set("v.suDisabled", disabled);
      return disabled;
    },    
    /*Function to load picklist values for Stage field*/
    loadStages: function(cmp, onSuccess, onFail) {        
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getStages", null, true, onSuccess, onFail);         
    },
    /*Function to load picklist values for Pursuit Type field*/
    loadPursuitTypes: function(cmp, onSuccess, onFail) {        
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getPursuitTypes", null, true, onSuccess, onFail);         
    },  
    /*Sets Performance Unit value to parent Opps Performance Unit*/
    setPerformanceUnitValue: function(cmp,event){

      //Action parameters
      var actionParams = {unitValue : cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c};


      //Check if the parent Lead Performance Unit is null or not. 
      if(cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c){
        //Success function
        var onSuccess = function(isActive){
          cmp.set("v.puDisabled", isActive);
          //Set the Selling Unit for the Booking Value if the suDisabled is false
          if(isActive){
            cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c);
          }
          else{
            cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", null);
          }
        }

        //Callback when action fails
        var onFail = function(errors){
          if(errors) {
            cmp.set('v.bookingValueError', errors[0].message);
          }
          
          //Send message to tell footer to hide spinner
          //this.sendMessage(cmp, event);
        }; 

        //Call server side method to check if Parent Selling Unit is active by calling Apex action
        this.callServer(cmp, "c.checkPerformanceUnitState", actionParams, true, onSuccess, onFail);                 
      }
      else{
        //If parent Lead PU is null, then allow users to select PU.
        cmp.set("v.puDisabled", false);
        cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", null);
      }
    },

    /*Function to build preview based on form parameters and show the preview*/
    buildPreview: function(cmp, event, helper){
    	
    	//Get the list of Columns
    	if(cmp.get("v.bookingValueColumns").length == 0){
    		this.setTableColumns(cmp);    		
    	}

    	//Get the list of booking values to be created
    	this.setTableRows(cmp, event, helper);

			//Call helper method to hide spinner
			this.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");     	

    },

    /*Function to fetch columns from Opportunity Fieldset*/
    setTableColumns: function(cmp){

		  //Callback when the server call succeeds
		  var onSuccess = function(columnHeaders){
		  	cmp.set("v.bookingValueColumns", columnHeaders);
		  };

			//Callback when the server call fails
    	var onFail = function(errors){
      	if(errors) {
        	cmp.set('v.newBookingValueError', errors);
      	}	
    	};	  
		 	//Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.getColumnHeaders", null, true, onSuccess, onFail);     		  
    },

    /*Function to fetch data rows*/
    setTableRows: function(cmp, event, helper){
    	
		  //Callback when the server call succeeds
		  var onSuccess = function(bookingValues){
	  		cmp.set("v.bookingValueList", bookingValues);
	  		cmp.set("v.bvCount", bookingValues.length);

	  		//Calculate Summary Revenue, GM, and Work Hours
	  		var bvSummaryRevenue = bookingValues.length * cmp.get("v.bookingValue.Revenue__c");
				var bvSummaryGM      = bookingValues.length * cmp.get("v.bookingValue.Amount");
        var bvSummaryHours   = bookingValues.length * cmp.get("v.bookingValue.Work_Hours__c"); //US2542: Added Work Hours

				//Set attribute values
				cmp.set("v.bvSummaryRevenue", bvSummaryRevenue);
				cmp.set("v.bvSummaryGM", bvSummaryGM);
        cmp.set("v.bvSummaryHours", bvSummaryHours); //US2542: Added Work Hours
	    
	    	//Make preview visible and Form invisible
	    	helper.hideElement(cmp, "bvForm");
	    	helper.showElement(cmp, "bvPreview");

				//Set the currentScreen to "preview"
				cmp.set("v.currentScreen", "preview"); 

				//Call helper method to show preview related buttons on footer
				helper.sendMessage(cmp, event, "showPreview", "BookingValueFooterChannel");	  		
	  	};
			//Callback when the server call fails
    	var onFail = function(errors){
      	if(errors) {
        	cmp.set('v.newBookingValueError', errors);
      	}	
    	};

    	//Set parent Opportunity on Booking value
    	var bookingValue = cmp.get("v.bookingValue");

    	bookingValue.Parent_Opportunity__c = cmp.get("v.bookingValueParentOpp.Id");

      //Set the Go and Get Value. We multiply by 100 
      //because the percent values are set as fractions
      if(bookingValue.Go__c <= 1){
				bookingValue.Go__c = bookingValue.Go__c * 100;
      }
      
      if(bookingValue.Get__c <= 1){
				bookingValue.Get__c = bookingValue.Get__c * 100;
      }   	

    	var parameters = {
    		bvParameters : bookingValue,
    		duration: cmp.get("v.overallDuration"),
    		frequency: cmp.get("v.bookingValueFrequency")
    	}	  
		 	//Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.getBookingValues", parameters, true, onSuccess, onFail);     		  
    },    
    /*Function to hide preview and show the form*/
    goBack: function(cmp, event){
			//Set the currentScreen to "preview"
			cmp.set("v.currentScreen", "form"); 
    	
    	//Make preview invisible and Form visible
    	this.hideElement(cmp, "bvPreview");
    	this.showElement(cmp, "bvForm");

			//Call helper method to hide spinner
			this.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel"); 

			//Call helper method to show preview related buttons on footer
			this.sendMessage(cmp, event, "showForm", "BookingValueFooterChannel");    	
    },
    /*Function to perform input validation before showing preview*/
    performInputValidation: function(cmp, onValidationSuccess, onValidationFail){
    	var allowPreview = true;
      
      //Error message for Required Fields  
      var errorMessage = '';

      //Duration
      if(!cmp.find("overallDurationInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + $A.get("$Label.c.FieldLabel_OverallDuration");
        }
        else{
          errorMessage = errorMessage + $A.get("$Label.c.FieldLabel_OverallDuration");
        }
        allowPreview = false;
      }        
      //Opportunity Name
      if(!cmp.find("bvNamePrefixInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + $A.get("$Label.c.FieldLabel_BVNamePrefix");
        }
        else{
          errorMessage = errorMessage + $A.get("$Label.c.FieldLabel_BVNamePrefix");
        }
        allowPreview = false;
      }
      //Scope Of Services
      if(!cmp.find("scopeOfServicesInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Scope_of_Services__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Scope_of_Services__c;
        }            
        allowPreview = false;
      } 
      //Selling Unit     
      if(!cmp.find("oppSalesPlanUnit").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Sales_Plan_Unit__r.Name;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Sales_Plan_Unit__r.Name;
        }
        allowPreview = false;
      } 
      //Revenue
      if(!cmp.find("revenueInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Revenue__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Revenue__c;
        }
        
        allowPreview = false;
      }
      //Gross Margin
      if(!cmp.find("grossMarginInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Amount;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Amount;
        }            
        allowPreview = false;
      }      
      //Lead Performance Unit
      if(!cmp.find("leadPerformanceUnitInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Lead_Performance_Unit_PU__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Lead_Performance_Unit_PU__c;
        }
        allowPreview = false;
      }       
      //CloseDate
      if(!cmp.find("targetCloseDateInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").CloseDate;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").CloseDate;
        }                        
        allowPreview = false;
      }
      //Target Project Start Date
      if(!cmp.find("targetProjectStartDateInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Target_Project_Start_Date__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Target_Project_Start_Date__c;
        }            
        allowPreview = false;
      }      
      //Go
      if(!cmp.find("goInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Go__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Go__c;
        }
        allowPreview = false;
      }
      //Get
      if(!cmp.find("getInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Get__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Get__c;
        }
        allowPreview = false;
      }
      //Stage
      if(!cmp.find("stageInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").StageName;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").StageName;
        }
        allowPreview = false;
      }
      //Pursuit Type
      if(!cmp.find("pursuitTypeInput").get("v.value")){
        if(errorMessage.length>0){
          errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Program_Renewal_Rebid__c;
        }
        else{
          errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Program_Renewal_Rebid__c;
        }
        allowPreview = false;
      }            

      //Check if the overall duration is more than Parent Opportunity's Duration
      if(cmp.get("v.overallDuration") > cmp.get("v.bookingValueParentOpp.Duration_Months__c")){
      	allowPreview = false;
      }

      //Check if Preview is allowed or not
      if(allowPreview){
        onValidationSuccess();
      }
      else{
        onValidationFail(errorMessage);
      }
        
		},
    /*Function to hide preview and show the form*/
    saveBookingValues: function(cmp, event, onSuccess, onFail){


    	//Get the Booking Value list shown in the preview
    	var bookingValueInsertList = cmp.get("v.bookingValueList");

    	//Make required changes before sending the list to the Apex method
    	for(var i=0; i<bookingValueInsertList.length; i++){

        //This is needed to ensure that a list Opportunities can be sent to Apex Method. 
        //Without this parameter we get an error
        bookingValueInsertList[i].sobjectType = "Opportunity";
        //Set the sales plan unit
        bookingValueInsertList[i].Sales_Plan_Unit__c  = cmp.get("v.bookingValueParentOpp").Sales_Plan_Unit__c ;
    	}
      
      
      //Parameters to be sent to the server call
      var parameters = {
          "opp": bookingValueInsertList
      };

      //Call server side method to create booking value(Opportunity)        
      this.callServer(cmp, "c.createBookingValue", parameters, true, onSuccess, onFail);
    },
    /*Function to communicate with other components*/ 
    sendMessage: function (cmp, event, message, channel){
      //Get a message event
    	var sendMsgEvent = $A.get("e.ltng:sendMessage"); 
		
        //Set the message event parameters
        sendMsgEvent.setParams({
            "message": message, 
            "channel": channel 
        }); 
        sendMsgEvent.fire();
    },
})