({
    doRender : function(cmp, evt, h){
        var revRecord = cmp.get('v.theReview');
        console.log('revRecord: ', revRecord);
        if(!$A.util.isEmpty(revRecord)){
            var hasError = false;
            if($A.util.isEmpty(revRecord.Account__r.Name) || $A.util.isEmpty(revRecord.Opportunity__r.Name) 
               || $A.util.isEmpty(revRecord.Service_Type_Code__c) || $A.util.isEmpty(revRecord.Scope_of_Services__c) 
               || $A.util.isEmpty(revRecord.Line_of_Business__c) || $A.util.isEmpty(revRecord.Contract_Award_Type__c) 
               || $A.util.isEmpty(revRecord.Revenue__c) || $A.util.isEmpty(revRecord.Gross_Margin__c) 
               || $A.util.isEmpty(revRecord.Project_Role__c) ){
                hasError = true;
            }
            
            // Check to see if the hasError attribute will be changing
            // and if so, fire the event.
            if(cmp.get('v.hasError') != hasError){
                var errorEvent = $A.get("e.c:RA_opptyErrorEvent");
                errorEvent.setParams({
                    hasError: hasError
                });
                errorEvent.fire();
            }
            cmp.set('v.hasError', hasError);
        }
    },
    cancelModal : function(cmp, evt, h) {
        $A.get('e.force:closeQuickAction').fire();
	},
    doContinue : function(cmp, evt, h) {
        cmp.set('v.currentStep', 'Step 200');
    },
    doRefresh : function(cmp, evt, h) {
        cmp.set("v.showSpinner", true);
        var refreshReview = cmp.get('c.updateOpportunityBrief');
        var recId = [cmp.get('v.recordId')];
        refreshReview.setParams({ reviewIds : recId });
        refreshReview.setCallback(this, function(response){
            // If apex call succeeds
            if(response.getState() == 'SUCCESS'){
                console.log('updatedReview: ', response.getReturnValue());
                var theReturn = response.getReturnValue();
                var theRecord = cmp.get('v.theReview');
                console.log('record revenue: ', theRecord.Revenue__c);
                theRecord.Account__r.Name = theReturn.Account__r.Name;
                theRecord.Opportunity__r.Name = theReturn.Opportunity__r.Name;
                theRecord.Line_of_Business__c = theReturn.Line_of_Business__c;
                theRecord.Scope_of_Services__c = theReturn.Scope_of_Services__c;
                theRecord.Service_Type_Code__c = theReturn.Service_Type_Code__c;
                theRecord.Contract_Award_Type__c = theReturn.Contract_Award_Type__c;
                theRecord.Total_Installed_Cost_TIC__c = theReturn.Total_Installed_Cost_TIC__c;
                theRecord.Revenue__c = theReturn.Revenue__c;
                theRecord.Gross_Margin__c = theReturn.Gross_Margin__c;
                theRecord.Total_Requested_B_P__c = theReturn.Total_Requested_B_P__c;
                theRecord.Contract_Type_s__c = theReturn.Contract_Type_s__c;
                console.log('the new record: ', theRecord);
                cmp.set('v.theReview', theRecord);
                
                cmp.set("v.showSpinner", false);
                //cmp.find('revRecordLoader').reloadRecord();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been refreshed successfully.",
                    "type": "success"
                });
                toastEvent.fire();
                console.log('toast success fired');
            } else if(response.getState() === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(refreshReview);
        
    },
    handleNavEvent : function(cmp, evt, h){
        var evtStep = evt.getParam('step');
        var evtAction = evt.getParam('actionType');
        if(evtStep == "Step 100" && evtAction == "continue"){
            cmp.set('v.currentStep', 'Step 200');
        }
        if(evtStep == "Step 100" && evtAction == "refresh"){
            h.doRefresh(cmp, evt, h);
        }
    }
})