({
	doRefresh : function(cmp, evt, h) {
        cmp.set("v.showSpinner", true);
        var refreshReview = cmp.get('c.updateOpportunityBrief');
        var recId = [cmp.get('v.recordId')];
        refreshReview.setParams({ reviewIds : recId });
        refreshReview.setCallback(this, function(response){
            // If apex call succeeds
            if(response.getState() == 'SUCCESS'){
                console.log('updatedReview: ', response.getReturnValue());
                var theReturn = response.getReturnValue();
                var theRecord = cmp.get('v.theReview');
                console.log('record revenue: ', theRecord.Revenue__c);
                theRecord.Account__r.Name = theReturn.Account__r.Name;
                theRecord.Opportunity__r.Name = theReturn.Opportunity__r.Name;
                theRecord.Line_of_Business__c = theReturn.Line_of_Business__c;
                theRecord.Scope_of_Services__c = theReturn.Scope_of_Services__c;
                theRecord.Service_Type_Code__c = theReturn.Service_Type_Code__c;
                theRecord.Contract_Award_Type__c = theReturn.Contract_Award_Type__c;
                theRecord.Total_Installed_Cost_TIC__c = theReturn.Total_Installed_Cost_TIC__c;
                theRecord.Revenue__c = theReturn.Revenue__c;
                theRecord.Gross_Margin__c = theReturn.Gross_Margin__c;
                theRecord.Total_Requested_B_P__c = theReturn.Total_Requested_B_P__c;
                theRecord.Contract_Type_s__c = theReturn.Contract_Type_s__c;
                console.log('the new record: ', theRecord);
                cmp.set('v.theReview', theRecord);
                
                cmp.set("v.showSpinner", false);
                //cmp.find('revRecordLoader').reloadRecord();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been refreshed successfully.",
                    "type": "success"
                });
                toastEvent.fire();
                console.log('toast success fired');
                h.refreshScore(cmp, evt, h);
            } else if(response.getState() === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(refreshReview);
        
    },
    refreshScore : function(cmp, evt, h){
        console.log('radio changed');
        var changeEvent = $A.get("e.c:RA_reviewChangeEvent")
        changeEvent.fire();
    }
})