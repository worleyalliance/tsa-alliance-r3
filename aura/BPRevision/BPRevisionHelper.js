({
    retrieveBPRequest: function (cmp, bpId, onSuccess, onFail) {
        var action = cmp.get("c.retrieveOriginalBAndP");
        action.setParams(
            {
                bpId : bpId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var bpRequest = response.getReturnValue();
                    onSuccess(bpRequest);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveBPRevision: function (cmp, bpRequestId, bpRevisionJSON, onSuccess, onFail) {
        var action = cmp.get("c.createBPRevision");
        action.setParams(
            {
                originalBPId : bpRequestId,
                bpRevisionJSON : bpRevisionJSON
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var bpRevisionId = response.getReturnValue();
                    onSuccess(bpRevisionId);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    navigateToSObject: function(recordId){
        if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
            sforce.one.navigateToSObject(recordId);
        }
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    }
})