({
    doInit: function (cmp, event, helper) {
        helper.showElement(cmp, 'modalSpinner');
        var bpRequestId = cmp.get('v.recordId');
        
        //Success Function
        var onBPRequestSuccess = function(bpRequest){
            cmp.set('v.bpRequest', bpRequest);
            
            //Check if the B&P status is Active, Pending Close or Close
            //Or check if the B&P is a legacy B&P
            var bpRequestStatus = cmp.get('v.bpRequest.status');
            var bpRequestLegacy = cmp.get('v.bpRequest.legacyBP');
            //debugger;
            if((bpRequestStatus != 'Active' &&
               bpRequestStatus != 'Pending Close' && 
               bpRequestStatus != 'Closed') || 
               bpRequestLegacy)
            {
            	//Show the message content and hide Save button
            	helper.showElement(cmp,'messageContent');
                helper.hideElement(cmp, 'saveButton');
            }
            else{
                //Show the form
                helper.showElement(cmp,'formContent');
            }
         	helper.hideElement(cmp, 'modalSpinner');
        };
        
        //Failure function
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.errorMessage', errors[0].message);
            }
            helper.hideElement(cmp, 'modalSpinner');
        };
        helper.retrieveBPRequest(cmp, bpRequestId, onBPRequestSuccess, onFail);
        helper.hideElement(cmp, 'modalSpinner');
    },
    doSave: function (cmp, event, helper) {
        //Reset error message
        cmp.set('v.errorMessage', "");

        //Disable cancel and save buttons
        cmp.set('v.buttonsDisabled', true);
        debugger;
        
        //Get B&P Request ID
        var bpRequestId = cmp.get('v.recordId');
        
        //Get user entered value
        var additionalBudgetLocal = cmp.find("additional_Budget").get("v.value");
        var additionalWorkHours   = cmp.get("v.additionalWorkHours");
        var requestJustification  = cmp.find("reason_input").get("v.value");
        
        //Check if user has entered all required information
        if(
            !additionalBudgetLocal || !requestJustification
          ){
			 cmp.set('v.errorMessage', $A.get("$Label.c.GivenFieldsAreRequired")+': '+
                    $A.get("$Label.c.Additional_B_P_Budget")+', '+
                    $A.get("$Label.c.Reason_for_Request_Justification") + ', ' + 
                    $A.get("$Label.c.Additional_Work_Hours"));
             cmp.set('v.buttonsDisabled', false);
             return;
            }
        //If Additional work hours not entered default it to 0
        if(!additionalWorkHours){
            additionalWorkHours = 0;
        }
        
        //Show spinner
        helper.showElement(cmp, 'modalSpinner');        
        
        //Prepare the JSON object for data entered by user
        var bpRevisionJSON = {
            "additionalBudgetLocal": additionalBudgetLocal,
            "additionalWorkHours"  : additionalWorkHours,
            "requestJustification" : requestJustification
        };

        var onSuccess = function(bpRequestId){
            helper.navigateToSObject(bpRequestId);
            helper.hideElement(cmp, 'modalSpinner');
            cmp.set('v.buttonsDisabled', false);
        };
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.errorMessage', errors[0].message);
            }
            helper.hideElement(cmp, 'modalSpinner');
            cmp.set('v.buttonsDisabled', false);

        };
        helper.saveBPRevision(cmp, bpRequestId, JSON.stringify(bpRevisionJSON), onSuccess, onFail);
    },
    doCancel: function (cmp, event, helper) {
        var bpRequestId = cmp.get('v.recordId');
        helper.navigateToSObject(bpRequestId);
    }
})