/**************************************************************************************
 Name: Documents
 Version: 1.0 
 Created Date: 07.03.2017
 Function: Documents helper

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           07.03.2017         Original Version
 *************************************************************************************/
({
    CHUNK_SIZE: 700000, /* Use a multiple of 4 */

    loadFiles: function (cmp, callback) {
        var action = cmp.get("c.retrieveDocuments");
        action.setParams(
            {
                folderPath  : cmp.get("v.folderPath"),
                recordId    : cmp.get("v.recordId"),
                objectType  : cmp.get("v.sObjectName")
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var documents = response.getReturnValue();
                    cmp.set("v.documents", documents);
                    cmp.set("v.dataLoaded", true);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        cmp.set("v.errorMsg", errors[0].message);
                        var errorMsg = cmp.find('errorMsg');
                        $A.util.removeClass(errorMsg, 'slds-hide');
                    }
                }
                callback();
            }
        });
        $A.enqueueAction(action);
    },

    readFile: function(cmp, file, callback) {
        this.hideElement(cmp, 'errorMsgModal');
        if (!file) return;
        var reader = new FileReader();
        reader.onload = function() {
            var fileContents = reader.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            cmp.set("v.fileName", file.name);
            cmp.set("v.fileContent", encodeURIComponent(fileContents));
            if(file.name) {
                cmp.set("v.readyToSave", true);
            }
            callback();
        };
        reader.readAsDataURL(file);
    },

    upload: function(cmp, callback) {
        var fileName = cmp.get('v.fileName');
        var fileContents = decodeURIComponent(cmp.get('v.fileContent'));
        var recordId = cmp.get("v.recordId");
        var objectType = cmp.get("v.sObjectName");
        var folderPath = cmp.get('v.folderPath');

        var fromPos = 0;
        var toPos = Math.min(fileContents.length, this.CHUNK_SIZE);
        var guid = this.guid();

        // start with the initial chunk
        this.uploadChunk(
            cmp, recordId, objectType, folderPath, fileName, fileContents, fromPos, toPos, 0,
            true, false, toPos == fileContents.length,
            guid, callback);
    },

    uploadChunk : function(
        cmp, recordId, objectType, folderPath, fileName, fileContents, fromPos, toPos, offset,
        isFirstChunk, isLastChunk, isSingleChunk,
        guid, callback
    ) {
        var action = cmp.get("c.uploadDocument");
        var chunk = fileContents.substring(fromPos, toPos);
        var self = this;
        action.setParams({
            recordId        : recordId,
            objectType      : objectType,
            folderPath      : folderPath,
            fileName        : fileName,
            base64Data      : encodeURIComponent(chunk),
            guid            : guid,
            offset          : offset,
            isFirstChunk    : isFirstChunk,
            isLastChunk     : isLastChunk,
            isSingleChunk   : isSingleChunk
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    fromPos = toPos;
                    toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
                    cmp.set("v.percentSaved", Math.round(fromPos/fileContents.length * 100));
                    if (fromPos < toPos && !isSingleChunk) {
                        var offset = response.getReturnValue();
                        isFirstChunk = false;
                        isLastChunk = toPos == fileContents.length;
                        self.uploadChunk(
                            cmp, recordId, objectType, folderPath, fileName, fileContents, fromPos, toPos, offset,
                            isFirstChunk, isLastChunk, false,
                            guid, callback);
                    } else {
                        callback();
                    }

                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        cmp.set("v.errorMsgModal", errors[0].message);
                        var errorMsgModal = cmp.find('errorMsgModal');
                        $A.util.removeClass(errorMsgModal, 'slds-hide');
                    }
                }
            }
        });

        $A.enqueueAction(action);
    },

    delete: function(cmp, fileName, callback) {
        this.hideElement(cmp, 'errorMsgModal');
        var action = cmp.get("c.deleteDocument");
        action.setParams({
            fileName    : fileName,
            folderPath  : cmp.get('v.folderPath'),
            recordId    : cmp.get("v.recordId"),
            objectType  : cmp.get("v.sObjectName")
        });
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response && response.getState() == "ERROR"){
                var errors = response.getError();
                if(errors) {
                    cmp.set("v.errorMsg", errors[0].message);
                    this.showElement(cmp, 'errorMsg');
                }
            }
            callback(errors);
        });
        $A.enqueueAction(action);
    },

    clearDocumentData: function (cmp) {
        cmp.set("v.fileName", null);
        cmp.set("v.fileContent", null);
        cmp.set("v.readyToSave", false);
        cmp.set("v.errorMsgModal", null);
        cmp.set("v.errorMsg", null);
        cmp.set("v.percentSaved", 0);
        cmp.find("file").getElement().value='';
        this.hideElement(cmp, 'errorMsg');

    },

    openDialog: function(cmp) {
        this.showElement(cmp, 'dialog');
        this.clearDocumentData(cmp);
    },
    closeDialog: function(cmp) {
        this.hideElement(cmp, 'dialog');
    },
    guid: function(){
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
})