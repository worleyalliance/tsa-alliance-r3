/**************************************************************************************
 Name: Documents
 Version: 1.0 
 Created Date: 07.03.2017
 Function: Documents controller

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           07.03.2017         Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        helper.showSpinner(cmp.getSuper());
        helper.loadFiles(cmp, function (){
            helper.hideSpinner(cmp.getSuper());
        });
    },

    onAddNewClicked: function (cmp, event, helper) {
        helper.openDialog(cmp);
    },

    onModalCancelClicked: function (cmp, event, helper) {
        helper.closeDialog(cmp);
    },

    onModalSaveClicked: function (cmp, event, helper) {
        helper.showElement(cmp, 'modalSpinner');
        helper.showElement(cmp, 'progressIndicator');
        helper.upload(cmp, function (errors){
            helper.hideElement(cmp, 'modalSpinner');
            helper.hideElement(cmp, 'progressIndicator');
            if(!errors) {
                helper.showToast('success', null, ''+ $A.get("$Label.c.Uploaded")+ ' ' + cmp.get('v.fileName') );
                helper.closeDialog(cmp);
                helper.showSpinner(cmp.getSuper());
                helper.loadFiles(cmp, function (){
                    helper.hideSpinner(cmp.getSuper());
                });
            }
        });
    },

    onDragOver: function(cmp, event) {
        event.preventDefault();
    },

    onSelect: function(cmp, event, helper) {
        helper.showElement(cmp, 'modalSpinner');
        event.preventDefault();
        event.stopPropagation();
        var fileInput = cmp.find("file").getElement();
        var file = fileInput.files[0];
        helper.readFile(cmp, file, function (){
            helper.hideElement(cmp, 'modalSpinner');
        });
    },

    onDrop: function(cmp, event, helper) {
        helper.showElement(cmp, 'modalSpinner');
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        helper.readFile(cmp, files[0], function (){
            helper.hideElement(cmp, 'modalSpinner');
        });
    },
    onEdit: function (cmp, event, helper) {
        var menuItem = event.getSource();
        console.log(menuItem.get('v.value'));
    },
    onDelete: function (cmp, event, helper) {
        helper.showElement(cmp, 'loadingSpinner');
        var menuItem = event.getSource();
        var fileName = menuItem.get('v.value');
        helper.delete(cmp, fileName, function (errors){
            if(!errors) {
                helper.loadFiles(cmp, function (){
                    helper.hideSpinner(cmp.getSuper());
                });
            } else {
                helper.hideSpinner(cmp.getSuper());
            }
        });
    }
})