<!--
/**************************************************************************************
Name: aura.OpportunityMultiOfficeTableWrapper
Version: 1.0 
Created Date: 13.04.2017
Function: Custom inline editable related list

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Stefan Abramiuk   04/13/2017      Original Version
* Pradeep Shetty    10/11/2017      DE270: Added parentOpp attribute to get Opp details
* Pradeep Shetty    08/20/2018      US2396: Adjust MOS start dates automatically. 
                                    Add Lag and Auto Adjust fields
*************************************************************************************/
-->
<aura:component description="OpportunityMultiOfficeTableWrapper">

    <aura:attribute name="editedRowIndex"   type="Integer" />
    <aura:attribute name="performanceUnits" type="Object[]" default="[]" />
    <aura:attribute name="subsectors"    type="Object[]" default="[]" />
    <aura:attribute name="resourceTypes"    type="Object[]" default="[]" />    
    <aura:attribute name="resourceTypeSettings" type="Map" default="{}" />
    <aura:attribute name="parentOpp" type="Opportunity" />
    <aura:attribute name="multioffices"     type="Object[]" />
    <aura:attribute name="buttonsDisabled"  type="Boolean" default="false" />

    <aura:attribute name="multiofficesTotal" type="Object" />

    <table class="slds-table slds-table--bordered slds-table--cell-buffer">
        <thead>
            <tr class="slds-text-title--caps">
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.Lead}">{!$Label.c.Lead}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.PerformanceUnit}">{!$Label.c.PerformanceUnit}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.ResourceType}">{!$Label.c.ResourceType}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.WorleyCapabilitySubsector}">{!$Label.c.WorleyCapabilitySubsector}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.Revenue}">{!$Label.c.Revenue}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.GM}">{!$Label.c.GM}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.Hours}">{!$Label.c.Hours}</div>
                </th>
                <!-- US2396: Move this column up. Before it was after Start Date-->
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.AmountMonths}">{!$Label.c.AmountMonths}</div>
                </th>
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.StartDate}">{!$Label.c.StartDate}</div>
                </th>
                <!-- US2396: Added lag column-->
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.Lag}">{!$Label.c.Lag}</div>
                </th>                
                <!-- US2396: Added lag column-->
                <th scope="col" class="tableHeader col">
                    <div class="slds-align--absolute-center" title="{!$Label.c.AutoAdjust}">{!$Label.c.AutoAdjustDates}</div>
                </th>  
                <th scope="col" class="tableHeader col">
                </th>
            </tr>
        </thead>
        <tbody>
            <aura:iteration items="{!v.multioffices}" var="multioffice" indexVar="index" >
                <c:OpportunityMultiOfficeRow editedRowIndex="{!v.editedRowIndex}"
                        parentOpp="{!v.parentOpp}"
                        currentRowIndex="{!index}"
						subsectors="{!v.subsectors}"
                        resourceTypes="{!v.resourceTypes}"
                        resourceTypeSettings="{!v.resourceTypeSettings}"
                        performanceUnits="{!v.performanceUnits}"
                        multioffice="{!multioffice}"
                        buttonsDisabled="{!v.buttonsDisabled}"
                />
            </aura:iteration>
        </tbody>
        <tfoot>
            <tr class="tableFooter">
                <td scope="col" class="tableHeader col lead">
                </td>
                <td scope="col" class="tableHeader col performanceUnit">
                </td>
                <td scope="col" class="tableHeader col resourceType">
                </td>
                <td scope="col" class="tableHeader col resourceType">
                    <div class="slds-align--absolute-center" title="{!$Label.c.OpportunityTotals}">{!$Label.c.OpportunityTotals}</div>
                </td>
                <td scope="col" class="tableHeader col revenue">
                    <ui:outputCurrency value="{!v.multiofficesTotal.revenue}"
                                       currencyCode="{!$Locale.currencyCode}"
                                       currencySymbol="{!$Locale.currency}"
                                       format="{!$Locale.currency + '#,##0'}"
                    />
                </td>
                <td scope="col" class="tableHeader col gm">
                    <ui:outputCurrency value="{!v.multiofficesTotal.gm}"
                                       currencyCode="{!$Locale.currencyCode}"
                                       currencySymbol="{!$Locale.currency}"
                                       format="{!$Locale.currency + '#,##0'}"
                    />
                </td>
                <td scope="col" class="tableHeader col hours">
                    <ui:outputNumber value="{!v.multiofficesTotal.hours}" />
                </td>
                <td scope="col" class="tableHeader col months">
                </td>                
                <td scope="col" class="tableHeader col startDate" colspan="2">
                    <ui:outputDate value="{!v.multiofficesTotal.startDate}" format="{!$Locale.dateFormat}" /> - <ui:outputDate value="{!v.multiofficesTotal.endDate}" format="{!$Locale.dateFormat}" />
                </td>
                <td scope="col" class="tableHeader col actions">
                </td>
            </tr>
        </tfoot>
    </table>
</aura:component>