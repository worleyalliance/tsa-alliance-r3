({
	fetchCBAs : function(cmp, event, helper) {
        var action = cmp.get("c.getCBAsOfProject");
        //Column data for the table
        var cbaColumns = [
            {
                'label':'Name',
                'name':'Name',
                'type':'reference',
                'value':'Id',
                'width': 550,
                'resizeable': true
            },
            {
                'label':'Date',
                'name':'dt_submittal_date__c',
                'type':'reference',
                'value':'Id',
                'width': 170,
                'resizeable': true
            },
            {
                'label':'Record Type',
                'name':'RecordType.Name',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'resizeable': true
            }             ];
       //Configuration data for the table to enable actions in the table
 /*       var cbaTableConfig = {
            "massSelect":true,
           
            "rowAction":[
                {
                    "label":"Edit",
                    "type":"URL",
                    "id":"editCBA"
                },
                {
                    "label":"Del",
                    "type":"URL",
                    "id":"delCBA"
                }
                ,
                {
                    "type":"menu",
                    "id":"actions",
                    "visible":function(Library__c){
                        return Library__c.lr_project__c == "v.projectId"
                    }
                }
            ]
            
        };
 */       
        //if(cmp.get("v.projectId")){
            
         //   action.setParams({
         //       "Id":cmp.get("v.projectId")
          //      });
            var selectedAccount =cmp.get("v.recordId")
               action.setParams({
            "id" : selectedAccount
        });
            
            action.setCallback(this,function(resp){
                var state = resp.getState();
            
            if(cmp.isValid() && state === 'SUCCESS'){
                
                //pass the records to be displayed
                cmp.set("v.libraryCBAs",resp.getReturnValue());
                console.log(resp.getReturnValue());
                //pass the column information
                cmp.set("v.cbaColumns",cbaColumns);
                
                //pass the configuration of CBA Table
 //               cmp.set("v.cbaTableConfig",cbaTableConfig);
                
                //initialize the data table
                //cmp.find("cbaTable").initialize(); 
                window.setTimeout($A.getCallback(function() {
    				cmp.find("cbaTable").initialize();
				                                            }),1000);	
            }
            else {   console.log(resp.getError());   }
            	});
            $A.enqueueAction(action);
           
            console.log("yay1");
            var action2 = cmp.get("c.fetchRecordTypeValues");
        var selectedAccount1 =cmp.get("v.recordId")
               action2.setParams({
            "id" : selectedAccount1
        });
        console.log("id = " + selectedAccount1);
            console.log("yay2");
            var inputsel = cmp.find("mySelect");
            var opts=[];
            var opts = [
            { value: "All", label: "All" },

         ];
            action2.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
             opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                                                        }
           cmp.set("v.options", opts);
                             });
           $A.enqueueAction(action2); 
    
            

    
	
        
},
    libraryRecordTypeSelected: function(component, event, helper){
        console.log("yay");
        var res = component.find("mySelect").get("v.value");
        console.log(res);
        var action = component.get("c.getCBAsOfProjectwRecordType");
        //var recordTypeLabel = component.find("mySelect").get("v.value");

        //Column data for the table
        var cbaColumns = [
            {
                'label':'Name',
                'name':'Name',
                'type':'reference',
                'value':'Id',
                'width': 550,
                'resizeable': true
            },
            {
                'label':'Date',
                'name':'dt_submittal_date__c',
                'type':'reference',
                'value':'Id',
                'width': 170,
                'resizeable': true
            },
            {
                'label':'Record Type',
                'name':'RecordType.Name',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'resizeable': true
            }             ];
       //Configuration data for the table to enable actions in the table
 /*       var cbaTableConfig = {
            "massSelect":true,
           
            "rowAction":[
                {
                    "label":"Edit",
                    "type":"URL",
                    "id":"editCBA"
                },
                {
                    "label":"Del",
                    "type":"URL",
                    "id":"delCBA"
                }
                ,
                {
                    "type":"menu",
                    "id":"actions",
                    "visible":function(Library__c){
                        return Library__c.lr_project__c == "v.projectId"
                    }
                }
            ]
            
        };
*/        
        //if(cmp.get("v.projectId")){
            
         //   action.setParams({
         //       "Id":cmp.get("v.projectId")
          //      });
            var selectedAccount =component.get("v.recordId")
               action.setParams({
            "id" : selectedAccount,
            "recordTypeLabel": res
        });
            
            action.setCallback(this,function(resp){
                var state = resp.getState();
            
            if(component.isValid() && state === 'SUCCESS'){
                
                //pass the records to be displayed
                component.set("v.libraryCBAs",resp.getReturnValue());
                console.log(resp.getReturnValue());
                //pass the column information
                component.set("v.cbaColumns",cbaColumns);
                
                //pass the configuration of CBA Table
//                component.set("v.cbaTableConfig",cbaTableConfig);
                
                //initialize the data table
                //cmp.find("cbaTable").initialize(); 
                window.setTimeout($A.getCallback(function() {
    				component.find("cbaTable").initialize();
				                                            }),1000);	
            }
            else {   console.log(resp.getError());   }
            	});
            $A.enqueueAction(action);
    }
    

})