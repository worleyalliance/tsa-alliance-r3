({
	currentStepChange : function(cmp, evt, h) {
		var currentStep = cmp.get('v.currentStep');
        console.log('currentStep: ' + currentStep);
        var prevStep = cmp.get('v.prevStep');
        console.log('prevStep: ' + prevStep);
        
        if(currentStep != prevStep){
            //var stepCmp = cmp.find(currentStep);
            var stepCmp = document.getElementById(currentStep);
            console.log('stepCmp', stepCmp);
            if(!$A.util.isEmpty(stepCmp)){
                //$A.util.addClass(stepCmp, 'slds-is-active');
                stepCmp.classList.add('slds-is-active');
            }
            
            var prevStepCmp = document.getElementById(prevStep);
            if(!$A.util.isEmpty(prevStepCmp)){
                prevStepCmp.classList.remove('slds-is-active');
                prevStepCmp.classList.add('slds-is-completed');
            }
            cmp.set('v.prevStep', currentStep);
        }
	},
    handleOpptyError : function(cmp, evt, h){
        var hasError = evt.getParam('hasError');
        if(hasError){
            cmp.set('v.riskLevel', 'N/A');
            cmp.set('v.category', 'Unknown');
            cmp.set('v.iconColor', '#DDD');
        } else {
            cmp.set('v.riskLevel', 'R4');
            cmp.set('v.category', 'Standard');
            cmp.set('v.iconColor', '#2FC9A1');
        }
    },
    handleReviewChange : function(cmp, evt, h){
        /*window.setTimeout(
            $A.getCallback(function() {
                var theReview = cmp.get('v.theReview');
                console.log('change event handled!');
        		console.log('review record: ', cmp.get('v.theReview'));
                if(theReview.Decision_authority_on_behalf_of_client__c === 'Yes'){
                    cmp.set('v.riskLevel', 'R4');
                    cmp.set('v.category', 'Standard');
                    cmp.set('v.iconColor', '#2FC9A1');
                } else if(theReview.Decision_authority_on_behalf_of_client__c === 'No'){
                    cmp.set('v.riskLevel', 'R3');
                    cmp.set('v.category', 'Elevated');
                    cmp.set('v.iconColor', 'Orange');
                }
            }), 600
        );*/
        
    }
})