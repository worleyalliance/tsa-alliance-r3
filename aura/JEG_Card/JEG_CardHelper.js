/**************************************************************************************
 Name: aura.JEG_Card
 Version: 1.0 
 Created Date: 05.04.2017
 Function: Card layout wrapper. Have header with icon and spinner

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.04.2017      Original Version
 *************************************************************************************/
({
    showSpinner: function (cmp) {
        this.hideElement(cmp, 'cardIconId');
        this.showElement(cmp, 'cardSpinner');
    },
    hideSpinner: function (cmp) {
        this.hideElement(cmp, 'cardSpinner');
        this.showElement(cmp, 'cardIconId');
    }
})