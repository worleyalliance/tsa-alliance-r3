({
	doInit : function(cmp, evt, h) {
        
        var recordId = cmp.get('v.recordId');
		var action = cmp.get('c.hasWriteAccess');
        action.setParams({reviewId: recordId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var retValue = response.getReturnValue();
                cmp.set('v.hasWriteAccess', retValue);
            } else if(state === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
            
            var spinner = cmp.find("mySpinner");
        	$A.util.toggleClass(spinner, "slds-hide");
        });
        $A.enqueueAction(action);
        
        var stepsAction = cmp.get('c.getRASteps');
        stepsAction.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                console.log('steps: ', response.getReturnValue());
                cmp.set('v.availableSteps', response.getReturnValue());
                cmp.set('v.currentStep', response.getReturnValue()[0].MasterLabel);
            } else if(state === 'ERROR'){
                console.log('getRASteps ERROR: ', response.getError());
            }
        })
        $A.enqueueAction(stepsAction);
        
        var getReviewAction = cmp.get('c.getReviewRec');
        getReviewAction.setParams({
            recordId : cmp.get('v.recordId')
        });
        getReviewAction.setCallback(this, function(response){
            console.log('### the review: ', response.getReturnValue());
            if(response.getState() === 'SUCCESS'){
                cmp.set('v.theReview', response.getReturnValue());
                h.checkPrevValues(cmp, evt);
                console.log('retrieved review rec: ', response.getReturnValue());
                var changeEvent = $A.get("e.c:RA_reviewChangeEvent");
                changeEvent.fire();
            } else if(response.getState() === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(getReviewAction)

	},
    handleRiskCalc : function(cmp, evt, h) {
        
        var theReview = cmp.get('v.theReview');
        var recalcAction = cmp.get('c.calcReviewPercent');
        recalcAction.setParams({
            theReview: theReview
        });
        recalcAction.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                console.log('SUCCESS!');
                var theReview = cmp.get('v.theReview');
                var returnReview = response.getReturnValue();
                theReview.Risk_Assessment_Complete__c = returnReview.Risk_Assessment_Complete__c;
                cmp.set('v.theReview', theReview);
            } if(response.getState() === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(recalcAction);
        
    }
})