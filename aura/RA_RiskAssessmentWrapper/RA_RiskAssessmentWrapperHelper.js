({
	checkPrevValues : function(cmp, evt) {
		var theReview = cmp.get('v.theReview');
        console.log('previously selected risks: ' + theReview.Risk_Categories_Selected__c);
        
        if(!$A.util.isEmpty(theReview.Risk_Categories_Selected__c)){
            var selectedCats = theReview.Risk_Categories_Selected__c.toLowerCase();
            var selectedCategories = cmp.get('v.selectedCategories');
            if(selectedCats.includes('execution')){
                selectedCategories.execution = true;
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'execution',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('client')){
                selectedCategories.client = true;
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'client',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('geography')){
                selectedCategories.geography = true;
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'geography',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('commercial')){
                selectedCategories.commercial = true;
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'commercial',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('contractual')){
                selectedCategories.contractual = true;
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'contractual',
                    "disable" : false
                });
                selectEvent.fire();
            }
            console.log('selectedCategories: ', selectedCategories);
            cmp.set('v.selectedCategories', selectedCategories);
        }
	}
})