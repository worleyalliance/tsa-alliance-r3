({	
    doInit : function(cmp, evt, h){
        var revRecord = cmp.get('v.theReview');
        console.log('revRecord: ', revRecord);
        if(!$A.util.isEmpty(revRecord)){
            if(!$A.util.isEmpty(revRecord.Review_Status__c) ){
                if(revRecord.Review_Status__c !== 'New') {
                    console.log('revRecord is not new: ', revRecord.Review_Status__c);
                    cmp.set("v.disabled", true);
                } else {
                	console.log('revRecord is new: ', revRecord.Review_Status__c);
                }
            }
        }
    },
	doRefresh : function(cmp, evt, h) {
		var currentStep = cmp.get('v.currentStep');
        var navEvent = $A.get('e.c:RA_navEvent');
        navEvent.setParams({
            actionType: 'refresh',
            step: currentStep
        });
        navEvent.fire();
        console.log('Firing nav event!');
	},
    doContinue : function(cmp, evt, h) {
		var currentStep = cmp.get('v.currentStep');
        var navEvent = $A.get('e.c:RA_navEvent');
        navEvent.setParams({
            actionType: 'continue',
            step: currentStep
        });
        navEvent.fire();
        console.log('Firing nav event!');
	},
    doBack : function(cmp, evt, h) {
        var currentStep = cmp.get('v.currentStep');
        var navEvent = $A.get('e.c:RA_navEvent');
        navEvent.setParams({
            actionType: 'back',
            step: currentStep
        });
        navEvent.fire();
        console.log('Firing nav event!');
    },
    doSaveClose : function(cmp, evt, h){
        var currentStep = cmp.get('v.currentStep');
        var navEvent = $A.get('e.c:RA_navEvent');
        navEvent.setParams({
            actionType: 'saveClose',
            step: currentStep
        });
        navEvent.fire();
        console.log('Firing nav event!');
    }
})