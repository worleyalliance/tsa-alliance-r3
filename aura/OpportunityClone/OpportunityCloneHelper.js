/**************************************************************************************
 Name: aura.OpportunityClone
 Version: 1.0 
 Created Date: 31.03.2017
 Function: Quick action which will clone opportunity with children records

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   31.03.2017      Original Version
 *************************************************************************************/
({
    cloneOpportunity: function (cmp) {
        this.showElement(cmp, 'modalSpinner');
        var action = cmp.get("c.doClone");
        action.setParams(
            {
                parentOpportunityId : cmp.get("v.recordId")
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var cloneOpportunityId = response.getReturnValue();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": cloneOpportunityId
                    });
                    navEvt.fire();
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        this.dismissActionPanel();
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
            this.hideElement(cmp, 'modalSpinner');
        });

        $A.enqueueAction(action);
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent) {
            message = message.replace(/{|}/g, '');
            toastEvent.setParams({
                title   : title,
                message : message,
                type    : type,
                mode    : 'sticky'
            });
            toastEvent.fire();
        }
    },
    dismissActionPanel : function() {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})