/**************************************************************************************
 Name: aura.OpportunityClone
 Version: 1.0 
 Created Date: 31.03.2017
 Function: Quick action which will clone opportunity with children records

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   31.03.2017      Original Version
 *************************************************************************************/
({
    doOpportunityClone: function(cmp, event, helper) {
        helper.cloneOpportunity(cmp);
    }
})