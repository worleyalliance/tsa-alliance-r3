/**************************************************************************************
 Name: JEG_DataTable
 Version: 1.0 
 Created Date: 07.03.2017
 Function: JEG_DataTable controller

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           07.03.2017         Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        cmp.set("v.dataLoaded", false);
        var totalRowPresent = cmp.get('v.totalRowPresentValue') == 'Yes' ? true : false;
        cmp.set('v.totalRowPresent', totalRowPresent);

        var fieldSetName         = cmp.get("v.fieldSetName"),
            recordId             = cmp.get("v.recordId"),
            sObjectName          = cmp.get('v.sObjectName'),
            relationField        = cmp.get('v.relationField'),
            isSumDefault         = cmp.get('v.defaultTotalRowBehaviour') == "Sum",
            totalRowPresent      = totalRowPresent,
            additionalValueField = cmp.get('v.additionalValueField');

        var onSuccess = function(tableData){
            cmp.set('v.data', tableData);
            cmp.set("v.dataLoaded", true);
        }
        helper.loadData(
            cmp,
            fieldSetName,
            recordId,
            sObjectName,
            relationField,
            isSumDefault,
            totalRowPresent,
            additionalValueField,
            onSuccess
        );
    }

})