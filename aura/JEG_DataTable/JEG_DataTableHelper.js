/**************************************************************************************
 Name: aura.JEG_DataTable
 Version: 1.0 
 Created Date: 08.03.2017
 Function: JEG_DataTable helper

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           08.03.2017         Original Version
 *************************************************************************************/
({
    loadData : function(
        cmp,
        fieldSetName,
        recordId,
        sObjectName,
        relationField,
        isSumDefault,
        totalRowPresent,
        additionalValueField,
        onSuccess
    ){
        var action = cmp.get("c.getData");
        action.setParams(
            {
                fieldSetName         : fieldSetName,
                recordId             : recordId,
                additionalValueField : additionalValueField,
                sObjectName          : sObjectName,
                relationField        : relationField,
                isSumDefault         : isSumDefault,
                totalRowPresent      : totalRowPresent
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var tableData = response.getReturnValue();
                    onSuccess(tableData);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})