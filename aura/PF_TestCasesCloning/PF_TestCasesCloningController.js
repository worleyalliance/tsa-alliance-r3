({
	//get WorkTask List from apex controller
    doInit : function(component, event, helper) {
        var action = component.get("c.getWorkTaskList");
        action.setParams({"parentRecordId" : component.get("v.recordId")
        });
	    action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
				component.set("v.WorkTaskList",result.getReturnValue());
                /*var resultMap = result.getReturnValue();
				var arry4 = Object.keys(resultMap);
                component.set("v.TestCaseName","Copy "+arry4[0]);
                component.set("v.WorkTaskList",resultMap[arry4[0]]);*/
            }  
        });
        $A.enqueueAction(action);
        //
    },
    getTestCaseName: function(component, event, helper) {
        component.set("v.TestCaseName", "Copy of " + component.find("hiddenOriginalName").get("v.value"));
    },
    //Select all WorkTasks
    handleSelectAllWorkTask: function(component, event, helper) {
        var getID = component.get("v.WorkTaskList");
        var checkvalue = component.find("selectAll").get("v.value");        
        var checkWorkTask = component.find("checkWorkTask"); 
        if(checkvalue == true){
            for(var i=0; i<checkWorkTask.length; i++){
                checkWorkTask[i].set("v.value",true);
            }
        }
        else{ 
            for(var i=0; i<checkWorkTask.length; i++){
                checkWorkTask[i].set("v.value",false);
            }
        }
    },
	handleSelectAllDeselect: function(component, event, helper) {
        var getID = component.get("v.WorkTaskList");
        var checkvalue = component.find("selectAll").get("v.value");        
        var checkWorkTask = component.find("checkWorkTask"); 
		var isAllSelected = true;
		for(var i=0; i<checkWorkTask.length; i++){
			if (checkWorkTask[i].get("v.value") == false) {
				isAllSelected = false;
			}
		}
		if(isAllSelected){
			component.set("v.isSelectAll", true);
		}else{
			component.set("v.isSelectAll", false);
		}
		isAllSelected = true;
    },
	
    //Process the selected WorkTasks
    saveTestCase : function (component, event, helper) {
        component.set("v.showSpinner", true);
		var listOfWorkTask = component.get("v.WorkTaskList");
        var modifiedTestCaseName = component.get("v.TestCaseName");
		var selectedWorkTasks =[];
		for(var i=0; i<listOfWorkTask.length; i++){
			if (listOfWorkTask[i].isSelected == true && listOfWorkTask[i].numberOfChildRecord > 0) {
				selectedWorkTasks.push(listOfWorkTask[i].childObjName); 
			}
		}
		
        var action = component.get("c.getCloneRecord");
        action.setParams({
            "relatedChildsToClone" : selectedWorkTasks,
           	"newPfStoriesId" : component.get("v.recordId"),
            "newPfStoriesName" : modifiedTestCaseName
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if(component.isValid() && state === "SUCCESS"){
                var navEvent = $A.get("e.force:navigateToSObject"); 
                navEvent.setParams({ recordId: result.getReturnValue(), slideDevName: "detail" }); 
                navEvent.fire();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Test Case "'+modifiedTestCaseName+'" was created.',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
            } else {
                //alert('error>>>>'+JSON.stringify(result.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    
    //Control the component behavior here when record is changed (via any component)
    handleRecordUpdated : function (component, event, helper) {
        var eventParams = event.getParams();
        if (eventParams.changeType === "CHANGED") {
            // get the fields that are changed for this record
            var changedFields = eventParams.changedFields;
            console.log("Fields that are changed: " + JSON.stringify(changedFields));
            // record is changed so refresh the component (or other component logic)
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "title" : "Saved",
                "message" : "The record was updated!"
            });
            resultsToast.fire();
        }
        else if (eventParams.changeType === "LOADED") {
            // record is loaded in the cache
        }
            else if (eventParams.changeType === "REMOVED") {
                // record is deleted and removed from the cache
            }
                else if (eventParams.changeType === "ERROR") {
                    console.log("Error: " + component.get("v.error"));
                }
    },
    
    cancel : function (component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
     // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.showSpinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.showSpinner", false);
    }
})