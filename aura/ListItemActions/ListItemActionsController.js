/**************************************************************************************
 Name: aura.ListItemActions
 Version: 1.0 
 Created Date: 20.04.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   20.04.2017      Original Version
 *************************************************************************************/
({
    onAction: function (cmp, event, helper) {
        var actionName = event.getSource().get('v.value');
        helper.fireActionEvent(cmp, actionName, cmp.get('v.index'));
    }
})