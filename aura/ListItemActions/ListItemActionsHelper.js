/**************************************************************************************
 Name: aura.ListItemActions
 Version: 1.0 
 Created Date: 20.04.2017
 Function: Helper for ListItemActions

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   20.04.2017      Original Version
 *************************************************************************************/
({
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    },
    fireActionEvent: function (cmp, actionName, itemIndex) {
        var actionEvent = cmp.getEvent("actionChosen");
        actionEvent.setParams({
            actionName: actionName,
            itemIndex : itemIndex
        });
        actionEvent.fire();
    }
})