({
	doBack : function(cmp, evt, h) {
        var currentPage = cmp.get('v.currentPage');
        if(currentPage == 1){
            cmp.set('v.currentStep', 'Step 300');
        } else {
            cmp.set('v.currentPage', currentPage - 1);
        }
    },
    doContinue : function(cmp, evt, h){
        var currentPage = cmp.get('v.currentPage');
        if(currentPage == cmp.get('v.totalPages')){ // if current page is the last page
            //  Change to add requirement validation later
            var selectedCategories = cmp.get('v.selectedCategories');
            if(selectedCategories.geography){
                cmp.set('v.currentStep', 'Step 500')
            } else if(selectedCategories.commercial){
                cmp.set('v.currentStep', 'Step 600')
            } else if(selectedCategories.contractual){
                cmp.set('v.currentStep', 'Step 700')
            } else {
                cmp.set('v.currentStep', 'Step 800');
            }
        } else {
            cmp.set('v.currentPage', currentPage + 1);
        }
    },
    doSaveClose : function(cmp, evt, h){
        var theSpinner = cmp.find('theSpinner');
        $A.util.toggleClass(theSpinner, 'slds-hide');
        var theReview = cmp.get('v.theReview');
        console.log('theReview', theReview);
        var saveAction = cmp.get('c.saveReviewRec')
        saveAction.setParams({
            reviewRecord: theReview
        });
        saveAction.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                location.reload();
                //$A.get('e.force:closeQuickAction').fire();
            } else if(response.getState() === 'ERROR'){
                console.log('error: ', response.getError());
            }
        });
        $A.enqueueAction(saveAction);
    },
})