({
	getMilestones : function(cmp, evt) {
		var recordId = cmp.get('v.recordId');
        var getAction = cmp.get('c.newGetMilestones');
        getAction.setParams({
            parentId: recordId,
            scale: cmp.get('v.scale'),
            objectName: cmp.get('v.objectName'),
            relationFieldName: cmp.get('v.relationName'),
            startDateFieldName: cmp.get('v.startDateFieldName'),
            endDateFieldName: cmp.get('v.endDateFieldName'),
            typeFieldName: cmp.get('v.typeFieldName')
        });
        console.log('recordId: ', recordId);
        console.log('scale: ', cmp.get('v.scale'));
        console.log('objectName: ', cmp.get('v.objectName'));
        console.log('relationFieldName: ', cmp.get('v.relationFieldName'));
        console.log('startDateFieldName: ', cmp.get('v.startDateFieldName'));
        console.log('endDateFieldName: ', cmp.get('v.endDateFieldName'));
        console.log('typeFieldName: ', cmp.get('v.typeFieldName'));
        getAction.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var returnVal = response.getReturnValue();
                console.log('response: ', returnVal);
                cmp.set('v.dates', returnVal.dates);
                cmp.set('v.milestones', returnVal.milestones);
            } else if(response.getState() === 'ERROR'){
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(getAction);
	}
})