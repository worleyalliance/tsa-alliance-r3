/*****************************************************************************************
 Name: aura.createRecordFooterComponent
 Version: 1.0 
 Created Date: 05.04.2017
 Function: Component is used to gather all common functionalities like whowing/hiding element

 Modification Log:
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Developer         Date            Description
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -             
 * Pradeep Shetty                    Original Version
 * Pradeep Shetty    09/05/2018      US2327: Added back button and logic to handle preview
 *****************************************************************************************/
({  
    /*Function called when the cancel button is clicked*/
    doCancel: function(cmp, event, helper){
		  cmp.find("overlayLib").notifyClose();
    },
    /*Function called when the Next buton is clicked*/
    doNext: function(cmp, event, helper){
      cmp.set("v.spinnerVisibility", true);
      cmp.set("v.buttonsDisabled", true);
      cmp.get("v.modalBodyComponent").nextAction();        
    },
    /*US2327: Added function that is called when Back button is clicked*/
    goBack: function(cmp, event, helper){
      cmp.set("v.spinnerVisibility", true);
      cmp.set("v.buttonsDisabled", true);      
      cmp.get("v.modalBodyComponent").backAction();
    },
    /*Function to change the spinner visibility*/
    onReceivingMessage: function(cmp,event,helper){
      //Get parameters sent by the action call
      var channel = event.getParam("channel");
      
      //If the message is sent on the right channel then get the message parameter
      if(channel === "BookingValueFooterChannel"){
        var message = event.getParam("message");
        //If the event sends hidespinner, then hide the spinner
        if(message==="hideSpinner"){
          cmp.set("v.spinnerVisibility", false);
          cmp.set("v.buttonsDisabled", false);            
    	  }
        //If the event sends "showPreview" change the button label to Save
        else if(message==="showPreview"){
          cmp.set("v.labelName", $A.get("$Label.c.Save"));
          cmp.set("v.showBackButton", true);
        }
        //If the event sends "showForm" change the button label to Preview
        else if(message==="showForm"){
          cmp.set("v.labelName", $A.get("$Label.c.Preview"));
          cmp.set("v.showBackButton", false);
        }        
		  }
	  }
    
})