/**************************************************************************************
 Name: aura.ScheduleMilestoneChart
 Version: 1.0 
 Created Date: 17.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   17.03.2017      Original Version
 * Chris Cornejo     9/5/2018        US2404 - Schedule Milestone: Visual Status on Chart
 * Chris Cornejo     10/3/2018       US2405 - Schedule Milestone - Add Year to Schedule Milestone Graph
 *************************************************************************************/
({

    load: function(cmp, callback) {
        var action = cmp.get("c.getMilestones");
        action.setParams({
            parentId            : cmp.get("v.recordId"),
            objectTypeName      : cmp.get("v.objectTypeName"),
            relationFieldName   : cmp.get("v.relationFieldName"),
            startDateFieldName  : cmp.get("v.startDateFieldName"),
            endDateFieldName    : cmp.get("v.endDateFieldName"),
            typeFieldName       : cmp.get("v.typeFieldName"),
            /* US2404 - able to pull status */
            statusFieldName          : cmp.get("v.statusFieldName")
        });
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var milestones = response.getReturnValue();
                    cmp.set("v.milestones", milestones);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        this.showToast('error', null, errors[0].message);
                    }
                }
                if(callback) {
                    callback(milestones);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getTickFormat: function(scale){
        var format = "%m/%d/%Y";
        if(scale == 'Week'){
            format = "%x";
        } else if(scale == 'Month'){
            format = "%B %Y";
        } //US2405
          else if(scale == 'Year'){
            format = "%Y";
        }
        return format;
    },
    getTickScale: function(scale){
        var xTicks = [d3.timeDay, 1];
        if(scale == 'Week'){
            xTicks = [d3.timeWeek, 1];
        } else if(scale == 'Month'){
            xTicks = [d3.timeMonth, 1];
        } //US2405 
        else if(scale == 'Year'){
            xTicks = [d3.timeYear, 1];
        }
        return xTicks;
    },
    getScaleFactor: function(scale){
        var factor = 38;
        if(scale == 'Week'){
            factor = 9;
        } else if(scale == 'Month'){
            factor = 3;
        } //US2405
        else if(scale == 'Year'){
            factor = 0.5; //US2405 - how spread out year is from next tick mark
        }
        return factor;
    },
    getXAxisLabelSloping: function(scale){
        var isXLabelSloping = true;
        if(scale == 'Week'){
            isXLabelSloping = false;
        } else if(scale == 'Month'){
            isXLabelSloping = false;
        } //US2405
          else if(scale == 'Year'){
            isXLabelSloping = false;
        }
        return isXLabelSloping;
    },
    getDayOffsetValue: function (scale) {
        var offset = 0.5;
        if(scale == 'Week'){
            offset = 1;
        } else if(scale == 'Month'){
            offset = 6;
        } //US2405
        else if(scale == 'Year'){
            offset = 12;
        }
        return offset;
    },
    getMinDaysBetweenValue: function (scale, daysBetween) {
        var minDaysBetween = daysBetween < 30 ? 30 : daysBetween;
        if(scale == 'Week'){
            minDaysBetween = daysBetween < 120 ? 120 : daysBetween;
        } else if(scale == 'Month'){
            minDaysBetween = daysBetween < 365 ? 365 : daysBetween;
        } //US2405
          else if(scale == 'Year'){
            minDaysBetween = daysBetween < 2190 ? 2190 : daysBetween; //US2405 - Display 6 years of year data
        }
        return minDaysBetween;
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent) {
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": type
            });
            toastEvent.fire();
        }
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    },
    getDaysBetween: function (firstDate, secondDate) {
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        return diffDays;
    }
})