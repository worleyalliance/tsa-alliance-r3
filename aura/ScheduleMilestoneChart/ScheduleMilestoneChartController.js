/**************************************************************************************
 Name: aura.ScheduleMilestoneChart
 Version: 1.0 
 Created Date: 16.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   16.03.2017      Original Version
 * Chris Cornejo     9/5/2018        US2404 - Schedule Milestone: Visual Status on Chart
 * Chris Cornejo     10/3/2018       US2405 - Schedule Milestone - Add Year to Schedule Milestone Graph
 *************************************************************************************/
({
    doInit: function (cmp, event, helper) {
        helper.hideElement(cmp, 'sectionIcon');
        helper.showElement(cmp, 'sectionSpinner');
        var componentCustomId = 'chart'+cmp.getGlobalId().replace(":", "-").replace(";", "-");
        cmp.set('v.componentCustomId', componentCustomId);
        helper.load(cmp, function (tasks) {
            d3.select('.'+componentCustomId + " svg.chart").remove();
            if(tasks && tasks.length > 0) {
                helper.hideElement(cmp, 'noDataMessage');
                helper.showElement(cmp, 'scaleButtons');
                var taskStatus = {
                    //"oneDay": "bar-one-day",
                    //"multiDay": "bar-multi-day",
                    //US2404 - added visuals dependent on status
                    "Past Due" : "bar-past-due",
                    "Closed" : "bar-closed",
                    "Upcoming" : "bar-upcoming"
                };

                var taskNames = [];

                for (var i = 0; i < tasks.length; i++) {
                    var startDate = new Date(tasks[i].startDate);
                    tasks[i].startDate = new Date(startDate.valueOf() + startDate.getTimezoneOffset() * 60000);
                    var endDate = new Date(tasks[i].endDate);
                    tasks[i].endDate = new Date(endDate.valueOf() + endDate.getTimezoneOffset() * 60000);
                    tasks[i].oneDayTask = tasks[i].startDate.getTime() == tasks[i].endDate.getTime();
                    taskNames.push(tasks[i].taskName);
                }

                tasks.sort(function (a, b) {
                    return a.endDate - b.endDate;
                });
                var timeDomainEnd = tasks[tasks.length -1].endDate;
                tasks.sort(function (a, b) {
                    return a.startDate - b.startDate;
                });
                var timeDomainStart = tasks[0].startDate;
                var daysBetween = helper.getDaysBetween(timeDomainStart, timeDomainEnd);

                cmp.set('v.timeDomainEnd', timeDomainEnd);
                cmp.set('v.timeDomainStart', timeDomainStart);

                cmp.set('v.timeDomainInDays', daysBetween);

                var dayButtonDisabled,weekButtonDisabled,monthButtonDisabled;
                /*
                if(daysBetween < 7){
                    dayButtonDisabled = false;
                    weekButtonDisabled = true;
                    monthButtonDisabled = true;
                } else if(daysBetween < 30){
                    dayButtonDisabled = false;
                    weekButtonDisabled = false;
                    monthButtonDisabled = true;
                } else if(daysBetween < 365){
                    dayButtonDisabled = true;
                    weekButtonDisabled = false;
                    monthButtonDisabled = false;
                } else {
                    dayButtonDisabled = true;
                    weekButtonDisabled = true;
                    monthButtonDisabled = false;
                }
                cmp.set('v.dayButtonDisabled', dayButtonDisabled);
                cmp.set('v.weekButtonDisabled', weekButtonDisabled);
                cmp.set('v.monthButtonDisabled', monthButtonDisabled);
                 */

                var scaleDomain = cmp.get('v.scaleDomain');

                if(scaleDomain == 'Day' && dayButtonDisabled && !weekButtonDisabled){
                    scaleDomain = 'Week';
                } else if(scaleDomain == 'Day' && dayButtonDisabled && weekButtonDisabled){
                    scaleDomain = 'Month';
                }else if(scaleDomain == 'Week' && weekButtonDisabled && !monthButtonDisabled){
                    scaleDomain = 'Month';
                } else if(scaleDomain == 'Week' && weekButtonDisabled && monthButtonDisabled){
                    scaleDomain = 'Day';
                } else if(scaleDomain == 'Month' && monthButtonDisabled && !weekButtonDisabled){
                    scaleDomain = 'Week';
                } else if(scaleDomain == 'Month' && monthButtonDisabled && weekButtonDisabled){
                    scaleDomain = 'Day';
                }

                if(daysBetween < helper.getMinDaysBetweenValue(scaleDomain, daysBetween) ){
                    timeDomainEnd = d3.timeDay.offset(timeDomainEnd, helper.getMinDaysBetweenValue(scaleDomain, daysBetween) - daysBetween);
                }

                cmp.set('v.scaleDomain', scaleDomain);
                timeDomainStart = d3.timeDay.offset(timeDomainStart, -helper.getDayOffsetValue(scaleDomain));
                timeDomainEnd = d3.timeDay.offset(timeDomainEnd, helper.getDayOffsetValue(scaleDomain));

                var format = helper.getTickFormat(scaleDomain);
                var xTicks = helper.getTickScale(scaleDomain);

                var onlyUnique = function (value, index, self) {
                    return self.indexOf(value) === index;
                };
                taskNames = taskNames.filter(onlyUnique);
                var scaleFactor = helper.getScaleFactor(scaleDomain);
                var isXAxisLabelSloping = helper.getXAxisLabelSloping(scaleDomain);
                cmp.gantt =
                    d3.gantt()
                        .xTicks(xTicks)
                        .taskTypes(taskNames)
                        .taskStatus(taskStatus)
                        .tickFormat(format)
                        .timeDomainMode(d3.gantt.FIXED_TIME_DOMAIN_MODE)
                        .timeDomain([timeDomainStart, timeDomainEnd])
                        .selector('.'+componentCustomId)
                        .isXLabelSloping(isXAxisLabelSloping)
                        .margin({top: 70, bottom: 0, left: 100, right: 40})
                        .height(100 + taskNames.length * 40)
                        .width(helper.getMinDaysBetweenValue(scaleDomain, daysBetween) *scaleFactor);
                cmp.gantt(tasks);
            } else {
                helper.showElement(cmp, 'noDataMessage');
                helper.hideElement(cmp, 'scaleButtons');
            }
            helper.hideElement(cmp, 'sectionSpinner');
            helper.showElement(cmp, 'sectionIcon');
        });
    },
    onTimeScaleChange: function (cmp, event, helper) {
        //US2405 - get viewSelection from new Radio button group
        var scaleDomain = cmp.get('v.viewSelection');
        var daysBetween = cmp.get('v.timeDomainInDays');
        var timeDomainStart = cmp.get('v.timeDomainStart');
        var timeDomainEnd = cmp.get('v.timeDomainEnd');

        timeDomainStart = d3.timeDay.offset(timeDomainStart, -helper.getDayOffsetValue(scaleDomain));
        timeDomainEnd = d3.timeDay.offset(timeDomainEnd, helper.getDayOffsetValue(scaleDomain));
        if(daysBetween < helper.getMinDaysBetweenValue(scaleDomain, daysBetween) ){
            timeDomainEnd = d3.timeDay.offset(timeDomainEnd, helper.getMinDaysBetweenValue(scaleDomain, daysBetween) - daysBetween);
        }
        var tasks = cmp.get("v.milestones");
        cmp.set('v.scaleDomain', scaleDomain);
        var format = helper.getTickFormat(scaleDomain);
        var xTicks = helper.getTickScale(scaleDomain);
        var scaleFactor = helper.getScaleFactor(scaleDomain);
        var isXAxisLabelSloping = helper.getXAxisLabelSloping(scaleDomain);
        cmp.gantt.isXLabelSloping(isXAxisLabelSloping);
        cmp.gantt.xTicks(xTicks);
        cmp.gantt.tickFormat(format);
        cmp.gantt.timeDomain([timeDomainStart, timeDomainEnd]);
        cmp.gantt.width(helper.getMinDaysBetweenValue(scaleDomain, daysBetween)*scaleFactor);
        cmp.gantt.redraw(tasks);
    },
    
    refresh : function (cmp) {
        var componentCustomId = cmp.get('v.componentCustomId');
        d3.select('.'+componentCustomId + " g.gantt-chart").remove();
        cmp.doInit();
    }
})