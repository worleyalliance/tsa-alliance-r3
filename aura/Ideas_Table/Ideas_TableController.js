({
	fetchTOs : function(cmp, event, helper) {
		var action = cmp.get("c.getallIdeas");
        
    //Column data for the table
        var toColumns = [
            {
                'label':'Idea Name',
                'name':'Name',
                'type':'reference',
                'value':'Id',
                'width': 400,
                'resizeable':true
            },
          {
                'label':'Status',
                'name':'Idea_Status__c',
                'type':'reference',
                'value':'Id',
                'width': 190,
                'resizeable':true
          },
            {
                'label':'Category',
                'name':'Idea_Category__c',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'width': 240,
                'resizeable':true
            },
            {
                'label':'Count of Up Votes',
                'name':'Count_of_Up_Votes__c',
                'type':'reference',
                'value':'Id',
                'type':'number',
                'resizeable':true
            }
        ];
        
    //Configuration data for the table to enable actions in the table
  /*     var toTableConfig = {
            "massSelect":true,
            //"globalAction":[
            //    {
            //        "label":"Add Task Order",
            //        "type":"button",
            //        "id":"addTO",
            //        "class":"slds-button slds-button--neutral"
            //    }
            //],
            "rowAction":[
                {
                    "label":"Edit",
                    "type":"URL",
                    "id":"editTO"
                },
                {
                    "label":"Del",
                    "type":"URL",
                    "id":"delTO"
                }
                ,
                {
                    "type":"menu",
                    "id":"actions",
                    "visible":function(Library__c){
                        return Library__c.lr_project__c == "v.projectId"
                    }
                }
            ]
            
        };
 */       
            var selectedAccount =cmp.get("v.recordId")
               action.setParams({
           "id" : selectedAccount
        });
            
            action.setCallback(this,function(resp){
                var state = resp.getState();
            
            if(cmp.isValid() && state === 'SUCCESS'){
                
                //pass the records to be displayed
                cmp.set("v.libraryTOs",resp.getReturnValue());
                console.log(resp.getReturnValue())
                //pass the column information
                cmp.set("v.toColumns",toColumns);
                
                //pass the configuration of CBA Table
  //              cmp.set("v.toTableConfig",toTableConfig);
                
                //initialize the data table
                //cmp.find("cbaTable").initialize(); 
                window.setTimeout($A.getCallback(function() {
    cmp.find("toTable").initialize();
}),1000);	
            }
            else {
                console.log(resp.getError());
            }
            	});
            $A.enqueueAction(action);
        
        //adding this 5/30
        console.log("yay1");
            var action2 = cmp.get("c.fetchIdeasCategories");
            console.log("yay2");
            var inputsel = cmp.find("mySelect");
            var opts=[];
            var opts = [
            { value: "All", label: "All" },

         ];
            action2.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
             opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                                                        }
           cmp.set("v.options", opts);
                             });
           $A.enqueueAction(action2); 
    
            

    
	
        
},
    ideasCategorySelected: function(component, event, helper){
        console.log("yay");
        var res = component.find("mySelect").get("v.value");
        console.log(res);
        var action = component.get("c.getideasCategoryType");
        //var recordTypeLabel = component.find("mySelect").get("v.value");

        //Column data for the table
        var toColumns = [
            {
                'label':'Idea Name',
                'name':'Name',
                'type':'reference',
                'value':'Id',
                'width': 400,
                'resizeable':true
            },
          {
                'label':'Status',
                'name':'Idea_Status__c',
                'type':'reference',
                'value':'Id',
                'width': 190,
                'resizeable':true
          },
            {
                'label':'Category',
                'name':'Idea_Category__c',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'width':240,
                'resizeable':true
            },
            {
                'label':'Count of Up Votes',
                'name':'Count_of_Up_Votes__c',
                'type':'reference',
                'value':'Id',
                'type':'number',
                'resizeable':true
            }
        ];
       //Configuration data for the table to enable actions in the table
/*        var toTableConfig = {
            "massSelect":true,
            //"globalAction":[
            //    {
            //        "label":"Add Task Order",
            //        "type":"button",
            //        "id":"addTO",
            //        "class":"slds-button slds-button--neutral"
            //    }
            //],
            "rowAction":[
                {
                    "label":"Edit",
                    "type":"URL",
                    "id":"editTO"
                },
                {
                    "label":"Del",
                    "type":"URL",
                    "id":"delTO"
                }
                ,
                {
                    "type":"menu",
                    "id":"actions",
                    "visible":function(Library__c){
                        return Library__c.lr_project__c == "v.projectId"
                    }
                }
            ]
            
        };
        
*/
            var selectedAccount =component.get("v.recordId")
               action.setParams({
            "recordTypeLabel": res
        });
            
            action.setCallback(this,function(resp){
                var state = resp.getState();
            
            if(component.isValid() && state === 'SUCCESS'){
                
                //pass the records to be displayed
                component.set("v.libraryTOs",resp.getReturnValue());
                console.log(resp.getReturnValue());
                //pass the column information
                component.set("v.toColumns",toColumns);
                
                //pass the configuration of CBA Table
 //               component.set("v.toTableConfig",toTableConfig);
                
                //initialize the data table
                //cmp.find("cbaTable").initialize(); 
                window.setTimeout($A.getCallback(function() {
    				component.find("toTable").initialize();
				                                            }),1000);	
            }
            else {   console.log(resp.getError());   }
            	});
            $A.enqueueAction(action);
    }
    

})