({
    handleReviewChange : function(cmp, evt, h) {
        var theReview = evt.getParam('theReview');
        console.log('change event handled!');
        console.log('review record: ', cmp.get('v.theReview'));
        console.log('event param record: ', theReview);
        
        if(!$A.util.isEmpty(theReview)){
            var selectedCategories = cmp.get('v.selectedCategories');
            var theCats = {};
            theCats.execution = selectedCategories.execution ? 'true' : 'false';
            theCats.client = selectedCategories.client ? 'true' : 'false';
            theCats.geography = selectedCategories.geography ? 'true' : 'false';
            theCats.commercial = selectedCategories.commercial ? 'true' : 'false';
            theCats.contractual = selectedCategories.contractual ? 'true' : 'false';
            theCats.opportunity = 'true';
            
            console.log('theCats: ', theCats);
            
            var getScoreAction = cmp.get('c.getRiskScore');
            getScoreAction.setParams({
                theReviewRec : theReview,
                selectedCategories : theCats
            });
            getScoreAction.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){
                    var theReturn = response.getReturnValue();
                    console.log('riskScore return: ', theReturn);
                    cmp.set('v.riskLevel', theReturn.riskLevel);
                    cmp.set('v.riskCategory', theReturn.category);
                    cmp.set('v.iconColor', theReturn.iconColor);
                    var theReview = cmp.get('v.theReview');
                    console.log('theReview after score: ' + theReview);
                    theReview.Risk_Assessment__c = theReturn.riskLevel + ' - ' + theReturn.category;
                    cmp.set('v.theReview', theReview);
                } else if(response.getState() === 'ERROR'){
                    console.log('ERROR: ', response.getError());
                }
            });
            $A.enqueueAction(getScoreAction);
            
            // Setup event for section-only risk score
            var currentStep = cmp.get('v.currentStep');
            console.log('currentstep: ' + currentStep);
            var cats = {execution: 'false', 
                        client: 'false', 
                        geography: 'false', 
                        commercial: 'false', 
                        contractual: 'false'};
            switch(currentStep) {
                case 'Step 300':
                    cats.execution = 'true';
                    break;
                case 'Step 400':
                    cats.client = 'true';
                    break;
                case 'Step 500':
                    cats.geography = 'true';
                    break;
                case 'Step 600':
                    cats.commercial = 'true';
                    break;
                case 'Step 700':
                    cats.contractual = 'true';
                    break;
            }
            var getSectionScore = cmp.get('c.getRiskScore');
            getSectionScore.setParams({
                theReviewRec : theReview,
                selectedCategories : cats
            });
            console.log('cats: ', cats);
            getSectionScore.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var currentStep = cmp.get('v.currentStep');
                    var theReview = cmp.get('v.theReview');
                    console.log('section risk return: ', response.getReturnValue());
                    switch(currentStep) {
                        case 'Step 300':
                            theReview.Execution_Risk_Score__c = response.getReturnValue().riskLevel;
                            theReview.Execution_Risk_Category__c = response.getReturnValue().category;
                            break;
                        case 'Step 400':
                            theReview.Client_Risk_Score__c = response.getReturnValue().riskLevel;
                            theReview.Client_Risk_Category__c = response.getReturnValue().category;
                            break;
                        case 'Step 500':
                            theReview.Geography_Risk_Score__c = response.getReturnValue().riskLevel;
                            theReview.Geography_Risk_Category__c = response.getReturnValue().category;
                            break;
                        case 'Step 600':
                            theReview.Commercial_Risk_Score__c = response.getReturnValue().riskLevel;
                            theReview.Commercial_Risk_Category__c = response.getReturnValue().category;
                            break;
                        case 'Step 700':
                            theReview.Contractual_Risk_Score__c = response.getReturnValue().riskLevel;
                            theReview.Contractual_Risk_Category__c = response.getReturnValue().category;
                            break;
                    }
                    cmp.set('v.theReview', theReview);
                } else if(response.getState() == 'ERROR'){
                    console.log('ERROR: ' + response.getError());
                }
            });
            $A.enqueueAction(getSectionScore);
        } else {
            console.log('RiskScoreController else run')
            var theReview = cmp.get('v.theReview');
            console.log('RiskScoreController else theReview: ', theReview);
            var riskLevel = 'R7';
            var riskCategory = 'Standard';
            var riskColor = '#2FC9A1';
            
            var cats = {execution: 'false', 
                        client: 'false', 
                        geography: 'false', 
                        commercial: 'false', 
                        contractual: 'false',
                        opportunity: 'true'};
            var getScoreAction = cmp.get('c.getRiskScore');
            console.log('theReviewRec: ', theReview);
            getScoreAction.setParams({
                theReviewRec: theReview,
                selectedCategories: cats
            });
            getScoreAction.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){
                    var returnVal = response.getReturnValue();
                    var riskLevel = returnVal.riskLevel;
                    var riskCategory = returnVal.category;
                    
                    theReview.Opportunity_Risk_Score__c = riskLevel;
                    theReview.Opportunity_Risk_Category__c = riskCategory;
                    cmp.set('v.theReview', theReview);
                    
                    if(!$A.util.isEmpty(theReview.Execution_Risk_Score__c )){
                        var existingLevel = parseInt(riskLevel.substring(1));
                        console.log('existing level: ' + existingLevel);
                        var thisLevel = parseInt(theReview.Execution_Risk_Score__c.substring(1));
                        console.log('execution level: ' + thisLevel);
                        if(thisLevel < existingLevel){
                            riskLevel = theReview.Execution_Risk_Score__c;
                            riskCategory = theReview.Execution_Risk_Category__c;
                        }
                    }
                    if(!$A.util.isEmpty(theReview.Client_Risk_Score__c)){
                        var existingLevel = parseInt(riskLevel.substring(1));
                        var thisLevel = parseInt(theReview.Client_Risk_Score__c.substring(1));
                        if(thisLevel < existingLevel){
                            riskLevel = theReview.Client_Risk_Score__c;
                            riskCategory = theReview.Client_Risk_Category__c;
                        }
                    }
                    if(!$A.util.isEmpty(theReview.Geography_Risk_Score__c )){
                        var existingLevel = parseInt(riskLevel.substring(1));
                        var thisLevel = parseInt(theReview.Geography_Risk_Score__c.substring(1));
                        if(thisLevel < existingLevel){
                            riskLevel = theReview.Geography_Risk_Score__c;
                            riskCategory = theReview.Geography_Risk_Category__c;
                        }
                    }
                    if(!$A.util.isEmpty(theReview.Commercial_Risk_Score__c )){
                        var existingLevel = parseInt(riskLevel.substring(1));
                        var thisLevel = parseInt(theReview.Commercial_Risk_Score__c.substring(1));
                        if(thisLevel < existingLevel){
                            riskLevel = theReview.Commercial_Risk_Score__c;
                            riskCategory = theReview.Commercial_Risk_Category__c;
                        }
                    }
                    if(!$A.util.isEmpty(theReview.Contractual_Risk_Score__c )){
                        var existingLevel = parseInt(riskLevel.substring(1));
                        var thisLevel = parseInt(theReview.Contractual_Risk_Score__c.substring(1));
                        if(thisLevel < existingLevel){
                            riskLevel = theReview.Contractual_Risk_Score__c;
                            riskCategory = theReview.Contractual_Risk_Category__c;
                        }
                    }
                    if(riskCategory === 'High') {
                        riskColor = '#c92a2e';
                    } else if(riskCategory === 'Elevated') {
                        riskColor = '#ffc52d';
                    } else {
                        riskColor = '#2FC9A1';
                    }
                    console.log('setting risk level:');
                    console.log('color: ' + riskColor);
                    console.log('level: ' + riskLevel);
                    console.log('category: ' + riskCategory);
                    if(!cmp.get('v.oppError')){
                        cmp.set('v.iconColor', riskColor);
                        cmp.set('v.riskLevel', riskLevel);
                        cmp.set('v.riskCategory', riskCategory);
                    }
                } else if(response.getState() === 'ERROR'){
                    console.log('ERROR: ', response.getError());
                }
            });
            $A.enqueueAction(getScoreAction);
        }
    },
    handleOpptyError : function(cmp, evt, h){
        var hasError = evt.getParam('hasError');
        if(hasError){
            cmp.set('v.oppError', true);
            cmp.set('v.riskLevel', 'N/A');
            //cmp.set('v.category', 'Unknown');
            cmp.set('v.iconColor', '#DDD');
        } else{
            cmp.set('v.oppError', false);
        }
    }
})