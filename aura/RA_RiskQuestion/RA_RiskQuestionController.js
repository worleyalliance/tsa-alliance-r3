({
    doInit : function(cmp, evt, h) {
        
        console.log('RiskQuestion Init');
        var apiName = cmp.get('v.apiName');
        var theReview = cmp.get('v.theReview');
        var revValue = theReview[apiName];
        cmp.set('v.currentValue', revValue);
        console.log('looking for ' + revValue);
        var selectedElement = cmp.find(revValue);
        if(!$A.util.isEmpty(selectedElement)){
            console.log('found: ', selectedElement[0]);
        }
        var type = cmp.get('v.type');
        switch(type){
            case 'Execution':
                var flagged = theReview.Flagged_Execution_Questions__c;
                if(flagged != null && flagged.includes(cmp.get('v.apiName'))){
                    var flagBox = cmp.find('flagBox');
                    flagBox.set('v.checked', true);
                }
                break;
            case 'Client':
                var flagged = theReview.Flagged_Client_Questions__c;
                if(flagged != null && flagged.includes(cmp.get('v.apiName'))){
                    var flagBox = cmp.find('flagBox');
                    flagBox.set('v.checked', true);
                }
                break;
            case 'Geography':
                var flagged = theReview.Flagged_Geography_Questions__c;
                if(flagged != null && flagged.includes(cmp.get('v.apiName'))){
                    var flagBox = cmp.find('flagBox');
                    flagBox.set('v.checked', true);
                }
                break;
            case 'Commercial':
                var flagged = theReview.Flagged_Commercial_Questions__c;
                if(flagged != null && flagged.includes(cmp.get('v.apiName'))){
                    var flagBox = cmp.find('flagBox');
                    flagBox.set('v.checked', true);
                }
                break;
            case 'Contractual':
                var flagged = theReview.Flagged_Contractual_Questions__c;
                if(flagged != null && flagged.includes(cmp.get('v.apiName'))){
                    var flagBox = cmp.find('flagBox');
                    flagBox.set('v.checked', true);
                }
                break;
        }
        
        /*var apiName = cmp.get('v.apiName');
        console.log('question API name: ' + apiName);
        $A.createComponent(
            'lightning:radioGroup',
            {
                "aura:id": "executionQuestion",
                "name": cmp.get('v.apiName'),
                "variant": "label-hidden",
                "options": cmp.get('v.options'),
                "value": cmp.getReference('v.theReview.' + apiName),
                "onchange": cmp.getReference('c.radioChange'),
                "onclick": cmp.getReference('c.radioClick')
            },
            function(newRadio, status, errorMessage){
                if(status == 'SUCCESS'){
                    console.log('question sucessfully created');
                    cmp.set('v.body', newRadio);
                } else if(status == 'ERROR'){
                    alert(errorMessage);
                }
            }
        );*/
    },
    radioChange : function(cmp, evt, h){        
        
        console.log('radio changed');
        console.log('evt: ', evt);
        console.log('evt source: ', evt.srcElement);
        var eventSrc = evt.srcElement;
        var apiName = cmp.get('v.apiName');
        console.log('event source: ' + eventSrc.checked);
        console.log('currentValue: ' + cmp.get('v.currentValue'));
        console.log('eventValue: ' + eventSrc.value);
        if(eventSrc.value == cmp.get('v.currentValue')){
            console.log('already selected: deselecing');
            eventSrc.checked = false;
            cmp.set('v.currentValue', null);
            
            var theReview = cmp.get('v.theReview');
            theReview[apiName] = null;
            cmp.set('v.theReview', theReview);
        } else {
            console.log('not selected: selecting');
            cmp.set('v.currentValue', eventSrc.value);
            var theReview = cmp.get('v.theReview');
            theReview[apiName] = eventSrc.value;
            cmp.set('v.theReview', theReview);
        }
        
        var changeEvent = $A.get("e.c:RA_reviewChangeEvent")
        changeEvent.setParams({
            theReview : cmp.get('v.theReview')
        });
        changeEvent.fire();
        
        $A.get("e.c:RA_RecalcPercentComplete").fire();
        
    },
    radioClick : function(cmp, evt, h){
        var apiName = cmp.get('v.apiName');
        var theReview = cmp.get('v.theReview');
        var theSource = evt.getSource();
        var currentValue = cmp.get('v.currentValue');
        console.log('theSource: ', theSource);
        console.log('theSource value: ' + theSource.get('v.value'));
        console.log('currentValue: ' + currentValue);
        var sourceValue = theSource.get('v.value');
        console.log('review value: ' + theReview[apiName]);
        
        theReview[apiName] = null;
        cmp.set('v.theReview', theReview);
        
    },
    flagChange : function(cmp, evt, h){
        var type = cmp.get('v.type');
        var clickCmp = evt.getSource();
        var clicked = clickCmp.get('v.checked');
        console.log('clicked: ' + clicked)
        var theReview = cmp.get('v.theReview');
        console.log('theReview', theReview);
        switch(type){
            case 'Execution':
                var flagged = theReview.Flagged_Execution_Questions__c;
                if(flagged == null){
                    flagged = '';
                }
                if(clicked && !flagged.includes(cmp.get('v.apiName'))){
                    flagged += (cmp.get('v.apiName')) + ';';
                } else if(!clicked && flagged.includes(cmp.get('v.apiName'))) {
                    flagged = h.removeFieldFromFlagged(cmp, evt, cmp.get('v.apiName'), flagged);
                }
                theReview.Flagged_Execution_Questions__c = flagged;
                break;
            case 'Client':
                var flagged = theReview.Flagged_Client_Questions__c;
                if(flagged == null){
                    flagged = '';
                }
                if(clicked && !flagged.includes(cmp.get('v.apiName'))){
                    flagged += (cmp.get('v.apiName')) + ';';
                } else if(!clicked && flagged.includes(cmp.get('v.apiName'))) {
                    flagged = h.removeFieldFromFlagged(cmp, evt, cmp.get('v.apiName'), flagged);
                }
                theReview.Flagged_Client_Questions__c = flagged;
                break;
            case 'Geography':
                var flagged = theReview.Flagged_Geography_Questions__c;
                if(flagged == null){
                    flagged = '';
                }
                if(clicked && !flagged.includes(cmp.get('v.apiName'))){
                    flagged += (cmp.get('v.apiName')) + ';';
                } else if(!clicked && flagged.includes(cmp.get('v.apiName'))) {
                    flagged = h.removeFieldFromFlagged(cmp, evt, cmp.get('v.apiName'), flagged);
                }
                theReview.Flagged_Geography_Questions__c = flagged;
                break;
            case 'Commercial':
                var flagged = theReview.Flagged_Commercial_Questions__c;
                if(flagged == null){
                    flagged = '';
                }
                console.log('flaggedQuesitons: ' + flagged);
                if(clicked && !flagged.includes(cmp.get('v.apiName'))){
                    flagged += (cmp.get('v.apiName')) + ';';
                } else if(!clicked && flagged.includes(cmp.get('v.apiName'))) {
                    flagged = h.removeFieldFromFlagged(cmp, evt, cmp.get('v.apiName'), flagged);
                }
                theReview.Flagged_Commercial_Questions__c = flagged;
                break;
            case 'Contractual':
                var flagged = theReview.Flagged_Contractual_Questions__c;
                if(flagged == null){
                    flagged = '';
                }
                if(clicked && !flagged.includes(cmp.get('v.apiName'))){
                    flagged += (cmp.get('v.apiName')) + ';';
                } else if(!clicked && flagged.includes(cmp.get('v.apiName'))) {
                    flagged = h.removeFieldFromFlagged(cmp, evt, cmp.get('v.apiName'), flagged);
                }
                theReview.Flagged_Contractual_Questions__c = flagged;
                break;
        }
        console.log('theReview after: ', theReview);
        cmp.set('v.theReview', theReview);
    }
})