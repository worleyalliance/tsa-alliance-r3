({
	removeFieldFromFlagged : function(cmp, evt, apiName, flagged) {
		var fieldIndex = flagged.indexOf(apiName);
        var newFlagged = flagged.substr(0, fieldIndex);
        flagged = flagged.substr(fieldIndex);
        fieldIndex = flagged.indexOf(';');
        newFlagged += flagged.substr(fieldIndex + 1);
        return newFlagged;
	}
})