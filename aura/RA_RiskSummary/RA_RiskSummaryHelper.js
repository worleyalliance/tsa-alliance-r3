({
	doSaveClose : function(cmp, evt, h){
        var theSpinner = cmp.find('theSpinner');
        $A.util.toggleClass(theSpinner, 'slds-hide');
        var theReview = cmp.get('v.theReview');
        console.log('theReview', theReview);
        var saveAction = cmp.get('c.saveReviewRec')
        saveAction.setParams({
            reviewRecord: theReview
        });
        saveAction.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                window.setTimeout(
                    $A.getCallback(function() {
                        location.reload();
                    }), 5000
                );
                //$A.get('e.force:closeQuickAction').fire();
            } else if(response.getState() === 'ERROR'){
                console.log('error: ', response.getError());
            }
        });
        $A.enqueueAction(saveAction);
    }
})