({ //Mocked up data
    init: function (cmp) {
        var columns = [
            {
                type: 'text',
                fieldName: 'riskScore',
                label: 'Risk Assessment',
                initialWidth: 300
            },
            {
                type: 'text',
                fieldName: 'response',
                label: 'Responses'
            }
        ];

        cmp.set('v.gridColumns', columns);

        // data
        var nestedData = [
            {
                "riskScore": "R4 - Standard",
                "response": "",
                "_children": [
                    {
                        "riskScore": "Total Contract Value (Revenue)",
                        "response": "$5,000,000"
                    },
                    {
                        "riskScore": "Contract Type",
                        "response": "Lump Sum"
                    },

                    {
                        "riskScore": "Scope of Services",
                        "response": "Architecture Only"
                    }
                ]
            }
        ];

        cmp.set('v.gridData', nestedData);


        var expandedRows = ["R4 - Standard"];

        cmp.set('v.gridExpandedRows', expandedRows);
        
        $A.get("e.c:RA_RecalcPercentComplete").fire();
    },
    redirectOpp : function(cmp, evt, h){
        window.scrollTo(cmp.find('opportunity')[0]);
    },
    doSaveClose : function(cmp, evt, h){
        var theReview = cmp.get('v.theReview');
        console.log('theReview', theReview);
        var saveAction = cmp.get('c.saveReviewRec')
        saveAction.setParams({
            reviewRecord: theReview
        });
        saveAction.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                $A.get('e.force:closeQuickAction').fire();
            } else if(response.getState() === 'ERROR'){
                console.log('error: ', response.getError());
            }
        });
        $A.enqueueAction(saveAction);
    },
    handleNavEvent : function(cmp, evt, h){
        var evtStep = evt.getParam('step');
        var evtAction = evt.getParam('actionType');
        if(evtStep == "Step 800" && evtAction == "saveClose"){
            h.doSaveClose(cmp, evt, h);
        }
    }
});