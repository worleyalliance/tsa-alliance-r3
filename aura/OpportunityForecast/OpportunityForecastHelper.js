/**************************************************************************************
 Name: aura.OpportunityForecast
 Version: 1.0 
 Created Date: 14.04.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   14.04.2017      Original Version
 * Chris Cornejo     9/5/2018        US2397 - Allow REV / GM / Hrs On Oppty Forecast
 * Pradeep Shetty    09/11/2018      US2406: Adding Front Load and Back load option for spreading formula
                                     Removing Standard Deviation.
* Pradeep Shetty     09/20/2018      DE701: Changed Quarterly/Monthly button group to a Lightning:radioGroup.
                                           toggleTimePeriod function not required anymore.                                      
 *************************************************************************************/
({
    loadSpreadingFormulas: function(cmp, callback) {
        var action = cmp.get("c.getSpreadingFormulas");
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var spreadingFormulas = response.getReturnValue();
                    callback(spreadingFormulas);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    //US2406: Standard deviation is not used anymore
    // loadStandardDeviations: function(cmp, callback) {
    //     var action = cmp.get("c.getStandardDeviations");
    //     action.setAbortable();
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         if (cmp.isValid()) {
    //             if (state === "SUCCESS") {
    //                 var standardDeviations = response.getReturnValue();
    //                 callback(standardDeviations);
    //             } else if(state == "ERROR"){
    //                 var errors = response.getError();
    //                 this.handleError(errors);
    //             }
    //         }
    //     });
    //     $A.enqueueAction(action);
    // },
    loadForecasts: function (cmp, recordId, helper, callback) {
        helper.showSpinner(cmp.getSuper());
        var action = cmp.get("c.retrieveForecasts");
        action.setParams(
            {
                opportunityId : recordId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var forecasts = response.getReturnValue(),
                        fiscalYears = [],
                        j=0, i=0;
                    for(j=0; j<forecasts.length; j++){
                        forecasts[j].fiscalYearMatrix = {};
                        for(i=0; i<forecasts[j].fiscalYears.length; i++){
                            if(!forecasts[j].fiscalYears[i].q1Revenue){
                                forecasts[j].fiscalYears[i].q1Revenue = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q2Revenue){
                                forecasts[j].fiscalYears[i].q2Revenue = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q3Revenue){
                                forecasts[j].fiscalYears[i].q3Revenue = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q4Revenue){
                                forecasts[j].fiscalYears[i].q4Revenue = 0;
                            }
                            //US2397 - added revenue and hours 
                            if(!forecasts[j].fiscalYears[i].q1Rev){
                                forecasts[j].fiscalYears[i].q1Rev = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q2Rev){
                                forecasts[j].fiscalYears[i].q2Rev = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q3Rev){
                                forecasts[j].fiscalYears[i].q3Rev = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q4Rev){
                                forecasts[j].fiscalYears[i].q4Rev = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q1Hours){
                                forecasts[j].fiscalYears[i].q1Hours = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q2Hours){
                                forecasts[j].fiscalYears[i].q2Hours = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q3Hours){
                                forecasts[j].fiscalYears[i].q3Hours = 0;
                            }
                            if(!forecasts[j].fiscalYears[i].q4Hours){
                                forecasts[j].fiscalYears[i].q4Hours = 0;
                            }//end US2397

                            fiscalYears.push(forecasts[j].fiscalYears[i].year);
                            forecasts[j].fiscalYearMatrix[forecasts[j].fiscalYears[i].year] = forecasts[j].fiscalYears[i];
                        }
                        forecasts[j].spreadingFormulaOriginal = forecasts[j].spreadingFormula;
                        forecasts[j].standardDeviationOriginal = forecasts[j].standardDeviation;
                    }

                    fiscalYears = this.keepOnlyUnique(fiscalYears);

                    fiscalYears = this.sortYears(fiscalYears);

                    forecasts = this.addMissingFiscalYearMatrix(forecasts, fiscalYears);
                    var totalRow = this.calculateTotalRow(forecasts, fiscalYears);
                    var forecastsPageReady = this.getTablePagesConfig(forecasts, fiscalYears, totalRow, 0);

                    callback(forecastsPageReady, fiscalYears, totalRow);
                } if (state === "ERROR") {
                    var errors = response.getError();
                    helper.handleError(errors);
                }
                helper.hideSpinner(cmp.getSuper());
            }

        });
        $A.enqueueAction(action);
    },
    keepOnlyUnique: function (years) {
        var onlyUnique = function (value, index, self) {
            return self.indexOf(value) === index;
        };
        years = years.filter(onlyUnique);
        return years;
    },
    sortYears: function (years) {
        years.sort(function compare(a, b) {
            if (a < b) {
                return -1;
            }
            return 1;
        });
        return years;
    },
    addMissingFiscalYearMatrix: function (forecasts, fiscalYears) {
        var j=0,
            i=0;
        for(j=0; j<forecasts.length; j++){
            for(i=0; i<fiscalYears.length; i++){
                if(!forecasts[j].fiscalYearMatrix[fiscalYears[i]]){
                    forecasts[j].fiscalYearMatrix[fiscalYears[i]] =
                    {
                        year : fiscalYears[i],
                        q1Revenue : 0,
                        q2Revenue : 0,
                        q3Revenue : 0,
                        q4Revenue : 0,
                        m1Revenue : 0,
                        m2Revenue : 0,
                        m3Revenue : 0,
                        m4Revenue : 0,
                        m5Revenue : 0,
                        m6Revenue : 0,
                        m7Revenue : 0,
                        m8Revenue : 0,
                        m9Revenue : 0,
                        m10Revenue : 0,
                        m11Revenue : 0,
                        m12Revenue : 0,
                        //US2397 - added revenue and hours
                        q1Rev : 0,
                        q2Rev : 0,
                        q3Rev : 0,
                        q4Rev : 0,
                        m1Rev : 0,
                        m2Rev : 0,
                        m3Rev : 0,
                        m4Rev : 0,
                        m5Rev : 0,
                        m6Rev : 0,
                        m7Rev : 0,
                        m8Rev : 0,
                        m9Rev : 0,
                        m10Rev : 0,
                        m11Rev : 0,
                        m12Rev : 0,
                        q1Hours : 0,
                        q2Hours : 0,
                        q3Hours : 0,
                        q4Hours : 0,
                        m1Hours : 0,
                        m2Hours : 0,
                        m3Hours : 0,
                        m4Hours : 0,
                        m5Hours : 0,
                        m6Hours : 0,
                        m7Hours : 0,
                        m8Hours : 0,
                        m9Hours : 0,
                        m10Hours : 0,
                        m11Hours : 0,
                        m12Hours : 0
                        //end US2397
                    };
                }
            }
        }

        for(j=0; j<forecasts.length; j++){
            forecasts[j].fiscalYears = [];
            for(i=0; i<fiscalYears.length; i++){
                forecasts[j].fiscalYears[i] = forecasts[j].fiscalYearMatrix[fiscalYears[i]];
            }
        }
        return forecasts;
    },
    getTablePagesConfig: function (forecasts, fiscalYears, totalRow, pageNb) {
        var j=0;
        var i=0;    
        var pageSize;

        if ( fiscalYears.length ) {
            pageSize = fiscalYears.length;
        } else {
            pageSize = 0;
        }

            /*
                Note the below line will 'turn on' pagination again. 
                You will also need to add back some nav controls in the component, example:

                     <div class="slds-col">
                        <lightning:buttonIcon iconName="utility:chevronleft"
                            variant="border-filled"
                            alternativeText="Prev"
                            iconClass="dark"
                            onclick="{!c.previousPage}"
                            disabled="{! v.currentPage == 0}"
                            class="chevronButton"
                        />
                        
                        <span class="slds-m-left_x-small slds-m-right_x-small">
                            {!$Label.c.ViewYears}
                        </span>
                        
                        <lightning:buttonIcon iconName="utility:chevronright"
                            variant="border-filled"
                            alternativeText="Next"
                            iconClass="dark"
                            onclick="{!c.nextPage}"
                            disabled="{!v.currentPage >= v.pageAmount-1}"
                            class="chevronButton"
                        />
                    />
                    </div>
            */
            
        var startIndex = pageNb * pageSize + pageSize > fiscalYears.length ? fiscalYears.length - pageSize : pageNb * pageSize;
        var pageAmount = Math.ceil(fiscalYears.length / pageSize);
        var pageFiscalYears = [];
        var pageTotalRow = [];
        for(j=0; j<forecasts.length; j++){

            forecasts[j].fiscalYearsPage = [];
            for(i=startIndex; i<startIndex+pageSize; i++){
                forecasts[j].fiscalYearsPage.push(forecasts[j].fiscalYears[i]);
            }

            for ( var k = 0; k < forecasts[j].fiscalYearsPage.length; k++ ) {
                var t = forecasts[j].fiscalYearsPage[k];
                if ( !t.m1Revenue ) {
                    console.log('filling in blanks for ');
                    console.log(t);
                    t.m1Revenue = 0;
                }

                if ( !t.m2Revenue ) {
                    t.m2Revenue = 0;
                }

                if ( !t.m3Revenue ) {
                    t.m3Revenue = 0;
                }

                if ( !t.m4Revenue ) {    
                    t.m4Revenue = 0;
                }

                if ( !t.m5Revenue ) {    
                    t.m5Revenue = 0;
                }

                if ( !t.m6Revenue ) {
                    t.m6Revenue = 0;
                }

                if ( !t.m7Revenue ) {    
                    t.m7Revenue = 0;
                }

                if ( !t.m8Revenue ) {
                    t.m8Revenue = 0;
                }

                if ( !t.m9Revenue ) {
                    t.m9Revenue = 0;
                }    
                 
                if ( !t.m10Revenue ) {
                    t.m10Revenue = 0;
                }

                if ( !t.m11Revenue ) {    
                    t.m11Revenue = 0;
                }
                
                if ( !t.m12Revenue ) {    
                    t.m12Revenue = 0;    

                }
                //US2397 - added revenue and hours
                if ( !t.m1Rev ) {
                    t.m1Rev = 0;
                }

                if ( !t.m2Rev ) {
                    t.m2Rev = 0;
                }

                if ( !t.m3Rev ) {
                    t.m3Rev = 0;
                }

                if ( !t.m4Rev ) {    
                    t.m4Rev = 0;
                }

                if ( !t.m5Rev ) {    
                    t.m5Rev = 0;
                }

                if ( !t.m6Rev ) {
                    t.m6Rev = 0;
                }

                if ( !t.m7Rev ) {    
                    t.m7Rev = 0;
                }

                if ( !t.m8Rev ) {
                    t.m8Rev = 0;
                }

                if ( !t.m9Rev ) {
                    t.m9Rev = 0;
                }    
                 
                if ( !t.m10Rev ) {
                    t.m10Rev = 0;
                }

                if ( !t.m11Rev ) {    
                    t.m11Rev = 0;
                }
                
                if ( !t.m12Rev ) {    
                    t.m12Rev = 0;    

                }
                if ( !t.m1Hours ) {
                    t.m1Hours = 0;
                }

                if ( !t.m2Hours ) {
                    t.m2Hours = 0;
                }

                if ( !t.m3Hours ) {
                    t.m3Hours = 0;
                }

                if ( !t.m4Hours ) {    
                    t.m4Hours = 0;
                }

                if ( !t.m5Hours ) {    
                    t.m5Hours = 0;
                }

                if ( !t.m6Hours ) {
                    t.m6Hours = 0;
                }

                if ( !t.m7Hours ) {    
                    t.m7Hours = 0;
                }

                if ( !t.m8Hours ) {
                    t.m8Hours = 0;
                }

                if ( !t.m9Hours ) {
                    t.m9Hours = 0;
                }    
                 
                if ( !t.m10Hours ) {
                    t.m10Hours = 0;
                }

                if ( !t.m11Hours ) {    
                    t.m11Hours = 0;
                }
                
                if ( !t.m12Hours ) {    
                    t.m12Hours = 0;    

                }//end US2397

            }

        }
        
        for(i=startIndex; i<startIndex+pageSize; i++) {
            if ( fiscalYears[i] ) {
                var shortYear = fiscalYears[i].toString();
                shortYear = shortYear.slice(5);
                pageFiscalYears.push(shortYear);
            }
            pageTotalRow.push(totalRow[i]);
        }
        return {
            forecasts       : forecasts,
            pageFiscalYears : pageFiscalYears,
            pageNb          : pageNb,
            pageAmount      : pageAmount,
            pageTotalRow    : pageTotalRow
        };
    },
    calculateTotalRow : function (forecasts, fiscalYears) {
        var entry = {},
            h=0,
            j=0,
            i=0,
            monthTotal = [],
            name = '',
            totalRow = [];
        for(i=0; i<fiscalYears.length; i++){
            totalRow.push({
                q1Revenue : 0,
                q2Revenue : 0,
                q3Revenue : 0,
                q4Revenue : 0,
                m1Revenue : 0,
                m2Revenue : 0,
                m3Revenue : 0,
                m4Revenue : 0,
                m5Revenue : 0,
                m6Revenue : 0,
                m7Revenue : 0,
                m8Revenue : 0,
                m9Revenue : 0,
                m10Revenue : 0,
                m11Revenue : 0,
                m12Revenue : 0,
                //US2397 - added revenue and hours
                q1Rev : 0,
                q2Rev : 0,
                q3Rev : 0,
                q4Rev : 0,
                m1Rev : 0,
                m2Rev : 0,
                m3Rev : 0,
                m4Rev : 0,
                m5Rev : 0,
                m6Rev : 0,
                m7Rev : 0,
                m8Rev : 0,
                m9Rev : 0,
                m10Rev : 0,
                m11Rev : 0,
                m12Rev : 0,
                q1Hours : 0,
                q2Hours : 0,
                q3Hours : 0,
                q4Hours : 0,
                m1Hours : 0,
                m2Hours : 0,
                m3Hours : 0,
                m4Hours : 0,
                m5Hours : 0,
                m6Hours : 0,
                m7Hours : 0,
                m8Hours : 0,
                m9Hours : 0,
                m10Hours : 0,
                m11Hours : 0,
                m12Hours : 0 //end US2397
            });
            for(j=0; j<forecasts.length; j++){
                totalRow[i].q1Revenue += new Number(forecasts[j].fiscalYears[i].q1Revenue);
                totalRow[i].q2Revenue += new Number(forecasts[j].fiscalYears[i].q2Revenue);
                totalRow[i].q3Revenue += new Number(forecasts[j].fiscalYears[i].q3Revenue);
                totalRow[i].q4Revenue += new Number(forecasts[j].fiscalYears[i].q4Revenue);
                totalRow[i].m1Revenue += new Number(forecasts[j].fiscalYears[i].m1Revenue);
                totalRow[i].m2Revenue += new Number(forecasts[j].fiscalYears[i].m2Revenue);
                totalRow[i].m3Revenue += new Number(forecasts[j].fiscalYears[i].m3Revenue);
                totalRow[i].m4Revenue += new Number(forecasts[j].fiscalYears[i].m4Revenue);
                totalRow[i].m5Revenue += new Number(forecasts[j].fiscalYears[i].m5Revenue);
                totalRow[i].m6Revenue += new Number(forecasts[j].fiscalYears[i].m6Revenue);
                totalRow[i].m7Revenue += new Number(forecasts[j].fiscalYears[i].m7Revenue);
                totalRow[i].m8Revenue += new Number(forecasts[j].fiscalYears[i].m8Revenue);
                totalRow[i].m9Revenue += new Number(forecasts[j].fiscalYears[i].m9Revenue);
                totalRow[i].m10Revenue += new Number(forecasts[j].fiscalYears[i].m10Revenue);
                totalRow[i].m11Revenue += new Number(forecasts[j].fiscalYears[i].m11Revenue);
                totalRow[i].m12Revenue += new Number(forecasts[j].fiscalYears[i].m12Revenue);
                //US2397 - add total revenue and hours
                totalRow[i].q1Rev += new Number(forecasts[j].fiscalYears[i].q1Rev);
                totalRow[i].q2Rev += new Number(forecasts[j].fiscalYears[i].q2Rev);
                totalRow[i].q3Rev += new Number(forecasts[j].fiscalYears[i].q3Rev);
                totalRow[i].q4Rev += new Number(forecasts[j].fiscalYears[i].q4Rev);
                totalRow[i].m1Rev += new Number(forecasts[j].fiscalYears[i].m1Rev);
                totalRow[i].m2Rev += new Number(forecasts[j].fiscalYears[i].m2Rev);
                totalRow[i].m3Rev += new Number(forecasts[j].fiscalYears[i].m3Rev);
                totalRow[i].m4Rev += new Number(forecasts[j].fiscalYears[i].m4Rev);
                totalRow[i].m5Rev += new Number(forecasts[j].fiscalYears[i].m5Rev);
                totalRow[i].m6Rev += new Number(forecasts[j].fiscalYears[i].m6Rev);
                totalRow[i].m7Rev += new Number(forecasts[j].fiscalYears[i].m7Rev);
                totalRow[i].m8Rev += new Number(forecasts[j].fiscalYears[i].m8Rev);
                totalRow[i].m9Rev += new Number(forecasts[j].fiscalYears[i].m9Rev);
                totalRow[i].m10Rev += new Number(forecasts[j].fiscalYears[i].m10Rev);
                totalRow[i].m11Rev += new Number(forecasts[j].fiscalYears[i].m11Rev);
                totalRow[i].m12Rev += new Number(forecasts[j].fiscalYears[i].m12Rev);
                totalRow[i].q1Hours += new Number(forecasts[j].fiscalYears[i].q1Hours);
                totalRow[i].q2Hours += new Number(forecasts[j].fiscalYears[i].q2Hours);
                totalRow[i].q3Hours += new Number(forecasts[j].fiscalYears[i].q3Hours);
                totalRow[i].q4Hours += new Number(forecasts[j].fiscalYears[i].q4Hours);
                totalRow[i].m1Hours += new Number(forecasts[j].fiscalYears[i].m1Hours);
                totalRow[i].m2Hours += new Number(forecasts[j].fiscalYears[i].m2Hours);
                totalRow[i].m3Hours += new Number(forecasts[j].fiscalYears[i].m3Hours);
                totalRow[i].m4Hours += new Number(forecasts[j].fiscalYears[i].m4Hours);
                totalRow[i].m5Hours += new Number(forecasts[j].fiscalYears[i].m5Hours);
                totalRow[i].m6Hours += new Number(forecasts[j].fiscalYears[i].m6Hours);
                totalRow[i].m7Hours += new Number(forecasts[j].fiscalYears[i].m7Hours);
                totalRow[i].m8Hours += new Number(forecasts[j].fiscalYears[i].m8Hours);
                totalRow[i].m9Hours += new Number(forecasts[j].fiscalYears[i].m9Hours);
                totalRow[i].m10Hours += new Number(forecasts[j].fiscalYears[i].m10Hours);
                totalRow[i].m11Hours += new Number(forecasts[j].fiscalYears[i].m11Hours);
                totalRow[i].m12Hours += new Number(forecasts[j].fiscalYears[i].m12Hours);
                //end US2397
            }
        }

        return totalRow;
    },
    discardForecastChanges: function(forecast){
        forecast.spreadingFormula = forecast.spreadingFormulaOriginal;
        forecast.standardDeviation = forecast.standardDeviationOriginal;
    },
    loadQuartersForMultiOffice: function(cmp, multiofficeId, callback) {
        var action = cmp.get("c.retrieveQuartersForMultiOffice");
        action.setParams(
            {
                multiofficeId : multiofficeId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var quarters = response.getReturnValue();
                    callback(quarters);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveQuarters: function (cmp, multiofficeId, spreadFormula, quarters, callback) {
        this.showSpinner(cmp.getSuper());
        var action = cmp.get("c.saveQuarters");
        action.setParams(
            {
                multiofficeId   : multiofficeId,
                spreadFormula   : spreadFormula,
                quartersJSON    : JSON.stringify(quarters)
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                var errors;
                if(state == "ERROR"){
                    errors = response.getError();
                    this.handleError(errors);
                }
                callback(errors);
                this.hideSpinner(cmp.getSuper());
            }
        });
        $A.enqueueAction(action);
    },
    saveSpreadingFormula: function (cmp, multiofficeId, spreadingFormula, callback) {
        this.showSpinner(cmp.getSuper());
        var action = cmp.get("c.saveSpreadingFormula");
        action.setParams(
            {
                multiofficeId       : multiofficeId,
                spreadingFormula    : spreadingFormula
                // standardDeviation   : standardDeviation
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
                callback();
                this.hideSpinner(cmp.getSuper());
            }
        });
        $A.enqueueAction(action);
    },
	getOpportunity: function(cmp, helper, callback) {
        var action = cmp.get("c.getOpportunity");
        action.setParams(
            {
                recordId : cmp.get("v.recordId")
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    cmp.set("v.parentOpp", response.getReturnValue());
                }  else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        console.log('errors',errors);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    //DE701: Commenting out the function due to new Lightning:radioGroup    
    /*toggleTimePeriod: function(cmp, event) {
        console.log("toggleTimePeriod");
        var curr = cmp.get("v.currentTimePeriodMode");
        var radioM = cmp.find('label-time-period-radio-m');        
        var radioQ = cmp.find('label-time-period-radio-q');
        
        var curr1 = cmp.get("v.viewSelection");
        console.log("!v.viewSelection " + curr1);

        if ( curr === 'Quarterly' ) {
            cmp.set("v.currentTimePeriodMode", "Monthly");
            cmp.set("v.altTimePeriodMode", "Quarterly");
            $A.util.addClass(radioQ, 'selected');
            $A.util.removeClass(radioM, 'selected');

        } else {
            cmp.set("v.currentTimePeriodMode", "Quarterly");
            cmp.set("v.altTimePeriodMode", "Monthly");
            $A.util.addClass(radioM, 'selected');
            $A.util.removeClass(radioQ, 'selected');

        }

    },*/
   
    spreadPercentChanged: function(cmp, event, sourceElQuarterName) {
        var newTotal = 0;
        var newVal = 0;
        var saveButton = cmp.find("modalSaveButton");
        var container = document.querySelector("#spread-inputs-container");
        var spreadPercentInputs = container.querySelectorAll('[data-kind="spread-percent-inputs"]');

        spreadPercentInputs.forEach(function(input) {
            newVal = input.value;
            if ( newVal.length === 0 ) {
                newVal = 0;
            } else {
                newVal = parseInt(input.value);
            }
            
            newTotal += newVal;
        });

        this.calcNewQuarterRevGmVal(cmp, event, sourceElQuarterName);

        if ( newTotal == 100 ) {
            saveButton.set("v.disabled", false);
            cmp.set("v.modalErrorMsg", "");
        } else {
            saveButton.set("v.disabled", true);
            cmp.set("v.modalErrorMsg", "The sum of spread percentage should equal 100, they cannot equal " + newTotal);
        }
    },
    /**
    *   sourceElQuarterName is a data attribute applied to the Gm and % spread input for each quarter/month
    **/
    calcNewQuarterRevGmVal: function(cmp, event, sourceElQuarterName) {
        var changed = false;
        var container = document.querySelector("#spread-inputs-container");
        var gmInputs = container.querySelectorAll('[data-kind="quarter-revenue-inputs"]');
        var probGm = cmp.get("v.manualEditedForecast.probableGM");
        var qn, newGmVal, spread, val;
        var quartersObj = cmp.get("v.quarters");
        var spreadPercentInputs = container.querySelectorAll('[data-kind="spread-percent-inputs"]');

        gmInputs.forEach( function(input) {
            qn = input.dataset.quartername;
            if ( qn === sourceElQuarterName ) {

                spreadPercentInputs.forEach( function(spreadInput) {
                    if ( spreadInput.dataset.quartername === sourceElQuarterName ) {
                        val = spreadInput.value;
                        if ( val.length === 0 ) {
                            spread = 0;
                        } else {
                            spread = parseInt(val);
                        }

                        quartersObj.forEach( function(q) {
                            if ( q.quaterName === sourceElQuarterName ) {
                                changed = true;
                                q.spreadPercent = spread;
                            }
                        });
                    }
                });

                newGmVal = spread * parseInt(probGm) / 100;
                input.value = newGmVal;

                quartersObj.forEach( function(q) {
                    if ( q.quaterName === sourceElQuarterName ) {
                        changed = true;
                        q.revenue = newGmVal;
                    }
                });

                if ( changed ) {
                    cmp.set("v.quarters", quartersObj);
                }
            }
        });
    },
    loadEditPermissions: function (cmp, onSuccess) {
        var action = cmp.get("c.hasEditPermission");
        action.setCallback(this, function(response) {
            var state = response.getState();
               if (cmp.isValid()) {
                    if (state === "SUCCESS") {
                        var hasEditPermission = response.getReturnValue();
                        onSuccess(hasEditPermission);
                    } else if(state == "ERROR"){
                        var errors = response.getError();
                        if(errors) {
                            console.log('errors',errors);
                            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                        }
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    isEditable: function (cmp, onSuccess) {
        var action = cmp.get("c.isEditable");
        action.setParams(
            {
                recordId : cmp.get("v.recordId")
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
               if (cmp.isValid()) {
                    if (state === "SUCCESS") {
                        var isEditable = response.getReturnValue();
                        onSuccess(isEditable);
                    } else if(state == "ERROR"){
                        var errors = response.getError();
                        if(errors) {
                            console.log('errors',errors);
                            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                        }
                    }
                }
        });
        $A.enqueueAction(action); 
    } 
})