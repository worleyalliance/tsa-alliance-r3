/**************************************************************************************
 Name: aura.OpportunityForecast
 Version: 1.0 
 Created Date: 14.04.2017
 Function: 

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   14.04.2017      Original Version
 * Jignesh Suvarna   03.20.2018      US1509: Data Stewards/Sales Super User/Admin users - Edit Access
 * Pradeep Shetty    09/11/2018      US2406: Adding Front Load and Back load option for spreading formula
                                    Removing Standard Deviation.
* Pradeep Shetty    09/20/2018      DE701: Changed Quarterly/Monthly button group to a Lightning:radioGroup.
                                           toggleTimePeriod function not required anymore.                                     
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        helper.loadForecasts(
            cmp,
            cmp.get("v.recordId"),
            helper,
            function (forecastsPageReady, fiscalYears, totalRow){
                cmp.set('v.fiscalYears', fiscalYears);
                cmp.set("v.totalRow", totalRow);
                cmp.set('v.pageFiscalYears', forecastsPageReady.pageFiscalYears);
                cmp.set('v.currentPage', forecastsPageReady.pageNb);
                cmp.set('v.pageAmount', forecastsPageReady.pageAmount);
                cmp.set("v.forecasts", forecastsPageReady.forecasts);
                console.log('forecasts set to ');
                console.log(forecastsPageReady.forecasts);
                cmp.set("v.pageTotalRow", forecastsPageReady.pageTotalRow);
                helper.loadSpreadingFormulas(cmp, function (spreadingFormulas) {
                    cmp.set('v.spreadingFormulas', spreadingFormulas);
                });
                //US2406: Standard deviation is not used anymore
                // helper.loadStandardDeviations(cmp, function (standardDeviations) {
                //     cmp.set('v.standardDeviations', standardDeviations);
                // });
                cmp.set('v.editedRow', -1);
                helper.loadEditPermissions(cmp, function(hasEditPermission){
                    cmp.set('v.hasEditPermission', hasEditPermission);
                });
                helper.isEditable(cmp, function(isEditable){
                    cmp.set('v.isEditable', isEditable);
                });                
            }
        );
        
        //DE270: Get Opportunity information
        helper.getOpportunity(cmp, helper);        
    },
    nextPage: function(cmp, event, helper) {
        var fiscalYears = cmp.get('v.fiscalYears'),
            forecasts = cmp.get("v.forecasts"),
            pageAmount = cmp.get("v.pageAmount"),
            currentPage = cmp.get("v.currentPage"),
            totalRow = cmp.get("v.totalRow");
        if(currentPage < pageAmount){
            currentPage++;
            var forecastsPageReady = helper.getTablePagesConfig(forecasts, fiscalYears, totalRow, currentPage);
            cmp.set('v.pageFiscalYears', forecastsPageReady.pageFiscalYears);
            cmp.set('v.currentPage', currentPage);
            cmp.set("v.forecasts", forecastsPageReady.forecasts);
            cmp.set("v.pageTotalRow", forecastsPageReady.pageTotalRow);
        }
    },
    previousPage: function(cmp, event, helper) {
        var fiscalYears = cmp.get('v.fiscalYears'),
            forecasts = cmp.get("v.forecasts"),
            currentPage = cmp.get("v.currentPage"),
            totalRow = cmp.get("v.totalRow");
        if(currentPage > 0){
            currentPage--;
            var forecastsPageReady = helper.getTablePagesConfig(forecasts, fiscalYears, totalRow, currentPage);
            cmp.set('v.pageFiscalYears', forecastsPageReady.pageFiscalYears);
            cmp.set('v.currentPage', currentPage);
            cmp.set("v.forecasts", forecastsPageReady.forecasts);
            cmp.set("v.pageTotalRow", forecastsPageReady.pageTotalRow);
        }
    },
    onDiscardChanges: function (cmp, event, helper) {
        var editedRow = cmp.get('v.editedRow'),
            forecasts = cmp.get('v.forecasts');
        if(editedRow>=0) {
            helper.discardForecastChanges(forecasts[editedRow]);
            cmp.set('v.forecasts', forecasts);
            cmp.set('v.editedRow', -1);
        }
    },
    onDiscardManualChanges: function (cmp, event, helper) {
        cmp.discardChanges();
        cmp.set('v.modalErrorMsg', '');
        cmp.set('v.editedRow', -1);
        cmp.set('v.quarters', []);
        helper.hideElement(cmp, 'manualSpreadModal');
        var saveButton = cmp.find("modalSaveButton");
        saveButton.set("v.disabled", false);
    },
    onSubmitChangesAction: function(cmp, event, helper){
        var newRevenue;
        var target = event.getSource();
        var itemIndex = target.get('v.value'),
            forecasts = cmp.get('v.forecasts');
        if(forecasts[itemIndex]){
            if(forecasts[itemIndex].spreadingFormula == 'Manual') {
                cmp.set('v.editedRow', itemIndex);
                cmp.set('v.manualEditedForecast', forecasts[itemIndex]);
                helper.loadQuartersForMultiOffice(cmp, forecasts[itemIndex].id, function (quarters) {
                    cmp.set('v.quarters', quarters);
                    console.log('Initial value of quarters from platform');
                    console.log(quarters);

                    quarters.forEach( function(q) {
                        newRevenue = Number.parseFloat(q.revenue).toFixed(2);
                        q.revenue = newRevenue;
                    });

                    cmp.set('v.quarters', quarters);
                    console.log('Shortened value of quarters from platform');
                    console.log(quarters);

                    helper.showElement(cmp, 'manualSpreadModal');
                });
            } else {
                cmp.saveSpreadingFormula(itemIndex);
            }
        }

    },
    onSaveSpreadingFormula: function(cmp, event, helper){
        var params = event.getParam('arguments');
        if (params) {
            var itemIndex = params.itemIndex,
                forecasts = cmp.get('v.forecasts');
            if(forecasts[itemIndex] ){
                helper.saveSpreadingFormula(
                        cmp, forecasts[itemIndex].id,
                        forecasts[itemIndex].spreadingFormula,
                        // forecasts[itemIndex].standardDeviation,
                        function () {
                            cmp.reload();
                            cmp.set('v.editedRow', -1);
                        }
                );
            }
        }
    },
    onSaveQuarters: function(cmp, event, helper){
        var quarters = cmp.get('v.quarters'),
            editedRow = cmp.get('v.editedRow'),
            forecasts = cmp.get('v.forecasts');
        cmp.set('v.modalErrorMsg', '');
        helper.saveQuarters(
            cmp,
            forecasts[editedRow].id,
            forecasts[editedRow].spreadingFormula,
            quarters,
            function (errors) {
                if(errors){
                    cmp.set('v.modalErrorMsg', errors[0].message);
                } else {
                    helper.hideElement(cmp, 'manualSpreadModal');
                    cmp.reload();
                    cmp.set('v.editedRow', -1);
                    cmp.set('v.quarters', []);
                }
            });

    },
    onSpreadingFormulaChange: function(cmp, event) {
        var target = event.getSource(),
            itemIndex = target.get('v.name'),
            editedRow = cmp.get('v.editedRow'),
            forecasts = cmp.get('v.forecasts');
        if(itemIndex != editedRow) {
            cmp.discardChanges();
        }
        cmp.set('v.editedRow', itemIndex);
    },
    //DE701: Commenting out the function due to new Lightning:radioGroup
    /*toggleTimePeriod: function(cmp, event, helper) {
        helper.toggleTimePeriod(cmp, event);        
    },*/
    spreadPercentChange: function(cmp, event, helper) {
        console.log('hey spreadPercentChanged');
        var target = event.currentTarget;
        var sourceElQuarterName = target.dataset.quartername;
        helper.spreadPercentChanged(cmp, event, sourceElQuarterName);        
    } 
})