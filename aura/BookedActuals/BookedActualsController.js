/**************************************************************************************
 Name: BookedActuals
 Version: 1.0 
 Created Date: 05.04.2017
 Function: BookedActuals controller

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.04.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        helper.loadBookedActualsData(cmp, cmp.get("v.recordId"), helper,
            function (result){
                cmp.set("v.bookedActuals", result);
            }
        );
    }
})