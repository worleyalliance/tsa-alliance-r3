/**************************************************************************************
 Name: aura.BookedActuals
 Version: 1.0 
 Created Date: 05.04.2017
 Function: BookedActuals hlper

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.04.2017      Original Version
 *************************************************************************************/
({
    loadBookedActualsData: function (cmp, recordId, helper, callback) {
        helper.showSpinner(cmp.getSuper());
        var action = cmp.get("c.getBookedActual");
        action.setParams(
            {
                opportunityId : recordId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var bookedActuals = response.getReturnValue();
                    callback(bookedActuals);
                } if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                        console.log('errros:', errors[0]);
                    }
                }
                helper.hideSpinner(cmp.getSuper());
            }

        });
        $A.enqueueAction(action);
    }
})