({
    doInit : function(cmp, evt, h){
        var label = cmp.get('v.label');
        console.log('label: ' + label);
        console.log('disable? ' + label.includes("Risks"));
        if(label.includes("Risks")){
            cmp.set('v.isDisabled', true);
            console.log('disabling step!');
        }
        if(cmp.get('v.value') == 'Step 100'){
			var theLI = cmp.find('theLI');
			console.log('theLI: ', theLI);
			$A.util.addClass(theLI, 'slds-is-active');
		}
    },
	selectStep : function(cmp, evt, h) {
		console.log('step selected');
		cmp.set('v.currentStep', cmp.get('v.value'));
	},
    currentStepChange : function(cmp, evt, h){
        console.log('currentStep has changed!');
		var currentStep = cmp.get('v.currentStep');
		var thisStep = cmp.get('v.value');
		console.log('currentStep: ' + currentStep);
		console.log('thisStep: ' + thisStep);

		var isActive = cmp.get('v.isActive');

		var theLI = cmp.find('theLI');
		console.log('theLI: ', theLI);
		if(theLI.length){
			theLI = theLI[0];
		}
		if(currentStep == thisStep){
			console.log('are equal!');
			$A.util.addClass(theLI, 'slds-is-active');
			cmp.set('v.isActive', true);
		} else {
			console.log('are NOT equal!');
			$A.util.removeClass(theLI, 'slds-is-active');
			cmp.set('v.isActive', true);
		}
    },
    handleOpptyError : function(cmp, evt, h){
        console.log('handling oppty error!');
        var stepName = cmp.get('v.value');
        console.log('step: ' + stepName);
        var label = cmp.get('v.label');
        var hasError = evt.getParam('hasError');
        console.log('hasError: ' + hasError);
        if(stepName == 'Step 1'){
            cmp.set('v.hasWarning', hasError);
        } else if(!label.includes('Risks')){
            cmp.set('v.isDisabled', hasError);
        }
    },
    handleCatSelect : function(cmp, evt, h){
        var thisCat = cmp.get('v.label');
        console.log('thisCat: ' + thisCat);
        thisCat = thisCat.substr(0, thisCat.indexOf(' ')).toLowerCase();
        console.log('thisCat: ' + thisCat);
        
        var targetCat = evt.getParam('categoryName');
        var disable = evt.getParam('disable');
        
        if(targetCat == thisCat){
            console.log('targetCat: ' + targetCat);
            console.log('disable: ' + disable);
            cmp.set('v.isDisabled', disable);
        }
    }
})