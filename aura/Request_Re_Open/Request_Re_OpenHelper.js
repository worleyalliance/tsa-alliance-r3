/**************************************************************************************
Name: Request_Closure
Version: 1.0 
Created Date: 21.08.2017 
Function: Quick action that will allow user to close the  the existing quick action 

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Raj Vakati      21.08.2017      Original Version
*************************************************************************************/
({
    retrieveBPRequestStatus: function (cmp, bpId, onSuccess) {
        var action = cmp.get("c.checkRequestAllowed");
        action.setParams({"bpId": cmp.get("v.recordId"),
                          "changeRequested": "ReOpen"});
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var requestAllowed = response.getReturnValue();
                onSuccess(requestAllowed);
            } else if(state == "ERROR"){
                console.log('Error');
            }
            
        });
        $A.enqueueAction(action);
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    }
})