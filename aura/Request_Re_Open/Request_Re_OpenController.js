/**************************************************************************************
Name: Request_Closure
Version: 1.0 
Created Date: 21.08.2017 
Function: Quick action that will allow user to close the  the existing quick action 

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Raj Vakati      21.08.2017      Original Version
*************************************************************************************/
({
    doInit: function (cmp, event, helper) {
        var bpRequestId = cmp.get('v.recordId');
        // callback function 
        var onBPRequestSuccess = function(requestAllowed){
            // BP Status is not closed 
            if(!requestAllowed){
                helper.showElement(cmp,'messageContent');
                helper.hideElement(cmp,'showMessage');
                helper.hideElement(cmp, 'saveButton');
            }
        }
        // Invoke helper method 
        helper.retrieveBPRequestStatus(cmp, bpRequestId, onBPRequestSuccess);
    },
    handleSave: function(component, event, helper) {
        var updateBP = component.get("c.updateBPStatus");
        updateBP.setParams({
            "bpId": component.get("v.recordId") ,
            "statusName": "Request Re-Open"
            
        });
        updateBP.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Your request has been submitted."
                });
                // Update the UI: close panel, show toast, refresh B&P page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error:",
                    "message": "Error occured during request submission. Try again. If you get error after multiple tries please contact system administrator",
                    "type": "error"
                });
                $A.get("e.force:closeQuickAction").fire();
                toastEvent.fire();
                $A.get("e.force:refreshView").fire();
                
                console.log('Problem saving Object, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to create the new contact
        $A.enqueueAction(updateBP);
    },
    doCancel : function(cmp,helper,event){
        var closeAction = $A.get("e.force:closeQuickAction");
        closeAction.fire();
    },
    
})