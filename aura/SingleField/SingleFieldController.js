/**
Version: 1.1 
Created Date: 02.16.2017
Function: Provides the ability to add a single field on a lightning page

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Stefan Abramiuk   02.16.2017      Original Version
* Pradeep Shetty	07.27.2017      DE168-Set Edit button visibility
*************************************************************************************
*/
({
    doInit: function(cmp, event, helper) {
        //helper.showSpinner(cmp);
        helper.retrieveFieldValue(
            cmp,
            helper           
        );
        helper.setEditButtonVisibility(
            cmp,
            helper
        );
        helper.hideSpinner(cmp);
    },
    onEdit: function (cmp, event, helper) {
        helper.onEditClicked(cmp);
    },
    onDiscard: function (cmp, event, helper) {
        helper.onDiscard(cmp, helper);
    },
    onUpdate: function (cmp, event, helper) {
        helper.showSpinner(cmp);
        helper.updateFieldValue(
            cmp,
            helper,
            function (){
                helper.hideSpinner(cmp);
            }
        );
    },
    toggleVisibility: function (cmp, event, helper) {
        helper.toggleVisibility(cmp, 'image');
    }
})