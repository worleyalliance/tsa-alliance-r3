/**
Version: 1.1 
Created Date: 02.16.2017
Function: Provides the ability to add a single field on a lightning page

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Stefan Abramiuk   02.16.2017      Original Version
* Pradeep Shetty	07.27.2017      DE168-Set Edit button visibility
*************************************************************************************
*/
({
    retrieveFieldValue: function(cmp, helper) {
        var action = cmp.get("c.retrieveFieldValue");
        action.setParams(
            {
                objectType : cmp.get("v.sObjectName"),
                fieldName : cmp.get("v.fieldName"),
                recordId : cmp.get("v.recordId")
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    cmp.set("v.value", response.getReturnValue());
                    cmp.set("v.originalValue", response.getReturnValue());
                } else if (state === "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        cmp.set("v.errorMsg", errors[0].message);
                        var errorMsg = cmp.find('errorMsg');
                        $A.util.removeClass(errorMsg, 'slds-hide');
                        var field = cmp.find('field');
                        $A.util.addClass(field, 'slds-hide');
                    }
                }
                //callback();
            }
        });
        $A.enqueueAction(action);
    },
    setEditButtonVisibility: function(cmp, helper) {
        var action = cmp.get("c.checkEditPermission");
        action.setParams(
            {
                objectType : cmp.get("v.sObjectName"),
                fieldName : cmp.get("v.fieldName"),
                recordId : cmp.get("v.recordId")
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    if(!response.getReturnValue()){
                        cmp.set("v.editButtonClass", 'slds-is-absolute edit-button slds-hide');
                        cmp.set("v.disabled", true);
                    }
                    /*else{
                        cmp.set("v.editButtonClass", 'slds-is-absolute edit-button slds-show');
                    }*/
                } else if (state === "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        cmp.set("v.errorMsg", errors[0].message);
                        var errorMsg = cmp.find('errorMsg');
                        $A.util.removeClass(errorMsg, 'slds-hide');
                        var field = cmp.find('field');
                        $A.util.addClass(field, 'slds-hide');
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },    
    onEditClicked: function(cmp) {
        cmp.set("v.readOnly", false);
        var editIcon = cmp.find('editIcon');
        var saveIcon = cmp.find('saveIcon');
        var discardIcon = cmp.find('discardIcon');
        $A.util.addClass(editIcon, 'slds-hide');
        $A.util.removeClass(saveIcon, 'slds-hide');
        $A.util.removeClass(discardIcon, 'slds-hide');
    },
    onChangeComplete: function (cmp) {
        cmp.set("v.readOnly", true);
        var editIcon = cmp.find('editIcon');
        var saveIcon = cmp.find('saveIcon');
        var discardIcon = cmp.find('discardIcon');
        $A.util.removeClass(editIcon, 'slds-hide');
        $A.util.addClass(saveIcon, 'slds-hide');
        $A.util.addClass(discardIcon, 'slds-hide');
    },
    onDiscard: function (cmp, helper) {
        var originalValue = cmp.get("v.originalValue");
        cmp.set("v.value", originalValue);
        helper.onChangeComplete(cmp);
    },
    updateFieldValue: function(cmp, helper, callback) {
        var action = cmp.get("c.updateFieldValue");
        action.setParams(
            {
                objectType : cmp.get("v.sObjectName"),
                fieldName : cmp.get("v.fieldName"),
                recordId : cmp.get("v.recordId"),
                value : cmp.get("v.value")
            }
        );
        action.setCallback(this, function(){
            if(cmp.isValid()) {
                var value = cmp.get("v.value");
                cmp.set("v.originalValue", value);
                helper.onChangeComplete(cmp);
                callback();
            }
        });
        $A.enqueueAction(action);
    },
    showSpinner: function(cmp) {
        cmp.set("v.spinnerClass", 'tabSpinner');
        //var spinner = cmp.find('spinner');
        //$A.util.removeClass(spinner, 'slds-hide');
    },
    hideSpinner: function(cmp) {
        cmp.set("v.spinnerClass", 'tabSpinner slds-hide');
        //var spinner = cmp.find('spinner');
        //$A.util.toggleClass(spinner, 'slds-hide');
    },
    toggleVisibility: function(cmp, id) {
        var acc = cmp.find(id);
        for(var component in acc) {
            $A.util.toggleClass(acc[component], 'slds-is-open');  
            $A.util.toggleClass(acc[component], 'slds-is-closed');  
        }
    }
})