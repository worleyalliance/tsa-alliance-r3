/**************************************************************************************
 Name: aura.OpportunityMultiOffice
 Version: 1.0 
 Created Date: 04.11.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04/11/2017      Original Version
 * Manuel Johnson	   10/09/2017		   Added validation for Hours per US1073
 * Pradeep Shetty    10/11/2017      DE270: Added method to get Opportunity details
 * Manuel Johnson	   08/03/2018		   DE613: Fix error handling
 * Pradeep Shetty    08/21/2018      US2396: Add a pop up to confirm Lead Multi Office change
 * Scott Stone		   09/07/2018		   US2398: Allow Write Ups with blank revenue or revenue < GM
 * Pradeep Shetty    12/20/2018      DE867: Adding validation that hours cannot be 0 or negative
 * Pradeep Shetty    1/14/2019       DE893: Resolve errors related workhours validation
 *************************************************************************************/
({
  doInit: function(cmp, event, helper) {
    helper.loadMultiOfficeSplit(
      cmp,
      cmp.get("v.recordId"),
      helper,
      function (multioffices){
        cmp.set("v.multioffices", multioffices);
        var multiofficesOriginal = [];
        helper.deepClone(multioffices, multiofficesOriginal);
        cmp.set("v.multiofficesOriginal", multiofficesOriginal);
        /*helper.loadPerformanceUnits(cmp, function (performanceUnits) {
            cmp.set('v.performanceUnits', performanceUnits);
        }); - US2010 changed PU to lookup*/
        helper.loadSubSectors(cmp, function (subsectors) {
            cmp.set('v.subsectors', subsectors);
        });
        helper.loadResourceTypes(cmp, function (resourceTypes) {
            cmp.set('v.resourceTypes', resourceTypes);
        });
        helper.loadResourceTypeSettings(cmp, function (resourceTypeSettings) {
            cmp.set('v.resourceTypeSettings', resourceTypeSettings);
        });
        var multiofficesTotal = helper.calculateMultiofficesTotal(multioffices);
        cmp.set('v.multiofficesTotal', multiofficesTotal);
      }
    );
    
    //DE270: Get Opportunity information
    helper.getOpportunity(cmp, helper);
    //US1509: Data Stewards/Sales Super User/Admin users - Edit Access 
    helper.loadEditPermissions(cmp, function(hasEditPermission){
                cmp.set('v.hasEditPermission', hasEditPermission);
    });
    helper.isEditable(cmp, function(isEditable){
                cmp.set('v.isEditable', isEditable);
    });        
  },
  onDiscardRowChanges: function(cmp, event, helper) {
    var editedRowIndex = cmp.get('v.editedRowIndex'),
        multiofficesOriginal = cmp.get("v.multiofficesOriginal"),
        multioffices = cmp.get("v.multioffices");

    if(editedRowIndex > -1){
      cmp.set('v.editedRowIndex', -1);
      if(!multioffices[editedRowIndex].id){
        multioffices.splice(editedRowIndex, 1);
      } else {
        helper.clone(multiofficesOriginal[editedRowIndex], multioffices[editedRowIndex]);
      }
      cmp.set("v.multioffices", multioffices);
    }
  },
  onRowEdit: function(cmp, event) {
    cmp.discardChanges();
    var params = event.getParam('arguments');
    if (params) {
      cmp.set('v.editedRowIndex', params.itemIndex);
    }
  },
  onActionChosen: function(cmp, event, helper){
    var actionName = event.getParam("actionName"),
        itemIndex = event.getParam("itemIndex");
    helper.handleActionChosen(cmp, actionName, itemIndex);
  },
  onInitiateSave: function(cmp, event, helper){
    cmp.set('v.buttonsDisabled', true);
    var params = event.getParam('arguments');
    var errorMsg = [];
    if (params) {
      var itemIndex = params.itemIndex,
          multioffices = cmp.get('v.multioffices'),
          multiofficesOriginal = cmp.get('v.multiofficesOriginal'),
          multioffice = multioffices[itemIndex],
          multiofficeOriginal = multiofficesOriginal[itemIndex],
      	  lineOfBusiness = cmp.get('v.parentOpp.Line_of_Business__c'),
          hoursRequired = false,
          revenueRequired = true,
          marginMustBeLessThanRevenue = true;

      if(cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType]){
        hoursRequired = cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType].Hours_Required__c;
      }
      else{
        hoursRequired = false;
      }
       //US2398: allow revenue to be 0 for writeUp and allow margin to be > revenue
      if(cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType]){
        revenueRequired = cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType].Revenue_Required__c;
      }
      else{
        revenueRequired = true;
      }
       
        //US2398: allow revenue to be 0 for writeUp and allow margin to be > revenue
      if(cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType]){
        marginMustBeLessThanRevenue = cmp.get('v.resourceTypeSettings')[lineOfBusiness + multioffice.resourceType].Margin_Must_Be_Less_than_Revenue__c;
      }
      else{
        marginMustBeLessThanRevenue = true;
      }
        
      if(!multioffice.performanceUnit) errorMsg.push($A.get("$Label.c.PerformanceUnit")); 
      //US2398: only require revenue if needed for LOB and Resource Type
        if(!multioffice.revenue) {
            if(revenueRequired) {
         		errorMsg.push($A.get("$Label.c.Revenue"));        
            } else {
                multioffice.revenue = 0;
            }
        }
      if(!multioffice.gm) errorMsg.push($A.get("$Label.c.GM")); 
      if(!multioffice.startDate) errorMsg.push($A.get("$Label.c.StartDate"));
      if(!multioffice.months) errorMsg.push($A.get("$Label.c.AmountMonths"));
      //DE867 & DE893: Make sure hours is greater than 0. If for a combination of LOB and Resource Type, hours is required then it has to be >0. 
      //If hours is not required then it can be 0 or blank, but cannot be negative.
      if((hoursRequired && (!multioffice.hours || multioffice.hours <=0)) || 
         (!hoursRequired && multioffice.hours<0)) {
        errorMsg.push($A.get("$Label.c.Hours"));
      }
      
      if(errorMsg !== undefined && errorMsg.length > 0){
        helper.showToast('error', 
                         $A.get("$Label.c.Error"),
                         $A.get("$Label.c.GivenFieldsAreRequired")+ ': ' + errorMsg.join(", ")
                        );
        
        cmp.set('v.buttonsDisabled', false);
        return;
      }

      var dateFormat = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;

      if(!dateFormat.test(multioffice.startDate)){
          //helper.showToast('error', $A.get("$Label.c.Error"), $A.get("$Label.c.DateFormatIsInvalid"));
          helper.showToast('error',   
                           $A.get("$Label.c.Error"), 
                           $A.get("$Label.c.DateFormatIsInvalid").replace("{0}", $A.get("$Locale.dateFormat"))
                          );
          cmp.set('v.buttonsDisabled', false);
          return;
      }

        //US2398: allow revenue to be 0 for writeUp and allow margin to be > revenue
        if(marginMustBeLessThanRevenue && multioffice.gm > multioffice.revenue){
            helper.showToast('error',   
                           $A.get("$Label.c.Error"), 
                           $A.get("$Label.c.Revenue_Must_Be_Larger_Than_GM")
                          );
          cmp.set('v.buttonsDisabled', false);
          return;
        }
        
      //US2396: Check if the Multi Office ead checkbox value was changed. 
      // If it was changed, then show the popup. Else save the record.
      if(multioffice.lead && !multiofficeOriginal.lead){
        helper.onLeadPUChangeShowDialog(cmp,event);
      }
      else{
        cmp.saveMultiOfficeRecord(itemIndex);              
      }

    }
  },
  onDelete: function(cmp, event, helper){
    cmp.set('v.buttonsDisabled', true);
    helper.showElement(cmp, 'modalSpinner');
    var itemIndex = cmp.get('v.editedRowIndex'),
        multioffices = cmp.get('v.multioffices');

    var onSuccess = function(){
        cmp.set('v.editedRowIndex', -1);
        multioffices.splice(itemIndex, 1);
        var multiofficesOriginal = cmp.get('v.multiofficesOriginal');
        multiofficesOriginal.splice(itemIndex, 1);
        cmp.set("v.multioffices", multioffices);
        cmp.set("v.multiofficesOriginal", multiofficesOriginal);
        var multiofficesTotal = helper.calculateMultiofficesTotal(multioffices);
        cmp.set('v.multiofficesTotal', multiofficesTotal);
        helper.hideElement(cmp, 'delConfirmationDialog');
        helper.hideElement(cmp, 'modalSpinner');
        helper.notifyDataChange();
        cmp.set('v.buttonsDisabled', false);
    };
    var onError = function(){
        helper.hideElement(cmp, 'delConfirmationDialog');
        helper.hideElement(cmp, 'modalSpinner');
        cmp.set('v.buttonsDisabled', false);
        cmp.set('v.editedRowIndex', -1);
    };
    helper.deleteMultiOffice(cmp,
                             multioffices[itemIndex].id,
                             onSuccess,
                             onError
                            );
  },
  onDeleteDiscard: function(cmp, event, helper){
    helper.hideElement(cmp, 'delConfirmationDialog');
    cmp.set('v.editedRowIndex', -1);
  },
  onAddNew: function(cmp, event, helper){
    cmp.discardChanges();
    var multioffices = cmp.get('v.multioffices');
    multioffices.push({});
    cmp.set('v.editedRowIndex', multioffices.length-1);
    cmp.set('v.multioffices', multioffices);
  },
  recordEditSuccess: function(cmp,event,helper){
    component.set("v.mode", "view");
  },
  /* US2396: Separated out the Save Method since this can either be called by the Row Save button or pop up Save button
     This function calls the helper method to make server call to save the record*/
  onRecordSave: function(cmp, event,helper){
    var params = event.getParam('arguments');
    if (params) {
      var recordId = cmp.get('v.recordId'),
          itemIndex = params.itemIndex,
          multioffices = cmp.get('v.multioffices'),
          multiofficesOriginal = cmp.get('v.multiofficesOriginal'),
          multioffice = multioffices[itemIndex],
          multiofficeOriginal = multiofficesOriginal[itemIndex],
          multiofficesTotal = helper.calculateMultiofficesTotal(multioffices);

      var shouldRecalculateSpread = multioffice.id == null ||
                                    multioffice.gm != multiofficeOriginal.gm ||
                                    multioffice.startDate != multiofficeOriginal.startDate ||
                                    multioffice.months != multiofficeOriginal.months;

      var onSuccess = function(){
        helper.refreshView();
        helper.loadMultiOfficeSplit(
          cmp,
          cmp.get("v.recordId"),
          helper,
          function (multioffices){
            cmp.set("v.multioffices", multioffices);
            var multiofficesOriginal = [];
            helper.deepClone(multioffices, multiofficesOriginal);
            cmp.set("v.multiofficesOriginal", multiofficesOriginal);

            var multiofficesTotal = helper.calculateMultiofficesTotal(multioffices);
            cmp.set('v.multiofficesTotal', multiofficesTotal);
            cmp.set('v.buttonsDisabled', false);
          }
        );

        helper.notifyDataChange();
        cmp.set('v.editedRowIndex', -1);
      };

      var onError = function () {
        cmp.set('v.buttonsDisabled', false);
      };

      //Call helper method to make server call
      helper.saveMultiOffice(cmp, recordId, multioffice, multiofficesTotal, shouldRecalculateSpread, onSuccess, onError);                  
    }
  },
  /*US2396: Added cancel function for Lead PU Change Cancellation*/
  onLeadPUChangeCancel: function(cmp, event, helper){
    helper.hideElement(cmp, 'leadPUChangeConfirmationDialog');
  },
  /*US2396: Added confirm function for Lead PU Change Confirmation*/
  onLeadPUChangeConfirmation: function(cmp, event, helper){
    cmp.saveMultiOfficeRecord(cmp.get('v.editedRowIndex'));
    helper.hideElement(cmp, 'leadPUChangeConfirmationDialog');
  }    
})