/**************************************************************************************
 Name: aura.OpportunityMultiOfficeHelper
 Version: 1.0 
 Created Date: 11.04.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04/11/2017      Original Version
 * Pradeep Shetty    10/11/2017      DE270: Added method to get Opportunity details
 * Jignesh Suvarna   23/04/2018      US1509: Added loadEditPermissions() to check Admin permissions
 * Pradeep Shetty    08/21/2018      US2396: Add a pop up to confirm Lead Multi Office change
 * Madhu Shetty      10/08/2018      US2565: Updated Avg GM to have value as 100, when GM > Revenue
 *************************************************************************************/
({
  loadMultiOfficeSplit: function (cmp, recordId, helper, callback) {
    helper.showSpinner(cmp.getSuper());
    var action = cmp.get("c.getMultiOffices");
    action.setParams(
        {
            opportunityId : recordId
        }
    );
    action.setAbortable();
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          var estimation = response.getReturnValue();
          callback(estimation);
        } 
        if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
              this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
        helper.hideSpinner(cmp.getSuper());
      }

    });
    $A.enqueueAction(action);
  },
    /*loadPerformanceUnits: function(cmp, callback) {
        var action = cmp.get("c.getPerformanceUnits");
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var performanceUnits = response.getReturnValue();
                    callback(performanceUnits);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        console.log('errors',errors);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },  - US2010 changed PU to lookup*/
  loadSubSectors: function(cmp, callback) {
    var action = cmp.get("c.getSubSectors");
    action.setAbortable();
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          var resourceTypes = response.getReturnValue();
          callback(resourceTypes);
        } 
        else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            console.log('errors',errors);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },
  loadResourceTypes: function(cmp, callback) {
    var action = cmp.get("c.getResourceTypes");
    action.setAbortable();
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          var resourceTypes = response.getReturnValue();
          callback(resourceTypes);
        } 
        else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            console.log('errors',errors);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },
  loadResourceTypeSettings: function(cmp, callback) {
    var action = cmp.get("c.getResourceTypeSettings");
    action.setAbortable();
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          var resourceTypeSettings = response.getReturnValue();
          callback(resourceTypeSettings);
        } 
        else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            console.log('errors',errors);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },
  handleActionChosen : function (cmp, actionName, itemIndex) {
    if(actionName == 'edit') {
      cmp.setEditedRow(itemIndex);
    } else if(actionName == 'discard') {
      cmp.discardChanges();
    } else if(actionName == 'save') {
      cmp.initiateMultiOfficeSave(itemIndex);
    } else if(actionName == 'delete') {
      this.showElement(cmp, 'delConfirmationDialog');
      cmp.set('v.editedRowIndex', itemIndex);
    }
  },
  saveMultiOffice: function(cmp, opportunityId, multioffice, multiofficesTotal, shouldRecalculateSpread, onSuccess, onError) {
    this.showSpinner(cmp.getSuper());
    var action = cmp.get("c.saveMultiOffice");
    var opportunityRecord = {
      id : opportunityId,
      targetProjectStartDate : multiofficesTotal.startDate
    };
    action.setParams(
      {
        opportunityJSON : JSON.stringify(opportunityRecord),
        multiOfficeJSON : JSON.stringify(multioffice),
        shouldRecalculateSpread : shouldRecalculateSpread
      }
    );
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          multioffice.id = response.getReturnValue();
          onSuccess(multioffice);
        } 
        else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            if(console) {
              console.log('errors', errors);
            }
            onError(errors[0].message);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
        this.hideSpinner(cmp.getSuper());
      }
    });
    $A.enqueueAction(action);
  },
  deleteMultiOffice: function(cmp, multiofficeId, onSuccess, onError) {
    this.showSpinner(cmp.getSuper());
    var action = cmp.get("c.deleteMultiOffice");
    action.setParams(
      {
        multiofficeId   : multiofficeId
      }
    );
    action.setCallback(this, function(response) {
      if (cmp.isValid() && response) {
        var state = response.getState();
        if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            if(console){
              console.log('errors',errors);
            }
            onError();
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        } 
        else {
          onSuccess();
        }
        this.hideSpinner(cmp.getSuper());
      }
    });

    $A.enqueueAction(action);
  },
  calculateMultiofficesTotal: function(multioffices){
    var multiofficesTotal =
      {
           revenue    : 0,
           gm         : 0,
           avgGM      : 0,
           hours      : 0,
           months     : 0,
           days       : 0,
           startDate  :null,
           endDate    :null
      };

    var startDate = null,
        endDate = null,
        startDateIndex = 0,
        endDateIndex = 0;
    for(var i=0; i<multioffices.length; i++){
      var currentStartDate = new Date(multioffices[i].startDate);
      currentStartDate = new Date(currentStartDate.valueOf() + currentStartDate.getTimezoneOffset() * 60000);
      if(!startDate || currentStartDate < startDate){
        startDate = currentStartDate;
        startDateIndex = i;
      }

      var currentEndDate = new Date(multioffices[i].endDate);
      currentEndDate = new Date(currentEndDate.valueOf() + currentEndDate.getTimezoneOffset() * 60000);
      if(!endDate || currentEndDate > endDate){
        endDate = currentEndDate;
        endDateIndex = i;
      }

      multiofficesTotal.revenue  += multioffices[i].revenue ? multioffices[i].revenue : 0;
      multiofficesTotal.gm       += multioffices[i].gm ? multioffices[i].gm : 0;
      multiofficesTotal.avgGM    += multioffices[i].avgGM ? multioffices[i].avgGM : 0;
      multiofficesTotal.hours    += multioffices[i].hours ? multioffices[i].hours : 0;
    }

    if(multioffices.length > 0 ){
      multiofficesTotal.startDate = multioffices[startDateIndex].startDate;
      multiofficesTotal.endDate = multioffices[endDateIndex].endDate;
      multiofficesTotal.months = this.calculateNumberOfMonths(multiofficesTotal.startDate, multiofficesTotal.endDate);
      if(multiofficesTotal.revenue===0){
        multiofficesTotal.avgGM = 0;
      }
      else{
          if(multiofficesTotal.gm > multiofficesTotal.revenue){
              multiofficesTotal.avgGM = 100;
          }
          else{
              multiofficesTotal.avgGM = multiofficesTotal.gm / multiofficesTotal.revenue *100;                 
          }
      }
    }
    return multiofficesTotal;
  },
  calculateNumberOfMonths: function(startDate, endDate){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var dateFrom = new Date(startDate);
    var dateTo = new Date(endDate);
    var numberOfMonths = Math.round((Math.round(Math.abs((dateTo.getTime() - dateFrom.getTime())/(oneDay))))/30.5);
    return numberOfMonths;
  },
  notifyDataChange: function () {
    var appEvent = $A.get("e.c:OpportunityMultiOfficeChangeEvent");
    appEvent.fire();
  },
  getOpportunity: function(cmp, helper, callback) {
    var action = cmp.get("c.getOpportunity");
    action.setParams(
      {
          recordId : cmp.get("v.recordId")
      }
    );
    action.setAbortable();
    action.setCallback(this, function(response){
      var state = response.getState();
      if(cmp.isValid()){
        if (state === "SUCCESS"){
          cmp.set("v.parentOpp", response.getReturnValue());
        }  
        else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            console.log('errors',errors);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
      }
    });

    $A.enqueueAction(action);
  },
  loadEditPermissions: function (cmp, onSuccess) {
    var action = cmp.get("c.hasEditPermission");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (cmp.isValid()) {
        if (state === "SUCCESS") {
          var hasEditPermission = response.getReturnValue();
          onSuccess(hasEditPermission);
        } else if(state == "ERROR"){
          var errors = response.getError();
          if(errors) {
            console.log('errors',errors);
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
          }
        }
      }
    });

    $A.enqueueAction(action); 
  },
  isEditable: function (cmp, onSuccess) {
    var action = cmp.get("c.isEditable");
    action.setParams(
      {
        recordId : cmp.get("v.recordId")
      }
    );
    action.setCallback(this, function(response) {
      var state = response.getState();
        if (cmp.isValid()) {
          if (state === "SUCCESS") {
            var isEditable = response.getReturnValue();
            onSuccess(isEditable);
          } 
          else if(state == "ERROR"){
            var errors = response.getError();
            if(errors) {
              console.log('errors',errors);
              this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
            }
          }
        }
    });

    $A.enqueueAction(action); 
  },
  /*US2396: Added show dialog function to show the pop up when an MOS is made a Lead*/
  onLeadPUChangeShowDialog: function(cmp, event){
    this.showElement(cmp, 'leadPUChangeConfirmationDialog');
  }
})