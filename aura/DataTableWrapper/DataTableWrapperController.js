/**************************************************************************************
 Name: aura.DataTableWrapper
 Version: 1.0 
 Created Date: 04.05.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04.05.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper){
        if(! cmp.get('v.dataLoaded')){
            helper.showSpinner(cmp.getSuper());
        }
    },
    onDataLoadedChange: function(cmp, event, helper){
        if(event.getParam('value')){
            helper.hideSpinner(cmp.getSuper());
        } else {
            helper.showSpinner(cmp.getSuper());
        }
    }
})