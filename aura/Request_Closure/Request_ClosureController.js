/**************************************************************************************
Name: Request_Closure
Version: 1.0 
Created Date: 21.08.2017 
Function: Quick action that will allow user to close the  the existing quick action 

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Raj Vakati      21.08.2017      Original Version
*************************************************************************************/
({
    doInit: function (cmp, event, helper) {
        var bpRequestId = cmp.get('v.recordId');
        // callback function 
        var onBPRequestSuccess = function(requestAllowed){
            // BP Status is closed 
            if(!requestAllowed){
                helper.showElement(cmp,'messageContent');
                helper.hideElement(cmp,'showMessage');
                helper.hideElement(cmp, 'saveButton');
             //   cmp.set('v.buttonsDisabled' , true);
            }
        }
        // Invoke helper method 
        helper.retrieveBPRequestStatus(cmp, bpRequestId, onBPRequestSuccess);
    },
    handleSave: function(component, event, helper) {
        var updateBP = component.get("c.updateBPStatus");
        updateBP.setParams({
            "bpId": component.get("v.recordId") ,
            "statusName": "Request Closure"
            
        });
        updateBP.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Your request has been submitted."
                });
                // Update the UI: close panel, show toast, refresh account page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                //[JS 25.05.2018 ] showing specific error messages on the UI
                let errors = response.getError();
                let message = 'Error closing the B&P. Please contact System Administrator'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message += ':' + errors[0].message;
                }
                // Display the message
                console.error(message); 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error:",
                    "message": message,
                    "type": "error"
                });
                $A.get("e.force:closeQuickAction").fire();
                toastEvent.fire();
                $A.get("e.force:refreshView").fire();
                
                console.log('Problem saving Object, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to create the new contact
        $A.enqueueAction(updateBP);
    },
    doCancel : function(cmp,helper,event){
        var closeAction = $A.get("e.force:closeQuickAction");
        closeAction.fire();
    },
    
})