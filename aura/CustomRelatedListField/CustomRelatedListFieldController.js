/**************************************************************************************
 Name: CustomRelatedListField
 Version: 1.0 
 Created Date: 15.03.2017
 Function: Controller for CustomRelatedListField

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   15.03.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp) {
        var record = cmp.get("v.record");
        var fieldName = cmp.get("v.fieldName");
        if(record) {
            cmp.set('v.value', record[fieldName]);
        }
    },
    onChange: function (cmp) {
        var record = cmp.get("v.record");
        var fieldName = cmp.get("v.fieldName");

        record[fieldName] = cmp.get('v.value');
        cmp.set("v.record", record);


        var valueChangedEvent = cmp.getEvent("itemValueChange");
        valueChangedEvent.setParams({
            fieldName : cmp.get("v.fieldName"),
            value     : cmp.get('v.value'),
            itemIndex : cmp.get('v.index')
        });
        valueChangedEvent.fire();
    }
})