({
 
   /* On the component Load this function call the apex class method, 
    * which is return the list of RecordTypes of object 
    * and set it to the lstOfRecordType attribute to display record Type values
    * on ui:inputSelect component. */
 
   fetchListOfRecordTypes: function(component, event, helper) {
       
       
      var action = component.get("c.fetchRecordTypeValues");
       
       var selectedAccount =component.get("v.recordId")
       var selectedObject = component.get("v.sObjectName")
               action.setParams({
            "id" : selectedAccount,
                   "objName" : selectedObject
        });
       console.log(selectedAccount);	
       console.log(selectedObject);

      action.setCallback(this, function(response) {
          var state = response.getState();
            
            if(component.isValid() && state === 'SUCCESS'){
         component.set("v.lstOfRecordType", response.getReturnValue());
                }
            else {   console.log(response.getError());   }
      
      });
      $A.enqueueAction(action);
       

    //var action = component.get("c.getpickval");
    //var inputsel = component.find("InputSelectDynamic");
    //var opts=[];
    //console.log('yay');
    //action.setCallback(this, function(a) {
    //    for(var i=0;i< a.getReturnValue().length;i++){
    //        opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
    //    }
    //    inputsel.set("v.options", opts);

 //   });
 //   $A.enqueueAction(action); 
//erroring out on this piece of code...component options
  /*  var action = component.get("c.fetchRecordTypeValues");
     var inputsel = component.find("mySelect");
    var opts=[];
    action.setCallback(this, function(a) {
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
        }
        component.set("v.options", opts);

    });
    $A.enqueueAction(action); 
*/
       //working ok
       //var opts = [
       //     { value: "Banana", label: "Banana" },
       //     { value: "Orange", label: "Orange" },
       //     { value: "Apple", label: "Apple" },
       //   { value: "Mango", label: "Mango" }
        // ];
     //component.set("v.options", opts);
   },
 
   /* In this "createRecord" function, first we have call apex class method 
    * and pass the selected RecordType values[label] and this "getRecTypeId"
    * apex method return the selected recordType ID.
    * When RecordType ID comes, we have call  "e.force:createRecord"
    * event and pass object API Name and 
    * set the record type ID in recordTypeId parameter. and fire this event
    * if response state is not equal = "SUCCESS" then display message on various situations.
    */
   createRecord: function(component, event, helper) {
      component.set("v.isOpen", false);
 
      var action = component.get("c.getRecTypeId");
      var recordTypeLabel = component.find("selectid").get("v.value");
      var selectedAccount =component.get("v.recordId");
      var selectedObject = component.get("v.sObjectName");
      
      action.setParams({
         "recordTypeLabel": recordTypeLabel,
          "id" : selectedAccount,
      });
       console.log('id of project = ' + selectedAccount);
      action.setCallback(this, function(response) {
         var state = response.getState();
         if (state === "SUCCESS") {
            var createRecordEvent = $A.get("e.force:createRecord");
            var RecTypeID  = response.getReturnValue();
            console.log('selectedobject =' + selectedObject);
             if (selectedObject == 'Jacobs_Project__c'){
            createRecordEvent.setParams({
               "entityApiName": 'Library__c',
                'defaultFieldValues': {
      				//'Name': selectedAccount,
                    'lr_project__c':selectedAccount,
                    //'AT_Task_Order__c':component.get("v.recordId"),
   				},
               "recordTypeId": RecTypeID
            });
             }
             else if (selectedObject == 'AT_Group__c'){
                 console.log('Hi Chris');
                 createRecordEvent.setParams({
               "entityApiName": 'Library__c',
                'defaultFieldValues': {
      				//'Name': selectedAccount,
                    'lu_group__c': selectedAccount,
                    //'AT_Task_Order__c':component.get("v.recordId"),
   				},
               "recordTypeId": RecTypeID
            });
             }
            createRecordEvent.fire();
             
         } else if (state == "INCOMPLETE") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
               "title": "Oops!",
               "message": "No Internet Connection"
            });
            toastEvent.fire();
             
         } else if (state == "ERROR") {
            var toastEvent = $A.get("e.force:showToast");
             toastEvent.setParams({
               "title": "Error!",
               "message": "Please contact your administrator"
            });
            toastEvent.fire();
         }
      });
      $A.enqueueAction(action);
   },
 
   closeModal: function(component, event, helper) {
      // set "isOpen" attribute to false for hide/close model box 
      component.set("v.isOpen", false);
   },
   
    /*closeCreate: function(component, event, helper){
        var action = component.get("c.closeModal");
        //action.$meth$();
        $A.enqueueAction(action);
        var action1 = component.get("c.createRecord");
        //action1.$meth$();
        $A.enqueueAction(action1);
    },
 */
   openModal: function(component, event, helper) {
      // set "isOpen" attribute to true to show model box
      component.set("v.isOpen", true);
   },
   //changes filter parameters
   handleSelectChangeEvent: function(component, event, helper) {
    var items = component.get("v.items");
    items = event.getParam("values");
    component.set("v.items", items);
    }//,
    //libraryRecordTypeSelected: function(component, event, helper){
    //    console.log("yay");
    //    var res = component.find("mySelect").get("v.value");
    //    console.log(res);
    //var evt = $A.get("e.c:LibraryRecordType");
    //var evt = component.getEvent("e.c:LibraryRecordType")
	//evt.setParams({ "Pass_Result": res});
	//evt.fire();
    //}
   //doInit : function(component, event, helper) {
   // var action = component.get("c.getpickval");
   // var inputsel = component.find("InputSelectDynamic");
   // var opts=[];
   // action.setCallback(this, function(a) {
   //     for(var i=0;i< a.getReturnValue().length;i++){
   //         opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
   //     }
   //     inputsel.set("v.options", opts);
//
//    });
//    $A.enqueueAction(action); 
//}
})