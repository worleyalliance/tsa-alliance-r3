/**************************************************************************************
 Name: aura.JEG_BaseComponent
 Version: 1.0 
 Created Date: 05.04.2017
 Function: Component is used to gather all common functionalities like whowing/hiding element

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.04.2017      Original Version
 * Pradeep Shetty    07.07.2018      US2270: Added checkPicklistExistence method
 *************************************************************************************/
({
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent) {
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": type
            });
            toastEvent.fire();
        }
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    },
    clone: function (sourceElm, targetElm) {
        if(targetElm){
            for(var k in targetElm){
                targetElm[k]=undefined;
            }
        }
        for(var k in sourceElm){
            targetElm[k]=sourceElm[k];
        }
    },
    deepClone: function (sourceElm, targetElm) {
        var clone = JSON.parse(JSON.stringify(sourceElm));
        this.clone(clone, targetElm);
    },
    refreshView: function(){
        var refreshEvent = $A.get('e.force:refreshView');
        if(refreshEvent){
            refreshEvent.fire();
        }
    },
    handleError: function(errors){
        if(errors) {
            if(console){
                console.log('errors',errors);
            }
            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
        }
    },
    callServer : function(cmp, actionName, parameters, isAbortable, onSuccess, onError){
        var action = cmp.get(actionName);
        if(parameters){
            action.setParams(parameters);
        }
        if(isAbortable){
            action.setAbortable();
        }
        action.setCallback(this, function(response){
            if(response){
                var state = response.getState();
                if(cmp.isValid()){
                    if (state === "SUCCESS"){
                        var result = response.getReturnValue();
                        onSuccess(result);
                    } else if(state == "ERROR"){
                        var errors = response.getError();
                        if(!onError){
                            this.handleError(errors);
                        } else {
                            onError(errors);
                        }
                    }
                }
            } else {
                onSuccess();
            }
        });
        $A.enqueueAction(action);
    },
    navigateToSObject : function(recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
              "recordId": recordId
            });
            navEvt.fire();
        }
    },
    //Check if a specific picklist value exists in a list of picklist options
    checkPickListExistence: function(picklistOptions,picklistValue){
        //Loop through the list to see if the Parent Selling Unit is active
        for (var i = 0, len = picklistOptions.length; i < len; i++) {
            if (picklistOptions[i]["value"] === picklistValue){
                return i;                
            } 

        }
        return -1;
    }    
})