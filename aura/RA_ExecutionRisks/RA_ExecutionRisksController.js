({
    doInit : function(cmp, evt, h) {
        console.log('***reviewRecord: ', cmp.get('v.theReview'));
        // Create the Apex action to get Execution Risk Questions
        cmp.set('v.currentPage', 1);
        var questionAction = cmp.get('c.getQuestions');
        questionAction.setParams({
            riskType: 'Execution Risks',
            theReview: cmp.get('v.theReview')
        });
        questionAction.setCallback(this, function(response){
            // If apex call succeeds
            if(response.getState() == 'SUCCESS'){
                console.log('response: ', response.getReturnValue());
                
                var testWrapper = response.getReturnValue();
                var questionData = [];
                var fieldNames = {};
                cmp.set('v.totalPages', testWrapper.length);
                var numOfQuestions = 0;
                for(var i = 0; i < testWrapper.length; i++){
                    
                    if(!$A.util.isEmpty(testWrapper[i].q1ApiName)){
                        numOfQuestions++;
                    }
                    if(!$A.util.isEmpty(testWrapper[i].q2ApiName)){
                        numOfQuestions++;
                    }
                    
                    fieldNames[testWrapper[i].q1ApiName] = 1;
                    if(!$A.util.isEmpty(testWrapper[i].q2ApiName)){
                    	fieldNames[testWrapper[i].q2ApiName] = 1;
                    }
                    
                    var question = {'page': i + 1};
                    question.q1Fields = [testWrapper[i].q1ApiName];
                    question.q2Fields = [testWrapper[i].q2ApiName];
                    
                    question.q1Label = testWrapper[i].q1Label;
                    question.q1Help = testWrapper[i].q1Help;
                    question.q1ApiName = testWrapper[i].q1ApiName;
                    question.q1Number = testWrapper[i].q1Number;
                    question.q2Label = testWrapper[i].q2Label;
                    question.q2Help = testWrapper[i].q2Help;
                    question.q2ApiName = testWrapper[i].q2ApiName;
                    question.q2Number = testWrapper[i].q2Number;
                    question.q1Options = [];
                    question.q2Options = [];
                    
                    
                    for(var j = 0; j < testWrapper[i].q1Options.length; j++){
                        var newOption = {'label': testWrapper[i].q1Options[j], 'value': testWrapper[i].q1Options[j]};
                        question.q1Options.push(newOption);
                    }
                    if(!$A.util.isEmpty(testWrapper[i].q2Options)){
                        for(var j = 0; j < testWrapper[i].q2Options.length; j++){
                            var newOption = {'label': testWrapper[i].q2Options[j], 'value': testWrapper[i].q2Options[j]};
                            question.q2Options.push(newOption);
                        }
                    }
                    
                    
                    questionData.push(question);
                }
                console.log('fieldName: ', fieldNames);
                console.log('fieldNames:', Object.keys(fieldNames));
                cmp.set('v.fieldNames', Object.keys(fieldNames));
                cmp.set('v.numberOfQuestions', numOfQuestions);
                console.log('questionData: ', questionData);
                cmp.set('v.questionData', questionData);
                
            }
        });
        $A.enqueueAction(questionAction);
    },
    doBack : function(cmp, evt, h) {
        var currentPage = cmp.get('v.currentPage');
        if(currentPage == 1){
            cmp.set('v.currentStep', 'Step 200');
        } else {
            cmp.set('v.currentPage', currentPage - 1);
        }
    },
    doContinue : function(cmp, evt, h){
        console.log('currentPage: ' + cmp.get('v.currentPage'));
        console.log('totalPages: ' + cmp.get('v.totalPages'));
        var currentPage = cmp.get('v.currentPage');
        if(currentPage == cmp.get('v.totalPages')){ // if current page is the last page
            //  Change to add requirement validation later
            var selectedCategories = cmp.get('v.selectedCategories');
            console.log('selectedCategories')
            if(selectedCategories.client){
                cmp.set('v.currentStep', 'Step 400');
            } else if(selectedCategories.geography){
                cmp.set('v.currentStep', 'Step 500');
            } else if(selectedCategories.commercial){
                cmp.set('v.currentStep', 'Step 600');
            } else if(selectedCategories.contractual){
                cmp.set('v.currentStep', 'Step 700');
            } else {
                cmp.set('v.currentStep', 'Step 800');
            }
        } else {
            cmp.set('v.currentPage', currentPage + 1);
        }
    },
    handleNavEvent : function(cmp, evt, h){
        var evtStep = evt.getParam('step');
        var evtAction = evt.getParam('actionType');
        if(evtStep == "Step 300" && evtAction == "continue"){
            h.doContinue(cmp, evt, h);
        }
        if(evtStep == "Step 300" && evtAction == "saveClose"){
            h.doSaveClose(cmp, evt, h);
        }
        if(evtStep == "Step 300" && evtAction == "back"){
            h.doBack(cmp, evt, h);
        }
    }
})