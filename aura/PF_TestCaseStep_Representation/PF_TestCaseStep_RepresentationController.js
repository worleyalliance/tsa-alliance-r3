({
    doInit : function(component, event, helper) {
        var Id = component.get("v.recordId");
        var action = component.get("c.getTestCaseSteps"); 
        action.setParams({       
            "testCaseExecutionId": Id
        });                                                                     
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var testCaseStep = [];
                testCaseStep = response.getReturnValue();
                if(testCaseStep.length == 0){
                    component.set("v.Show_No_Item_Div" , true);
                } else if(testCaseStep.length > 1){
                    component.set("v.Next_Button_Enabler" , true);
                }
                //alert(testCaseStep[0].Id);
                //alert(document.getElementById(testCaseStep[0].Id));
                component.set("v.Test_Case_Steps" , testCaseStep);
                document.getElementById("test123").setAttribute("style", "transform:translateX(-000%)");
                //document.getElementById(""+testCaseStep[0].Id+"").setAttribute("class", "slds-carousel__indicator-action slds-is-active");
            }
        });
        $A.enqueueAction(action);
        
        var checkPageAccess = component.get("c.checkPageAccess");
        checkPageAccess.setCallback(component, function(a) {
            console.log(a.getReturnValue());                
            component.set("v.notHavingAccess", a.getReturnValue());
        });
        
        //console.log ("URL -->" + URL.getSalesforceBaseUrl().toExternalForm());
        var picklist_fields = ['PF_Severity__c','PF_Status__c','PF_Type__c','PF_Resolution_Type__c','PF_Priority__c'];
        
        //var cmp_attributes = ["v.newDefect.PF_Severity__c","v.newDefect.PF_Status__c","v.newDefect.PF_Type__c"];
        var cmp_attributes = ["v.Severity","v.Status","v.Type","v.ResType","v.Priority"];
        
        var actionPickList = component.get("c.getPicklist");
        actionPickList.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var values = response.getReturnValue();
                
                //fetch all the values of picklist fields
                for(var k=0;k < picklist_fields.length;k++){
                    var picklist_field_values = values.optionsMap[picklist_fields[k]];
                    var picklist_values=[];
                    //console.log(k);
                    //console.log(picklist_fields);
                    //console.log(picklist_field_values);
                    if(!picklist_field_values.includes("Defect")){
                        picklist_values.push({value: '', label: '--None--'});
                    }
                    //picklist_values.push({value: '', label: '--None--'});
                    for(var i=0;i< picklist_field_values.length;i++){
                        var value = picklist_field_values[i];                        
                        picklist_values.push({value: value, label: value});
                    }
                    //component.set("v.Severity", "Medium");
                    component.set(cmp_attributes[k], picklist_values);
                    //component.find(cmp_attributes[k]).set(cmp_attributes[k], picklist_values);
                }
                
            } else {
                console.log(state);
            }
        });
        checkPageAccess.setCallback(component, function(a) {             
            component.set("v.notHavingAccess", a.getReturnValue());
        });
        $A.enqueueAction(checkPageAccess);
        $A.enqueueAction(actionPickList);
        
    },
    moveLeft: function(component, event, helper) {
        var listSize = component.get("v.Test_Case_Steps").length;
        var previousIndex = parseInt(event.target.id);
        if(previousIndex == 1){
            component.set("v.Next_Button_Enabler" , true);
            component.set("v.Previous_Button_Enabler" , false);
        }
        if(previousIndex > 0 ){
            component.set("v.Next_Button_Enabler" , true);
            document.getElementById("test123").setAttribute("style", "transform:translateX(-"+(previousIndex - 1)*100+"%)");
            //document.getElementById("a0C3C000004syCSUAY").setAttribute("class", "slds-carousel__indicator-action slds-is-active");
        }   
    },
    moveRight: function(component, event, helper) {
        var listSize = component.get("v.Test_Case_Steps").length;
        var nextIndex = parseInt(event.target.id);
        if(nextIndex == (listSize-2)){
            component.set("v.Next_Button_Enabler" , false);
            component.set("v.Previous_Button_Enabler" , true);
        }
        if(nextIndex < (listSize-1)){
            component.set("v.Previous_Button_Enabler" , true);
            document.getElementById("test123").setAttribute("style", "transform:translateX(-"+(nextIndex + 1)*100+"%)");
        } 
    },
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        //component.set("v.newDefect",{'sobjectType': 'PF_Defects__c','Name': ''});
    },
    openmodal: function(component,event,helper) {
        component.set("v.newDefect",{'sobjectType': 'PF_Defects__c','Name': '', 'PF_Type__c': 'Defect', 'PF_Status__c': 'New'});
        component.set("v.TestCaseStepId" , event.getSource().get("v.value"));
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    Save : function(component, event, helper) {
        
        var btn = event.getSource();
        var theDefect = component.get("v.newDefect");
        var action = component.get("c.createDefect");
        var dname = component.get("v.newDefect.Name");   
        var dSeverity = component.get("v.newDefect.PF_Severity__c");   
        var dStatus = component.get("v.newDefect.PF_Status__c"); 
        var dType =  (component.get("v.newDefect.PF_Type__c") != "") ? component.get("v.newDefect.PF_Type__c") : 'Defect';   
        var dDesc = component.get("v.newDefect.PF_Description__c");   
        var dReproduce = component.get("v.newDefect.PF_Steps_to_Reproduce__c"); 
        var resType = component.get("v.newDefect.PF_Resolution_Type__c"); 
        var priority = component.get("v.newDefect.PF_Priority__c"); 
        var resDetails = component.get("v.newDefect.PF_Resolution_Details__c"); 
        var planFixDate =  component.get("v.newDefect.PF_Planned_Fixed_Date__c"); 
        
        if(!dname || !dSeverity || !dType || !dDesc || !dReproduce ){ //|| !dStatus 
            alert('Please populate all the Mandatory Fields !!!');
            return;
        }
        
        console.log("DStatus " + dStatus);
        
        action.setParams({                
            "theDefect": JSON.stringify(theDefect),
            "recordId": component.get("v.recordId"),
            "testCaseStepId": component.get("v.TestCaseStepId")
        });
        
        action.setCallback(component, function(a) {
            if(a.getReturnValue() === "SUCCESS"){                      
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.removeClass(cmpBack,'slds-backdrop--open');
                $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
                
                
                //alert("Defect sucessfully created");                     
                var toastEvent = $A.get("e.force:showToast");
                if(toastEvent) {
                    $A.get("e.force:closeQuickAction").fire();
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Defect \""+dname+"\" sucessfully created",
                        "duration": 2000
                    });
                    toastEvent.fire();  
                }
                
            }
            else{
                alert("error occured while creating Defect/nError Message --> " + a.getReturnValue());
            }                
        });
        
        $A.enqueueAction(action);            
        
        component.set("v.newDefect", theDefect);
        
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.showSpinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.showSpinner", false);
    }
    
})