({
    onLoad : function(component, event) {
        
        var reviewId = component.get("v.recordId");
        
        console.log('reviewId: ', reviewId);
        
        var action = component.get('c.fetchReview');
        action.setParams({
            "reviewId":reviewId
        });
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('state: ', state);
                var returnVal = response.getReturnValue();
                console.log('return: ', returnVal);
                component.set('v.rev', returnVal.theReview);
                component.set('v.fieldNames', returnVal.fieldNames);
            }
        });
        $A.enqueueAction(action);
        
    },
    convertReviewToCSV : function(component, rev){
        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;
        console.log("convert rev: ", rev);
        
        // check if rev parameter is null, then return from function
        if (rev == null) {
            console.log('rev is null');
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider variable  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key 
        // this labels use in CSV file header  
        header = ['Field Name', 'Value']
        var firstRow = ['"Id"', '"' + rev.Id + '"'];
        var fieldMaps = component.get('v.fieldNames');
        
        csvStringResult = '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;
        csvStringResult += firstRow.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i = 0; i < fieldMaps.length; i++){
            if(fieldMaps[i].Name.includes('"')){
                console.log('Found name with double quote.');
                var regex = new RegExp('"', "g");
                fieldMaps[i].Name = fieldMaps[i].Name.replace(regex, "'");
            }
            if(fieldMaps[i].value.includes('"')){
                console.log('Found value with double quote.')
                var regex = new RegExp('"', "g");
                fieldMaps[i].Name = fieldMaps[i].Name.replace(regex, "'");
            }
            csvStringResult += '"' + fieldMaps[i].Name + '"' + columnDivider + '"' + fieldMaps[i].value + '"' + lineDivider;
        }
        
        // return the CSV format String 
        return csvStringResult;        
    }
})