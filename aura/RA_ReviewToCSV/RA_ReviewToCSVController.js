({
    init : function(component, event, helper) {
        helper.onLoad(component, event);
    },
    downloadCsv : function(component,event,helper){
        
        // get the review record 
        var rev = component.get("v.rev");
        console.log("rev: ", rev);
        
        // call the helper function to return the CSV data as a String   
        var csv = helper.convertReviewToCSV(component, rev);   
        if (csv == null){return;} 
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        var date = new Date();
		var timestamp = date.getTime();
        hiddenElement.download = rev.Id + ' - ' + timestamp + ' ExportData.csv';  // CSV file Name
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    }
})