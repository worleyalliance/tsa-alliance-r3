/**************************************************************************************
 Name: ReachbackDataTable
 Version: 1.0 
 Created Date: 05.05.2017
Function: Component displays Reachback list of given Group

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.05.2017      Original Version
 *************************************************************************************/
({
    loadData : function(
        cmp,
        recordId,
        fieldSetName,
        fiscalYear,
        fiscalYearField,
        sObjectName,
        relationField,
        onSuccess
    ){
        var action = cmp.get("c.getData");
        action.setParams(
            {
                fieldSetName        : fieldSetName,
                recordId            : recordId,
                fiscalYear          : fiscalYear,
                fiscalYearField     : fiscalYearField,
                sObjectName         : sObjectName,
                relationField       : relationField
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var tableData = response.getReturnValue();
                    onSuccess(tableData);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})