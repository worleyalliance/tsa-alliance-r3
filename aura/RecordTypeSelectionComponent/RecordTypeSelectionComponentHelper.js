/**************************************************************************************
 Name: aura.RecordTypeSelectionComponent
 Version: 1.0 
 Created Date: 03/13/2018
 Function: Generic component that can be used to provide RecordType selection

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                   
 * Pradeep Shetty   03/13/2018      Original Version
 * Pradeep Shetty   08/20/2018      DE633: Added additional parameters to make this 
                                    component generic.  
 *************************************************************************************/
({
	retrieveRecordTypeValues: function (cmp, objectAPI, recordTypeNames, sortOrder, onSuccess, onFail) {
        var action = cmp.get("c.retrieveRecordTypes");
        action.setParams(
            {
                objectAPIName 		 : objectAPI,
                recordTypeNameString : recordTypeNames,
                sortOrder            : sortOrder	
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var recordTypes = response.getReturnValue();
                    onSuccess(recordTypes);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    sendMessage: function (cmp, event){
        //Get a message event
    	var sendMsgEvent = $A.get("e.ltng:sendMessage"); 
        
        //get recordTypeRows attribute
        var recordTypeRows = cmp.get("v.recordTypeRows");
		
        //Variable to store recordTypeDeveloperName
        var recordTypeDevName;
        
        //Set the recordTypeDeveloperName
        for(var i = 0; i < recordTypeRows.length; i++){
            if(recordTypeRows[i].Id === cmp.get("v.selectedRecordType")){
                recordTypeDevName = recordTypeRows[i].DeveloperName;
            }
        }
		
        //Set the message event parameters
        sendMsgEvent.setParams({
            "message": recordTypeDevName + "," + cmp.get("v.selectedRecordType"), 
            "channel": cmp.get("v.channelName")
        }); 
        sendMsgEvent.fire();
    }    
})