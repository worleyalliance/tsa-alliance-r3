/**************************************************************************************
 Name: aura.RecordTypeSelectionComponent
 Version: 1.0 
 Created Date: 03/13/2018
 Function: Generic component that can be used to provide RecordType selection

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Pradeep Shetty   03/13/2018      Original Version
 * Pradeep Shetty   08/20/2018      DE633: Added additional parameters to make this 
                                    component generic. 
 *************************************************************************************/
({
	doInit : function(cmp, event, helper) {
        
        //Show the spinner
        cmp.set("v.spinnerVisibility", true);
        
        //Get ObjectAPI name
		var objectAPI = cmp.get("v.objectAPI");
        
        //Get recordType names
		var recordTypeNames = cmp.get("v.recordTypeNames");

        //Get the Sort order
        var sortOrder = cmp.get("v.sortOrder");

        //Function to be called when  recordtypes are fetched successfully
        var onRecordTypeSuccess = function(recordTypes){
            var recordTypeList = [];
            //Set the recordTypeROws attribute. This is used to send Dev Name in goEvent.
            cmp.set("v.recordTypeRows", recordTypes);
            
            //Get the default recordtype attribute
            var defaultRecordType = cmp.get("v.defaultRecordType");
            
            //Build the recordTypeList attribute to be used for radiobutton group
            for (var i = 0; i < recordTypes.length; i++) {
                //var description = recordTypes[i].Description != null ? " ( " + recordTypes[i].Description + " )" : "";
                var label = recordTypes[i].Name + "\n\r"; //+ description;
                var recordTypeEntry = {
                    label : label,
                    value : recordTypes[i].Id
                };
                //Add the option to the list
                recordTypeList.push(recordTypeEntry);
                
                //Set default value
                if(recordTypes[i].DeveloperName === defaultRecordType){
                    //Set the defaultRecordTypeId
                    cmp.set("v.defaultRecordTypeId", recordTypes[i].Id)
                    cmp.set("v.selectedRecordType", recordTypes[i].Id);
                    
                }
			} 
            
            //Set the recordTypeList attribute         
            cmp.set("v.recordTypeList", recordTypeList);
            
            //Hide the spinner
            cmp.set("v.spinnerVisibility", false);
        };
        
        //Function to be called when the Apex call fails
        var onFail = function(errors){
            if(errors) {
                cmp.set("v.errorMessage", errors[0].message);
            }
            helper.hideElement(cmp, "modalSpinner");
        };
        
        //Call helper method to make apex call for fetching recordtypes
        helper.retrieveRecordTypeValues(cmp, objectAPI, recordTypeNames, sortOrder, onRecordTypeSuccess, onFail);         
	},
    doCancel: function(cmp, event, helper){
        //Reset the selectedRecordType to defaultRecordType
        cmp.set("v.selectedRecordType", cmp.get("v.defaultRecordTypeId"));
        //Fire CancelEvent
		helper.fireCancelEvent(cmp);
    },
    doNext: function(cmp, event, helper){
                
        //Send message to the parent component
        helper.sendMessage(cmp,event);
		       
        cmp.find("overlayLib").notifyClose();
        //Reset the selectedRecordType to defaultRecordType
        //cmp.set("v.selectedRecordType", cmp.get("v.defaultRecordTypeId"));        
    }
})