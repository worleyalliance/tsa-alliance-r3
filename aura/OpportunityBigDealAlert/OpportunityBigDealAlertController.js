/**************************************************************************************
Name: aura.OpportunityBigDealAlert
Version: 1.0 
Created Date: 05/25/2018
Function: Displays a message when a big Opportunity is booked

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Manuel Johnson   	5.25.2018      	Original Version
* Pradeep Shetty    6.10.2018       Added logic for the message display
*************************************************************************************/
({
	doInit : function(component) {
		var confettiCount = component.get("v.confettiCount");
		var confettiList = [];
        //Prepare confetti list for the aura:iteration based on the confetti count
		for(var i=1; i<=confettiCount; i++){
			confettiList.push(i);
		}
		component.set("v.confettiList",confettiList);
        console.log(component.get("v.isAppbuilderView"));
	},
	handleRecordUpdated: function(component, event, helper) {
        //This function is called when LDS loads.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           	// record is loaded
           	console.log("Record is loaded successfully.");
            //Call the save record to stop the animation automatically after a while
			helper.handleSaveRecord(component, event);
        } else if(eventParams.changeType === "ERROR") {
            console.log("Error saving record.");
        }
    },    
	closeModal : function(component,event,helper) {
		var modal = component.find('oppWinPopUp');      
    	$A.util.addClass(modal, 'slds-hide'); //Hide the modal
        //Changing the runtime value to 0 to stop the animation immediately
        component.set("v.confettiRuntime",0);
        //Call the save record function to stop the animation immediately
        helper.handleSaveRecord(component,event);  
	}
})