/**************************************************************************************
Name: aura.OpportunityBigDealAlert
Version: 1.0 
Created Date: 05/25/2018
Function: Displays a message when a big Opportunity is booked

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Manuel Johnson   	5.25.2018      	Original Version
* Pradeep Shetty    6.10.2018       Added logic for the message display
*************************************************************************************/
({
	handleSaveRecord: function(component, event) {
        //set this value to true so that the animation stops
        component.set("v.simpleOppRecord.Confetti_Dropped__c", true);
        window.setTimeout(
            $A.getCallback(function() {
                var dataService = component.find("oppRecordHandler");
                if(dataService){
                    dataService.saveRecord($A.getCallback(function(saveResult) {
                        if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                            console.log("Record saved successfully");
                        } else if (saveResult.state === "INCOMPLETE") {
                            console.log("User is offline, device doesn't support drafts.");
                        } else if (saveResult.state === "ERROR") {
                            console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                        } else {
                            console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                        }
                	})); 
                }

            }), component.get("v.confettiRuntime")*1000
        );        
    }
})