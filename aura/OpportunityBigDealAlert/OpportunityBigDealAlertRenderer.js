/**************************************************************************************
Name: aura.OpportunityBigDealAlert
Version: 1.0 
Created Date: 05/25/2018
Function: Displays a message when a big Opportunity is booked

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Manuel Johnson   	5.25.2018      	Original Version
* Pradeep Shetty    6.10.2018       Added logic for the message display
*************************************************************************************/
({
	// Renderer method override
    afterRender: function(component, helper) {
        //This is required. Never delete.
		this.superAfterRender();
		
        //Following logic to make sure that the confetti is not rendered in appbuilder view
        var elements = document.querySelectorAll("*[class^=\"cOpportunityBigDealAlert\"]");
        //console.log("render" + elements.length);
		//console.log("render" + elements[0].parentElement.nodeName);        
        //console.log("render" + elements[0].parentElement.className);
        
        if(elements[0].parentElement.className!="desktop"){
            component.set("v.inRunMode",false);
        }
        
        //Defaulting color to red
    	var color = "red";
		
        //Get all divs with classes beginning with "confetti-"
	    var confettiElements = document.querySelectorAll("*[class^=\"confetti-\"]");
        //Loop through each element and apply random values to style attributes.
        for (var i = 0; i < confettiElements.length; i++) {
			//generates a random number to decide the confetti color. If more colors are desired, 
			//add it to thhe case section and increase the number of colors value
			var num_of_colors = 7;
			var colorIdx = Math.ceil(Math.random() * num_of_colors);
			// Select random color for particle
			switch(colorIdx){
            	case 1:
			      color = "yellow";
			      break;
			    case 2:
			      color = "blue";
			      break;
			    case 3:
                  color = "green";
                  break;
			    case 4:
                  color = "cyan";
                  break;
			    case 5:
                  color = "gold";
                  break;
			    case 6:
                  color = "white";
                  break;                      
                // Make sure to style the class of your color name to set the background color
                default:
                  color = "red";
			  }
            var confettiElement = confettiElements[i];
            //Width.
            confettiElement.style.setProperty("width", Math.random() * 25 +"px");
            //Height
            confettiElement.style.setProperty("height", Math.random() * 25 *0.4 +"px");
            //Top. This value starts as negative so thatconfetti starts dropping from the top
            confettiElement.style.setProperty("top", (-Math.random()*20 - 10)+"%");
            //Left. This property spreads the confetti across the screen
            confettiElement.style.setProperty("left", Math.random()*120+"%");
            //Opacity
            confettiElement.style.setProperty("opacity", Math.random()+0.5);
            //Rotation transformation
            confettiElement.style.setProperty("transform", "rotate("+Math.random()*360+"deg)");
            //Background color based on the random color selection
            confettiElement.style.setProperty("background-color", color);
            //Random animation delay for each of the confetti element
            confettiElement.style.setProperty("animation-delay", Math.ceil(Math.random() * 15)+"s");
        }
    }
})