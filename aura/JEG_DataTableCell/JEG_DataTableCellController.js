/**************************************************************************************
 Name: aura.JEG_DataTableCell
 Version: 1.0 
 Created Date: 07.03.2017
 Function: JEG_DataTableCell component controller

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           07.03.2017         Original Version
 *************************************************************************************/
({
    doInit: function(cmp) {
        var record = cmp.get("v.record"),
            fieldName = cmp.get("v.fieldName"),
            linkFieldId = cmp.get("v.linkFieldId"),
            isLinkField = cmp.get('v.isLinkField');
        if(record) {
            cmp.set('v.value', record[fieldName]);
            if(isLinkField){
                cmp.set('v.idValue', record[linkFieldId]);
            }
        }
    }
})