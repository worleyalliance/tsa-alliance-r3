/**************************************************************************************
 Name: aura.NewOpportunityLocation
 Version: 1.0 
 Created Date: 26.04.2017
 Function: Adds new Location to Oppotunity base on Account locations

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   26.04.2017      Original Version
 *************************************************************************************/
({
    doInit: function (cmp, event, helper) {
        helper.showElement(cmp, 'modalSpinner');
        var opportunityId = cmp.get('v.recordId');

        var onLocationSuccess = function(locations){
            cmp.set('v.locations', locations);
            if(locations && locations.length>0){
                cmp.set('v.selectedLocationId', locations[0].value);
            }

            helper.hideElement(cmp, 'modalSpinner');
        };
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.errorMessage', errors[0].message);
            }
            helper.hideElement(cmp, 'modalSpinner');
        };
        helper.retrieveAccountLocations(cmp, opportunityId, onLocationSuccess, onFail);

        var onOpportunitySuccess = function(opportunityName){
            cmp.set('v.opportunityName', opportunityName);
            helper.hideElement(cmp, 'modalSpinner');
        };
        helper.retrieveOpportunityName(cmp, opportunityId, onOpportunitySuccess, onFail);
    },
    doSave: function (cmp, event, helper) {
        cmp.set('v.errorMessage', "");
        helper.showElement(cmp, 'modalSpinner');
        cmp.set('v.buttonsDisabled', true);
        debugger;
        var opportunityId = cmp.get('v.recordId'),
            selectedLocationId = cmp.get('v.selectedLocationId');

        var onSuccess = function(locationId){
            helper.navigateToSObject(opportunityId);
            helper.hideElement(cmp, 'modalSpinner');
            cmp.set('v.buttonsDisabled', false);
        };
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.errorMessage', errors[0].message);
            }
            helper.hideElement(cmp, 'modalSpinner');
            cmp.set('v.buttonsDisabled', false);

        };
        helper.saveOpportunityLocation(cmp, opportunityId, selectedLocationId, onSuccess, onFail);
    },
    doCancel: function (cmp, event, helper) {
        var opportunityId = cmp.get('v.recordId');
        helper.navigateToSObject(opportunityId);
    }
})