/**************************************************************************************
 Name: aura.NewOpportunityLocation
 Version: 1.0 
 Created Date: 26.04.2017
 Function: Adds new Location to Oppotunity base on Account locations

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   26.04.2017      Original Version
 *************************************************************************************/
({
    retrieveAccountLocations: function (cmp, opportunityId, onSuccess, onFail) {
        var action = cmp.get("c.retrieveAccountLocations");
        action.setParams(
            {
                opportunityId : opportunityId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var locations = response.getReturnValue();
                    onSuccess(locations);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    retrieveOpportunityName: function (cmp, opportunityId, onSuccess, onFail) {
        var action = cmp.get("c.retrieveOpportunityName");
        action.setParams(
            {
                opportunityId : opportunityId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var opportunityName = response.getReturnValue();
                    onSuccess(opportunityName);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveOpportunityLocation: function (cmp, opportunityId, locationId, onSuccess, onFail) {
        var action = cmp.get("c.saveOpportunityLocation");
        action.setParams(
            {
                opportunityId : opportunityId,
                locationId    : locationId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var opportunityLocationId = response.getReturnValue();
                    onSuccess(opportunityLocationId);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    onFail(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    navigateToSObject: function(recordId){
        if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
            sforce.one.navigateToSObject(recordId);
        }
    }
})