({
    doInit : function(cmp, evt, h) {
        var theReview = cmp.get('v.theReview');
        console.log('previously selected risks: ' + theReview.Risk_Categories_Selected__c);
        
        if(!$A.util.isEmpty(theReview.Risk_Categories_Selected__c)){
            var selectedCats = theReview.Risk_Categories_Selected__c.toLowerCase();
            var selectedCategories = cmp.get('v.selectedCategories');
            var hasCatsSelected = false;
            if(selectedCats.includes('execution')){
                selectedCategories.execution = true;
                hasCatsSelected = true;
                var boxCmp = cmp.find('executionBox');
                console.log('boxCmp: ', boxCmp);
                boxCmp.set('v.selected', true);
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'execution',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('client')){
                selectedCategories.client = true;
                hasCatsSelected = true;
                var boxCmp = cmp.find('clientBox');
                boxCmp.set('v.selected', true);
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'client',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('geography')){
                selectedCategories.geography = true;
                hasCatsSelected = true;
                var boxCmp = cmp.find('geographyBox');
                boxCmp.set('v.selected', true);
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'geography',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('commercial')){
                selectedCategories.commercial = true;
                hasCatsSelected = true;
                var boxCmp = cmp.find('commercialBox');
                boxCmp.set('v.selected', true);
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'commercial',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(selectedCats.includes('contractual')){
                selectedCategories.contractual = true;
                hasCatsSelected = true;
                var boxCmp = cmp.find('contractualBox');
                boxCmp.set('v.selected', true);
                var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
                selectEvent.setParams({
                    "categoryName" : 'contractual',
                    "disable" : false
                });
                selectEvent.fire();
            }
            if(hasCatsSelected){
                cmp.set('v.hasCatsSelected', true);
            }
            console.log('selectedCategories: ', selectedCategories);
            cmp.set('v.selectedCategories', selectedCategories);
        }
    },
	doBack : function(cmp, evt, h) {
		cmp.set('v.currentStep', 'Step 100');
	},
    doContinue : function(cmp, evt, h){
        var selectedCategories = cmp.get('v.selectedCategories');
        var selectedCatList = [];
        if(selectedCategories.execution){
            cmp.set('v.currentStep', 'Step 300');
        } else if(selectedCategories.client){
            cmp.set('v.currentStep', 'Step 400');
        } else if(selectedCategories.geography){
            cmp.set('v.currentStep', 'Step 500');
        } else if(selectedCategories.commercial){
            cmp.set('v.currentStep', 'Step 600');
        } else if(selectedCategories.contractual){
            cmp.set('v.currentStep', 'Step 700');
        } else {
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
                title: 'Warning',
                message: 'No Risk Categories have been selected!',
                duration: '5000',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
        if(selectedCategories.execution){
            selectedCatList.push('execution');
        }
        if(selectedCategories.client){
            selectedCatList.push('client');
        }
        if(selectedCategories.geography){
            selectedCatList.push('geography');
        }
        if(selectedCategories.commercial){
            selectedCatList.push('commercial');
        }
        if(selectedCategories.contractual){
            selectedCatList.push('contractual');
        }
        var selectedCatString = selectedCatList.join(';');
        var theReview = cmp.get('v.theReview');
        theReview.Risk_Categories_Selected__c = selectedCatString;
        cmp.set('v.theReview', theReview);
    },
    handleNavEvent : function(cmp, evt, h){
        var evtStep = evt.getParam('step');
        var evtAction = evt.getParam('actionType');
        if(evtStep == "Step 200" && evtAction == "continue"){
            h.doContinue(cmp, evt, h);
        }
        if(evtStep == "Step 200" && evtAction == "back"){
            cmp.set('v.currentStep', 'Step 100');
        }
    }
})