({
	doContinue : function(cmp, evt, h){
        var selectedCategories = cmp.get('v.selectedCategories');
        var selectedCatList = [];
        if(selectedCategories.execution){
            cmp.set('v.currentStep', 'Step 300');
        } else if(selectedCategories.client){
            cmp.set('v.currentStep', 'Step 400');
        } else if(selectedCategories.geography){
            cmp.set('v.currentStep', 'Step 500');
        } else if(selectedCategories.commercial){
            cmp.set('v.currentStep', 'Step 600');
        } else if(selectedCategories.contractual){
            cmp.set('v.currentStep', 'Step 700');
        } else {
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
                title: 'Warning',
                message: 'No Risk Categories have been selected!',
                duration: '5000',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
        if(selectedCategories.execution){
            selectedCatList.push('execution');
        }
        if(selectedCategories.client){
            selectedCatList.push('client');
        }
        if(selectedCategories.geography){
            selectedCatList.push('geography');
        }
        if(selectedCategories.commercial){
            selectedCatList.push('commercial');
        }
        if(selectedCategories.contractual){
            selectedCatList.push('contractual');
        }
        var selectedCatString = selectedCatList.join(';');
        var theReview = cmp.get('v.theReview');
        theReview.Risk_Categories_Selected__c = selectedCatString;
        cmp.set('v.theReview', theReview);
    },
})