/**************************************************************************************
 Name: ChildOpportunitiesRelatedList
 Version: 1.0 
 Created Date: 08.05.2017
 Function: Related list of opportunity child opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   08.05.2017      Original Version
 * Pradeep Shetty    03/07/2018		 US1636: Add Grand Total Row, new columns and factored/
									 unfactored toggle
 * Pradeep Shetty	 03/22/2018		 US1641: Added changes for creating Booking Value  
 * Pradeep Shetty    08/29/2018      US2327: Added a Booking Value Schedule functionality                                  
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        //DEBUG
        console.log('LOADED CHILD OPP COMP' + cmp.get("v.recordId"));
        var recordId = cmp.get("v.recordId");
        cmp.set('v.chosenItemIndex', null);
        cmp.set('v.chosenOpportunity', null);
        helper.showSpinner(cmp.getSuper());

        var loadUserPermissionsOnSuccess = function(crudPermissions){
            cmp.set('v.crudPermissions', crudPermissions);
        }
        helper.loadUserPermissions(cmp, loadUserPermissionsOnSuccess);

        var loadChildOpportunitiesOnSuccess = function(opportunities){
            cmp.set('v.opportunities', opportunities);
            var totalSum = helper.calculateTotalSum(opportunities);
            cmp.set('v.totalSum', totalSum);
            var parentOpportunity = cmp.get('v.parentOpportunity');            
            //Start: Added as part of US1636
            var grandTotal = helper.calculateGrandTotal(opportunities, parentOpportunity);
            cmp.set('v.grandTotal', grandTotal);
            //End: Added as part of US1636
            var difference = helper.calculateTotalDifference(totalSum, parentOpportunity);
            cmp.set('v.difference', difference);
            helper.hideSpinner(cmp.getSuper());
        }

        var loadParentOpportunityOnSuccess = function(opportunity){
            cmp.set('v.parentOpportunity', opportunity);

            helper.loadChildOpportunities(
                cmp,
                recordId,
                loadChildOpportunitiesOnSuccess
            );
        }
        
        helper.loadParentOpportunity(
            cmp,
            recordId,
            loadParentOpportunityOnSuccess
        );
    },
    onNew: function(cmp, event, helper){
        //Show the recordType selection pop up
        helper.showRecordTypeSelection(cmp,event);
    },
	onCreateNewRecord: function(cmp, event, helper){
        //Get parameters sent my the action call
        var channel = event.getParam("channel");

        //Check the channel and message and take action accordingly
        if(channel === "BookingValueCreateChannel"){
            var message = event.getParam("message");
            //If the message is reload, refresh the component
            if(message === "reload"){
                cmp.reload();
            }
            //If the message is a list of recordtypes, execute the next action
            else{
                var params = message.split(",");
                var recordTypeDeveloperName = params[0];
                var recordTypeId = params[1]; 
                //Determine which type of Opportunity does the user want to create               
                if(recordTypeDeveloperName === "New_Opportunity"){
                    //call helper method to create New Opportunity record
                    helper.createNewOpportunity(cmp, cmp.get("v.parentOpportunity"), recordTypeId); 
                }
                else if(recordTypeDeveloperName === "Booking_Value"){
                    //Show the booking value creation dialog
                    helper.createNewBookingValue(cmp, recordTypeId);                        
                }
            }

        }        
    },
    /*US2327: Added function that's called when 'New Booking Schedule' button is clicked*/
    onNewBookingSchedule: function(cmp, event, helper){
        //Show the Booking Schedule Form
        helper.showBookingScheduleForm(cmp,event);
    }
})