/**************************************************************************************
 Name: ChildOpportunitiesRelatedList
 Version: 1.0 
 Created Date: 08.05.2017
 Function: Related list of opportunity child opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05/08/2017      Original Version
 * Pradeep Shetty	 03/07/2018      US1636: Add Grand Total Row, new columns and factored/
									         unfactored toggle
 * Pradeep Shetty	03/22/2018		 US1641: Added changes for creating Booking Value   
 * Pradeep Shetty   07/07/2018       US2270: Add Selling Unit and Performance Unit
 * Pradeep Shetty    08/29/2018      US2327: Added a Booking Value Schedule functionality                                   
 *************************************************************************************/
({
    loadChildOpportunities : function(
        cmp,
        recordId,
        onSuccess
    ){
        var actionName = "c.getChildOpportunities",
            parameters = {recordId : recordId},
            isAbortable = true;

        this.callServer(cmp, actionName, parameters, isAbortable, onSuccess);
    },
    loadParentOpportunity : function(
        cmp,
        recordId,
        onSuccess
    ){
        var actionName = "c.getOpportunity",
            parameters = {recordId : recordId},
            isAbortable = true;

        this.callServer(cmp, actionName, parameters, isAbortable, onSuccess);
    },
    loadUserPermissions: function(
        cmp,
        onSuccess
    ){
        var actionName = "c.getUserPermissions",
            parameters = null,
            isAbortable = false;

        this.callServer(cmp, actionName, parameters, isAbortable, onSuccess);
    },
    calculateTotalSum: function(opportunities){
        var totalSum   = {gm : 0, factoredGM : 0, workHours : 0, factoredWorkHours : 0, revenue : 0, factoredRevenue : 0};
        for(var i=0; i<opportunities.length; i++){
            totalSum.gm 		       += opportunities[i].gm                ? opportunities[i].gm                : 0;
            totalSum.factoredGM        += opportunities[i].factoredGM        ? opportunities[i].factoredGM        : 0;
            totalSum.workHours 		   += opportunities[i].workHours         ? opportunities[i].workHours         : 0;
            totalSum.factoredWorkHours += opportunities[i].factoredWorkHours ? opportunities[i].factoredWorkHours : 0;            
            totalSum.revenue           += opportunities[i].revenue           ? opportunities[i].revenue           : 0;
            totalSum.factoredRevenue   += opportunities[i].factoredRevenue   ? opportunities[i].factoredRevenue   : 0;
        }
        return totalSum;
    },
    calculateTotalDifference: function(totalSum, parentOpportunity){
        var parentGM                = parentOpportunity.Amount 			      		 ? parentOpportunity.Amount                		  : 0;
        var parentFactoredGM        = parentOpportunity.Total_Probable_GM__c  		 ? parentOpportunity.Total_Probable_GM__c         : 0;
        var parentWorkHours         = parentOpportunity.Work_Hours__c 	      		 ? parentOpportunity.Work_Hours__c 	    		  : 0;
        var parentFactoredWorkHours = parentOpportunity.Factored_Workhours__c 		 ? parentOpportunity.Factored_Workhours__c 		  : 0;        
        var parentRevenue		    = parentOpportunity.Revenue__c 		  			 ? parentOpportunity.Revenue__c 		    	  : 0;
        var parentFactoredRevenue   = parentOpportunity.Pipeline_Factored_Revenue__c ? parentOpportunity.Pipeline_Factored_Revenue__c : 0;     
        var difference = {
            gm 				  : parentGM 				- totalSum.gm,
            factoredGM 		  : parentFactoredGM 		- totalSum.factoredGM,
            workHours 		  : parentWorkHours 		- totalSum.workHours,
            factoredWorkHours : parentFactoredWorkHours	- totalSum.factoredWorkHours,
            revenue 		  : parentRevenue 		    - totalSum.revenue,
            factoredRevenue   : parentFactoredRevenue   - totalSum.factoredRevenue
        };
        return difference;
    },
    calculateGrandTotal: function(opportunities, parentOpportunity){ //Added as part of US1636
        var grandTotal = {gm : 0, factoredGM : 0, workHours : 0, factoredWorkHours : 0, revenue : 0, factoredRevenue : 0};
        for(var i=0; i<opportunities.length; i++){
            if(opportunities[i].forecastCategory === 'Include'){
                grandTotal.gm 			     += opportunities[i].gm 	           ? opportunities[i].gm                : 0;
                grandTotal.factoredGM 	     += opportunities[i].factoredGM 	   ? opportunities[i].factoredGM        : 0;
                grandTotal.workHours 	     += opportunities[i].workHours 	       ? opportunities[i].workHours 	    : 0;
                grandTotal.factoredWorkHours += opportunities[i].factoredWorkHours ? opportunities[i].factoredWorkHours : 0;
				grandTotal.revenue           += opportunities[i].revenue           ? opportunities[i].revenue           : 0;
            	grandTotal.factoredRevenue   += opportunities[i].factoredRevenue   ? opportunities[i].factoredRevenue   : 0;                
            }
        }
        
        //Add the parent
        if(parentOpportunity.Forecast_Category__c === 'Include'){
            grandTotal.gm 			     += parentOpportunity.Amount 			           ? parentOpportunity.Amount                       : 0;
            grandTotal.factoredGM 	     += parentOpportunity.Total_Probable_GM__c 	       ? parentOpportunity.Total_Probable_GM__c         : 0;
            grandTotal.workHours 	     += parentOpportunity.Work_Hours__c 	           ? parentOpportunity.Work_Hours__c 	            : 0;
            grandTotal.factoredWorkHours += parentOpportunity.Factored_Workhours__c 	   ? parentOpportunity.Factored_Workhours__c        : 0;
            grandTotal.revenue           += parentOpportunity.Revenue__c           		   ? parentOpportunity.Revenue__c                   : 0;
            grandTotal.factoredRevenue   += parentOpportunity.Pipeline_Factored_Revenue__c ? parentOpportunity.Pipeline_Factored_Revenue__c : 0;                
        }
        return grandTotal;        
    },
    showRecordTypeSelection: function(cmp,event){
    	var modalBody;
        var modalFooter;
        $A.createComponents([["c:RecordTypeSelectionComponent",{"objectAPI":"Opportunity",
                            									"recordTypeNames":"Booking_Value,New_Opportunity",
                            									"defaultRecordType":"New_Opportunity",
                                                                "helpTextContent": $A.get("$Label.c.RecordTypeSelectionText"),
                                                                "sortOrder": "DESC",
                                                                "channelName": "BookingValueCreateChannel"
                                                               }
                             ],
        					 ["c:createRecordFooterComponent",{}
                             ]
    						],
                           function(components, status) {
                               if (status === "SUCCESS") {
                                   modalBody = components[0];
                                   modalFooter = components[1];
                                   modalFooter.set("v.modalBodyComponent", modalBody);
                                   cmp.find('overlayLib').showCustomModal({
                                       header: "New Opportunity",
                                       body: modalBody, 
                                       footer:modalFooter,
                                       showCloseButton: true,
                                       closeCallback: function() {
                                       }
                                   }).then(function(overlay){
                                   })
                               } 
                           });        
    },
    /*US2698: pre populate the values of fields copied by enhanced clone when opening new record form */
    createNewOpportunity: function(cmp, parentOpportunity, recordTypeId){
        var actionName = "c.getClonedOpportunityFieldsAndValues",
            parameters = {"opportunityId" : parentOpportunity.Id},
            isAbortable = true;

        var createChildOnSuccess = function(fieldsToCopy){
            var createRecordEvent = $A.get("e.force:createRecord");
            //Call standard create screen if user wants to create New Opportunity
            if(createRecordEvent){
                var params = {
                    entityApiName      : "Opportunity",
                    recordTypeId       : recordTypeId,
                    defaultFieldValues : {
                    }
                };

                fieldsToCopy.forEach(field => {
                    params.defaultFieldValues[field.apiName] = field.value;
                });

                params.defaultFieldValues.CloseDate = null;
                params.defaultFieldValues.Target_Project_Start_Date__c = null;
                params.defaultFieldValues.Account_Business_Segment__c = null;
                params.defaultFieldValues.AccountId = parentOpportunity.AccountId;
                params.defaultFieldValues.Parent_Opportunity__c = parentOpportunity.Id;
                params.defaultFieldValues.Name = params.defaultFieldValues.Name + ' CHILD';
                
                if(parentOpportunity.StageName == 'Closed - Won'){
                    params.defaultFieldValues.Program_New__c = parentOpportunity.Program_New__c;
                    params.defaultFieldValues.Task_Order_New__c = parentOpportunity.Task_Order_New__c;
                }

                createRecordEvent.setParams(params);  
                createRecordEvent.fire();
            }
        
        }
        
        this.callServer(cmp, actionName, parameters, isAbortable, createChildOnSuccess);
    },
	createNewBookingValue: function(cmp, recordTypeId){
		//Show the screen to create Booking Value
    	var modalBody;
      var modalFooter;

      //Changed the param to send the Opp Id instead of the Parent Opp
      $A.createComponents([["c:NewBookingValueComponent",{"bookingValueRecordTypeId":recordTypeId,
                                                          "bookingValueParentOpp":cmp.get("v.parentOpportunity")
                                                         }
                           ],
                           ["c:createRecordFooterComponent",{"labelName": $A.get("$Label.c.Save")}
                           ]
              ],
                         function(components, status) {
                             if (status === "SUCCESS") {
                                 modalBody = components[0];
                                 modalFooter = components[1];
                                 modalFooter.set("v.modalBodyComponent", modalBody);
                                 cmp.find('overlayLib').showCustomModal({
                                     header: "New Booking Value",
                                     body: modalBody, 
                                     footer:modalFooter,
                                     showCloseButton: true,
                                     closeCallback: function() {
                                     }
                                 }).then(function(overlay){  
                                 })
                             } 
                         });      
    },
    /*US2327: Added the function to launch Booking Schedule form. 
      This is launched when the 'New Booking Schedule' button is clicked*/
    showBookingScheduleForm: function(cmp,event){
        var modalBody;
        var modalFooter;
        $A.createComponents([["c:BookingValueScheduleForm",
                              {"bookingValueParentOpp":cmp.get("v.parentOpportunity")}
                             ],
                             ["c:createRecordFooterComponent",{"labelName": $A.get("$Label.c.Preview")}
                             ]
                            ],
                           function(components, status) {
                               if (status === "SUCCESS") {
                                   modalBody = components[0];
                                   modalFooter = components[1];
                                   modalFooter.set("v.modalBodyComponent", modalBody);
                                   cmp.find('overlayLib').showCustomModal({
                                       header: "New Booking Value Schedule",
                                       body: modalBody, 
                                       footer:modalFooter,
                                       cssClass: "slds-modal_medium",
                                       showCloseButton: true,
                                       closeCallback: function() {
                                       }
                                   }).then(function(overlay){
                                   })
                               } 
                           });        
    }    
})