({
	doInit : function(cmp, evt, h) {
		var componentName = cmp.get('v.componentName');
        console.log('componentName: ' + componentName);
        console.log('recordId: ' + cmp.get('v.recordId'));
        console.log('stepwrapper review record: ', cmp.get('v.theReview'));
        if(!$A.util.isEmpty(componentName)){
            componentName = 'c:' + componentName;
            $A.createComponent(
            	componentName,
                {
                    theReview: cmp.getReference('v.theReview'),
                    recordId: cmp.get('v.recordId'),
                    currentStep: cmp.getReference('v.currentStep'),
                    selectedCategories: cmp.getReference('v.selectedCategories'),
                    hasCatsSelected: cmp.getReference('v.hasCatsSelected'),
                    currentPage: cmp.getReference('v.currentPage'),
                    hasError: cmp.getReference('v.hasError')
                },
                function(newCmp, status, errorMessage){
                    if(status === 'SUCCESS'){
                        cmp.set('v.body', newCmp);
                    } else if(status === 'INCOMPLETE'){
                        console.log('No response from server');
                    } else if(status === 'ERROR'){
                        console.log('ERROR: ' + errorMessage);
                        cmp.set('v.body', 'Error loading step: ' + errorMessage);
                    }
                }
            );
        } else {
            console.log('component name is empty');
            cmp.set('v.configError', true);
        }
	}
})