/**************************************************************************************
 Name: JEG_DataTableFiscalYearSelector
 Version: 1.0 
 Created Date: 08.03.2017
 Function: Controller for JEG_DataTableFiscalYearSelector

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           08.03.2017         Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        var action = cmp.get("c.getFiscalYears");
        action.setParams(
            {
                groupId         : cmp.get("v.recordId"),
                sObjectName     : cmp.get("v.sObjectName"),
                fiscalYearField : cmp.get("v.fiscalYearField"),
                relationField   : cmp.get('v.relationField')
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var yearOptions = response.getReturnValue();
                    var opts = [];
                    for(var i=0; i<yearOptions.length; i++){
                        opts.push({
                            label : yearOptions[i],
                            value : yearOptions[i]
                        })
                    }
                    if(opts && opts.length > 0){
                        opts[opts.length-1].selected = "true";
                        var fiscalYearSelect = cmp.find("fiscalYearId");
                        fiscalYearSelect.set('v.options', opts);
                        cmp.yearChangedNotify();
                        helper.showElement(cmp, 'slector');
                    } else {
                        helper.showElement(cmp, 'missingData');
                    }
                    cmp.set("v.fiscalYearOptions", yearOptions);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        if(console){
                            console.log('errros', errors);
                        }
                        cmp.set("v.errorMsg", errors[0].message);
                        var errorMsg = cmp.find('errorMsg');
                        $A.util.removeClass(errorMsg, 'slds-hide');

                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    doYearChanged: function (cmp) {
        var appEvent = $A.get("e.c:JEG_DataTableFiscalYearSelectionEvent");
        var fiscalYearSelect = cmp.find("fiscalYearId");
        var fiscalYear = fiscalYearSelect.get("v.value");
        if(!fiscalYear){
            fiscalYear = null;
        }
        appEvent.setParams({ "fiscalYear" : fiscalYear });
        appEvent.fire();
    }
})