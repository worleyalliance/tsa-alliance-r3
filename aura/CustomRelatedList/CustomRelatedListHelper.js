/**************************************************************************************
 Name: aura.CustomRelatedList
 Version: 1.0 
 Created Date: 15.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   15.03.2017      Original Version
 *************************************************************************************/
({
    saveRecord : function (cmp, record, onSuccess, onFail) {
        this.hideElement(cmp, 'listIcon');
        this.showElement(cmp, 'listSpinner');
        var action = cmp.get("c.saveRecord");
        action.setParams(
            {
                record : record
            }
        );
        action.setCallback(this, function(response) {
            if (cmp.isValid()) {
                if(response) {
                    var state = response.getState();
                    if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                            console.log('errros:', errors[0].message);
                            onFail();
                        }
                    } else if (state === "SUCCESS") {
                        var recordId = response.getReturnValue();
                        record["Id"]= recordId;
                        if (onSuccess) {
                            onSuccess();
                        }
                        this.showToast('success', null, $A.get("$Label.c.Saved"));
                    }
                }
                this.hideElement(cmp, 'listSpinner');
                this.showElement(cmp, 'listIcon');
            }
        });
        $A.enqueueAction(action);
    },
    deleteRecord : function (cmp, recordId, callback) {
        var actionName = "c.deleteRecord",
            parameters = {recordId : recordId},
            isAbortable = false;

        this.callServer(cmp, actionName, parameters, isAbortable, callback);
    },
    loadUserPermissions: function(
        cmp,
        objectType,
        onSuccess
    ){
        var actionName = "c.getUserPermissions",
            parameters = {objectType : objectType},
            isAbortable = false;

        this.callServer(cmp, actionName, parameters, isAbortable, onSuccess);
    }
})