/**************************************************************************************
 Name: aura.CustomRelatedList
 Version: 1.0 
 Created Date: 14.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk           14.03.2017         Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        helper.showSpinner(cmp.getSuper());
        var fieldNames = cmp.get("v.fieldNameValues").split(','),
            objectType = cmp.get("v.objectType");
        cmp.set('v.fieldNames', fieldNames);
        var action = cmp.get("c.getData");
        action.setParams(
            {
                parentId        : cmp.get("v.recordId"),
                objectType      : objectType,
                relationName    : cmp.get("v.relationName"),
                linkFieldId     : cmp.get("v.linkFieldId"),
                linkFieldLabel  : cmp.get("v.linkFieldLabel"),
                fieldNames      : fieldNames
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var tableData = response.getReturnValue();

                    var linkFieldId = cmp.get("v.linkFieldId");
                    var linkFieldLabel = cmp.get("v.linkFieldLabel");
                    var linkColumnLabel = cmp.get("v.linkColumnLabel");
                    if(linkFieldId && linkFieldLabel &&  linkColumnLabel) {
                        tableData.linkFieldDescription = {
                            linkFieldId: linkFieldId,
                            linkFieldLabel: linkFieldLabel,
                            linkColumnLabel: linkColumnLabel
                        };
                    }
                    cmp.set("v.data", tableData);
                    cmp.set("v.originalRows", JSON.parse(JSON.stringify(tableData.rows)));
                }
                helper.hideSpinner(cmp.getSuper());
            }
        });
        $A.enqueueAction(action);

        var loadUserPermissionsOnSuccess = function(crudPermissions){
            cmp.set('v.crudPermissions', crudPermissions);
        }

        helper.loadUserPermissions(cmp, objectType, loadUserPermissionsOnSuccess);
    },

    createRecord : function (cmp) {
        var tableData = cmp.get("v.data");
        var newRecord = {
            mode        : 'edit',
            sobjectType : cmp.get('v.objectType'),
            selected    : true
        };
        newRecord[cmp.get("v.relationName")] = cmp.get("v.recordId");
        tableData.rows.unshift(newRecord);
        cmp.set("v.data", tableData);
    },
    handleItemValueChanged : function (cmp, event, helper) {
        var value = event.getParam("value");
        var fieldName = event.getParam("fieldName");
        var itemIndex = event.getParam("itemIndex");
        var tableData = cmp.get("v.data");
        tableData.rows[itemIndex][fieldName] = value;
        cmp.set('v.data', tableData);
    },
    handleActionChosen : function (cmp, event, helper) {
        var actionName = event.getParam("actionName");
        var itemIndex = event.getParam("itemIndex");
        if(actionName == 'edit') {
            var tableDataEdit = cmp.get("v.data");
            tableDataEdit.rows[itemIndex].mode = 'edit';
            cmp.set('v.data', tableDataEdit);
        } else if(actionName == 'discard') {
            var tableDataDiscard = cmp.get("v.data");
            var originalDataDiscard = cmp.get("v.originalRows");
            if(tableDataDiscard.rows[itemIndex].Id) {
                var originalRowClone = {};
                helper.clone(originalDataDiscard[itemIndex], originalRowClone);
                tableDataDiscard.rows[itemIndex] = originalRowClone;
                tableDataDiscard.rows[itemIndex].mode = 'view';
            } else {
                tableDataDiscard.rows.splice(itemIndex, 1);
            }
            cmp.set('v.data', tableDataDiscard);
        } else if(actionName == 'save') {
            helper.showSpinner(cmp.getSuper());
            var tableDataSave = cmp.get("v.data");

            for(var i=0; i<tableDataSave.fieldDescriptions.length; i++){
                var fieldLength = tableDataSave.fieldDescriptions[i].fieldLength;
                var fieldName = tableDataSave.fieldDescriptions[i].fieldName;
                var fieldLabel = tableDataSave.fieldDescriptions[i].fieldLabel;
                var fieldValue = tableDataSave.rows[itemIndex][fieldName];
                
                if(fieldValue && fieldLength && fieldValue.length > fieldLength){
                    helper.showToast('error', $A.get("$Label.c.Error"), fieldLabel + " - " + fieldLength + " " + $A.get("$Label.c.MaxFieldLength"));
                    return;
                }
            }

            helper.saveRecord(
                cmp,
                tableDataSave.rows[itemIndex],
                function () {
                    var originalDataSave = [];
                    tableDataSave.rows[itemIndex].mode = 'view';

                    helper.deepClone(tableDataSave.rows, originalDataSave);
                    cmp.set('v.data', tableDataSave);
                    cmp.set('v.originalRows', originalDataSave);
                    helper.hideSpinner(cmp.getSuper());
                },
                function(){
                    helper.hideSpinner(cmp.getSuper());
                }
            );

        } else if(actionName == 'delete') {
            var tableData = cmp.get("v.data");
            var temp = {
                itemId : tableData.rows[itemIndex].Id,
                itemIndex : itemIndex
            };
            cmp.set('v.temp', temp);

            helper.showElement(cmp, 'delConfirmationDialog');
        }
    },
    onDeleteConfirmation : function (cmp, event, helper) {
        var temp = cmp.get('v.temp');
        var itemId = temp.itemId;
        var itemIndex = temp.itemIndex;
        helper.showElement(cmp, 'modalSpinner');
        helper.deleteRecord(
            cmp,
            itemId,
            function () {
                var tableData = cmp.get("v.data");
                var originalData = cmp.get("v.originalRows");

                originalData.splice(itemIndex, 1);
                tableData.rows.splice(itemIndex, 1);
                cmp.set('v.data', tableData);
                cmp.set('v.originalRows', originalData);
                helper.hideElement(cmp, 'delConfirmationDialog');
                helper.hideElement(cmp, 'modalSpinner');
                helper.showToast('success', null, $A.get("$Label.c.Deleted"));
            }
        );
    },
    onDeleteDiscard : function (cmp, event, helper) {
        cmp.set('v.temp', null);
        helper.hideElement(cmp, 'delConfirmationDialog');
    }
})