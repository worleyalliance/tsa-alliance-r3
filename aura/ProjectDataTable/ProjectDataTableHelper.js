/**************************************************************************************
 Name: ProjectDataTable
 Version: 1.0 
 Created Date: 04.05.2017
Function: Component displays project list of given opportunity

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04.05.2017      Original Version
 * Pradeep Shetty    09/12/2017      Added Project Type parameter
 *************************************************************************************/
({
    loadData : function(
        cmp,
        fieldSetName,
        recordId,
        sObjectName,
        relationField,
        isSumDefault,
        totalRowPresent,
        recordTypeNames,
        projectTypeNames,
        onSuccess
    ){
        var action = cmp.get("c.getData");
        action.setParams(
            {
                fieldSetName        : fieldSetName,
                recordId            : recordId,
                recordTypeNames     : recordTypeNames,
                projectTypeNames	: projectTypeNames,
                sObjectName         : sObjectName,
                relationField       : relationField,
                isSumDefault        : isSumDefault,
                totalRowPresent     : totalRowPresent
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var tableData = response.getReturnValue();
                    onSuccess(tableData);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})