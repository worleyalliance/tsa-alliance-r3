/**************************************************************************************
 Name: ProjectDataTable
 Version: 1.0 
 Created Date: 04.05.2017
Function: Component displays project list of given opportunity

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04.05.2017      Original Version
 * Pradeep Shetty    09/12/2017      Added Project Type parameter
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        cmp.set("v.dataLoaded", false);

        var fieldSetName        = cmp.get("v.fieldSetName"),
            recordId             = cmp.get("v.recordId"),
            sObjectName         = cmp.get('v.sObjectName'),
            relationField       = cmp.get('v.relationField'),
            //isSumDefault        = true,
            //totalRowPresent     = true,
            recordTypeNames     = cmp.get('v.recordTypeNames'),
            projectTypeNames    = cmp.get('v.projectTypeNames');
        console.log('fieldsetname: ' + fieldSetName);
        	if(fieldSetName == 'Project_Info_Opportunity') {
            	var isSumDefault = false;
            	var totalRowPresent = false;
            }else{
                var isSumDefault = true;
                var totalRowPresent = true;
           }
       // }

        var onSuccess = function(tableData){
            cmp.set('v.data', tableData);
            cmp.set("v.dataLoaded", true);
        }
        helper.loadData(
            cmp,
            fieldSetName,
            recordId,
            sObjectName,
            relationField,
            isSumDefault,
            totalRowPresent,
            recordTypeNames,
            projectTypeNames,
            onSuccess
        );
    }
})