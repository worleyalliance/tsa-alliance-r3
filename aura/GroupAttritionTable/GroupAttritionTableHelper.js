/**************************************************************************************
 Name: aura.GroupAttritionTable
 Version: 1.0 
 Created Date: 05.05.2017
Function: Component adjusted to display Group Attrition table values

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.05.2017      Original Version
 *************************************************************************************/
({
    loadData : function(
        cmp,
        groupId,
        fieldSetName,
        fiscalYear,
        fiscalYearField,
        sObjectName,
        relationField,
        isSumDefault,
        additionalValueField,
        totalRowPresent,
        onSuccess
    ){
        var action = cmp.get("c.getData");
        action.setParams(
            {
                fieldSetName        : fieldSetName,
                groupId             : groupId,
                fiscalYear          : fiscalYear,
                fiscalYearField     : fiscalYearField,
                sObjectName         : sObjectName,
                relationField       : relationField,
                isSumDefault        : isSumDefault,
                additionalValueField: additionalValueField,
                totalRowPresent     : totalRowPresent
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var tableData = response.getReturnValue();
                    onSuccess(tableData);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})