/**************************************************************************************
 Name: GroupAttritionTable
 Version: 1.0 
 Created Date: 05.05.2017
Function: Component adjusted to display Group Attrition table values

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   05.05.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        cmp.set("v.dataLoaded", false);

        var totalRowPresent = cmp.get('v.totalRowPresentValue') == 'Yes' ? true : false;
        cmp.set('v.totalRowPresent', totalRowPresent);

        var fieldSetName        = cmp.get("v.fieldSetName"),
            groupId             = cmp.get("v.recordId"),
            fiscalYear          = cmp.get("v.fiscalYear"),
            fiscalYearField     = cmp.get("v.fiscalYearField"),
            sObjectName         = cmp.get('v.sObjectName'),
            relationField       = cmp.get('v.relationField'),
            isSumDefault        = cmp.get('v.defaultTotalRowBehaviour') == "Sum",
            additionalValueField= cmp.get('v.additionalValueField');

        var onSuccess = function(tableData){
            cmp.set('v.data', tableData);
            cmp.set("v.dataLoaded", true);
        }

        helper.loadData(
            cmp,
            groupId,
            fieldSetName,
            fiscalYear,
            fiscalYearField,
            sObjectName,
            relationField,
            isSumDefault,
            additionalValueField,
            totalRowPresent,
            onSuccess
        )

    },
    handleFiscalYearChange: function (cmp, event) {
        var fiscalYear = event.getParam("fiscalYear");
        cmp.set("v.fiscalYear", fiscalYear);
    }
})