/**************************************************************************************
Name: Request_SharePointSite
Version: 1.0 
Created Date: 20.09.2018 
Function: Quick action that will allow user to request a SharePoint Site

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Scott Stone      20.09.2018      Original Version
*************************************************************************************/
({
    checkRequestAllowed: function (cmp, bpId, onSuccess, onError) {
        var action = cmp.get("c.checkRequestAllowed");
        action.setParams({"oppId": cmp.get("v.recordId")});
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var requestAllowed = response.getReturnValue();
                onSuccess(requestAllowed);
            } else if(state == "ERROR" && response.getError().length > 0){
                let errorData = JSON.parse(response.getError()[0].message);
                onError(errorData);
            }
            
        });
        $A.enqueueAction(action);
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    }
})