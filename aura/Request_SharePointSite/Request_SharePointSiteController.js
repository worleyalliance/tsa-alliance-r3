/**************************************************************************************
Name: Request_SharePointSite
Version: 1.0 
Created Date: 20.09.2018 
Function: Quick action that will allow user to request a SharePoint Site	

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Scott Stone      20.09.2018      Original Version
*************************************************************************************/
({
    doInit: function (cmp, event, helper) {
        var oppId = cmp.get('v.recordId');
        // callback function 
        var requestAllowedSuccess = function(requestAllowed){
            if(requestAllowed){
                helper.showElement(cmp,'confirmationContent');
                helper.showElement(cmp, 'saveButton');
                helper.hideElement(cmp, 'errorContent');
            } else {
                helper.showElement(cmp,'errorContent');
                helper.showElement(cmp,'notAuthorizedMessage');
                helper.hideElement(cmp, 'saveButton');
            }
        }
        var requestAllowedError = function(errorData){
            helper.showElement(cmp, 'errorContent');
            helper.hideElement(cmp,'confirmationContent');
            helper.hideElement(cmp, 'saveButton');
            
            helper.hideElement(cmp, 'alreadyRequestedMessage');
            helper.hideElement(cmp, 'alreadyRequestedMessage');
            helper.hideElement(cmp, 'requestFailedMessage');
            helper.hideElement(cmp, 'alreadyCreatedMessage');
            
            cmp.set("v.errorData", errorData);
            if (errorData.id == 'ALREADY_CREATED') {
                helper.showElement(cmp, 'alreadyCreatedMessage');
                cmp.set("v.url", errorData.sharePointUrl);
            } else if (errorData.id == 'ALREADY_REQUESTED') {
                helper.showElement(cmp, 'alreadyRequestedMessage');
            } else if (errorData.id == 'REQUEST_FAILED') {
                helper.showElement(cmp, 'requestFailedMessage');
                helper.showElement(cmp, 'saveButton');
            } else if (errorData.id == 'IN_PROGRESS') {
                helper.showElement(cmp, 'inProgressMessage');
            }
            
            if(errorData.displayOverride){
                helper.showElement(cmp, 'overrideSection');
            }
        }
        // Invoke helper method 
        helper.checkRequestAllowed(cmp, oppId, requestAllowedSuccess, requestAllowedError);
    },
    handleSave: function(component, event, helper) {
        var requestSharePointSite = component.get("c.requestOpportunitySharePointSite");
        requestSharePointSite.setParams({
            "oppId": component.get("v.recordId") 
        });
        requestSharePointSite.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Your request has been submitted."
                });
                // Update the UI: close panel, show toast, refresh account page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                let errors = response.getError();
                let message = 'Error requesting the SharePoint Site. Please contact System Administrator'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message += ':' + errors[0].message;
                }
                // Display the message
                console.error(message); 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error:",
                    "message": message,
                    "type": "error"
                });
                $A.get("e.force:closeQuickAction").fire();
                toastEvent.fire();
                $A.get("e.force:refreshView").fire();
                
                console.log('Problem saving Object, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to create the new contact
        $A.enqueueAction(requestSharePointSite);
    },
    navigateToSharePoint : function(cmp, helper, event){
        let sharepointUrl = cmp.get("v.url");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": sharepointUrl
        });
        urlEvent.fire();
    },
    doCancel : function(cmp,helper,event){
        var closeAction = $A.get("e.force:closeQuickAction");
        closeAction.fire();
    },
    
})