/**************************************************************************************
 Name: aura.OpportunityEstimations
 Version: 1.0 
 Created Date: 11.04.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   11.04.2017      Original Version
 * Jignesh Suvarna   03/12/2018      US1509: Data Stewards/Sales Super User - Edit Access
 * Jignesh Suvarna	 21/08/2018		 US2392: Added Submit logic to save Target Close Date field
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        helper.loadEstimation(
            cmp,
            cmp.get("v.recordId"),
            helper,
            function (estimation){
                if(estimation.classAT == '' || estimation.classAT == null){
                    cmp.set("v.isClassSet", false);
                } else {
                     cmp.set("v.isClassSet", true);
                }
                cmp.set("v.estimation", estimation);
                cmp.set("v.classAT", estimation.classAT);
                var estimationOriginal = {};
                helper.clone(estimation, estimationOriginal);
                cmp.set("v.estimationOriginal", estimationOriginal);
                helper.loadClassesDrivenFlag(cmp, function(isClassDriven){
                    cmp.set('v.isClassDriven', isClassDriven);
                });
                helper.loadForecastCategories(cmp, function(forecastCategories){
                    cmp.set('v.forecastCategories', forecastCategories);
                });
                helper.loadClasses(cmp, function(classes){
                    cmp.set('v.classes', classes);
                });
                helper.loadEditPermissions(cmp, function(hasEditPermission){
                    cmp.set('v.hasEditPermission', hasEditPermission);
                });
                helper.isEditable(cmp, function(isEditable){
                    cmp.set('v.isEditable', isEditable);
                });                
            }
        );
    },
    doSave: function(cmp, event, helper) {
        var estimation = cmp.get("v.estimation"),
            recordId = cmp.get("v.recordId");
        // US2392 JS - calling the Edit form's submit method to save Target Close Date
        cmp.find("recordEditForm").submit();
        // JS end
        helper.saveEstimation(
            cmp,
            recordId,
            estimation,
            helper,
            function (result) {
                helper.refreshView();
                helper.notifyDataChange();
                cmp.set("v.estimation", result);
                var estimationOriginal = {};
                helper.clone(result, estimationOriginal);
                cmp.set("v.estimationOriginal", estimationOriginal);
            }
        );
    },
    doCancel: function(cmp, event, helper) {
        var estimationOriginal = cmp.get("v.estimationOriginal");
        var estimation = {};
        helper.clone(estimationOriginal, estimation);
        cmp.set("v.estimation", estimation);
    },
    onClassATChange: function(cmp, event, helper){
        var estimation = cmp.get("v.estimation"),
            classAT = cmp.get("v.classAT"),
            isClassDriven = cmp.get("v.isClassDriven");
        if(isClassDriven){
            if(classAT == 'Class I'){
                estimation.go = 95;
            } else if(classAT == 'Class II'){
                estimation.go = 66;
            } else if(classAT == 'Class III'){
                estimation.go = 20;
            }
            estimation.classAT = classAT;
            cmp.set("v.estimation", estimation);
        }
        if(classAT == "" || classAT == null || classAT == undefined){
            cmp.set("v.isClassSet", false);
        } else {
            cmp.set("v.isClassSet", true);
        }
    }
})