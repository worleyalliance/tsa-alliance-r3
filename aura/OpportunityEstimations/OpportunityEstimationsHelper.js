/**************************************************************************************
 Name: aura.OpportunityEstimations
 Version: 1.0 
 Created Date: 11.04.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   11.04.2017      Original Version
 * Jignesh Suvarna   03/12/2018      US1509: Data Stewards/Sales Super User - Edit Access 
 *************************************************************************************/
({
    loadForecastCategories: function(cmp, onSuccess) {
        var action = cmp.get("c.getForecastCategories");
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var forecastCategories = response.getReturnValue();
                    onSuccess(forecastCategories);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        console.log('errors',errors);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    loadClasses: function(cmp, onSuccess){
        var action = cmp.get("c.getClasses");
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var classes = response.getReturnValue();
                    onSuccess(classes);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        console.log('errors',errors);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    loadClassesDrivenFlag: function(cmp, onSuccess){
        var action = cmp.get("c.isClassDriven");
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var isClassDriven = response.getReturnValue();
                    onSuccess(isClassDriven);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    if(errors) {
                        console.log('errors',errors);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    loadEstimation: function (cmp, recordId, helper, callback) {
        helper.showSpinner(cmp.getSuper());
        var action = cmp.get("c.getEstimation");
        action.setParams(
            {
                recordId : recordId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var estimation = response.getReturnValue();
                    callback(estimation);
                } if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
                helper.hideSpinner(cmp.getSuper());
            }

        });
        $A.enqueueAction(action);
    },
    saveEstimation: function (cmp, recordId, estimation, helper, callback) {
        helper.showSpinner(cmp.getSuper());
        var action = cmp.get("c.saveEstimations");
        action.setParams(
            {
                recordId       : recordId,
                estimationJSON : JSON.stringify(estimation)
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid()) {
                if (state === "SUCCESS") {
                    var estimation = response.getReturnValue();
                    callback(estimation);
                } if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        console.log('error', errors[0]);
                        this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                    }
                }
                helper.hideSpinner(cmp.getSuper());
            }
        });
        $A.enqueueAction(action);
    },
    notifyDataChange: function () {
        var appEvent = $A.get("e.c:OpportunityMultiOfficeChangeEvent");
        appEvent.fire();
    },
    loadEditPermissions: function (cmp, onSuccess) {
        var action = cmp.get("c.hasEditPermission");
        action.setCallback(this, function(response) {
            var state = response.getState();
               if (cmp.isValid()) {
                    if (state === "SUCCESS") {
                        var hasEditPermission = response.getReturnValue();
                        onSuccess(hasEditPermission);
                    } else if(state == "ERROR"){
                        var errors = response.getError();
                        if(errors) {
                            console.log('errors',errors);
                            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                        }
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    isEditable: function (cmp, onSuccess) {
        var action = cmp.get("c.isEditable");
        action.setParams(
            {
                recordId : cmp.get("v.recordId")
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
               if (cmp.isValid()) {
                    if (state === "SUCCESS") {
                        var isEditable = response.getReturnValue();
                        onSuccess(isEditable);
                    } else if(state == "ERROR"){
                        var errors = response.getError();
                        if(errors) {
                            console.log('errors',errors);
                            this.showToast('error', $A.get("$Label.c.Error"), errors[0].message);
                        }
                    }
                }
        });
        $A.enqueueAction(action); 
    }  
})