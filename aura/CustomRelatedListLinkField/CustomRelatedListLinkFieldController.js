/**************************************************************************************
 Name: aura.CustomRelatedListLinkField
 Version: 1.0 
 Created Date: 17.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   17.03.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp) {
        var record = cmp.get("v.record");
        var labelFieldName = cmp.get("v.fieldName");
        var idFieldName = cmp.get("v.fieldIdName");
        if(record) {
            cmp.set('v.labelValue', record[labelFieldName]);
            cmp.set('v.idValue', record[idFieldName]);
        }
    },
    onLinkClick: function (cmp) {
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt) {
            navEvt.setParams({
                "recordId": cmp.get("v.idValue"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
    }
})