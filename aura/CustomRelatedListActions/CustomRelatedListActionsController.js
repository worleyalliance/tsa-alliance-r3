/**************************************************************************************
 Name: aura.CustomRelatedListActions
 Version: 1.0 
 Created Date: 15.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   15.03.2017      Original Version
 *************************************************************************************/
({
    onAction: function (cmp, event, helper) {
        var actionName = event.getSource().get('v.value');
        helper.fireActionEvent(cmp, actionName, cmp.get('v.index'));
    }
})