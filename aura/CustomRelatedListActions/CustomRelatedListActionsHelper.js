/**************************************************************************************
 Name: aura.CustomRelatedListActions
 Version: 1.0 
 Created Date: 15.03.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   15.03.2017      Original Version
 *************************************************************************************/
({
    fireActionEvent: function (cmp, actionName, itemIndex) {
        var actionEvent = cmp.getEvent("actionChosen");
        actionEvent.setParams({
            actionName: actionName,
            itemIndex : itemIndex
        });
        actionEvent.fire();
    }
})