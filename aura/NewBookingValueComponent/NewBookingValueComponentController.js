/**************************************************************************************
Name: aura.NewBookingValueComponent
Version: 1.0 
Created Date: 03/20/2018
Function: Component to create a booking value Opportunity

Modification Log:
**************************************************************************************
* Developer         Date           	Description
**************************************************************************************
* Pradeep Shetty   	03/21/2018      Original Version
* Pradeep Shetty   	06/13/2018      US2179: Added Pursuit Type field to the layout
* Manuel Johnson	06/26/2018		DE532: Hide spinner and enable buttons when callback fails
* Pradeep Shetty   07/07/2018      US2270: Add Selling Unit and Performance Unit
*************************************************************************************/
({
    doInit: function(cmp, event, helper){
        
        //Instantiate the bookingValue attribute
        var bookingValue = {};
        cmp.set("v.bookingValue", bookingValue);
        
        //Callback when the save fails
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.newBookingValueError', errors[0].message);
            }
            
            //Send message to tell footer to hide spinner
            helper.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");
        };    


        if(  $A.util.isUndefinedOrNull( cmp.get("v.bookingValueParentOpp").Sales_Plan_Unit__c )){
            cmp.set('v.newBookingValueError', "WARNING: The Booking Value cannot be created if the Opportunity does not have a Sales Plan Unit assigned. Please go back to the Opportunity and add Sales Plan Unit if it is blank."
            );
        }
        
        //Get Picklist vslues for Scope of Services field
        helper.loadScopeOfServices(cmp, function (scopeOfServices) {
            cmp.set('v.scopeOfServicesOptions', scopeOfServices);
        }, onFail);
        
        //Get Picklist values for Stage field
        helper.loadStages(cmp, function (stages) {
            cmp.set('v.stageOptions', stages);
        }, onFail);

        //Start: US2179
        //Get Picklist values for Pursuit Type field
        helper.loadPursuitTypes(cmp, function (pursuitTypes) {
            cmp.set('v.pursuitTypeOptions', pursuitTypes);
        }, onFail);
        //End: US2179

        //Start: US2270
        //Get Picklist values for Selling Unit field
        helper.loadSellingUnits(cmp, function (sellingUnits) {
            cmp.set('v.sellingUnitOptions', sellingUnits);
            if(helper.setSUDisabledAttribute(cmp)){
                cmp.set("v.bookingValue.Selling_Unit__c", cmp.get("v.bookingValueParentOpp").Selling_Unit__c);
               // cmp.set("v.bookingValue.Sales_Plan_Unit__c.Name", cmp.get("v.bookingValueParentOpp").Sales_Plan_Unit__c.Name);
            }            
        }, onFail);      
        //End: US2270       

		//Get help text map
        helper.loadHelpText(cmp, function(helpTextMap){
            cmp.set('v.fieldHelpMap', helpTextMap);
        }, onFail); 

		//Get help text map
        helper.loadFieldLabel(cmp, function(fieldLabelMap){
            cmp.set('v.fieldLabelMap', fieldLabelMap);
        }, onFail);

        helper.setPerformanceUnitValue(cmp,event);
      
    },
 	doNext: function(cmp, event, helper) {

        //Callback when the save is successful
        var onSuccess = function(bookingValueList){
            helper.navigateToSObject(bookingValueList[0].Id);
            cmp.find("overlayLib").notifyClose();
        };
        
        //Callback when the save fails
        var onFail = function(errors){
            if(errors) {
                cmp.set('v.newBookingValueError', errors[0].message);
            }
            
            //Send message to tell footer to hide spinner
            helper.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");
        };                

        //Callback when validation is successful
        var onValidationSuccess = function(){
            //Call the helper method to save        
        	helper.saveBookingValue(cmp, onSuccess, onFail);
        }
        
        //Callback when validation fails
        var onValidationFail = function(error){
            if(error){                
                //Call the helper method to save        
                cmp.set("v.newBookingValueError", "These required fields must be completed: " + error);                
            }
            
            //Send message to tell footer to hide spinner
            helper.sendMessage(cmp, event, "hideSpinner", "BookingValueFooterChannel");
        }    
        
        //Perform validation
        helper.performInputValidation(cmp, onValidationSuccess, onValidationFail);        
    }    
})