/**************************************************************************************
Name: aura.NewBookingValueComponent
Version: 1.0 
Created Date: 03/20/2018
Function: Component to create a booking value Opportunity

Modification Log:
**************************************************************************************
* Developer         Date           Description
**************************************************************************************
* Pradeep Shetty   03/21/2018      Original Version
* Pradeep Shetty   06/13/2018      US2179: Added Pursuit Type field to the layout
* Pradeep Shetty   07/07/2018      US2270: Add Selling Unit and Performance Unit
* Pradeep Shetty   12/06/2018      DE839: Add logic to allow PU and SU selection when 
                                   Parent Opp PU / SU is null
*************************************************************************************/
({
    loadScopeOfServices: function(cmp, onSuccess, onFail) {
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getScopeOfServices", null, true, onSuccess, onFail);         
    },
    loadStages: function(cmp, onSuccess, onFail) {        
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getStages", null, true, onSuccess, onFail);         
    },
    //Start: US2179
    loadPursuitTypes: function(cmp, onSuccess, onFail) {        
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getPursuitTypes", null, true, onSuccess, onFail);         
    },
    //End: Us2179 

    //Start: US2270
    loadSellingUnits: function(cmp, onSuccess, onFail) {        
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getSellingUnits", null, true, onSuccess, onFail);         
    },
    //End: US2270 

    loadHelpText: function(cmp, onSuccess, onFail){
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getOpportunityFieldsHelpText", null, true, onSuccess, onFail);        
    },
    loadFieldLabel: function(cmp, onSuccess, onFail){
        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.getOpportunityFieldsLabel", null, true, onSuccess, onFail);        
    }, 
    performInputValidation: function(cmp, onValidationSuccess, onValidationFail){
    	var allowSave = true;
        
        var errorMessage = '';
        
        
        //Opportunity Name
        if(!cmp.find("oppName").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Name;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Name;
            }
            allowSave = false;
        }
        
        
        //Scope Of Services
        if(!cmp.find("oppScopeOfServices").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Scope_of_Services__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Scope_of_Services__c;
            }            
            allowSave = false;
        }   
        
        
        //Revenue
        if(!cmp.find("oppRevenue").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Revenue__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Revenue__c;
            }
            
            allowSave = false;
        }
        
        
        //Gross Margin
        if(!cmp.find("oppGrossMargin").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Amount;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Amount;
            }            
            allowSave = false;
        }
        
        
        //CloseDate
        if(!cmp.find("oppCloseDate").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").CloseDate;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").CloseDate;
            }                        
            allowSave = false;
        }
        
        
        //Target Project Start Date
        if(!cmp.find("oppProjectStartDate").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Target_Project_Start_Date__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Target_Project_Start_Date__c;
            }            
            allowSave = false;
        }
        

        //Duration
        if(!cmp.find("oppDuration").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Duration_Months__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Duration_Months__c;
            }
            allowSave = false;
        }
        
        
        //Go
        if(!cmp.find("oppGo").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Go__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Go__c;
            }
            allowSave = false;
        }
        
        
        //Get
        if(!cmp.find("oppGet").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Get__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Get__c;
            }
            allowSave = false;
        }
        
        
        //Stage
        if(!cmp.find("oppStage").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").StageName;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").StageName;
            }
            allowSave = false;
        }
        
        
        //Start: US2179
        //Pursuit Type
        if(!cmp.find("oppPursuitType").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Program_Renewal_Rebid__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Program_Renewal_Rebid__c;
            }
            allowSave = false;
        }        
        //End: US2179
        
        //Start: US2270
        //
         if(!cmp.find("oppSalesPlanUnit").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Sales_Plan_Unit__r.Name;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Sales_Plan_Unit__r.Name;
            }
            allowSave = false;
        }    
        
        
        if(!cmp.find("oppPerformanceUnit").get("v.value")){
            if(errorMessage.length>0){
                errorMessage = errorMessage + ", " + cmp.get("v.fieldLabelMap").Lead_Performance_Unit_PU__c;
            }
            else{
                errorMessage = errorMessage + cmp.get("v.fieldLabelMap").Lead_Performance_Unit_PU__c;
            }
            allowSave = false;
        }              
        //End: US2270       
        
        //Set the validation result
        // cmp.set("v.validationPassed", allowSave);
        
        //Check if save is allowed or not
        if(allowSave){
            onValidationSuccess();
        }
        else{
            console.log("sudeep-allowSaveElse");
        }
        
	},
    saveBookingValue: function(cmp, onSuccess, onFail){
		
        //Get the booking value attribute
        var bookingValue = cmp.get("v.bookingValue");
        
        //Set the RecordTypeId for the Booking Value
        bookingValue.RecordTypeId = cmp.get("v.bookingValueRecordTypeId");
        
        //Set the ParentOpportunity
        bookingValue.Parent_Opportunity__c = cmp.get("v.bookingValueParentOpp").Id;

        //Set the Go and Get Value
        bookingValue.Go__c = bookingValue.Go__c * 100;
        bookingValue.Get__c = bookingValue.Get__c * 100;

        //Set the sales plan unit
        bookingValue.Sales_Plan_Unit__c  = cmp.get("v.bookingValueParentOpp").Sales_Plan_Unit__c ;

        //This is needed to ensure that a list Opportunities can be sent to Apex Method. 
        //Without this parameter we get an error
        bookingValue.sobjectType = "Opportunity";
        
        //Prepare the list of booking value to be sent as a parameter
        var bookingValueList = [bookingValue];

        //Parameters to be sent to the server call
        var parameters = {
            "opp": bookingValueList
        };

        //Call server side method to create booking value(Opportunity)        
        this.callServer(cmp, "c.createBookingValue", parameters, true, onSuccess, onFail);
    },
    /*Function to communicate with other components*/ 
    sendMessage: function (cmp, event, message, channel){
      //Get a message event
        var sendMsgEvent = $A.get("e.ltng:sendMessage"); 
        
        //Set the message event parameters
        sendMsgEvent.setParams({
            "message": message, 
            "channel": channel 
        }); 
        sendMsgEvent.fire();
    },
    //Start US2270
    setSUDisabledAttribute: function(cmp){
        //Get the array of Selling Units
        var sellingUnitList = cmp.get("v.sellingUnitOptions");

        //Get the Parent Selling Unit
        var parentSellingUnit = cmp.get("v.bookingValueParentOpp").Selling_Unit__c;

        //Set the value based on the check
        var disabled = this.checkPickListExistence(sellingUnitList, parentSellingUnit) > 0 ? true : false;
        cmp.set("v.suDisabled", disabled);
        return disabled;

    },
    setPerformanceUnitValue: function(cmp,event){

        //Action parameters
        var actionParams = {unitValue : cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c};

        //Check if the parent Lead Performance Unit is null or not. 
        if(cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c){
            //Success function
            var onSuccess = function(isActive){
                cmp.set("v.puDisabled", isActive);
                //Set the Performance Unit for the Booking Value if the puDisabled is false
                if(isActive){
                    cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", cmp.get("v.bookingValueParentOpp").Lead_Performance_Unit_PU__c);
                }
                else{
                    cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", null);
                }
            }

            //Callback when action fails
            var onFail = function(errors){
                if(errors) {
                    cmp.set('v.newBookingValueError', errors[0].message);
                }
            };         

            //Call server side method to check if Parent Selling Unit is active by calling Apex action
            this.callServer(cmp, "c.checkPerformanceUnitState", actionParams, true, onSuccess, onFail);  
        }
        else{
            //If parent Lead PU is null, then allow users to select PU.
            cmp.set("v.puDisabled", false);
            cmp.set("v.bookingValue.Lead_Performance_Unit_PU__c", null);
        }

    } 
    //End US2270             
})