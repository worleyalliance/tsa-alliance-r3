/**************************************************************************************
 Name: aura.OpportunityMultiOfficeRow
 Version: 1.0 
 Created Date: 10.09.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Manuel Johnson    10.09.2017      Original Version
 * Pradeep Shetty	 01/11/2018		 US1337: preventUnchecking helper method to prevent users from manually unchecking lead PU
 *****************************************************************************************************************************/
({
  makeHoursRequired : function(cmp) {
    var resourceType = cmp.get('v.multioffice.resourceType') == undefined ? cmp.get('v.resourceTypes[0].value') : cmp.get('v.multioffice.resourceType');
    var lineOfBusiness = cmp.get('v.parentOpp.Line_of_Business__c');
    var resourceTypeSettings = cmp.get('v.resourceTypeSettings');
    var hours = cmp.find('hours');
    var hoursRequired = false;
    //Check if the combination exists in the setting
    if(resourceTypeSettings[lineOfBusiness + resourceType]){
      hoursRequired = resourceTypeSettings[lineOfBusiness + resourceType].Hours_Required__c;
    }
    else{
      hoursRequired = false;
    }
    
    if(hoursRequired){
        $A.util.addClass(hours, 'requiredField');
    } else {
        $A.util.removeClass(hours, 'requiredField');
    }
  },
  //Function to prevent unchecking the lead PU
  preventUnchecking: function(cmp){
      var leadPUCheckbox = cmp.find('leadPUCheckbox');
      var leadPUValue = leadPUCheckbox.get('v.value');
      if(leadPUValue===false)
      { 
          leadPUCheckbox.set('v.value', true);
          this.showToast('error', $A.get("$Label.c.Error"), $A.get("$Label.c.Lead_PU_Uncheck_Error"));
      }
  }
})