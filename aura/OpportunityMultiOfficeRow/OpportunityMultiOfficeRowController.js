/**************************************************************************************
 Name: aura.OpportunityMultiOfficeRow
 Version: 1.0 
 Created Date: 04.13.2017
 Function: Batch class for converting financial data in opportunities

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   04.13.2017      Original Version
 * Manuel Johnson	   10.09.2017		   Added validation for Hours per US1073
 * Pradeep Shetty    04/29/2018      US2010: All changes related to PU design change
 *************************************************************************************/
({
  doInit: function(cmp, event, helper) {
    var editedRowIndex = cmp.get('v.editedRowIndex'),
        currentRowIndex = cmp.get('v.currentRowIndex');
    if(cmp.isValid()){
      if(editedRowIndex == currentRowIndex){
        cmp.set('v.mode', 'edit');
        helper.makeHoursRequired(cmp);
      } else {
        cmp.set('v.mode', 'view');
      }
    }
  },
  //Some dummy workaroud cause aura is firing events in wrong ordered
  //Therefore ui:selectOption component have to rerender event if aura:iteration should rerender first
  performanceUnitChange: function(cmp, event, helper) {
    var multioffice = cmp.get('v.multioffice');      
    if(!multioffice){
      cmp.set('v.multioffice', {});
    }
  },
  //Hours are required when a certain type of Resource Type is selected
  resourceTypeChange: function(cmp, event, helper) {
    helper.makeHoursRequired(cmp);
  },
  //Function to show an error message if user tries to uncheck a lead PU
  leadPUUnchecked: function(cmp, event, helper){
    helper.preventUnchecking(cmp);
  }
 
})