/**************************************************************************************
Name: aura.ldtListview
Version: 1.0 
Created Date: 01-Jul-2018
Function: Component for displaying library records

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Madhu Shetty   	01-Jul-2018     Original Version
* Madhu Shetty   	20-Jul-2018     DE633 - Updated for fixing issue related to sort order of record type
* Chris Cornejo     02-Oct-2018     ATEN: (LDT) Lightning Data Table - Display number of files and allow Download All
*************************************************************************************/
({
    //Code to initialise the component
    initialiseLDTComponent : function(component, event, helper) {
        console.log("hello there");
        
        var currentRecordID = component.get("v.recordId");
        var currentObjectName = component.get("v.sObjectName");
        var lookupFieldName = component.get("v.sLookupFieldName");
        
        /*Setup the Lightning datatable*/
        helper.ConstructLDT(component, currentRecordID, lookupFieldName, "All");
        
        /*Setup the filter dropdown*/
        helper.populateRecTypeDropdowns(component, currentRecordID, currentObjectName, "FILTER", "lstRecTypeFilter");
        console.log("Finished populating Filter");
        /*Setup the dropdown on the New record popup */
        helper.populateRecTypeDropdowns(component, currentRecordID, currentObjectName, "NEW", "lstRecTypeForNewRec");
        console.log("Finished populating New");
    },
    
    //Event handler to load library records when the filter dropdown selection has changed.
    RecordTypeSelected: function(component, event, helper){
        var currentRecordID = component.get("v.recordId");
        var selRecordType = component.find("ddlRecTypeFilter").get("v.value");
        var lookupFieldName = component.get("v.sLookupFieldName");
        
        helper.ConstructLDT(component, currentRecordID, lookupFieldName, selRecordType);
    },
    
    //Event handler that is called when user selects a record type and clicks next on the new record popup.
    onCreateNewRecord: function(component, event, helper) {
        //Get parameters sent my the action call
        var channel = event.getParam("channel");
        //alert(channel);
        if(channel === "ldtLibraryListview"){
            var recordTypeDeveloperName;
            var recordTypeId;
            
            var message = event.getParam("message");
            var params = message.split(",");
            recordTypeDeveloperName = params[0];
            recordTypeId = params[1];
                    
            var action = component.get("c.getRecTypeId");
            var selectedRecord = component.get("v.recordId");
            var selectedObject = component.get("v.sObjectName");
            
            var createRecordEvent = $A.get("e.force:createRecord");
            
            var lookupFieldName = component.get("v.sLookupFieldName");
            var sdefaultvalues = {};
            sdefaultvalues[lookupFieldName] = selectedRecord;
           
            createRecordEvent.setParams({
                "entityApiName": 'Library__c',
                "defaultFieldValues": sdefaultvalues,
                "recordTypeId": recordTypeId
            });            
            createRecordEvent.fire();
            
        }
    },
    
    createNewLibraryRecord: function(cmp, event, helper){
        var modalBody;
        var modalFooter;
        var lstRecTypes = cmp.get("v.lstRecTypeForNewRec");
        var strRecTypes = [];
        for (var strRecType of lstRecTypes){
            strRecTypes.push(strRecType.DeveloperName);
        }
        
        $A.createComponents([["c:RecordTypeSelectionComponent",{"objectAPI":"Library__c",
                                                                "recordTypeNames":strRecTypes.join(),
                                                                "defaultRecordType":strRecTypes[0],
                                                                "sortOrder":"ASC",
                                                                "channelName": "ldtLibraryListview"
                                                               }],
                             ["c:createRecordFooterComponent",{}]
                            ],
                            function(components, status) {
                                if (status === "SUCCESS") {
                                    modalBody = components[0];
                                    modalFooter = components[1];
                                    modalFooter.set("v.modalBodyComponent", modalBody);
                                    cmp.find('overlayLib').showCustomModal({
                                        header: "New Library",
                                        body: modalBody, 
                                        footer:modalFooter,
                                        showCloseButton: true,
                                        closeCallback: function() {
                                        }
                                    }).then(function(overlay){
                                    })
                                } 
                            });  
    },
    tabActionClicked: function(cmp, event, helper){
        //get the id of the action being fired
 var actionId = event.getParam('actionId');

//US1890 - download all files
 if(actionId == 'downloadtask'){
     //alert('downloadtask');
 //get the row where click happened and its position
 var rowIdx = event.getParam("index");
 var clickedRow = event.getParam("row");

 //store the row and its position will for editing
 cmp.set("v.rowIndex",rowIdx);
 cmp.set("v.selectedLibrary",clickedRow);
     
  //set the type of task operation being done
 cmp.set("v.taskOpType",'Download'); 

helper.downloadFiles(cmp, clickedRow.Id, rowIdx);
 
    }
    }  
})