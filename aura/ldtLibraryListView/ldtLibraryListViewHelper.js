/**************************************************************************************
Name: aura.ldtLibraryListView
Version: 1.0 
Created Date: 01-Jul-2018
Function: Component for displaying library records

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Madhu Shetty   	01-Jul-2018     Original Version
* Chris Cornejo     02-Oct-2018     US1890 - ATEN: (LDT) Lightning Data Table - Display number of files and allow Download All
*************************************************************************************/

({
    //Function for building and loading the Lightning data table
	ConstructLDT: function(component, currentRecordID, LookupFieldName, selrecordType){
        //Column data for the table
        var ldtColumns = [
            {
                'label':'Name',
                'name':'Name',
                'type':'reference',
                'value':'Id',
                'class': 'wrapText',
                'resizeable': true
            },
            {
                'label':'Date',
                'name':'dt_submittal_date__c',
                'type':'reference',
                'value':'Id',
                'width': 170,
                'resizeable': true
            },
            {
                'label':'Record Type',
                'name':'RecordType.Name',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'resizeable': true
            },
            /*US1890 - added no of files column*/
            {
                'label':'No. of Files',
                'name':'num_count_of_related_files__c',
                'type':'reference',
                'value':'Id',
                'type':'string',
                'width':120,
                'resizeable': true
            }
        ];
        //Configuration data for the table to enable actions in the table
 var taskTableConfig = {
 //"massSelect":true,
 /*"globalAction":[
 {
     "label":"Add Task",
 "type":"button",
 "id":"addtask",
 "class":"slds-button slds-button--neutral"
 },
{
 "label":"Complete Task",
 "type":"button",
 "id":"completetask",
 "class":"slds-button slds-button--neutral"
 }
 ],
 */
"rowAction":[
 /*{
 "label":"Edit",
 "type":"url",
 "id":"edittask"
 },
 {
 "label":"Del",
 "type":"url",
 "id":"deltask"
 },
 */
 /*{
 "label":"Download All",
 "type":"button",
 "id":"downloadtask"
 },
 */

/* {
 "type":"menu",
 "id":"actions",
 "visible":function(task){
 return task.Stage__c != "Completed"
 },
 "menuOptions": [{
 "label":"Complete",
 "id":"completeTask"
 }]
     
 } */
 // US1890 - pull number of files per library record
 {
 "type":"menu",
 "id":"actions",
 "visible":function(libraryRecords){
 return libraryRecords.num_count_of_related_files__c != 0
 },
 "menuOptions": [{
 "label":"Download all",
 "id":"downloadtask"
 }]
     
 } 
 ]

 };
     
     
        var action = component.get("c.getLibraryRecords");
        action.setParams({
            "iRecordID": currentRecordID,
            "recordTypeLabel": selrecordType,
            "sLookupFieldName": LookupFieldName
        });
        console.log("currentRecordID =" + currentRecordID);
        console.log("yay");
        action.setCallback(this,function(resp){
            var state = resp.getState();
            
            if(component.isValid() && state === 'SUCCESS'){
                //set the records to be displayed
                component.set("v.libraryRecords",resp.getReturnValue());
                //set the column information
                component.set("v.ldtColumns",ldtColumns);
               
                //initialise the Lightning data table
                component.find("ldtLibraryTable").initialize();
                 //us1890
                ////pass the configuration of task table
                component.set("v.taskTableConfig",taskTableConfig);
                console.log("selrecordtype == " + selrecordType);
                if(selrecordType == "All")
                	component.set("v.sRecordcount", " (" + resp.getReturnValue().length + ")");
                    //$A.util.toggleClass(component.find("ldtLibraryTable"), "sls-hide");
                console.log("selrecordtype == all");
            }
            else {   console.log(resp.getError());   }
        });
        $A.enqueueAction(action);
        console.log("Finished initialization");
    },
    //Function for populating the recordtype dropdowns
    populateRecTypeDropdowns: function(component, currentRecordID, CurrentObjectName, recordTypesFor, dropdownValueAttrib){
        console.log("In populateRecTypeDropdowns function");
        console.log("component = " + component);
        console.log("dropdownValueAttrib = " + dropdownValueAttrib);
        var action = component.get("c.fetchRecordTypeValues");
        
        action.setParams({
            "iRecordID" : currentRecordID,
            "sObjectName" : CurrentObjectName,
            "RecordtypeFor" : recordTypesFor
        });
       console.log("currentRecordID = " + currentRecordID);
       console.log("CurrentObjectName = " + CurrentObjectName);
       console.log("recordTypesFor = " + recordTypesFor);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS'){
                var respValues = response.getReturnValue();
                component.set("v." + dropdownValueAttrib, respValues);
            }
            else {   console.log(response.getError());   }
        });
        
        $A.enqueueAction(action); 
    },
    //US1890 - download feature
    downloadFiles : function(cmp, task, index){
        var selectedProjId = cmp.get("v.recordId");
        
        var action = cmp.get("c.getdownloadFiles");
        action.setParams({
           "libId":task
         });

        action.setCallback(this,function(resp){
        var state = resp.getState();

        //if SUCCESS, download files
        if(cmp.isValid() && state === 'SUCCESS'){
         var fileIds = resp.getReturnValue();
        window.open('/sfc/servlet.shepherd/document/download' + fileIds + '?operationContext=S1');
        }
        else{
        console.log(resp.getError());
       }
       });

 $A.enqueueAction(action);
        
    }
    
})