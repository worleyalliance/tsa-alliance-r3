({
	clickBox : function(cmp, evt, h) {
		
        // get pre-click value of selected
        var iconImage = cmp.get('v.iconImage');
        var selected = cmp.get('v.selected');
        var newSelected = !selected;
        console.log('selected prev: ' + selected);
        cmp.set('v.selected', !selected);
        var selectEvent = $A.get("e.c:RA_CatSelectedEvent");
        console.log('targetCat: ' + iconImage);
        selectEvent.setParams({
            "categoryName" : iconImage,
            "disable" : selected
        });
        selectEvent.fire();
        
        var selectedCategories = cmp.get('v.selectedCategories');
        selectedCategories[iconImage] = newSelected;
        cmp.set('v.selectedCategories', selectedCategories);
        
        // If the box is becoming selected
        if(newSelected){
            cmp.set('v.hasCatsSelected', true);
        } else {
            if(selectedCategories.execution || selectedCategories.client || selectedCategories.geography ||
               selectedCategories.commercial || selectedCategories.contractual){
                cmp.set('v.hasCatsSelected', true);
            } else {
                cmp.set('v.hasCatsSelected', false);
            }
        }
        
        var selectedCatList = [];
        if(selectedCategories.execution){
            selectedCatList.push('execution');
        }
        if(selectedCategories.client){
            selectedCatList.push('client');
        }
        if(selectedCategories.geography){
            selectedCatList.push('geography');
        }
        if(selectedCategories.commercial){
            selectedCatList.push('commercial');
        }
        if(selectedCategories.contractual){
            selectedCatList.push('contractual');
        }
        var selectedCatString = selectedCatList.join(';');
        var theReview = cmp.get('v.theReview');
        theReview.Risk_Categories_Selected__c = selectedCatString;
        cmp.set('v.theReview', theReview);
        
        $A.get("e.c:RA_RecalcPercentComplete").fire();
        
	}
})