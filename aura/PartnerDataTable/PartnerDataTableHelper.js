/**************************************************************************************
 Name: PartnerDataTable
 Version: 1.0 
 Created Date: 07.05.2017
Function: Component adjusted to display Partner table values on Review page

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   07.05.2017      Original Version
 *************************************************************************************/
({
    loadData : function(
        cmp,
        fieldSetName,
        recordId,
        onSuccess
    ){
        var action = cmp.get("c.getData");
        action.setParams(
            {
                fieldSetName        : fieldSetName,
                recordId            : recordId
            }
        );
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            if(cmp.isValid()){
                if (state === "SUCCESS"){
                    var tableData = response.getReturnValue();
                    onSuccess(tableData);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleError(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})