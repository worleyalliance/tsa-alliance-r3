/**************************************************************************************
 Name: PartnerDataTable
 Version: 1.0 
 Created Date: 07.05.2017
Function: Component adjusted to display Partner table values on Review page

 Modification Log:
 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
 * Developer         Date            Description
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
 * Stefan Abramiuk   07.05.2017      Original Version
 *************************************************************************************/
({
    doInit: function(cmp, event, helper) {
        cmp.set("v.dataLoaded", false);

        var fieldSetName        = cmp.get("v.fieldSetName"),
            recordId             = cmp.get("v.recordId");

        var onSuccess = function(tableData){
            cmp.set('v.data', tableData);
            cmp.set("v.dataLoaded", true);
        }
        helper.loadData(
            cmp,
            fieldSetName,
            recordId,
            onSuccess
        );
    }
})