<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Opportunity 02 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{
    &quot;additionalConditions&quot;: [
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;5000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;35000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;100000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;3000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;15000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;100000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;1000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;15000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;35000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;75000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Pro-Services Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;5000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;35000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;75000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;1000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;15000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;20000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;60000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;0&quot;,
            &quot;RiskLevel&quot;: &quot;R5&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;1000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Standard&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;15000000&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;20000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;60000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Combined Services&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;0&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Reimbursable;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;0&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;25000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;75000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Fixed Rates;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;0&quot;,
            &quot;RiskLevel&quot;: &quot;R4&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;15000000&quot;,
            &quot;RiskLevel&quot;: &quot;R3&quot;,
            &quot;RiskCategory&quot;: &quot;Elevated&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;50000000&quot;,
            &quot;RiskLevel&quot;: &quot;R2&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        },
        {
            &quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
            &quot;FieldName&quot;: &quot;Contract_Type_s__c;Revenue__c&quot;,
            &quot;isString&quot;: true,
            &quot;Operator&quot;: &quot;E;GE&quot;,
            &quot;Value&quot;: &quot;Lump Sum;150000000&quot;,
            &quot;RiskLevel&quot;: &quot;R1&quot;,
            &quot;RiskCategory&quot;: &quot;High&quot;,
            &quot;Answer&quot;: &quot;Construction Only&quot;
        }
    ]
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Service_Type_Code__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_100</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R5&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
