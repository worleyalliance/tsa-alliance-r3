<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GRP - Contract Documentation</label>
    <protected>false</protected>
    <values>
        <field>For_Filter__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>For_New__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Library_RecType_API__c</field>
        <value xsi:type="xsd:string">Contract_Documentation</value>
    </values>
    <values>
        <field>Obj_API__c</field>
        <value xsi:type="xsd:string">AT_Group__c</value>
    </values>
    <values>
        <field>Obj_RecType_API__c</field>
        <value xsi:type="xsd:string">None</value>
    </values>
</CustomMetadata>
