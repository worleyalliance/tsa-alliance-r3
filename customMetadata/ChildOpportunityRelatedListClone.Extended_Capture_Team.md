<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Extended Capture Team</label>
    <protected>false</protected>
    <values>
        <field>API_ChildRelationshipName__c</field>
        <value xsi:type="xsd:string">Extended_Capture_Team__r</value>
    </values>
    <values>
        <field>API_Name__c</field>
        <value xsi:type="xsd:string">Extended_Capture_Team__c</value>
    </values>
    <values>
        <field>API_RelationshipName__c</field>
        <value xsi:type="xsd:string">Opportunity__c</value>
    </values>
</CustomMetadata>
