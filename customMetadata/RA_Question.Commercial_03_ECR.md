<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Commercial 03 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;:[ 
{
    &quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
    &quot;RiskLevel&quot;: &quot;R3&quot;, 
    &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
    &quot;Answer&quot;:&quot;Payment terms &gt; net 60 days&quot;
},
{
    &quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
    &quot;RiskLevel&quot;: &quot;R4&quot;, 
    &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
    &quot;Answer&quot;:&quot;Payment terms 31-60 days&quot;
},
{
    &quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
    &quot;RiskLevel&quot;: &quot;R3&quot;, 
    &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
    &quot;Answer&quot;:&quot;Lump Sum: Negative cash flow &gt; $3M&quot;
},
{
    &quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
    &quot;RiskLevel&quot;: &quot;R4&quot;, 
    &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
    &quot;Answer&quot;:&quot;Lump Sum: Negative cash flow &lt; $3M&quot;
}
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Payment_Terms_Less_Than_Favorable_ECR__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_600</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R5&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
