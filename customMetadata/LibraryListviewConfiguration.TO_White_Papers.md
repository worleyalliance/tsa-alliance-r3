<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TO - White Papers</label>
    <protected>false</protected>
    <values>
        <field>For_Filter__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>For_New__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Library_RecType_API__c</field>
        <value xsi:type="xsd:string">White_Papers</value>
    </values>
    <values>
        <field>Obj_API__c</field>
        <value xsi:type="xsd:string">Project_Descriptions__c</value>
    </values>
    <values>
        <field>Obj_RecType_API__c</field>
        <value xsi:type="xsd:string">Task_Order</value>
    </values>
</CustomMetadata>
