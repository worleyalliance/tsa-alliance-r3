<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contractual 06 BIAF</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [ 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R2&quot;, 
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;Performance guarantees required&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R4&quot;, 
&quot;RiskCategory&quot;:&quot;Elevated&quot;, 
&quot;Answer&quot;:&quot;Heightened standard of care&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R7&quot;, 
&quot;RiskCategory&quot;:&quot;Standard&quot;, 
&quot;Answer&quot;:&quot;Normal professional negligence standard of care&quot;}
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Performance_Guarantees__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">BIAF</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_700</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R7&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
</CustomMetadata>
