<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Execution 04 BIAF</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;, 
        &quot;RiskCategory&quot;:&quot;High&quot;, 
        &quot;Answer&quot;:&quot;Yes - All (excl. pre-FEL3 or any type of exclusivity) (BIA only)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;, 
        &quot;RiskCategory&quot;:&quot;High&quot;, 
        &quot;Answer&quot;:&quot;Yes - FEL3/DD and TCV&gt;$15M (BIE only)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;, 
        &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
        &quot;Answer&quot;:&quot;Yes - New or pilot technology (BIE only)&quot;}
]
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Does_project_involve_new_technology__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">BIAF</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_300</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R7&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
</CustomMetadata>
