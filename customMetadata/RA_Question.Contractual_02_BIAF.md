<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contractual 02 BIAF</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;, 
        &quot;RiskCategory&quot;:&quot;High&quot;, 
        &quot;Answer&quot;:&quot;PRIVATE: Exposure &gt; $10M (no prior approval)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;, 
        &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
        &quot;Answer&quot;:&quot;PRIVATE: Exposure $5-10M or &gt;$10M &amp; prior approval&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R5&quot;, 
        &quot;RiskCategory&quot;:&quot;Standard&quot;, 
        &quot;Answer&quot;:&quot;PRIVATE: Exposure &lt; $5M&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R7&quot;, 
        &quot;RiskCategory&quot;:&quot;Standard&quot;, 
        &quot;Answer&quot;:&quot;PRIVATE: Exposure &lt; 5xFee&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;, 
        &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
        &quot;Answer&quot;:&quot;PUBLIC: New FW or exposure &gt; $5M (new clients) (BIE only)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R5&quot;, 
        &quot;RiskCategory&quot;:&quot;Standard&quot;, 
        &quot;Answer&quot;:&quot;PUBLIC: Exposure &gt; $5M (prior approval) (BIE only)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R7&quot;, 
        &quot;RiskCategory&quot;:&quot;Standard&quot;, 
        &quot;Answer&quot;:&quot;PUBLIC: Exposure &lt; 5xFee (BIE only)&quot;},
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;RiskLevel&quot;: &quot;R7&quot;, 
        &quot;RiskCategory&quot;:&quot;Standard&quot;, 
        &quot;Answer&quot;:&quot;No liabilities&quot;}
]
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Total_Limit_of_Contract_Liability__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">BIAF</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_700</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R7&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
