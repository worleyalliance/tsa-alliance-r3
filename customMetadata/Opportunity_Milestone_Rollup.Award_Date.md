<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Award Date</label>
    <protected>false</protected>
    <values>
        <field>Aggregate_Result_Field__c</field>
        <value xsi:type="xsd:string">Award_Date__c</value>
    </values>
    <values>
        <field>Milestone_Type__c</field>
        <value xsi:type="xsd:string">Award</value>
    </values>
    <values>
        <field>Occurs_Only_Once__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
