<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OpportunityTeamMember</label>
    <protected>false</protected>
    <values>
        <field>API_ChildRelationshipName__c</field>
        <value xsi:type="xsd:string">OpportunityTeamMembers</value>
    </values>
    <values>
        <field>API_Name__c</field>
        <value xsi:type="xsd:string">OpportunityTeamMember</value>
    </values>
    <values>
        <field>API_RelationshipName__c</field>
        <value xsi:type="xsd:string">OpportunityId</value>
    </values>
</CustomMetadata>
