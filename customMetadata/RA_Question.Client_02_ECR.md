<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Client 02 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;,
     &quot;FieldName&quot;: &quot;Is_the_project_for_a_new_client__c&quot;,
    &quot;isString&quot;: true,
    &quot;Operator&quot;: &quot;E&quot;,
    &quot;Value&quot;: &quot;Yes&quot;,
    &quot;RiskLevel&quot;: &quot;R3&quot;,
    &quot;RiskCategory&quot;:&quot;Elevated&quot;, 
    &quot;Answer&quot;: &quot;Yes&quot;}
]
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Is_the_client_a_newly_formed_JV__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_400</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R5&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
