<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Target_Project_Start_Date__c</label>
    <protected>false</protected>
    <values>
        <field>Opportunity_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Target_Project_Start_Date__c</value>
    </values>
    <values>
        <field>Refresh__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
