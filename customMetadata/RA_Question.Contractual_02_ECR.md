<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contractual 02 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [ 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R2&quot;, 
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;All others (unlimited)&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R3&quot;, 
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;LOL &gt;TCV and $5-10M&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R3&quot;, 
&quot;RiskCategory&quot;:&quot;Elevated&quot;, 
&quot;Answer&quot;:&quot;LOL &gt;TCV and &lt;$5M&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R3&quot;, 
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;LOL &lt;TCV and $10-75M (w. exclusions)&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R3&quot;, 
&quot;RiskCategory&quot;:&quot;Elevated&quot;, 
&quot;Answer&quot;:&quot;LOL &lt;TCV and $5-10M (w. exclusions)&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R4&quot;, 
&quot;RiskCategory&quot;:&quot;Standard&quot;, 
&quot;Answer&quot;:&quot;LOL &lt;TCV and &lt;$5M (w. exclusions)&quot;}
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Total_Limit_of_Contract_Liability__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_700</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R5&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
