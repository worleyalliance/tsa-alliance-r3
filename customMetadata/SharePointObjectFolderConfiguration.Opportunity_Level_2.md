<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Opportunity Level 2</label>
    <protected>false</protected>
    <values>
        <field>FolderName__c</field>
        <value xsi:type="xsd:string">Opportunities</value>
    </values>
    <values>
        <field>IsReference__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Level__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>ReferenceFieldName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ReferenceObjectType__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
