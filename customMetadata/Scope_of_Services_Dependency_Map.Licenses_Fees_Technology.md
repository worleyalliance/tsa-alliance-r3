<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Licenses Fees (Technology)</label>
    <protected>false</protected>
    <values>
        <field>Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Scope_of_Services__c</field>
        <value xsi:type="xsd:string">Licenses Fees (Technology)</value>
    </values>
    <values>
        <field>Service_Type_Code__c</field>
        <value xsi:type="xsd:string">B</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">EPC, EPCM (Comprimo) or EPF (Chemetics)</value>
    </values>
</CustomMetadata>
