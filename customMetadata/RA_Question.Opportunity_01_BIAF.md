<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Opportunity 01 BIAF</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [ 
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;0&quot;, 
        &quot;RiskLevel&quot;: &quot;R7&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;1000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R6&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;5000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R5&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;35000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R4&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;70000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;150000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Reimbursable&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;0&quot;, 
        &quot;RiskLevel&quot;: &quot;R7&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;1000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R6&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;5000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R5&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;15000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R4&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;40000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;50000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;150000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R1&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Lump Sum&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;1000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R6&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;5000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R5&quot;,
        &quot;RiskCategory&quot;:&quot;Standard&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;15000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R4&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;40000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R3&quot;,
        &quot;RiskCategory&quot;:&quot;Elevated&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;50000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R2&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    },
    {&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
        &quot;FieldName&quot;: &quot;Revenue__c&quot;,
        &quot;Operator&quot;:&quot;GE&quot;, 
        &quot;Value&quot;:&quot;150000000&quot;, 
        &quot;RiskLevel&quot;: &quot;R1&quot;,
        &quot;RiskCategory&quot;:&quot;High&quot;,
        &quot;Answer&quot;:&quot;Fixed Rates&quot; 
    } 
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Contract_Type_s__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">BIAF</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_100</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R7&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
