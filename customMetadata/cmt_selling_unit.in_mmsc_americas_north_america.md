<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MMSC Americas North America</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Business_Unit__c</field>
        <value xsi:type="xsd:string">Mining &amp; Minerals and Specialty Chemicals</value>
    </values>
    <values>
        <field>End_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">Industrial</value>
    </values>
    <values>
        <field>Selling_Unit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Start_Date__c</field>
        <value xsi:type="xsd:date">2015-09-01</value>
    </values>
    <values>
        <field>txt_pu_number__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>txt_selling_unit_full_name__c</field>
        <value xsi:type="xsd:string">Industrial M&amp;M and Spec Chem Americas North America</value>
    </values>
</CustomMetadata>
