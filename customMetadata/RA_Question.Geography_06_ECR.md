<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Geography 06 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Previously_approved_by_LOB_president__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_500</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
</CustomMetadata>
