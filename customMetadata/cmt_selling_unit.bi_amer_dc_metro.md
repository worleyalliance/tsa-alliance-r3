<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bldgs &amp; Infra Americas DC Metro</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Business_Unit__c</field>
        <value xsi:type="xsd:string">B&amp;I-Americas</value>
    </values>
    <values>
        <field>End_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">Buildings &amp; Infrastructure</value>
    </values>
    <values>
        <field>Selling_Unit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Start_Date__c</field>
        <value xsi:type="xsd:date">2015-09-01</value>
    </values>
    <values>
        <field>txt_pu_number__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>txt_selling_unit_full_name__c</field>
        <value xsi:type="xsd:string">Bldgs &amp; Infra Americas DC Metro</value>
    </values>
</CustomMetadata>
