<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Line_of_Business__c</label>
    <protected>false</protected>
    <values>
        <field>Opportunity_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Line_of_Business__c</value>
    </values>
    <values>
        <field>Refresh__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
