<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Execution 01 BIAF</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;:[ 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;FieldName&quot;: &quot;Revenue__c&quot;, 
&quot;Operator&quot;:&quot;GE&quot;, 
&quot;Value&quot;:&quot;25000000&quot;, 
&quot;RiskLevel&quot;: &quot;R2&quot;,
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;Yes&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;FieldName&quot;: &quot;Revenue__c&quot;, 
&quot;Operator&quot;:&quot;LT&quot;, 
&quot;Value&quot;:&quot;25000000&quot;, 
&quot;RiskLevel&quot;: &quot;R4&quot;,
&quot;RiskCategory&quot;:&quot;Elevated&quot;, 
&quot;Answer&quot;:&quot;Yes&quot;} 
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Decision_authority_on_behalf_of_client__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">BIAF</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_300</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R7&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
