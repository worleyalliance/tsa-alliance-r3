<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contractual 05 ECR</label>
    <protected>false</protected>
    <values>
        <field>Additional_Condition__c</field>
        <value xsi:type="xsd:string">{ &quot;additionalConditions&quot;: [ 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R2&quot;, 
&quot;RiskCategory&quot;:&quot;High&quot;, 
&quot;Answer&quot;:&quot;Unlimited (R2)&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R3&quot;, 
&quot;RiskCategory&quot;:&quot;Elevated&quot;, 
&quot;Answer&quot;:&quot;Semi limited, exposure &lt;$5M  (R3)&quot;}, 
{&quot;ObjectAPIName&quot;: &quot;Review__c&quot;, 
&quot;RiskLevel&quot;: &quot;R4&quot;, 
&quot;RiskCategory&quot;:&quot;Standard&quot;, 
&quot;Answer&quot;:&quot;Limited (R4)&quot;}
] 
}</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Indemnification_beyond_our_negligence__c</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">ECR</value>
    </values>
    <values>
        <field>RA_Step__c</field>
        <value xsi:type="xsd:string">Step_700</value>
    </values>
    <values>
        <field>Risk_Level_Condition__c</field>
        <value xsi:type="xsd:string">{&quot;else&quot;:&quot;R5&quot;}</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
</CustomMetadata>
