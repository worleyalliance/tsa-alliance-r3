<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Step 600</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">RA_CommercialRisks</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">600.0</value>
    </values>
    <values>
        <field>Step_Label__c</field>
        <value xsi:type="xsd:string">Commercial Risks</value>
    </values>
</CustomMetadata>
