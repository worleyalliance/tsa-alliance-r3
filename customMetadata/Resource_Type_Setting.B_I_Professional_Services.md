<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>B&amp;I Professional Services</label>
    <protected>false</protected>
    <values>
        <field>Hours_Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">Buildings &amp; Infrastructure</value>
    </values>
    <values>
        <field>Margin_Must_Be_Less_than_Revenue__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Resource_Type__c</field>
        <value xsi:type="xsd:string">Professional Services</value>
    </values>
    <values>
        <field>Revenue_Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
