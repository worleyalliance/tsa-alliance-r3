/**************************************************************************************
Name: OpportunityLocation_Trigger
Version: 1.0 
Created Date: 03.08.2018
Function: To handle all the trigger functionality of Opportunity Location object

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    03.08.2017         Original Version
*************************************************************************************/
trigger OpportunityLocation_Trigger on Opportunity_Locations__c (before insert, after update, before delete) {
	
    if(TriggerUtil.isTriggerActive('OpportunityLocation_Trigger')){
        if(Trigger.isBefore && Trigger.isInsert) {
            OpportunityLocation_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }   
        
        if(Trigger.isAfter && Trigger.isUpdate) { 
            OpportunityLocation_TriggerHandler.getHandler().onAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }   
        
        if(Trigger.isBefore && Trigger.isDelete) { 
            OpportunityLocation_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
        }    
    }
}