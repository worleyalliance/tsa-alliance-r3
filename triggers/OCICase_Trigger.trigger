/**************************************************************************************
Name: OCICase_Trigger
Version: 1.0 
Created Date: 20.07.2017
Function: OCI Case Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    20.07.2017      Original Version
* Manuel Johnson	12.13.2017		Updated to switch to OCI Case instead of Case object
*************************************************************************************/
trigger OCICase_Trigger on OCI_Case__c (after delete) {
	if(TriggerUtil.isTriggerActive('OCI_Case_Trigger')){
        if(Trigger.isAfter && Trigger.isDelete){
            OCICase_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }
    }
}