/**************************************************************************************
Name: RH_TMRA_OpportunityTeamMember
Function: Resource Hero - Auto create resource assignment on opportunity team member create

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         		Date            	Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-09-24       	Original Version
*************************************************************************************/
trigger RH_TMRA_OpportunityTeamMember on OpportunityTeamMember (after insert) {
    /*
    system.debug('-------------------------');
    //Get list of user ids
    List<Id> userids = new List<Id>();
    Set<Id> oppids = new Set<Id>();
    for(OpportunityTeamMember otm : Trigger.new) {
		userids.add(otm.UserId);
        oppids.add(otm.OpportunityId);
    }
    
    //Find resources with this user id
    List<ResourceHeroApp__Resource__c> resources = [SELECT Id, ResourceHeroApp__User__c
                                                    FROM ResourceHeroApp__Resource__c
                                                    WHERE ResourceHeroApp__User__c in :userids];
    
	//Build reference map
    Map<Id, Id> resource_userref = new Map<Id, Id>();
    if(resources.size() > 0) {
        for(ResourceHeroApp__Resource__c r : resources) {
            resource_userref.put(r.ResourceHeroApp__User__c, r.Id);
        }
    }
    
    //Get existing ids
    List<ResourceHeroApp__Resource_Assignment__c> existingras = [SELECT Id, ResourceHeroApp__Opportunity__c, ResourceHeroApp__Resource__c
                                                                 FROM ResourceHeroApp__Resource_Assignment__c
                                                                 WHERE ResourceHeroApp__Opportunity__c in :oppids];
    Map<String, ResourceHeroApp__Resource_Assignment__c> rasref = new Map<String, ResourceHeroApp__Resource_Assignment__c>();
    for(ResourceHeroApp__Resource_Assignment__c ra : existingras) {
        rasref.put(ra.ResourceHeroApp__Opportunity__c + '-' + ra.ResourceHeroApp__Resource__c, ra);
    }
    
    
    List<ResourceHeroApp__Resource_Assignment__c> ras = new List<ResourceHeroApp__Resource_Assignment__c>();
    for(OpportunityTeamMember otm : Trigger.new) {
        if(resource_userref.containsKey(otm.UserId) && rasref.containsKey(otm.OpportunityId + '-' + resource_userref.get(otm.UserId)) == false) {
            ResourceHeroApp__Resource_Assignment__c ra = new ResourceHeroApp__Resource_Assignment__c();
            ra.ResourceHeroApp__Opportunity__c = otm.OpportunityId;
            ra.ResourceHeroApp__Role__c = otm.TeamMemberRole;
            ra.ResourceHeroApp__Resource__c = resource_userref.get(otm.UserId);
            ras.add(ra);
            system.debug('ra:  ' + ra);
        }
    }
    system.debug('Ras:  ' + ras);
    insert ras;
    system.debug('---------------------');
*/
}