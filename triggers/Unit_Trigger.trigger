/**************************************************************************************
Name: Unit_Trigger
Version: 1.0 
Created Date: 12.04.2018
Function: To handle all the trigger functionality of the Unit object

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Walker      12.04.2018      Original Version
*************************************************************************************/
trigger Unit_Trigger on Unit__c(after update){
    if(TriggerUtil.isTriggerActive('Unit_Trigger')){
       if (Trigger.isAfter){
            if(Trigger.isUpdate){
                Unit_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
        }     
    }    
}