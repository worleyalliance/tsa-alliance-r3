/**************************************************************************************
Name: SiteLocation_Trigger
Version: 1.0 
Created Date: 04.20.2017
Function: To handle all the trigger functionality of Location object

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni    04.20.2017         Original Version
*************************************************************************************/
trigger SiteLocation_Trigger on SiteLocation__c (after insert, after update, after delete, after undelete, before delete, before insert, before update) {
    
    if(TriggerUtil.isTriggerActive('SiteLocation_Trigger')){   
        if(Trigger.isBefore && Trigger.isInsert){
            SiteLocation_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }
        
        if(Trigger.isBefore && Trigger.isUpdate){
            SiteLocation_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new);
        }
        
        if(Trigger.isBefore && Trigger.isDelete){
            //Handler method to make sure a Location record with Primary flag is not deleted
            SiteLocation_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
        }
        
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) { 
            //Handler method to make sure there is only one unique Primary location per account
            SiteLocation_TriggerHandler.getHandler().onAfterUpsert(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
        }
    }
}