/**************************************************************************************
* Name: Jacobs_User_Trigger
* Version: 1.0 
* Created Date: 11.30.2017
* Function: On User Creation/Update, this trigger will update the associated License 
*     Request, so the License count always reflect the realtime readings.
* 
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Ray Zhao          11.30.2017      Original Version
* Manuel Johnson    04.26.2018      Updated isUpdate to allow for multiple actions for US2038
***************************************************************************************/
trigger Jacobs_User_Trigger_AIAU on User (after insert, after update) {
    
    if(TriggerUtil.isTriggerActive('Jacobs_User_Trigger')){
    
        // On insert, pass the bulk trigger.new to InsertHandler
        if (Trigger.isInsert) {
            
            // Get an email=>Id user collection
            Map<String, Id> um = new Map<String, Id>();
            for (User u : Trigger.New) {
                if (u.isActive == TRUE) {
                    //Link back to License Request use Email
                    um.put(u.Email, u.Id);
                }
            }
            if (um.Size()>0){
                Jacobs_User_TriggerHandler.onAfterInsert(um);
            }
        }

        // On update, add all records that has isActive changed in the map, call UpdateHandler
        // Pass the bulk to Update handler.  Any other update should not fire up this code.
        if (Trigger.isUpdate) {
            Jacobs_User_TriggerHandler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}