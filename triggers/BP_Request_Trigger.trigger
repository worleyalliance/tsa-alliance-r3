/**************************************************************************************
Name: BP_Request_Trigger
Version: 1.0 
Created Date: 03.15.2017
Function: B_P_Request__c Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      03/15/2017      Original Version
* Manuel Johnson    08/16/2017      Updates per US780 and US768
*************************************************************************************/

trigger BP_Request_Trigger on B_P_Request__c (after insert, after update, after delete, after undelete, before insert, before update) {

    if(TriggerUtil.isTriggerActive('BP_Request_Trigger')){
        if(Trigger.isAfter && Trigger.isInsert){
            BP_Request_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
        } else if (Trigger.isAfter && Trigger.isUpdate){
            BP_Request_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
        } else if (Trigger.isBefore && Trigger.isInsert){
            BP_Request_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        } else if (Trigger.isBefore && Trigger.isUpdate){
            BP_Request_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new, Trigger.oldMap);
        } 
        /*if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || trigger.isDelete || trigger.isUndelete)){  
          BP_Request_TriggerHandler.getHandler().onAfterOperations(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete);
        }*/
    }

}