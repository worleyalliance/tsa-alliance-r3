/**************************************************************************************
Name: Review_Trigger
Version: 1.0 
Created Date: 10.06.2018
Function: Trigger for the Review__c Object

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Blake Poutra   	10/06/2018      Original Version
* Manuel Johnson	10/09/2018		US2578 Copy values when Review is cloned
*************************************************************************************/
trigger Review_Trigger on Review__c (before insert, after update) {
    
    if(TriggerUtil.isTriggerActive('Review_Trigger')){
        
        // The handler method to execute steps before insert
        if(Trigger.isBefore && Trigger.isInsert){
            Review_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }
        
        // The handler method to execute steps before update
        if(Trigger.isAfter && Trigger.isUpdate){
            Review_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }


}