/**************************************************************************************
Name: ContentDocumentLink_Trigger
Version: 1.0
Created Date: 10/09/2017
Function: Trigger for ContentDocumentLink to execute actions on file upload/update

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    10/09/2017      Original Version
*************************************************************************************/
trigger ContentDocumentLink_Trigger on ContentDocumentLink (before insert, after insert, after update) {
    
    if(TriggerUtil.isTriggerActive('ContentDocumentLink_Trigger')){
        if(Trigger.isBefore && Trigger.isInsert){
            ContentDocumentLink_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }
        
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            ContentDocumentLink_TriggerHandler.getHandler().onAfterUpsert(Trigger.new);
        }
    }
    
}