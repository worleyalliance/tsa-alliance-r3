/**************************************************************************************
Name: Library_Trigger
Version: 1.0 
Created Date: 09/04/2018
Function:Library Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    09/04/2018      Original Version
*************************************************************************************/
trigger Library_Trigger on Library__c (before insert, before update) {
    
    if(TriggerUtil.isTriggerActive('Library_Trigger')){  
        
        // The handler method to execute steps before insert
        if(Trigger.isBefore && Trigger.isInsert){
            Library_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }  
        
        //The handler method call to execute steps before update
        if(Trigger.isBefore && Trigger.isUpdate){
            Library_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }  
        
    }  
    
}