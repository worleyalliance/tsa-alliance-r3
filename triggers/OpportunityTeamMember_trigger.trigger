/**************************************************************************************
Name: OpportunityTeamMember_Trigger
Version: 1.0 
Created Date: 10/04/2018
Function:Opportunity Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer      	   	Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna   	10/04/2018      Original Version
* Madhu Shetty     	 	14/12/2018      US2563: Added after delete trigger handler 
*************************************************************************************/
trigger OpportunityTeamMember_trigger on OpportunityTeamMember (before insert, before delete, after insert, after update, after delete ) {
   if(TriggerUtil.isTriggerActive('OpportunityTeamMember_Trigger')){
        if(Trigger.isBefore && Trigger.isInsert) {
            OpportunityTeamMember_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }     
        
        if(Trigger.isBefore && Trigger.isDelete) { 
            OpportunityTeamMember_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
        }  

        //DE612 - Added AfterInsert to update the Opportunity Access Level to Owner for the OTM with Read/Edit access
        if(Trigger.isAfter && Trigger.isInsert) {
            OpportunityTeamMember_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
        }   
        
        if(Trigger.isAfter && Trigger.isUpdate) { 
            OpportunityTeamMember_TriggerHandler.getHandler().onAfterUpdate(Trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.isDelete) {
            OpportunityTeamMember_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }    
    }
}