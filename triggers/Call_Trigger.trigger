/**************************************************************************************
Name: SiteLocation_Trigger
Version: 1.0 
Created Date: 08.28.2018
Function: To handle all the trigger functionality of Call object

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Stone       08.28.2018         Original Version
*************************************************************************************/
trigger Call_Trigger on Call__c (before delete) {
    
    if(Trigger.isBefore && Trigger.isDelete){
        Call_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
    }
    
}