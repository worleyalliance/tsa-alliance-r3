/**************************************************************************************
Name: Competitor_Trigger
Version: 1.0 
Created Date: 10/04/2018
Function: Competitor Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer             Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna       05/10/2018      Original Version
*************************************************************************************/

trigger Competitor_Trigger on Competitor__c (after insert, after update, after delete) {
    if(TriggerUtil.isTriggerActive('Competitor_Trigger')){
        if(Trigger.isAfter && Trigger.isInsert) { 
            Competitor_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
        }  
        
        if(Trigger.isAfter && Trigger.isUpdate) { 
            Competitor_TriggerHandler.getHandler().onAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }   
        
        if(Trigger.isAfter && Trigger.isDelete) { 
            Competitor_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }    
    }
}