/**************************************************************************************
Name: MultiOfficeSplit_Trigger
Version: 1.0 
Created Date: 03.15.2017
Function: To handle all the trigger functionality of Multi-Office Split object

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni    04.03.2017         Original Version
* Pradeep Shetty    08/21/2018      US2396: Automatically adjust MOS start dates
*************************************************************************************/
trigger MultiOfficeSplit_Trigger on Multi_Office_Split__c (after insert, after update, after delete, after undelete, before delete, before insert, before update) {

    if(TriggerUtil.isTriggerActive('MultiOfficeSplit_Trigger')){
        Set<Id> oppIdSet = new Set<Id>{};
            
        if (Trigger.isBefore){
            //Handler method to make sure a record with Lead flag is not deleted
            if(Trigger.isDelete){
                 MultiOfficeSplit_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
            }else if(Trigger.isUpdate){
                MultiOfficeSplit_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }else if(Trigger.isInsert){
                MultiOfficeSplit_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
            }
        }    
        
        if(Trigger.isAfter) {
         
         //US2396: Handler method to execute operations after insert
         if(trigger.isInsert) {
               MultiOfficeSplit_TriggerHandler.getHandler().onAfterInsert(trigger.new);
         }    

         //US2396: Handler method to execute operations after update
         else if(trigger.isUpdate) {
               MultiOfficeSplit_TriggerHandler.getHandler().onAfterUpdate(trigger.oldMap, trigger.newMap);
         }     
      }
    }
}