/**************************************************************************************
Name: RH_ACR_Contact
Function: Resource Hero - Automatic creation of resource from resource contact

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-09-24          Original Version
*************************************************************************************/
trigger RH_ACR_Contact on Contact (after insert, before update) {
    /*
    for(Contact c : Trigger.new) {
        if(c.Utilize_in_Resource_Hero__c && c.User__c == null)
            c.addError('User field must be populated before you can utilized a contact in Resource Hero.');
    }
    
    if(Trigger.isInsert) {
        List<ResourceHeroApp__Resource__c> newresources = new List<ResourceHeroApp__Resource__c>();
        List<Id> contactidsforreset = new List<Id>();
        
        for(Contact c : Trigger.new) {
            if(c.Utilize_in_Resource_Hero__c) {
                newresources.add(new ResourceHeroApp__Resource__c(Name = c.FirstName + ' ' + c.LastName,
                                                                  ResourceHeroApp__User__c = c.User__c,
                                                                  ResourceHeroApp__Weekly_Target_Min_Hours__c = 35,
                                                                  ResourceHeroApp__Weekly_Target_Max_Hours__c = 45,
                                                                  Jacobs_Resource_Contact__c = c.Id));
                contactidsforreset.add(c.Id);
            }
        }
        
        //If we have inserted resources insert them and create a reference
        Map<Id, Id> con_resref = new Map<Id, Id>();
        if(newresources.size() > 0) {
            insert newresources;
            
            for(ResourceHeroApp__Resource__c r : newresources) {
                con_resref.put(r.Jacobs_Resource_Contact__c, r.Id);
            }
        }
        
        if(contactidsforreset.size() > 0)
        {
            List<Contact> contactsforreset = [SELECT Id, Utilize_in_Resource_Hero__c FROM Contact WHERE Id in :contactidsforreset];
            for(Contact c : contactsforreset) {
                c.Utilize_in_Resource_Hero__c = false;
                c.Resource_Hero_Record__c = con_resref.get(c.Id);
            }
            update contactsforreset;
        }
    }
    else if(Trigger.isUpdate) {
        Set<Id> contactidsforsearch = new Set<Id>();
        for(Contact c : Trigger.new) {
            //Only process the records where flag is set
            if(c.Utilize_in_Resource_Hero__c) {
                //Add this contact id to the set so we can search to see if a resource already exists that is linked to this record
                contactidsforsearch.add(c.Id);
            }
        }
        
        //Get list of resources that are already connected to our contact
        List<ResourceHeroApp__Resource__c> resourceswithmatchingcontacts = new List<ResourceHeroApp__Resource__c>();
        resourceswithmatchingcontacts = [SELECT Id, Jacobs_Resource_Contact__c
                                         FROM ResourceHeroApp__Resource__c
                                         WHERE Jacobs_Resource_Contact__c IN :contactidsforsearch];
        
        //If we find any resources, put them in a format that we can use
        Map<Id, Id> con_resref = new Map<Id, Id>();
        if(resourceswithmatchingcontacts.size() > 0) {
            for(ResourceHeroApp__Resource__c r : resourceswithmatchingcontacts) {
                con_resref.put(r.Jacobs_Resource_Contact__c, r.Id);
            }
        }
        
        //For each contact, either add new resource or update based on existing
        Map<Id, ResourceHeroApp__Resource__c> newresources = new Map<Id, ResourceHeroApp__Resource__c>();
        for(Contact c : Trigger.new) {
            //If we already have a matching contact, us it
            if(c.Utilize_in_Resource_Hero__c && con_resref.containsKey(c.Id)) {
                c.Resource_Hero_Record__c = con_resref.get(c.Id);
            }
            else if(c.Utilize_in_Resource_Hero__c && con_resref.containsKey(c.Id) == false) {
                newresources.put(c.Id, new ResourceHeroApp__Resource__c(Name = c.FirstName + ' ' + c.LastName,
                                                                  ResourceHeroApp__User__c = c.User__c,
                                                                  ResourceHeroApp__Weekly_Target_Min_Hours__c = 35,
                                                                  ResourceHeroApp__Weekly_Target_Max_Hours__c = 45,
                                                                  Jacobs_Resource_Contact__c = c.Id));
            }
        }
        
        //If we have new resources to create
        if(newresources.size() > 0) {
            insert newresources.values();
        }
        
        for(Contact c : Trigger.new) {
            //Uncheck checkbox
            c.Utilize_in_Resource_Hero__c = false;
            
            //If we just insert the resource for this contact, update the resource lookup field
            if(newresources.containsKey(c.Id)) {
                c.Resource_Hero_Record__c = newresources.get(c.Id).Id;
            }
        }
    }
  */  
}