/**************************************************************************************
Name: RH_SMAF_Milestone
Function: Resource Hero - Scheduled milestone autoforecasting

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-09-21          Original Version
*************************************************************************************/
trigger RH_SMAF_Milestone on Schedule_Milestone__c (after update, after delete) {
/*
//Schedule milestone auto
    if(Trigger.isInsert || Trigger.isUpdate) {
        //Get opportunity ids and milestone names and build reference
        Set<Id> oppids = new Set<Id>();
        Set<String> sm_types = new Set<String>();
        
        for(Schedule_Milestone__c milestone : Trigger.new) {
            oppids.add(milestone.Opportunity__c);
            sm_types.add(milestone.Milestone_Type__c);
        }   
        
        //Get assignments related to these opportunity / milestones
        List<ResourceHeroApp__Resource_Assignment__c> ras = [SELECT Id, Name, ResourceHeroApp__Opportunity__c, End_Milestone__c, Start_Milestone__c, ResourceHeroApp__End_Date__c, ResourceHeroApp__Start_Date__c
                                                             FROM ResourceHeroApp__Resource_Assignment__c
                                                             WHERE ResourceHeroApp__Opportunity__c in :oppids
                                                             AND (Start_Milestone__c in :sm_types OR End_Milestone__c in :sm_types)];
        
        //For each assignment either update flag or add to reference for delete processing
        for(ResourceHeroApp__Resource_Assignment__c ra : ras) {
            ra.Requires_Milestone_Processing__c = true;
        }
        
        update ras;
    }
    else if(Trigger.isDelete) {
        //Get opportunity ids and milestone names and build reference
        Set<Id> oppids = new Set<Id>();
        Set<String> sm_types = new Set<String>();
        
        for(Schedule_Milestone__c milestone : Trigger.old) {
            oppids.add(milestone.Opportunity__c);
            sm_types.add(milestone.Milestone_Type__c);
        }   
        
        //Get assignments related to these opportunity / milestones
        List<ResourceHeroApp__Resource_Assignment__c> ras = [SELECT Id, Name, ResourceHeroApp__Opportunity__c, End_Milestone__c, Start_Milestone__c, ResourceHeroApp__End_Date__c, ResourceHeroApp__Start_Date__c
                                                             FROM ResourceHeroApp__Resource_Assignment__c
                                                             WHERE ResourceHeroApp__Opportunity__c in :oppids
                                                             AND (Start_Milestone__c in :sm_types OR End_Milestone__c in :sm_types)];
        
        //For each assignment either update flag or add to reference for delete processing
        Set<String> preventdeletes = new Set<String>();
        for(ResourceHeroApp__Resource_Assignment__c ra : ras) {
            if(ra.Start_Milestone__c != null)
                preventdeletes.add(ra.ResourceHeroApp__Opportunity__c + '-' + ra.Start_Milestone__c);
            
            if(ra.End_Milestone__c != null)
                preventdeletes.add(ra.ResourceHeroApp__Opportunity__c + '-' + ra.End_Milestone__c);
        }
        
        for(Schedule_Milestone__c milestone : Trigger.old) {
            if(preventdeletes.contains(milestone.Opportunity__c + '-' + milestone.Milestone_Type__c))
                milestone.addError('You cannot delete this milestone as it is currently referenced in an assignment on the related opportunity.');
        }   
    }
    */
}