/**************************************************************************************
Name: Schedule_Milestone_Trigger
Version: 1.0 
Created Date: 03.04.2017
Function: Schedule_Milestone__c Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    03.04.2017      Original Version
* Manuel Johnson    20.07.2017      Update to set the Opportunity RFP and Proposal Due Date fields on create, edit or delete (US725)
* Madhu Shetty      15.05.2018      Added validation to check only one Award Milestone is added for each opportunity (US1892)
* Madhu Shetty      10.09.2018      Added beforedelete trigger to stop users from deleting auto created milestones (US2467)
* Scott Stone       13.09.2018      Added beforeinsert trigger to set the read only record type when milestone is auto created (US2467)
*************************************************************************************/

trigger Schedule_Milestone_Trigger on Schedule_Milestone__c (before insert, before update, before delete, after insert, after update, after delete) {
    if(TriggerUtil.isTriggerActive('Schedule_Milestone_Trigger')){
        
        // Run insert specific actions and upsert actions.
        if(Trigger.isBefore && Trigger.isInsert){
            Schedule_Milestone_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
            Schedule_Milestone_TriggerHandler.getHandler().onBeforeUpsert(Trigger.new);
        }
        
        // Validate Milestone before insert and update
        if(Trigger.isBefore && Trigger.isUpdate){
            Schedule_Milestone_TriggerHandler.getHandler().onBeforeUpsert(Trigger.new);
        }
        
        // Updates the parent Opportunity Milestone Date fields on delete
        if(Trigger.isBefore && Trigger.isDelete){
            Schedule_Milestone_TriggerHandler.getHandler().onBeforeDelete(Trigger.old);
        }
        
        // Updates the parent Opportunity Milestone Date fields on upsert
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            Schedule_Milestone_TriggerHandler.getHandler().onAllChanges(Trigger.new);
        }

        // Updates the parent Opportunity Milestone Date fields on delete
        if(Trigger.isAfter && Trigger.isDelete){
            Schedule_Milestone_TriggerHandler.getHandler().onAllChanges(Trigger.old);
        }
    }
    
}