/**************************************************************************************
Name: Account_Trigger
Version: 1.0 
Created Date: 27.02.2017
Function: Account Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           27.02.2017         Original Version
* Madhu Shetty              08.11.2017         (US654) - Added validation to block deletion of Account if an associated 
                                               Opportunity exists for that Account.
*************************************************************************************/
trigger Account_Trigger on Account (before insert, before delete, after insert, after update, after delete) {

    if(TriggerUtil.isTriggerActive('Account_Trigger')){
      /*  if(Trigger.isBefore && Trigger.isInsert){
            Account_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        } 
      */  
        
        if(Trigger.isAfter && Trigger.isInsert){
            Account_TriggerHandler.getHandler().onAfterInsert(Trigger.newMap);
        }
    
        if(Trigger.isAfter && Trigger.isUpdate){
            Account_TriggerHandler.getHandler().onAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            Account_TriggerHandler.getHandler().onAfterUpsert(Trigger.new, Trigger.old, Trigger.isInsert);
        }
       
        if(Trigger.isAfter && Trigger.isDelete){
            Account_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }
        
      /* SIVA KATNENI - Commenting out as the account delete scenario is not being handled now
        if(Trigger.isAfter && Trigger.isDelete){
            Account_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }
       */
   }
}