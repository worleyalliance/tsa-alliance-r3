/**************************************************************************************
Name: Partner_Trigger
Version: 1.0 
Created Date: 10/04/2018
Function:Partner Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer             Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna       05/03/2018      Original Version
*************************************************************************************/

trigger Partner_Trigger on Partner__c (after insert, after update, after delete) {
    if(TriggerUtil.isTriggerActive('Partner_Trigger')){
        if(Trigger.isAfter && Trigger.isInsert) { 
            Partner_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
        }  
        
        if(Trigger.isAfter && Trigger.isUpdate) { 
            Partner_TriggerHandler.getHandler().onAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }   
        
        if(Trigger.isAfter && Trigger.isDelete) { 
            Partner_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }    
    }
}