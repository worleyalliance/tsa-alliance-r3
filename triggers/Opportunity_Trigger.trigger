/**************************************************************************************
Name: Opportunity_Trigger
Version: 1.0 
Created Date: 08/04/2017
Function:Opportunity Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    08/04/2017      Original Version
*************************************************************************************/
trigger Opportunity_Trigger on Opportunity (before insert, before update, after insert, after update) {
    
    if(TriggerUtil.isTriggerActive('Opportunity_Trigger')){  
        
        // The handler method to execute steps before insert
        if(Trigger.isBefore && Trigger.isInsert){
            Opportunity_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }  
        
        //The handler method call to execute steps before update
        if(Trigger.isBefore && Trigger.isUpdate){
            Opportunity_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }  
        
        //The handler method call to execute steps after insert
        if(Trigger.isAfter && Trigger.isInsert){
            Opportunity_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
        }  
        
        //The handler method call to execute steps after update
        if(Trigger.isAfter && Trigger.isUpdate){
            Opportunity_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
        }    
        
    }  
    
}