/**************************************************************************************
Name: ContentDocument_Trigger
Version: 1.0
Created Date: 12/05/2017
Function: Trigger for ContentDocument to execute actions on file deletion

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    12/05/2017      Original Version
*************************************************************************************/
trigger ContentDocument_Trigger on ContentDocument (after delete, after undelete) {
    
    if(TriggerUtil.isTriggerActive('ContentDocumentLink_Trigger')){
        if(Trigger.isAfter && Trigger.isDelete){
            ContentDocument_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
        }
           
        if(Trigger.isAfter && Trigger.isUndelete){
            ContentDocument_TriggerHandler.getHandler().onAfterDelete(Trigger.new);
        }
    }
    
}