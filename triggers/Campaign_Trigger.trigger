/**************************************************************************************
Name: Campaign_Trigger
Version: 1.0 
Created Date: 11/21/2017
Function:Campaign Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    11/21/2017      Original Version
* Pradeep Shetty    12/23/2018      US2537: Moving workflow logic for 'Active' field to trigger
*************************************************************************************/
trigger Campaign_Trigger on Campaign (after insert, after update, before insert) {

  if(TriggerUtil.isTriggerActive('Campaign_Trigger')){  

    //The handler method call to execute steps before insert
    if(Trigger.isbefore && Trigger.isInsert){
        Campaign_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
    } 

    //The handler method call to execute steps after insert
    if(Trigger.isAfter && Trigger.isInsert){
        Campaign_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
    }  

    //The handler method call to execute steps after update
    if(Trigger.isAfter && Trigger.isUpdate){
        Campaign_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
    }    

  }  

}