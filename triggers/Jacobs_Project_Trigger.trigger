/**************************************************************************************
Name: Jacobs_Project_Trigger
Version: 1.0 
Created Date: 04.20.2017
Function: Jacobs Project Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04.20.2017      Original Version
*************************************************************************************/
trigger Jacobs_Project_Trigger on jacobs_project__c (before insert, before update, after insert, after update, after delete, after undelete) {
    
  if(TriggerUtil.isTriggerActive('Jacobs_Project_Trigger')){

    //Before Insert 
    if(Trigger.isBefore && Trigger.isInsert){
        Jacobs_Project_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
    }

    //Before Update
    if(Trigger.isBefore && Trigger.isUpdate){
        Jacobs_Project_TriggerHandler.getHandler().onBeforeUpdate(Trigger.new);
    }

    //After Insert
    if(Trigger.isAfter && Trigger.isInsert){
        Jacobs_Project_TriggerHandler.getHandler().onAfterInsert(Trigger.new);
    }
    
    //AfterUpdate  
    if(Trigger.isAfter && Trigger.isUpdate){
        Jacobs_Project_TriggerHandler.getHandler().onAfterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
    }
    
    //AfterDelete
    if(Trigger.isAfter && Trigger.isDelete){
        Jacobs_Project_TriggerHandler.getHandler().onAfterDelete(Trigger.old);
    }

    //AfterUndelete
    if(Trigger.isAfter && Trigger.isUnDelete){
        Jacobs_Project_TriggerHandler.getHandler().onAfterUnDelete(Trigger.new);
    }

  }
}