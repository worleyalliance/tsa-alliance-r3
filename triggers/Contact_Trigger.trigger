/**************************************************************************************
Name: Contact_Trigger
Version: 1.0 
Created Date: 06.06.2017
Function: Contact Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           06.06.2017         Original Version
*************************************************************************************/
trigger Contact_Trigger on Contact (before insert) {
    if(TriggerUtil.isTriggerActive('Contact_Trigger')){
        if(Trigger.isInsert && Trigger.isInsert){
            Contact_TriggerHandler.getHandler().onBeforeInsert(Trigger.new);
        }
    }
}