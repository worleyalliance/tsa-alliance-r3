<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Request_SharePoint_Site</fullName>
        <description>Request SharePoint Site</description>
        <protected>false</protected>
        <recipients>
            <field>SharePoint_Site_Admin__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SharePoint_Site_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Schedule_Milestone_Status</fullName>
        <description>Updates the Status field to &apos;Past Due&apos;</description>
        <field>Status__c</field>
        <literalValue>Past Due</literalValue>
        <name>Update Schedule Milestone Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Mark Schedule Milestone Past Due</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Schedule_Milestone__c.Status__c</field>
            <operation>equals</operation>
            <value>Upcoming</value>
        </criteriaItems>
        <description>This workflow triggers a time dependent action to change the Milestone status to &apos;Past Due&apos; if the due date has passed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Schedule_Milestone_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Schedule_Milestone__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SharePoint Site Request</fullName>
        <actions>
            <name>Request_SharePoint_Site</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Schedule_Milestone__c.Request_SharePoint_Site__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends email alert to request SharePoint access.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
