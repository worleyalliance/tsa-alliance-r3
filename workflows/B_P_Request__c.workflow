<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>B_P_Request_is_Approved</fullName>
        <description>B&amp;P Request is Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>B_P_Accountant</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_P_Requests/B_P_Request_Submitted</template>
    </alerts>
    <alerts>
        <fullName>CH2M_BP_Request_Approved</fullName>
        <description>CH2M BP Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>B_P_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_P_Requests/CH2M_B_P_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>CH2M_BP_Revision_Approved</fullName>
        <description>CH2M BP Revision Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>B_P_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_P_Requests/CH2M_B_P_Revision_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Notify_B_P_Owner_that_B_P_Request_has_been_approved</fullName>
        <description>Notify B&amp;P Owner that B&amp;P Request has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Sales_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>B_P_Requests/B_P_Request_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Verification_Check</fullName>
        <field>Account_Verification_Check__c</field>
        <literalValue>1</literalValue>
        <name>Account Verification Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Verification_UnCheck</fullName>
        <field>Account_Verification_Check__c</field>
        <literalValue>0</literalValue>
        <name>Account Verification UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_B_P_status_to_Draft</fullName>
        <description>Changes B&amp;P status to draft</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Change B&amp;P status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_B_P_status_to_Submitted</fullName>
        <description>Changes the B&amp;P status to Submitted</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Change B&amp;P status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_BP_Confidential_to_TRUE</fullName>
        <field>Confidential__c</field>
        <literalValue>1</literalValue>
        <name>Set BP Confidential to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_B_P_Status_to_Draft</fullName>
        <description>Updates B&amp;P status to Draft</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update B&amp;P Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_B_P_record_type_to_B_P_Request</fullName>
        <description>Updates B&amp;P record type to &apos;B&amp;P request&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>B_P_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update B&amp;P record type to &apos;B&amp;P Request&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_B_P_type_to_CH2M_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CH2M_Approved_B_P_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update B&amp;P type to &apos;CH2M Approved&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_B_P_type_to_Manual_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Manual_Approved_B_P_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update B&amp;P type to &apos;Manual Approved&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_B_P_type_to_Manual_B_P_Request</fullName>
        <description>Updates B&amp;P record type to &apos;Manual B&amp;P request&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Manual_B_P_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update B&amp;P type to &apos;Manual B&amp;P Request&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Original_B_P_Budget_USD</fullName>
        <field>Original_B_P_Budget_USD__c</field>
        <formula>IF( ISBLANK(Original_B_P__c), 0, Original_B_P__r.BP_Budget__c)</formula>
        <name>Update Original B&amp;P Budget (USD)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <description>Changes the status to Approved when all approvers approve the request</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <description>Updates the status to rejected when the approvers reject the request</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_to_CH2M_B_P_Request</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CH2M_B_P_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type to &apos;CH2M B&amp;P Request&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>B%26P CH2M Request Approved</fullName>
        <actions>
            <name>CH2M_BP_Request_Approved</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_B_P_type_to_CH2M_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Draft,Rejected,Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>CH2M</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>notContain</operation>
            <value>Revision</value>
        </criteriaItems>
        <description>When a B&amp;P record has been approved, lock the record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B%26P CH2M Revision Approved</fullName>
        <actions>
            <name>CH2M_BP_Revision_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Draft,Rejected,Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CH2M B&amp;P Revision</value>
        </criteriaItems>
        <description>When a B&amp;P revision record has been approved, lock the record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B%26P Manual Request Approved</fullName>
        <actions>
            <name>B_P_Request_is_Approved</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_B_P_type_to_Manual_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>India Only Manual B&amp;P Request</value>
        </criteriaItems>
        <description>When a B&amp;P record has been approved, lock the record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B%26P Request Approved and Active</fullName>
        <actions>
            <name>Notify_B_P_Owner_that_B_P_Request_has_been_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.Project_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a B&amp;P record has been approved and is active, send a notification to the owner of the B&amp;P Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B%26P Request marked Confidential based on Account or Opportunity</fullName>
        <actions>
            <name>Set_BP_Confidential_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Mark B&amp;P Request as confidential when account/opportunity is confidential</description>
        <formula>OR( (Client__r.Confidential__c = true), (Opportunity__r.Confidential__c =true))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Rejected B%26P to Draft on Update</fullName>
        <actions>
            <name>Update_B_P_Status_to_Draft</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>When a Rejected B&amp;P is updated set the status to Draft.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update B%26P Record Type on create</fullName>
        <actions>
            <name>Update_B_P_record_type_to_B_P_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New B&amp;P Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.Manual_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Updates the B&amp;P record type to B&amp;P request when a &apos;New B&amp;P Request&apos; is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update CH2M B%26P Record Type on create</fullName>
        <actions>
            <name>Update_record_type_to_CH2M_B_P_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New CH2M B&amp;P Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.Manual_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the CH2M B&amp;P record type to CH2M B&amp;P request when a &apos;New CH2M B&amp;P Request&apos; is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Manual B%26P Record Type on create</fullName>
        <actions>
            <name>Update_B_P_type_to_Manual_B_P_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>B_P_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft,Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.Manual_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>B_P_Request__c.RecordTypeId</field>
            <operation>notContain</operation>
            <value>CH2M</value>
        </criteriaItems>
        <description>Updates the record type to Manual B&amp;P Request when a &apos;New B&amp;P Request&apos; is created for an ECR Selling Unit</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
