<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Verified</fullName>
        <description>Account Verified</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Stewards_Folder/Account_Verified</template>
    </alerts>
    <fieldUpdates>
        <fullName>CH2M_Status_Unverified</fullName>
        <description>CH2M Status to Unverified</description>
        <field>CH2M_Status__c</field>
        <literalValue>Unverified</literalValue>
        <name>CH2M Status Unverified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Record_Type_to_Client</fullName>
        <description>Field update to change Account record type to &apos;Client&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Client</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Account Record Type to Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Jacobs_Status_Unverified</fullName>
        <description>to set Jacobs Status to Unverified</description>
        <field>Status__c</field>
        <literalValue>Unverified</literalValue>
        <name>Jacobs Status Unverified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_at_Creation_to_Client</fullName>
        <field>Record_Type_at_Creation__c</field>
        <literalValue>Client</literalValue>
        <name>Set Record Type at Creation to Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_at_Creation_to_Competito</fullName>
        <field>Record_Type_at_Creation__c</field>
        <literalValue>Competitor</literalValue>
        <name>Set Record Type at Creation to Competito</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_at_Creation_to_Partner</fullName>
        <field>Record_Type_at_Creation__c</field>
        <literalValue>Partner</literalValue>
        <name>Set Record Type at Creation to Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Lead_User_Role</fullName>
        <field>Account_Lead_User_Role__c</field>
        <formula>Owner.UserRole.DeveloperName</formula>
        <name>Update Account Lead User Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Record_Type_to_Universal</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Universal</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Account Record Type to Universal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Lead Modified</fullName>
        <actions>
            <name>Update_Account_Lead_User_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Account Lead is modified execute the actions</description>
        <formula>OR( ISNEW(), ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Verified</fullName>
        <actions>
            <name>Account_Verified</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OracleAccountID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CH2M_Oracle_Account_ID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Account Record Type to Client</fullName>
        <actions>
            <name>Change_Account_Record_Type_to_Client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Client</value>
        </criteriaItems>
        <description>This workflow rule changes the Account record type from &apos;New Client&apos; to Client so that the user can see all fields instead of a subset of fields.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Client Created</fullName>
        <actions>
            <name>Set_Record_Type_at_Creation_to_Client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>When an Account is created with the Record Type = Client, set the Record Type at Creation field to Client</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Competitor Created</fullName>
        <actions>
            <name>Set_Record_Type_at_Creation_to_Competito</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <description>When an Account is created with the Record Type = Competitor, set the Record Type at Creation field to Competitor</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OracleAccountCreationChecked</fullName>
        <actions>
            <name>CH2M_Status_Unverified</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Jacobs_Status_Unverified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reset Status__c and CH2M_Status__c to unverified when Do not create Oracle Account unchecked</description>
        <formula>ISCHANGED( Do_Not_Create_Oracle_Account__c ) &amp;&amp;  (Do_Not_Create_Oracle_Account__c  = False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Partner Created</fullName>
        <actions>
            <name>Set_Record_Type_at_Creation_to_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>When an Account is created with the Record Type = Partner, set the Record Type at Creation field to Partner</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Universal Account</fullName>
        <actions>
            <name>Update_Account_Record_Type_to_Universal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 4) AND (2 OR 5) AND (7)) OR ((1 OR 4) AND (3 OR 6) AND (7)) OR ((2 OR 5) AND (3 OR 6) AND (7))</booleanFilter>
        <criteriaItems>
            <field>Account.of_Opportunities__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.of_Times_is_Competitor__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.of_Times_is_Partner__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_at_Creation__c</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_at_Creation__c</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_at_Creation__c</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Internal</value>
        </criteriaItems>
        <description>Updates the Account Record Type based on the following logic: if (1) # of Opportunities &gt; 0 or (2) # of times is a competitor &gt; 0 or (3) # of times is a partner &gt; 0</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
