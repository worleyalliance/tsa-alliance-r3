<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateFieldEndDate</fullName>
        <field>PF_TC_End_Date__c</field>
        <formula>TODAY()</formula>
        <name>UpdateFieldEndDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Test_Case_End_date</fullName>
        <field>PF_TC_End_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Test Case End date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PF Update Test Case End date For Fail</fullName>
        <actions>
            <name>Update_Test_Case_End_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_TestCases__c.PF_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <description>This is used to update Test Case end date when status changes to failed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PF Update Test Case End date For Pass</fullName>
        <actions>
            <name>UpdateFieldEndDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_TestCases__c.PF_Status__c</field>
            <operation>equals</operation>
            <value>Passed</value>
        </criteriaItems>
        <description>This is used to update Test Case end date when status changes to passed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
