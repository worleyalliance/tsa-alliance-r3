<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Field_Update_Campaign_Active</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Field Update Campaign Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Campaign_Name</fullName>
        <description>Standardize Campaign Name</description>
        <field>Name</field>
        <formula>TEXT(pl_fiscal_year__c)&amp;&quot;-Sales Plan-&quot;&amp;
LEFT(IF(AND(ISBLANK(TEXT(pl_selling_unit__c))=FALSE,
TEXT(pl_selling_unit__c)&lt;&gt;&quot;NA&quot;),
TEXT(pl_selling_unit__c),
IF(AND(ISBLANK(TEXT(pl_business_unit__c))=FALSE,
TEXT(pl_business_unit__c)&lt;&gt;&quot;NA&quot;),
TEXT(pl_business_unit__c),
TEXT(pl_line_of_business__c))),68)</formula>
        <name>Field Update Campaign Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Campaign_Status</fullName>
        <description>Set Campaign Status to Approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Field Update Campaign Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_In_Review</fullName>
        <description>Set Field Update Status to In Review</description>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Field Update In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Sales_Plan_Draft</fullName>
        <description>Set Sales Plan Status to Draft</description>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Field Update Sales Plan Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Campaign_Name_on_Save</fullName>
        <field>Name</field>
        <formula>TEXT(pl_fiscal_year__c)&amp; &quot;-&quot; &amp; &quot;Sales Plan&quot; &amp; &quot;-&quot; &amp; IF(ISPICKVAL(pl_selling_unit__c,&quot;&quot;), TEXT(pl_business_unit__c), TEXT(pl_selling_unit__c))</formula>
        <name>Update Campaign Name on Save</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Campaign Name on Save</fullName>
        <actions>
            <name>Update_Campaign_Name_on_Save</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WFR Active Campaign</fullName>
        <actions>
            <name>Field_Update_Campaign_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Set the Campaign Active status to True upon create</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WFR Campaign Name</fullName>
        <actions>
            <name>Field_Update_Campaign_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.cb_sales_plan_locked__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Standardize the Campaign Name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
