<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Initiate_PAP_Process_Notification</fullName>
        <description>Initiate PAP Process Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Inside Sales Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Operations Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Won</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Owner_that_an_OCI_Case_has_not_been_closed</fullName>
        <description>Notify Opportunity Owner that an OCI Case has not been closed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OCI_Folder/OCI_Case_Not_Closed</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Parent_Account_Unverified</fullName>
        <description>Opportunity Parent Account Unverified</description>
        <protected>false</protected>
        <recipients>
            <recipient>Inside Sales Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Parent_Account_Unverified</template>
    </alerts>
    <alerts>
        <fullName>Send_A_T_Closed_Won_email_notification</fullName>
        <description>Send ATN Closed Won email notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>A_T_Booked_Opportunity_Review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Closed_Won</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Auto_Create_Contract_Program_to_N</fullName>
        <field>Auto_Create_Contract_Program__c</field>
        <literalValue>No</literalValue>
        <name>Update Auto Create Contract/Program to N</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Auto_Create_Contract_Program_to_Y</fullName>
        <field>Auto_Create_Contract_Program__c</field>
        <literalValue>Yes</literalValue>
        <name>Update Auto Create Contract/Program to Y</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Prob_to_100</fullName>
        <description>Update Closed Won Probability to 100</description>
        <field>Probability</field>
        <formula>1</formula>
        <name>Update CW Prob to 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date_to_TODAY</fullName>
        <field>CloseDate</field>
        <formula>IF(ISCHANGED( StageName ),TODAY(), CloseDate )</formula>
        <name>Update Close Date to TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Get_to_100_for_CW</fullName>
        <description>Update the Get to 100 for Closed Won Opportunities</description>
        <field>Get__c</field>
        <formula>1</formula>
        <name>Update Get to 100 for CW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Go_20</fullName>
        <field>Go__c</field>
        <formula>20/100</formula>
        <name>Update Go% = 20%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Go_66</fullName>
        <field>Go__c</field>
        <formula>66/100</formula>
        <name>Update Go% = 66%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Go_95</fullName>
        <field>Go__c</field>
        <formula>95/100</formula>
        <name>Update Go% = 95%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Go_To_100_for_CW</fullName>
        <description>Update the Go to 100 for CW</description>
        <field>Go__c</field>
        <formula>1</formula>
        <name>Update Go To 100 for CW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NewCo_Approved_to_False</fullName>
        <field>NewCo_Synergy_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Update NewCo Approved to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NewCo_Not_Approved_to_False</fullName>
        <field>NewCo_Synergy_Not_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Update NewCo Not Approved to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_to_Confidential</fullName>
        <field>Confidential__c</field>
        <literalValue>1</literalValue>
        <name>Update Opp to Confidential</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Duration</fullName>
        <field>Duration_Months__c</field>
        <formula>Number_of_Months_Calculated__c</formula>
        <name>Update Opportunity Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Gross_Margin</fullName>
        <field>Amount</field>
        <formula>Gross_Margin_Calculated__c</formula>
        <name>Update Opportunity Gross Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Revenue</fullName>
        <field>Revenue__c</field>
        <formula>Revenue_Calculated__c</formula>
        <name>Update Opportunity Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Start_Date</fullName>
        <field>Target_Project_Start_Date__c</field>
        <formula>Start_Date_Calculated__c</formula>
        <name>Update Opportunity Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Work_Hours</fullName>
        <field>Work_Hours__c</field>
        <formula>Work_Hours_Calculated__c</formula>
        <name>Update Opportunity Work Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Probability</fullName>
        <field>Probability</field>
        <formula>Go__c  *  Get__c</formula>
        <name>Update Probability</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Full_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Booking_Value</fullName>
        <description>Update Opportunity record type to Booking Value</description>
        <field>RecordTypeId</field>
        <lookupValue>Booking_Value</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Booking Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Full_Opportunity</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Full_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Full Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_to_Closed_Won_BV</fullName>
        <description>Update record type of Booking Value to Closed Won Booking Value</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Won_Booking_Value</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type to Closed Won BV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Opp_Record_Type_to_Closed_Won</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Won</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Updated Opp Record Type to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>A%26T End Game without closed OCI</fullName>
        <actions>
            <name>Notify_Opportunity_Owner_that_an_OCI_Case_has_not_been_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ATN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>EG - Propose,EG - Select,Closed - Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OCI_Case_Status__c</field>
            <operation>equals</operation>
            <value>None</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OCI_Case_Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>When an ATN Opportunity goes past Middle Game Stage without a closed OCI Case, send a notification to the Opportunity Owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>A%26T Opportunity Closed Won</fullName>
        <actions>
            <name>Send_A_T_Closed_Won_email_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ATN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Forecast_Category__c</field>
            <operation>equals</operation>
            <value>Include</value>
        </criteriaItems>
        <description>When an ATN Opportunity is marked as Closed-Won send an email alert to the ATN Opportunity Review public group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NewCo Synergy Updated</fullName>
        <actions>
            <name>Update_NewCo_Approved_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_NewCo_Not_Approved_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the NewCo Synergy fields are updated, reset the approved or not approved flag.</description>
        <formula>AND( OR(PRIORVALUE(NewCo_Synergy_Approved__c) == True ,PRIORVALUE( NewCo_Synergy_Not_Approved__c) == True),      OR( ISCHANGED(NewCo_Pre_Synergy_Pwin_Get__c),  ISCHANGED(NewCo_Pwin_Get_Change__c), ISCHANGED(NewCo_Revenue_Change__c), ISCHANGED(NewCo_GM_Change__c) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Class I</fullName>
        <actions>
            <name>Update_Go_95</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Class_A_T__c</field>
            <operation>equals</operation>
            <value>Class I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Go__c</field>
            <operation>notEqual</operation>
            <value>95</value>
        </criteriaItems>
        <description>When an Opportunity is created or edited with the Opportunity Class (A&amp;T) field populated, set the Go % field to 95% for Class I, or 66% for Class II, 20% for Class III.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Class II</fullName>
        <actions>
            <name>Update_Go_66</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Class_A_T__c</field>
            <operation>equals</operation>
            <value>Class II</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Go__c</field>
            <operation>notEqual</operation>
            <value>66</value>
        </criteriaItems>
        <description>When an Opportunity is created or edited with the Opportunity Class (A&amp;T) field populated, set the Go % field to 95% for Class I, or 66% for Class II, 20% for Class III.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Class III</fullName>
        <actions>
            <name>Update_Go_20</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Class_A_T__c</field>
            <operation>equals</operation>
            <value>Class III</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Go__c</field>
            <operation>notEqual</operation>
            <value>20</value>
        </criteriaItems>
        <description>When an Opportunity is created or edited with the Opportunity Class (A&amp;T) field populated, set the Go % field to 95% for Class I, or 66% for Class II, 20% for Class III.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Closed</fullName>
        <actions>
            <name>Update_Close_Date_to_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to update the Close Date to Today when Status is marked to a closed Stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Closed Won with Forecast Included</fullName>
        <actions>
            <name>Initiate_PAP_Process_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Forecast_Category__c</field>
            <operation>equals</operation>
            <value>Include</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Executing_Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>ATN</value>
        </criteriaItems>
        <description>When an Opportunity that is included in the forecast is changed to &quot;Closed - Won&quot;, send an email to initiate the PAP.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Duration Updated</fullName>
        <actions>
            <name>Update_Opportunity_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Duration Calculated Roll Up field is updated, update the Opportunity Duration field.</description>
        <formula>ISCHANGED( Number_of_Months_Calculated__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Gross Margin Updated</fullName>
        <actions>
            <name>Update_Opportunity_Gross_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Gross Margin Calculated Roll Up field is updated, update the Opportunity Gross Margin field.</description>
        <formula>ISCHANGED(Gross_Margin_Calculated__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Reaches EG Stage</fullName>
        <actions>
            <name>Opportunity_Parent_Account_Unverified</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>EG - Propose</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>EG - Propose</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>EG - Select</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Unverified</value>
        </criteriaItems>
        <description>The first time an Opportunity goes from a non-EG Stage to an EG Stage, send an email alert if the parent Account is not verified.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Revenue Updated</fullName>
        <actions>
            <name>Update_Opportunity_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Revenue Calculated Roll Up field is updated, update the Opportunity Revenue field.</description>
        <formula>ISCHANGED(Revenue_Calculated__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Start Date Updated</fullName>
        <actions>
            <name>Update_Opportunity_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Start Date Calculated Roll Up field is updated, update the Opportunity Start Date field.</description>
        <formula>ISCHANGED( Start_Date_Calculated__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Tier Changed to 1-3</fullName>
        <actions>
            <name>Update_Auto_Create_Contract_Program_to_Y</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Tier is changed to 1-3, update the Auto Create Contract/Program field to Yes</description>
        <formula>AND( OR( ISPICKVAL(Opportunity_Tier__c, &quot;Tier 1&quot;), ISPICKVAL(Opportunity_Tier__c, &quot;Tier 2&quot;), ISPICKVAL(Opportunity_Tier__c, &quot;Tier 3&quot;) ), !CONTAINS(RecordType.Name, &quot;Booking Value&quot;), !CONTAINS(TEXT(Business_Unit__c), &quot;JASARA&quot;), !CONTAINS(TEXT(Business_Unit__c), &quot;JESA&quot;), NOT(ISPICKVAL(Auto_Create_Contract_Program__c, &quot;Yes&quot;)), OR( ISNEW(), ISCHANGED(Opportunity_Tier__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Tier Changed to 4</fullName>
        <actions>
            <name>Update_Auto_Create_Contract_Program_to_N</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the Opportunity Tier is changed to 4, update the Auto Create Contract/Program field to No</description>
        <formula>OR( CONTAINS(TEXT(Business_Unit__c), &quot;JASARA&quot;), CONTAINS(TEXT(Business_Unit__c), &quot;JESA&quot;), AND( ISPICKVAL(Opportunity_Tier__c, &quot;Tier 4&quot;), OR( ISNEW(), AND( ISCHANGED(Opportunity_Tier__c), !ISCHANGED(Auto_Create_Contract_Program__c) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Work Hours Updated</fullName>
        <actions>
            <name>Update_Opportunity_Work_Hours</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Opportunity Work Hours Calculated Roll Up field is updated, update the Opportunity Work Hours field.</description>
        <formula>ISCHANGED( Work_Hours_Calculated__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity marked confidential based on account</fullName>
        <actions>
            <name>Update_Opp_to_Confidential</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Confidential__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Account is marked &quot;Confidential&quot; the related Opportunities are also marked &quot;Confidential&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Booking Value Record Type to Closed Won - Booking Value</fullName>
        <actions>
            <name>Update_record_type_to_Closed_Won_BV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Booking Value</value>
        </criteriaItems>
        <description>Updates the Booking Value record type to the &quot;Closed Won - Booking Value&quot; record type when the Booking Value stage is changed to &quot;Closed - Won&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Booking Value Record Type when Reopening Booking Value</fullName>
        <actions>
            <name>Update_Record_Type_to_Booking_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Closed Won - Booking Value</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <description>Updates Booking Value record type to &quot;Booking Value&quot; when a Closed Won Boking Value is reopened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Record Type to Closed Won</fullName>
        <actions>
            <name>Updated_Opp_Record_Type_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Full Opportunity</value>
        </criteriaItems>
        <description>Updates the Opportunity record type to the &quot;Closed - Won&quot; record type when the Opportunity stage is changed to &quot;Closed - Won&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Record Type when Reopening Opp</fullName>
        <actions>
            <name>Update_Record_Type_to_Full_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <description>Updates Opportunity record type to &quot;Full Opportunity&quot; when a Closed Won Opportunity is reopened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Probability</fullName>
        <actions>
            <name>Update_Probability</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Go__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Get__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Opportunity Probability is updated to Go% x Get%</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Probability To 100 for CW</fullName>
        <actions>
            <name>Update_CW_Prob_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Get_to_100_for_CW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Go_To_100_for_CW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Opportunity Prob to 100 when Closed Won</description>
        <formula>AND(ISCHANGED(StageName),ISPICKVAL(StageName,&quot;Closed - Won&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update to Full Layout</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Opportunity</value>
        </criteriaItems>
        <description>When a New Opportunity is created the record type changes to &quot;Full Opportunity&quot; so that a new page layout is assigned.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
