<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_finance_manager_when_survey_is_marked_as_High_Risk</fullName>
        <description>Send email to finance manager when survey is marked as High Risk</description>
        <protected>false</protected>
        <recipients>
            <recipient>avinash.gaikwad@pwc.com.wpprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Survey/email_finance_high_risk</template>
    </alerts>
    <fieldUpdates>
        <fullName>Field_Update_Opportunity_ID</fullName>
        <description>Updates Opportunity ID from related Opportunity Record</description>
        <field>txt_opportunity_id__c</field>
        <formula>lr_opportunity__c</formula>
        <name>Field Update Opportunity ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Partner_Name</fullName>
        <description>Field update to pull partner name from account record</description>
        <field>txt_partner_name__c</field>
        <formula>lr_partner__r.Name</formula>
        <name>Field Update Partner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Partner_Survey_Name</fullName>
        <description>Updates and standardizes the partner survey name</description>
        <field>Name</field>
        <formula>LEFT(lr_partner__r.Name,68) &amp; &quot;-DCAA Survey&quot;</formula>
        <name>Field Update Partner Survey Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Date_Verified</fullName>
        <description>Update DCAA Survey Verified Date to Today</description>
        <field>dt_dcaa_verified__c</field>
        <formula>TODAY()</formula>
        <name>Record Date Verified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>field_update_verified_false</fullName>
        <description>Set DCAA Verified Field  = False</description>
        <field>cb_finalization__c</field>
        <literalValue>0</literalValue>
        <name>Set Verified to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>wfr_dcaa_date_verified</fullName>
        <actions>
            <name>Record_Date_Verified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Survey__c.cb_finalization__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>field_update_verified_false</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Partner_Survey__c.CreatedDate</offsetFromField>
            <timeLength>183</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>wfr_partner_survey_field_update</fullName>
        <actions>
            <name>Field_Update_Opportunity_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Field_Update_Partner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Field_Update_Partner_Survey_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow Rule to update the opportunity ID and partner name fields.</description>
        <formula>ISBLANK(txt_partner_name__c) ||   ISBLANK(txt_opportunity_id__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>wfr_send_email_to_finance</fullName>
        <actions>
            <name>Send_email_to_finance_manager_when_survey_is_marked_as_High_Risk</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>email_sent_to_finance_manager_high_risk</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Sends email to finance person when DCAA survey is High Risk</description>
        <formula>us_fm_risk_rating__c = &apos;High Risk&apos; &amp;&amp; ISCHANGED(us_fm_risk_rating__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>email_sent_to_finance_manager_high_risk</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>email sent to finance manager - high risk</subject>
    </tasks>
</Workflow>
