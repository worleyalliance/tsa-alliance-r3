<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Project_Id_FY_Key</fullName>
        <field>Project_ID_FY_Key__c</field>
        <formula>Project_ID__c + Fiscal_Year__c</formula>
        <name>Update Project Id FY Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Calculate Project Id FY Key</fullName>
        <actions>
            <name>Update_Project_Id_FY_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Calculates the Project Id FY Key when the Project ID is populated</description>
        <formula>RecordType.DeveloperName = &apos;Finance_YTD&apos; &amp;&amp; NOT(ISBLANK(Project_ID__c)) &amp;&amp; NOT(ISBLANK(Fiscal_Year__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Email_to_PM_for_updates_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email to PM for updates sent</subject>
    </tasks>
    <tasks>
        <fullName>initial_project_creation_update_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>initial project creation update email sent</subject>
    </tasks>
</Workflow>
