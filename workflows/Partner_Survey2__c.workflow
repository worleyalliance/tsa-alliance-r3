<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>cas_update_name</fullName>
        <field>Name</field>
        <formula>Left(mdr_partner_survey__r.lr_partner__r.Name, 69)  &amp; &quot;-CAS Survey&quot;</formula>
        <name>CAS Update Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>wfr_cas_survery_name</fullName>
        <actions>
            <name>cas_update_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_Survey2__c.CreatedDate</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
