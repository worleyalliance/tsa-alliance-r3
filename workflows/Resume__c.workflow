<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_employee_after_verifying_resume</fullName>
        <description>Email employee after verifying resume</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Thank_you_updates_verification</template>
    </alerts>
    <alerts>
        <fullName>Email_employee_initially_after_HRIS_info_uploads_to_Salesforce</fullName>
        <description>Email employee initially after HRIS info uploads to Salesforce</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Initial_Resume_verfication</template>
    </alerts>
    <alerts>
        <fullName>Email_employee_initially_after_HRIS_info_uploads_to_Salesforce_1</fullName>
        <description>Email employee initially after HRIS info uploads to Salesforce.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Initial_Resume_verfication</template>
    </alerts>
    <alerts>
        <fullName>Email_employee_initially_after_HRIS_info_uploads_to_Salesforce_2</fullName>
        <description>Email employee initially after Resume has been created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Initial_Resume_verfication</template>
    </alerts>
    <alerts>
        <fullName>New_Email_Alert_for_Reminder</fullName>
        <description>New Email Alert for Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>New_Email_Reminder</fullName>
        <description>New Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Email_6_months_after_last_verified</fullName>
        <description>Reminder Email 6 months after last verified</description>
        <protected>false</protected>
        <recipients>
            <recipient>avinash.gaikwad@pwc.com.wpprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_Update_Your_Resume_Six_Months</fullName>
        <description>Reminder to Update Your Resume - Six Months</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_verify_Resume</fullName>
        <description>Reminder to verify Resume</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Verified_resume_info</template>
    </alerts>
    <alerts>
        <fullName>Sending_reminder_to_verify_Resume_14_days_after_initial_load</fullName>
        <description>Sending reminder to verify Resume 14 days after initial load</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Initial_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Sending_reminder_to_verify_Resume_28_days_after_initial_load</fullName>
        <description>Sending reminder to verify Resume 28 days after initial load</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Initial_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Sending_reminder_to_verify_Resume_28_days_after_initial_load_28day</fullName>
        <description>Sending reminder to verify Resume 28 days after initial load</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Initial_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Sends_initial_email_to_user_to_verify_resume_contents</fullName>
        <description>Sends initial email to user to verify resume contents.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Initial_Resume_verfication</template>
    </alerts>
    <alerts>
        <fullName>Six_Month_Reminder_to_Verify_Salesforce_Resume</fullName>
        <description>Six Month Reminder to Verify Salesforce Resume</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Six_month_reminder_email</fullName>
        <description>Six month reminder email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/RESUME_Verified_resume_info</template>
    </alerts>
    <alerts>
        <fullName>Twelve_Month_Reminder_to_Verify_Salesforce_Resume</fullName>
        <description>Twelve Month Reminder to Verify Salesforce Resume</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Twelve_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Welcome_to_the_Employee_Community</fullName>
        <description>Welcome to the Employee Community</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Employee_Join_the_community</template>
    </alerts>
    <alerts>
        <fullName>X012_month_email_reminder</fullName>
        <description>12 month email reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Twelve_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>X028_day_email_reminder</fullName>
        <description>28 day email reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/X28_day_Unverified_Resume</template>
    </alerts>
    <alerts>
        <fullName>X12_month_email_reminder</fullName>
        <description>12 month email reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Twelve_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>X12_month_reminder_to_verify_resume</fullName>
        <description>12 month reminder to verify resume</description>
        <protected>false</protected>
        <recipients>
            <recipient>avinash.gaikwad@pwc.com.wpprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Twelve_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>X14_day_email_reminder</fullName>
        <description>14 day email reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/X14_day_Unverified_Resume</template>
    </alerts>
    <alerts>
        <fullName>X28_day_email_reminder</fullName>
        <description>28 day email reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>avinash.gaikwad@pwc.com.wpprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/X28_day_Unverified_Resume</template>
    </alerts>
    <alerts>
        <fullName>X28_day_email_reminder_sent</fullName>
        <description>28 day email reminder sent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/X28_day_Unverified_Resume</template>
    </alerts>
    <alerts>
        <fullName>X6_month_email_reminder</fullName>
        <description>6 month email reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>email14</fullName>
        <description>email14</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/X14_day_Unverified_Resume</template>
    </alerts>
    <alerts>
        <fullName>send_6_month_email_alert</fullName>
        <description>send 6 month email alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Six_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>send_email_12_mos</fullName>
        <description>send email 12 mos</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Twelve_Month_Reminder</template>
    </alerts>
    <alerts>
        <fullName>send_initial_email</fullName>
        <description>send initial email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Resume_Email_Templates/Employee_Join_the_community</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Reminder_Date</fullName>
        <description>Adds 180 days to the reminder date</description>
        <field>dt_initial_upload_date__c</field>
        <formula>TODAY() + 1</formula>
        <name>Change Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Reminder_Date14</fullName>
        <description>adds 14 days to the reminder date.</description>
        <field>dt_initial_upload_date__c</field>
        <formula>TODAY() + 1</formula>
        <name>Change Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Reminder_Date_12months</fullName>
        <description>Adds 180 days to the reminder date</description>
        <field>dt_initial_upload_date__c</field>
        <formula>TODAY() + 2</formula>
        <name>Change Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Active_Date_Checkbox</fullName>
        <description>Restores Active checkbox to false once active checkbox is set to true and date time stamp   saved.</description>
        <field>cb_Active__c</field>
        <literalValue>0</literalValue>
        <name>Field Update Active Date Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Days_to_Reminder</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>180</formula>
        <name>Field Update Days to Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Last_Verified_Date</fullName>
        <description>Update the last verified date when verify checkbox is true upon saving.</description>
        <field>dt_verified_date__c</field>
        <formula>Now()</formula>
        <name>Field Update Last Verified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Verified_Checkbox</fullName>
        <description>Restores Verfied checkbox to false once verified checkbox is set to true and date time stamp   saved.</description>
        <field>cb_verified__c</field>
        <literalValue>0</literalValue>
        <name>Field Update Verified Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetEmailReminderStage</fullName>
        <field>pl_days_to_reminder__c</field>
        <literalValue>Second</literalValue>
        <name>SetEmailReminderStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetEmailReminderStage2</fullName>
        <field>pl_days_to_reminder__c</field>
        <literalValue>Third</literalValue>
        <name>SetEmailReminderStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetEmailReminderStage3</fullName>
        <field>pl_days_to_reminder__c</field>
        <literalValue>First</literalValue>
        <name>SetEmailReminderStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_1</fullName>
        <description>Sets the stage field to 3.</description>
        <field>Stage__c</field>
        <literalValue>3</literalValue>
        <name>Set Stage 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_5</fullName>
        <description>Sets the stage field to 5.</description>
        <field>Stage__c</field>
        <literalValue>5</literalValue>
        <name>Set Stage 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_6</fullName>
        <description>Sets the stage field to 6.</description>
        <field>Stage__c</field>
        <literalValue>6</literalValue>
        <name>Set Stage 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>14</formula>
        <name>UpdateDaysToNextReminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder180</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>180</formula>
        <name>UpdateDaysToNextReminder180</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder2</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>28</formula>
        <name>UpdateDaysToNextReminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder3</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>180</formula>
        <name>UpdateDaysToNextReminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder4</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>180</formula>
        <name>UpdateDaysToNextReminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDaysToNextReminder5</fullName>
        <field>Days_to_Next_Reminder__c</field>
        <formula>14</formula>
        <name>UpdateDaysToNextReminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_stage_picklist_to_5</fullName>
        <description>Update stage picklist to 2</description>
        <field>Stage__c</field>
        <literalValue>5</literalValue>
        <name>Update stage picklist to 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_upload_date</fullName>
        <description>should be, change reminder date</description>
        <field>dt_initial_upload_date__c</field>
        <formula>TODAY() + 1</formula>
        <name>change upload date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>intialuploadplus1</fullName>
        <field>dt_initial_upload_date__c</field>
        <formula>today()+1</formula>
        <name>intialuploadplus1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_stage1</fullName>
        <description>Set stage = 1</description>
        <field>Stage__c</field>
        <literalValue>1</literalValue>
        <name>set stage1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setinitialupdateto14</fullName>
        <field>dt_initial_upload_date__c</field>
        <formula>today() + 1</formula>
        <name>setinitialupdateto14</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setinitialuploaddate12mo</fullName>
        <field>dt_initial_upload_date__c</field>
        <formula>today() + 2</formula>
        <name>setinitialuploaddate12mo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setinitialuploaddate6mo</fullName>
        <field>dt_initial_upload_date__c</field>
        <formula>today() + 2</formula>
        <name>setinitialuploaddate6mo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setreminderdate120</fullName>
        <field>dt_initial_upload_date__c</field>
        <formula>today() + 2</formula>
        <name>setreminderdate120</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setstage3</fullName>
        <field>Stage__c</field>
        <literalValue>3</literalValue>
        <name>setstage3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setstageto1</fullName>
        <field>Stage__c</field>
        <literalValue>1</literalValue>
        <name>setstageto1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setstageto6</fullName>
        <field>Stage__c</field>
        <literalValue>6</literalValue>
        <name>setstageto6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setstageto_5</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>setstageto_5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setstageto_6</fullName>
        <field>Stage__c</field>
        <literalValue>6</literalValue>
        <name>setstageto_6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>settriggeredtofalseinitial</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>settriggeredtofalseinitial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggeredtofalse28</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggeredtofalse28</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggeredtofalse6mo</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggeredtofalse6mo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggertofalse</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggertofalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggertofalse12mo</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggertofalse12mo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggertofalse12mos</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggertofalse12mos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggertofalse28</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggertofalse28</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>triggertofalse6mo</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>triggertofalse6mo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_initial_upload_date_add_1_day</fullName>
        <description>Update the initial upload date and add 1 day for testing, after change to 180 days.</description>
        <field>dt_initial_upload_date__c</field>
        <formula>TODAY() + 1</formula>
        <name>update initial upload date add 1 day</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatetriggeredfalse</fullName>
        <field>Triggered__c</field>
        <literalValue>0</literalValue>
        <name>updatetriggeredfalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <tasks>
        <fullName>Email_Reminder_Sent</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Email Reminder Sent</subject>
    </tasks>
    <tasks>
        <fullName>Email_Reminder_Task</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Resume__c.Next_Reminder_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Email Reminder Task</subject>
    </tasks>
    <tasks>
        <fullName>Email_Sent_Initial_Update_Request</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Resume__c.dt_verified_date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Sent Initial Update Request</subject>
    </tasks>
    <tasks>
        <fullName>Email_Sent_Resume_Verified</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Email has been sent to employee, resume verified.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Resume__c.dt_verified_date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Sent Resume Verified</subject>
    </tasks>
    <tasks>
        <fullName>Email_reminder</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Email reminder</subject>
    </tasks>
    <tasks>
        <fullName>Initial_Reminder_to_Verify_Salesforce_Resume</fullName>
        <assignedToType>owner</assignedToType>
        <description>A recent review of your resume in the Client Success Platform indicates you have not verified your information since initial creation. Please review and confirm all information is up-to-date, remember to check the “Verified” checkbox and click “Save.”</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>14 day Reminder to Verify Salesforce Resume</subject>
    </tasks>
    <tasks>
        <fullName>Initial_Reminder_to_Verify_Salesforce_Resume2</fullName>
        <assignedToType>owner</assignedToType>
        <description>A recent review of your resume in the Client Success Platform indicates you have not verified your information since it&apos;s creation. Please review and confirm all information is up-to-date, remember to check the “Verified” checkbox and click “Save.”</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Initial Reminder to Verify Salesforce Resume</subject>
    </tasks>
    <tasks>
        <fullName>Initial_Reminder_to_Verify_Salesforce_Resume28</fullName>
        <assignedToType>owner</assignedToType>
        <description>A recent review of your resume in the Client Success Platform indicates you have not verified your information since it&apos;s creation. Please review and confirm all information is up-to-date, remember to check the “Verified” checkbox and click “Save.”</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Initial 28 day Reminder to Verify Salesforce Resume</subject>
    </tasks>
    <tasks>
        <fullName>Initial_resume_verification_email_request_sent</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Initial verification resume update email request sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Initial resume verification email request sent</subject>
    </tasks>
    <tasks>
        <fullName>Initial_resume_verification_email_request_sent1</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Initial verification resume update email request sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Initial resume verification email request sent</subject>
    </tasks>
    <tasks>
        <fullName>Please_Verify_Your_Resume</fullName>
        <assignedToType>owner</assignedToType>
        <description>A recent review of your resume in the Client Success Platform indicates you have not verified your information in the last six months. Please review and confirm all information is up-to-date, remember to check the “Verified” checkbox and click “Save.”</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Six Month Reminder to Verify Salesforce Resume</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_Email_Nancy</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Resume__c.Next_Reminder_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder Email - Nancy</subject>
    </tasks>
    <tasks>
        <fullName>Six_Month_Reminder_to_Update_Resume</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>It has been six months since you&apos;ve reviewed your resume in Salesforce, please verify that all the information is still correct.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Six Month Reminder to Update Resume</subject>
    </tasks>
    <tasks>
        <fullName>Six_month_reminder_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Six month reminder sent</subject>
    </tasks>
    <tasks>
        <fullName>Six_month_reminder_task</fullName>
        <assignedTo>avinash.gaikwad@pwc.com.wpprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Six month reminder task</subject>
    </tasks>
    <tasks>
        <fullName>Twelve_Month_Reminder_to_Verify_Resume</fullName>
        <assignedToType>owner</assignedToType>
        <description>A recent review of your resume in the Client Success Platform indicates you have not verified your information in the last twelve months. Please review and confirm all information is up-to-date, remember to check the “Verified” checkbox and click “Save.”</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Twelve Month Reminder to Verify Resume</subject>
    </tasks>
    <tasks>
        <fullName>X12_month_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>12 month email sent</subject>
    </tasks>
    <tasks>
        <fullName>X12_month_email_sent1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>12 month email sent1</subject>
    </tasks>
    <tasks>
        <fullName>X12_month_reminder_email_to_verify_resume</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>12 month reminder email to verify resume</subject>
    </tasks>
    <tasks>
        <fullName>X14_day_reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>14 day reminder</subject>
    </tasks>
    <tasks>
        <fullName>X14_day_reminder_apex</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>14 day reminder - apex</subject>
    </tasks>
    <tasks>
        <fullName>X28_day_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>28 day email sent</subject>
    </tasks>
    <tasks>
        <fullName>X28_days_reminder_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>28 days reminder sent</subject>
    </tasks>
    <tasks>
        <fullName>X6_mo_email_reminder_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>6 mo email reminder sent</subject>
    </tasks>
    <tasks>
        <fullName>initial_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>initial email sent</subject>
    </tasks>
</Workflow>
