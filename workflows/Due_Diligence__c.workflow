<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Due_Diligence_Completed_in_Last_12_Month</fullName>
        <field>Due_Diligence_Completed_in_Last_12_Month__c</field>
        <literalValue>1</literalValue>
        <name>Due Diligence Completed in Last 12 Month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Changed_Date</fullName>
        <field>Status_Changed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Status Changed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Due Diligence Completed in Last 12 Month</fullName>
        <actions>
            <name>Due_Diligence_Completed_in_Last_12_Month</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates &quot;Due Diligence Completed in Last 12 Months&quot; checkbox if the status is marked to &quot;Approved&quot; and it&apos;s been less than 365 days between the Status Change and Today.</description>
        <formula>IF(  ISPICKVAL( Status__c , &quot;Approved&quot;) &amp;&amp; ((TODAY() -  Status_Changed_Date__c ) &lt;= 365), True, False)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status Changed Date</fullName>
        <actions>
            <name>Status_Changed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Time stamps the date the Due Diligence Status is changed.</description>
        <formula>ISCHANGED( Status__c )  ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
