<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>set_name_to_partner_rating</fullName>
        <description>Set Name for Partner Rating</description>
        <field>Name</field>
        <formula>&quot;Partner Rating&quot;</formula>
        <name>Set Name for Partner Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_rating_name_cost</fullName>
        <description>Partner Cost Rating</description>
        <field>Name</field>
        <formula>&quot;Partner Cost Rating&quot;</formula>
        <name>Set Rating Name Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>wfr_rename_rating_name</fullName>
        <actions>
            <name>set_name_to_partner_rating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>partner_ratings__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Rating</value>
        </criteriaItems>
        <description>Sets rating name based on recordtype</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>wfr_rename_rating_name_cost</fullName>
        <actions>
            <name>set_rating_name_cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>partner_ratings__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Cost Rating</value>
        </criteriaItems>
        <description>Set ratings that are cost&apos;s name is set to &quot;Partner Cost Rating&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
