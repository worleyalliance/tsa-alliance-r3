<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_Submit_for_Approval</fullName>
        <field>Submit_for_approval__c</field>
        <literalValue>0</literalValue>
        <name>Reset Submit for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Recommendation_to_Go_Bid</fullName>
        <field>Recommendation__c</field>
        <literalValue>Go / Bid</literalValue>
        <name>Update Recommendation to Go/Bid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Recommendation_to_No_Go_No_Bid</fullName>
        <field>Recommendation__c</field>
        <literalValue>No-Go / No-Bid</literalValue>
        <name>Update Recommendation to No-Go/No-Bid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Review_Status_to_Submitted</fullName>
        <field>Review_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Review Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>Review_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_New</fullName>
        <field>Review_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Review_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Bid_No_Bid_BIAF</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Bid_No_Bid_Review_BIAF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Bid/No-Bid BIAF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Bid_No_Bid_ECR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Bid_No_Bid_Review_ECR</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Bid/No-Bid ECR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Go_No_Go_BIAF</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Go_No_Go_Review_BIAF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Go/No-Go BIAF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Go_No_Go_Bid_No_Bid_GSOP110</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Go_No_Go_Bid_No_Bid_Review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Go/No-Go/Bid/No-Bid GSOP110</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Go_No_Go_ECR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Go_No_Go_Review_ECR</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Go/No-Go ECR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Green_Team_BIAF</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Proposal_Risk_Pricing_Review_BIAF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Green Team BIAF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Green_Team_ECR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Proposal_Risk_Pricing_Review_ECR</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Green Team ECR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Green_Team_GSOP_110</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Proposal_Risk_Pricing_Green_Team_Review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Green Team GSOP 110</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Is Cloned</fullName>
        <active>false</active>
        <formula>ISCLONE()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Bid%2FNo-Bid BIAF Review</fullName>
        <actions>
            <name>Update_to_Bid_No_Bid_BIAF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>BIAF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Bid/No-Bid Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Bid%2FNo-Bid ECR Review</fullName>
        <actions>
            <name>Update_to_Bid_No_Bid_ECR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Bid/No-Bid Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Go%2FNo-Go BIAF Review</fullName>
        <actions>
            <name>Update_to_Go_No_Go_BIAF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>BIAF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Go/No-Go Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Go%2FNo-Go ECR Review</fullName>
        <actions>
            <name>Update_to_Go_No_Go_ECR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Go/No-Go Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Go%2FNo-Go%2FBid%2FNo-Bid GSOP110 Review</fullName>
        <actions>
            <name>Update_to_Go_No_Go_Bid_No_Bid_GSOP110</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Bid/No-Bid Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Go/No-Go Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Green Team BIAF Review</fullName>
        <actions>
            <name>Update_to_Green_Team_BIAF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>BIAF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Proposal Risk &amp; Pricing (Green Team) Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Green Team ECR Review</fullName>
        <actions>
            <name>Update_to_Green_Team_ECR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Proposal Risk &amp; Pricing (Green Team) Review</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Proposal Risk %26 Pricing Review GSOP 110</fullName>
        <actions>
            <name>Update_to_Green_Team_GSOP_110</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Proposal Risk &amp; Pricing (Green Team) Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submit for Approval Set to True</fullName>
        <actions>
            <name>Reset_Submit_for_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Review__c.Submit_for_approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to reset the Submit for Approval flag</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Bid Recommendation GSOP 110</fullName>
        <actions>
            <name>Update_Recommendation_to_Go_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Bid/No-Bid Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF,ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.Recommendation__c</field>
            <operation>equals</operation>
            <value>Bid</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Go Recommendation GSOP 110</fullName>
        <actions>
            <name>Update_Recommendation_to_Go_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Go/No-Go Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF,ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.Recommendation__c</field>
            <operation>equals</operation>
            <value>Go</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update No-Bid Recommendation GSOP 110</fullName>
        <actions>
            <name>Update_Recommendation_to_No_Go_No_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Bid/No-Bid Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF,ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.Recommendation__c</field>
            <operation>equals</operation>
            <value>No-Bid</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update No-Go Recommendation GSOP 110</fullName>
        <actions>
            <name>Update_Recommendation_to_No_Go_No_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Review__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Go/No-Go Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Line_of_Business__c</field>
            <operation>notEqual</operation>
            <value>BIAF,ECR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Review__c.Recommendation__c</field>
            <operation>equals</operation>
            <value>No-Go</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
