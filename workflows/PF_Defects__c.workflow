<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_defect_close_date_when_closed</fullName>
        <field>PF_Closed_Date__c</field>
        <formula>today()</formula>
        <name>Populate defect close date when closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PF_Populate defect close date when closed</fullName>
        <actions>
            <name>Populate_defect_close_date_when_closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_Defects__c.PF_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Will automatically populate the date in which the defect was closed in the closed date field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
