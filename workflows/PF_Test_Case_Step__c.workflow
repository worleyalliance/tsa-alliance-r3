<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Remove_Duplicate</fullName>
        <field>PF_Test_Case_Step_Unique__c</field>
        <formula>PF_Test_Case__c&amp;text(PF_Step_Number__c)</formula>
        <name>Remove Duplicate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PF_Prevent Duplicate Test Case Step Number</fullName>
        <actions>
            <name>Remove_Duplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_Test_Case_Step__c.PF_Step_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
