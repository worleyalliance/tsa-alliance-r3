<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Management_Reviewer</fullName>
        <field>Management_Reviewer__c</field>
        <formula>$User.FirstName + &quot; &quot; +  $User.LastName</formula>
        <name>Management Reviewer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Management_Reviewer_field</fullName>
        <field>Management_Reviewer__c</field>
        <name>Update Management Reviewer field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Technical_Reviewer</fullName>
        <field>Technical_Reviewer__c</field>
        <name>Update Technical Reviewer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Technical_Reviewer_Field</fullName>
        <field>Technical_Reviewer__c</field>
        <formula>$User.FirstName + &quot; &quot; +  $User.LastName</formula>
        <name>Update Technical Reviewer Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clear Management Reviewer</fullName>
        <actions>
            <name>Update_Management_Reviewer_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CPC__c.Management_Review__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Clears the name in Management Reviewer when Management Review Complete checkbox is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear Technical Reviewer</fullName>
        <actions>
            <name>Update_Technical_Reviewer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CPC__c.Technical_Review__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Clears the name in Technical Reviewer when Technical Review Complete checkbox is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Management Reviewer</fullName>
        <actions>
            <name>Management_Reviewer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the name of the person who checks Management Review Complete.</description>
        <formula>AND( ISCHANGED(  Management_Review__c  ), Management_Review__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Technical Reviewer</fullName>
        <actions>
            <name>Update_Technical_Reviewer_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the name of the person who checks Technical Review Complete.</description>
        <formula>AND( ISCHANGED(   Technical_Review__c   ), Technical_Review__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
