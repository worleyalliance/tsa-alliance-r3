<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Sprint_Status_to_Completed</fullName>
        <description>Will automatically update the status to completed based on the end date.</description>
        <field>PF_Sprint_Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Update Sprint Status to Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sprint_Status_to_In_Progress</fullName>
        <description>Will automatically update the status to in progress.</description>
        <field>PF_Sprint_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Sprint Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sprint_Status_to_Not_Started</fullName>
        <description>Updates the sprint status to not started.</description>
        <field>PF_Sprint_Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>Update Sprint Status to Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PF_Update Sprint Status to Completed</fullName>
        <actions>
            <name>Update_Sprint_Status_to_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will automatically update the status to completed based on the end date.</description>
        <formula>NOT(ISNEW()) &amp;&amp;  PF_End_Date__c &lt; TODAY()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PF_Update Sprint Status to In Progress</fullName>
        <actions>
            <name>Update_Sprint_Status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will automatically update the status to in progress.</description>
        <formula>NOT(ISNEW()) &amp;&amp; PF_Start_Date__c &lt; TODAY()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PF_Update Sprint Status to Not Started</fullName>
        <actions>
            <name>Update_Sprint_Status_to_Not_Started</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_Sprints__c.PF_Start_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Will automatically update the status to Not Started when the start date is greater than today.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
