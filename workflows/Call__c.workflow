<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Call_Owner_Role</fullName>
        <field>Owner_Role__c</field>
        <formula>Owner:User.UserRole.DeveloperName</formula>
        <name>Update Call Owner Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Call Owner Modified</fullName>
        <actions>
            <name>Update_Call_Owner_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(), ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
