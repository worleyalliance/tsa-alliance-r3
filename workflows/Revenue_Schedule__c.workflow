<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Quarter_Revenue</fullName>
        <description>Updates Quarter Revenue field with calculated flat spread value</description>
        <field>Probable_GM__c</field>
        <formula>Probable_GM_Linear__c</formula>
        <name>Update Quarter Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Linear Spread</fullName>
        <actions>
            <name>Update_Quarter_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Multi_Office_Split__c.Spreading_Formula__c</field>
            <operation>equals</operation>
            <value>Linear</value>
        </criteriaItems>
        <description>Workfow to copy the linear revenue spread values to Quarter Revenue field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
