<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clear_date_passed</fullName>
        <description>Will clear the date passed field when the test case assignment status is moved from passed to a non-passed status.</description>
        <field>PF_Date_Passed__c</field>
        <name>Clear date passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_date_passed</fullName>
        <description>Will automatically populate the date in which the defect was closed in the closed date field.</description>
        <field>PF_Date_Passed__c</field>
        <formula>TODAY()</formula>
        <name>Populate date passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PF_Clear date passed when status moved back to non-passed</fullName>
        <actions>
            <name>Clear_date_passed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will clear the date passed field when the test case assignment status is moved from passed to a non-passed status.</description>
        <formula>TEXT(PRIORVALUE( PF_Status__c))= &quot;Passed&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PF_Populate date passed</fullName>
        <actions>
            <name>Populate_date_passed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PF_TestCaseExecution__c.PF_Status__c</field>
            <operation>equals</operation>
            <value>Passed</value>
        </criteriaItems>
        <description>Will automatically populate the date in which the test case assignment was passed in the date passed field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
