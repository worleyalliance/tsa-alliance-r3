<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Reminder_Email_to_Case_Owners_High_Priority</fullName>
        <description>Send Reminder Email to Case Owners High Priority</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Inactivity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_Email_to_Case_Owners_Low_Priority</fullName>
        <description>Send Reminder Email to Case Owners Low Priority</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Inactivity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_Email_to_Case_Owners_Medium_Priority</fullName>
        <description>Send Reminder Email to Case Owners Medium Priority</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Inactivity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_Email_to_High_Priority_Case_Queue_Members</fullName>
        <description>Send Reminder Email to High Priority Case Queue Members</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Unassigned_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_Email_to_Low_Priority_Case_Queue_Members</fullName>
        <description>Send Reminder Email to Low Priority Case Queue Members</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Unassigned_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Reminder_Email_to_Medium_Priority_Case_Queue_Members</fullName>
        <description>Send Reminder Email to Medium Priority Case Queue Members</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_Unassigned_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v1</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v2</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v3</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v4</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v5</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Reminder_Date_v6</fullName>
        <description>Populate Last Reminder Email date for reference</description>
        <field>Last_Email_Reminder_sent_on__c</field>
        <formula>Text(Today())</formula>
        <name>Set Last Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>High Priority Case reminders for Queue Members</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up High Priority Case reminders to Queues for unassigned Cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_High_Priority_Case_Queue_Members</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v4</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>High Priority Case reminders to Owners</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up High Priority Case reminders to Owners for inactivity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_Case_Owners_High_Priority</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Low Priority Case reminders for Queue Members</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up Low Priority Case reminders to Queues for unassigned Cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_Low_Priority_Case_Queue_Members</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v5</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Low Priority Case reminders to Owners</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up Low Priority Case reminders to Owners for inactivity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_Case_Owners_Low_Priority</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v3</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Medium Priority Case reminders for Queue Members</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up Medium Priority Case reminders to Queues for unassigned Cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_Medium_Priority_Case_Queue_Members</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v6</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Medium Priority Case reminders to Owners</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner__c</field>
            <operation>contains</operation>
            <value>User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Success Platform (CSP)</value>
        </criteriaItems>
        <description>To Set up Medium Priority Case reminders to Owners for inactivity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Reminder_Email_to_Case_Owners_Medium_Priority</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Last_Reminder_Date_v2</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
