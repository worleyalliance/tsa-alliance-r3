<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LR_Notify_CSP_User_Prod_Update_Change</fullName>
        <description>LR - Notify CSP User Prod Update Change</description>
        <protected>false</protected>
        <recipients>
            <field>Provisioned_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Provisioning_Update_LRChange_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_CSP_Admin</fullName>
        <description>LR - Notify CSP Admin ask PROD Provisioning</description>
        <protected>false</protected>
        <recipients>
            <recipient>cindy.stephenson@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shannon.davis@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Pending_Prod_Provisioning_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_CSP_Admin_LRChange</fullName>
        <description>LR - Notify CSP Admin Need Prod Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>cindy.stephenson@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shannon.davis@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Pending_Prod_Update_LRChange_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_Deprovisioning</fullName>
        <description>LR - Notify CSP Admin User Deprovisioned</description>
        <protected>false</protected>
        <recipients>
            <recipient>cindy.stephenson@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shannon.davis@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_User_Prod_Deprovisioned_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_Trainee</fullName>
        <ccEmails>nicole.mejia@worleyparsons.com</ccEmails>
        <description>LR - Notify Trainee TRN Provisioning Ready</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_For__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Training_Instructor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Training_Login_Instruction_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_Trainer</fullName>
        <description>LR - Notify CSP Admin ask TRN Provisioning.</description>
        <protected>false</protected>
        <recipients>
            <recipient>shannon.davis@worleyparsons.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Request_Completed_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Training_Instructor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Pending_TRN_Provisioning_New</template>
    </alerts>
    <alerts>
        <fullName>License_Request_Notify_User_Active</fullName>
        <ccEmails>shannon.davis@worleyparsons.com</ccEmails>
        <description>LR - Notify User is Provisioned in Prod.</description>
        <protected>false</protected>
        <recipients>
            <field>Provisioned_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Requests/LR_Production_Login_Instruction_New</template>
    </alerts>
    <fieldUpdates>
        <fullName>LR_Record_Type_Master_License</fullName>
        <description>Set Record Type = Master License Request</description>
        <field>RecordTypeId</field>
        <lookupValue>Master_License_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LR Record Type Master License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Change_Request_Record_Type</fullName>
        <description>Updates record type from New Change Request to Change Request</description>
        <field>RecordTypeId</field>
        <lookupValue>Change_License_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Change Request Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <description>Updates the record type from New License Request to License Request</description>
        <field>RecordTypeId</field>
        <lookupValue>License_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <description>Update Status to Approved by LOB on Approvals</description>
        <field>Status__c</field>
        <literalValue>Approved by LOB</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_LRChange</fullName>
        <description>Update status to Approved for License Request Change</description>
        <field>Status__c</field>
        <literalValue>Change Awaiting Prod Update</literalValue>
        <name>Update Status to Approved LRChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Pending</fullName>
        <description>Change Status Field to Pending</description>
        <field>Status__c</field>
        <literalValue>Pending LOB Approval</literalValue>
        <name>Update Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Pending_LRChange</fullName>
        <description>Set License Request status to Change Pending LOB Approval</description>
        <field>Status__c</field>
        <literalValue>Change Pending LOB Approval</literalValue>
        <name>Update Status to Pending LRChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Recalled</fullName>
        <description>Update Status to Recalled on User Recalls</description>
        <field>Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Update Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Recalled_LRChange</fullName>
        <description>Update the status to Change Recalled.</description>
        <field>Status__c</field>
        <literalValue>Change Recalled</literalValue>
        <name>Update Status to Recalled LRChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <description>Update Status to Rejected by LOB on Rejections</description>
        <field>Status__c</field>
        <literalValue>Rejected By LOB</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected_LRChange</fullName>
        <description>Update status to Rejected for License Request Change</description>
        <field>Status__c</field>
        <literalValue>Change Rejected by LOB</literalValue>
        <name>Update Status to Rejected LRChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Change LR Record Type</fullName>
        <actions>
            <name>LR_Record_Type_Master_License</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>License_Request__c.Request_Completed_By__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>License_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>License Request</value>
        </criteriaItems>
        <description>When License Request: Request Completed By field is entered (change from blank to some value).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Change Request</fullName>
        <actions>
            <name>Update_Change_Request_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>License_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Change License Request</value>
        </criteriaItems>
        <description>When a New Change License Request is created the record type changes to &quot;Change Request&quot; so that a new page layout is assigned.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update License Request</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>License_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New License Request</value>
        </criteriaItems>
        <description>When a New License Request is created the record type changes to &quot;License Request&quot; so that a new page layout is assigned.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
