<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OCI_Case_Closed</fullName>
        <description>OCI Case Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>OCI_Committee</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OCI_Folder/OCI_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>OCI_Committee_Submitted_Notification</fullName>
        <description>OCI Committee - Submitted Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>OCI_Committee</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OCI_Folder/OCI_Case_Submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_OCI_Committee</fullName>
        <field>OwnerId</field>
        <lookupValue>OCI_Committee</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign OCI Committee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Time_Closed_Cleared</fullName>
        <description>Clears the value from the Date/Time Closed field on OCI Case.</description>
        <field>Date_Time_Closed__c</field>
        <name>Date/Time Closed Cleared</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OCI_Case_Closed_Date</fullName>
        <description>Sets the OCI Case Closed Date to now</description>
        <field>Date_Time_Closed__c</field>
        <formula>NOW()</formula>
        <name>Set OCI Case Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Closed</fullName>
        <field>Stage__c</field>
        <literalValue>Closed</literalValue>
        <name>Set Stage to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_open</fullName>
        <description>Change status to Open - Potential Issues when OCI record is re-opened</description>
        <field>Status__c</field>
        <literalValue>Open - Potential Issues</literalValue>
        <name>Status to open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Closed_to_blank</fullName>
        <description>Update Date/Time Closed to blank when OCI record is re-opened</description>
        <field>Date_Time_Closed__c</field>
        <name>Update Date Closed to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OCI Case Closed</fullName>
        <actions>
            <name>Set_OCI_Case_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stage_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>OCI_Case__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - No OCI</value>
        </criteriaItems>
        <criteriaItems>
            <field>OCI_Case__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Mitigation Possible</value>
        </criteriaItems>
        <criteriaItems>
            <field>OCI_Case__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Mitigation Not Possible</value>
        </criteriaItems>
        <criteriaItems>
            <field>OCI_Case__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Opportunity Dropped</value>
        </criteriaItems>
        <description>When an OCI Case Status is modified to a Closed status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OCI Case Reopened</fullName>
        <actions>
            <name>Date_Time_Closed_Cleared</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OCI_Case__c.Status__c</field>
            <operation>equals</operation>
            <value>Open - No Known Issues,Open - Potential Issues</value>
        </criteriaItems>
        <description>When an OCI Case Status is changed from Closed to Open</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Re open change status</fullName>
        <actions>
            <name>Status_to_open</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Date_Closed_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a user re-opens an OCI case (current stage is Closed) to &quot;Open&quot; &quot;Ops Review&quot; or &quot;Stales review, the status should automatically change to Open - Potential Issues. In addition, the &quot;Date/Time Closed&quot; field should be blank.</description>
        <formula>ISPICKVAL( PRIORVALUE(Stage__c) ,&quot;Closed&quot;)  &amp;&amp;  NOT( ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>wfr_Submitted_to_OCI_Committee</fullName>
        <actions>
            <name>OCI_Committee_Submitted_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_OCI_Committee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OCI_Case__c.Stage__c</field>
            <operation>equals</operation>
            <value>Sales Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
