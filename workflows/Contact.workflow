<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>parnter_community_reminder</fullName>
        <description>Email to be sent on an ad-hoc basis.</description>
        <protected>false</protected>
        <recipients>
            <field>lr_partner_community_user__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Survey/pcs_welcome_email_reminder</template>
    </alerts>
    <alerts>
        <fullName>partner_community_welcome_email</fullName>
        <description>Email to be sent when a contact is first selected to be a Partner Community user</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Survey/emt_pcs_welcome_email_template</template>
    </alerts>
    <alerts>
        <fullName>partner_community_welcome_email_reminder</fullName>
        <description>Partner Community Timed Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Survey/pcs_reminder_email_1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Field_Update_False_Financial_POC</fullName>
        <description>Field Update Financial POC to FALSE</description>
        <field>cb_financial_poc__c</field>
        <literalValue>0</literalValue>
        <name>Field Update False Financial POC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Financial_POC</fullName>
        <description>Set Financial POC to TRUE when the Contact Type includes Finance</description>
        <field>cb_financial_poc__c</field>
        <literalValue>1</literalValue>
        <name>Field Update Financial POC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_sent_update</fullName>
        <description>Updates Date Sent field with today&apos;s date</description>
        <field>dt_date_sent__c</field>
        <formula>TODAY()</formula>
        <name>Date Sent Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_sent_update_reminder</fullName>
        <description>Updates the field with today&apos;s date when the checkbox is clicked</description>
        <field>dt_date_sent__c</field>
        <formula>TODAY()</formula>
        <name>Date Sent Update Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>reset_check_box</fullName>
        <description>Resets the check box to unchecked once the email is sent</description>
        <field>cb_send_update_email__c</field>
        <literalValue>0</literalValue>
        <name>Reset Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_community_user_true</fullName>
        <description>Sets the Community User checkbox to true</description>
        <field>cb_community_user__c</field>
        <literalValue>1</literalValue>
        <name>Set Community User True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_date_email_sent</fullName>
        <description>Set the Date the email was sent</description>
        <field>dt_date_sent__c</field>
        <formula>TODAY()</formula>
        <name>Set the Date the email was sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>wfr_community_member_on_demand_reminder</fullName>
        <actions>
            <name>parnter_community_reminder</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>date_sent_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>date_sent_update_reminder</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>reset_check_box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.cb_send_update_email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.frm_cb_community_user__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Reminder for partner community members/contacts to update the partner&apos;s information</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>wfr_partner_community_date_sent</fullName>
        <actions>
            <name>set_community_user_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_date_email_sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.frm_cb_community_user__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.dt_date_sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Rule to populate the date sent field when a contact is first setup as a community user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>wfr_partner_contact_financial_POC</fullName>
        <actions>
            <name>Field_Update_Financial_POC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow Rule to indicate a Partner Contact Financial Point of Contact (POC)</description>
        <formula>INCLUDES( pl_contact_type__c , &quot;Finance&quot;)=TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>wfr_partner_contact_non_survey_POC</fullName>
        <actions>
            <name>Field_Update_False_Financial_POC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Partner Contact to not be a financial POC</description>
        <formula>cb_financial_poc__c = TRUE &amp;&amp; INCLUDES( pl_contact_type__c ,&quot;Finance&quot;)=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
