/**************************************************************************************
Name: LVTest_ControllerTest
Version: 
Created Date: 
Function: Test class for LVtest_Controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo                    
*************************************************************************************/
@isTest
private class LVTest_ControllerTest{

    static testMethod void test_LVTest() {

       Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       Map <String,Schema.RecordTypeInfo> recordTypeMapProj = Jacobs_Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

       Jacobs_Project__c proj = new Jacobs_Project__c();
       proj.Name = 'testProj';
       proj.Project_Start_Date__c = System.today();
       insert proj;
       system.debug('proj.id=' + proj.Id);
       Library__c lib = new Library__c();
       lib.Name = 'testLib';
       lib.cb_active__c = true;
       lib.dt_submittal_date__c = System.today();
       lib.lr_project__c = proj.Id;
       
       if(recordTypeMap.containsKey('Other')) {
         lib.RecordTypeId= recordTypeMap.get('Other').getRecordTypeId();
}

       insert lib;
       system.debug('library.id='+lib.Id);
        ApexPages.currentPage().getParameters().put('Id', proj.Id);
        
        //A&T
        Jacobs_Project__c projAT = new Jacobs_Project__c();
       projAT.Name = 'testATProj';
       projAT.Project_Start_Date__c = System.today();
       if(recordTypeMapProj.containsKey('A&T Program')) {
         projAT.RecordTypeId= recordTypeMapProj.get('A&T Program').getRecordTypeId();
}
       insert projAT;
       system.debug('projAT.id=' + projAT.Id);
       Library__c libAT = new Library__c();
       libAT.Name = 'testLib';
       libAT.cb_active__c = true;
       libAT.dt_submittal_date__c = System.today();
       libAT.lr_project__c = projAT.Id;
       
       if(recordTypeMap.containsKey('PWS')) {
         libAT.RecordTypeId= recordTypeMap.get('PWS').getRecordTypeId();
}
      
       insert libAT;
       system.debug('libraryAT.id='+libAT.Id);
        ApexPages.currentPage().getParameters().put('Id', projAT.Id);
        
        LDT_Lightning_Filter__c ATsetting = new LDT_Lightning_Filter__c();
           ATsetting.Name = 'Test Setting';
           ATsetting.Label_del__c = 'PWS';
           ATsetting.Object_API__c = 'Jacobs_Project__c';
           ATsetting.OBJ_RecordType_API_Name__c = 'A_T_Program';
           ATsetting.Coverage__c = 'Include';
           Atsetting.Active__c = true;
           insert ATsetting;
        
           List<Library__c> l = LVTest_Controller.getCBAsOfProject(proj.Id);
           
           List <Library__c> m = LVTest_Controller.getCBAsOfProjectwRecordType(proj.Id, 'All');
           List <Library__c> n = LVTest_Controller.getCBAsOfProjectwRecordType(proj.Id, 'Other');
           List <String> o = LVTest_Controller.fetchRecordTypeValues(proj.Id);
           List <String> p = LVTest_Controller.fetchRecordTypeValues(projAT.Id);

           Test.StartTest();
           system.assert(l.size() > 0);
           system.assert(m.size() > 0);
           system.assert(n.size() > 0);
           system.assertequals(lib.RecordTypeId, LVTest_Controller.getRecTypeId('Other'));
           system.assert(o.size() > 0);
           system.assert(p.size() > 0);
           
           
       Test.StopTest();
       }
}