/**************************************************************************************
Name:BPRequest_C_Test
Version: 1.0 
Created Date: 09/26/2017
Function: Test class to test the BP Revision creation process

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    09/26/2017       Original Version
*************************************************************************************/
@isTest
private class BPRequest_C_Test {
    
  //Constants
  private static final String BP_STATUS_ACTIVE         = 'Active';
  private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
  private static final String OPERATING_UNIT_LABEL_USD = 'USD - United States';
  private static final String OPERATING_UNIT_USD       = 'US_OU';
  private static final String BP_REQTYPE_REVISION      = 'Revision';
  private static final String BP_RECTYPE_REVISION      = 'B_P_Revision';
  private static final String USERNAME                 = 'salesBpRevision@testuser.com';


  //Test data setup
  @testSetup
  static void setUpTestData(){

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

    
    System.runAs(salesUser){

      //Account required for contacts
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      Account acct = accountBuilder.build().save().getRecord(); 

      //Create contact records required for creating a B&P
      TestHelper.ContactBuilder contactBuilder = new TestHelper.ContactBuilder();
      Contact con = contactBuilder.build().withAccount(acct.Id).save().getRecord();

      //Create B&P Builder
      TestHelper.BPRequestBuilder bpRequestBuilder = new TestHelper.BPRequestBuilder();

      //Active B&P
      bpRequestBuilder.build(con.Id,
                             con.Id,
                             con.Id,
                             null)
                      .withBudget(100)
                      .withSellingUnit('BIAF - Americas S-South Central')
                      .withSalesBPUnit('019243')
                      .withOperatingUnit(OPERATING_UNIT_USD)
                      .withLegalEntity('2466')
                      .withStatus(BP_STATUS_ACTIVE)
                      .withProjectAccountant(con.Id)
                      .withJacobsSalesLead(con.Id)
                      .withProjectManager(con.Id)
                      .save()
                      .getRecord();

    }
  }

  /*Scenario 1: test the retrieveOriginalBAndP method
  * Create a B&P and use the method to get the B&P details
  */
    @isTest static void test_retrieveOriginalBAndP() {

    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //Variable for Original B&P
    B_P_Request__c originalBP;

    //Variable for the BPReqData
    BPRequest_Service.BPRequestData bpReqData;

    test.startTest();

    //Execute tests
    System.runAs(salesUser){

      //Get the B&P created by the user
      originalBP = [Select Id,
                           B_P_Budget_Local_Currency__c,
                           BP_Budget__c,
                           Operating_Unit__c,
                           Status__c
                    From B_P_Request__c 
                    Where CreatedById = :salesUser.Id 
                    Limit 1];

    //Call the method to get data from Original B&P
    bpReqData = BPRequest_C.retrieveOriginalBAndP(originalBP.Id);                                   

    }
    test.stopTest();

    //Validate results
    System.assert(bpReqData!=null);

    System.assertEquals(originalBP.B_P_Budget_Local_Currency__c, bpReqData.bpBudgetLocalCurrency);
    System.assertEquals(originalBP.Status__c, bpReqData.status);
    System.assertEquals(OPERATING_UNIT_LABEL_USD, bpReqData.operatingUnitLabel);
  }
    
  /*Scenario 2: test the createBPRevision method
  * Call the controller method to create a revision
  */
    @isTest static void test_createBPRevision() {

    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //Variable for Original B&P
    B_P_Request__c originalBP;

    //Create a Jason String for revision data
    String revisionJSONString = '{"additionalBudgetLocal":"500", "additionalWorkHours": "40", "requestJustification" : "Test revision B&P"}';

    test.startTest();

    //Execute tests
    System.runAs(salesUser){

      //Get the B&P created by the user
      originalBP = [Select Id,
                           B_P_Budget_Local_Currency__c,
                           BP_Budget__c,
                           Operating_Unit__c,
                           Status__c
                    From B_P_Request__c 
                    Where CreatedById = :salesUser.Id 
                    Limit 1];

    //Call the method to get data from Original B&P
    BPRequest_C.createBPRevision(originalBP.Id, revisionJSONString);                                   

    }

    test.stopTest();

    //Validate the test
    //Get the newly created revision B&P
    B_P_Request__c revisionBP = [Select Id,
                                        B_P_Budget_Local_Currency__c,
                                        Operating_Unit__c,
                                        Reason_for_Request_Justification__c,
                                        Status__C,
                                        RecordTypeId,
                                        Request_Type__c,
                                        Work_Hours__c,
                                        Original_B_P__c 
                                 From B_P_Request__c 
                                 Where Request_Type__c = :BP_REQTYPE_REVISION 
                                 And Original_B_P__c = :originalBP.Id 
                                 Limit 1];

    System.assert(revisionBP!=null);

    //Get the record type ID for revision record type
    RecordType revisionRecType = [Select Id 
                                  From RecordType 
                                  Where SObjectType = 'B_P_Request__c' 
                                  And DeveloperName = :BP_RECTYPE_REVISION 
                                  Limit 1];

    //Check all the field values
    System.assertEquals(500, revisionBP.B_P_Budget_Local_Currency__c);
    System.assertEquals('US_OU', revisionBP.Operating_Unit__c);
    System.assertEquals('Test revision B&P', revisionBP.Reason_for_Request_Justification__c);
    System.assertEquals(revisionRecType.Id, revisionBP.RecordTypeId);
    System.assertEquals(BP_REQTYPE_REVISION, revisionBP.Request_Type__c);
    System.assertEquals(40, revisionBP.Work_Hours__c);
    System.assertEquals(originalBP.Id, revisionBP.Original_B_P__c);

    }
    
}