@isTest
public class RA_ReviewToCSVControllerTest {
    
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME = 'salesBpRevision@testuser.com';
    
    @testSetup
    static void setUpTestData(){
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();
        
        System.runAs(salesUser){
            
            //Create Opportunity
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity opp = oppBuilder.build().withGo(50).save().getOpportunity();
            
            //Create Review
            TestHelper.ReviewBuilder reviewBuilder = new TestHelper.ReviewBuilder();
            Review__c review = reviewBuilder.build().withOpportunity(opp.Id).recordType('Go/No-Go Review (BIAF)').save().getRecord();
            Review__c reviewECR = reviewBuilder.build().withOpportunity(opp.Id).recordType('Go/No-Go Review (ECR)').save().getRecord();
            Review__c reviewNull = reviewBuilder.build().withOpportunity(opp.Id).recordType('New Bid/No-Bid Review').save().getRecord();
            
        }
    }
    
    static testmethod void RA_ReviewToCSVControllerTest(){
        
        
        Review__c theReview = [SELECT Id FROM Review__c LIMIT 1][0];
        
        RA_ReviewToCSVController.fetchReview(theReview.Id);
        
    }

}