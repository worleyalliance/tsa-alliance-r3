/**************************************************************************************
Name: newPartnerSurvey2ControllerTest
Version:
Created Date: 8/4/2017
Function: Test class for newPartnerSurvey2Controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo        8/4/2017    Test class for newPartnerSurvey2Controller
*************************************************************************************/
@isTest
private class newPartnerSurvey2ControllerTest{

    static testMethod void test_newPartnerSurvey2Controller() {
        
        Integer IntTest;
        
        Partner_Survey__c ps1 = new Partner_Survey__c();
        
        insert ps1;
        
        Partner_Survey2__c partnerSurvey2 = new Partner_Survey2__c();
        
        partnerSurvey2.mdr_partner_survey__c = ps1.Id;
        
        insert partnerSurvey2;
        
        //partnerSurvey.us_cb_billing_approved__c = True;
        
        //update partnerSurvey;
        
        Partner_Survey__c ps2 = new Partner_Survey__c();
        
        insert ps2;
        
        Partner_Survey2__c a = new Partner_Survey2__c();
        
        a.mdr_partner_survey__c = ps2.Id;
        
        insert a;
        
        //a.us_cb_accounting_approved__c   = True;
        //a.us_cb_annual_incurred_cost__c = True;
        //a.us_cb_billing_approved__c = True;
        //a.us_dt_accounting_approved__c = System.today();
        //a.us_dt_date_of_last_report__c = System.today();
        //a.us_dt_billing_approved__c   = System.today();
        
        //update a;
        
        //Partner_Survey__c b = new Partner_Survey__c();
        
        //insert b;
        
        //b.Id = b.createdById;
        
        //update b;
        
       // Partner_Survey__c b = newPartnerSurveyController.getPartnerSurvey();
        
        Test.StartTest();
        //    system.assert(.size() > 0);
            PageReference pageRef = Page.vf_cas_survey;
            Test.setCurrentPage(pageRef);
            
            pageRef.getParameters().put('id',partnerSurvey2.id);
            ApexPages.StandardController sc = new ApexPages.standardController(partnerSurvey2);
             
            newPartnerSurvey2Controller  getPartnerSurvey = new newPartnerSurvey2Controller(sc);
           
            getPartnerSurvey.save();
            
            getPartnerSurvey.getPartnerSurvey2();
            
            pageRef.getParameters().put('id',a.id);
            ApexPages.StandardController sc1 = new ApexPages.standardController(a);
             
            newPartnerSurvey2Controller  getPartnerSurvey1 = new newPartnerSurvey2Controller(sc1);
           
            getPartnerSurvey1.save();
            
            //System.debug('chck = ' + a.us_cb_accounting_approved__c);
            
             try{
                 //if(Test.isRunningTest())
                 //   IntTest = 20/0;
                 //   System.debug('intest = ' + IntTest);
                 
                 Partner_Survey2__c failedTest = new Partner_Survey2__c();
        
                insert failedTest;
                
                pageRef.getParameters().put('id',failedTest.id);
                ApexPages.StandardController sc2 = new ApexPages.standardController(failedTest);
             
            newPartnerSurvey2Controller  gPS = new newPartnerSurvey2Controller(sc2);
           
            gPS.save();
             }
             catch(Exception ex){
                 List<Apexpages.Message> msgs = ApexPages.getMessages();
                 boolean b = false;
                 ApexPages.addMessages(ex);
                 for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('Required fields are missing'))
                b = true;
                }
                System.debug('myexception = ' + b);
                System.assertEquals(b, True);
                System.assert(msgs.size() > 0);
                System.debug('myexceptionsize = ' + msgs.size());

                 //for (ApexPages.Message msg : ApexPAges.getMessages()){
                 //System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
                 //}
             }
            

        Test.StopTest();
       /*Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       Map <String,Schema.RecordTypeInfo> recordTypeMapProj = Jacobs_Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       
       Jacobs_Project__c proj = new Jacobs_Project__c();
       proj.Name = 'testProj';
       proj.Project_Start_Date__c = System.today();
       if(recordTypeMapProj.containsKey('Task Orders')) 
       {
           proj.RecordTypeId= recordTypeMapProj.get('Task Orders').getRecordTypeId();
       }
       insert proj;
       system.debug('proj.id=' + proj.Id);
       
       Library__c lib = new Library__c();
       lib.Name = 'testLib';
       lib.cb_active__c = true;
       lib.dt_submittal_date__c = System.today();
       lib.AT_Task_Order__c = proj.Id;
       if(recordTypeMap.containsKey('Other')) 
       {
           lib.RecordTypeId= recordTypeMap.get('Other').getRecordTypeId();
       }
       insert lib;
       system.debug('library.id='+lib.Id);
       
        ApexPages.currentPage().getParameters().put('Id', proj.Id);
        LDT_Lightning_Filter__c TOsetting = new LDT_Lightning_Filter__c();
        TOsetting.Name = 'Test Setting';
        TOsetting.Label_del__c = 'Other';
        TOsetting.Object_API__c = 'Jacobs_Project__c';
        TOsetting.OBJ_RecordType_API_Name__c = 'task_orders';
        TOsetting.Coverage__c = 'Include';
        TOsetting.Active__c = true;
        insert TOsetting;
           

        List<Library__c> l = TaskOrderDocs_Controller.getTOsOfProject(proj.Id);
        List <String> m = TaskOrderDocs_Controller.fetchRecordTypeValues();
        List<Library__c> n = TaskOrderDocs_Controller.getCBAsOfProjectwRecordType(proj.Id,'All');
        List<Library__c> o = TaskOrderDocs_Controller.getCBAsOfProjectwRecordType(proj.Id,'Other');
        
        
           
        Test.StartTest();
           system.assert(l.size() > 0);
           system.assert(m.size() > 0);
           system.assert(n.size() > 0);
           system.assert(o.size() > 0);
           system.assertequals(lib.RecordTypeId, TaskOrderDocs_Controller.getRecTypeId('Other'));
        Test.StopTest();
        */
       }
}