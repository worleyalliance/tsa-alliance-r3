/**************************************************************************************
Name: OCICase_TriggerHandler
Version: 1.0 
Created Date: 20.07.2017
Function: OCI Case Trigger handler

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    07.20.2017      Original Version
* Manuel Johnson	12.13.2017		Updated to switch to OCI Case instead of Case object
*************************************************************************************/

public class OCICase_TriggerHandler {
    
    private static OCICase_TriggerHandler handler;
	
    /*
    * Singleton like pattern
    */
    public static OCICase_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new OCICase_TriggerHandler();
        }
        return handler;
    }
    
    public void onAfterDelete(List<OCI_Case__c> deletedCases){
        updateOpportunityOCICaseStatus(deletedCases);
    }
    
    private void updateOpportunityOCICaseStatus(List<OCI_Case__c> deletedCases){
        List<Id> opportunityIds = new List<Id>();
        Map<Id, String> oppOCIStatusMap = new Map<Id, String>();
        List<Opportunity> oppsToUpdateList = new List<Opportunity>();
        
        for(OCI_Case__c c : deletedCases){
            if(c.Opportunity__c != null){
                opportunityIds.add(c.Opportunity__c);
            }
        }
        
        // Update the Opportunity OCI Case Status to match the last created OCI Case or to 'None' if no other Cases exist
        for(Opportunity o : [SELECT Id, (SELECT Status__c FROM OCI_Cases__r ORDER BY CreatedDate DESC LIMIT 1) 
                             FROM Opportunity WHERE Id IN : opportunityIds]) {
            if(o.OCI_Cases__r.size() > 0) {
                for(OCI_Case__c c : o.OCI_Cases__r) {
                    o.OCI_Case_Status__c = c.Status__c;
                }
            } 
            else { 
                o.OCI_Case_Status__c = 'None'; 
            }
            oppsToUpdateList.add(o);
        }
        
        update oppsToUpdateList;
    }

}