/**************************************************************************************
Name: CustomRelatedList_Service
Version: 1.0 
Created Date: 14.03.2017
Function: Service for CustomRelatedList component. Handles data read, save and delete

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           14.03.2017         Original Version
*************************************************************************************/
public class CustomRelatedList_Service extends BaseComponent_Service{

   /*
    *   Retrieves related list data for given parent record id based on relationship name and wanted fields.
    *   Data is packed and send as single object dataset
    */
    public TableData getData(
            Id parentId,
            String objectType,
            String relationName,
            List<String> fieldNames,
            String linkFieldId,
            String linkFieldLabel
    ) {
        validateObjectAccesible(Schema.getGlobalDescribe().get(objectType).getDescribe().getName());

        TableData data = new TableData();
        data.fieldDescriptions = prepareFieldDescriptions(fieldNames, objectType);

        String linkColumnsFields =
                (linkFieldId != null ? ',' + linkFieldId + ' ' : '')
                        + (linkFieldLabel != null ? ',' + linkFieldLabel + ' ' : '');

        String query =
                'SELECT ' + String.join(fieldNames, ',') + ' ' + linkColumnsFields +
                        'FROM ' + objectType + ' ' +
                        'WHERE ' + relationName + ' = \'' + parentId + '\' ' +
                        'ORDER BY CreatedDate DESC';
        System.debug('query: ' + query);
        List<sObject> records = (List<sObject>) Database.query(query);
        data.rows = new List<Map<String, Object>>();
        List<Map<String, Object>> rows = new List<Map<String, Object>>();
        for (sObject obj : records) {
            Map<String, Object> record = new Map<String, Object>();
            for (String fieldName : fieldNames) {
                record.put(fieldName, obj.get(fieldName));
            }
            if (linkFieldId != null && linkFieldLabel != null) {
                String[] idPath = linkFieldId.split('\\.');
                sObject subObj = obj;
                for (Integer i = 0; i < idPath.size() - 1; i++) {
                    subObj = subObj.getSObject(idPath[i]);
                }
                record.put(linkFieldId, subObj.get(idPath[idPath.size() - 1]));

                subObj = obj;
                String[] labelPath = linkFieldLabel.split('\\.');
                for (Integer i = 0; i < labelPath.size() - 1; i++) {
                    subObj = subObj.getSObject(labelPath[i]);
                }
                record.put(linkFieldLabel, subObj.get(labelPath[labelPath.size() - 1]));
            }

            record.put('Id', obj.Id);
            rows.add(record);
        }
        data.rows = rows;
        return data;
    }

   /*
    *   Performes insert/update of record
    */
    public Id saveRecord(sObject record) {
        validateObjectCreateable(record.getSObjectType().getDescribe().getName());
        validateObjectUpdateable(record.getSObjectType().getDescribe().getName());
        try{
            upsert record;
        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
        return record.Id;
    }

   /*
    *  Performes deletion of record based on it's id
    */
    public void deleteRecord(Id recordId) {
        validateObjectDeletable(recordId.getSObjectType().getDescribe().getName());
        try {
            Database.delete(recordId);
        } catch (DmlException ex) {
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
    }

   /*
    *   Gives update regarding user permissions to given object object
    */
    public CRUDpermissions getUserOpportunityPermissios(String objectType){
        return getUserPermissionsForObject(Schema.getGlobalDescribe().get(objectType).getDescribe().getName());
    }

   /*
    * Method creates list of fields and it's labels from field set
    */
    private List<FieldDescription> prepareFieldDescriptions(List<String> fieldNames, String objectType){
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        List<FieldDescription> fieldDescriptions = new List<FieldDescription>();
        for(String fieldName : fieldNames){
            if(fields.containsKey(fieldName)) {
                Schema.DescribeFieldResult dfr = fields.get(fieldName).getDescribe();
                FieldDescription singleField = new FieldDescription();
                singleField.fieldLabel = dfr.getLabel();
                singleField.fieldName = dfr.getName();
                singleField.fieldLength = dfr.getLength();
                fieldDescriptions.add(singleField);
            }
        }
        return fieldDescriptions;
    }

   /*
    * Total data set used to generate table
    */
    public class TableData{

        @AuraEnabled
        public List<FieldDescription> fieldDescriptions {get; set;}

        @AuraEnabled
        public List<Map<String, Object>> rows {get; set;}
    }

    public class FieldDescription{
        @AuraEnabled
        public String labelFieldName {get; set;}

        @AuraEnabled
        public String fieldName {get; set;}

        @AuraEnabled
        public String fieldLabel {get; set;}

        @AuraEnabled
        public Integer fieldLength {get; set;}
    }
}