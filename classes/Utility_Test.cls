@isTest
private class Utility_Test{

    private static testMethod void positivetestCustomPermissions() {
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User salesUser = userBuilder.build().withProfile('Sales Super User').withUserName('salesSuperuser@testuser.com').save().getRecord();
        User resAdminUser = userBuilder.build().withProfile('Restricted Administrator').withUserName('resAdminuser@testuser.com').save().getRecord();
        User sysAdminUser = userBuilder.build().withProfile('System Administrator').withUserName('sysAdminuser@testuser.com').save().getRecord();
        User dataStewUser = userBuilder.build().withProfile('Data Stewards').withUserName('dataStewuser@testuser.com').save().getRecord();
        
        System.runAs(salesUser){
            Boolean hasEditPermission = Utility.checkAdminPermissions();
            system.assert(hasEditPermission);

        }
        System.runAs(resAdminUser){
            Boolean hasEditPermission = Utility.checkAdminPermissions();
            system.assert(hasEditPermission);

        }
        System.runAs(sysAdminUser){
            Boolean hasEditPermission = Utility.checkAdminPermissions();
            system.assert(hasEditPermission);

        }
        System.runAs(dataStewUser){
            Boolean hasEditPermission = Utility.checkAdminPermissions();
            system.assert(hasEditPermission);

        }
        // checking for Admin Permissions
        System.runAs(resAdminUser){
            Boolean hasAdminPermission = Utility.checkSysAdminPermissions();
            system.assert(hasAdminPermission);

        }
        System.runAs(sysAdminUser){
            Boolean hasAdminPermission = Utility.checkSysAdminPermissions();
            system.assert(hasAdminPermission);

        }

    }
  
    private static testMethod void negativetestCustomPermissions() {
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User insideSalesUser= userBuilder.build().withProfile('Inside Sales').withUserName('insidesales@testuser.com').save().getRecord();
        
        System.runAs(insideSalesUser){
            Boolean hasEditPermission = Utility.checkAdminPermissions();
            system.assertEquals(hasEditPermission,false);

        }

    }
    private static testMethod void opportunityEditAccessPositivetest() {
        
        Opportunity opp; 
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).withSellingUnit('BIAF - Americas National').save().getOpportunity();      
        }
        
       
        
        //Query the Opportunity and check the Wave Selling Unit, Selling LOB and Selling BU
        List<Opportunity> testOpportunity = [Select Business_Unit__c,
                                             Line_Of_Business__c,
                                             Wave_Selling_Unit__c
                                             From Opportunity 
                                             Where Id = :opp.Id];
                                             
        System.runAs(testUser){ 
            Boolean hasRecordPermission = Utility.havRecordAccess(null,null,opp.Id);
            system.assertEquals(hasRecordPermission ,true);
        }
         //Stop Test
        Test.stopTest();
     }
    
    private static testMethod void opportunityEditAccessNegativetest() {
        
        Opportunity opp; 
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
       
        User testUser2 = userBuilder.build().withProfile('Outside Sales').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).withSellingUnit('BIAF - Americas National').save().getOpportunity();      
        }
        
        System.runAs(testUser2){
            Boolean hasRecordPermission = Utility.havRecordAccess(null,null,opp.Id);
            system.assertEquals(hasRecordPermission ,false);
            Boolean hasRecordPermission2 = Utility.havRecordAccess(null,null,null);
            system.assertEquals(hasRecordPermission2 ,false);
        }
         //Stop Test
        Test.stopTest();
     }


     @istest(SeeAllData=true)
     private static void testAddChatterFeed(){

        user testUser = [SELECT id FROM USER where id =: UserInfo.getUserId()];
        Utility.AddChatterpost(testUser.Id, testUser.Id, 'test');

     }
}