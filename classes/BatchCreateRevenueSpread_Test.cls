@isTest public class BatchCreateRevenueSpread_Test{

    static testMethod void testMethod1() 
    { 
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        Decimal revenue = 20;
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withRevenue(revenue).withGM(10).withOpportunity(opp.Id).save().getRecord();

        Test.startTest();

        BatchCreateRevenueSpread obj = new BatchCreateRevenueSpread();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }

}