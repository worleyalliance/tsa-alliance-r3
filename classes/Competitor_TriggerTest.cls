/**************************************************************************************
Name: Competitor_TriggerTest
Version: 1.0
Created Date: 15/05/2018
Function: Test class for Competitor_TriggerTest

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    15/05/2018      Original Version
* Pradeep Shetty     09/25/2018      DE713: Updated to remove compilation errors after changing Opportunity Builder class
*************************************************************************************/
@IsTest
public class Competitor_TriggerTest {
//Constants
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String PROFILE_Admin_USER = 'System Administrator';
    private static final String USERNAME1 = 'teamMemberTriggerTest1@jacobstestuser.net';
    private static final String USERNAME2 = 'teamMemberTriggerTest2@jacobstestuser.net';
    private static final String USERNAME3 = 'teamMemberTriggerTest3@jacobstestuser.net';
    private static final String USERNAME4 = 'teamMemberTriggerTest4@jacobstestuser.net';
    private static final String COUNTRY1 = 'US';
    private static final String COUNTRY2 = 'CA';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){

      //Get user builder
      TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();

      //List for user creation
      List<User> testUserList = new List<User>();

      //Create test user records. These are not saved as yet.
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME1).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME2).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME3).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_Admin_USER).withUserName(USERNAME4).getRecord());
      
      //Insert Users
      insert testUserList;        
        
      //Create test records
      
      system.runAs(testUserList[0]){    
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
        TestHelper.OpportunityTeamMemberBuilder otmBuilder = new TestHelper.OpportunityTeamMemberBuilder();
        TestHelper.CompetitorBuilder CompetitorBuilder = new TestHelper.CompetitorBuilder();
        
        Account acc = accountBuilder.build().save().getRecord();
        Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
        OpportunityTeamMember otm = otmBuilder.build().withOpportunity(opp.Id).withUser(testUserList[1].Id).getRecord();
        Competitor__c Competitor = CompetitorBuilder.build().withOpportunity(opp.Id).withCompetitor(opp.AccountId).save().getRecord();
      }
    }

    @isTest
    private static void testInsertCompetitor() {
      //Get test user for running tests
      User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1];

      TestHelper.CompetitorBuilder competitorBuilder = new TestHelper.CompetitorBuilder();      
      
      Test.startTest();

      System.runAs(testUser){
        Opportunity opp = [Select Id, 
                                  AccountId, 
                                  Competitors__c 
                           From Opportunity 
                           Limit 1];

        Account acc = [Select Id,
                              Name 
                       From Account 
                       Limit 1];
        
        // create new Competitor records        
        Competitor__c Competitor2 = competitorBuilder.build().withOpportunity(opp.Id).withCompetitor(opp.AccountId).save().getRecord();
        
        Opportunity opp2 = [Select Id, 
                                  AccountId, 
                                  Competitors__c 
                           From Opportunity 
                           Limit 1];

        System.assertEquals(opp2.Competitors__c, acc.Name);
      }
      Test.stopTest();        
    }

    @isTest
    private static void testUpdateCompetitor() {
      //Get test user to run tests
      User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1];

      Test.startTest();
      System.runAs(testUser){
        Opportunity opp = [SELECT Id, 
                                  AccountId, 
                                  Competitors__c  
                           FROM Opportunity 
                           LIMIT 1];

        Account acc = [SELECT Id,
                              Name 
                       FROM Account 
                       LIMIT 1];

        Competitor__c Competitor1 = [SELECT Id, 
                                            Opportunity__c 
                                     FROM Competitor__c 
                                     where Opportunity__c =:opp.Id 
                                     Limit 1];
        
        System.assertEquals(opp.Competitors__c, acc.Name);

        // Update Competitor records  
        Competitor1.Opportunity__c = null;
        update Competitor1;
        
        Opportunity opp2 = [SELECT Id, AccountId, Competitors__c FROM Opportunity where Id=:opp.ID LIMIT 1];

        System.assertEquals(opp2.Competitors__c, null);
      }
      Test.stopTest();        
    }
    
    @isTest
    private static void testDeleteCompetitor() {
      //This test is run as an Admin
      User testAdminUser = [SELECT Id FROM User WHERE Username = :USERNAME4];

      Test.startTest();
      System.runAs(testAdminUser){
        Opportunity opp = [SELECT Id, 
                                  AccountId, 
                                  Competitors__c 
                           FROM Opportunity 
                           LIMIT 1];
        Account acc = [SELECT Id,
                              Name 
                       FROM Account 
                       LIMIT 1];
        Competitor__c Competitor1 = [SELECT Id, 
                                            Opportunity__c 
                                     FROM Competitor__c 
                                     where Opportunity__c =:opp.Id 
                                     LIMIT 1];
        
        System.assertEquals(opp.Competitors__c, acc.Name);

        // Delete Competitor records  
        delete Competitor1;

        Opportunity opp2 = [SELECT Id, AccountId, Competitors__c FROM Opportunity where Id=:opp.ID LIMIT 1];

        System.assertEquals(opp2.Competitors__c, null);
      }
      Test.stopTest();
    }
}