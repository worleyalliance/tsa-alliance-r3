/**************************************************************************************
Name: OpportunityForecast_Service
Version: 1.0 
Created Date: 04.14.2017
Function: Service for operaiton on Multioffice and Quarter records of OpportunityForecast LC

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04/14/2017      Original Version
* Pradeep Shetty    04/22/2018      US1424: Changes related to Monthly Spread
* Chris Cornejo     09/06/2018      US2397: Allow REV / GM / Hrs On Oppty Forecast
* Pradeep Shetty    09/11/2018      US2406: Adding Front Load and Back load option for spreading formula
                                     Removing Standard Deviation.
*************************************************************************************/
public class OpportunityForecast_Service extends BaseComponent_Service{

    /*
    *   Retrieves revenue schedules sorted by multioffice record.
    *   Structure if flatted and adjusted to display in tabular format
    */
    public List<OpportunityForecast> retrieveForecasts(Id opportunityId){
        validateObjectAccesible(Multi_Office_Split__c.sObjectType.getDescribe().getName());

        List<Multi_Office_Split__c> multiOfficeSplits = [
                SELECT
                        Resource_Type__c, 
                        Performance_Unit_PU__c, 
                        Performance_Unit_PU__r.Name, 
                        Spreading_Formula__c,
                        Standard_Deviation__c, 
                        Gross_Margin__c,
                        Probable_GM__c,
                        //US2397 - add revenue and hours
                        Revenue__c,
                        Hours__c, //end US2397
                        (SELECT Probable_GM__c, 
                                Reporting_Quarter__c,
                                Reporting_Month__c,
                                RecordType.DeveloperName, 
                                Fiscal_Year__c,
                                //US2397
                                Probable_Revenue__c,
                                Probable_Hours__c  //end US2397
                        FROM Revenue_Schedules__r
                        ORDER BY Fiscal_Year__c, 
                                 Reporting_Quarter__c
                                 )
                FROM Multi_Office_Split__c
                WHERE Opportunity__c = :opportunityId
                ORDER BY CreatedDate
        ];

        List<OpportunityForecast> forecasts = new List<OpportunityForecast>();
        for(Multi_Office_Split__c split : multiOfficeSplits){
            OpportunityForecast forecast = new OpportunityForecast();
            forecast.id = split.Id;
            forecast.gm = split.Gross_Margin__c;
            forecast.probableGM = split.Probable_GM__c;
            forecast.resourceType = split.Resource_Type__c;
            forecast.officeGroup = split.Performance_Unit_PU__r.Name;
            forecast.spreadingFormula = split.Spreading_Formula__c;
            forecast.standardDeviation = split.Standard_Deviation__c;
            forecast.fiscalYears = new List<FiscalYear>();
            //US2397 - add revenue and hours
            forecast.revenue = split.Revenue__c;
            forecast.hours = split.Hours__c; //end US2397
            FiscalYear fYear = new FiscalYear();
            for(Revenue_Schedule__c revenueSchedule : split.Revenue_Schedules__r){
                if(fYear.year != revenueSchedule.Fiscal_Year__c){
                    if(fYear.year != null){
                        forecast.fiscalYears.add(fYear);
                        fYear = new FiscalYear();
                    }
                    fYear.year = revenueSchedule.Fiscal_Year__c;
                }
               if(revenueSchedule.RecordType.DeveloperName == 'Quarterly_Spread' && revenueSchedule.Reporting_Quarter__c.contains('Q1')){
                    fYear.q1Revenue = revenueSchedule.Probable_GM__c;
                    //US2397 - add revenue and hours
                    fYear.q1Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.q1Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Quarterly_Spread' && revenueSchedule.Reporting_Quarter__c.contains('Q2')){
                    fYear.q2Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.q2Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.q2Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Quarterly_Spread' && revenueSchedule.Reporting_Quarter__c.contains('Q3')){
                    fYear.q3Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.q3Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.q3Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Quarterly_Spread' && revenueSchedule.Reporting_Quarter__c.contains('Q4')){
                    fYear.q4Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.q4Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.q4Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('JUL')){
                    fYear.m1Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m1Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m1Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('AUG')){
                    fYear.m2Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m2Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m2Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('SEP')){
                    fYear.m3Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m3Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m3Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('OCT')){
                    fYear.m4Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m4Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m4Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('NOV')){
                    fYear.m5Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m5Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m5Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('DEC')){
                    fYear.m6Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m6Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m6Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('JAN')){
                    fYear.m7Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m7Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m7Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('FEB')){
                    fYear.m8Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m8Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m8Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('MAR')){
                    fYear.m9Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m9Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m9Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('APR')){
                    fYear.m10Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m10Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m10Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('MAY')){
                    fYear.m11Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m11Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m11Hours = revenueSchedule.Probable_Hours__c; //end US2397
                } else if(revenueSchedule.RecordType.DeveloperName == 'Monthly_Spread' && revenueSchedule.Reporting_Month__c.contains('JUN')){
                    fYear.m12Revenue = revenueSchedule.Probable_GM__c;
                    //US2397
                    fYear.m12Rev = revenueSchedule.Probable_Revenue__c;
                    fYear.m12Hours = revenueSchedule.Probable_Hours__c; //end US2397
                }
            }
            forecast.fiscalYears.add(fYear);
            forecasts.add(forecast);
        }

        return forecasts;
    }

   /*
    *   Retrieves revenue schedules for given multioffice record
    */
    public List<Quarter> retrieveQuartersForMultiOffice(Id multiofficeId){
        validateObjectAccesible(Revenue_Schedule__c.sObjectType.getDescribe().getName());

        List<Revenue_Schedule__c> revenueSchedules = [
                SELECT  Id,
                        Probable_GM__c, 
                        Reporting_Quarter__c, 
                        Fiscal_Year__c,
                        Spread_Percent__c,
                        //US2397 - add revenue and hours
                        Probable_Revenue__c,
                        Probable_Hours__c //end US2397
                FROM Revenue_Schedule__c
                WHERE Multi_Office_Split__c = :multiofficeId 
                AND Number_of_Days__c > 0
                ANd RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD
                ORDER BY Fiscal_Year__c, Reporting_Quarter__c
        ];

        List<Quarter> quarters = new List<Quarter>();
        for(Revenue_Schedule__c revenueSchedule : revenueSchedules){
            Quarter quar = new Quarter();
            quar.id = revenueSchedule.Id;
            quar.quaterName = revenueSchedule.Reporting_Quarter__c;
            quar.revenue = revenueSchedule.Probable_GM__c;
            quar.year = revenueSchedule.Fiscal_Year__c;
            quar.spreadPercent = revenueSchedule.Spread_Percent__c;
            //US2397 - add revenue and hours
            quar.rev = revenueSchedule.Probable_Revenue__c;
            quar.hours = revenueSchedule.Probable_Hours__c; //end US2397
            quarters.add(quar);
        }
        return quarters;
    }

   /*
    *   Updates quarter value for given records
    *   Validation: Sum of all records value have to be equal to multioffice GM value
    */
    public void saveQuarters(Id multiofficeId, String spreadFormula, String quartersJSON){
        //Check if the user has access to update revenue schedule
        validateObjectUpdateable(Revenue_Schedule__c.sObjectType.getDescribe().getName());

        //Check if the user has access to update multi office
        validateObjectUpdateable(Multi_Office_Split__c.sObjectType.getDescribe().getName());

        //Get the quarters from lightning component
        List<Quarter> quarters = (List<Quarter>) JSON.deserialize(quartersJSON, List<Quarter>.class);

        //Get Multi Office and all related revenue schedule for the given multi office
        List<Multi_Office_Split__c> multiOfficeSplits = [SELECT Probable_GM__c,
                                                                Spreading_Formula__c,
                                                                (SELECT Id,
                                                                        Probable_GM__c,
                                                                        //US2397 - add revenue and hours
                                                                        Probable_Revenue__c,
                                                                        Probable_Hours__c, //end US2397
                                                                        Reporting_Quarter__c,
                                                                        RecordType.DeveloperName,
                                                                        Spread_Percent__c  
                                                                 FROM Revenue_Schedules__r)
                                                         FROM Multi_Office_Split__c
                                                         WHERE Id = :multiofficeId
        ];

        //Old Quarter map to see if any quarter values changed
        Map<Id, Revenue_Schedule__c> oldQuartersMap = new Map<Id, Revenue_Schedule__c>();

        //Quarter to monthly periods mapping
        Map<String, List<Revenue_Schedule__c>> quarterMonthMap = new Map<String, List<Revenue_Schedule__c>>();

        //Map storing dividing factor for each quarter
        Map<String, Integer> quarterDivideMap = new Map<String, Integer>();

        //Prepare a map of old value of revenue schedule quarters
        //Also prepare the map of relating Quarter Id with Monthly rows
        //Also prepare the map that tells how should we divide the quarter GM
        for(Revenue_Schedule__c revSched: multiOfficeSplits[0].Revenue_Schedules__r){
            if(revSched.RecordType.DeveloperName == ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD){
                oldQuartersMap.put(revSched.Id, revSched);
                if(!quarterMonthMap.containsKey(revSched.Reporting_Quarter__c)){
                    quarterMonthMap.put(revSched.Reporting_Quarter__c, new List<Revenue_Schedule__c>());
                    quarterDivideMap.put(revSched.Reporting_Quarter__c, 0);   
                }
            }
            else if(revSched.RecordType.DeveloperName == ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD &&
                    revSched.Probable_GM__c>0){
                if(quarterMonthMap.containsKey(revSched.Reporting_Quarter__c)){
                    quarterMonthMap.get(revSched.Reporting_Quarter__c).add(revSched);
                }
                else{
                    quarterMonthMap.put(revSched.Reporting_Quarter__c, new List<Revenue_Schedule__c>{revSched});   
                }

                if(quarterDivideMap.containsKey(revSched.Reporting_Quarter__c)){

                    quarterDivideMap.put(revSched.Reporting_Quarter__c, quarterDivideMap.get(revSched.Reporting_Quarter__c) + 1);
                }
                else{
                    quarterDivideMap.put(revSched.Reporting_Quarter__c, 1); 
                }
            }
        }

        Decimal percentTotal = 0;

        //List of Revenue Schedules to be updated
        List<Revenue_Schedule__c> schedules = new List<Revenue_Schedule__c>();

        //Boolean value to determine if the any values have changed or not
        //This value is used to determine if we need to throw an error if the sum does not equal the Multi Office Gross margin
        //If no value has changed, but the total gross Margin does not equal Multi Office Gross margin, it is due to the rounding error.
        //No validation error is needed in such case
        Boolean isAnyValueChanged = false;

        for(Quarter quarter : quarters){
            //quarter.revenue = quarter.revenue != null ? quarter.revenue : 0;
            quarter.spreadPercent = quarter.spreadPercent != null ? quarter.spreadPercent : 0;

            //Check if the spread percent changed for this quarter
            isAnyValueChanged = isAnyValueChanged || quarter.spreadPercent != oldQuartersMap.get(quarter.id).Spread_Percent__c;
            
            schedules.add(new Revenue_Schedule__c(Id = quarter.id,
                                                  Probable_GM__c = quarter.revenue));

            //Get the monthly schedules for each of the quarter
            for(Revenue_Schedule__c monthlySched: quarterMonthMap.get(quarter.quaterName)){
                schedules.add(new Revenue_Schedule__c(Id = monthlySched.id,
                                                      Probable_GM__c = quarter.revenue/quarterDivideMap.get(quarter.quaterName)));
            }

            //Sum up individual quarter GM
            percentTotal += quarter.spreadPercent;
        }

        for(Revenue_Schedule__c rs: schedules){
            system.debug('Rev Sch: ' + rs);
        }

        if(isAnyValueChanged && percentTotal != 100){
            throw new AuraHandledException(Label.GrossMarginSumNotEqualsToExpected);
        }
        
        try{
            //If the spreading formula changed to Manual, update Multi Office and realetd schedules. 
            //We need to update the schedules to equally distribute monthly values
            if(multiOfficeSplits[0].Spreading_Formula__c != spreadFormula){

                Multi_Office_Split__c multiOfficeSplit = new Multi_Office_Split__c(Id = multiofficeId, Spreading_Formula__c = spreadFormula);
                update multiOfficeSplit; 
                update schedules;

            }
            //If the Multi Office was Manual to begin with, but some Quarter values changed, then update schedules
            else if(multiOfficeSplits[0].Spreading_Formula__c == ForecastSpread_Service.MANUAL && 
                    isAnyValueChanged){
                update schedules;
            }

        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
    }

   /*
    *  Updates quarter value for given records based on the spreading formula
    *  US2406: Removed Standard Deviation parameter from the method. Standard Deviation is not used anymore
    */
    //public void saveSpreadingFormula(Id multiofficeId, String spreadingFormula, String standardDeviation)
    public void saveSpreadingFormula(Id multiofficeId, String spreadingFormula){
        validateObjectUpdateable(Revenue_Schedule__c.sObjectType.getDescribe().getName());
        validateObjectUpdateable(Multi_Office_Split__c.sObjectType.getDescribe().getName());
        Multi_Office_Split__c multiOfficeSplit =
                new Multi_Office_Split__c(
                        Id = multiofficeId,
                        Spreading_Formula__c = spreadingFormula
                );
        try{
            update multiOfficeSplit;

        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
    }

    /*
    *   Retrieves picklist values for Spreading_Formula__c
    */
    public List<PicklistValue> getSpreadingFormulas(){
        return getPicklistValues(
                Multi_Office_Split__c.sObjectType.getDescribe().getName(),
                Multi_Office_Split__c.Spreading_Formula__c.getDescribe().getName());
    }

    /*
    *   Retrieves picklist values for Standard_Deviation__c
    */
    public List<PicklistValue> getStandardDeviations(){
        return getPicklistValues(
                Multi_Office_Split__c.sObjectType.getDescribe().getName(),
                Multi_Office_Split__c.Standard_Deviation__c.getDescribe().getName());
    }

    /*
    *   Retrieves opportunity (Added for DE270)
    */
    public Opportunity getOpportunity(Id recordId){
        Opportunity opportunity = [
            SELECT Line_of_Business__c, IsWon 
            FROM Opportunity WHERE Id = :recordId LIMIT 1
        ];
        
        return opportunity;
    }    

    public class OpportunityForecast{
        @AuraEnabled
        public Id id {get; set;}

        @AuraEnabled
        public String officeGroup {get; set;}

        @AuraEnabled
        public Decimal gm {get; set;}

        @AuraEnabled
        public Decimal probableGM {get; set;}

        @AuraEnabled
        public String resourceType {get; set;}

        @AuraEnabled
        public String spreadingFormula {get; set;}

        @AuraEnabled
        public String standardDeviation {get; set;}

        @AuraEnabled
        public List<FiscalYear> fiscalYears {get; set;}
        
        //US2397 - add revenue and hours
        @AuraEnabled
        public Decimal revenue {get; set;}
        
        @AuraEnabled
        public Decimal hours {get; set;} //end US2397
    }

    public class FiscalYear{
        @AuraEnabled
        public String year {get; set;}

        @AuraEnabled
        public Decimal q1Revenue {get; set;}

        @AuraEnabled
        public Decimal q2Revenue {get; set;}

        @AuraEnabled
        public Decimal q3Revenue {get; set;}

        @AuraEnabled
        public Decimal q4Revenue {get; set;}
        
        //US2397 - add revenue and hours for each quarter
        @AuraEnabled
        public Decimal q1Rev {get; set;}

        @AuraEnabled
        public Decimal q2Rev {get; set;}

        @AuraEnabled
        public Decimal q3Rev {get; set;}
        
        @AuraEnabled
        public Decimal q4Rev {get; set;}
        
        @AuraEnabled
        public Decimal q1Hours {get; set;}

        @AuraEnabled
        public Decimal q2Hours {get; set;}

        @AuraEnabled
        public Decimal q3Hours {get; set;}

        @AuraEnabled
        public Decimal q4Hours {get; set;}

        //US2397 - end
        @AuraEnabled
        public Decimal m1Revenue {get; set;}

        @AuraEnabled
        public Decimal m2Revenue {get; set;}

        @AuraEnabled
        public Decimal m3Revenue {get; set;}

        @AuraEnabled
        public Decimal m4Revenue {get; set;}

        @AuraEnabled
        public Decimal m5Revenue {get; set;}

        @AuraEnabled
        public Decimal m6Revenue {get; set;}

        @AuraEnabled
        public Decimal m7Revenue {get; set;}

        @AuraEnabled
        public Decimal m8Revenue {get; set;}

        @AuraEnabled
        public Decimal m9Revenue {get; set;}

        @AuraEnabled
        public Decimal m10Revenue {get; set;}

        @AuraEnabled
        public Decimal m11Revenue {get; set;}

        @AuraEnabled
        public Decimal m12Revenue {get; set;}        

        //US2397 - add revenue and hours for each month
        @AuraEnabled
        public Decimal m1Rev {get; set;}

        @AuraEnabled
        public Decimal m2Rev {get; set;}

        @AuraEnabled
        public Decimal m3Rev {get; set;}

        @AuraEnabled
        public Decimal m4Rev {get; set;}

        @AuraEnabled
        public Decimal m5Rev {get; set;}

        @AuraEnabled
        public Decimal m6Rev {get; set;}

        @AuraEnabled
        public Decimal m7Rev {get; set;}

        @AuraEnabled
        public Decimal m8Rev {get; set;}

        @AuraEnabled
        public Decimal m9Rev {get; set;}

        @AuraEnabled
        public Decimal m10Rev {get; set;}

        @AuraEnabled
        public Decimal m11Rev {get; set;}

        @AuraEnabled
        public Decimal m12Rev {get; set;}
        
        @AuraEnabled
        public Decimal m1Hours {get; set;}

        @AuraEnabled
        public Decimal m2Hours {get; set;}

        @AuraEnabled
        public Decimal m3Hours {get; set;}

        @AuraEnabled
        public Decimal m4Hours {get; set;}

        @AuraEnabled
        public Decimal m5Hours {get; set;}

        @AuraEnabled
        public Decimal m6Hours {get; set;}

        @AuraEnabled
        public Decimal m7Hours {get; set;}

        @AuraEnabled
        public Decimal m8Hours {get; set;}

        @AuraEnabled
        public Decimal m9Hours {get; set;}

        @AuraEnabled
        public Decimal m10Hours {get; set;}

        @AuraEnabled
        public Decimal m11Hours {get; set;}

        @AuraEnabled
        public Decimal m12Hours {get; set;}
        //US2397 - end
    }

    public class Quarter{
        @AuraEnabled
        public Id id {get; set;}

        @AuraEnabled
        public String year {get; set;}

        @AuraEnabled
        public String quaterName {get; set;}

        @AuraEnabled
        public Decimal revenue {get; set;}

        @AuraEnabled
        public Decimal spreadPercent {get; set;} 
        
        //US2397 - add revenue and hours
        @AuraEnabled
        public Decimal rev {get; set;}
        
        @AuraEnabled
        public Decimal hours {get; set;} //end US2397       
    }

}