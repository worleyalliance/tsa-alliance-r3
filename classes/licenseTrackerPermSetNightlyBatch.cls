/**************************************************************************************
Name: licenseTrackerPermSetNightlyBatch                
Version: 1.0 
Created Date: July 30, 2018
Function: Update License Request record with related user's Permission Set Assignments.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Ray Zhao                  03/10/2018          Original
* Ray Zhao                  03/21/2018          Bugfix on Syncing up Global Value Set
* Jignesh Suvarna           07/30/2018          Copied the Code logic from schedule
                                                class to batchable class
* Jignesh Suvarna           08/07/2018          added few check to fix Test class failures
* Jignesh Suvarna			10/09/2018			added fix for DE751 
*************************************************************************************/
global class licenseTrackerPermSetNightlyBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC){
      system.debug('Start Job');
        //JS 10/09/2018 Clear all License Request's Permission sets to fix DE751
        List<License_Request__c> lrListClear = [SELECT Id, Requested_Permission_Sets__c, Provisioned_User__c FROM License_Request__c 
                                           WHERE Provisioned_User_Status__c = TRUE AND 
                                           RecordTypeId = :Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('Master License Request').getRecordTypeId()];
        //System.debug(loggingLevel.INFO, 'LR List size =' + lrListClear.size());
        
        //Loop through the License Request Maps, Set Permission Set Values
        for (License_Request__c lr : lrListClear){
            	lr.Requested_Permission_Sets__c ='';
            }
        try {
            Database.SaveResult[] srListClear = Database.update(lrListClear, false);
        }
        catch (Exception e){
            System.Debug(e);
        }
        // fix for DE751 End here 
      // JS 8/7/2018 - to fix test class failures
      if(!Test.isRunningTest()){
          return Database.getQueryLocator('SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSet.IsOwnedByProfile = FALSE ORDER BY AssigneeId');
      }else{
          return Database.getQueryLocator('SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSet.IsOwnedByProfile = FALSE ORDER BY AssigneeId limit 199');
      }
   }
    public void execute(Database.BatchableContext BC, List<PermissionSetAssignment> psaList){

        // Initialize Log Entries and licenseTrackerUtilities class
        List<Log__c> logEntries = new List<Log__c>();
        licenseTrackerUtilities ltu = new licenseTrackerUtilities();
        
        
        //Build PermissionSetAssignment Map<AssigneeId, String - List of PermSet delimited by ';'> 
        //List<PermissionSetAssignment> psaList = [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSet.IsOwnedByProfile = FALSE ORDER BY AssigneeId];
        //System.debug(loggingLevel.INFO, 'Number Of Assignements =' + psaList.size());
        Map<Id, String> userPermSetNamesMap = new Map<Id, String>();
        for (PermissionSetAssignment psa : psaList){
            if (userPermSetNamesMap.containsKey(psa.AssigneeId)){
                userPermSetNamesMap.put(psa.AssigneeId,(userPermSetNamesMap.get(psa.AssigneeId) + ';' + ltu.getPermSetNameById(psa.PermissionSetId)));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Had =' + userPermSetNamesMap.get(psa.AssigneeId));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Add =' + ltu.getPermSetNameById(psa.PermissionSetId));
            }
            else {
                userPermSetNamesMap.put(psa.AssigneeId,ltu.getPermSetNameById(psa.PermissionSetId));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Empty, Adding =' + ltu.getPermSetNameById(psa.PermissionSetId));            
            }
        }
        //System.debug(loggingLevel.INFO, 'userPermSetNamesMap =' + userPermSetNamesMap);
        //System.debug(loggingLevel.INFO, 'userPermSetNamesMap size =' + userPermSetNamesMap.size());
    
        //Build License Request List<License_Request__c>
        List<License_Request__c> lrList = [SELECT Id, Requested_Permission_Sets__c, Provisioned_User__c FROM License_Request__c 
                                           WHERE Provisioned_User_Status__c = TRUE AND Provisioned_User__c in :userPermSetNamesMap.keySet() AND
                                           RecordTypeId = :Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('Master License Request').getRecordTypeId()];
        //System.debug(loggingLevel.INFO, 'LR List size =' + lrList.size());
        
        //Loop through the License Request Maps, Set Permission Set Values
        for (License_Request__c lr : lrList){
            if(userPermSetNamesMap.get(lr.Provisioned_User__c) != NULL){
                if(lr.Requested_Permission_Sets__c != userPermSetNamesMap.get(lr.Provisioned_User__c))
                    lr.Requested_Permission_Sets__c = userPermSetNamesMap.get(lr.Provisioned_User__c);
            }
            else{
                System.debug(loggingLevel.INFO,'That\'s the null pointer: ' + lr.Provisioned_User__c);            
                logEntries.add(new Log__c(AppName__c = 'License Management',
                                          ClassName__c = 'licenseTrackerPermSetNightly',
                                          Event__c = 'Night Scheduleable',
                                          EventType__c = 'AppException',
                                          Operation__c = 'Lookup User Id in the userPermSet Map',
                                          SFID__c = lr.Id,
                                          Exception__c = 'User Id ' + lr.Provisioned_User__c + ' not found in userPermSetmap'));
            }
        }
        System.debug(loggingLevel.INFO,'Number of unfound users: ' + logEntries.size());            
        
        try {
            // JS 07/30/2018 Using Database.Update instead of update to allow for partial record processing if errors
            //Update lrList;
            Database.SaveResult[] srList = Database.update(lrList, false);
            
            //Insert log entries
            if (logEntries.size()>0) insert logEntries;
        }
        catch (Exception e){
            System.Debug(e);
            logEntries.add(new Log__c(AppName__c = 'License Management',
                                      ClassName__c = 'licenseTrackerPermSetNightly',
                                      Event__c = 'Night Scheduleable',
                                      EventType__c = 'AppException',
                                      Operation__c = 'Unknown exception occurred, Logging UserId',
                                      SFID__c = userInfo.getUserId(),
                                      Exception__c = 'Exception: ' + e));
            insert logEntries;
        }
    }
    global void finish(Database.BatchableContext BC){
        system.debug('Finish Job');
   }
}