/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 05.05.2017
Function: Unit test class of ReachbackDataTable_C controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
@isTest
private class ReachbackDataTable_C_Test {

    @IsTest
    private static void shouldCalculateDataForReachbackTable() {
        //given
        String fieldSetName =
                Schema.getGlobalDescribe().get('AT_Reachback__c').getDescribe().fieldSets.getMap().values()[0].name;

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.ReachbackBuilder reachbackBuilder = new TestHelper.ReachbackBuilder();
        AT_Reachback__c reachback1 =
                reachbackBuilder
                        .build()
                        .withProvidingSegment(atGroup.Id)
                        .withFiscalYear(2015)
                        .save()
                        .getRecord();

        AT_Reachback__c reachback2 =
                reachbackBuilder
                        .build()
                        .withProvidingSegment(atGroup.Id)
                        .withFiscalYear(2015)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                ReachbackDataTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        2015,
                        'Fiscal_Year__c',
                        'Providing_Segment__c',
                        'AT_Reachback__c'
                );

        //then
        System.assertNotEquals(0, result.fieldDescriptions.size());
        System.assertEquals(2, result.rows.size());
    }
}