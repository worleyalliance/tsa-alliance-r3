/**************************************************************************************
Name: OpportunityQueueableHandler
Version: 1.0 
Created Date: 09/13/2017
Function: This handler actually makes the callout to the Opportunity service.The service creates records in the JEDI table
          containing Opportunity information. The table will be used by AutoPAP system for Porject creation.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    09/13/2017      Original Version
* Madhu Shetty      01/18/2018      US1429: Updated code to fix Null value issue while populating the client Id and EndClientId maps
* Madhu Shetty      12/16/2018      US2653: Corrected the Opportunity Teammember Role that was changed as part of US1535 but wasn't updated in this class
***************************************************************************************/
public with sharing class OpportunityQueueableHandler{
  /**
  * Invocable method called from the Process Builder Process: **Add Process name here**
  * @param opps - List of opportunities that were closed won
  */
  @invocableMethod(label='Create JEDI Opportunity table rows' 
                   description='Calls method to make an integration call to create JEDI Opp rows')
  public static void createJEDIOppRows(List<Opportunity> opps){
    //Enqueue OpportunityRenewalQueue job
    System.enqueueJob(new JEDIOpportunityCreationQueue(opps));
  }

  /**
  * Class implementing Queueable interface to queue the Renewal creation process
  */
  public class JEDIOpportunityCreationQueue implements OracleQueueableHandler, Database.AllowsCallouts {

    public Integer max;
    public Boolean updateFlag;
    private Integer counter = 1;

    private OracleCalloutRetryService retryService = new OracleCalloutRetryService();

    //List of original opps for which JEDI rows need to be created
    private List<Opportunity> closedWonOpps;

    /** Constructor
    * @param closedWonOpps - List of opportunities for which JEDI rows need to be created
    */
    public JEDIOpportunityCreationQueue( List<Opportunity> closedWonOpps) {
      this.closedWonOpps = closedWonOpps;   
    }

    public void setObject(sObject obj) {

    }
    
    public void setRetrySyncLog(RetrySyncLog__c retrySyncLog) {
        retryService.setRetryLog(retrySyncLog);
    }    

    /**
    * execute() - Execute method for the queue
    * @param context - QueueableContext
    */
    public void execute(QueueableContext context){

      try
      {
        system.debug('Inside execute');
        //Result from the request
        List<OpportunityAutoPAPWSDL.StatusMessage_element> insertOppResult;

        //List of Opportunity IDs
        List<Id> oppIdList = new List<Id>();

        //Map of Client Ids and Opportunity Id
        Set<Id> accountIdSet = new Set<Id>();

        //Map of Opp ID and Opp team members
        Map<Id, List<OpportunityTeamMember>> oppIdOTMMap = new Map<Id, List<OpportunityTeamMember>>();

        //List of Log Entries to be updated
        List<Log__c> logEntries = new List<Log__c>();

        //Loop through all Opps and populate the client Id and EndClientId maps
        for(Opportunity currentOpp: closedWonOpps)
        {
          
          //Populate Client Id maps
          if(!accountIdSet.contains(currentOpp.AccountId))
          {
            accountIdSet.add(currentOpp.AccountId);
          }
          if(String.isNotBlank(currentOpp.End_Client__c) && 
                  currentOpp.End_Client__c != currentOpp.AccountId &
                  !accountIdSet.contains(currentOpp.End_Client__c))
          {
            accountIdSet.add(currentOpp.End_Client__c);
          }

          oppIdList.add(currentOpp.Id);

        } // END FOR

        //Get all Accounts 
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id,
                                                                   Name, 
                                                                   Oracle_Account_Number__c
                                                            From Account 
                                                            Where Id in :accountIdSet]);

        //Get PU names
        Map<Id, Unit__c> performanceUnitMap = new Map<Id, Unit__c>([Select Id, Name From Unit__c]);

        //Get all Opportunity Team members for the closed won opportunities and create a map of Opp and Opp team members
        for(OpportunityTeamMember otm: [Select User.Name,
                                               TeamMemberRole,
                                               OpportunityId
                                        From OpportunityTeamMember 
                                        Where OpportunityId in :oppIdList 
                                        And TeamMemberRole In ('Capture Manager','Project Manager')])
        {
          //If the map contains the Opp Id, then check if the Opp roles have already been added. 
          // We only need one Capture Manager and one PM per Opportunity. If the map entry for an Opp already
          // contains 2 Opp team members, then we skip. Else we add the member. 

          if(oppIdOTMMap!=null &&
             !oppIdOTMMap.isEmpty() &&
             oppIdOTMMap.containsKey(otm.OpportunityId))
          {
            if(oppIdOTMMap.get(otm.OpportunityId).size() < 2 && 
               oppIdOTMMap.get(otm.OpportunityId)[0].TeamMemberRole != otm.TeamMemberRole)
            {
              oppIdOTMMap.get(otm.OpportunityId).add(otm);
            }
          }
          else
          {
            oppIdOTMMap.put(otm.OpportunityId, new List<OpportunityTeamMember>{otm});
          }
        } //END FOR

        //Preparing the integration call parameter
        for(Opportunity currentOpp : closedWonOpps)
        {

          //Create an instance of the integration variable
          OpportunityAutoPAPWSDL.OpportunityCreate_element oppJEDIRow= new OpportunityAutoPAPWSDL.OpportunityCreate_element();

          //Set Parameters
          oppJEDIRow.ClientAccount = accountMap.get(currentOpp.AccountId).Name;
          oppJEDIRow.ClientNumber = accountMap.get(currentOpp.AccountId).Oracle_Account_Number__c;

          //Check if the ultimate client is same as AccountId
          if(String.isNotBlank(currentOpp.End_Client__c) && 
             currentOpp.End_Client__c != currentOpp.AccountId){
            oppJEDIRow.UltimateClient = accountMap.get(currentOpp.End_Client__c).Name;
            oppJEDIRow.UltimateNumber = accountMap.get(currentOpp.End_Client__c).Oracle_Account_Number__c;
          }
          else{
            oppJEDIRow.UltimateClient = accountMap.get(currentOpp.AccountId).Name;
            oppJEDIRow.UltimateNumber = accountMap.get(currentOpp.AccountId).Oracle_Account_Number__c;
          }

          oppJEDIRow.SFOpportunityID = currentOpp.Id;
          oppJEDIRow.OpportunityName = currentOpp.Name;
          oppJEDIRow.ScopeofServices = currentOpp.Scope_of_Services__c;
          oppJEDIRow.ProjectRole = currentOpp.Project_Role__c;
          oppJEDIRow.targetCloseDate = currentOpp.CloseDate;
          oppJEDIRow.ContractAwardType = currentOpp.Contract_Award_Type__c;
          oppJEDIRow.PerformanceUnit = performanceUnitMap.get(currentOpp.Lead_Performance_Unit_PU__c).Name;
          oppJEDIRow.StartDate = currentOpp.Target_Project_Start_Date__c;
          oppJEDIRow.SalesLead = currentOpp.Owner.Name;
          oppJEDIRow.SellingLob = currentOpp.Line_of_Business__c;
          oppJEDIRow.ExecutingLob = currentOpp.Executing_Line_of_Business__c;
          oppJEDIRow.SellingBsu = currentOpp.Business_Unit__c;
          oppJEDIRow.ExecutingBsu = currentOpp.Executing_Business_Unit__c;
          
          if(oppIdOTMMap!=null &&
             !oppIdOTMMap.isEmpty() &&
             oppIdOTMMap.containsKey(currentOpp.Id))
          {
            for(OpportunityTeamMember currentOTM: oppIdOTMMap.get(currentOpp.Id)){
              if(currentOTM.TeamMemberRole == 'Capture Manager'){
                oppJEDIRow.CaptureMngOpp = currentOTM.User.Name;
              }
              else{
                oppJEDIRow.PMOpp = currentOTM.User.Name;
              }
            }            
          }



          oppJEDIRow.Status = currentOpp.Status__c;
          oppJEDIRow.Revenue = Math.round(currentOpp.Revenue__c);
          oppJEDIRow.GrossMargin = Math.round(currentOpp.Amount);
          
          //Creating the request for webservice call
          OpportunityAutoPAPWSDL.InsertOpportunityId_pt insertOppRequest = new OpportunityAutoPAPWSDL.InsertOpportunityId_pt();

          //setting the timeout to Maximum Value          
          insertOppRequest.timeout_x = 120000;

          //Execute the call and get the result
          insertOppResult = insertOppRequest.process(new List<OpportunityAutoPAPWSDL.OpportunityCreate_element>{oppJEDIRow});           

          system.debug('***RESULT' + insertOppResult[0].result);

          if(insertOppResult!=null
             && !insertOppResult.isEmpty()
             && insertOppResult[0].Result!='S')
          {

            //Create Log Entry
            logEntries.add(new Log__c(AppName__c = 'SFDC',
                                      ClassName__c = 'OpportunityQueueableHandler',
                                      Event__c = 'AutoPAP Service',
                                      EventType__c = 'AppException',
                                      Operation__c = 'AutoPAP Service Call',
                                      SFID__c = currentOpp.Id,
                                      Exception__c = 'SOA Service returned Status E'));

          }

        } //END FOR

        //Insert log entries
        insert logEntries;
        
      }
      //All the system.calloutExceptions are handled by the below Catch block. 
      //The callout is re-tried 3 times before logging an error
      catch (System.CalloutException ex) 
      {
        JEDIOpportunityCreationQueue job = new JEDIOpportunityCreationQueue(closedWonOpps);
        job.max = max;
        job.updateFlag = updateFlag;
        job.counter = counter + 1;
        if (max >= job.counter && !test.isRunningTest()) 
        {
          system.debug('***' + job.counter);
          System.enqueueJob(job);
        } 
        else 
        {
          system.debug('***' + job.counter);
          String message = ex.getMessage().length() > 4000 ?  ex.getMessage().subString(0, 4000) : ex.getMessage();
          /*retryService.logEntry( proj.Id,
                                Jacobs_Project__c.getSObjectType().getDescribe().getName(),
                                'SFDC Sales',
                                'ProjectQueueableHandler',
                                'B&P Approved',
                                'InfraException',
                                message,
                                'Callout Exception');*/
        }
        system.debug('Callout Exception: ' + ex);
      }
        
      // All other exceptions are handled in the below catch block. 
      // Here also the callout is retried 3 times before logging an error.
      catch (Exception ex) 
      {
          JEDIOpportunityCreationQueue job = new JEDIOpportunityCreationQueue(closedWonOpps);
          job.max = max;
          job.updateFlag = updateFlag;
          job.counter = counter + 1;
          if (max >= job.counter) 
          {
            system.debug('***' + job.counter);
            System.enqueueJob(job);
          } else 
          {
            system.debug('***' + job.counter);
            String message = ex.getMessage().length() > 4000 ?  ex.getMessage().subString(0, 4000) : ex.getMessage();
            //retryService.logEntry( proj.Id,
            //                       Jacobs_Project__c.getSObjectType().getDescribe().getName(),
            //                       'SFDC Sales',
            //                       'ProjectQueueableHandler',
            //                       'B&P Approved',
            //                       'InfraException',
            //                       message,
            //                       'Other Exception');
          }
          system.debug('Exception from callout:' + ex);
      }
        
      retryService.deleteRetryLogIfNeeded();
    
    }    
  }  
}