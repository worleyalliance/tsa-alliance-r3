/**************************************************************************************
Name: Campaign_TriggerTest
Version: 1.0 
Created Date: 11/22/2017
Function: To Test Campaign_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
* Manuel Johnson    11/22/2017      Original Version
* Madhu Shetty      12/15/2017      US2537: Deleted test methods newSalesPlanShouldCreatePlanWeeks and
                                    newSalesPlanShouldCreatePlanWeeksAndAssociateOpps. 
                                    Added test method newSalesPlanShouldAssociateOpps
*************************************************************************************/

@IsTest
public with sharing class Campaign_TriggerTest {
    //Constants
    private static final String PROFILE_ADMIN               = 'System Administrator';
    private static final String PERMISSION_SET_SALES_PLAN   = 'Sales Plan Create and Edit';
    private static final String USERNAME                    = 'salesBulkTest@jacobstestuser.net';
    
    // commented out as part of the  release 2 

    // private static final String AT_LOB                      = 'ATN';
    // private static final String AT_BU                       = 'ATN - International';
    // private static final String AT_SU                       = 'ATN - International - Professional Services';
    // private static final String BI_LOB                      = 'BIAF';
    // private static final String BI_BU                       = 'BIAF - Americas';
    // private static final String BI_SU                       = 'BIAF - Americas S-South Central';
    

    private static final String JESA_LOB                        = 'JESA';
    private static final String JESA_BU                         = 'JESA - Joint Venture';
    private static final String JESA_SU                         = 'JESA JV - Morocco';
    private static final String DIGI_LOB                        = 'Digital';
    private static final String DIGI_BU                         = 'JESA - Joiint Venture';
    private static final String DIGI_SU                         = 'JESA JV - Morocco';

    //Test data setup
    @testSetup
    static void setUpTestData(){
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User salesPlanUser = userBuilder.build().withProfile(PROFILE_ADMIN).withUserName(USERNAME).save().getRecord();
        
        //Create test records
        system.runAs(salesPlanUser){
            //Create Account
            TestHelper.AccountBuilder accBuilder = new TestHelper.AccountBuilder();
            Account acct = accBuilder.build().save().getRecord();
            
            //Create Opportunities in different business/selling units
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity oppBI = oppBuilder.build()
                .withAccount(acct.Id)
                .withName('test' + acct.id)
                .save().getOpportunity();
            Opportunity oppAT = oppBuilder.build()
                .withAccount(acct.Id)
                .withName('test' + acct.id)
                .withATLobBuSu()
                .withOpportunityClass('Class I').save().getOpportunity();
        }
    }
    
    //US2537: Method to test that Opportunities are tagged with a Sales Plan when a new Sales Plan is created
    @isTest
    private static void newSalesPlanShouldAssociateOpps(){
        User salesPlanUser = [SELECT Id FROM User WHERE UserName = :USERNAME Limit 1];
        
        //Start Test
        Test.startTest();
        
        System.runAs(salesPlanUser){
            
            //Create B&I Sales Plan for Business/Selling Unit
            TestHelper.CampaignBuilder camBuilder = new TestHelper.CampaignBuilder();
            Campaign camBI = camBuilder.build().save().getRecord();
            Campaign camAT = camBuilder.build().withATLobBu().save().getRecord();
        }
        
        //Stop Test
        Test.stopTest();
        
        //Get the Campaign
        // Campaign campaignAT = [SELECT Id, NumberOfOpportunities
        //                        FROM Campaign 
        //                        WHERE CreatedById = :salesPlanUser.Id AND pl_line_of_business__c = :JESA_LOB LIMIT 1];
        // Campaign campaignBI = [SELECT Id, NumberOfOpportunities
        //                        FROM Campaign 
        //                        WHERE CreatedById = :salesPlanUser.Id AND pl_line_of_business__c = :DIGI_LOB LIMIT 1];
        
        //Opportunities should be associated
        // System.assertEquals(1, campaignBI.NumberOfOpportunities);
        
        //Opportunities should be associated
        // System.assertEquals(1, campaignAT.NumberOfOpportunities);
    }
    
    // @isTest
    // private static void updateSalesPlanShouldDisassociateOpps(){
    //     User salesPlanUser = [SELECT Id FROM User WHERE UserName = :USERNAME Limit 1];
        
    //     //Start Test
    //     Test.startTest();
        
    //     System.runAs(salesPlanUser){
            
    //         //Create B&I Sales Plan for Business/Selling Unit
    //         TestHelper.CampaignBuilder camBuilder = new TestHelper.CampaignBuilder();
    //         Campaign camBI = camBuilder.build().save().getRecord();
            
    //         camBI.pl_selling_unit__c = DIGI_BU;
    //         update camBI;
    //     }
        
    //     //Stop Test
    //     Test.stopTest();
        
    //     //Get the Campaign
    //     Campaign campaignBI = [SELECT Id, NumberOfOpportunities
    //                            FROM Campaign 
    //                            WHERE CreatedById = :salesPlanUser.Id AND pl_line_of_business__c = :DIGI_LOB LIMIT 1];
        
    //     //Opportunities should be disassociated
    //     System.assertEquals(0, campaignBI.NumberOfOpportunities);
    // }
}