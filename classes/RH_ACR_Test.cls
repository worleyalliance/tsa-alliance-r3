/**************************************************************************************
Name: RH_ACR_Test
Function: Resource Hero - Test class related to RH_ACR

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-09-24          Original Version
*************************************************************************************/
@isTest
public class RH_ACR_Test {   
    /*static testMethod void test_contactInsert_UtilizeFalse() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', User__c = System.userInfo.getUserId(), AccountId = acc.Id));
        insert cons;
        
        //Confirm that no resource has been created
        List<ResourceHeroApp__Resource__c> verify = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(0, verify.size());
    }

    static testMethod void test_contactInsert_UtilizeTrue() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', User__c = System.userInfo.getUserId(), AccountId = acc.Id, Utilize_in_Resource_Hero__c = true));
        insert cons;
        
        //Confirm that no resource has been created
        List<ResourceHeroApp__Resource__c> verify = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(1, verify.size());
        system.assertEquals('Bill Kuehler', verify[0].Name);
        system.assertEquals(cons[0].Id, verify[0].Jacobs_Resource_Contact__c);
        system.assertEquals(cons[0].User__c, verify[0].ResourceHeroApp__User__c);
        
        //Confirm that resource lookup field on contact has been updated correctly and flag has been reset
        List<Contact> verify_contact = [SELECT Id, Resource_Hero_Record__c, Utilize_in_Resource_Hero__c FROM Contact];
        system.assertEquals(1, verify_contact.size());
        system.assertEquals(verify[0].Id, verify_contact[0].Resource_Hero_Record__c);
        system.assertEquals(false, verify_contact[0].Utilize_in_Resource_Hero__c);
    }
    
    
    static testMethod void test_contactInsert_UtilizeTrue_missingUser() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', AccountId = acc.Id, Utilize_in_Resource_Hero__c = true));
        
        //Test fail condition
        try {
            insert cons;
            system.assert(false);  //Should fail, if we make it here, we have a problem
        } catch(Exception e) {
            System.assert(e.getMessage().contains('User field must be populated before you can utilized a contact in Resource Hero.'));  
        }

        //Confirm that no records have been created
        List<ResourceHeroApp__Resource__c> verify_resource = [SELECT Id FROM ResourceHeroApp__Resource__c];
        system.assertEquals(0, verify_resource.size());
        
        List<Contact> verify_contact = [SELECT Id FROM Contact];
        system.assertEquals(0, verify_contact.size());
    }
    
    static testMethod void test_contactUpdate_missingUser() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', AccountId = acc.Id, Utilize_in_Resource_Hero__c = false));
        insert cons;

        cons[0].Utilize_in_Resource_Hero__c = true;
        
        //Test fail condition
        try {
            update cons;
            system.assert(false);  //Should fail, if we make it here, we have a problem
        } catch(Exception e) {
            System.assert(e.getMessage().contains('User field must be populated before you can utilized a contact in Resource Hero.'));  
        }

        //Confirm that no records have been created
        List<ResourceHeroApp__Resource__c> verify_resource = [SELECT Id FROM ResourceHeroApp__Resource__c];
        system.assertEquals(0, verify_resource.size());
    }
    
    
    static testMethod void test_contactUpdate_noExistingResource() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', User__c = System.userInfo.getUserId(), AccountId = acc.Id, Utilize_in_Resource_Hero__c = false));
        insert cons;

        //Verify that a Resource has not been created        
        List<ResourceHeroApp__Resource__c> verify_before = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(0, verify_before.size());

        cons[0].Utilize_in_Resource_Hero__c = true;
        update cons;
        
        //Confirm that a resource has been created
        List<ResourceHeroApp__Resource__c> verify_after = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(1, verify_after.size());
        system.assertEquals('Bill Kuehler', verify_after[0].Name);
        system.assertEquals(cons[0].Id, verify_after[0].Jacobs_Resource_Contact__c);
        system.assertEquals(cons[0].User__c, verify_after[0].ResourceHeroApp__User__c);
        
        //Confirm that resource lookup field on contact has been updated correctly and flag has been reset
        List<Contact> verify_contact = [SELECT Id, Resource_Hero_Record__c, Utilize_in_Resource_Hero__c FROM Contact];
        system.assertEquals(1, verify_contact.size());
        system.assertEquals(verify_after[0].Id, verify_contact[0].Resource_Hero_Record__c);
        system.assertEquals(false, verify_contact[0].Utilize_in_Resource_Hero__c);
    }
    
    
    static testMethod void test_contactUpdate_ExistingResource() {
        //Create test account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', User__c = System.userInfo.getUserId(), AccountId = acc.Id, Utilize_in_Resource_Hero__c = false));
        insert cons;

        //Verify that a Resource has not been created        
        List<ResourceHeroApp__Resource__c> verify_before = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(0, verify_before.size());
        
        //Manually create resource record
        List<ResourceHeroApp__Resource__c> res = new List<ResourceHeroApp__Resource__c>();
        res.add(new ResourceHeroApp__Resource__c(Name = 'Bill Kuehler', ResourceHeroApp__Weekly_Target_Min_Hours__c = 30, ResourceHeroApp__Weekly_Target_Max_Hours__c = 40, Jacobs_Resource_Contact__c = cons[0].Id));
        insert res;

        cons[0].Utilize_in_Resource_Hero__c = true;
        update cons;
        
        //Confirm that no resource has been created and that contact has been updated
        List<ResourceHeroApp__Resource__c> verify_after = [SELECT Id, Name, ResourceHeroApp__Weekly_Target_Max_Hours__c, ResourceHeroApp__Weekly_Target_Min_Hours__c, ResourceHeroApp__User__c, Jacobs_Resource_Contact__c FROM ResourceHeroApp__Resource__c];
        system.assertEquals(1, verify_after.size());
        system.assertEquals(res[0].Id, verify_after[0].Id);
        
        //Confirm that resource lookup field on contact has been updated correctly and flag has been reset
        List<Contact> verify_contact = [SELECT Id, Resource_Hero_Record__c, Utilize_in_Resource_Hero__c FROM Contact];
        system.assertEquals(1, verify_contact.size());
        system.assertEquals(res[0].Id, verify_contact[0].Resource_Hero_Record__c);
        system.assertEquals(false, verify_contact[0].Utilize_in_Resource_Hero__c);
    }*/
}