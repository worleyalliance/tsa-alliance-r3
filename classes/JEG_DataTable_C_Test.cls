/**************************************************************************************
Name: JEG_DataTable_C_Test
Version: 1.0 
Created Date: 08.03.2017
Function: Unit test clas for JEG_DataTable_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           08.03.2017         Original Version
*************************************************************************************/
@IsTest
private class JEG_DataTable_C_Test {

    @IsTest
    private static void shouldCalculateDataForGroupAttritionTable() {
        //given
        String fieldSetName =
                Schema.getGlobalDescribe().get('AT_Segment_Attrition__c').getDescribe().fieldSets.getMap().values()[0].name;

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 = 
            attritionBuilder
                .build()
                .withGroupId(atGroup.Id)
                .withFiscalYear(2015)
                .save()
                .getRecord();
        AT_Segment_Attrition__c groupAttrition2 =
            attritionBuilder
                .build()
                .withGroupId(atGroup.Id)
                .withFiscalYear(2015)
                .save()
                .getRecord();

        //when
        DataTable_Service.TableData result =
                JEG_DataTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        'Group__c',
                        'AT_Segment_Attrition__c',
                        true,
                        true,
                        null
                );

        //then
        System.assertNotEquals(0, result.fieldDescriptions.size());
        System.assertEquals(2, result.rows.size());
        System.assertNotEquals(0, result.totalRow.values().size());
    }
}