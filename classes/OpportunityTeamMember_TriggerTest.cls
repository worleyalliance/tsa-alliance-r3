/**************************************************************************************
Name: OpportunityTeamMember_TriggerTest
Version: 1.0
Created Date: 10/04/2018
Function: Test class for OpportunityTeamMember_TriggerHandler

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    02/05/2018      Original Version
* Pradeep Shetty     09/20/2018      Updated class to reduce the number of SOQL queries
*************************************************************************************/
@IsTest
public class OpportunityTeamMember_TriggerTest {
        
    //Constants
    private static final String PROFILE_SALES_SUPER_USER         = 'Sales Super User';
    private static final String USERNAME1                        = 'teamMemberTriggerTest1@jacobstestuser.net';
    private static final String USERNAME2                        = 'teamMemberTriggerTest2@jacobstestuser.net';
    private static final String USERNAME3                        = 'teamMemberTriggerTest3@jacobstestuser.net';
    private static final String USERNAME4                        = 'teamMemberTriggerTest4@jacobstestuser.net';
    private static final String COUNTRY1                         = 'US';
    private static final String COUNTRY2                         = 'CA';
    private static final String MEMBER_ROLE_BD_COORDINATOR       = 'BD Coordinator';
    private static final String MEMBER_ROLE_CAPTURE_MANAGER      = 'Capture Manager';
    private static final String MEMBER_ROLE_CONSTRUCTION_MANAGER = 'Construction Manager';
    private static final String MEMBER_ROLE_OPP_OWNER            = 'Opportunity Owner';
    private static final String MEMBER_ACCESS_ALL                = 'All';
    private static final String MEMBER_ACCESS_READ               = 'Read';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        
      //Get user builder
      TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();

      //List for user creation
      List<User> testUserList = new List<User>();

      //Create test user records. These are not saved as yet.
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME1).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME2).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME3).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME4).getRecord());
      
      //Insert Users
      insert testUserList;

      //Builder for Account
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();

      //Builder for Opportunity
      TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();

      //List of Opportunities
      List<Opportunity> opportunityList = new List<Opportunity>();

      //Create test records
      system.runAs(testUserList[0]){    
        
        Account acc = accountBuilder.build().save().getRecord();

        opportunityList.add(oppBuilder.build().withAccount(acc.Id).getOpportunity());
        opportunityList.add(oppBuilder.build().withAccount(acc.Id).getOpportunity());

        insert opportunityList;

        opportunityList[1].Parent_Opportunity__c = opportunityList[0].Id;
        update opportunityList[1];
      }
    }

    /*
    * This test checks that OTMs on Child Opportunities are inserted with correct role and access level 
    * when OTM is added to parent Opp
    */
    @isTest
    private static void testinsertTeamMember() {
      //Get test users
      List<User> testUserList = [Select Id From User Where Username in (:USERNAME1, :USERNAME2, :USERNAME3, :USERNAME4) Order By Username];

      Test.startTest();
      System.runAs(testUserList[0]){
        //Get the parent Opportunity
        Opportunity opp = [Select Id
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        // Add team members to the Parent Opportunity
        List<OpportunityTeamMember> lstNewOTM = new List<OpportunityTeamMember>();

        OpportunityTeamMember newOTM1  = new OpportunityTeamMember();
        newOTM1.OpportunityAccessLevel ='Read';
        newOTM1.OpportunityId          = opp.Id;
        newOTM1.TeamMemberRole         ='Construction Manager';
        newOTM1.UserId                 = testUserList[1].Id;  
        lstNewOTM.add(newOTM1); 
        
        OpportunityTeamMember newOTM2  = new OpportunityTeamMember();
        newOTM2.OpportunityAccessLevel ='Edit';
        newOTM2.OpportunityId          = opp.Id;
        newOTM2.TeamMemberRole         ='BD Coordinator';
        newOTM2.UserId                 = testUserList[2].Id;
        lstNewOTM.add(newOTM2);

        // Adding muliple to cover bulk operations
        OpportunityTeamMember newOTM3  = new OpportunityTeamMember();
        newOTM3.OpportunityAccessLevel ='Edit';
        newOTM3.OpportunityId          = opp.Id;
        newOTM3.TeamMemberRole         ='Capture Manager';
        newOTM3.UserId                 = testUserList[3].Id;
        lstNewOTM.add(newOTM3);

        // Insert team members in Parent Opportunity
        insert lstNewOTM;
        
        //Get the Child Opportunity
        Opportunity childOpp = [Select Id
                                From Opportunity 
                                where Parent_Opportunity__c =:opp.Id 
                                Limit 1];

        //DE612: Loop through child OTM and check that team members were added with the right role and access
        for( OpportunityTeamMember childOTM : [Select Id, 
                                                      TeamMemberRole,
                                                      OpportunityAccessLevel,   
                                                      OpportunityID, 
                                                      UserID 
                                              From OpportunityTeamMember 
                                              Where OpportunityID = :childOpp.ID]){

          if(childOTM.UserId == testUserList[0].Id){
            system.assertEquals(MEMBER_ROLE_OPP_OWNER, childOTM.TeamMemberRole);
            system.assertEquals(MEMBER_ACCESS_ALL, childOTM.OpportunityAccessLevel);
          }
          else if(childOTM.UserId == testUserList[1].Id){
            system.assertEquals(MEMBER_ROLE_CONSTRUCTION_MANAGER, childOTM.TeamMemberRole);
            system.assertEquals(MEMBER_ACCESS_READ, childOTM.OpportunityAccessLevel);
          }
          else if(childOTM.UserId == testUserList[2].Id){
            system.assertEquals(MEMBER_ROLE_BD_COORDINATOR, childOTM.TeamMemberRole);
            system.assertEquals(MEMBER_ACCESS_ALL, childOTM.OpportunityAccessLevel);
          }
          else if(childOTM.UserId == testUserList[3].Id){
            system.assertEquals(MEMBER_ROLE_CAPTURE_MANAGER, childOTM.TeamMemberRole);
            system.assertEquals(MEMBER_ACCESS_ALL, childOTM.OpportunityAccessLevel);
          }
          else{
            system.assert(false, 'Test Failed: Team member not created');
          }
        } //END FOR

        //DE612: Loop through parent team members and check the access level
        for(OpportunityTeamMember pOTM: [Select UserId, 
                                                OpportunityAccessLevel 
                                         From OpportunityTeamMember 
                                         Where OpportunityID = :opp.Id]){
            if(pOTM.UserId == testUserList[1].Id){
                system.assertEquals(MEMBER_ACCESS_READ, pOTM.OpportunityAccessLevel);
            }
            else{
                system.assertEquals(MEMBER_ACCESS_ALL, pOTM.OpportunityAccessLevel);                
            }

        }
        
        // Error check on inserting Parent's team member when the user ID is null
        try{
            OpportunityTeamMember newOTM4= new OpportunityTeamMember();
            newOTM4.OpportunityAccessLevel ='Edit';
            newOTM4.OpportunityId = opp.Id;
            newOTM4.TeamMemberRole ='Capture Manager';
            newOTM4.UserId = null;
            insert newOTM4;   
        }
        catch(Exception e)
        {   
            Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed') ? true : false;   
            System.AssertEquals(expectedExceptionThrown, true);
        }
      }
      Test.stopTest();

    }

    /*
    * This test checks that OTMs on Child Opportunities are deleted 
    * when OTM is deleted from a parent Opp
    */
    @isTest
    private static void testDeleteTeamMember() {
      //Get test users
      List<User> testUserList = [Select Id From User Where Username in (:USERNAME1, :USERNAME2) Order By Username];

      //Get the Opportunity Team Member builder
      TestHelper.OpportunityTeamMemberBuilder otmBuilder = new TestHelper.OpportunityTeamMemberBuilder();

      //Start Test
      Test.startTest();

      //Run as Test user with Username1
      System.runAs(testUserList[0]){

        //Get the Parent Opportunity
        Opportunity opp = [Select Id, 
                                  AccountId 
                           From Opportunity 
                           Where Parent_Opportunity__c = Null 
                           Limit 1];

        //Add a team member to this Opportunity
        OpportunityTeamMember newOTM = otmBuilder.build().withOpportunity(opp.Id).withUser(testUserList[1].Id).getRecord();
        insert newOTM;

        //Check that OTM is added to the Child 
        Opportunity childOpp = [Select Id 
                                From Opportunity 
                                Where Parent_Opportunity__c = :opp.id 
                                Limit 1];

        //Child OTM
        OpportunityTeamMember childOTM = [Select Id
                                          From OpportunityTeamMember 
                                          Where OpportunityId = :childOpp.Id 
                                          And UserId = :testUserList[1].Id];

        //Check that ChildOTM has been created
        System.assert(childOTM!=null);

        //Delete Parent OTM
        delete newOTM;

        //Get ChildOTM again
        List<OpportunityTeamMember> childOTMPostDelete = [Select Id
                                                          From OpportunityTeamMember 
                                                          Where OpportunityId = :childOpp.Id 
                                                          And UserId = :testUserList[1].Id];  

        //Assert
        System.assert(childOTMPostDelete.isEmpty());
      }
      Test.stopTest();
    }

    /*
    * This test checks that OTMs on Child Opportunities are updated 
    * when OTM is updated on a parent Opp
    */
    @isTest
    private static void testUpdateTeamMember() {
      //Get test users
      List<User> testUserList = [Select Id From User Where Username in (:USERNAME1, :USERNAME2) Order By Username];    

      //Get the Opportunity Team Member builder
      TestHelper.OpportunityTeamMemberBuilder otmBuilder = new TestHelper.OpportunityTeamMemberBuilder();

      //Start Test
      Test.startTest();

      //Run as USERNAME1
      System.runAs(testUserList[0]){

        //Get the parent opp
        Opportunity opp = [Select Id
                           From Opportunity 
                           Where Parent_Opportunity__c = Null 
                           Limit 1];

        //Add a team member to this Opportunity
        OpportunityTeamMember newOTM = otmBuilder
                                       .build()
                                       .withOpportunity(opp.Id)
                                       .withUser(testUserList[1].Id)
                                       .withAccess(MEMBER_ACCESS_READ)
                                       .getRecord();
        insert newOTM;

        //Check that OTM is added to the Child 
        Opportunity childOpp = [Select Id 
                                From Opportunity 
                                Where Parent_Opportunity__c = :opp.id 
                                Limit 1];

        //Child OTM
        OpportunityTeamMember childOTM = [Select Id
                                          From OpportunityTeamMember 
                                          Where OpportunityId = :childOpp.Id 
                                          And UserId = :testUserList[1].Id];

        //Check that ChildOTM has been created
        System.assert(childOTM!=null);

        //Prepare parent OTM for update
        newOTM.OpportunityAccessLevel = MEMBER_ACCESS_ALL;
        newOTM.TeamMemberRole         = MEMBER_ROLE_CAPTURE_MANAGER; 

        update newOTM;   

        //Requery Child OTM
        OpportunityTeamMember updatedChildOTM = [Select Id,
                                                        OpportunityAccessLevel,
                                                        TeamMemberRole
                                                 From OpportunityTeamMember 
                                                 Where OpportunityId = :childOpp.Id 
                                                 And UserId = :testUserList[1].Id]; 

        //validate if team member in child is updated
        System.assertEquals(newOTM.TeamMemberRole, updatedChildOTM.TeamMemberRole);
        System.assertEquals(newOTM.OpportunityAccessLevel, updatedChildOTM.OpportunityAccessLevel);                                                            

      }

      Test.stopTest();
    } 
}