/**************************************************************************************
Name: Account_TriggerHandler
Version: 1.0 
Created Date: 27.02.2017
Function: Account Trigger handler

Modification Log: 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           02.27.2017          Original Version
* Pradeep Shetty            07.27.2017          DE168 - Added a try catch block to throw custom exception 
* Madhu Shetty              08.11.2017          (US654) - Added validation to block deletion of Account if an associated 
                                                Opportunity exists for that Account.
* Chris Cornejo             10.04.2017          DE192 - Updated to add Partner and Competitor, Internal record types to Account Call out
* Jignesh Suvarna           04.16.2018          Modified Version - US1825 - Populate Account Group Name
* Jignesh Suvarna           05.16.2018          US2080 - added checks to set status and CH2M status to Unverified when Do Not Create Oracle Account is modified
* Manuel Johnson            07.07.2018          US2272 - add check to prevent users from deleting verified accounts
*************************************************************************************/

public class Account_TriggerHandler {

    private static final String LOCATION = 'Location ';

    @TestVisible
    private List<String> folderPaths;

    private SharePoint_Service service = new SharePoint_Service();

    @TestVisible
    private static Set<Id> mergedAccountIds = new Set<Id>();

    private static Account_TriggerHandler handler;

   /*
    * Singleton like pattern
    */
    public static Account_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Account_TriggerHandler();
        }
        return handler;
    }

    /*
    * Actions performed before the account is inserted
    */
    // SIVA KATNENI - The operating Unit is now populated on Locations and hence this logic is commented out
  /*
    public void onBeforeInsert(List<Account> accounts){
        populateOperatingUnit(accounts);
    }
  */  

    public void onAfterDelete(List<Account> deletedAccounts){
        findMergedWinnerAccountIds(deletedAccounts);
    }

   /*
    * Actions performed on after account being
    */
    public void onAfterInsert(Map<Id, Account> accounts) {
        createLocation(accounts);
    }

    /*
    * Actions performed on after update
    */
    public void onAfterUpdate(Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap) {
        // Create a new location if billing address of account is changed
        addLocationOnAddressChange(newAccountMap, oldAccountMap);
        updatePrimaryFlagOnMergeAccountLocations(newAccountMap.values());
    }

    /*
    * Actions performed on after update
    */
    public void onAfterUpsert(List<Account> newAccounts, List<Account> oldAccounts, Boolean isInsert) {
        // Create a new location if billing address of account is changed
        makeAccountCallout(newAccounts, oldAccounts, isInsert);
    }
    
    /*
    * Actions performed on after delete
    */
  /* Commenting out as the account delete scenario is not being handled by callout
    public void onAfterDelete(List<Account> delAccounts){
        makeDeleteCallout(delAccounts);
    }
  */

    /*
    * Populate operating unit before the account is inserted based on custom settings
    */
 /* 
    private void populateOperatingUnit(List<account> accounts){
        Map<String, Country_Operating_Unit_Mapping__c> cou = Country_Operating_Unit_Mapping__c.getAll();
        Map<string, string> couMap = new Map<string, string>();
        for (Country_Operating_Unit_Mapping__c co : cou.values()) {
            couMap.put(co.Country_Code__c, co.Operating_Unit__c);
        }

        for (Account acc: accounts){
            acc.Operating_Unit__c = couMap.get(acc.BillingCountryCode);
        }
    }
 */
    /*
    * Creating location on Account Creation
    */
    private void createLocation(Map<Id, Account> accounts) {
        List<SiteLocation__c> locList = new List<SiteLocation__c>();

        for (account acc: accounts.values()) {
            if(acc.BillingStreet != null && acc.BillingCountryCode != null){
                SiteLocation__c loc = new SiteLocation__c();
                loc.Street_Address__c = acc.BillingStreet;
                loc.City__c = acc.BillingCity;
                loc.Postal_Code__c = acc.BillingPostalCode;
                loc.Country__c = acc.BillingCountryCode;
                if (acc.BillingCountryCode == 'US') {
                    loc.State_Province__c = acc.BillingStateCode;
                } else if (acc.BillingCountryCode != 'US') {
                    loc.State_Province__c = acc.BillingState;
                }
                loc.Primary__c = TRUE;
                loc.Account__c = acc.id;
                loc.Operating_Unit__c = acc.Operating_Unit__c;
                if (String.isNotBlank(acc.BillingCity)) {
                    loc.Name = LOCATION + acc.BillingCity;
                } else if (acc.BillingStateCode != null) {
                    loc.Name = LOCATION + acc.BillingStateCode;
                } else if (acc.BillingState != null) {
                    loc.Name = LOCATION + acc.BillingState;
                } else if (acc.BillingCountryCode != null) {
                    loc.Name = LOCATION + acc.BillingCountryCode;
                }
                locList.add(loc);
            }
        }
        insert locList;
    }

    private void findMergedWinnerAccountIds(List<Account> deletedAccounts){
        Boolean SystemAdmin = userinfo.getprofileid().equals('00e1U000001B0zjQAC');
        System.debug(userinfo.getprofileid());
        
        for(Account acc : deletedAccounts){
            if(acc.MasterRecordId != null){
                mergedAccountIds.add(acc.MasterRecordId);
            }
            else{
                if(acc.of_Opportunities__c > 0){
                    acc.adderror(String.format(Label.OpportunityOrProjectExistsForAccount,  new String[]{'Opportunities',  String.valueOf(acc.of_Opportunities__c)}));
                }
                else if(acc.Number_of_Projects__c > 0){
                    acc.adderror(String.format(Label.OpportunityOrProjectExistsForAccount, new String[]{'Projects', String.valueOf(acc.Number_of_Projects__c)}));
                }
                //prevent anyone besides System Admins from deleting
                else if((acc.Status__c == 'Verified' || acc.CH2M_Status__c == 'Verified') && !SystemAdmin){
                    acc.adderror(Label.VerifiedAccountCannotBeDeleted);
                }
            }
        }
    }

    /*
    * Create a new location if billing address of account is changed and mark it as primary.
    * Check to make sure there aren't any location records with same address
    */
    private void addLocationOnAddressChange(Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap) {

        List<Id> changedIds = new List<Id>();
        Map<String, Account> accountStrMap = new Map<String, Account>();
        Set<String> locationAddressString = new Set<String>();
        List<SiteLocation__c> locationInsertList = new List<SiteLocation__c>();

        for (account acc: newAccountMap.values()) {
            String str1;
            if (acc.BillingCountryCode == 'US') {
                str1 = acc.BillingStreet + acc.BillingCity + acc.BillingStateCode + acc.BillingCountryCode + acc.BillingPostalCode;
            } else if (acc.BillingCountryCode != 'US') {
                str1 = acc.BillingStreet + acc.BillingCity + acc.BillingState + acc.BillingCountryCode + acc.BillingPostalCode;
            }
            system.debug(str1);
            String str2;
            if (oldAccountMap.get(acc.id).BillingCountryCode == 'US') {
                str2 = oldAccountMap.get(acc.id).BillingStreet + oldAccountMap.get(acc.id).BillingCity + oldAccountMap.get(acc.id).BillingStateCode +
                        oldAccountMap.get(acc.id).BillingCountryCode + oldAccountMap.get(acc.id).BillingPostalCode;
            } else if (oldAccountMap.get(acc.id).BillingCountryCode != 'US') {
                str2 = oldAccountMap.get(acc.id).BillingStreet + oldAccountMap.get(acc.id).BillingCity + oldAccountMap.get(acc.id).BillingState +
                        oldAccountMap.get(acc.id).BillingCountryCode + oldAccountMap.get(acc.id).BillingPostalCode;
            }
            system.debug(str2);
            if (str1 != str2) {
                changedIds.add(acc.id);
                system.debug(changedIds);

                accountStrMap.put(str1, acc);
                system.debug(accountStrMap);
            }
        }

        for (SiteLocation__c sl : [
                SELECT
                        Id, 
                        City__c, 
                        Country__c, 
                        Street_Address__c, 
                        State_Province__c, 
                        Postal_Code__c, 
                        Operating_Unit__c
                FROM SiteLocation__c
                WHERE Account__c IN :changedIds
        ]) {
            String str;
            str = sl.Street_Address__c + sl.City__c + sl.State_Province__c + sl.Country__c + sl.Postal_Code__c;
            locationAddressString.add(str);
            system.debug(str);
        }

        //If any of the new address on address change match already existing location address, then remove them from the map
        for (String str : locationAddressString) {
            if (accountStrMap.ContainsKey(str)) {
                accountStrMap.remove(str);
            }
        }

        for (String str : accountStrMap.keySet()) {
            if(accountStrMap.get(str).BillingStreet != null && accountStrMap.get(str).BillingCountryCode != null){
                SiteLocation__c loc = new SiteLocation__c();
                loc.Street_Address__c = accountStrMap.get(str).BillingStreet;
                loc.City__c = accountStrMap.get(str).BillingCity;
                loc.Postal_Code__c = accountStrMap.get(str).BillingPostalCode;
                loc.Country__c = accountStrMap.get(str).BillingCountryCode;
                if (accountStrMap.get(str).BillingCountryCode == 'US') {
                    loc.State_Province__c = accountStrMap.get(str).BillingStateCode;
                } else if (accountStrMap.get(str).BillingCountryCode != 'US') {
                    loc.State_Province__c = accountStrMap.get(str).BillingState;
                }
                loc.Primary__c = TRUE;
                loc.Account__c = accountStrMap.get(str).id;
                loc.Operating_Unit__c = accountStrMap.get(str).Operating_Unit__c;
                if (accountStrMap.get(str).BillingCity != null) {
                    loc.Name = 'Location ' + accountStrMap.get(str).BillingCity;
                } else if (accountStrMap.get(str).BillingStateCode != null) {
                    loc.Name = 'Location ' + accountStrMap.get(str).BillingStateCode;
                } else if (accountStrMap.get(str).BillingState != null) {
                    loc.Name = 'Location ' + accountStrMap.get(str).BillingState;
                } else if (accountStrMap.get(str).BillingCountryCode != null) {
                    loc.Name = 'Location ' + accountStrMap.get(str).BillingCountryCode;
                }
                locationInsertList.add(loc);
            }
        }

        try{
            insert locationInsertList;
        }
        catch(DmlException exc){
            if(exc.getMessage().contains('Street Address is required')){
                throw new AccountTriggerException(Label.Location_Street_Address_Error);
            }
            else
            {
                throw new AccountTriggerException(exc.getMessage());
            }    
        }        

    }

    /*
    * Make a callout from Account Service to Oracle
    */
    private void makeAccountCallout(List<Account> newAccounts, List<Account> oldAccounts, Boolean isInsert) {
        Set<ID> RTIds = new Set<ID>();
        //Added 2 additional record types - Competitor and Partner
        for (RecordType rt : [select id from Recordtype where SobjectType = 'Account' and DeveloperName in ('New_Client', 'Universal', 'Client','Partner','Competitor','Internal')]) {
            RTIds.add(rt.id);
        }
        system.debug(RTIds);
        List<Account> updateAccount = new List <Account>();
        
        for (Integer i = 0; i < newAccounts.size(); i++) {
            system.debug(newAccounts[i].RecordTypeId);
            //JS - US1825 - added new condition for Account Group Name  
            //PS - US1966 - added condition to prevent integration call if Do_Not_Create_Oracle_Account__c = true
            if (!newAccounts[i].Do_Not_Create_Oracle_Account__c &&
                (isInsert || mergedAccountIds.contains(newAccounts[i].Id) 
                || newAccounts[i].Status__c != oldAccounts[i].Status__c
                || newAccounts[i].CH2M_Status__c != oldAccounts[i].CH2M_Status__c
                || newAccounts[i].BillingStreet != oldAccounts[i].BillingStreet
                || newAccounts[i].BillingCity != oldAccounts[i].BillingCity
                || newAccounts[i].BillingState != oldAccounts[i].BillingState
                || newAccounts[i].BillingCountry != oldAccounts[i].BillingCountry
                || newAccounts[i].BillingPostalCode != oldAccounts[i].BillingPostalCode
                || newAccounts[i].Name != oldAccounts[i].Name
                || newAccounts[i].Account_Group__c != oldAccounts[i].Account_Group__c 
                || newAccounts[i].Do_Not_Create_Oracle_Account__c != oldAccounts[i].Do_Not_Create_Oracle_Account__c)
                && (newAccounts[i].Status__c == 'Verified' || newAccounts[i].CH2M_Status__c == 'Verified')
                && RTIds.contains(newAccounts[i].RecordTypeId) 
                && !Test.isRunningTest()) {
                AccountQueueableHandler job = new AccountQueueableHandler(newAccounts[i]);
                job.max = 3;
                ID jobID = System.enqueueJob(job);
            }
        }
    }

    private void updatePrimaryFlagOnMergeAccountLocations(List<Account> accounts){
        if(!mergedAccountIds.isEmpty()) {
            Map<Id, List<SiteLocation__c>> locationsByAccountId = getMergedAccountPrimaryLocations();
            List<SiteLocation__c> duplicatePrimaryLocations = new List<SiteLocation__c>();
            for (Account acc : accounts) {
                if (mergedAccountIds.contains(acc.Id) && locationsByAccountId.containsKey(acc.Id)) {
                    duplicatePrimaryLocations.addAll(findDuplicatePrimaryLocation(acc, locationsByAccountId.get(acc.Id)));
                }
            }
            updatePrimaryFlagOnMergedAccountLocations(duplicatePrimaryLocations);
        }
    }

    private Map<Id, List<SiteLocation__c>> getMergedAccountPrimaryLocations(){
        List<SiteLocation__c> locations = [
                SELECT
                        Account__c, Primary__c,
                        Street_Address__c, City__c, Postal_Code__c, Country__c,
                        State_Province__c, Operating_Unit__c
                FROM SiteLocation__c
                WHERE Account__c IN :mergedAccountIds AND Primary__c = true
        ];
        Map<Id, List<SiteLocation__c>> locationsByAccountId = new Map<Id, List<SiteLocation__c>>();
        for(SiteLocation__c location : locations){
            if(!locationsByAccountId.containsKey(location.Account__c)){
                locationsByAccountId.put(location.Account__c, new List<SiteLocation__c>());
            }
            locationsByAccountId.get(location.Account__c).add(location);
        }
        return locationsByAccountId;
    }

    private List<SiteLocation__c> findDuplicatePrimaryLocation(Account acc, List<SiteLocation__c> locations){
        List<SiteLocation__c> duplicatePrimaryLocations = new List<SiteLocation__c>();
        Boolean isPrimaryLocationFound = false;
        if(locations.size() > 1){
            for(SiteLocation__c location : locations){
                if( isPrimaryLocationFound ||
                    !(  isEqual(acc.BillingStreet, location.Street_Address__c)
                    &&  isEqual(acc.BillingCity, location.City__c)
                    &&  isEqual(acc.BillingPostalCode, location.Postal_Code__c)
                    &&  isEqual(acc.BillingCountryCode, location.Country__c)
                    &&  isEqual(acc.Operating_Unit__c, location.Operating_Unit__c))
                ){
                    duplicatePrimaryLocations.add(location);
                } else {
                    isPrimaryLocationFound = true;
                }
            }
        }
        if(duplicatePrimaryLocations.size() == locations.size() && !duplicatePrimaryLocations.isEmpty()){
            duplicatePrimaryLocations.remove(0);
        }
        return duplicatePrimaryLocations;
    }

    private void updatePrimaryFlagOnMergedAccountLocations(List<SiteLocation__c> locations){
        for(SiteLocation__c location : locations){
            location.Primary__c = false;
        }
        update locations;
    }

    private Boolean isEqual(String value1, String value2){
        if(value1 == null && value2 == null){
            return true;
        } else if(value1 == null){
            return false;
        } else {
            return value1.equals(value2);
        }
    }


    public class AccountTriggerException extends Exception {}

    /*
    * Make a callout from Account Service to Oracle on Account Deletion
    */
  /* SIVA KATNENI - Commenting out as the account delete scenario is not being handled now
    private void makeDeleteCallout(List<Account> delAccounts){
        for(Integer i = 0; i < delAccounts.size(); i++){
            if(delAccounts[i].MasterRecordId ==null && delAccounts[i].OracleAccountID__c != null){
                AccountQueueableHandler job = new AccountQueueableHandler(delAccounts[i]);
                job.max = 3;
                job.accountDelete = TRUE;
                ID jobID = System.enqueueJob(job);
            }
        }
    }      
   */

}