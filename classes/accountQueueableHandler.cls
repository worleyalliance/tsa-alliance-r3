/**************************************************************************************
Name: accountQueueableHandler
Version: 1.0 
Created Date: 25.04.2017
Function: This handler actually makes the callout to the Account service of Oracle. Handles new Account creation,
new primary location creation and any updates to them. It also handles delete scenarios. 

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04/25/2017      Original Version
* Raj Vakati        08/22/2017      Modified Version - US 607
* Pradeep Shetty    08/30/2017       Modified Version - US 782 - Populate Site Id
* Jignesh Suvarna   04/16/2018      Modified Version - US1825 - Populate Account Group Name
* Jignesh Suvarna   04/27/2018      Modified Version - US1826 - Adding Ch2m oracle details
* Madhu Shetty      04/19/2018      Modified Version - DE412 - Code added for marking account as unverified if an error occurs while calling the GFS web service.
***************************************************************************************/
public class accountQueueableHandler implements OracleQueueableHandler, Database.AllowsCallouts {
    /*
* Project passed on class creation (the actual project from the Trigger)
*/
    private Account acc;
    public Integer max;
    private Integer counter = 1;
    public Boolean locationChange;
    public SiteLocation__c location { get; set; }
    private String AddressExists = JacobsOracleConfigurationHelper.getConfiguration('AddressExists');
    private String AccountExists = JacobsOracleConfigurationHelper.getConfiguration('AccountExists');
    
    private OracleCalloutRetryService retryService = new OracleCalloutRetryService();
    
    /*
* Constructor
*/
    public AccountQueueableHandler(Account acc) {
        this.acc = acc;
        System.debug(acc);
    }
    
    public AccountQueueableHandler(){}
    
    public void setObject(sObject obj){
        this.acc = (Account) obj;
    }
    
    public void setRetrySyncLog(RetrySyncLog__c retrySyncLog){
        retryService.setRetryLog(retrySyncLog);
    }
    
    
    public void execute(QueueableContext context) {
        System.debug(acc);
        NewAccount_WS_XSD.AccountSetupResponse_element result = new NewAccount_WS_XSD.AccountSetupResponse_element();
        NewAccount_WS.ProcessAccountReqABCS_pt request = new NewAccount_WS.ProcessAccountReqABCS_pt();
        string status;
        string errMessage;
        string chStatus;
        string chErrMessage;
        List<SiteLocation__c> locList = new List<SiteLocation__c>();
        
        if (locationChange == TRUE) {
            locList.add(location);
        } else {
            locList = [
                SELECT  Id, City__c,Name, Country__c, County__c, Region__c,
                State_Province__c, Street_Address__c, Primary__c,
                Address_ID__c, Postal_Code__c, Operating_Unit__c
                FROM SiteLocation__c
                WHERE Account__c = :acc.Id
            ];
        }
        
        try {
            
            //PartySetup Parameters
            List<NewAccount_WS_XSD.PartySetup_element> partyList = new List<NewAccount_WS_XSD.PartySetup_element>();
            //PartyRecordParameters
            NewAccount_WS_XSD.PartySetup_element partyRecordParameters = new NewAccount_WS_XSD.PartySetup_element();
            partyRecordParameters.accountinfo = new List<NewAccount_WS_XSD.Accountinfo_element>();
            //AccountInfoRecordParameters
            NewAccount_WS_XSD.Accountinfo_element accountInfoRecordParameters = new NewAccount_WS_XSD.Accountinfo_element();
            accountInfoRecordParameters.accountName = acc.name;
            //JS US1826 - Added below Flags to call pass details to Jacobs/Ch2m oracle 
            if(acc.Status__c == 'Verified'){
                accountInfoRecordParameters.JacobsFlag = 'Y';
            }
            else{
                accountInfoRecordParameters.JacobsFlag = 'N';
            }

            if(acc.CH2M_Status__c == 'Verified'){
                accountInfoRecordParameters.CH2MFlag = 'Y';
            }
            else{
                accountInfoRecordParameters.CH2MFlag = 'N';
            }            
            //put if logic if exists
            if (acc.OracleAccountID__c != null) {
                accountInfoRecordParameters.JBAccountId = acc.OracleAccountID__c;
            }
            accountInfoRecordParameters.sfAccountId = acc.id;
            //JS Start US1826 - CH2M_Oracle_Account_ID__c, CH2M_Oracle_Account_Number__c
            if (acc.CH2M_Oracle_Account_ID__c != null) {
                accountInfoRecordParameters.CHAccountId = acc.CH2M_Oracle_Account_ID__c;
            }
            //JS End US1826
           
            // JS START - US1825
            system.debug('****Account Group id****' + acc.Account_Group__c);
            if (acc.Acct_Group_ID_Name__c != null) {
                accountInfoRecordParameters.Flexattribute2 = acc.Acct_Group_ID_Name__c;
            } 
            // JS END
            /*  if (accountDelete == TRUE){
                    accountInfoRecordParameters.flexattribute1 = 'I';
                } else if (accountDelete == FALSE){
                    accountInfoRecordParameters.flexattribute1 = 'A';
                }
            */
            accountInfoRecordParameters.partycontactInfo = new List<NewAccount_WS_XSD.PartycontactInfo_element>();
            accountInfoRecordParameters.addressinfo = new List<NewAccount_WS_XSD.Addressinfo_element>();
            //PartyContactParameters
            NewAccount_WS_XSD.PartycontactInfo_element partyContactParameters = new NewAccount_WS_XSD.PartycontactInfo_element();

            // Adding the record to the List
            accountInfoRecordParameters.partycontactInfo.add(partyContactParameters);
            for (SiteLocation__c loc : locList) {
                //AddressInfoRecord
                NewAccount_WS_XSD.Addressinfo_element addressInfoRecord = new NewAccount_WS_XSD.Addressinfo_element();
                addressInfoRecord.orgName    = loc.Operating_Unit__c;
                addressInfoRecord.address1   = loc.Street_Address__c;
                addressInfoRecord.city       = loc.City__c;
                addressInfoRecord.state      = loc.State_Province__c;
                addressInfoRecord.county     = loc.County__c;
                addressInfoRecord.country    = loc.Country__c;
                addressInfoRecord.postalCode = loc.Postal_Code__c;

                if (loc.Address_ID__c != null) {
                    addressInfoRecord.addressId = loc.Address_ID__c;
                }

                addressInfoRecord.sfAddressId = loc.id;

                if (loc.Primary__c == TRUE) {
                    addressInfoRecord.Flexattribute1 = 'Y';
                }
                //Raj 08/22/2017: START - US782 
                if(loc.Name!=null){
                    //Send Location Name
                    addressInfoRecord.Flexattribute2 = loc.Name;
                }
                //Raj 08/22/2017: END - US782 

                //Adding the record to the list
                accountInfoRecordParameters.addressinfo.add(addressInfoRecord);
            }
            //Adding all accountInfo records to the PartyRecordParameter list
            partyRecordParameters.accountinfo.add(accountInfoRecordParameters);
            //Adding everything to the main list
            partyList.add(partyRecordParameters);
            
            //setting the timeout
            request.timeout_x = 120000;
            
            //Auth
            request.Header = new NewAccount_WS.Security();
            request.Header.UserNameToken = new NewAccount_WS.UsernameToken();
            request.Header.UserNameToken.UserName = JacobsOracleConfigurationHelper.getConfiguration('Account_Service_Username');
            request.Header.UserNameToken.password = JacobsOracleConfigurationHelper.getConfiguration('Account_Service_Password');
            
            System.debug(partyList);
            //Callout string response
            // result = request.process(accountId, sfPartyId, sfAccountId, flexattribute1, flexattribute2, flexattribute3, flexattribute4, flexattribute5, locList, contList);
            result = request.process(partyList);
        } catch (System.CalloutException ex) {
            accountQueueableHandler job = new accountQueueableHandler(acc);
            job.max = max;
            //   job.accountDelete = accountDelete;
            job.locationChange = locationChange;
            job.location = location;
            job.counter = counter + 1;
            if (max >= job.counter ) {
                if(!Test.isRunningTest()) {
                    System.debug('***' + job.counter);
                    System.enqueueJob(job);
                }
            } else {
                errMessage = ex.getMessage();
                System.debug('***' + job.counter); 
                retryService.logEntry(
                    acc.Id,
                    Account.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'accountQueueableHandler',
                    'Account Created',
                    'InfraException',
                    ex.getMessage(),
                    'Callout Exception'
                );
            }
            System.debug('Callout Exception: ' + ex);
        } catch (Exception ex) {
            accountQueueableHandler job = new accountQueueableHandler(acc);
            job.max = max;
            job.locationChange = locationChange;
            job.location = location;
            job.counter = counter + 1;
            if (max >= job.counter) {
                if(!Test.isRunningTest()) {
                    System.debug('***' + job.counter);
                    System.enqueueJob(job);
                }
            } else if (max < job.counter) {
                errMessage = ex.getMessage();
                System.debug('***' + job.counter);
                retryService.logEntry(
                    acc.Id,
                    Account.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'accountQueueableHandler',
                    'Account Created',
                    'InfraException',
                    ex.getMessage(),
                    'Callout Exception'
                );
            }
            System.debug('Exception from callout:' + ex.getMessage());
            System.debug('Exception from callout:' + ex.getStackTraceString());
        }
        
        System.debug(result);
        
        if (result != null) {
            if (result.ErrorMessage != null) {
                // JS US1826 - modified fields reference as per new WSDL
                status = result.ErrorMessage[0].JBstatus;
                errMessage = result.ErrorMessage[0].JBErrorMessage;
                if(result.ErrorMessage.size() > 1){
                    chStatus = result.ErrorMessage[1].CHstatus;
                    chErrMessage = result.ErrorMessage[1].CHErrorMessage;
                }
            }
            System.debug(status);
            System.debug(errMessage);
            System.debug(chStatus);
            System.debug(chErrMessage);
            
            //    if (status == 'S' && locationDelete != TRUE && accountDelete != TRUE){
            
            // Jacobs Result Handling
            Boolean jbverified = false;
            if (status == 'S') {
                if (acc.OracleAccountID__c == null) {   // If condition put in place to avoid updating an existing Oracle Account ID on update operations                    
                    Account acc1 = new Account(id = acc.id);    
                    acc1.OracleAccountID__c = string.valueOf(result.AccountSetup1[0].JBAccountId);
                    // Start - added by Raj - US607
                    acc1.Oracle_ACCOUNT_NUMBER__c =  string.valueOf(result.AccountSetup1[0].Flexattribute1);
                    // End - added by Raj -US607    
                    acc1.Status__c  = 'Verified';                
                    update acc1;                                
                }

                List<SiteLocation__c> locationList = new List<SiteLocation__c>();
                for (integer i = 0; i < result.AccountSetup1[0].Location.size(); i++) {
                    if (locList[i].Address_ID__c == null && string.valueOf(result.AccountSetup1[0].Location[i].addressId) != null) {
                        SiteLocation__c loc1 = new SiteLocation__c(id = locList[i].id);
                        loc1.Address_ID__c   = string.valueOf(result.AccountSetup1[0].Location[i].addressId);
                        //PS 08/30/2017: START US782: Populate site field
                        loc1.Site_Number__c       = string.valueOf(result.AccountSetup1[0].Location[i].Flexattribute1);
                        //PS 08/30/2017: END US782: Populate site field
                        locationList.add(loc1);
                    }
                }
                update locationList; 
            } else if (status == 'E' && errMessage == AccountExists) {
                Account acc1;
                if (acc.OracleAccountID__c == null && string.valueOf(result.AccountSetup1[0].JBAccountId) != null) {
                    acc1 = new Account(id = acc.id, Operating_Unit__c = acc.Operating_Unit__c, name = acc.Name);
                    acc1.OracleAccountID__c = string.valueOf(result.AccountSetup1[0].JBAccountId);
                    acc1.Status__c = 'Verified';
                    update acc1;
                }
                List<SiteLocation__c> locationList = new List<SiteLocation__c>();
                for (integer i = 0; i < result.AccountSetup1[0].Location.size(); i++) {
                    if (locList[i].Address_ID__c == null && string.valueOf(result.AccountSetup1[0].Location[i].addressId) != null) {
                        SiteLocation__c loc1 = new SiteLocation__c(id = locList[i].id);
                        loc1.Address_ID__c = string.valueOf(result.AccountSetup1[0].Location[i].addressId);
                        locationList.add(loc1);
                    }
                }
                update locationList;
                
                accountQueueableHandler job = new accountQueueableHandler(acc1);
                job.max = max;
                job.locationChange = locationChange;
                job.location = location;
                job.counter = counter + 1;
                if (max >= job.counter ) {
                    if(!Test.isRunningTest()) {
                        System.debug('***' + job.counter);
                        System.enqueueJob(job);
                    }
                } else {
                    retryService.logEntry(
                        acc.Id,
                        Account.getSObjectType().getDescribe().getName(),
                        'SFDC Sales',
                        'accountQueueableHandler',
                        'Address Creation Error',
                        'Other',
                        errMessage,
                        'Callout Exception'
                    );
                }
            } else if (status == 'E' && errMessage == AddressExists) {
                List<SiteLocation__c> locationList = new List<SiteLocation__c>();
                for (integer i = 0; i < result.AccountSetup1[0].Location.size(); i++) {
                    if (locList[i].Address_ID__c == null && string.valueOf(result.AccountSetup1[0].Location[i].addressId) != null) {
                        SiteLocation__c loc1 = new SiteLocation__c(id = locList[i].id);
                        loc1.Address_ID__c = string.valueOf(result.AccountSetup1[0].Location[i].addressId);
                        locationList.add(loc1);
                    }
                }
                update locationList;
                
                accountQueueableHandler job = new accountQueueableHandler(acc);
                job.max = max;
                job.locationChange = locationChange;
                job.location = location;
                job.counter = counter + 1;
                if (max >= job.counter) {
                    if(!Test.isRunningTest()) {
                        System.debug('***' + job.counter);
                        System.enqueueJob(job);
                    }
                } else {
                    retryService.logEntry(
                        acc.Id,
                        Account.getSObjectType().getDescribe().getName(),
                        'SFDC Sales',
                        'accountQueueableHandler',
                        'Address Creation Error',
                        'Other',
                        errMessage,
                        status
                    );
                }
            } else if (status == 'E' && errMessage != AddressExists && errMessage != AccountExists) {
                retryService.logEntry(
                    acc.Id,
                    Account.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'accountQueueableHandler',
                    'Address Creation Error',
                    'Other',
                    errMessage,
                    status
                );
            }
            
            // Ch Result Handling
            // JS Start US1826 - Modified logic to work for CH2m oracle fields
            if (chStatus == 'S') {
                if(acc.CH2M_Oracle_Account_ID__c == null){
                    Account acc1 = new Account(id = acc.id);
                    if (acc.CH2M_Oracle_Account_ID__c == null) {  
                        acc1.CH2M_Oracle_Account_ID__c = string.valueOf(result.AccountSetup1[0].CH2MaccountId);                   
                        acc1.CH2M_Oracle_Account_Number__c =  string.valueOf(result.AccountSetup1[0].CH2MaccountNo);
                        acc1.CH2M_Status__c = 'Verified';
                    }
                    update acc1;
                }
            }else if (chStatus == 'E' && chErrMessage == AccountExists) {
                Account acc1;
                if (acc.CH2M_Oracle_Account_ID__c == null && string.valueOf(result.AccountSetup1[0].CH2MaccountId) != null) {
                    acc1 = new Account(id = acc.id, Operating_Unit__c = acc.Operating_Unit__c, name = acc.Name);
                    acc1.CH2M_Oracle_Account_ID__c = string.valueOf(result.AccountSetup1[0].CH2MaccountId);
                    acc1.CH2M_Status__c = 'Verified';
                    update acc1;
                }
            }else if (chStatus == 'E' && chErrMessage != AccountExists) {
                retryService.logEntry(
                    acc.Id,
                    Account.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'accountQueueableHandler',
                    'Address Creation Error',
                    'Other',
                    chErrMessage,
                    chStatus
                );
            }
            // JS END US1826
        }
 
        //DE412 - Code added for marking account as unverified if an error occurs while calling the GFS web service.
        if ((status == 'E' && acc.OracleAccountID__c == null) ||
            (chStatus == 'E' && acc.CH2M_Oracle_Account_ID__c == null))
        {
            Account accountToUpdate = new Account(Id = acc.id);
            if(acc.OracleAccountID__c == null){
               accountToUpdate.Status__c = 'Unverified'; 
            }
            if(acc.CH2M_Oracle_Account_ID__c == null){
               accountToUpdate.CH2M_Status__c = 'Unverified'; 
            }
            
            update accountToUpdate;

            if(!Test.isRunningTest()){ 
                utility.AddChatterpost(UserInfo.getUserId(), accountToUpdate.Id, String.format(System.Label.AccountVerifyFailed,  new String[]{errMessage}));
            }
        }
        
        retryService.deleteRetryLogIfNeeded();
        
    }
}