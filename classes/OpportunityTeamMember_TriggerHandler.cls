/**************************************************************************************
Name: OpportunityTeamMember_TriggerHandler
Version: 1.0
Created Date: 10/04/2018
Function: Handler for OpportunityTeamMembers

Modification Log:
-- -- -- -- -- -- -- -onBeforeInsert- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    10/04/2018      Original Version
* Pradeep Shetty     08/10/2018      DE612: Change Read/Edit access to All and modularise the class
* Madhu Shetty     	 14/12/2018      US2563: Added code to update the Capture manager and proposal manager name on the opportunity
*************************************************************************************/

public class OpportunityTeamMember_TriggerHandler {
    private static OpportunityTeamMember_TriggerHandler handler;
    private static final String CaptureManager = 'Capture Manager';
    private static final String ProposalManager = 'Proposal Manager';
    /*
	* Singleton like pattern
	*/
    public static OpportunityTeamMember_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new OpportunityTeamMember_TriggerHandler();
        }
        return handler;
    }
    
    /*
    * Actions performed on Opportunity Team Member records before insert
    */
    public void onBeforeInsert(List<OpportunityTeamMember> oppTeamMember) {
        processTeamMembers(buildOppAndOppTeamMembersMap(oppTeamMember),true,false,false);       
    }
    
    /*
    * Actions performed on Opportunity Team Member records before delete
    */
    public void onBeforeDelete (List<OpportunityTeamMember> oppTeamMember) {
        processTeamMembers(buildOppAndOppTeamMembersMap(oppTeamMember),false,false,true);
    }

    //DE612 - Added AfterInsert to update the Opportunity Access Level to Owner for the OTM with Read/Edit access
    /*
    * Actions performed on Opportunity Team Member records before insert
    */
    public void onAfterInsert(List<OpportunityTeamMember> oppTeamMember) {
        updateOpportunityAccessLevel(buildOppIdAndUserIDList(oppTeamMember)[0], buildOppIdAndUserIDList(oppTeamMember)[1]);
        updateOppTeamManagerNames(oppTeamMember);
    }
    
    //DE612 - Modified AfterUpdate to update the Opportunity Access Level to Owner for the OTM with Read/Edit access
    /*
    * Actions performed on Opportunity Team Member records before update
    */
    public void onAfterUpdate(List<OpportunityTeamMember> oppTeamMember) {
        processTeamMembers(buildOppAndOppTeamMembersMap(oppTeamMember),false,true,false);
        updateOpportunityAccessLevel(buildOppIdAndUserIDList(oppTeamMember)[0], buildOppIdAndUserIDList(oppTeamMember)[1]);
        updateOppTeamManagerNames(oppTeamMember);
    }

    /*
    * Actions performed on Opportunity Team Member records after delete
    */
    public void onAfterDelete(List<OpportunityTeamMember> oppTeamMember) {
        updateOppTeamManagerNames(oppTeamMember);
    }

    /*
    * To sync all Child opportunities Team members with Parent opportunity, 
    * to make sure team members on Parent opporutunity have access to all Child opportunities
    */
	public void processTeamMembers(Map<ID, List<OpportunityTeamMember>> mapOppIdLstMembers,Boolean isInsert,Boolean isUpdate,Boolean isdelete){
        List<OpportunityTeamMember> newTeamMembers = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> deleteTeamMembers = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> updateTeamMembers = new List<OpportunityTeamMember>();
        
        // Get children of all Parent Opportunities
        Map<ID, Opportunity> mapChildOpp = getChildrenOfParentOpty(mapOppIdLstMembers.keyset());
        
        if(!mapChildOpp.isEmpty()){
            for (Opportunity childopp : mapChildOpp.values())
            {
                for(OpportunityTeamMember member : mapOppIdLstMembers.get(childopp.Parent_Opportunity__c)){
                    if(isInsert){
                        OpportunityTeamMember newMember = member.clone();
                        newMember.OpportunityId = childopp.id;
                        newTeamMembers.add(newMember);
                    }
                    else if(isdelete){
                        for(OpportunityTeamMember otm : childopp.OpportunityTeamMembers){
                            if(otm.UserID == member.UserID  && childopp.OwnerId != member.UserID){
                                deleteTeamMembers.add(otm);
                            }
                        }
                    }
                    else if(isupdate){
                        for(OpportunityTeamMember otm : childopp.OpportunityTeamMembers){
                            if(otm.UserID == member.UserID && childopp.OwnerId != member.UserID){
                                OpportunityTeamMember updateMember = member.clone();
                                updateMember.OpportunityId = childopp.id;
                                updateTeamMembers.add(updateMember);
                            }
                        }
                    }
                }
            }
            // DML operations
            List<Database.UpsertResult> insertresults = new List<Database.UpsertResult>();
            List<Database.UpsertResult> updateresults = new List<Database.UpsertResult>();
            List<Database.DeleteResult> delResults = new List<Database.DeleteResult>();
            
            try{
                if(newTeamMembers!=null){
                   insertresults = Database.upsert(newTeamMembers,false); 
                }
                if(updateTeamMembers!=null){
                   updateresults = Database.upsert(updateTeamMembers,false); 
                }
                if(deleteTeamMembers!= null ){
                    delResults = Database.delete(deleteTeamMembers,false);
                } 
            }catch(Exception e){
                system.debug('Exception occured'+e.getMessage());
            }
            
            // Error handling to display DML errors on the UI
            
            // Process the delete results, to handle "Can't remove a team member assigned to a split from an opportunity"
            for(Database.DeleteResult sr : delResults){
                if(!sr.isSuccess()){
                    // Get the first result error
                    Database.Error err = sr.getErrors()[0];
                    // Throw an error.
                    for(SObject tm : trigger.old){
                        if(err.getMessage().contains('remove a team member assigned to a split from an opportunity')){
                                                     tm.addError(
                                                             'Unable to Delete team member in some Child Opportunities, '
                                                             + err.getMessage());
                                                     }
                    }
                }
            }
            // Process the insert results
            for(Database.UpsertResult sr : insertresults){
                if(!sr.isSuccess()){
                    // Get the first result error
                    Database.Error err = sr.getErrors()[0];
                    // Throw an error.
                    for(SObject tm : Trigger.new){ 
                        tm.addError(
                            'Unable to insert team member in some Child Opportunities, '
                            + err.getMessage());
                    }
                }
            }
            // Process the update results
            for(Database.UpsertResult sr : updateresults){
                if(!sr.isSuccess()){
                    // Get the first result error
                    Database.Error err = sr.getErrors()[0];
                    // Throw an error.
                    for(SObject tm : Trigger.newMap.values()){ 
                        tm.addError(
                            'Unable to update team member in some Child Opportunities, '
                            + err.getMessage());
                    }
                }
            }
        }
    }
    /*
    * Returns Map of all Child opportunities for the given list of opportunities 
    */
    public Map<ID, Opportunity> getChildrenOfParentOpty(Set<ID> lstOpps){      
        // Query to extract all the Child opportunities
        Map<ID, Opportunity> mapChildOpp = new Map<ID, Opportunity>(
            [SELECT Id,
             Parent_Opportunity__c, OwnerId,
             	(SELECT Id,
              		OpportunityAccessLevel, 
              		OpportunityId, 
              		TeamMemberRole, 
              		UserId 
              		FROM OpportunityTeamMembers) 
             FROM Opportunity 
             WHERE Confidential__c = false 
             AND Parent_Opportunity__c in :[SELECT ID,Parent_Opportunity__c                                 
                                            FROM Opportunity 
                                            WHERE Parent_Opportunity__c = null and ID =: lstOpps]]);
        
        return mapChildOpp;
    }

    //Start: DE612 - Adding a method to build the map of Opp and its team members
    /*
    * Loops through the Opp Team Members record and performs the following activities:
    * -- Builds the map of Opportunity Id and its Opportunity Team Member records
    */
    public Map<ID,List<OpportunityTeamMember>> buildOppAndOppTeamMembersMap(List<OpportunityTeamMember> oppTeamMember){
        Map<ID, List<OpportunityTeamMember>> mapOppIdLstMembers = new Map<ID,List<OpportunityTeamMember>>();
        for (OpportunityTeamMember teamMem : oppTeamMember) {
            //Build a map of Opportunity Id and its OTM
            if(mapOppIdLstMembers.containsKey(teamMem.OpportunityID)) {
                List<OpportunityTeamMember> members = mapOppIdLstMembers.get(teamMem.OpportunityID);
                members.add(teamMem);
                mapOppIdLstMembers.put(teamMem.OpportunityID, members);
            } else {
                mapOppIdLstMembers.put(teamMem.OpportunityID, new List<OpportunityTeamMember> { teamMem });
            }
        }

        return mapOppIdLstMembers;       
    }
    //END: DE612 

    //Start: DE612 - Adding a method to build the map of Opp and Team Member User Ids
    /*
    * Loops through the Opp Team Members record and performs the following activities:
    * -- Builds the map of Opportunity Id and Opportunity Team Member User Ids
    */
    public List<Set<Id>> buildOppIdAndUserIDList(List<OpportunityTeamMember> oppTeamMember){
        List<Set<Id>> oppIdUserIdList = new List<Set<Id>>();
        Set<Id> oppIdList = new Set<Id>();
        Set<Id> userIdList = new Set<Id>();
        for (OpportunityTeamMember otm : oppTeamMember) {
            //Add Opp to the set
            if(!oppIdList.contains(otm.OpportunityId)){
                oppIdList.add(otm.OpportunityId);
            }
            //Add UserId to the set
            if(!userIdList.contains(otm.UserId)){
                userIdList.add(otm.UserId);
            }            
        }

        oppIdUserIdList.add(oppIdList);
        oppIdUserIdList.add(userIdList);

        return oppIdUserIdList;       
    }
    //END: DE612     

    //Start: DE612 - Adding a method to update OpportunityShare records to change the Access level
    /*
    * Updates the OpportunityShare record corresponding to the OTM record so that the user has an All access 
    * when the team member is added with an Read/Edit access
    */
    public void updateOpportunityAccessLevel(Set<Id> oppIdList, Set<Id> userIdList){

        system.debug('OPPLIST: ' + oppIdList);
        system.debug('UIDLIST: ' + userIdList);
        
        //OpportunityShare records to be updated
        List<OpportunityShare> oppShareList = new List<OpportunityShare>();

        //Get OpportunityShare records corresponding to the OTMs and 
        // Loop through all OppShare records and update the access level to All
        for(OpportunityShare oppShare : [Select Id, 
                                                UserOrGroupId,
                                                OpportunityAccessLevel 
                                        From OpportunityShare 
                                        Where OpportunityId in :oppIdList 
                                        And UserOrGroupId in :userIdList 
                                        And OpportunityAccessLevel = 'Edit' 
                                        And RowCause = 'Team']){

                oppShare.OpportunityAccessLevel = 'All';
                oppShareList.add(oppShare);            
        }

        //Update Opportunity Share
        if(oppShareList!=null && !oppShareList.isEmpty()){
            update oppShareList;
        }

    }
    //END: DE612  

    /*
    * Method to update Team manager names on the opportunity records.
    */
    public void updateOppTeamManagerNames(List<OpportunityTeamMember> oppTeamMembers){
        Set<Id> oppIdList = new Set<Id>();
        
        //Get the list of all Opportunity Ids whose opportunity team members were updated
        for(OpportunityTeamMember oppTeamMem : oppTeamMembers){
            if(!oppIdList.contains(oppTeamMem.OpportunityId)){
                oppIdList.add(oppTeamMem.OpportunityId);
            }
        }
        
        //Get list of all affected Opportunities and their corresponding Capture and Proposal managers
        List<Opportunity> selOpps = [select id, Capture_Manager__c, Proposal_Manager__c,
                                             (select id, OpportunityId, User.Name, TeamMemberRole
                                              from OpportunityTeamMembers 
                                              where TeamMemberRole =: CaptureManager or TeamMemberRole =: ProposalManager
                                              ORDER BY LastModifiedDate DESC)
                                     from Opportunity
                                     where id in :oppIdList];
        
        for(Opportunity selOpp :  selOpps){
            string CapManager='';
            string ProManager='';
            //Loop to get the latest Capture Manager
            for (OpportunityTeamMember opptm : selOpp.OpportunityTeamMembers) {
                if(opptm.TeamMemberRole == CaptureManager){
                    CapManager = opptm.User.Name;
                    break;
                }
            }
            
            //Loop to get the latest Proposal Manager
            for (OpportunityTeamMember opptm : selOpp.OpportunityTeamMembers) {
                if(opptm.TeamMemberRole == ProposalManager){
                    ProManager = opptm.User.Name;
                    break;
                }
            }
            
            //Update the manager names on the Opportunity, only if they have changed
            if(selOpp.Capture_Manager__c != CapManager || selOpp.Proposal_Manager__c != ProManager){
                selOpp.Capture_Manager__c = CapManager;
                selOpp.Proposal_Manager__c = ProManager;
                update selOpp;
            }
        }
    }
}