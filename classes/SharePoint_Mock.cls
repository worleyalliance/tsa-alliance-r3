/**************************************************************************************
Name: SharePoint_Mock
Version: 1.0 
Created Date: 24.02.2017
Function: Mock for SharePint services

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/
@IsTest
public with sharing class SharePoint_Mock implements HttpCalloutMock {
    public Boolean isError = false;
    public Integer executionCounter = 0;
    public String successResp = '{"d":{"results":[{"CheckInComment":"","CheckOutType":2,"ContentTag":"{56F9BB55-6FCD-4CFA-9039-6F4B59D6E558},1,1","CustomizedPageStatus":0,"ETag":"{56F9BB55-6FCD-4CFA-9039-6F4B59D6E558},1","Exists":true,"IrmEnabled":false,"Length":"286","Level":1,"LinkingUri":"https://jacobsengineering.sharepoint.com/sites/SharePointChampions/SalesForceIntegration/Shared%20Documents/StefanTest(001n000000CeOpqAAF)/Research/error020917034642991.csv?d=w56f9bb556fcd4cfa90396f4b59d6e558","LinkingUrl":"","MajorVersion":1,"MinorVersion":0,"Name":"error020917034642991.csv","ServerRelativeUrl":"/sites/SharePointChampions/SalesForceIntegration/Shared Documents/StefanTest(001n000000CeOpqAAF)/Research/error020917034642991.csv","TimeCreated":"2017-03-03T10:26:03Z","TimeLastModified":"2017-03-03T10:26:03Z","Title":null,"UIVersion":512,"UIVersionLabel":"1.0","UniqueId":"56f9bb55-6fcd-4cfa-9039-6f4b59d6e558"}]}}';
    private String errorResp = '{"error":{"code":"-2130247139, Microsoft.SharePoint.SPException","message":{"lang":"en-US","value":" not found."}}}';

   /*
    *  Implementation of HttpCalloutMock interface
    *  Mock is quite simple and responses with some fixed response when error is enforced or not
    */
    public HTTPResponse respond(HTTPRequest req) {
        executionCounter++;
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String respBody = isError ?
                errorResp
            :   successResp;
        res.setBody(respBody);
        Integer statusCode = isError ? 400 : 200;
        res.setStatusCode(statusCode);
        return res;
    }
}