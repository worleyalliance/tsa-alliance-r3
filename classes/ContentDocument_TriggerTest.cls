/**************************************************************************************
Name: ContentDocument_TriggerTest
Version: 1.0
Created Date: 10/09/2017
Function: Test class for ContentDocument_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    01/29/2018      Original Version
*************************************************************************************/
@isTest
private class ContentDocument_TriggerTest {
	
  private static final String PROFILE_SALES_SUPER_USER  = 'Sales Super User';
  private static final String USERNAME                  = 'salescdlTest@testuser.com';
  private static final string LIBRARY_RECORDTYPE_RESUME = 'Resume';

  //Test the num_count_of_related_files__c field on Library when File is created and deleted
	@isTest static void test_LibraryFileCount() {

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

    Test.startTest();
    System.runAs(salesUser){

      //Create a Library record
      TestHelper.LibraryBuilder libraryBuilder = new TestHelper.LibraryBuilder();
      Library__c  lib = libraryBuilder.build().withRecordType(LIBRARY_RECORDTYPE_RESUME).save().getRecord();

      //Create a File
      TestHelper.ContentVersionBuilder fileBuilder = new TestHelper.ContentVersionBuilder();
      ContentVersion file = fileBuilder.build().save().getRecord();   

      //Query the ContentVersion to get the ContentDocumentId
      List<ContentVersion> fileList = [Select Id, 
                                              ContentDocumentId 
                                       From ContentVersion 
                                       Where Id= :file.Id];

      //CreateContentDocument Link
      ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = fileList[0].ContentDocumentId,
                                                        LinkedEntityId    = lib.Id,
                                                        ShareType         = 'V');
      //Insert the Content Document Link
      insert cdl;


      //Query the library to get the num_count_of_related_files__c
      List<Library__c> libList = [Select Id, 
                                      Num_count_of_related_files__c 
                               From Library__c
                               Where Id = :lib.Id];
      
      //Check the file count                               
      System.assertEquals(1, libList[0].Num_count_of_related_files__c);   

      //Get the contentDocument for the file (ContentVersion cannot be deleted)
      List<ContentDocument> cd = [Select Id from ContentDocument where Id=:fileList[0].ContentDocumentId];
      //Delete the file
      delete cd;

      //query the library again to get the updated Library
      List<Library__c> updatedLibList = [Select Id, 
                                      Num_count_of_related_files__c 
                               From Library__c
                               Where Id = :lib.Id];      

      //Check the file count                               
      System.assertEquals(0, updatedLibList[0].Num_count_of_related_files__c);                                
    }
    Test.stopTest();

	}
}