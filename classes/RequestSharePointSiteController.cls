/**************************************************************************************
Name:RequestSharePointSiteController
Version: 1.0 
Created Date: 09/20/2018
Function: Update Opportunity SharePoint Site Requestor to current user and SharePoint Status to Requested

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Stone   09/20/2018       Original Version
*************************************************************************************/
public class RequestSharePointSiteController {
	@AuraEnabled
    public static void requestOpportunitySharePointSite(Id oppId) {
        SharePointSiteRequestService service = new SharePointSiteRequestService();
        service.requestSharePointSite(oppId);
    }
    @AuraEnabled
    public static Boolean checkRequestAllowed(Id oppId) {
        SharePointSiteRequestService service = new SharePointSiteRequestService();
        
        Opportunity oppty = service.getSharePointSiteDetails(oppId);
        String id = '';
        Boolean err = false;
        if(oppty.SharePoint_URL__c != null)  {
            id='ALREADY_CREATED';
            err = true;
        } else if(oppty.Sharepoint_Status__c == 'Requested') {
            id='ALREADY_REQUESTED';
            err = true;
        } else if(oppty.Sharepoint_Status__c == 'In Progress') {
            id='IN_PROGRESS';
            err = true;
        } else if(oppty.Sharepoint_Status__c == 'Failed') {
            id='REQUEST_FAILED';
            err = true;
        }
        
        if(err){
            //use a custom error data object to return additional error data - https://developer.salesforce.com/blogs/2017/09/error-handling-best-practices-lightning-apex.html
            SharePointSiteRequestExceptionData data = new SharePointSiteRequestExceptionData(id, '', oppty.SharePoint_Site_Requested_On__c, 
                                                                                             service.isUserAnAdministrator(), oppty.SharePoint_URL__c);
            throw new AuraHandledException(JSON.serialize(data));
        }
        
        return service.userCanEditOpportunity(oppId);
    }
    
    @AuraEnabled
    public static Opportunity getSharePointSiteDetails(Id oppId){
        SharePointSiteRequestService service = new SharePointSiteRequestService();
        return service.getSharePointSiteDetails(oppId);
    }
    
}