/**************************************************************************************
Name: CustomRelatedList_Service
Version: 1.0 
Created Date: 14.03.2017
Function: Controller for CustomRelatedList component. Handles data read, save and delete

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           14.03.2017         Original Version
*************************************************************************************/
public with sharing class CustomRelatedList_C {

    private static CustomRelatedList_Service service = new CustomRelatedList_Service();

   /*
    *   Retrieves related list data for given parent record id based on relationship name and wanted fields.
    *   Data is packed and send as single object dataset
    */
    @AuraEnabled
    public static CustomRelatedList_Service.TableData getData(
            Id parentId,
            String objectType,
            String relationName,
            List<String> fieldNames,
            String linkFieldId,
            String linkFieldLabel
    ) {
        return
            service.getData(
                    parentId,
                    objectType,
                    relationName,
                    fieldNames,
                    linkFieldId,
                    linkFieldLabel
            );
    }

   /*
    *   Performes insert/update of record
    */
    @AuraEnabled
    public static Id saveRecord(sObject record) {
        return service.saveRecord(record);
    }

   /*
    *  Performes deletion of record based on it's id
    */
    @AuraEnabled
    public static void deleteRecord(Id recordId) {
        service.deleteRecord(recordId);
    }

   /*
    *   Gives update regarding user permissions to given object object
    */
    @AuraEnabled
    public static BaseComponent_Service.CRUDpermissions getUserPermissions(String objectType) {
        return service.getUserOpportunityPermissios(objectType);
    }
}