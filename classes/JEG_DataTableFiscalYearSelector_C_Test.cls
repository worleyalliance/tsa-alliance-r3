/**************************************************************************************
Name: JEG_DataTableFiscalYearSelector_C_Test
Version: 1.0 
Created Date: 07.05.2017
Function: Unit test class for JEG_DataTableFiscalYearSelector_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.05.2017         Original Version
*************************************************************************************/
@isTest
private class JEG_DataTableFiscalYearSelector_C_Test {

    @IsTest
    private static void shouldCalculateTwoFiscalYearsForGroup() {
        //given
        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 = attritionBuilder.build().withGroupId(atGroup.Id).withFiscalYear(2015).save().getRecord();
        AT_Segment_Attrition__c groupAttrition2 = attritionBuilder.build().withGroupId(atGroup.Id).withFiscalYear(2016).save().getRecord();

        //when
        List<Integer> result = JEG_DataTableFiscalYearSelector_C.getFiscalYears(atGroup.Id, 'Fiscal_Year__c', 'Group__c', 'AT_Segment_Attrition__c');

        //then
        System.assertEquals(2, result.size());
    }
}