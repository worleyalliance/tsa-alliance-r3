/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_B_P_RequestTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_B_P_RequestTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new B_P_Request__c());
    }
}