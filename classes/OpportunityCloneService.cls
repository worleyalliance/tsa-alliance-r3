/**************************************************************************************
Name: OpportunityCloneService
Version: 1.0 
Created Date: 29.02.2017
Function: Makes a child copy of an opportunity and child records of the opprotunity

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   29.02.2017      Original Version
* Raj Vakati        10.08.2017      US588 story fix (Enhanced Clone - Not Copying All Multi-Office Splits)
* Manuel            16.05.2018      DE467 Set default Go and Get values
* Scott Stone       26.02.2019      US2806 - cloned opportunity name should be up to 120 characters, not 80.
* Madhu Shetty      12.06.2019      DE1004 - Changed the approach from loading all Opportunity related object data in one query to 
                                    getting individual related object data to avoid errors.
*************************************************************************************/
public with sharing class OpportunityCloneService {

    private final static String OPENING_GAME_STATE = 'Opening Game';
    private final static String CLASS_A_T = 'Class III';
    private final static Integer DEFAULT_PROBABILITY = 1;
    private final static String COPY = ' Copy';

   /*
    *   Method which makes a copy of opportuniy with given Id
    *   Besides opportunity record also children records are copied
    *   Fields which are to be copied from parent opportunity are set in custom metadata types
    *   Child objects which are to be copied are also set in custom metatada types.
    *   For child object all creatable fields are copied.
    */
    public Id doClone(Id parentOpportunityId) {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        List<String> opportunityFields = new List<String>();
        List<ChildOpportunityFieldClone__mdt> childOpportunityFields = [
                SELECT API_Name__c
                FROM ChildOpportunityFieldClone__mdt
        ];

        for(ChildOpportunityFieldClone__mdt field : childOpportunityFields){
            opportunityFields.add(field.API_Name__c);
        }

        List<ChildOpportunityRelatedListClone__mdt> childOpportunityRelatedListObjects = [
                SELECT API_Name__c, API_RelationshipName__c, API_ChildRelationshipName__c
                FROM ChildOpportunityRelatedListClone__mdt
        ];

        //List<String> subqueries = prepareSubqueries(childOpportunityRelatedListObjects, schemaMap);
        String query =
                'SELECT ' + String.join(opportunityFields, ',')+
                        //','+String.join(subqueries, ',') +
                        ' FROM Opportunity' +
                        ' WHERE Id = \''+parentOpportunityId+'\'';
       
        List<Opportunity> opportunities = Database.query(query);
        if(opportunities.isEmpty()){
            System.debug('No opportunity with given Id: '+parentOpportunityId);
            throw new AuraHandledException(Label.ErrorOccurredDuringOpportunityCopy);
        }
        Opportunity childOpportunity;
        Opportunity parentOpportunity = opportunities[0];
        try {
            childOpportunity = cloneOpportunity(parentOpportunity);
            insert childOpportunity;
        } catch (Exception ex){
            system.debug( ex.getStackTraceString());
            String errorMsg = Label.ErrorOccurredDuringOpportunityCopy;
            if(ex instanceof DmlException){
                errorMsg += ': '+ ((DmlException)ex).getDmlMessage(0);
            }
            throw new AuraHandledException(errorMsg);
        }
        List<sObject> childObjects;
        try{
            childObjects =
                    cloneOpportunityChildren(
                            parentOpportunity, childOpportunity, childOpportunityRelatedListObjects, schemaMap
                    );
            insert childObjects;
        } catch (Exception ex){
            system.debug( ex.getStackTraceString());
            String errorMsg = Label.ErrorOccurredDuringOpportunityCopy;
            if(ex instanceof DmlException){
                errorMsg += ': '+ ((DmlException)ex).getDmlMessage(0);
                errorMsg += ' '+ childObjects[((DmlException)ex).getDmlIndex(0)];
            }
            throw new AuraHandledException(errorMsg);
        }
        return childOpportunity.Id;
    }

   /*
    *   Method creates a list of subqueries for opportunity childred records.
    *   Queries are buid based on custom metadata inputs and field describe data
    */
    private List<String> prepareSubqueries(
            List<ChildOpportunityRelatedListClone__mdt> childOpportunityRelatedListObjects,
            Map <String, Schema.SObjectType> schemaMap
    ){
        List<String> subqueries = new List<String>();
        for(ChildOpportunityRelatedListClone__mdt childObject : childOpportunityRelatedListObjects){
            List<String> childFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get(childObject.API_Name__c).getDescribe().fields.getMap();
            for(Schema.SObjectField sof : fieldMap.values()){
                DescribeFieldResult dfr = sof.getDescribe();
                // added excluded field to remove ECRmigration id from getting copied in cloned records
                if(dfr.isCreateable()  && dfr.getName() != System.Label.ExcludeField ){
                    childFields.add(dfr.getName());
                }
            }
            if(!childFields.isEmpty()){
                String query = '(SELECT ' + String.join(childFields, ',') + ' FROM ' + childObject.API_ChildRelationshipName__c + ')';
                subqueries.add(query);
            }
        }
        return subqueries;
    }

    /*
    *   Method creates subqueries for each opportunity related objects.
    *   Queries are built based on custom metadata inputs and field describe data
    */
    private String prepareSeperateSubqueries(ChildOpportunityRelatedListClone__mdt childObjectDefinition,
                                            Id parentOpportunityId,
                                            Map <String, Schema.SObjectType> schemaMap){
        List<String> childFields = new List<String>();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(childObjectDefinition.API_Name__c).getDescribe().fields.getMap();
        for(Schema.SObjectField sof : fieldMap.values()){
            DescribeFieldResult dfr = sof.getDescribe();
             // added excluded field to remove ECRmigration id from getting copied in cloned records
            if(dfr.isCreateable()  && dfr.getName() != System.Label.ExcludeField){
                childFields.add(dfr.getName());
            }
        }
        String query='';
        if(!childFields.isEmpty()){
            query = 'SELECT ' + String.join(childFields, ',') + ' FROM ' + childObjectDefinition.API_Name__c + ' where ' + childObjectDefinition.API_RelationshipName__c    + ' = \'' + parentOpportunityId + '\'';
        }
        return query;
    }

   /*
    *   Creating a copy of opprotunty. Specific field values are set for clone.
    */
    private Opportunity cloneOpportunity(Opportunity parentOpportunity){
        Opportunity childOpportunity = parentOpportunity.clone();
        //Start -  US 588 - Added by Raj 
        childOpportunity.Is_Cloned_Opportunity__c = true ;
        childOpportunity.Forecast_Category__c='Exclude';
        //End  -  US 588 - Added by Raj 
        
        //US2806 - cloned opportunity name should be up to 120 characters, not 80.
        Integer nameLength = parentOpportunity.Name.length() + COPY.length() > 120 ? parentOpportunity.Name.length() - ((parentOpportunity.Name.length() + COPY.length()) - 120) : parentOpportunity.Name.length();
        String childOpportunityName = parentOpportunity.Name.substring(0, nameLength);
        childOpportunity.Name = childOpportunityName + COPY;
        childOpportunity.StageName = OPENING_GAME_STATE;
        childOpportunity.Go__c = DEFAULT_PROBABILITY;
        childOpportunity.Get__c = DEFAULT_PROBABILITY;
        //DE 264 Start: Commented to prevent Opportunity enhanced clone failure due to the validation rule : StartDate cannot be before Opp close date.
        //childOpportunity.CloseDate = Date.Today();
        //childOpportunity.Opportunity_Class_A_T__c = CLASS_A_T;
        //DE264 End
        
        return childOpportunity;
    }


   /*
    *   Creates a list of opporutunity child record clones.
    *   Childred objects are defined in custom metadata types.
    */
    private List<sObject> cloneOpportunityChildren(
            Opportunity parentOpportunity,
            Opportunity childOpportunity,
            List<ChildOpportunityRelatedListClone__mdt> childOpportunityRelatedListObjects,
            Map <String, Schema.SObjectType> schemaMap
    ){
        List<sObject> childObjects = new List<SObject>();
        try{
            //Original code
            /*for (ChildOpportunityRelatedListClone__mdt childObjectDefinition : childOpportunityRelatedListObjects) {
                List<sObject> parentRecords = parentOpportunity.getSObjects(childObjectDefinition.API_ChildRelationshipName__c);
                if (parentRecords != null) {
                    List<sObject> records = parentRecords.deepClone();
                    for (sObject record : records) {
                        record.put(childObjectDefinition.API_RelationshipName__c, childOpportunity.Id);
                    }
                    childObjects.addAll(records);
                }
            }  */
            
            for (ChildOpportunityRelatedListClone__mdt childObjectDefinition : childOpportunityRelatedListObjects) {
                string query = prepareSeperateSubqueries(childObjectDefinition, parentOpportunity.Id, schemaMap);
                list<SObject> parentRecords = Database.query(query);
                
                if (parentRecords != null) {
                    List<sObject> records = parentRecords.deepClone();
                    for (sObject record : records) {
                        record.put(childObjectDefinition.API_RelationshipName__c, childOpportunity.Id);
                    }
                    childObjects.addAll(records);
                }
            } 
        }
        Catch (Exception ex){
            system.debug('ex: ' + ex);
            throw ex;
        }
        return childObjects;
   }
}