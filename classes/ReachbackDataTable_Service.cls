/**************************************************************************************
Name: ReachbackDataTable_Service
Version: 1.0 
Created Date: 05.05.2017
Function: Reachback component service

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
public class ReachbackDataTable_Service extends DataTable_Service{

   /*
    * Method retrieves Reachback records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    public TableData getReachbackData(
            String fieldSetName,
            Id recordId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName
    ) {
        validateObjectAccesible(Schema.getGlobalDescribe().get(sObjectName).getDescribe().getName());

        List<FieldDescription> fieldDescriptions = prepareFieldDescriptions(fieldSetName, sObjectName);
        String query =
                buildReachbackQuery(
                        fieldDescriptions,
                        recordId,
                        fiscalYear,
                        fiscalYearField,
                        relationField,
                        sObjectName
                );

        //Execute query
        List<sObject> records = (List<sObject>) Database.query(query);
        Boolean totalRowPresent = false;
        Boolean isSumDefault = false;
        String additionalValueField = null;
        TableData data = buildTableData(records, fieldDescriptions, totalRowPresent, isSumDefault, additionalValueField);
        return data;
    }

   /*
    * Method prepares a query based on delivered data
    */
    private String buildReachbackQuery(
            List<DataTable_Service.FieldDescription> fieldDescriptions,
            Id recordId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName
    ){
        String query = super.buildQuery(
                fieldDescriptions,
                recordId,
                relationField,
                sObjectName
        );
        if(String.isNotBlank(fiscalYearField)) {
            query += ' AND ' + fiscalYearField + ' = ' + fiscalYear;
        }
        System.debug('query: '+query);
        return query;
    }
}