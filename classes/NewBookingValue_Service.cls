/**************************************************************************************
Name:NewBookingValue_Service
Version: 1.0 
Created Date: 03/24/2018
Function: Creates a new bookng for a parent opp

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    03/24/2018      Original Version
* Pradeep Shetty    06/13/2018      US2179: Added Pursuit Type field to the layout
* Pradeep Shetty    07/07/2018      US2270: Add Selling Unit and Performance Unit
* Pradeep Shetty    09/02/2018      US2327: Add methods to support Booking Value Schedule creation
*************************************************************************************/
public with sharing class NewBookingValue_Service extends BaseComponent_Service{
  /*
  * Returns picklist options for Scope of Services
  */
  public List<PicklistValue> getScopeOfServices(){
    return getPicklistValues(
      Opportunity.sObjectType.getDescribe().getName(),
      Opportunity.Scope_of_Services__c.getDescribe().getName());
  }

  /*
  * Returns picklist options for Stage
  */
  public List<PicklistValue> getStages(){

    List<PicklistValue> stagePicklist = getPicklistValues( Opportunity.sObjectType.getDescribe().getName(),
                                                           Opportunity.StageName.getDescribe().getName());

    if(stagePicklist!=null){
      for(Integer i=0; i<stagePicklist.size(); i++){
        if(stagePicklist.get(i).value.contains('Closed')){
          stagePicklist.remove(i);
          i--; //reduce the index value since the value was removed.
        }
      }
    }

    return stagePicklist;
  }

  //Start: US2179
  /*
  * Returns picklist options for Pursuit Types
  */
  public List<PicklistValue> getPursuitTypes(){
    return getPicklistValues(
      Opportunity.sObjectType.getDescribe().getName(),
      Opportunity.Program_Renewal_Rebid__c.getDescribe().getName());
  }  
  //End: US2179
  
  //Start: US2270
  /*
  * Returns picklist options for Pursuit Types
  */
  public List<PicklistValue> getSellingUnits(){
    return getPicklistValues(
      Opportunity.sObjectType.getDescribe().getName(),
      Opportunity.Selling_Unit__c.getDescribe().getName());
  }  
  //End: US2270
  /*
  * Call BaseComponent Service method to get help texts for all fields
  */
  public Map<String, String> getOpportunityFieldsHelpText(){
    return getHelpText('Opportunity', 'NewBookingValueFields');
  } 

  /*
  * Call BaseComponent Service method to get field labels for all fields
  */
  public Map<String, String> getOpportunityFieldsLabel(){
    return getFieldLabel('Opportunity', 'NewBookingValueFields');
  } 

  /*
  * Create booking value based on the Opportunity information provided by lightning component
  */
  public List<Opportunity> createBookingValue(List<Opportunity> opp){

    system.debug('OPP: ' + opp);
    try{
      //List<Opportunity> oppList= (List<Opportunity>) JSON.deserializeStrict(opp, Opportunity.class) ;     
      //opp.Get__c = opp.Get__c*100;
      //opp.Go__c = opp.Go__c*100;
      insert opp;
      return opp;
      //return new List<Opportunity>();
    }
    catch(Exception e){
      throw new AuraHandledException(e.getMessage() + opp);
    }
  }

  //Start: US2270
  /*
  * Check if Performance unit is active
  */
  public Boolean checkPerformanceUnitState(Id unitValue){
    //DE609 - SWS - always exclude CH2M Pre FY18 PUs.
    Unit__c unit =  [SELECT Id, Name, Active__c
                    FROM Unit__c
                    WHERE Id = :unitValue];

    return (unit != null && unit.Active__c 
            && unit.Name != null && !unit.Name.contains('(CH2M) Pre FY18'));
  }

  //End: US2270

  /*
  * US2327: Call BaseComponent Service method to get table column headers
  */
  public List<BaseComponent_Service.DataTableColumn> getColumnHeaders(){
    return getColumnHeaders('Opportunity', 'BookingValuesPreviewColumns');
  }

  /*
  * US2327: Method to build Booking values based on the parameters provided by the user
  */
  public List<Opportunity> getBookingValues(Opportunity bvParameters, Integer duration, String frequency){

    //Get the record type Id
    List<RecordType> bvRecordType = Utility.getRecordTypes('Opportunity', new List<String>{'Booking_Value'});  

    //Add recordtype to BvParameters 
    bvParameters.RecordTypeId = bvRecordType[0].Id;  

    //List of booking values to be returned
    List<Opportunity> bvList = new List<Opportunity>();  

    //Frequency is passed as string. Convert it into Integer
    Integer frequencyInt = Integer.valueOf(frequency);

    //Frequency suffix
    String frequencySuffix = 'QTRLY';

    switch on frequencyInt {
      when 1 {   
        frequencySuffix = 'MTHLY';
      } 
      when 3 {   // when block 2
        frequencySuffix = 'QTRLY';
      }
      when 12 {   // when block 3
        frequencySuffix = 'YRLY';
      }
      when else {     // default block, optional
        frequencySuffix = 'QTRLY';
      }
    }

    //Determine the number of Booking Values to be created
    Integer remainder = Math.mod(Integer.valueOf(duration), frequencyInt);

    //Total number of Booking Values to be created
    Integer bvCount = (Integer.valueOf(duration) / frequencyInt) + (remainder > 0? 1:0);


    //Query all Booking Value for the bvParameters parent Opportunity
    //This count + 1 will be the initial count for booking values.
    //We are trying to maintain continuous count as much as possible
    Integer existingBVCount = [Select count()
                               From Opportunity 
                               Where Parent_Opportunity__c = :bvParameters.Parent_Opportunity__c 
                               And RecordType.DeveloperName in ('Booking_Value', 'Closed_Won_Booking_Value')];




    //Loop based on bvCount + existingBVCount and create Booking Values
   for(Integer count = 1; count <=bvCount; count ++ ){

      //Clone bvParameters to get all Opportunity field
      Opportunity newBV = bvParameters.clone();

      //Change record type to Booking Value
      newBV.RecordTypeId = bvRecordType[0].Id;

      //Change the name to match the standard naming convention
      newBV.Name = bvParameters.Name + ' ' + frequencySuffix + ' BV' + (count + existingBVCount);

      //Change CloseDate based on the frequency
      newBV.CloseDate = bvParameters.CloseDate.addMonths(frequencyInt*(count - 1));

      //Change Target Project Start Date to maintain the same difference as in the first BV
      newBV.Target_Project_Start_Date__c = bvParameters.Target_Project_Start_Date__c.addMonths(frequencyInt*(count - 1));

      //Change the duration. If the Overall duration is multiple of the frequency, 
      //duration of last BV is same as the frequency else duration is the remainder of overall duration
      if(remainder > 1 && count == bvCount){
        newBV.Duration_Months__c = remainder;
      }else{
        newBV.Duration_Months__c = frequencyInt;
      }

      //Add it to the list
      bvList.add(newBV);
    }

    //Return the list
    return bvList;
  } 
}