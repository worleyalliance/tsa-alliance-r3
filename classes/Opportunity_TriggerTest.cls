/**************************************************************************************
Name: Opportunity_TriggerTest
Version: 1.0 
Created Date: 08/04/2017
Function: To Test Opportunity_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep           08/04/2017      Original Version
* Manuel Johnson    11/28/2017      Updated for US1303 - to include checks for Sales Plan automation
* Pradeep           03/02/2018      Updated for DE367 - To check deletion of PWLI
* Chris Cornejo     08/24/2018      US2362 - Oppty Scope of Services translation to Oracle Service Type field
* Madhu Shetty    	15/12/2018       US2537: Plan week changes were deleted based on the steps mentioned in the user story.
*************************************************************************************/

@IsTest
public with sharing class Opportunity_TriggerTest {
    
    //Constants
    private static final String PROFILE_SALES_SUPER_USER    = 'Sales Super User';
    private static final String USERNAME                    = 'salesOppTriggerTest@jacobstestuser.net';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        
        //Builders
        TestHelper.CampaignBuilder camBuilder    = new TestHelper.CampaignBuilder();
        TestHelper.UserBuilder userBuilder       = new TestHelper.UserBuilder();
        
        //Create test user
        User testUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();
        
        //Create test records
        system.runAs(testUser){
            
            //Create two campaigns (A&T and B&I)
            Campaign campaignBI = camBuilder.build().save().getRecord();
            Campaign campaignAT = camBuilder.build().withATLobBu().save().getRecord();
        }
    }
    
    @isTest
    private static void shouldSetSellingBUandSellingLOBvaluesOnInsert() {
        
        Opportunity opp; 
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).withSellingUnit('BIAF - Americas National').save().getOpportunity();      
        }
        
        //Stop Test
        Test.stopTest();
        
        //Query the Opportunity and check the Wave Selling Unit, Selling LOB and Selling BU
        List<Opportunity> testOpportunity = [Select Business_Unit__c,
                                             Line_Of_Business__c,
                                             Wave_Selling_Unit__c
                                             From Opportunity 
                                             Where Id = :opp.Id];
        
        //Assertions
        //Line Of Business
        System.assertEquals('JESA', testOpportunity[0].Line_of_Business__c);
        
        //Business Unit
        // System.assertEquals('BIAF - Americas', testOpportunity[0].Business_Unit__c);
        
        //Wave Selling Unit
        // System.assertEquals('BIAF - Americas National', testOpportunity[0].Wave_Selling_Unit__c);
        
    }
    
    @isTest
    private static void shouldSetExecutingBUandExecutingLOBvaluesOnUpdate() {
        
        Opportunity opp; 
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();      
            
            //Update Selling Unit on the Opportunity
            opp.Selling_Unit__c = 'BIAF - Americas National';
            Update opp;
        }
        
        //Stop Test
        Test.stopTest();
        
        //Query the Opportunity and check the Wave Selling Unit, Selling LOB and Selling BU
        List<Opportunity> testOpportunity = [Select Business_Unit__c,
                                             Line_Of_Business__c,
                                             Wave_Selling_Unit__c
                                             From Opportunity 
                                             Where Id = :opp.Id];
        
        //Assertions
        //Line Of Business
        System.assertEquals('JESA', testOpportunity[0].Line_of_Business__c);
        
        //Business Unit
        // System.assertEquals('BIAF - Americas', testOpportunity[0].Business_Unit__c);
        
        //Wave Selling Unit
        // System.assertEquals('BIAF - Americas National', testOpportunity[0].Wave_Selling_Unit__c);
    }  
    
    @isTest
    private static void shouldSetFiscalYearandWeek() {
        
        Opportunity opp; 
        
        //Get the user
        User testUser = [Select Id from User where UserName = :USERNAME Limit 1];
        
        //Query Fiscal Year Setting to get current and next FY
        FiscalYearSettings currentFY = [Select Name, StartDate, EndDate From FiscalYearSettings 
                                 Where StartDate < TODAY and EndDate > TODAY];
        
        Date newCloseDate = currentFY.EndDate.addMonths(1);
        
        FiscalYearSettings nextFY = [Select Name, StartDate, EndDate From FiscalYearSettings 
                                 Where StartDate < :newCloseDate and EndDate > :newCloseDate];
        
        Decimal fiscalWeek = math.MIN(53,math.MAX(1,math.CEIL((nextFY.StartDate.daysBetween(newCloseDate)+1)/7.0)));
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();
            
            //Update Close Date
            opp.CloseDate = newCloseDate;
            opp.Target_Project_Start_Date__c = newCloseDate.addMonths(1);
            update opp;
        }
        
        //Stop Test
        Test.stopTest();
        
        //Query the Opportunity and check the Fiscal Year and Week
        List<Opportunity> testOpportunity = [Select Fiscal_Year__c, num_fiscal_week_close_date__c, CloseDate 
                                             From Opportunity
                                             Where Id = :opp.Id];
        
        //Assertions
        //Fiscal Year
        System.assertEquals('FY' + nextFY.Name.right(2), testOpportunity[0].Fiscal_Year__c);
        
        //Fiscal Week
        System.assertEquals(fiscalWeek, testOpportunity[0].num_fiscal_week_close_date__c);
        
    }

    /*
    * US2537: Test method to test that Sales Plan is removed from Opp, 
    * when there is no Sales Plan that matches the Selling Unit and Fiscal Year
    */
    @IsTest
    private static void shouldRemoveSalesPlan(){

      Opportunity opp; 
      
      //Get the user
      User testUser = [Select Id from User where UserName = :USERNAME Limit 1];
      
      //Start Test
      Test.startTest();
      
      System.runAs(testUser){
        //Create Test Account
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        //Create Test Opportunity with a specific Selling Unit
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        opp = opportunityBuilder.build().withAccount(acc.Id).getOpportunity();
        
        //Update Close Date
        opp.CloseDate = Date.today();
        opp.Target_Project_Start_Date__c = Date.today().addMonths(1);
        insert opp;

        //Get the campaign 
        List<Campaign> salesPlan  = [Select Id from Campaign where pl_line_of_business__c = 'JESA'];

        //Get the Opportunity Record
        List<Opportunity> oppTest = [Select Id,
                                            CloseDate,
                                            CampaignId 
                                    From Opportunity 
                                    Where Id = :opp.Id];

        //Check that Opp has been assigned a sales plan
        // System.assertEquals(salesPlan[0].id, oppTest[0].CampaignId);
        
        //Change Opp Id
        oppTest[0].CloseDate = Date.today().addYears(1);

        update oppTest;
      }
      
      //Stop Test
      Test.stopTest();
      
      //Query the Opportunity and check Salesplan, Planweek, and PWLI
      List<Opportunity> updatedOpp = [Select Id,
                                          CampaignId
                                   From Opportunity 
                                   Where CreatedById = :testUser.id];

      //Assertions
      //Sales Plan should be blank on the Opp
      System.assert(String.isBlank(updatedOpp[0].campaignId));
      
    }
    
    //US2362
    @isTest
    private static void shouldSetServiceTypeonUpdate() {
        
        Opportunity opp; 
        String ServiceTypeafterInsert;
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
            //Create Test Account
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            //Create Test Opportunity with a specific Selling Unit
            TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
            opp = opportunityBuilder.build().withAccount(acc.Id).getOpportunity();      
            
            //Insert Scope of Services on the Opportunity
            opp.Scope_of_Services__c = 'Staff Augmentation';
            Insert opp;
                        
            //Query the Opportunity and check the Service Type
            List<Opportunity> testOpportunity = [Select Service_Type__c
                                             From Opportunity 
                                             Where Id = :opp.Id];
            ServiceTypeafterInsert = testOpportunity[0].Service_Type__c;                                 
            
            //Update Scope of Services on the Opportunity
            opp.Scope_of_Services__c = 'Procurement';
            Update opp;
              
        }
        
        //Stop Test
        Test.stopTest();
        
        //Query the Opportunity and check the Service Type
        List<Opportunity> testOpportunity = [Select Service_Type__c
                                             From Opportunity 
                                             Where Id = :opp.Id];
        
        //Assertions
        //Service Type
        System.assertEquals('Secondment (SO)', ServiceTypeafterInsert);
        System.assertEquals('Procurement (PO)', testOpportunity[0].service_type__c);
        
    }  
}