/**************************************************************************************
Name:BatchGenerateOpportunityNumber
Version: 1.0 
Created Date: 12.28.2018
Function: Test class to test batch class for generating Opportunity number

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                   
* Pradeep Shetty    12/28/2018      Original Version
*************************************************************************************/
@isTest
public with sharing class BatchGenerateOpportunityNumber_Test {
    
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME                 = 'salesOpp@testuser.com';      
    
    //Test data setup
    @testSetup
    static void setUpTestData(){

        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

        System.runAs(salesUser){

            //Account required for the Opportunity
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acct = accountBuilder.build().save().getRecord(); 

            List<Opportunity> newOppList = new List<Opportunity>();

            //Generate 50 opportunities
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            for(Integer i=0; i<50; i++){
                newOppList.add(oppBuilder.build().withAccount(acct.Id).getOpportunity());
            }

            insert newOppList; 

            /*Get these 300 opps and delete the Opp number generated. Opp number is generated as part of automation on creation. 
            We are cleaning up Opp number so that the batch number can generate it*/ 
            

                        
            List<Opportunity> updateOppList = new List<Opportunity>();
            for(Opportunity currentOpp: [Select id,Opportunity_Number__c from Opportunity]){
                currentOpp.Opportunity_Number__c = '';
                updateOppList.add(currentOpp);
            }

            update updateOppList;

        }
    }
    @isTest
    private static void test_BatchClass(){       
        Test.startTest();
        BatchGenerateOpportunityNumber oppNumberBatch = new BatchGenerateOpportunityNumber();
        Id batchId = Database.executeBatch(oppNumberBatch);
        Test.stopTest();

        //Validate results
        //Fetch newly created opp
        for(Opportunity opp: [Select System_Number__c, Opportunity_Number__c from Opportunity]){
            system.assertEquals(Utility.generateAlphaNumericCode(Integer.valueOf(opp.System_Number__c)), opp.Opportunity_Number__c);
        }
    }
    
}