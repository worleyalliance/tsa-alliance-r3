/**************************************************************************************
Name: recordtypeController
Version: 
Created Date: 
Function: Create document button on the LDT tables in Project and Group

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    7/25/2017        Changes made re:DE165, add other record types to the list
*************************************************************************************/
public class recordtypeController {
    public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues(String id, String objName)
    {
        string objectName;
        string recordType;
        List<Schema.RecordTypeInfo> recordtypes = Library__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        
        if(id != null && objName == 'Jacobs_Project__c')
        {
            System.debug('Its a project');
            objectName ='Project';
            recordType = [Select Recordtype.developerName from Jacobs_Project__c where Id = :id LIMIT 1].Recordtype.developerName;
            System.debug(recordType);
        }
        else
        {
            System.debug('Its a group');
            objectName = 'Group';
        }
        System.debug(objectName);
        
        if (objectName == 'Project')
        {//if project
           system.debug('recordType ' + recordType);
           
           if (recordType == 'task_orders')
           {//for task orders
               List <LDT_Lightning_Filter__c> rtlist = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'Jacobs_Project__c' and OBJ_RecordType_API_Name__c = 'task_orders' and Active__c = True];
               for(RecordTypeInfo rt : recordtypes)
               {//for rt
                   //List <LDT_Lightning_Filter__c> rtlist = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'Jacobs_Project__c' and OBJ_RecordType_API_Name__c = 'task_orders' and Active__c = True];
                   for (LDT_Lightning_Filter__c l: rtlist)
                   {
                       if (rt.getName() == l.Label_del__c && rt.IsAvailable() && rt.getName() != 'Master')
                       recordtypemap.put(rt.getRecordTypeId(), rt.getName());
                   }
             
               }//end for rt
           }//for task orders
           else if (recordType == 'A_T_Program')
           {
               List <LDT_Lightning_Filter__c> rtlist1 = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'Jacobs_Project__c' and OBJ_RecordType_API_Name__c = 'A_T_Program' and Active__c = True];
               for(RecordTypeInfo rt : recordtypes)
               {//for rt
                   //List <LDT_Lightning_Filter__c> rtlist1 = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'Jacobs_Project__c' and OBJ_RecordType_API_Name__c = 'A_T_Program' and Active__c = True];
                   for (LDT_Lightning_Filter__c k: rtlist1)
                   {
                       if (rt.getName() == k.Label_del__c && rt.getName() != 'Master' && rt.IsAvailable())
                       recordtypemap.put(rt.getRecordTypeId(), rt.getName());
             
                   }
               }//end for rt
           }
        else
        {//for proj
            for(RecordTypeInfo rt : recordtypes)
            {//for rt
                if(rt.getName() != 'Master' && rt.IsAvailable())
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }//end for rt
        }//end for proj
        }//end if project
        else
        {
        List <LDT_Lightning_Filter__c> rtgrplist = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'AT_Group__c' and Active__c = True];
        for(RecordTypeInfo rt : recordtypes)
        {
            //List <LDT_Lightning_Filter__c> rtgrplist = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'AT_Group__c' and Active__c = True];
            for (LDT_Lightning_Filter__c g: rtgrplist)
            {
                if (rt.getName() == g.Label_del__c)
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }

        }
        }
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel)
    {
        Id recid = Schema.SObjectType.Library__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }
}