/**************************************************************************************
Name: OracleCalloutRetryService
Version: 1.0 
Created Date: 05.07.2017
Function: Service that hanldes all traffic related to callout retry action

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.07.2017         Original Version
*************************************************************************************/
public with sharing class OracleCalloutRetryService {

    public final static Integer MAX_RETRY_ATTEMPTS = 5;

    private Boolean successSync = true;

    private RetrySyncLog__c retryLog;
    private Log__c log;

    public void logEntry(
            Id sfdcId,
            String objectType,
            String appName,
            String className,
            String event,
            String eventType,
            String exceptionMessage,
            String status
    ){
        if(retryLog == null){
            retryLog = new RetrySyncLog__c(
                    RecordId__c = sfdcId,
                    ObjectType__c = objectType,
                    NumberOfAttempts__c = 1
            );
        } else {
            retryLog.NumberOfAttempts__c++;
        }
        upsert retryLog;
        log = new Log__c(
                AppName__c = appName,
                ClassName__c = className,
                Sfid__c = sfdcId,
                Event__c = event,
                EventType__c = eventType,
                Exception__c = exceptionMessage,
                Status__c = status,
                Retry_Sync_Log__c = retryLog.Id
        );
        insert log;
        System.debug('Log: '+log);
        successSync = false;
    }

    public void setRetryLog(RetrySyncLog__c retryLog){
        this.retryLog = retryLog;
    }

    public void deleteRetryLogIfNeeded(){
        if(successSync && retryLog != null) {
            delete retryLog;
        }
    }
}