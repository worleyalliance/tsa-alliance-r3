/**************************************************************************************
Name: JacobsOracleSyncRetryBatch
Version: 1.0 
Created Date: 05.07.2017
Function: Batch which will try to repeat callout when error

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.07.2017         Original Version
*************************************************************************************/
global with sharing class JacobsOracleSyncRetryBatch implements Schedulable, Database.Batchable<sObject>{

    private static Map<String, String> fieldsByObjectType = new Map<String, String>{
            Account.getSObjectType().getDescribe().getName() =>
                    'Name, OracleAccountID__c, Operating_Unit__c, Status__c, CH2M_Status__c, CH2M_Oracle_Account_ID__c, Account_Group__c, Acct_Group_ID_Name__c',
            Jacobs_Project__c.getSObjectType().getDescribe().getName() =>
                    'Legal_Entity__c, txt_description__c, Project_Start_Date__c, Oracle_Account_ID__c, Operating_Unit__c, '+
                    'Request_Type__c, Project_ID__c, Jacobs_Sales_Lead_Employee_Number__c, B_P_Request_Created_Date__c, ' +
                    'Project_Accountant_Employee_Number__c, Project_Manager_Employee_Number__c, TasK_Validation__c, Market_Sector__c, '+
                    'Area_of_Business__c, Controlling_Performance_Unit__c, B_P_Budget_Local__c'
    };

    private static Map<String, String> calloutHandlerNameByObjectType = new Map<String, String>{
            Account.getSObjectType().getDescribe().getName() => 'accountQueueableHandler',
            Jacobs_Project__c.getSObjectType().getDescribe().getName() => 'ProjectQueueableHandler'
    };

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 30);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Integer maxNumberOfAttempts = OracleCalloutRetryService.MAX_RETRY_ATTEMPTS;
        String query = 'SELECT NumberOfAttempts__c, ObjectType__c, RecordId__c FROM RetrySyncLog__c WHERE NumberOfAttempts__c = null OR NumberOfAttempts__c <= :maxNumberOfAttempts';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<RetrySyncLog__c> retrySyncLogs){
        rerunCallouts(retrySyncLogs);
    }

    global void finish(Database.BatchableContext bc){

    }

    private void rerunCallouts(List<RetrySyncLog__c> retrySyncLogs){
        Map<String, List<Id>> objectIdsByObjectType = getObjectIdsByObjectType(retrySyncLogs);
        Map<Id, RetrySyncLog__c> retryLogByObjectId = getRetryLogByObjectId(retrySyncLogs);
        Map<String, List<sObject>> objectByObjectType = getObjectByObjectType(objectIdsByObjectType);
        enqueueJobs(objectByObjectType, retryLogByObjectId);
    }

    private Map<String, List<Id>> getObjectIdsByObjectType(List<RetrySyncLog__c> retrySyncLogs){
        Map<String, List<Id>> objectIdsByObjectType = new Map<String, List<Id>>();
        for(RetrySyncLog__c retrySyncLog : retrySyncLogs){
            if(!objectIdsByObjectType.containsKey(retrySyncLog.ObjectType__c)){
                objectIdsByObjectType.put(retrySyncLog.ObjectType__c, new List<Id>());
            }
            objectIdsByObjectType.get(retrySyncLog.ObjectType__c).add(retrySyncLog.RecordId__c);
        }
        return objectIdsByObjectType;
    }

    private Map<Id, RetrySyncLog__c> getRetryLogByObjectId(List<RetrySyncLog__c> retrySyncLogs){
        Map<Id, RetrySyncLog__c> retryLogByObjectId = new Map<Id, RetrySyncLog__c>();
        for(RetrySyncLog__c retrySyncLog : retrySyncLogs){
            retryLogByObjectId.put(retrySyncLog.RecordId__c, retrySyncLog);
        }
        return retryLogByObjectId;
    }

    private Map<String, List<sObject>> getObjectByObjectType(Map<String, List<Id>> objectIdsByObjectType){
        Map<String, List<sObject>> objectByObjectType = new Map<String, List<SObject>>();
        for(String objectType : objectIdsByObjectType.keySet()) {
            if (fieldsByObjectType.containsKey(objectType)){
                List<Id> objectIds = objectIdsByObjectType.get(objectType);
                String query = 'SELECT '+fieldsByObjectType.get(objectType)+ ' FROM '+ objectType + ' WHERE Id IN :objectIds';
                List<sObject> records = Database.query(query);
                objectByObjectType.put(objectType, records);
            }
        }
        return objectByObjectType;
    }

    private void enqueueJobs(Map<String, List<sObject>> objectByObjectType, Map<Id, RetrySyncLog__c> retryLogByObjectId){
        for(String objectType : objectByObjectType.keySet()){
            if(calloutHandlerNameByObjectType.containsKey(objectType)) {
                for(sObject obj : objectByObjectType.get(objectType) ) {
                    OracleQueueableHandler handler = (OracleQueueableHandler) Type.forName(calloutHandlerNameByObjectType.get(objectType)).newInstance();
                    handler.setObject(obj);
                    handler.setRetrySyncLog(retryLogByObjectId.get(obj.Id));
                    System.enqueueJob(handler);
                }
            }
        }
    }
}