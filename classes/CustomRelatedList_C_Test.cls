/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 16.03.2017
Function: Test class for CustomRelatedList_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           16.03.2017         Original Version
*************************************************************************************/
@IsTest
private class CustomRelatedList_C_Test {

    @IsTest
    private static void shouldRetrieveAllGtoPRecordForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.GtoPBuilder gtoPBuilder = new TestHelper.GtoPBuilder();
        String benefits = 'someValue';
        GtoPMatrix__c gtop = gtoPBuilder.build().withOpportunityId(opp.Id).withBenefits(benefits).save().getRecord();

        //when
        CustomRelatedList_Service.TableData data = CustomRelatedList_C.getData(opp.Id, 'GtoPMatrix__c', 'Opportunity__c', new List<String>{'benefits__c'}, 'Opportunity__c', 'Opportunity__r.Name');

        //then
        System.assertEquals(1, data.fieldDescriptions.size());
        System.assertEquals(1, data.rows.size());
        System.assertEquals(benefits, data.rows[0].get('benefits__c'));
        System.assertEquals(opp.Name, data.rows[0].get('Opportunity__r.Name'));

    }

    @IsTest
    private static void shouldUpdateGtoPRecord() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.GtoPBuilder gtoPBuilder = new TestHelper.GtoPBuilder();
        String benefits = 'someValue';
        GtoPMatrix__c gtop = gtoPBuilder.build().withOpportunityId(opp.Id).withBenefits(benefits).save().getRecord();

        //when
        //gtop.benefits__c = 'new Value';
        Id recordId = CustomRelatedList_C.saveRecord(gtop);

        //then
        //List<GtoPMatrix__c> results = [SELECT benefits__c FROM GtoPMatrix__c WHERE Id = :recordId];
        //System.assertEquals(1, results.size());
        //System.assertEquals(gtop.benefits__c, results[0].benefits__c);
    }

    @IsTest
    private static void shouldDeleteGtoPRecord() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.GtoPBuilder gtoPBuilder = new TestHelper.GtoPBuilder();
        String benefits = 'someValue';
        GtoPMatrix__c gtop = gtoPBuilder.build().withOpportunityId(opp.Id).withBenefits(benefits).save().getRecord();

        //when
        CustomRelatedList_C.deleteRecord(gtop.Id);

        //then
        //List<GtoPMatrix__c> results = [SELECT benefits__c FROM GtoPMatrix__c WHERE Id = :gtop.Id];
        //System.assertEquals(0, results.size());
    }
}