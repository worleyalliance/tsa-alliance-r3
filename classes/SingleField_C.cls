/**************************************************************************************
Name: SingleField_C
Version: 1.1
Created Date: 24.02.2017
Function: Controller class for SingleFiled Lightning Component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
* Pradeep Shetty            07.27.2017         DE167: Set Edit Button visibility
*************************************************************************************/

public with sharing class SingleField_C{

    private static SingleField_Service service = new SingleField_Service();

   /*
    * Retrieves given field value of give record
    */
    @AuraEnabled
    public static Object retrieveFieldValue(String objectType, String fieldName, Id recordId) {
        return service.retrieveFieldValue(objectType, fieldName, recordId);
    }

   /*
    * Updates given field value of give record
    */
    @AuraEnabled
    public static void updateFieldValue(String objectType, String fieldName, Id recordId, String value) {
        service.updateFieldValue(objectType, fieldName, recordId, value);
    }

   /*
    * Checks the edit permission for the object, record, and field
    */
    @AuraEnabled
    public static Boolean checkEditPermission(String objectType, String fieldName, Id recordId) {
        return service.checkEditPermission(objectType, fieldName, recordId);
    }    
}