/**************************************************************************************
Name: Jacobs_Project_TriggerTest
Version: 1.0 
Created Date: 04.20.2017
Function: Jacobs Project Trigger Test Class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04/20/2017     Original Version
* Pradeep Shetty    09/27/2017     mpdified for R100117 release changes
*************************************************************************************/

@isTest
private class Jacobs_Project_TriggerTest {

  //Constants
  private static final String USERNAME  = 'sysadmin@testuser.com';
  private static final String FY_2018               = 'FY2018';
  private static final String CURRENCY_USD          = 'USD'; 

  public final static String  PROJECT_TYPE_CONTRACT = 'CONTRACT';

  //Test data setup
  @testSetup
  static void setUpTestData(){

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User adminUser = userBuilder.build().withUserName(USERNAME).save().getRecord();

    System.runAs(adminUser){

      //Account required for contacts
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      Account acct = accountBuilder.build().save().getRecord(); 

      //Create Contact
      TestHelper.ContactBuilder contactBuilder = new TestHelper.ContactBuilder();
      Contact con = contactBuilder.build().withAccount(acct.Id).save().getRecord();

      //Create an Opportunity
      TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
      Opportunity opp = oppBuilder.build().withAccount(acct.Id).save().getOpportunity();      

    }
  }  

  @isTest static void projectCallout() {
    // Implement test code
    TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
    Jacobs_Project__c proj = projectBuilder.build().getRecord();

    Test.startTest();
    insert proj;
    Test.stopTest();

  }

  @IsTest
  private static void shouldUpdateCurrencyValueBasedOnRates(){
    //given
    String ficalYear = 'FY2017';
    String localCurrencyCode = 'EUR';
    Decimal conversionRate = 2;
    List<Currency_Exchange_Rates__c> currencyExchangeRates = new List<Currency_Exchange_Rates__c>();
    currencyExchangeRates.add(new Currency_Exchange_Rates__c(
            Name = localCurrencyCode + '-' + ficalYear,
            Local_Currency_Code__c = localCurrencyCode,
            Fiscal_Year__c = ficalYear,
            Conversion_Rate__c = conversionRate
    ));
    insert currencyExchangeRates;

    TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
    Jacobs_Project__c project = projectBuilder.build().getRecord();
    Decimal actualCostLocal = 10;
    Decimal costPTDLocal = 10;

    //when
    project.Local_Currency__c = localCurrencyCode;
    project.Fiscal_Year__c = ficalYear;
    project.Actual_Cost_Local__c = actualCostLocal;
    project.Cost_PTD_Local__c = costPTDLocal;
    insert project;

    //then
    List<Jacobs_Project__c> results = [SELECT Actual_Cost__c, Cost_PTD__c FROM Jacobs_Project__c WHERE Id = :project.Id];
    System.assertEquals(actualCostLocal * conversionRate, results[0].Actual_Cost__c );
    System.assertEquals(costPTDLocal * conversionRate, results[0].Cost_PTD__c );
  }

  @IsTest
  private static void shouldCopyCurrencyValueWhenConversionRateIsMissing(){
    //given
    String ficalYear = 'FY2017';
    String localCurrencyCode = 'EUR';

    TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
    Jacobs_Project__c project = projectBuilder.build().getRecord();
    Decimal actualCostLocal = 10;
    Decimal costPTDLocal = 10;

    //when
    project.Local_Currency__c = localCurrencyCode;
    project.Fiscal_Year__c = ficalYear;
    project.Actual_Cost_Local__c = actualCostLocal;
    project.Cost_PTD_Local__c = costPTDLocal;
    insert project;

    //then
    List<Jacobs_Project__c> results = [SELECT Actual_Cost__c, Cost_PTD__c FROM Jacobs_Project__c WHERE Id = :project.Id];
    System.assertEquals(actualCostLocal, results[0].Actual_Cost__c );
    System.assertEquals(costPTDLocal, results[0].Cost_PTD__c );
  }  

  /*
  * Scenario: Check that when the finance YTD project is created, if the parent project is not present then create the project.
  */
  @IsTest
  private static void shouldCreateParentProject(){

    //Get the user
    User adminUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //Get the Account created by the sales user
    Account acct = [Select Id from Account where CreatedById = :adminUser.Id Limit 1];

    //Get the Contact created by the sales user
    Contact con = [Select Id from Contact where CreatedById = :adminUser.Id Limit 1];

    //Get the Opportunity created by the sales user
    Opportunity opp = [Select Id from Opportunity where CreatedById = :adminUser.Id Limit 1];

    Test.startTest();

    System.runAs(adminUser)
    {
      //Create a Project without specifying the record type
      TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
      Jacobs_Project__c project = projectBuilder.build().getRecord();

      project.Name                         = 'ABC12345';
      project.Project_Code__c              = 'ABC12345';
      project.Project_Id__c                = '987654';
      project.PU_Description__c            = '019243';
      project.Line_of_Business__c          = 'BIAF';
      project.Project_Start_Date__c        = Date.today();
      project.Project_Status__c            = Jacobs_Project_TriggerHandler.PROJECT_STATUS_ACTIVE ;
      project.Contract_Type__c             = 'PR';
      project.Govt_Agency__c               = 'N/A';
      project.Market_Sector__c             = 'C';
      project.Project_Type__c              =  PROJECT_TYPE_CONTRACT;
      project.Service_Type__c              =  'N/A';
      project.Operating_Unit__c            =  'US_OU';
      project.Project_Accountant__c        = con.Id;
      project.Project_Manager__c           = con.Id;
      project.lr_Client__c                 = acct.Id;
      project.Actual_Cost_Local__c         = 100;
      project.Actual_Gross_Margin_Local__c = -100;
      project.Cost_Current_Month_Local__c  = 0;
      project.Cost_PTD_Local__c            = 100;
      project.Cost_YTD_Local__c            = 100;
      project.Local_Currency__c            = CURRENCY_USD;
      project.Fiscal_Year__c               = FY_2018;
      project.Opportunity__c               = opp.Id; 

      insert project;

      //Check that the FY project is created and PTD_Project__c is populated
      Jacobs_Project__c fyProject = [Select Id,
                                            Project_Id__c,
                                            Fiscal_Year__c,
                                            PTD_Project__c, 
                                            RecordType.DeveloperName, 
                                            Project_ID_FY_Key__c 
                                     From Jacobs_Project__c 
                                     Where Id = :project.Id 
                                     Limit 1];
      //Check the values                                     
      System.assert(String.isNotBlank(fyProject.PTD_Project__c));
      System.assertEquals(Jacobs_Project_TriggerHandler.FIN_YTD_PROJECT_RECORD_TYPE_NAME, fyProject.RecordType.DeveloperName);
      System.assertEquals(fyProject.Project_Id__c + fyProject.Fiscal_Year__c, fyProject.Project_ID_FY_Key__c);

      //Get the parent Finance Project
      Jacobs_Project__c finProject = [Select Id,
                                            Project_Id__c,
                                            Fiscal_Year__c,
                                            PTD_Project__c, 
                                            RecordType.DeveloperName, 
                                            Project_ID_FY_Key__c 
                                     From Jacobs_Project__c 
                                     Where Id = :fyProject.PTD_Project__c
                                     Limit 1];
      //Check the values                                
      System.assert(String.isBlank(finProject.PTD_Project__c));
      System.assertEquals(Jacobs_Project_TriggerHandler.FIN_PROJECT_RECORD_TYPE_NAME, finProject.RecordType.DeveloperName);
      System.assert(String.isBlank(finProject.Project_ID_FY_Key__c));
    }

    Test.stopTest();
  } 
}