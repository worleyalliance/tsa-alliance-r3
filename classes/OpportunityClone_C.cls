/**************************************************************************************
Name: OpportunityClone_C
Version: 1.0 
Created Date: 29.02.2017
Function: Makes a child copy of an opportunity and child records of the opprotunity

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   29.02.2017         Original Version
*************************************************************************************/
public with sharing class OpportunityClone_C {

    private static OpportunityCloneService service = new OpportunityCloneService();

   /*
    *   Method which makes a copy of opportuniy with given Id
    *   Besides opportunity record also children records are copied
    *   Fields which are to be copied from parent opportunity are set in custom metadata types
    *   Child objects which are to be copied are also set in custom metatada types.
    *   For child object all creatable fields are copied.
    */
    @AuraEnabled
    public static Id doClone(Id parentOpportunityId) {
        Id cloneOpportunityId = service.doClone(parentOpportunityId);
        return cloneOpportunityId;
    }

 }