/**************************************************************************************
Name: SharePointConfiguration
Version: 1.0 
Created Date: 24.02.2017
Function: Configuration data for SharePoint integration

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/

public with sharing class SharePointConfiguration {
/*
    public final static String NAMED_CREDENTIAL_DEV_NAME = 'jacobsengineeringSharePoint';
    public final static String SHARE_POINT_ENDPOINT_URL = 'callout:'+NAMED_CREDENTIAL_DEV_NAME;

    public final static String CREATE_FOLDER_URL = '/_api/web/folders';
    public final static String GET_FOLDER_FILES_URL = '/_api/web/GetFolderByServerRelativeUrl(@v)/Files?@v=\'$FOLDER_PATH\'';

    public final static String GET_FOLDER_PATH_URL = '/_api/web/GetFolderByServerRelativeUrl(@v)?@v=\'$FILE_PATH\'';

    public final static String FILE_STARTUPLOAD_URL =   '/_api/web/GetFileByServerRelativeUrl(@v)/startupload(\'$GUID\')?@v=\'$FILE_PATH\'';
    public final static String FILE_CONTINUEUPLOAD_URL ='/_api/web/GetFileByServerRelativeUrl(@v)/continueupload(uploadId=\'$GUID\',fileOffset=\'$OFFSET\')?@v=\'$FILE_PATH\'';
    public final static String FILE_FINISHUPLOAD_URL =  '/_api/web/GetFileByServerRelativeUrl(@v)/finishupload(uploadId=\'$GUID\',fileOffset=\'$OFFSET\')?@v=\'$FILE_PATH\'';

    public final static String FOLDER_PATH = '$FOLDER_PATH';
    public final static String FILE_NAME = '$FILE_NAME';
    public final static String FILE_PATH = '$FILE_PATH';
    public final static String GUID = '$GUID';
    public final static String OFFSET = '$OFFSET';

    public final static String CREATE_FOLDER_BODY = '{ "__metadata": { "type": "SP.Folder" }, "ServerRelativeUrl": "$FOLDER_PATH"}';

    public final static String CREATE_FILE_URL = '/_api/web/GetFolderByServerRelativeUrl(@v)/Files/add(url=\'$FILE_NAME\',overwrite=true)?@v=\'$FOLDER_PATH\'';

    public final static String ROOT_SHAREPOINT_FOLDER = 'Shared Documents/';

    public final static String SHAREPOINT_ALL_ITEMS_FORM_URL = '/Shared Documents/Forms/AllItems.aspx';

    private static Map<String, List<String>> folderStructureByObjectType = new Map<String, List<String>>();

    public static List<String> getFolderStructureFor(String objectType){
        if(!folderStructureByObjectType.containsKey(objectType)){
            List<JacobsSharePointConfiguration__mdt> configuration = [
                    SELECT Value__c
                    FROM JacobsSharePointConfiguration__mdt
                    WHERE Type__c = :objectType
                    ORDER BY Order__c];
            List<String> folderStructure = new List<String>();
            folderStructureByObjectType.put(objectType, folderStructure);
            for(JacobsSharePointConfiguration__mdt c : configuration){
                folderStructure.add(c.Value__c);
            }
        }
        return folderStructureByObjectType.get(objectType);
    }*/

   /*
    * Wrapper classes for all files server response
    */
/*
    public class FilesResponse{
        public Descriptive d {get; set;}
    }

    public class Descriptive {
        public List<File> results {get; set;}
        public Integer ContinueUpload {get; set;}
        public Integer StartUpload {get; set;}
    }

    public class File{

        @AuraEnabled
        public String Name {get; set;}

        @AuraEnabled
        public String LinkingUrl {get; set;}

        public String ServerRelativeUrl {get; set;}
    }
    */
}