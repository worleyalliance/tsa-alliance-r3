/**************************************************************************************
Name: OpportunityMultiOffice_Service
Version: 1.0 
Created Date: 29.02.2017
Function: Service for all operations used on OpportunityMultiOffice component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
* Stefan Abramiuk   04/11/2017      Original Version
* Pradeep Shetty    08/20/2018      US2396: Adjust MOS start dates automatically
* Scott Stone       09/07/2018      US2398: Allow Write Ups with blank revenue or revenue < GM
*************************************************************************************/
public class OpportunityMultiOffice_Service extends BaseComponent_Service {

   /*
    *   Retrieves picklist values for Resource_Type__c
    */
    public List<PicklistValue> getResourceTypes(){
        return getPicklistValues(
                Multi_Office_Split__c.sObjectType.getDescribe().getName(),
                Multi_Office_Split__c.Scope_of_Services__c.getDescribe().getName());
    }

   /*
    *   Retrieves picklist values for Worley_Capability_Subsector__c
    */
    public List<PicklistValue> getSubSectors(){
        return getPicklistValues(
                Multi_Office_Split__c.sObjectType.getDescribe().getName(),
                Multi_Office_Split__c.Worley_Capability_Subsector__c.getDescribe().getName());
    }    
    
   /*
    * Retrieves MultiOffice split records for opportunity
    * US2396: Added Lag and Auto Adjust fields to the query
    */
    public List<MultiOffice> getMultiOffices(Id opportunityId){
        validateObjectAccesible(Multi_Office_Split__c.sObjectType.getDescribe().getName());

        List<Multi_Office_Split__c> multiOfficeSplits = [SELECT Performance_Unit_PU__r.Name,
                                                                Scope_of_Services__c,
                                                                Worley_Capability_Subsector__c,
                                                                Revenue__c, 
                                                                Gross_Margin__c, 
                                                                Average_Gross_Margin__c,
                                                                Hours__c, 
                                                                Start_Date__c, 
                                                                End_Date__c, 
                                                                Number_of_Months__c,
                                                                Lead__c, 
                                                                Name,
                                                                Opportunity__r.Line_of_Business__c,
                                                                Opportunity__r.IsWon,
                                                                Lag__c,                             //US2396: Added Lag field
                                                                Auto_Adjust_Dates__c                //US2396: Added Auto Adjust Field
                                                         FROM Multi_Office_Split__c
                                                         WHERE Opportunity__c = :opportunityId
                                                         ORDER BY CreatedDate];
        List<MultiOffice> multiOffices = new List<MultiOffice>();
        for(Multi_Office_Split__c split : multiOfficeSplits){
            multiOffices.add(new MultiOffice(split));
        }
        return multiOffices;
    }

   /*
    * Upates/inserts record of MutiOffice split for Oppotunity
    * US2396: Added Auto Adjust field in the save method
    */
    public Multi_Office_Split__c saveMultiOffice(String opportunityJSON, String multiOfficeJSON, Boolean shouldRecalculateSpread){
        validateObjectUpdateable(Multi_Office_Split__c.sObjectType.getDescribe().getName());
        //DE177: Commented out to allow restricted users to create/edit Multi-Office Records
        //validateObjectUpdateable(Opportunity.sObjectType.getDescribe().getName());  
        MultiOffice multiO = (MultiOffice) JSON.deserialize(multiOfficeJSON, MultiOffice.class);
        OpportunityData opportunityRecord = (OpportunityData) JSON.deserialize(opportunityJSON, OpportunityData.class);
        Multi_Office_Split__c multiOfficeSplit;
        if(multiO.id != null){
            multiOfficeSplit = [
                    SELECT
                            Spreading_Formula__c
                    FROM Multi_Office_Split__c
                    WHERE Id = :multiO.id
            ];
        } else {
            multiOfficeSplit = new Multi_Office_Split__c(Opportunity__c = opportunityRecord.id);
        }

        Opportunity opp = new Opportunity(
                Id = opportunityRecord.id,
                Target_Project_Start_Date__c = opportunityRecord.targetProjectStartDate
        );

        multiOfficeSplit.Performance_Unit_PU__c = multiO.performanceUnit;
        multiOfficeSplit.Scope_of_Services__c       = multiO.resourceType;
        multiOfficeSplit.Worley_Capability_Subsector__c = multiO.subsector;
        multiOfficeSplit.Revenue__c             = multiO.revenue;
        multiOfficeSplit.Gross_Margin__c        = multiO.gm;
        multiOfficeSplit.Hours__c               = multiO.hours;
        multiOfficeSplit.Start_Date__c          = multiO.startDate;
        multiOfficeSplit.Number_of_Months__c    = multiO.months;
        multiOfficeSplit.Lead__c                = multiO.lead != null ? multiO.lead : false;
        multiOfficeSplit.Auto_Adjust_Dates__c   = multiO.autoAdjust !=null ? multiO.autoAdjust : false;                            //US2396: Added Auto Adjust field

        if(multiOfficeSplit.Spreading_Formula__c == ForecastSpread_Service.MANUAL && shouldRecalculateSpread){
            multiOfficeSplit.Spreading_Formula__c = ForecastSpread_Service.LINEAR;
        }

        try{
            //update opp;
            upsert multiOfficeSplit;

        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
        return multiOfficeSplit;
    }

   /*
    *   Removes MultiOffice record
    */
    public void deleteMultiOffice(Id multiOfficeId){
        try{
            Database.delete(multiOfficeId);
        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
    }

   /*
    *   Data structure for Opportunity record
    */
    public class OpportunityData{
        @AuraEnabled
        public Id id {get; set;}

        @AuraEnabled
        public Date targetProjectStartDate {get; set;}
    }

   /*
    * Data structure for MultiOffice record
    * US2396: Added Lag and Auto Adjust fields
    */
    public class MultiOffice{

        @AuraEnabled
        public String performanceUnit {get; set;}
        
        @AuraEnabled
        public String performanceUnitName {get; set;}

        @AuraEnabled
        public String resourceType {get; set;}

        @AuraEnabled
        public String subsector {get; set;}
        
        @AuraEnabled
        public Decimal revenue {get; set;}

        @AuraEnabled
        public Decimal gm {get; set;}

        @AuraEnabled
        public Decimal avgGM {get; set;}

        @AuraEnabled
        public Decimal hours {get; set;}

        @AuraEnabled
        public Date startDate {get; set;}

        @AuraEnabled
        public Date endDate {get; set;}

        @AuraEnabled
        public Decimal months {get; set;}      

        @AuraEnabled
        public Boolean lead {get; set;}

        @AuraEnabled
        public Id id {get; set;}

        @AuraEnabled
        public String name {get; set;}
        
        @AuraEnabled
        public String lineOfBusiness {get; set;}
        
        @AuraEnabled
        public Boolean isWon {get; set;}

        //US2396: Added Lag field
        @AuraEnabled
        public String lag {get; set;}
        
        //US2396: Added Auto Adjust field
        @AuraEnabled
        public Boolean autoAdjust {get; set;}        

        public MultiOffice(Multi_Office_Split__c split){
            id                  = split.Id;
            name                = split.Name;
            lead                = split.Lead__c;
            performanceUnit     = split.Performance_Unit_PU__c;
            performanceUnitName = split.Performance_Unit_PU__r.Name;
            resourceType        = split.Scope_of_Services__c;
            subsector           = split.Worley_Capability_Subsector__c;
            revenue             = split.Revenue__c;
            gm                  = split.Gross_Margin__c;
            avgGM               = split.Average_Gross_Margin__c;
            hours               = split.Hours__c;
            startDate           = split.Start_Date__c;
            months              = split.Number_of_Months__c;
            endDate             = split.End_Date__c;
            lineOfBusiness      = split.Opportunity__r.Line_of_Business__c;
            isWon               = split.Opportunity__r.IsWon;
            lag                 = split.Lag__c;                                  //US2396: Added Lag Field
            autoAdjust          = split.Auto_Adjust_Dates__c;                    //US2396: Added Auto Adjust Field

        }
    }  
    
   /*
    *   Retrieves settings for Resource Types
    */
    public Map<String, Resource_Type_Setting__mdt> getResourceTypeSettings(){
        
        //US2398: added Revenue_Required__c, Margin_Must_Be_Less_than_Revenue__c
        List<Resource_Type_Setting__mdt> rtSettings = [
            SELECT Line_of_Business__c, Resource_Type__c, Hours_Required__c, Revenue_Required__c, Margin_Must_Be_Less_than_Revenue__c 
            FROM Resource_Type_Setting__mdt
        ];
        Map<String, Resource_Type_Setting__mdt> resourceTypeSettings = new Map<String, Resource_Type_Setting__mdt>();
        
        for(Resource_Type_Setting__mdt rts : rtSettings){
            resourceTypeSettings.put(rts.Line_of_Business__c + rts.Resource_Type__c, rts);
        }

        return resourceTypeSettings;
    }  
    
   /*
    *   Retrieves opportunity (Added for DE270)
    */
    public Opportunity getOpportunity(Id recordId){
        Opportunity opportunity = [
            SELECT Line_of_Business__c, IsWon 
            FROM Opportunity WHERE Id = :recordId LIMIT 1
        ];
        
        return opportunity;
    }
}