/**************************************************************************************
Name: Review_TriggerHandler
Version: 1.0 
Created Date: 10.06.2018
Function: Trigger Handler for the Review Object Trigger

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Blake Poutra   	10/06/2018      Original Version
* Manuel Johnson	10/09/2018		US2578 Copy values when Review is cloned
*************************************************************************************/
public class Review_TriggerHandler {
    
    private static Review_TriggerHandler handler;
    
    //Singleton like pattern
    public static Review_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Review_TriggerHandler();
        }
        return handler;
    }
    
    //Actions performed on Review records on before insert
    public void onBeforeInsert(List<Review__c> reviews) {
        
        /* Removed due to known defect - W-4328595
* If it is a clone, set the Risk Assessment fields that are hidden
List<Review__c> clonedReviews = new List<Review__c>();

for(Review__c r : reviews) {
if(r.isClone()){
clonedReviews.add(r);
}
}
setHiddenFields(clonedReviews);*/
        
        //Calculate the risk score and set the Risk Assessment field
        calculateRiskScore(reviews);
    }
    
    //Actions performed on Review records on before update
    public void onAfterUpdate(List<Review__c> reviews, Map<Id, Review__c> oldReviewByIds) {
        for(Review__c r : reviews) {
            Boolean flagged = false;
            // Checking for flags on the Review
            if((r.Flagged_Execution_Questions__c != '' && r.Flagged_Execution_Questions__c != null) 
               || (r.Flagged_Client_Questions__c != '' && r.Flagged_Client_Questions__c != null) 
               || (r.Flagged_Geography_Questions__c != '' && r.Flagged_Geography_Questions__c != null) 
               || (r.Flagged_Commercial_Questions__c != '' && r.Flagged_Commercial_Questions__c != null) 
               || (r.Flagged_Contractual_Questions__c != '' && r.Flagged_Contractual_Questions__c != null) ){
                flagged = true;
            }
            if(oldReviewByIds.get(r.Id).Submit_for_approval__c == False && r.Submit_for_approval__c == True){
                if(!flagged){
                	submitForApproval(r);
                } else {
                    r.addError('This review record cannot be submitted for approval as questions in the risk assessment tool have been flagged by a user for review.  Those flags must be removed/resolved before submittal.');
                }
            }
        }
    }
    
    /*private void setHiddenFields(List<Review__c> reviews) {
Set<Id> sourceReviewIdSet = new Set<Id>();

for(Review__c r : reviews) {
sourceReviewIdSet.add(r.getCloneSourceId());
}

//Hidden fields
Map<Id, Review__c> sourceReviewMap = new Map<Id, Review__c>(
[SELECT Id, 
Contractual_Risk_Category__c, 
Risk_Categories_Selected__c,
Client_Risk_Score__c,
Geography_Risk_Score__c,
Commercial_Risk_Score__c,
Contractual_Risk_Score__c,
Execution_Risk_Score__c,
Opportunity_Risk_Score__c,
Execution_Risk_Category__c,
Geography_Risk_Category__c,
Opportunity_Risk_Category__c,
Client_Risk_Category__c,
Commercial_Risk_Category__c
FROM Review__c WHERE Id IN :sourceReviewIdSet]
);

for(Review__c r : reviews) {
r.Contractual_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Contractual_Risk_Category__c;
r.Risk_Categories_Selected__c = sourceReviewMap.get(r.getCloneSourceId()).Risk_Categories_Selected__c;
r.Client_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Client_Risk_Score__c;
r.Geography_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Geography_Risk_Score__c;
r.Commercial_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Commercial_Risk_Score__c;
r.Contractual_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Contractual_Risk_Score__c;
r.Execution_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Execution_Risk_Score__c;
r.Opportunity_Risk_Score__c = sourceReviewMap.get(r.getCloneSourceId()).Opportunity_Risk_Score__c;
r.Execution_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Execution_Risk_Category__c;
r.Geography_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Geography_Risk_Category__c;
r.Opportunity_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Opportunity_Risk_Category__c;
r.Client_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Client_Risk_Category__c;
r.Commercial_Risk_Category__c = sourceReviewMap.get(r.getCloneSourceId()).Commercial_Risk_Category__c;

if(Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(r.RecordTypeId).getName().contains('Proposal')) {
r.Recommendation__c = '';
}
}
}*/
    
    private void calculateRiskScore(List<Review__c> reviews) {
        
        // Check if the revenue is empty on these reviews.  If so, go get it.
        if(reviews[0].Revenue__c == null){
            Set<Id> oppIds = new Set<Id>();
            for(Review__c r : reviews){
                oppIds.add(r.Opportunity__c);
            }
            
            Map<Id, Decimal> oppRevenueMap = ReviewService.getOppRevenue(oppIds);
            
            for(Review__c r : reviews){
                r.Revenue__c = oppRevenueMap.get(r.Opportunity__c);
            }
            
        }
        
        for(Review__c r : reviews){
            Map<String, String> selectedCategories = new Map<String, String>();
            
            if(r.Risk_Categories_Selected__c == null){
                selectedCategories.put('execution', 'false');
                selectedCategories.put('client', 'false');
                selectedCategories.put('geography', 'false');
                selectedCategories.put('commercial', 'false');
                selectedCategories.put('contractual', 'false');
            } else {
                if(r.Risk_Categories_Selected__c.contains('execution')){
                    selectedCategories.put('execution', 'true');
                }
                if(r.Risk_Categories_Selected__c.contains('client')){
                    selectedCategories.put('client', 'true');
                }
                if(r.Risk_Categories_Selected__c.contains('geography')){
                    selectedCategories.put('geography', 'true');
                }
                if(r.Risk_Categories_Selected__c.contains('commercial')){
                    selectedCategories.put('commercial', 'true');
                }
                if(r.Risk_Categories_Selected__c.contains('contractual')){
                    selectedCategories.put('contractual', 'true');
                }
            }
            selectedCategories.put('opportunity', 'true');
            System.debug('trigger selectedCategories: ' + selectedCategories);
            RA_RiskAssessmentService.scoreWrapper score = RA_RiskAssessmentService.calcRiskScore(r, selectedCategories);
            r.Risk_Assessment__c = score.riskLevel + ' - ' + score.category;
            if(score.riskLevel != null){
            	r.Risk_Level__c = score.riskLevel.substring(1);
            }
            r.Risk_Category__c = score.category + ' Risk';
            
            if(r.Risk_Categories_Selected__c != null){
                for(String key : selectedCategories.keyset()){
                    System.debug('key: ' + key);
                    if(selectedCategories.get(key) == 'true'){
                        Map<String, String> newSelectedCategories = new Map<String, String>();
                        newSelectedCategories.put(key, 'true');
                        RA_RiskAssessmentService.scoreWrapper newScore = RA_RiskAssessmentService.calcRiskScore(r, newSelectedCategories);
                    
                        switch on key{
                            when 'execution' {
                                r.Execution_Risk_Score__c = newScore.riskLevel;
                                r.Execution_Risk_Category__c = newScore.category;
                            }
                            when 'client' {
                                r.Client_Risk_Score__c = newScore.riskLevel;
                                r.Client_Risk_Category__c = newScore.category;
                            } 
                            when 'geography' {
                                r.Geography_Risk_Score__c = newScore.riskLevel;
                                r.Geography_Risk_Category__c = newScore.category;
                            }
                            when 'commercial' {
                                r.Commercial_Risk_Score__c = newScore.riskLevel;
                                r.Commercial_Risk_Category__c = newScore.category;
                            }
                            when 'contractual' {
                                r.Contractual_Risk_Score__c = newScore.riskLevel;
                                r.Contractual_Risk_Category__c = newScore.category;
                            }
                        }
                    }
                }
                System.debug('r: ' + r);
            }
        }
    }
    
    @TestVisible
    private static void submitForApproval(Review__c review) {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments( review.Justification_Comments__c.stripHtmlTags() );
        req.setObjectId(review.Id);
        Approval.ProcessResult result = Approval.process(req);
    }
}