/**************************************************************************************
Name:RecordTypeSelection_C_Test
Version: 1.0
Created Date: 04/09/2018
Function: Unit test for RecordTypeSelection_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
*Pradeep Shetty         04/09/2018             Original Version
*************************************************************************************/
@isTest
private class RecordTypeSelection_C_Test {

  //Test user profile
  private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';

  //Test user username
  private static final String USERNAME                 = 'salesrtselection@testuser.com';  

  //String constant for Object name
  public static final String OBJECTAPI                 = 'Opportunity';

  //String Developer Names for Opportunity RecordTypes
  public static final String RECTYPE_NEW_OPPORTUINTY   = 'New_Opportunity';
  public static final String RECTYPE_BOOKING_VALUE     = 'Booking_Value';
	
  //Test data setup
  @testSetup
  static void setUpTestData(){

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

  }
	
  /*
  * Test method to test that all recordtypes are retrieved
  */
	@isTest static void test_RetrieveRecordTypesAll() {
    //Get the user      
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //Map of RecordType ID and corresponding object
    Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>([Select Name, 
                                                                        Id,
                                                                        Description,
                                                                        DeveloperName 
                                                                 From RecordType 
                                                                 Where SobjectType = :OBJECTAPI]);

    //List of recordtypes obtained from method call
    List<RecordType> recordTypeList;

    Test.startTest();

    System.runAs(salesUser){

      //Call the method to retrieve recordtypes for Opportunity
      recordTypeList = RecordTypeSelection_C.retrieveRecordTypes(OBJECTAPI, '', 'ASC');

    }

    Test.stopTest();

    //ASSERTIONS
    //Check that the recordTypeList is not empty
    System.assert(!recordTypeList.isEmpty());

    //Check that the number of records is identical
    System.assertEquals(recordTypeList.size(), recordTypeMap.size());

    //Check that the rows are identical
    for(RecordType rt: recordTypeList){
      System.assert(recordTypeMap.containsKey(rt.Id));
    }
  }

  /*
  * Test method to test that specified recordtypes are retrieved
  */  
  @isTest static void test_RetrieveRecordTypesSome() {
    //Get the user      
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //Map of RecordType ID and corresponding object
    Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>([Select Name, 
                                                                        Id,
                                                                        Description,
                                                                        DeveloperName 
                                                                 From RecordType 
                                                                 Where SobjectType = :OBJECTAPI 
                                                                 And DeveloperName in (:RECTYPE_BOOKING_VALUE, :RECTYPE_NEW_OPPORTUINTY)]);

    //List of recordtypes obtained from method call
    List<RecordType> recordTypeList;

    Test.startTest();

    System.runAs(salesUser){

      //Call the method to retrieve recordtypes for Opportunity
      recordTypeList = RecordTypeSelection_C.retrieveRecordTypes(OBJECTAPI, RECTYPE_BOOKING_VALUE + ',' + RECTYPE_NEW_OPPORTUINTY, 'DESC');

    }

    Test.stopTest();

    //ASSERTIONS
    //Check that the recordTypeList is not empty
    System.assert(!recordTypeList.isEmpty());

    //Check that the recordTypeList is not empty
    System.assertEquals(2,recordTypeList.size());    

    //Check that the number of records is identical
    System.assertEquals(recordTypeList.size(), recordTypeMap.size());

    //Check that the rows are identical
    for(RecordType rt: recordTypeList){
      System.assert(recordTypeMap.containsKey(rt.Id));
    }    

	}

  /*
  * Test method to test that objct API name is retrieved
  */
  @isTest static void test_getObjectName() {
    //Get the user      
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //List of recordtypes obtained from method call
    String objectName;

    Test.startTest();

    System.runAs(salesUser){

      //Call the method to retrieve recordtypes for Opportunity
      objectName = RecordTypeSelection_C.getObjectName(OBJECTAPI);

    }

    Test.stopTest();

    //ASSERTIONS
    //Check that the recordTypeList is not empty
    System.assert(String.isNotBlank(objectName));

    //Check that the recordTypeList is not empty
    System.assertEquals('Opportunity',objectName);    

  }  

  @isTest static void test_getObjectNameNull() {
    //Get the user      
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    //List of recordtypes obtained from method call
    String objectName;

    Test.startTest();

    System.runAs(salesUser){

      //Call the method to retrieve recordtypes for Opportunity
      objectName = RecordTypeSelection_C.getObjectName('testclassdummyobject');

    }

    Test.stopTest();

    //ASSERTIONS
    //Check that the recordTypeList is not empty
    System.assert(String.isBlank(objectName));

    //Check that the recordTypeList is not empty
    System.assertEquals(null,objectName);    

  }    
	
}