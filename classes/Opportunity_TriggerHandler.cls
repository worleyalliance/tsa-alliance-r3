/**************************************************************************************
Name: Opportunity_TriggerHandler
Version: 1.0
Created Date: 08/04/2017
Function: Handler for Opportunity_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    08/04/2017      Original Version
* Madhu Shetty      09/13/2017      DE223-Added condition to updateLOBandBUpicklistWhenSUChanged to check if Selling Unit is not blank
* Manuel Johnson    09/27/2017      US746 - Set base currency based on Selling Unit
* Manuel Johnson    11/21/2017      US1303 - Moved non bulkified process/flow automations into trigger
* Pradeep Shetty    03/14/2018      US1641 - Added method to enrich booking value opportunities
* Pradeep Shetty    06/28/2019      US2213 - Add Primary location to Booking Values
* Madhu Shetty      08/20/2018      US2369 - Update payment term for Opportunity record with the payment term of parent opportunity record
* Chris Cornejo     08/23/2018      US2362 - Oppty Scope of Services translation to Oracle Service Type field
* Madhu Shetty      09/07/2018      US2369 - Moved code for setting payment terms to Before triggers to fix SOQL 101 errors.
* Chris Cornejo     09/27/2018      DE739: Regression 10/05 - US2327: Program and Task Order fields not populated/updated on BV creation
* Scott W Stone     11/15/2018      DE752: Don't copy Task Order or Auto Create Project/Program from parent opportunity to booking value.  
                                           If the Program is being removed from the parent oppty, set Auto Create Project/Program on Booking Values to "NO"
* Madhu Shetty      12/15/2018      US2537: Plan week changes were deleted based on the steps mentioned in the user story.
* Scott Stone       1/6/2019        US2698: When a Project is assigned to a Closed Won opportunity as 
                                            the Program or Task Order, assign it to all Full Opportunity Children.
*************************************************************************************/
public with sharing class Opportunity_TriggerHandler {
    
    private static Opportunity_TriggerHandler handler;
    
    private final static String SKIP_VALUE = '';

    private final static String NEW_BOOKING_VALUE_RECORDTYPE = 'New_Booking_Value';
    private final static String BOOKING_VALUE_RECORDTYPE     = 'Booking_Value';
    private final static String CLOSED_WON_STAGENAME         = 'Closed - Won';

    private final static Id bvRecordTypeId = [Select Id
                                              From RecordType 
                                              where SObjectType ='Opportunity' 
                                              and DeveloperName = :BOOKING_VALUE_RECORDTYPE].Id;

    //US2537: Constant for ATN LOB
    private final static String LOB_ATN = 'ATN';

    //US2537:Set containing Sales Plan Keys for fetching Sales Plans
    private Set<String> salesPlanKeys = new Set<String>(); 
  
    //Singleton like pattern
    public static Opportunity_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Opportunity_TriggerHandler();
        }
        return handler;
    }
    
    //Actions performed on Opportunity records on before insert
    public void onBeforeInsert(List<Opportunity> opps) {

        //US2537: Clean up all collections
        salesPlanKeys.clear();

        //Update Booking Values from Parent Opps
        enrichBookingValueOpps(opps);

        //Determine LOB and BU for Opp based on Selling Unit
        updateLOBandBUpicklist(updateWaveSellingUnit(opps));

        //Determine fiscal year based on CloseDate
        updateFiscalYear(opps);

        //Set initial stage for Opps.
        //US2537: Add sales plan keys to the set. We are leveraging the same method to reduce loops.
        //This method should always be after the LOB, BU and Fiscal Year calculation
        setInitialStage(opps);     

        //DEBUG
        System.debug('SALES PLAN KEYS' + salesPlanKeys);   

        //US2537: Call method to assign Sales Plan to Opportunities
        assignSalesPlan(opps);

        //US2362: Assign Service Type
        setServiceType(opps);

        //Update payment term for Opportunity record with the payment term of parent opportunity record
        setPaymentTermsForParentOpportunity(opps);
    }
    
    //Actions performed on Opportunity records on before update
    public void onBeforeUpdate(List<Opportunity> opps, Map<Id, Opportunity> oldOppsByIds) {
        //US2537: Clean up all collections
        salesPlanKeys.clear();

        //If the SU has changed update the BU/LOB and if the Opp Close Date has changed update the FY
        List<Opportunity> oppsSUChanged = new List<Opportunity>();
        List<Opportunity> oppsCloseDateChanged = new List<Opportunity>();
        List<Opportunity> oppsSUorFYChanged = new List<Opportunity>();
        List<Opportunity> oppsScopeOfServicesChanged = new List<Opportunity>(); //US2362 - updates on Scope of Services
        List<Opportunity> OppWithNewParentOpp = new List<Opportunity>(); //Opps whose parent opp has changed
        
        for(Opportunity opp : opps){
            Opportunity oldOpp = oldOppsByIds.get(opp.Id);
            
            if(opp.Selling_Unit__c != null && opp.Selling_Unit__c != oldOppsByIds.get(opp.Id).Selling_Unit__c){
                oppsSUChanged.add(opp);
                oppsSUorFYChanged.add(opp);

                //US2537: Populate sales plan key
                salesPlanKeys.add(opp.Sales_Plan_Key__c);
            }
            
            if(opp.CloseDate != oldOppsByIds.get(opp.Id).CloseDate){
                oppsCloseDateChanged.add(opp);
                oppsSUorFYChanged.add(opp);
                
                //US2537: Populate sales plan key
                salesPlanKeys.add(opp.Sales_Plan_Key__c);

            }

            //US2362
            if (opp.Scope_of_Services__c != oldOppsByIds.get(opp.Id).Scope_of_Services__c){
                oppsScopeOfServicesChanged.add(opp);
            }
            
            //List Opportunity records whose parent opportunity has been updated
            if(opp.Parent_Opportunity__c != null && opp.Parent_Opportunity__c != oldOpp.Parent_Opportunity__c ){
                OppWithNewParentOpp.add(opp);
            }
        }
        
        if(oppsSUChanged.isEmpty() == false){
            updateLOBandBUpicklist(updateWaveSellingUnit(oppsSUChanged));
        }
        
        if(oppsCloseDateChanged.isEmpty() == false){
            updateFiscalYear(oppsCloseDateChanged);
        }
        
        if(oppsSUorFYChanged.isEmpty() == false){
            //US2537: Loop through these Opps to populate the 
            for(Opportunity currentOpp: oppsSUorFYChanged){
                if(currentOpp.Line_Of_Business__c == LOB_ATN){
                    salesPlanKeys.add(currentOpp.Business_Unit__c + currentOpp.Fiscal_Year__c);
                }
                else{
                    salesPlanKeys.add(currentOpp.Selling_Unit__c + currentOpp.Fiscal_Year__c);
                }
            }
            assignSalesPlan(oppsSUorFYChanged);
        }
        //US2362
        if(oppsScopeOfServicesChanged.isEmpty() == false){
            setServiceType(oppsScopeOfServicesChanged);
        }
        
        //Update payment term for Opportunity record with the payment term of parent opportunity record
        if(OppWithNewParentOpp != null && !OppWithNewParentOpp.isEmpty())
            setPaymentTermsForParentOpportunity(OppWithNewParentOpp);
    } 
    
    //Actions performed on Opportunity records on after insert
    public void onAfterInsert(List<Opportunity> opps) {
        System.debug(Logginglevel.ERROR,'afterinsert');
        

        //Update Opportunity access for Opportunity Team Members
        updateDefaultTeam(opps);

        //Add Opportunity Team Members for Booking Value Opps
        createBookingValueRelatedList(opps);
    }
    
    //Actions performed on Opportunity records on after update
    public void onAfterUpdate(List<Opportunity> opps, Map<Id, Opportunity> oldOppsByIds) {
        
        //Opps that have child BV opps
        List<Opportunity> bookingValueParentOppList = new List<Opportunity>();
        
        //Identify parent opps that have changed values
        for(Opportunity opp : opps){
            
            Opportunity oldOpp = oldOppsByIds.get(opp.Id);
            
            //US1641: Identify Opps that act as parents on BV Opps
            if( opp.RecordTypeId                   != bvRecordtypeId && 
               (opp.AccountId                     != oldOpp.AccountId                     || 
                opp.End_Client__c                 != oldOpp.End_Client__c                 ||
                opp.ContractId                    != oldOpp.ContractId                    || 
                opp.Selling_Unit__c               != oldOpp.Selling_Unit__c               || 
                opp.Executing_Business_Unit__c    != oldOpp.Executing_Business_Unit__c    || 
                opp.Executing_Line_of_Business__c != oldOpp.Executing_Line_of_Business__c || 
                opp.OCI_Case_Status__c            != oldOpp.OCI_Case_Status__c            || 
                opp.Program_New__c                != oldOpp.Program_New__c   || //DE739 - Replace Program__c to Program_New__c
                //US2698 - update children when task order is populated.
                opp.Task_Order_New__c             != oldOpp.Task_Order_New__c ) ) //DE739 - Replace Task_Order__c to Task_Order_New__c
            {
                bookingValueParentOppList.add(opp);
            }
            
        }
        
        //Update Booking Values based on parent opp changes
        if(bookingValueParentOppList!=null &&  !bookingValueParentOppList.isEmpty())
            updateChildren(bookingValueParentOppList);
    }

    //Update payment term for Opportunity record with the payment term of parent opportunity record
    public void setPaymentTermsForParentOpportunity(List<Opportunity> OppWithNewParentOpp){
        list<Id> lstoppParentIDs = new list<Id>();
        //get parent id of updated opportunities
        for(Opportunity oppnew : OppWithNewParentOpp){
            if(oppnew.Parent_Opportunity__c != null)
                lstoppParentIDs.add(oppnew.Parent_Opportunity__c);
        }
        
        //get payment terms for all parent opportunities
        map<id,Opportunity> oppParentOpp = new map<id,Opportunity>([select id, Payment_Terms__c from Opportunity where id in: lstoppParentIDs]);
        
        for(Opportunity oppnew : OppWithNewParentOpp){
            //Condition to check if opportunity has an assigned parent Opportunity and that the parent Opportunity has a payment term
            if(oppnew.Payment_Terms__c == null && 
               oppnew.Parent_Opportunity__c != null && oppParentOpp.get(oppnew.Parent_Opportunity__c).Payment_Terms__c != null){
                oppnew.Payment_Terms__c = oppParentOpp.get(oppnew.Parent_Opportunity__c).Payment_Terms__c;
            }
        }
    }
    //Set Initial Stage equal to the Opportunity Stage
    public void setInitialStage(List<Opportunity> opps){
        
        for(Opportunity currentOpp : opps){
            //Set Initial Stage
            currentOpp.Initial_Stage__c = currentOpp.StageName;

            //Add to SalesPlanKeys
            if(currentOpp.Line_of_Business__c == LOB_ATN){
                salesPlanKeys.add(currentOpp.Business_Unit__c + currentOpp.Fiscal_Year__c);                
            }
            else{
                salesPlanKeys.add(currentOpp.Selling_Unit__c + currentOpp.Fiscal_Year__c);                
            }
        }
    }
    
    //Copy Selling Unit to Wave Selling Unit
    public List<Opportunity> updateWaveSellingUnit(List<Opportunity> opps){
        
        for(Opportunity currentOpp : opps){
            //Copy Selling Unit to Wave Selling Unit
            currentOpp.Wave_Selling_Unit__c = currentOpp.Selling_Unit__c;
        }
        
        return opps;
    }
    
    //Method to determine the BU, LOB and Base Currency values for a given Selling Unit based on the field dependency in Opportunity
    private void updateLOBandBUpicklist(List<Opportunity> Opps){
        FieldDescribeUtil.PicklistDependency suBUDependency =
            FieldDescribeUtil.getDependentOptionsImpl(
                Opportunity.Wave_Selling_Unit__c,
                Opportunity.Business_Unit__c,
                new Set<String>{SKIP_VALUE}
            );
        FieldDescribeUtil.PicklistDependency buLOBDependency =
            FieldDescribeUtil.getDependentOptionsImpl(
                Opportunity.Business_Unit__c,
                Opportunity.Line_of_Business__c,
                new Set<String>{SKIP_VALUE}
            );
        FieldDescribeUtil.PicklistDependency baseCurrencyDependency =
            FieldDescribeUtil.getDependentOptionsImpl(
                Opportunity.Base_Currency_for_FX_Conversion_to_USD__c,
                Opportunity.Selling_Unit__c,
                new Set<String>{SKIP_VALUE}
            );
        for(Opportunity currentOpp : Opps){
            if(String.isNotBlank(currentOpp.Selling_Unit__c)){
               // currentOpp.Business_Unit__c = suBUDependency.controlingByDependent.get(currentOpp.Selling_Unit__c);
               // currentOpp.Line_Of_Business__c = buLOBDependency.controlingByDependent.get(currentOpp.Business_Unit__c);
               // currentOpp.Base_Currency_for_FX_Conversion_to_USD__c = baseCurrencyDependency.dependentByControling.get(currentOpp.Selling_Unit__c)[0];
            }
        }
    }
    
    //Method to update the Fiscal Year and Week based on the Close Date
    private void updateFiscalYear(List<Opportunity> opps){
        String fiscalYear;
        
        //Loop through Fiscal Years
        for(FiscalYearSettings fy : [SELECT Id, Name, Startdate, Enddate 
                                                FROM FiscalYearSettings ORDER BY enddate DESC LIMIT 100]){
            //The fiscal year format is FY##
            fiscalYear = 'FY' + fy.Name.right(2);
            
            //Set the fiscal year and week where the CloseDate is between the Start and End Dates
            for(Opportunity opp : opps){
                if(opp.CloseDate >= fy.StartDate && opp.CloseDate <= fy.EndDate){
                    opp.Fiscal_Year__c = fiscalYear;
                    opp.num_fiscal_week_close_date__c = math.MIN(53,math.MAX(1,math.CEIL((fy.StartDate.daysBetween(opp.CloseDate)+1)/7.0)));
                }
            }
        }
    }
    
    //On inserting an Opportunity, update default team members that have Read/Write to All(Owner) access
    public void updateDefaultTeam(List<Opportunity> opps){
        System.debug(Logginglevel.ERROR,'updateDefaultTeam');
        List<OpportunityTeamMember> defaultTeam = new List<OpportunityTeamMember>();
        for(OpportunityTeamMember tm : [SELECT Id, OpportunityAccessLevel FROM OpportunityTeamMember WHERE OpportunityId IN : opps]){
            
            if(tm.OpportunityAccessLevel == 'Edit'){
                tm.OpportunityAccessLevel = 'All';
                defaultTeam.add(tm);
            }
        }
        update defaultTeam;
    }
    
    //Enrich Booking Value opportunities with values from Parent Opportunity
    public void enrichBookingValueOpps(List<Opportunity> opps){

      //List for parent Opportunity Id
      List<Id> parentOppList = new List<Id>();

      //Loop through Opps, filter out non booking value Opps and get the list of Parent Opps
      for(Opportunity currentOpp: opps){
        if(currentOpp.RecordTypeId == bvRecordtypeId && 
          String.isNotBlank(String.valueOf(currentOpp.Parent_Opportunity__c))){
          parentOppList.add(currentOpp.Parent_Opportunity__c);
        }
      } //END FOR

      if(parentOppList!=null && !parentOppList.isEmpty()){
        //Prepare a map of parent Opp ID and corresponding record
        Map<Id, Opportunity> parentOppMap = new Map<Id, Opportunity>([SELECT Name,
                                                                             AccountId,
                                                                             Amount,
                                                                             Auto_Create_Contract_Program__c,
                                                                             Business_Unit__c,
                                                                             CloseDate,
                                                                             Contract_Award_Type__c,
                                                                             ContractId, 
                                                                             End_Client__c,
                                                                             Executing_Business_Unit__c,
                                                                             Executing_Line_of_Business__c,
                                                                             Factored_Workhours__c, 
                                                                             Forecast_Category__c,
                                                                             Jacobs_Industry_Group__c,
                                                                             Lead_Performance_Unit_PU__c,
                                                                             CH2M_Cost_Center__c,
                                                                             CH2M_Org_ID__c,
                                                                             Line_of_Business__c,
                                                                             Markets__c,
                                                                             OCI_Case_Status__c,
                                                                             Opportunity_Class_A_T__c,
                                                                             Opportunity_Structure__c,
                                                                             Opportunity_Tier__c,
                                                                             OwnerId,
                                                                             Parent_Opportunity__c,
                                                                             Primary_Street__c,          //US2213
                                                                             Primary_City__c,            //US2213
                                                                             Primary_State_Province__c,  //US2213
                                                                             Primary_Country__c,         //US2213
                                                                             Primary_Zip_Postal_Code__c, //US2213                                                                             
                                                                             Program_New__c, //DE739 - Replace Program__c to Program_New__c
                                                                             Program_Renewal_Rebid__c,
                                                                             Project_Categories__c,
                                                                             Project_Role__c,
                                                                             Pipeline_Factored_Revenue__c,
                                                                             Revenue__c,
                                                                             SEC_Codes_and_Categories__c,
                                                                             Selling_Unit__c,
                                                                             StageName, 
                                                                             Task_Order_New__c, //DE739 - Replace Task_Order__c to Task_Order_New__c
                                                                             Total_Probable_GM__c, 
                                                                             Work_Hours__c
                                                                     FROM Opportunity
                                                                     WHERE Id in :parentOppList]);

        //Loop through Booking Value Opps and set fields that are inherited from parent
        for(Opportunity currentOpp: opps){
          if(currentOpp.RecordTypeId == bvRecordtypeId && 
            String.isNotBlank(String.valueOf(currentOpp.Parent_Opportunity__c))){

            currentOpp.AccountId                       = parentOppMap.get(currentOpp.Parent_Opportunity__c).AccountId;
            //DE752 - don't copy Auto Create Project/Program from parent oppty to Booking Value
            //currentOpp.Auto_Create_Contract_Program__c = parentOppMap.get(currentOpp.Parent_Opportunity__c).Auto_Create_Contract_Program__c;
            currentOpp.Contract_Award_Type__c          = parentOppMap.get(currentOpp.Parent_Opportunity__c).Contract_Award_Type__c;
            currentOpp.ContractId                      = parentOppMap.get(currentOpp.Parent_Opportunity__c).ContractId;
            currentOpp.End_Client__c                   = parentOppMap.get(currentOpp.Parent_Opportunity__c).End_Client__c;
            //currentOpp.Executing_Business_Unit__c      = parentOppMap.get(currentOpp.Parent_Opportunity__c).Executing_Business_Unit__c;
            //currentOpp.Executing_Line_of_Business__c   = parentOppMap.get(currentOpp.Parent_Opportunity__c).Executing_Line_of_Business__c;
            currentOpp.Jacobs_Industry_Group__c        = parentOppMap.get(currentOpp.Parent_Opportunity__c).Jacobs_Industry_Group__c;
            //currentOpp.Lead_Performance_Unit_PU__c     = parentOppMap.get(currentOpp.Parent_Opportunity__c).Lead_Performance_Unit_PU__c;
            currentOpp.CH2M_Org_ID__c                  = parentOppMap.get(currentOpp.Parent_Opportunity__c).CH2M_Org_ID__c;
            currentOpp.CH2M_Cost_Center__c             = parentOppMap.get(currentOpp.Parent_Opportunity__c).CH2M_Cost_Center__c;
            currentOpp.Markets__c                      = parentOppMap.get(currentOpp.Parent_Opportunity__c).Markets__c;
            currentOpp.OCI_Case_Status__c              = parentOppMap.get(currentOpp.Parent_Opportunity__c).OCI_Case_Status__c;
            //currentOpp.Opportunity_Class_A_T__c        = parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Class_A_T__c;
            currentOpp.Opportunity_Structure__c        = parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Structure__c;
            currentOpp.Opportunity_Tier__c             = parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Tier__c;
            currentOpp.Program_New__c                  = parentOppMap.get(currentOpp.Parent_Opportunity__c).Program_New__c; //DE739 - Replace Program__c to Program_New__c
            //DE752: If program is blank, make sure all booking values have a Auto_Create_Contract_Program__c value of "No"
            if(currentOpp.Program_New__c == null){
                currentOpp.Auto_Create_Contract_Program__c = 'No';
            }
            currentOpp.Program_Renewal_Rebid__c        = parentOppMap.get(currentOpp.Parent_Opportunity__c).Program_Renewal_Rebid__c;            
            currentOpp.Project_Categories__c           = parentOppMap.get(currentOpp.Parent_Opportunity__c).Project_Categories__c;
            currentOpp.Project_Role__c                 = parentOppMap.get(currentOpp.Parent_Opportunity__c).Project_Role__c;
            currentOpp.SEC_Codes_and_Categories__c     = parentOppMap.get(currentOpp.Parent_Opportunity__c).SEC_Codes_and_Categories__c;
            //currentOpp.Selling_Unit__c                 = parentOppMap.get(currentOpp.Parent_Opportunity__c).Selling_Unit__c;
            //DE752 - don't copy task order from parent oppty to booking value
            //currentOpp.Task_Order_New__c               = parentOppMap.get(currentOpp.Parent_Opportunity__c).Task_Order_New__c; //DE739 - Replace Task_Order__c to Task_Order_New__c


            //US2213: Adding primary location values 
            currentOpp.Primary_Street__c               = parentOppMap.get(currentOpp.Parent_Opportunity__c).Primary_Street__c;
            currentOpp.Primary_City__c                 = parentOppMap.get(currentOpp.Parent_Opportunity__c).Primary_City__c;
            currentOpp.Primary_State_Province__c       = parentOppMap.get(currentOpp.Parent_Opportunity__c).Primary_State_Province__c;
            currentOpp.Primary_Country__c              = parentOppMap.get(currentOpp.Parent_Opportunity__c).Primary_Country__c;
            currentOpp.Primary_Zip_Postal_Code__c      = parentOppMap.get(currentOpp.Parent_Opportunity__c).Primary_Zip_Postal_Code__c;

            //Start: US2270 
            if(String.isNotBlank(currentOpp.Selling_Unit__c) && 
               currentOpp.Selling_Unit__c.startsWith('ATN')){
              if(String.isBlank(parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Class_A_T__c)){
                currentOpp.Opportunity_Class_A_T__c = 'Class III';
              }
              else{
                currentOpp.Opportunity_Class_A_T__c = parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Class_A_T__c;
              }
            }
            else{
              currentOpp.Opportunity_Class_A_T__c = parentOppMap.get(currentOpp.Parent_Opportunity__c).Opportunity_Class_A_T__c;
            }
            //End: US2270

          }
        } //END FOR
      }
    }

    /*
    * Creates Opportunity team members for booking value Opps
    * US2213 Creates primary opportunity location if one exists on an Opportunity
    */
    private void createBookingValueRelatedList(List<Opportunity> opps){

      //List for Parent Opps
      List<Id> parentOppList = new List<Id>();

      //Map of Parent Opp Id and its Oppty Team Members
      Map<Id, List<OpportunityTeamMember>> parentOpptyTeamMemberMap = new Map<Id, List<OpportunityTeamMember>>();

      //US2213 Map of Parent Opp Id and its primary Oppty Locations
      Map<Id, List<Opportunity_Locations__c>> parentOpptyOLMap = new Map<Id, List<Opportunity_Locations__c>>();       

      //List for inserting Oppty Team Members
      List<OpportunityTeamMember> newOTMList = new List<OpportunityTeamMember>();

      //US2213 List for inserting Oppty primary location
      List<Opportunity_Locations__c> newOLList = new List<Opportunity_Locations__c>();        

      //Get Parent Opps
      //Loop through Opps, filter out non booking value Opps and get the list of Parent Opps
      for(Opportunity currentOpp: opps){
        if(currentOpp.RecordTypeId == bvRecordtypeId && 
          String.isNotBlank(String.valueOf(currentOpp.Parent_Opportunity__c))){
          parentOppList.add(currentOpp.Parent_Opportunity__c);
        }
      } //END FOR

      //Prepare a map of Parent Opp Id and its Oppty team members
      for(OpportunityTeamMember otm: [Select OpportunityAccessLevel, 
                                             OpportunityId,
                                             TeamMemberRole,
                                             Title,
                                             UserId 
                                      From OpportunityTeamMember 
                                      Where OpportunityId in :parentOppList]){

        //Populate the map
        if(!parentOpptyTeamMemberMap.containsKey(otm.OpportunityId)){
          parentOpptyTeamMemberMap.put(otm.OpportunityId, new List<OpportunityTeamMember>{otm});
        }
        else{
          parentOpptyTeamMemberMap.get(otm.OpportunityId).add(otm);
        }

      }//END FOR

      //US2213: Prepare a map of Parent Opp Id and its Primary Oppty Location
      for(Opportunity_Locations__c ol: [Select Account_ID__c,
                                               Location__c,
                                               Opportunity__c
                                        From Opportunity_Locations__c
                                        Where Opportunity__c in :parentOppList 
                                        And Primary__c = true]){

        //Populate the map
        if(!parentOpptyOLMap.containsKey(ol.Opportunity__c)){
          parentOpptyOLMap.put(ol.Opportunity__c, new List<Opportunity_Locations__c>{ol});
        }
        else{
          parentOpptyOLMap.get(ol.Opportunity__c).add(ol);
        }

      }//END FOR


      //Loop through the Opps and create Opp Team Members and Primary opp Locations
     for(Opportunity currentOpp: opps){
        if(currentOpp.RecordTypeId == bvRecordtypeId && 
          String.isNotBlank(String.valueOf(currentOpp.Parent_Opportunity__c))){

          //Check that the map is not null
          if(parentOpptyTeamMemberMap!=null && !parentOpptyTeamMemberMap.isEmpty()){
            for(OpportunityTeamMember currentOTM : parentOpptyTeamMemberMap.get(currentOpp.Parent_Opportunity__c)){
              newOTMList.add(new OpportunityTeamMember(OpportunityAccessLevel = currentOTM.OpportunityAccessLevel,
                                                       OpportunityId = currentOpp.Id,
                                                       TeamMemberRole = currentOTM.TeamMemberRole,
                                                       UserId = currentOTM.UserId));                        
            } //END FOR
          }

          //US2213 Check that the map is not null
          if(parentOpptyOLMap!=null && !parentOpptyOLMap.isEmpty()){
            //US2213 Opportunity Locations
            for(Opportunity_Locations__c currentOL : parentOpptyOLMap.get(currentOpp.Parent_Opportunity__c)){
              newOLList.add(new Opportunity_Locations__c(Account_ID__c = currentOL.Account_ID__c,
                                                         Opportunity__c = currentOpp.Id,
                                                         Location__c = currentOL.Location__c,
                                                         Primary__c = true));   

            } //END FOR             
          }                    
        }
      } //END FOR


      //Insert new OTM
      insert newOTMList;

      //US2213 Insert new primary Opp Location
      insert newOLList;       
    }    

    //US1641: Method to update Booking Values based on Parent Opportunity changes
    //US2698 - update the program / task order on all non-booking value opportunities when it's updated on a Closed-Won parent.
    private void updateChildren(List<Opportunity> parentOppList){

      //List of children to be updated
      List<Opportunity> childrenToUpdateList = new List<Opportunity>();

      //Prepare a map of parent opp Id and it's field values
      Map<Id, Opportunity> parentOppMap = new Map<Id, Opportunity>([SELECT Id,
                                                                           AccountId,
                                                                           ContractId, 
                                                                           End_Client__c,
                                                                           OCI_Case_Status__c,
                                                                           StageName, //US2698 - include stage name in parent details so we can check it when updating project/task order on children
                                                                           Program_New__c, //DE739 - Replace Program__c to Program_New__c
                                                                           Task_Order_New__c //DE739 - Replace Task_Order__c to Task_Order_New__c
                                                                      FROM Opportunity
                                                                      WHERE Id in :parentOppList]);

      //Get a list of all children for the parent opps
      for(Opportunity currentChild : [SELECT Id,
                                          AccountId,
                                          ContractId, 
                                          End_Client__c,
                                          OCI_Case_Status__c,
                                          Parent_Opportunity__c,
                                          Program_New__c, //DE739 - Replace Program__c to Program_New__c
                                          Task_Order_New__c, //DE739 - Replace Task_Order__c to Task_Order_New__c
                                          RecordTypeId //US2698 - get record type id so we can handle booking values and everything else.
                                     FROM Opportunity
                                     WHERE Parent_Opportunity__c in :parentOppList 
                                     ])
      {
        //Get the Parent Opp details
        Opportunity parentOpp = parentOppMap.get(currentChild.Parent_Opportunity__c);

        if(currentChild.RecordTypeId == bvRecordtypeId){
            //Set Booking Values fields
            currentChild.AccountId                     = parentOpp.AccountId;
            currentChild.ContractId                    = parentOpp.ContractId;
            currentChild.End_Client__c                 = parentOpp.End_Client__c;
            currentChild.OCI_Case_Status__c            = parentOpp.OCI_Case_Status__c;
            currentChild.Program_New__c                = parentOpp.Program_New__c; //DE739 - Replace Program__c to Program_New__c
            //DE752: If program is blank, make sure all booking values have a Auto_Create_Contract_Program__c value of "No"
            if(parentOpp.Program_New__c == null){
                currentChild.Auto_Create_Contract_Program__c = 'No';
            }

            //DE752 - don't copy parent task order to child booking value.  only copy the program.
            //currentBV.Task_Order_New__c             = parentOpp.Task_Order_New__c; //DE739 - Replace Task_Order__c to Task_Order_New__c       
    
        } else if(parentOpp.StageName == CLOSED_WON_STAGENAME) {
            //US2698 - copy both the program and task order from the parent oppty to non-booking value children when the parent is closed won
            currentChild.Program_New__c                = parentOpp.Program_New__c;
            currentChild.Task_Order_New__c             = parentOpp.Task_Order_New__c;
        }
        
        childrenToUpdateList.add(currentChild);

      } //END FOR

      update childrenToUpdateList;


    }
    //US2362
    private void setServiceType(List<Opportunity> opps)
    {
    //Declare mapping for the Scope_of_Services_Dependency_Map__mdt custom metadata
    Map<String, String> codeToType = new Map<String, String>();   

    //Map all values for Scope of Services and Service Types
    for(Scope_of_Services_Dependency_Map__mdt record: [SELECT scope_of_services__c, service_type__c FROM Scope_of_Services_Dependency_Map__mdt]) {
          codeToType.put(record.scope_of_services__c, record.service_type__c);
    }

    //Assign Service Type for Opportunity where a match exist on Scope of Services
    for(Opportunity record: opps) {
          record.service_type__c = codeToType.get(record.scope_of_services__c);
    }
    
    } 

    //US2537: Method to assign Sales Plan
    private void assignSalesPlan(List<Opportunity> opps){
        //Map of sales plan key and Sales Plan Id
        Map<String, Id> salesPlanMap = new Map<String, Id>(); 

        //Populate the map
        for(Campaign salesPlan: [Select Id, 
                                        Sales_Plan_Key__c
                                 From Campaign 
                                 Where Sales_Plan_Key__c in :salesPlanKeys]){
            
            //Populate map
            if(!salesPlanMap.containsKey(salesPlan.Sales_Plan_Key__c)){
                salesPlanMap.put(salesPlan.Sales_Plan_Key__c, salesPlan.Id);
            }
        } //END FOR

        //DEBUG
        System.debug('SALES PLAN MAP: ' + salesPlanMap);
            
        String key;

        for(Opportunity opp: opps){
            system.debug('Fiscal Year: ' + opp.Fiscal_Year__c);
            system.debug('BU: ' + opp.Business_Unit__c);
            system.debug('SU: ' + opp.Selling_Unit__c);
            if(opp.Line_Of_Business__c== LOB_ATN){
                key = opp.Business_Unit__c + opp.Fiscal_Year__c;
            }
            else{
                key = opp.Selling_Unit__c + opp.Fiscal_Year__c;
            }
            //Assign the sales plan
            if(salesPlanMap!=null && salesPlanMap.containsKey(key)){
                opp.CampaignId = salesPlanMap.get(key);
            }
            else{
                opp.CampaignId = null;
            }

        }//END FOR
    }

    public class OpportunityTriggerException extends Exception {}
    
}