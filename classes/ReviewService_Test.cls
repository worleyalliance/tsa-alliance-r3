/**************************************************************************************
Name:ReviewService_Test
Version: 1.0 
Created Date: 09/04/2018
Function: Test class to test the ReviewSerice methods

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    09/04/2018       Original Version
*************************************************************************************/
@isTest
private class ReviewService_Test {
    
  	private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
  	private static final String USERNAME = 'salesBpRevision@testuser.com';
    
	//Test data setup
    @testSetup
    static void setUpTestData(){
        //Create test user
    	TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    	User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

        System.runAs(salesUser){
            
            //Create Opportunity
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity opp = oppBuilder.build().withGo(50).save().getOpportunity();
            
            //Create Review
            TestHelper.ReviewBuilder reviewBuilder = new TestHelper.ReviewBuilder();
            Review__c review = reviewBuilder.build().withOpportunity(opp.Id).recordType('Go/No-Go Review (BIAF)').save().getRecord();
            
    	}
    }
    
    /*Scenario 1: test the updateOpportunityBrief method
	* Update the Opportunity field and use the method to get the update the Review
	*/
    @isTest static void test_updateOpportunityBrief() {
        
        //Get the user
    	User salesUser = [SELECT Id FROM User WHERE UserName = :USERNAME LIMIT 1];
        
        //Get the parent Opportunity
        Opportunity opp = [SELECT Id, Go__c FROM Opportunity WHERE OwnerId = :salesUser.Id LIMIT 1];
        
        //Get Review record before updating the Opportunity
        Review__c reviewBefore = [SELECT Id, Go__c, Refresh_Opportunity_Brief__c 
                            FROM Review__c WHERE Opportunity__c = :opp.Id LIMIT 1];
        
        //Update the Opportunity Go%
        opp.Go__c = 75;
        update opp;
        
        test.startTest();
        
        //Execute tests
    	System.runAs(salesUser){
            reviewBefore.Refresh_Opportunity_Brief__c = TRUE;
            update reviewBefore;
        }
        
        test.stopTest();
        
        //Get the Review after updating the Opportunity
        Review__c reviewAfter = [SELECT Id, Go__c, Refresh_Opportunity_Brief__c 
                       FROM Review__c WHERE Opportunity__c = :opp.Id LIMIT 1];
        
        //Validate results
    	System.assertEquals(50, reviewBefore.Go__c);
    	System.assertEquals(75, reviewAfter.Go__c);
    	System.assertEquals(FALSE, reviewAfter.Refresh_Opportunity_Brief__c);
        
    }
}