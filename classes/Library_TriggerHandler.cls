/**************************************************************************************
Name: Library_TriggerHandler
Version: 1.0
Created Date: 09/04/2018
Function: Handler for Library_Trigger
Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    09/04/2018      Original Version
* Chris Cornejo    09/04/2018      DE667 - UAT - Library Records - Can't Edit LOB, BU and PU fields
*************************************************************************************/
public with sharing class Library_TriggerHandler {
    
    private static Library_TriggerHandler handler;
    
    private final static String SKIP_VALUE = '';
    
    //Singleton like pattern
    public static Library_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Library_TriggerHandler();
        }
        return handler;
    }
    
    //Actions performed on Library records on before insert
    public void onBeforeInsert(List<Library__c> libs) {
        
        setLOBBU(libs); //DE667
    }
    
    //Actions performed on Library records on before update
    public void onBeforeUpdate(List<Library__c> libs, Map<Id, Library__c> oldLibsByIds) {
       
        List<Library__c> libsLOBBUChanged = new List<Library__c>(); //DE667 - updates on LOB, BU
        
        for(Library__c lib : libs){
                        
            //DE667 - adds to list if PU has changed
            if (lib.Performance_Unit__c!= oldLibsByIds.get(lib.Id).Performance_Unit__c){
            
                libsLOBBUChanged.add(lib);
            }
        }
        
        //DE667 - checks if list if empty, if not go to method
        if(libsLOBBUChanged.isEmpty() == false){
            setLOBBU(libsLOBBUChanged);
        }
    }

    //DE667 - updates BU and LOB when PUs change
    private void setLOBBU(List<Library__c> libs)
    {
    
    Map<Id, Unit__c> unitNames = new Map<Id, Unit__c>([Select Id, Business_Unit__c, Line_of_Business__c From Unit__c]);
    
    for(Library__c lib : libs){
      if(String.isNotBlank(lib.Performance_Unit__c)){
        lib.Business_Unit__c = unitNames.get(lib.Performance_Unit__c).Business_Unit__c;
        lib.Line_of_Business__c = unitNames.get(lib.Performance_Unit__c).Line_of_Business__c;
      }
    }
    
    }

    public class LibraryTriggerException extends Exception {}
    
}