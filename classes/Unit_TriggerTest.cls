/**************************************************************************************
Name: Unit_TriggerTest
Version: 1.0 
Created Date: 01.03.2019
Function: Test class for Unit trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Walker      01.03.2019      Original Version
*************************************************************************************/
@IsTest
private class Unit_TriggerTest {
    
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME                  = 'datasteward@testuser.com'; 
    
    //Test data setup
    @testSetup
    static void setUpTestData(){

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

    System.runAs(salesUser){

      //Account required for Opportunity
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      Account acct = accountBuilder.build().save().getRecord();
      
      //Create an Opportunity
      TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
      Opportunity opp = oppBuilder.build().withAccount(acct.Id).save().getOpportunity();  
        
      //Create a Multi Office
      Decimal revenue = 20;
      TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
      Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withRevenue(revenue).withGM(10).withOpportunity(opp.Id).getRecord();
	
      //Create Library
	    /*TestHelper.LibraryBuilder libraryBuilder = new TestHelper.LibraryBuilder();
      Library__c lib = LibraryBuilder.build().getRecord();
      system.debug('library ' + lib.Performance_Unit__c + lib.Name);*/
      }
  }  
  
  /* Scenario 1: Test if the unit LOB and BU changes are reflected on the Opportunity
  */
  @isTest
    private static void test_opportunityUpdate(){
      
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];
        
    //Get the Opportunity created by the sales user
    Opportunity opp = [Select Id, Lead_Performance_Unit_PU__c from Opportunity where CreatedById = :salesUser.Id Limit 1];
        
    //Get the Unit assigned to the Opportunity
    Unit__c OppUnit = [Select Id from Unit__c where Id = :opp.Lead_Performance_Unit_PU__c Limit 1];

    //Create Library
    TestHelper.LibraryBuilder libraryBuilder = new TestHelper.LibraryBuilder();
    Library__c lib = LibraryBuilder.build().withUnitRecord(OppUnit.Id).save().getRecord();


     test.startTest();
    //Execute tests   
     System.runAs(salesUser){ 
         
      //Query for the Unit assigned to the Opportunity
      Unit__c newUnit = [Select Id, Line_of_Business__c, Business_Unit__c  
               From Unit__c 
               Where Id = :OppUnit.Id];
         
         //change LOB and BU
         newUnit.Line_of_Business__c = 'JESA';
         newUnit.Business_Unit__c = 'JESA - Joint Venture';
      

                
         //update Unit
         update newUnit;
             
     }


    test.stopTest(); 
     
    //Validate results
    System.assertEquals('JESA', [SELECT Executing_Line_of_Business__c FROM Opportunity WHERE id = :opp.id Limit 1].Executing_Line_of_Business__c);
    // System.assertEquals('JESA  - Joint Venture', [SELECT Executing_Business_Unit__c FROM Opportunity WHERE id = :opp.id Limit 1].Executing_Business_Unit__c);    
        
    }
    
  /* Scenario 2: Test if the unit LOB and BU changes are reflected on the Multi Office
  */
  @isTest
    private static void test_multiofficeUpdate(){
      
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];
        
    //Get the Multi Office created by the sales user    
    Multi_Office_Split__c mos = [Select Id, Performance_Unit_PU__c from Multi_Office_Split__c where CreatedById = :salesUser.Id Limit 1];
        
    //Get the Unit assigned to the Multi Office
    Unit__c MultiOfficeUnit = [Select Id from Unit__c where Id = :mos.Performance_Unit_PU__c Limit 1];
        
     test.startTest();
    //Execute tests   
     System.runAs(salesUser){ 
         
    //Query for the Unit assigned to the multi office
    Unit__c newUnit = [Select Id, Line_of_Business__c, Business_Unit__c  
               From Unit__c 
               Where Id = :MultiOfficeUnit.Id];
         
         //change LOB and BU
         newUnit.Line_of_Business__c = 'JESA ';
        //  newUnit.Business_Unit__c = 'JESA - Joint Venture';
         
         //update Unit
         update newUnit;
             
     }
    test.stopTest(); 
     
    //Validate results
    System.assertEquals('JESA', [SELECT Executing_LOB__c FROM Multi_Office_Split__c WHERE id = :mos.id Limit 1].Executing_LOB__c);
    // System.assertEquals('JESA  - Joint Venture' , [SELECT Executing_BU__c FROM Multi_Office_Split__c WHERE id = :mos.id Limit 1].Executing_BU__c);    
        
    }
    
    /* Scenario 3: Test if the unit LOB and BU changes are reflected on the Library
  */
 /* @isTest
    private static void test_libraryUpdate(){
      
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];
        
    //Get the Library created by the sales user    
    Library__c lib = [Select Id, Performance_Unit__c from Library__c where CreatedById = :salesUser.Id Limit 1];
        system.debug('library created by sales user ' + lib.Performance_Unit__c + lib.Id);
        
    //Get the Unit assigned to the Library
    Unit__c LibraryUnit = [Select Id from Unit__c where Id = :lib.Performance_Unit__c Limit 1];
        
     test.startTest();
    //Execute tests   
     System.runAs(salesUser){ 
         
    //Query for the Unit assigned to the multi office
    Unit__c newUnit = [Select Id, Line_of_Business__c, Business_Unit__c  
               From Unit__c 
               Where Id = :LibraryUnit.Id];
         
         //change LOB and BU
         newUnit.Line_of_Business__c = 'ECR';
         newUnit.Business_Unit__c = 'ECR - Asia';
         
         //update Unit
         update newUnit;
             
     }
    test.stopTest(); 
     
    //Validate results
    System.assertEquals('ECR', [SELECT Line_of_Business__c FROM Library__c WHERE id = :lib.id Limit 1].Line_of_Business__c);
    System.assertEquals('ECR - Asia', [SELECT Business_Unit__c FROM Library__c WHERE id = :lib.id Limit 1].Business_Unit__c);          
    } */

}