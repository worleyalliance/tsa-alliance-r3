/**************************************************************************************
Name: Utility
Version: 1.0 
Created Date: 03.08.2018
Function: Utility class provides methods for multiple classes

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    03.08.2018      Original Version
* Jignesh Suvarna   13.03.2018      Added checkAdminPermissions() and havRecordAccess() method
* Madhu Shetty        25.04.2018      Added AddChatterpost method to add chatterposts through code
* Jignesh Suvarna   29.05.2018        Added checkSysAdminPermissions() to check System Admin and Restricted admins permissions for DE498
* Pradeep Shetty    06.13.2018      Added constants to be used in other classes
* Pradeep Shetty    12.06.2018      US1855: Added logic to generate base36 code
*************************************************************************************/
public class Utility {

    /*CONSTANTS*/
    //BOOKING
    public static String BOOKING = 'Booking';
    //BOOKING VALUE
    public static String BOOKING_VALUE = 'Booking Value';    
    //FULL OPPORTUNITY
    public static String FULL_OPPORTUNITY = 'Full Opportunity'; 

    public static String EditClosedWonPerms= 'EditClosedWonOpty';
    //JS 29/05/2018 for DE498
    public static String Administrators= 'Administrators';

    //US1855: Constant for base36 mapping
    public static Map<Integer, String> base36Map = new Map<Integer,String>{0 =>'0',
                                                          1 =>'1',
                                                          2 =>'2',
                                                          3 =>'3',
                                                          4 =>'4',
                                                          5 =>'5',
                                                          6 =>'6',
                                                          7 =>'7',
                                                          8 =>'8',
                                                          9 =>'9',
                                                          10 =>'A',
                                                          11 =>'B',
                                                          12 =>'C',
                                                          13 =>'D',
                                                          14 =>'E',
                                                          15 =>'F',
                                                          16 =>'G',
                                                          17 =>'H',
                                                          18 =>'I',
                                                          19 =>'J',
                                                          20 =>'K',
                                                          21 =>'L',
                                                          22 =>'M',
                                                          23 =>'N',
                                                          24 =>'O',
                                                          25 =>'P',
                                                          26 =>'Q',
                                                          27 =>'R',
                                                          28 =>'S',
                                                          29 =>'T',
                                                          30 =>'U',
                                                          31 =>'V',
                                                          32 =>'W',
                                                          33 =>'X',
                                                          34 =>'Y',
                                                          35 =>'Z'};

    /*
  * Method to update uncheck the unique field for all other records
  */
  public static void uniqueFieldAcrossChildren(List<SObject> childRecords, String parentIdField, String uniqueField){
    List<SObject> uniqueRecordList = new List<SObject>();
    List<Id> parentIdList = new List<Id>();
    String sObjectName;
    
    // Get list of records where the unique field is set to true
    for (SObject cr : childRecords) {
      if (cr.get(uniqueField) == true) {
          parentIdList.add((Id)cr.get(parentIdField));
          uniqueRecordList.add(cr);
          sObjectName = cr.getSObjectType().getDescribe().getName();
      }
    }
    
    // Get list of existing records where the unique field is set to true
    if (!uniqueRecordList.isEmpty()) {
      List<SObject> existRecordList = Database.query('SELECT Id FROM ' + sObjectName + 
                                                     ' WHERE ' + parentIdField + ' IN :parentIdList AND ' + 
                                                     uniqueField + '= true AND Id NOT In :uniqueRecordList');
    
      // Set the unique field to false for all existing records
      if (!existRecordList.isEmpty()) {
          for (SObject orl : existRecordList){
              orl.put(uniqueField, false);
          }
          update existRecordList;  
      }
    }
  }

  /*
  * Method to get record types for an object
  */
  public static List<RecordType> getRecordTypes(String sObjectName, List<String> developerNameList){

    //Check if Object Name is provided
    if(String.isNotBlank(sObjectName)){
      //If Developer names are providede then get record types for those developer names else get all
      if(developerNameList!=null && !developerNameList.isEmpty()){
        return [Select Id, 
                       DeveloperName, 
                       Name 
                From RecordType 
                Where SObjectType = :sobjectName
                And DeveloperName in :developerNameList];
      }      
      else{
        return [Select Id, 
                       DeveloperName, 
                       Name 
                From RecordType 
                Where SObjectType = :sobjectName];

      }
    }
    else{
      //Object name not provided. Return null.
      return null;
    }
  }
    
    /*
    * Checks if the user has edit permission after the Opp is closed won
    */
    public static Boolean checkAdminPermissions(){
        Boolean hasEditPermission = false;
        
            // Check permissions of currently logged in user, 20180312_JS - Added as part of US1509
            hasEditPermission = FeatureManagement.checkPermission(EditClosedWonPerms);     
        
        return (hasEditPermission);
    }
    /*
    * Checks if the user has System Admin / Restricted Admin permissions
    * JS per DE498 - used in Opportunity Forecast__c
    */
    public static Boolean checkSysAdminPermissions(){
        Boolean hasSystemEditPermission = false;
        
            // Check permissions of currently logged in user
            hasSystemEditPermission = FeatureManagement.checkPermission(Administrators);     
        
        return (hasSystemEditPermission);
    }
    /*
    * Checks if User has object edit access, then check if the user has edit access on the field record
    */
    public static Boolean havRecordAccess(String objectType, String fieldName, Id recordId){
        
        Boolean havRecordAccess = false;
        UserRecordAccess accessToRecord = null;
          //Get the user access level for this record
        IF(recordId != null)
          accessToRecord = [Select RecordId, 
                            HasEditAccess 
                            From UserRecordAccess 
                            Where UserId = :UserInfo.getUserId()  
                            And RecordId = :recordId 
                            Limit 1];
         
          if(accessToRecord!=null && accessToRecord.HasEditAccess)
          {
            //If user has record edit access, then check if the user has edit access on the field
            //Get all fields for this object
           if(objectType != Null || fieldName !=Null){
               // For SingleField Component
                Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        
                if(fieldMap.containsKey(fieldName))
                {
                  havRecordAccess= fieldMap.get(fieldName).getDescribe().isUpdateable();
                }
                else{
                  havRecordAccess= false;
                } 
           }
           else{
                // for OpportunityEstimation & OpportunityForecast components
                havRecordAccess= accessToRecord.HasEditAccess;
           }
          }
        else 
        {
            havRecordAccess= false;
        }   
        return havRecordAccess;
    }
 
    //Code to add a chatter post.
    public static void AddChatterpost(id userId, id recordID, String message){
      //Code to add a chatter post, mentioning the user who verified the account.
      ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
      ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
      ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
      
      messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
      mentionSegmentInput.id = userId;
      messageBodyInput.messageSegments.add(mentionSegmentInput);
      
      textSegmentInput.text = message;
      messageBodyInput.messageSegments.add(textSegmentInput);

      ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
      
      feedItemInput.body = messageBodyInput;
      feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
      feedItemInput.subjectId = recordID;
      
      ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }

  //Method to check if the unit is active or not
    public static Boolean checkUnitState(Id unitValue){
      Unit__c unit = [Select Active__c from Unit__c where Id = :unitValue];
      if(unit!=null && unit.Active__c){
        return true;
      }
      else{
        return false;
      }

    }    

    //US1855:Method to generate base36 alphanumeric code
    public static String generateAlphaNumericCode(Integer num) {

        //Variable for the source number
        Integer sourceNumber;

        //Check if the number passed is not null
        if(num!=null){
            sourceNumber = num;
        }
        else{
            //If null is passed, consider the number to be 0
            sourceNumber = 0;
        }

        //List to capture the converted number. This list will be used to build alphanumeric code
        List<String> codeCharacterList = new List<String>();

        //Initialize the alphaNumericCode
        String alphaNumericCode = '';

        //Divide the number by 36. The remainder after division is used to figure out the code based on the map. 
        //The Quotient, if not 0, is then further divided by 36 and the process continues till the quotient is 0.
        While(num/36!=0){
            //Get the remainder and corresponding character
            
            system.debug('here is the number '+num);            
            codeCharacterList.add(base36Map.get(Math.mod(num, 36)));
            system.debug('here is the character output '+base36Map.get(Math.mod(num, 36)));
            
            //Get the quotient
            num = num/36;
        }

        //If num/36 is 0, then use the num to get the code from the map
        codeCharacterList.add(base36Map.get(num));

        //prepare the code by looping through the list
        //If list length is less than 6 then pad with 0s
        for(Integer i = 0; i < 6 - codeCharacterList.size(); i++){
            alphaNumericCode += '0';
        }

        //Get remaining characters from the list
        for(Integer j = codeCharacterList.size() - 1; j >=0; j--){
            alphaNumericCode += codeCharacterList[j];
        }

        //Return the code
        return alphaNumericCode;
    }
}