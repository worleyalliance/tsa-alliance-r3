/**************************************************************************************
Name: BP_Request_TriggerService
Version: 1.0 
Created Date: 10.05.2017
Function: Functionalities related to B_P_Request__c trigger. i.e. logic which calculates reuqest approvers

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           10.05.2017         Original Version
*************************************************************************************/
public with sharing class BP_Request_TriggerService {

    public static final String OPPORTUNITY_REQUEST_TYPE = 'Opportunity B&P';
    public static final String CLIENT_REQUEST_TYPE = 'Client B&P';
    public static final String SALES_REQUEST_TYPE = 'Sales PU B&P';

    public static final String CLOSED_APPROVED_STATUS = 'Approved';
    public static final String CLOSED_REJECTED_STATUS = 'Rejected';
    public static final String ACTIVE_STATUS = 'Active';

   /*
    *   Fileters open requests
    */

/*
    public List<B_P_Request__c> selectNotClosed(List<B_P_Request__c> bPRequests){
        List<B_P_Request__c> notClosedBPRequests = new List<B_P_Request__c>();
        for(B_P_Request__c request : bPRequests){
            if (request.Status__c != CLOSED_APPROVED_STATUS && request.Status__c != CLOSED_REJECTED_STATUS) {
                notClosedBPRequests.add(request);
            }
        }
        return notClosedBPRequests;
    }
*/

   /*
    *   Fileters open requests whee budged has changed
    */
/*    public List<B_P_Request__c> selectNotClosedAndBudgetChanged(List<B_P_Request__c> newBPRequests, Map<Id, B_P_Request__c> oldBPRequestsByIds){
        List<B_P_Request__c> notClosedBPRequests = new List<B_P_Request__c>();
        for(B_P_Request__c request : newBPRequests){
            if((request.Status__c != CLOSED_APPROVED_STATUS && request.Status__c != CLOSED_REJECTED_STATUS) &&
                    (request.BP_Budget__c != oldBPRequestsByIds.get(request.Id).BP_Budget__c)) {
                notClosedBPRequests.add(request);
            }
        }
        return notClosedBPRequests;
    }*/

   /*
    *   Wraps all processes of updating approver fields
    */
/*    public void updateApprovers(List<B_P_Request__c> notClosedBPRequests) {

        Map<Id, Double> amoutByRequestId = calculateRequestAmout(notClosedBPRequests);
        Map<Id, String> ownerSellingUnitByRequestId = findRequestOwnerSellingUnit(notClosedBPRequests);
        List<Selling_Unit_Approvers__c> sellingUnitApprovers = Selling_Unit_Approvers__c.getAll().values();
        Map<String, Selling_Unit_Approvers__c> approversBySellingUnit = new Map<String, Selling_Unit_Approvers__c>();
        for(Selling_Unit_Approvers__c sellingUnitApprover : sellingUnitApprovers){
            approversBySellingUnit.put(sellingUnitApprover.Selling_Unit__c, sellingUnitApprover);
        }

        Map<String, Id> userIdByUsername = getApproverIdByBaseUsername(approversBySellingUnit);

        List<B_P_Request__c> bPRequests = updateApproversBasedOnAmount(
                notClosedBPRequests,
                ownerSellingUnitByRequestId,
                approversBySellingUnit,
                amoutByRequestId,
                userIdByUsername
        );
        update bPRequests;
    }*/

   /*
    *   Calculates verified amout based on request type
    */
/*    private Map<Id, Double> calculateRequestAmout(List<B_P_Request__c> bPRequests){
        Map<Id, Double> amoutByRequestId = new Map<Id, Double>();
        for (B_P_Request__c request : bPRequests) {
            Decimal amount = 0;
            if (request.Request_Type__c == OPPORTUNITY_REQUEST_TYPE && request.Opportunity__c != null) {
                amount = request.Total_Requested_B_P__c + request.BP_Budget__c;
            } else if (request.Request_Type__c == CLIENT_REQUEST_TYPE && request.Client__c != null) {
                amount = request.Total_Requested_Account_B_P__c + request.BP_Budget__c;
            } else if (request.Request_Type__c == SALES_REQUEST_TYPE && request.Sales_PU__c != null) {
                amount = request.BP_Budget__c;
            }
            amoutByRequestId.put(request.Id, amount);
        }
        return amoutByRequestId;
    }*/

   /*
    *   Creates relation B_P_Request__c and owner selling unit
    *   Owner is determined base on request type
    */
/*    private Map<Id, String> findRequestOwnerSellingUnit(List<B_P_Request__c> bPRequests){
        Set<Id> ownerIds = new Set<Id>();
        Set<Id> clientIds = new Set<Id>();
        Set<Id> opportunityIds = new Set<Id>();

        for(B_P_Request__c request : bPRequests){
            if (request.Request_Type__c == OPPORTUNITY_REQUEST_TYPE && request.Opportunity__c != null) {
                opportunityIds.add(request.Opportunity__c);
            } else if (request.Request_Type__c == CLIENT_REQUEST_TYPE && request.Client__c != null) {
                clientIds.add(request.Client__c);
            } else if (request.Request_Type__c == SALES_REQUEST_TYPE && request.Sales_PU__c != null) {
                ownerIds.add(request.OwnerId);
            }
        }
        Map<Id, User> usersByIds = new Map<Id, User>([
                SELECT Selling_Unit__c
                FROM User
                WHERE Id IN :ownerIds
        ]);
        Map<Id, Opportunity> opportunitiesByIds = new Map<Id, Opportunity>([
                SELECT Owner.Selling_Unit__c
                FROM Opportunity
                WHERE Id IN :opportunityIds
        ]);
        Map<Id, Account> accountsByIds = new Map<Id, Account>([
                SELECT Owner.Selling_Unit__c
                FROM Account
                WHERE Id IN :clientIds
        ]);

        Map<Id, String> ownerSellingUnitByRequestId = new Map<Id, String>();
        for (B_P_Request__c request : bPRequests) {
            if (request.Request_Type__c == OPPORTUNITY_REQUEST_TYPE && request.Opportunity__c != null) {
                ownerSellingUnitByRequestId.put(request.Id, opportunitiesByIds.get(request.Opportunity__c).Owner.Selling_Unit__c);
            } else if (request.Request_Type__c == CLIENT_REQUEST_TYPE && request.Client__c != null) {
                ownerSellingUnitByRequestId.put(request.Id, accountsByIds.get(request.Client__c).Owner.Selling_Unit__c);
            } else if (request.Request_Type__c == SALES_REQUEST_TYPE && request.Sales_PU__c != null) {
                ownerSellingUnitByRequestId.put(request.Id, usersByIds.get(request.OwnerId).Selling_Unit__c);
            }
        }
        return ownerSellingUnitByRequestId;
    }*/

   /*
    *  Generates map of user id by it's base username, skipping sandbox suffixes
    */
/*    private Map<String, Id> getApproverIdByBaseUsername(Map<String, Selling_Unit_Approvers__c> approversBySellingUnit){
        Set<String> approverUsernamesSet = new Set<String>();
        for(Selling_Unit_Approvers__c sellingUnitApprover : approversBySellingUnit.values()){
            approverUsernamesSet.add(sellingUnitApprover.Level_2_Regional_VP_of_Ops_Username__c + '%');
            approverUsernamesSet.add(sellingUnitApprover.Level_2_Regional_VP_of_Sales_Username__c + '%');
            approverUsernamesSet.add(sellingUnitApprover.Level_3_SVP_Sales_Username__c + '%');
            approverUsernamesSet.add(sellingUnitApprover.Level_3_SVP_GM_Ops_Username__c + '%');
            approverUsernamesSet.add(sellingUnitApprover.Level_3_LOB_President_Username__c + '%');
            approverUsernamesSet.add(sellingUnitApprover.Level_4_Chairman_CEO_Username__c + '%');
        }
        List<String> approverUsernames = new List<String>(approverUsernamesSet);
        List<User> users = [SELECT Username FROM User WHERE Username LIKE :approverUsernames];
        Map<String, Id> userIdByUsername = new Map<String, Id>();
        for(User u : users){
            for(Selling_Unit_Approvers__c sellingUnitApprover : approversBySellingUnit.values()){
                if(u.Username.startsWith(sellingUnitApprover.Level_2_Regional_VP_of_Ops_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_2_Regional_VP_of_Ops_Username__c, u.Id);
                }
                if(u.Username.startsWith(sellingUnitApprover.Level_2_Regional_VP_of_Sales_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_2_Regional_VP_of_Sales_Username__c, u.Id);
                }
                if(u.Username.startsWith(sellingUnitApprover.Level_3_SVP_Sales_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_3_SVP_Sales_Username__c, u.Id);
                }
                if(u.Username.startsWith(sellingUnitApprover.Level_3_SVP_GM_Ops_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_3_SVP_GM_Ops_Username__c, u.Id);
                }
                if(u.Username.startsWith(sellingUnitApprover.Level_3_LOB_President_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_3_LOB_President_Username__c, u.Id);
                }
                if(u.Username.startsWith(sellingUnitApprover.Level_4_Chairman_CEO_Username__c)){
                    userIdByUsername.put(sellingUnitApprover.Level_4_Chairman_CEO_Username__c, u.Id);
                }
            }
        }
        return userIdByUsername;
    }*/

   /*
    *  Updates approver fields on request base on amout and owner* selling unit
    *  *Owner is determined base on request type
    */
/*    private List<B_P_Request__c> updateApproversBasedOnAmount(
            List<B_P_Request__c> notClosedBPRequests,
            Map<Id, String> ownerSellingUnitByRequestId,
            Map<String, Selling_Unit_Approvers__c> approversBySellingUnit,
            Map<Id, Double> amoutByRequestId,
            Map<String, Id> userIdByUsername
    ){
        // Check for various levels of Total Requested B&P amount and assign approvers accordingly
        List<B_P_Request__c> bPRequests = new List<B_P_Request__c>();
        for (B_P_Request__c request : notClosedBPRequests) {
            String sellingUnit = ownerSellingUnitByRequestId.get(request.Id);
            if (approversBySellingUnit.containsKey(sellingUnit)) {
                Selling_Unit_Approvers__c sellingUnitApprovers = approversBySellingUnit.get(sellingUnit);
                B_P_Request__c updatedRequest =
                        new B_P_Request__c(
                            Id = request.Id,
                            Regional_VP_of_Ops__c = null,
                            Regional_VP_of_Sales__c = null,
                            SVP_GM_Ops__c = null,
                            SVP_Sales__c = null,
                            LOB_PresIdent__c = null,
                            Chairman_CEO__c = null
                        );

                Decimal requestAmount = amoutByRequestId.get(request.Id);
                if (requestAmount > 25000) {
                    Id regionalVPOfOps = userIdByUsername.get(sellingUnitApprovers.Level_2_Regional_VP_of_Ops_Username__c);
                    updatedRequest.Regional_VP_of_Ops__c = regionalVPOfOps;

                    Id regionalVPOfSales = userIdByUsername.get(sellingUnitApprovers.Level_2_Regional_VP_of_Sales_Username__c);
                    updatedRequest.Regional_VP_of_Sales__c = regionalVPOfSales;
                }
                if (requestAmount > 100000 ) {
                    Id svpSales = userIdByUsername.get(sellingUnitApprovers.Level_3_SVP_Sales_Username__c);
                    updatedRequest.SVP_Sales__c = svpSales;

                    Id svpGMOps = userIdByUsername.get(sellingUnitApprovers.Level_3_SVP_GM_Ops_Username__c);
                    updatedRequest.SVP_GM_Ops__c = svpGMOps;
                }
                if (requestAmount > 500000 ) {
                    Id lobPresident = userIdByUsername.get(sellingUnitApprovers.Level_3_LOB_President_Username__c);
                    updatedRequest.LOB_President__c = lobPresident;
                }
                if (requestAmount > 2000000) {
                    Id chairmanCEO = userIdByUsername.get(sellingUnitApprovers.Level_4_Chairman_CEO_Username__c);
                    updatedRequest.Chairman_CEO__c = chairmanCEO;
                }
                bPRequests.add(updatedRequest);
            }
        } //END FOR
        return bPRequests;
    }*/
}