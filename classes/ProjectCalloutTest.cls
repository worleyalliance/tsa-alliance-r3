/**************************************************************************************
Name: ProjectCalloutTest
Version: 1.0 
Created Date: 17.04.2017
Function: This class is used to initiate all the variables and classes in Project_XSD_WS and generate the mock response
that is set by the ProjectCalloutMock class, in order to meet code coverage.  

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      17.04.2017      Original Version
***************************************************************************************/

@isTest
private class ProjectCalloutTest {
    
    @isTest static void testProjectCallout() {
        
        //Customer Parameters
        Project_XSD_WS.APPS_JEG_CUSTOMER_TBL_TYPE customerParameters = new Project_XSD_WS.APPS_JEG_CUSTOMER_TBL_TYPE();
        customerParameters.P_CUSTOMERS_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE>();
        //Customer Record Parameters
        Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE customerRecord = new Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE();
        customerRecord.CUSTOMER_ID = 123;
        customerRecord.CUSTOMER_NUMBER = '123';
        customerRecord.PROJECT_RELATIONSHIP_CODE = 'CR';
        customerRecord.BILL_TO_CUSTOMER_ID = 1;
        customerRecord.BILL_TO_CUSTOMER_NUM = '';
        customerRecord.SHIP_TO_CUSTOMER_ID = 1;
        customerRecord.SHIP_TO_CUSTOMER_NUM ='';
        customerRecord.BILL_TO_ADDRESS_ID =100;
        customerRecord.BILL_TO_SITE_NUMBER ='';
        customerRecord.SHIP_TO_ADDRESS_ID = 100;
        customerRecord.SHIP_TO_SITE_NUMBER = '';
        customerRecord.CONTACT_ID = 100;
        customerRecord.CONTACT_NUMBER ='';
        customerRecord.PROJECT_CONTACT_TYPE_CODE = '';
        customerRecord.CUSTOMER_BILL_SPLIT = 100;
        customerRecord.ALLOW_INV_USER_RATE_TYPE_FLAG = '';
        customerRecord.INV_RATE_DATE = Date.Today();
        customerRecord.INV_RATE_TYPE = '';
        customerRecord.INV_CURRENCY_CODE = '';
        customerRecord.INV_EXCHANGE_RATE = 100;
        customerRecord.ENABLE_TOP_TASK_CUST_FLAG = '';
        customerRecord.BILL_ANOTHER_PROJECT_FLAG = '';
        customerRecord.RECEIVER_TASK_ID = 123;
        customerRecord.RECEIVER_TASK_NUMBER ='';
        customerParameters.P_CUSTOMERS_IN_ITEM.add(customerRecord);
        ////Deliverable Actions Parameters
        Project_XSD_WS.APPS_JEG_ACTION_IN_TBL_TYPE deliverableActions = new Project_XSD_WS.APPS_JEG_ACTION_IN_TBL_TYPE();
        deliverableActions.P_DELIVERABLE_ACTIONS_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_ACTION_IN_REC_TYPE>();
        //ActionRecordParameters
        Project_XSD_WS.APPS_JEG_ACTION_IN_REC_TYPE actionRecord = new Project_XSD_WS.APPS_JEG_ACTION_IN_REC_TYPE();
        actionRecord.ACTION_NAME = '';
        actionRecord.ACTION_OWNER_ID = 123;
        actionRecord.ACTION_OWNER = '';
        actionRecord.ACTION_ID = 123;
        actionRecord.ACTION = 'Test Action';
        actionRecord.FUNCTION_CODE = '';
        actionRecord.DUE_DATE = Date.Today();
        actionRecord.COMPLETION_DATE = Date.Today();
        actionRecord.DESCRIPTION = '';
        actionRecord.PM_SOURCE_CODE = '';
        actionRecord.PM_ACTION_REFERENCE = '';
        actionRecord.PM_DELIVERABLE_REFERENCE = '';
        actionRecord.DELIVERABLE_ID = 123;
        actionRecord.DELIVERABLE = '';
        actionRecord.FINANCIAL_TASK_ID = 123;
        actionRecord.FINANCIAL_TASK_NUM = '';
        actionRecord.FINANCIAL_TASK_REFERENCE = '';
        actionRecord.DESTINATION_TYPE_CODE = '';
        actionRecord.RECEIVING_ORG_ID = 1;
        actionRecord.RECEIVING_ORG_NAME = '';
        actionRecord.RECEIVING_LOCATION_ID = 123;
        actionRecord.RCV_LOCATION_NAME = '';
        actionRecord.PO_NEED_BY_DATE = Date.Today();
        actionRecord.VENDOR_ID = 321;
        actionRecord.VENDOR_NUMBER = '';
        actionRecord.VENDOR_SITE_CODE = '';
        actionRecord.QUANTITY = 100;
        actionRecord.UOM_CODE = '';
        actionRecord.UNIT_PRICE = 100;
        actionRecord.EXCHANGE_RATE_TYPE = '';
        actionRecord.EXCHANGE_RATE_DATE = Date.Today();
        actionRecord.EXCHANGE_RATE = 100;
        actionRecord.EXPENDITURE_TYPE = '';
        actionRecord.EXPENDITURE_ORG_ID = 123;
        actionRecord.EXPENDITURE_ORG_NAME = '';
        actionRecord.EXPENDITURE_ITEM_DATE = Date.Today();
        actionRecord.REQUISITION_LINE_TYPE_ID = 123;
        actionRecord.REQUISITION_LINE_TYPE = '';
        actionRecord.CATEGORY_ID = 123;
        actionRecord.CATEGORY = '';
        actionRecord.READY_TO_PROCURE_FLAG = '';
        actionRecord.INITIATE_PROCURE_FLAG = '';
        actionRecord.SHIP_FROM_ORGANIZATION_ID = 123;
        actionRecord.SHIP_FROM_ORG_NAME = '';
        actionRecord.SHIP_FROM_LOCATION_ID = 123;
        actionRecord.SHIP_FROM_LOC_NAME = '';
        actionRecord.SHIP_TO_ORGANIZATION_ID = 123;
        actionRecord.SHIP_TO_ORG_NAME = '';
        actionRecord.SHIP_TO_LOCATION_ID = 123;
        actionRecord.SHIP_TO_LOC_NAME = '';
        actionRecord.DEMAND_SCHEDULE = '';
        actionRecord.EXPECTED_SHIPMENT_DATE = Date.Today();
        actionRecord.PROMISED_SHIPMENT_DATE = Date.Today();
        actionRecord.VOLUME = 12;
        actionRecord.VOLUME_UOM = '';
        actionRecord.WEIGHT = 123;
        actionRecord.WEIGHT_UOM = '';
        actionRecord.READY_TO_SHIP_FLAG = '';
        actionRecord.INITIATE_PLANNING_FLAG = '';
        actionRecord.INITIATE_SHIPPING_FLAG = '';
        actionRecord.EVENT_TYPE = '';
        actionRecord.CURRENCY_x = 'USD';
        actionRecord.INVOICE_AMOUNT = 100;
        actionRecord.REVENUE_AMOUNT = 100;
        actionRecord.EVENT_DATE = Date.Today();
        actionRecord.EVENT_NUMBER = 123;
        actionRecord.ORGANIZATION_ID = 123;
        actionRecord.ORGANIZATION_NAME = '';
        actionRecord.BILL_HOLD_FLAG = '';
        actionRecord.PROJECT_FUNCTIONAL_RATE_TYPE = '';
        actionRecord.PROJECT_FUNCTIONAL_RATE_DATE = Date.Today();
        actionRecord.PROJECT_FUNCTIONAL_RATE = 100;
        actionRecord.PROJECT_RATE_TYPE = '';
        actionRecord.PROJECT_RATE_DATE = Date.Today();
        actionRecord.PROJECT_RATE = 100;
        actionRecord.FUNDING_RATE_TYPE = '';
        actionRecord.FUNDING_RATE_DATE = Date.Today();
        actionRecord.FUNDING_RATE = 100;
        actionRecord.PM_EVENT_REFERENCE = '';
        deliverableActions.P_DELIVERABLE_ACTIONS_IN_ITEM.add(actionRecord);
        //BudgetParameters
        Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_TBL_TYPE budget = new Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_TBL_TYPE();
        budget.P_BUDGET_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE>();
        //BudgetRecordParameters
        Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE budgetRecord = new Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE();
        budgetRecord.PA_TASK_ID = 123;
        budgetRecord.PA_TASK_NUM = '';
        budgetRecord.PM_TASK_REFERENCE = '';
        budgetRecord.RESOURCE_ALIAS = '';
        budgetRecord.RESOURCE_LIST_MEMBER_ID = 123;
        budgetRecord.RESOURCE_LIST_MEMBER = '';
        budgetRecord.COST_CODE = '';
        budgetRecord.CBS_ELEMENT_ID = 123;
        budgetRecord.CBS_ELEMENT = '';
        budgetRecord.BUDGET_START_DATE = Date.Today();
        budgetRecord.BUDGET_END_DATE = Date.Today();
        budgetRecord.PERIOD_NAME = '';
        budgetRecord.DESCRIPTION = '';
        budgetRecord.RAW_COST = 100;
        budgetRecord.BURDENED_COST = 100;
        budgetRecord.REVENUE = 100;
        budgetRecord.QUANTITY = 100;
        budgetRecord.PM_PRODUCT_CODE = '';
        budgetRecord.PM_BUDGET_LINE_REFERENCE ='';
        budgetRecord.ATTRIBUTE_CATEGORY = '';
        budgetRecord.ATTRIBUTE1 = '';
        budgetRecord.ATTRIBUTE2 = '';
        budgetRecord.ATTRIBUTE3 = '';
        budgetRecord.ATTRIBUTE4 = '';
        budgetRecord.ATTRIBUTE5 = '';
        budgetRecord.ATTRIBUTE6 = '';
        budgetRecord.ATTRIBUTE7 = '';
        budgetRecord.ATTRIBUTE8 = '';
        budgetRecord.ATTRIBUTE9 = '';
        budgetRecord.ATTRIBUTE10 = '';
        budgetRecord.ATTRIBUTE11 = '';
        budgetRecord.ATTRIBUTE12 = '';
        budgetRecord.ATTRIBUTE13 = '';
        budgetRecord.ATTRIBUTE14 = '';
        budgetRecord.ATTRIBUTE15 = '';
        budgetRecord.TXN_CURRENCY_CODE = '';
        budgetRecord.PROJFUNC_COST_RATE_TYPE = '';
        budgetRecord.PROJFUNC_COST_RATE_DATE_TYPE = '';
        budgetRecord.PROJFUNC_COST_RATE_DATE = Date.Today();
        budgetRecord.PROJFUNC_COST_EXCHANGE_RATE = 100;
        budgetRecord.PROJFUNC_REV_RATE_TYPE = '';
        budgetRecord.PROJFUNC_REV_RATE_DATE_TYPE = '';
        budgetRecord.PROJFUNC_REV_RATE_DATE = Date.Today();
        budgetRecord.PROJFUNC_REV_EXCHANGE_RATE = 100;
        budgetRecord.PROJECT_COST_RATE_TYPE = '';
        budgetRecord.PROJECT_COST_RATE_DATE_TYPE = '';
        budgetRecord.PROJECT_COST_RATE_DATE = Date.Today();
        budgetRecord.PROJECT_COST_EXCHANGE_RATE = 100;
        budgetRecord.PROJECT_REV_RATE_TYPE = '';
        budgetRecord.PROJECT_REV_RATE_DATE_TYPE = '';
        budgetRecord.PROJECT_REV_RATE_DATE = Date.Today();
        budgetRecord.PROJECT_REV_EXCHANGE_RATE = 100;
        budgetRecord.CHANGE_REASON_CODE = '';
        budget.P_BUDGET_IN_ITEM.add(budgetRecord);
        //FundingParameters
        Project_XSD_WS.APPS_JEG_FUNDING_IN_TBL_TYPE funding = new Project_XSD_WS.APPS_JEG_FUNDING_IN_TBL_TYPE();
        funding.P_FUNDING_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_FUNDING_REC_IN_TYPE>();
        //FundingRecordParameters
        Project_XSD_WS.APPS_JEG_FUNDING_REC_IN_TYPE fundingRecord = new Project_XSD_WS.APPS_JEG_FUNDING_REC_IN_TYPE();
        fundingRecord.PM_FUNDING_REFERENCE = '';
        fundingRecord.PROJECT_FUNDING_ID = 123;
        fundingRecord.PROJECT_FUNDING = '';
        fundingRecord.AGREEMENT_ID = 123;
        fundingRecord.AGREEMENT_NUM = '';
        fundingRecord.PROJECT_ID = 123;
        fundingRecord.PROJECT_NUMBER = '';
        fundingRecord.TASK_ID = 123;
        fundingRecord.TASK_NUMBER = '';
        fundingRecord.ALLOCATED_AMOUNT = 100;
        fundingRecord.DATE_ALLOCATED = Date.Today();
        fundingRecord.DESC_FLEX_NAME = '';
        fundingRecord.ATTRIBUTE_CATEGORY = '';
        fundingRecord.ATTRIBUTE1 = '';
        fundingRecord.ATTRIBUTE2 = '';
        fundingRecord.ATTRIBUTE3 = '';
        fundingRecord.ATTRIBUTE4 = '';
        fundingRecord.ATTRIBUTE5 = '';
        fundingRecord.ATTRIBUTE6 = '';
        fundingRecord.ATTRIBUTE7 = '';
        fundingRecord.ATTRIBUTE8 = '';
        fundingRecord.ATTRIBUTE9 = '';
        fundingRecord.ATTRIBUTE10 = '';
        fundingRecord.PROJECT_RATE_TYPE = '';
        fundingRecord.PROJECT_RATE_DATE = Date.Today();
        fundingRecord.PROJECT_EXCHANGE_RATE = 100;
        fundingRecord.PROJFUNC_RATE_TYPE = '';
        fundingRecord.PROJFUNC_RATE_DATE = Date.Today();
        fundingRecord.PROJFUNC_EXCHANGE_RATE = 100;
        fundingRecord.FUNDING_CATEGORY = '';
        funding.P_FUNDING_IN_ITEM.add(fundingRecord);
        //Task Parameters
        Project_XSD_WS.APPS_JEG_TASK_IN_TBL_TYPE taskParameters = new Project_XSD_WS.APPS_JEG_TASK_IN_TBL_TYPE();
        taskParameters.P_TASKS_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_TASK_IN_REC_TYPE>();
        //TaskRecord Parameters
        Project_XSD_WS.APPS_JEG_TASK_IN_REC_TYPE taskRecord = new Project_XSD_WS.APPS_JEG_TASK_IN_REC_TYPE();
        taskRecord.PM_TASK_REFERENCE = '';
        taskRecord.PA_TASK_ID = 123;
        taskRecord.TASK_NAME = '';
        taskRecord.LONG_TASK_NAME = '';
        taskRecord.PA_TASK_NUMBER = '';
        taskRecord.TASK_DESCRIPTION = '';
        taskRecord.TASK_START_DATE = Date.Today();
        taskRecord.TASK_COMPLETION_DATE = Date.Today();
        taskRecord.PM_PARENT_TASK_REFERENCE = '';
        taskRecord.PA_PARENT_TASK_ID = 123;
        taskRecord.PA_PARENT_TASK_NUM = '';
        taskRecord.ADDRESS_ID = 123;
        taskRecord.SITE_NUMBER = '';
        taskRecord.CARRYING_OUT_ORGANIZATION_ID = 123;
        taskRecord.CARRYING_OUT_ORGANIZATION_NAME = '';
        taskRecord.SERVICE_TYPE_CODE = '';
        taskRecord.TASK_MANAGER_PERSON_ID = 123;
        taskRecord.TSK_EMPLOYEE_NUMBER = '';
        taskRecord.BILLABLE_FLAG = '';
        taskRecord.CHARGEABLE_FLAG = '';
        taskRecord.READY_TO_BILL_FLAG = '';
        taskRecord.READY_TO_DISTRIBUTE_FLAG = '';
        taskRecord.LIMIT_TO_TXN_CONTROLS_FLAG = '';
        taskRecord.LABOR_BILL_RATE_ORG_ID = 123;
        taskRecord.LABOR_STD_BILL_RATE_SCHDL = '';
        taskRecord.LABOR_SCHEDULE_FIXED_DATE = Date.Today();
        taskRecord.LABOR_SCHEDULE_DISCOUNT = 100;
        taskRecord.NON_LABOR_BILL_RATE_ORG_ID = 123;
        taskRecord.NON_LABOR_BILL_RATE_ORG_NAME = '';
        taskRecord.NON_LABOR_STD_BILL_RATE_SCHDL = '';
        taskRecord.NON_LABOR_SCHEDULE_FIXED_DATE = Date.Today();
        taskRecord.NON_LABOR_SCHEDULE_DISCOUNT = 100;
        taskRecord.LABOR_COST_MULTIPLIER_NAME = '';
        taskRecord.COST_IND_RATE_SCH_ID = 123;
        taskRecord.CST_IND_RATE_SCH_NAME = '';
        taskRecord.REV_IND_RATE_SCH_ID = 123;
        taskRecord.REV_IND_RATE_SCH_NAME = '';
        taskRecord.INV_IND_RATE_SCH_ID = 123;
        taskRecord.INV_IND_RATE_SCH_NAME = '';
        taskRecord.COST_IND_SCH_FIXED_DATE = Date.Today();
        taskRecord.REV_IND_SCH_FIXED_DATE = Date.Today();
        taskRecord.INV_IND_SCH_FIXED_DATE = Date.Today();
        taskRecord.LABOR_SCH_TYPE = '';
        taskRecord.NON_LABOR_SCH_TYPE = '';
        taskRecord.ACTUAL_START_DATE = Date.Today();
        taskRecord.ACTUAL_FINISH_DATE = Date.Today();
        taskRecord.EARLY_START_DATE = Date.Today();
        taskRecord.EARLY_FINISH_DATE = Date.Today();
        taskRecord.LATE_START_DATE = Date.Today();
        taskRecord.LATE_FINISH_DATE = Date.Today();
        taskRecord.SCHEDULED_START_DATE = Date.Today();
        taskRecord.SCHEDULED_FINISH_DATE = Date.Today();
        taskRecord.TASKS_DFF = '';
        taskRecord.ATTRIBUTE_CATEGORY = '';
        taskRecord.ATTRIBUTE1 = '';
        taskRecord.ATTRIBUTE2 = '';
        taskRecord.ATTRIBUTE3 = '';
        taskRecord.ATTRIBUTE4 = '';
        taskRecord.ATTRIBUTE5 = '';
        taskRecord.ATTRIBUTE6 = '';
        taskRecord.ATTRIBUTE7 = '';
        taskRecord.ATTRIBUTE8 = '';
        taskRecord.ATTRIBUTE9 = '';
        taskRecord.ATTRIBUTE10 = '';
        taskRecord.ATTRIBUTE11 = '';
        taskRecord.ATTRIBUTE12 = '';
        taskRecord.ATTRIBUTE13 = '';
        taskRecord.ATTRIBUTE14 = '';
        taskRecord.ATTRIBUTE15 = '';
        taskRecord.ALLOW_CROSS_CHARGE_FLAG = '';
        taskRecord.PROJECT_RATE_DATE = Date.Today();
        taskRecord.PROJECT_RATE_TYPE = '';
        taskRecord.CC_PROCESS_LABOR_FLAG = '';
        taskRecord.LABOR_TP_SCHEDULE_ID = 123;
        taskRecord.LABOR_TP_SCHEDULE = '';
        taskRecord.LABOR_TP_FIXED_DATE = Date.Today();
        taskRecord.CC_PROCESS_NL_FLAG = '';
        taskRecord.NL_TP_SCHEDULE_ID = 123;
        taskRecord.NL_TP_SCHEDULE = '';
        taskRecord.NL_TP_FIXED_DATE = Date.Today();
        taskRecord.RECEIVE_PROJECT_INVOICE_FLAG = '';
        taskRecord.WORK_TYPE_ID = 123;
        taskRecord.WORK_TYPE_NAME = '';
        taskRecord.EMP_BILL_RATE_SCHEDULE_ID = 123;
        taskRecord.EMP_BILL_RATE_SCHEDULE = '';
        taskRecord.JOB_BILL_RATE_SCHEDULE_ID = 123;
        taskRecord.JOB_BILL_RATE_SCHEDULE = '';
        taskRecord.STD_BILL_RATE_SCHEDULE = '';
        taskRecord.NON_LAB_STD_BILL_RT_SCH_ID = 123;
        taskRecord.NON_LAB_STD_BILL_RT_SCH = '';
        taskRecord.TASKFUNC_COST_RATE_TYPE = '';
        taskRecord.TASKFUNC_COST_RATE_DATE = Date.Today();
        taskRecord.DISPLAY_SEQUENCE = 100;
        taskRecord.WBS_LEVEL = 100;
        taskRecord.OBLIGATION_START_DATE = Date.Today();
        taskRecord.OBLIGATION_FINISH_DATE = Date.Today();
        taskRecord.ESTIMATED_START_DATE = Date.Today();
        taskRecord.ESTIMATED_FINISH_DATE = Date.Today();
        taskRecord.BASELINE_START_DATE = Date.Today();
        taskRecord.BASELINE_FINISH_DATE = Date.Today();
        taskRecord.CLOSED_DATE = Date.Today();
        taskRecord.WQ_UOM_CODE = '';
        taskRecord.WQ_ITEM_CODE = '';
        taskRecord.STATUS_CODE = '';
        taskRecord.WF_STATUS_CODE = '';
        taskRecord.PM_SOURCE_CODE = '';
        taskRecord.PRIORITY_CODE = '';
        taskRecord.MILESTONE_FLAG = '';
        taskRecord.CRITICAL_FLAG = '';
        taskRecord.INC_PROJ_PROGRESS_FLAG = '';
        taskRecord.LINK_TASK_FLAG = '';
        taskRecord.CALENDAR_ID = 123;
        taskRecord.PLANNED_EFFORT = 100;
        taskRecord.DURATION = 100;
        taskRecord.PLANNED_WORK_QUANTITY = 100;
        taskRecord.TASK_TYPE = 100;
        taskRecord.LABOR_DISC_REASON_CODE = '';
        taskRecord.NON_LABOR_DISC_REASON_CODE = '';
        taskRecord.RETIREMENT_COST_FLAG = '';
        taskRecord.CINT_ELIGIBLE_FLAG = '';
        taskRecord.CINT_STOP_DATE = Date.Today();
        taskRecord.PRED_STRING = '';
        taskRecord.PRED_DELIMITER = '';
        taskRecord.BASE_PERCENT_COMP_DERIV_CODE = '';
        taskRecord.SCH_TOOL_TSK_TYPE_CODE = '';
        taskRecord.CONSTRAINT_TYPE_CODE = '';
        taskRecord.CONSTRAINT_DATE = Date.Today();
        taskRecord.FREE_SLACK = 100;
        taskRecord.TOTAL_SLACK = 100;
        taskRecord.EFFORT_DRIVEN_FLAG = '';
        taskRecord.LEVEL_ASSIGNMENTS_FLAG = '';
        taskRecord.INVOICE_METHOD = '';
        taskRecord.CUSTOMER_ID = 123;
        taskRecord.CUSTOMER_NUMBER = '';
        taskRecord.GEN_ETC_SOURCE_CODE = '';
        taskRecord.FINANCIAL_TASK_FLAG = '';
        taskRecord.MAPPED_TASK_ID = 123;
        taskRecord.MAPPED_TASK_NUMBER = '';
        taskRecord.MAPPED_TASK_REFERENCE = '';
        taskRecord.DELIVERABLE = '';
        taskRecord.DELIVERABLE_ID = 123;
        taskRecord.EXT_ACT_DURATION = 100;
        taskRecord.EXT_REMAIN_DURATION = 100;
        taskRecord.EXT_SCH_DURATION = 100;
        taskRecord.ETC_EFFORT = 100;
        taskRecord.PERCENT_COMPLETE = 100;
        taskRecord.ADJ_ON_STD_INV = '';
        taskRecord.COST_PLUS_FIXED_FEE_FLAG = '';
        taskRecord.PROGRESS_PAYMENT_FLAG = '';
        taskRecord.RETENTION_PERCENTAGE = 100;
        taskRecord.RATE_PERCENTAGE = 100;
        taskRecord.LIQUIDATION_RATE_PERCENTAGE = 100;
        taskRecord.ALT_LIQUIDATION_RATE_PER = 100;
        taskRecord.WITHHOLD_AMOUNT = 100;
        taskRecord.FEE_RATE_SCHEDULE = '';
        taskRecord.OVR_FEE_RATE_SCHEDULE = '';
        taskRecord.FEE_REVENUE_SCHEDULE = '';
        taskRecord.INV_LINE_GROUP = '';
        taskRecord.BILL_GROUP_NAME = '';
        taskRecord.FEE_PERCENTAGE = 100;
        taskRecord.FEE_RATE = 100;
        taskParameters.P_TASKS_IN_ITEM.add(taskRecord);
        //External Attributes Parameters
        Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_TABLE_TYPE extAttributes = new Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_TABLE_TYPE();
        extAttributes.P_EXT_ATTR_TBL_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_ROW_TYPE>();
        //AttributesRecord Parameters
        Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_ROW_TYPE attRecord = new Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_ROW_TYPE();
        attRecord.PROJ_ELEMENT_ID = 100;
        attRecord.PROJ_ELEMENT_REFERENCE = '';
        attRecord.ROW_IDENTIFIER = 123;
        attRecord.ATTR_GROUP_INT_NAME = '';
        attRecord.ATTR_GROUP_ID = 123;
        attRecord.ATTR_INT_NAME = '';
        attRecord.ATTR_VALUE_STR  = '';
        attRecord.ATTR_VALUE_NUM = 123;
        attRecord.ATTR_VALUE_DATE = Date.Today();
        attRecord.ATTR_DISP_VALUE = '';
        attRecord.ATTR_UNIT_OF_MEASURE  = '';
        attRecord.USER_ROW_IDENTIFIER = '';
        attRecord.TRANSACTION_TYPE = '';
        extAttributes.P_EXT_ATTR_TBL_IN_ITEM.add(attRecord);
        //DeliverablesParameters
        Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_TBL_TYPE deliverables = new Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_TBL_TYPE();
        deliverables.P_DELIVERABLES_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_REC_TYPE>();
        //Deliverable Record
        Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_REC_TYPE deliverableRecord = new Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_REC_TYPE();
        deliverableRecord.DELIVERABLE_SHORT_NAME = '';
        deliverableRecord.DELIVERABLE_NAME = '';
        deliverableRecord.DESCRIPTION  = '';
        deliverableRecord.DELIVERABLE_OWNER_ID = 123;
        deliverableRecord.DELIVERABLE_OWNER = '';
        deliverableRecord.STATUS_CODE = '';
        deliverableRecord.DELIVERABLE_TYPE_ID = 123;
        deliverableRecord.DELIVERABLE_TYPE = '';
        deliverableRecord.PROGRESS_WEIGHT = 100;
        deliverableRecord.DUE_DATE = Date.Today();
        deliverableRecord.COMPLETION_DATE = Date.Today();
        deliverableRecord.PM_SOURCE_CODE = '';
        deliverableRecord.PM_DELIVERABLE_REFERENCE = '';
        deliverableRecord.DELIVERABLE_ID = 123;
        deliverableRecord.DELIVERABLE = '';
        deliverableRecord.TASK_ID = 123;
        deliverableRecord.TASK_NUMBER  = '';
        deliverableRecord.TASK_SOURCE_REFERENCE = '';
        deliverableRecord.ITEM_ID = 123;
        deliverableRecord.ITEM = '';
        deliverableRecord.INVENTORY_ORG_ID = 123;
        deliverableRecord.INV_ORGANIZATION_NAME = '';
        deliverableRecord.QUANTITY = 100;
        deliverableRecord.UOM_CODE = '';
        deliverableRecord.UNIT_PRICE = 100;
        deliverableRecord.UNIT_NUMBER = '';
        deliverableRecord.CURRENCY_CODE = '';
        deliverables.P_DELIVERABLES_IN_ITEM.add(deliverableRecord);
        //Class Category Parameters
        Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_TBL_TYPE classCategoryParameters = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_TBL_TYPE();
        classCategoryParameters.P_CLASS_CATEGORIES_ITEM = new List<Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE>();
        //ClassCategoryRecord Parameters
        Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
        classCategoryRecord.CLASS_CATEGORY = '';
        classCategoryRecord.CLASS_CODE = '';
        classCategoryRecord.NEW_CLASS_CODE = '';
        classCategoryRecord.CODE_PERCENTAGE = 100;
        classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord);
        
        
        //     String x = 'Salesforce';
        //     String y = 'a0Mn0000000gRfd';
        
        /*     TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
Account acc = accountBuilder.build().getRecord();
insert acc;

TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
Opportunity opp = oppBuilder.build().withAccount(acc.Id).getOpportunity();
insert opp;
*/      
        //Build Project Record    
        TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
        Jacobs_Project__c proj = projectBuilder.build().getRecord();
        proj.Project_Status__c='Request Re-Open';
        //    Jacobs_Project__c proj = projectBuilder.build().withOpportunity(opp.id).getRecord();      
        //    insert proj;
        
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ProjectCalloutMock());
        
        projectQueueableHandler job = new projectQueueableHandler(proj);
        job.max = 3;
        ID jobID = System.enqueueJob(job);
        system.debug(job.result);
        
        //string result = ProjectSOAPCallout.createProject(x, y);
        Test.stopTest();
        // Verify that a fake result is returned
        // System.assertEquals('S', result); 
    }
    
    @isTest static void testCalloutException() {
        TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
        Jacobs_Project__c proj = projectBuilder.build().save().getRecord();
        proj.Project_Status__c='Request Pending Closure';
        Test.setMock(WebServiceMock.class, new ResponseExceptionMock());
        
        Test.startTest();
        projectQueueableHandler job = new projectQueueableHandler(proj);
        job.max = 1;
        ID jobID = System.enqueueJob(job);
        Test.stopTest();
        
    }
}