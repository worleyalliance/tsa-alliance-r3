public class SharePointSiteRequestExceptionData {
    public String id;
	public String sharePointUrl;
    public DateTime requestedOn;
    public String message;
    public Boolean displayOverride;

    public SharePointSiteRequestExceptionData(String id, String message, DateTime requestedOn, Boolean displayOverride, String sharePointUrl) {
        this.id = id;
        this.sharePointUrl = sharePointUrl;
        this.message = message;
        this.requestedOn = requestedOn;
        this.displayOverride = displayOverride;
    }

}