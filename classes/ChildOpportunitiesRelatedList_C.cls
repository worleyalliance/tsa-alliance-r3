/**************************************************************************************
Name: ChildOpportunitiesRelatedList_C
Version: 1.0 
Created Date: 08.05.2017
Function: Related list of opportunity child opportunities

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           08.05.2017         Original Version
* Scott Stone               01.09.2019         US2678: add method to retrieve fields copied in enhanced clone + their values.
*************************************************************************************/
public with sharing class ChildOpportunitiesRelatedList_C {
    private static ChildOpportunitiesRelatedList_Service service = new ChildOpportunitiesRelatedList_Service();

   /*
    *   Retrieves list of child opportunities
    */
    @AuraEnabled
    public static List<ChildOpportunitiesRelatedList_Service.OpportunityWrapper> getChildOpportunities(Id recordId) {
        return service.getChildOpportunities(recordId);
    }
    
   /*
    *   Retrieves opportunity record with needed data
    */
    @AuraEnabled
    public static Opportunity getOpportunity(Id recordId) {
        return service.getOpportunity(recordId);
    }

   /*
    *   Deletes record with given id
    */
    @AuraEnabled
    public static void deleteOpportunity(Id recordId) {
        service.deleteOpportunity(recordId);
    }

   /*
    *   Gives update regarding user permissions to opportunity object
    */
    @AuraEnabled
    public static BaseComponent_Service.CRUDpermissions getUserPermissions() {
        return service.getUserOpportunityPermissios();
    }
    
    /*
     * US2698
     * gets list of cloned fields with their values.
     */
    @AuraEnabled
    public static List<ChildOpportunitiesRelatedList_Service.OpportunityCloneField> getClonedOpportunityFieldsAndValues(Id opportunityId) {
        if(opportunityId == null) { return null; }
        return service.getClonedOpportunityFieldsAndValues(opportunityId);
    }
}