/**************************************************************************************
Name: Library_TriggerTest
Version: 1.0 
Created Date: 09/04/2018
Function: To Test Library_Trigger
Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo     09/04/2018      Original Version
* Chris Cornejo     09/04/2018      DE667 - UAT - Library Records - Can't Edit LOB, BU and PU fields
*************************************************************************************/

@IsTest
public with sharing class Library_TriggerTest {
    
    //Constants
    private static final String PROFILE_SALES_SUPER_USER    = 'Sales Super User';
    private static final String USERNAME                    = 'salesOppTriggerTest@jacobstestuser.net';
    
    //DE667 - testing insert and update on PU change
    @isTest
    private static void shouldSetLOBBUonUpdate() {
        
        Library__c lib;
       
        String LOB;
        String BU;
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile('Sales Super User').save().getRecord();
        
        //Start Test
        Test.startTest();
        
        System.runAs(testUser){
                    
            TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
            //Unit__c ATNPU = unitBuilder.build().withLOB('ATN').withBU('ATN - Mission Solutions (MS)').withName('020342 (ATN MS) JT-I2S').save().getRecord();
            //Unit__c BIAFPU = unitBuilder.build().withLOB('BIAF').withBU('BIAF - Americas').withName('003700 (BIAF AMER) Aviation-Us').save().getRecord();

            Unit__c DigitalPU = unitBuilder.build().withLOB('Digital').withBU('JESA - Joint Venture').withName('020342 (Digital MS) JT-I2S').save().getRecord();
            Unit__c JESAPU = unitBuilder.build().withLOB('JESA').withBU('JESA - Joint Venture').withName('003700 (JESA AMER) Aviation-Us').save().getRecord();

                        
            TestHelper.LibraryBuilder libraryBuilder = new TestHelper.LibraryBuilder();
            lib = LibraryBuilder.build().getRecord();
                     
            lib.Performance_Unit__c = DigitalPU.Id;
            Insert lib;
                        
            //Query Library and check the Performance Unit
            List<Library__c> testLibrary = [Select Business_Unit__c, Line_of_Business__c
                                            From Library__c
                                            Where Id = :lib.Id];
            LOB = testLibrary[0].Line_of_Business__c;
            BU  = testLibrary[0].Business_Unit__c;                 
            
            lib.Performance_Unit__c = JESAPU.Id;
            Update lib;
              
        }
        
        //Stop Test
        Test.stopTest();
        
        //Query Library and check the Performance Unit
        List<Library__c> testLibrary = [Select Line_of_Business__c, Business_Unit__c
                                        From Library__c
                                        Where Id = :lib.Id]; 
        
        //Assertions
        System.assertEquals('Digital', LOB);
        System.assertEquals('JESA - Joint Venture', BU);
        
        System.assertEquals('JESA', testLibrary[0].Line_of_Business__c);
        System.assertEquals('JESA - Joint Venture', testLibrary[0].Business_Unit__c);
        
    }  
}