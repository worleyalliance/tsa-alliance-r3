/**************************************************************************************
Name: PartnerDataTable_C_Test
Version: 1.0 
Created Date: 07.05.2017
Function: Unit test class for PartnerDataTable_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.05.2017         Original Version
*************************************************************************************/
@isTest
private class PartnerDataTable_C_Test {

    @IsTest
    private static void shouldCalculateDataForPrartnerRecords() {
        //given
        Map<String, FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('Partner__c').getDescribe().fieldSets.getMap();
        String fieldSetName =
                fieldSetMap.values()[0].name;

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.PartnerBuilder partnerBuilder = new TestHelper.PartnerBuilder();
        Partner__c partner =
                partnerBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .withPartner(acc.Id)
                        .save()
                        .getRecord();

        TestHelper.ReviewBuilder reviewBuilder = new TestHelper.ReviewBuilder();
        Review__c review =
                reviewBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                PartnerDataTable_C.getData(
                        fieldSetName,
                        review.Id
                );

        //then
        System.assertEquals(1, result.rows.size());
    }
}