/**
 * Created by Stefan Abramiuk on 08.07.2017.
 */

public with sharing class FieldDescribeUtil {

    public enum ResultType {LABEL, VALUE}

    public static PicklistDependency getDependentOptionsImpl(Schema.SObjectField theField, Schema.SObjectField ctrlField, Set<String> skippedValues) {
        // validFor property cannot be accessed via a method or a property,
        // so we need to serialize the PicklistEntry object and then deserialize into a wrapper.
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries =
                FieldDescribeUtil.wrapPicklistEntries(theField.getDescribe().getPicklistValues());

        // Set up the return container - Map<ControllingValue, List<DependentValues>>
        PicklistDependency picklistDepent = new PicklistDependency();
        Map<String, List<String>> objResults = new Map<String, List<String>>();
        Map<String, String> controlingByDependent = new Map<String, String>();
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String value = ple.getValue(); //changed to Value
            objResults.put(value, new List<String>());
            controllingValues.add(value);
        }

        for (PicklistEntryWrapper plew : depEntries) {
            String value = plew.value; //changed to Value
            if(!skippedValues.contains(value)) {
                String validForBits = base64ToBits(plew.validFor);
                for (Integer i = 0; i < validForBits.length(); i++) {
                    // For each bit, in order: if it's a 1, add this label to the dependent list for the corresponding controlling value
                    String bit = validForBits.mid(i, 1);
                    if (bit == '1') {
                        String controllingValue = controllingValues.get(i);
                        if(!skippedValues.contains(controllingValue)) {
                            objResults.get(controllingValue).add(value);
                            controlingByDependent.put(value, controllingValue);
                        }
                    }
                }
            }
        }
        picklistDepent.dependentByControling = objResults;
        picklistDepent.controlingByDependent = controlingByDependent;

        return picklistDepent;
    }

    public static PicklistDependency getDependentOptionsWithLabels(Schema.SObjectField theField, ResultType fieldResultType, Schema.SObjectField ctrlField, ResultType ctrlFieldResultType, Set<String> skippedValues) {
        // validFor property cannot be accessed via a method or a property,
        // so we need to serialize the PicklistEntry object and then deserialize into a wrapper.
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries =
                FieldDescribeUtil.wrapPicklistEntries(theField.getDescribe().getPicklistValues());

        // Set up the return container - Map<ControllingValue, List<DependentValues>>
        PicklistDependency picklistDepent = new PicklistDependency();
        Map<String, List<String>> objResults = new Map<String, List<String>>();
        Map<String, String> controlingByDependent = new Map<String, String>();
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String value = '';
            if(ctrlFieldResultType == ResultType.LABEL){
            	value = ple.getLabel();   
            } else {
                value = ple.getValue();
            }
              
            objResults.put(value, new List<String>());
            controllingValues.add(value);
        }

        for (PicklistEntryWrapper plew : depEntries) {
            String value = '';
            if(fieldResultType == ResultType.LABEL){
            	value = plew.label;   
            } else {
                value = plew.value;
            }
              
            if(!skippedValues.contains(value)) {
                String validForBits = base64ToBits(plew.validFor);
                for (Integer i = 0; i < validForBits.length(); i++) {
                    // For each bit, in order: if it's a 1, add this label to the dependent list for the corresponding controlling value
                    String bit = validForBits.mid(i, 1);
                    if (bit == '1') {
                        String controllingValue = controllingValues.get(i);
                        if(!skippedValues.contains(controllingValue)) {
                            objResults.get(controllingValue).add(value);
                            controlingByDependent.put(value, controllingValue);
                        }
                    }
                }
            }
        }
        picklistDepent.dependentByControling = objResults;
        picklistDepent.controlingByDependent = controlingByDependent;

        return picklistDepent;
    }
    
    // Convert decimal to binary representation (alas, Apex has no native method :-(
    //    eg. 4 => '100', 19 => '10011', etc.
    // Method: Divide by 2 repeatedly until 0. At each step note the remainder (0 or 1).
    // These, in reverse order, are the binary.
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }

    // Convert a base64 token into a binary/bits representation
    // e.g. 'gAAA' => '100000000000000000000'
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';

        String validForBits = '';

        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }

        return validForBits;
    }


    private static final String base64Chars = '' +
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
            'abcdefghijklmnopqrstuvwxyz' +
            '0123456789+/';


    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
                JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }

    public class PicklistEntryWrapper {
        public String active { get; set; }
        public String defaultValue { get; set; }
        public String label { get; set; }
        public String value { get; set; }
        public String validFor { get; set; }
    }

    public class PicklistDependency {
        public Map<String, String> controlingByDependent { get; set; }
        public Map<String, List<String>> dependentByControling { get; set; }
    }

}