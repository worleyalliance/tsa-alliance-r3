/**************************************************************************************
Name:RecordTypeSelection_C 
Version: 1.0 
Created Date: 03/13/2018
Function: Controller for component used to show record types for selection

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -                 
* Pradeep Shetty    03/13/2019      Original Version
* Pradeep Shetty   08/20/2018      DE633: Sorting order of Record types is now a parameter
*******************************************************************************************/
public with sharing class RecordTypeSelection_C {
    private static RecordTypeSelection_Service service = new RecordTypeSelection_Service();

   /*
    * Returns list of recordtypes to be shown on recordtype selection
    */
    @AuraEnabled
    public static List<RecordType> retrieveRecordTypes(String objectAPIName, String recordTypeNameString,String sortOrder){
        return service.retrieveRecordTypes(objectAPIName, recordTypeNameString, sortOrder);
    }

   /*
    * Get the object name for a given Object API Name
    */
    @AuraEnabled
    public static String getObjectName(String objectAPIName){
      return service.getObjectName(objectAPIName);
    }
}