/**************************************************************************************
Name: OpportunityClone_c_Test
Version: 1.0 
Created Date: 03.30.2017
Function: Class used in unit test to ie. prepare test data

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           03.30.2017         Original Version
* Raj Vakati                10.08.2017         US588 story fix (Enhanced Clone - Not Copying All Multi-Office Splits)

*************************************************************************************/

@IsTest
private class OpportunityClone_c_Test {

   @isTest
   private static void shouldCloneOpportunity(){
       //given
       User currentUser = new User(
               Id = UserInfo.getUserId(),
               Line_of_Business__c = 'BIAF',
               Business_Unit__c = 'BIAF - Americas'
       );
       update currentUser;
       TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
       Account acc = accountBuilder.build().save().getRecord();
       TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();

       //when
       Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();
	   opp.Is_Cloned_Opportunity__c = false ;
       opp.Forecast_Category__c='Excluded';
       Test.startTest();
       Id cloneId = OpportunityClone_C.doClone(opp.Id);
       Test.stopTest();

       //then
       System.assertNotEquals(cloneId, opp.id);

       List<Opportunity> results = [SELECT Id, Name FROM Opportunity WHERE Id = :cloneId];
       System.assertEquals(1, results.size());
   }

    @isTest
    private static void shouldCloneOpportunityWithLongName(){
        //given
        User currentUser = new User(
                Id = UserInfo.getUserId(),
                Line_of_Business__c = 'BIAF',
                Business_Unit__c = 'BIAF - Americas'
        );
        update currentUser;
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();

        //when
        String veryLongName = 'qwertyuiopasdfghjklzqwertyuiopasdfghjklzqwertyuiopasdfghjklzqwertyuiopasdfghjklz';
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().withName(veryLongName).save().getOpportunity();
        Test.startTest();
        Id cloneId = OpportunityClone_C.doClone(opp.Id);
        Test.stopTest();

        //then
        System.assertNotEquals(cloneId, opp.id);

        List<Opportunity> results = [SELECT Id, Name FROM Opportunity WHERE Id = :cloneId];
        System.assertEquals(1, results.size());
        // System.assertEquals(80, results[0].Name.length());
    }

    @isTest
    private static void shouldCloneOpportunityWithSWOT(){
        User currentUser = new User(
                Id = UserInfo.getUserId(),
                Line_of_Business__c = 'JESA',
                Business_Unit__c = 'JESA - Joint Venture'
        );
        update currentUser;
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        TestHelper.SWOTBuilder swotBuilder = new TestHelper.SWOTBuilder();
        SWOT__c swot = swotBuilder.build().withOpportunity(opp.Id).withOpportunities('someValue').save().getRecord();

        Test.startTest();
        Id cloneId = OpportunityClone_C.doClone(opp.Id);
        Test.stopTest();

        List<SWOT__c> results = [SELECT Opportunities__c FROM SWOT__c WHERE Opportunity__c = :cloneId];
        System.assertEquals(1, results.size());
        System.assertEquals(swot.Opportunities__c, results[0].Opportunities__c);
    }
}