/**************************************************************************************
Name: SharePointClient_Test
Version: 1.0 
Created Date: 24.02.2017
Function: Test class for SharePointClient

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/
@IsTest
private class SharePointClient_Test {
/*

    @IsTest
    private static void shoulRetrieveFilesFromFolderInSharepoint() {
        //given
        Test.setMock(HttpCalloutMock.class, new SharePoint_Mock());

        //when
        SharePointClient client = new SharePointClient();
        List<SharePointConfiguration.File> files;
        try{
            files = client.retrieveFolderFiles('test/test123');

            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
        System.assertEquals(1, files.size());
    }

    @IsTest
    private static void shouldThrowErrorWhenRetrievingFilesFromSharepoint() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        SharePointClient client = new SharePointClient();

        try{
            client.retrieveFolderFiles('test/test123');

            //then
            System.assert(false, 'Should throw an exception');
        } catch (Exception ex){
            System.assert(ex instanceof SharePointClient.SharpointCommunicationException, 'Wrong exception type');
        }
    }

    @IsTest
    private static void shouldCreateSmallFileInSharepoint() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        SharePointClient client = new SharePointClient();
        Integer result;
        try{
            result = client.uploadDocument(
                    'test.txt',
                    Blob.valueOf('some value'),
                    'test/test123',
                    'notImportant',
                    'notImportant',
                    0,
                    true,
                    true,
                    false);
            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
        System.assertEquals(null, result);
        System.assertEquals(1, mock.executionCounter);
    }

    @IsTest
    private static void shouldStarUploadOfLargeFileInSharepoint() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.successResp = '{"d":{"StartUpload":"16"}}';
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        SharePointClient client = new SharePointClient();
        Integer result;
        try{
            result = client.uploadDocument(
                    'test.txt',
                    Blob.valueOf('some value'),
                    'test/test123',
                    'notImportant',
                    'notImportant',
                    0,
                    true,
                    false,
                    false);
            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
        System.assertEquals(16, result);
        System.assertEquals(2, mock.executionCounter); // first callout resets/creates new file, second upload fisrt chunk
    }

    @IsTest
    private static void shouldContinueUploadOfLargeFileInSharepoint() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.successResp = '{"d":{"ContinueUpload":"16"}}';
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        SharePointClient client = new SharePointClient();
        Integer result;
        try{
            result = client.uploadDocument(
                    'test.txt',
                    Blob.valueOf('some value'),
                    'test/test123',
                    'notImportant',
                    'notImportant',
                    0,
                    false,
                    false,
                    false);
            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
        System.assertEquals(16, result);
        System.assertEquals(1, mock.executionCounter);
    }

    @IsTest
    private static void shouldFinishUploadOfLargeFileInSharepoint() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        SharePointClient client = new SharePointClient();
        Integer result;
        try{
            result = client.uploadDocument(
                    'test.txt',
                    Blob.valueOf('some value'),
                    'test/test123',
                    'notImportant',
                    'notImportant',
                    0,
                    false,
                    false,
                    true);
            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
        System.assertEquals(null, result);
        System.assertEquals(1, mock.executionCounter);
    }

    @IsTest
    private static void shouldDeleteFileInSharepoint() {
        //given
        Test.setMock(HttpCalloutMock.class, new SharePoint_Mock());

        //when
        SharePointClient client = new SharePointClient();

        try{
            client.deleteDocument('test.txt', 'test/test123');

            //then
        } catch (Exception ex){
            System.assert(false, 'Should not throw an exception');
        }
    }
*/

}