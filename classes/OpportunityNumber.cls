/**************************************************************************************
Name: OpportunityNumber
Version: 1.0 
Created Date: 12/06/2018
Function: Class for generating alphanumeric code for Opportunity

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty   12/06/2018       US1855: Original Version
*************************************************************************************/

public with sharing class OpportunityNumber {
    @InvocableMethod(label='Generate alphanumeric code' description='Generates an alphanumeric code for a given number')
    public static void generateOpportunityNumber(List<Opportunity> opps) {
        List<Opportunity> updateOppList = new List<Opportunity>();

        //Loop through each opportunity and generate alphanumeric code based on the autonumber
        for(Opportunity opp: opps){         
            updateOppList.add(new Opportunity(Id = opp.Id, 
                                              Opportunity_Number__c = Utility.generateAlphaNumericCode(Integer.valueOf(opp.System_Number__c))));
        }

        //Update Opportunities
        update updateOppList;
  }
}