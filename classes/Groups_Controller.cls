/**************************************************************************************
Name: Groups_Controller
Version: 
Created Date: 
Function: LDT table for the Group Object to display Library records.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo     7/25/2017       In addition to DE164, DE165, US718, correct record types was not displaying for Group.
* Madhu Shetty     31-Jul-2018      Updated condition to show inactive record types in filters
*************************************************************************************/
public with sharing class Groups_Controller
{
public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues()
    {
        List<Schema.RecordTypeInfo> recordtypes = Library__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes)
        {
            List <LDT_Lightning_Filter__c> rtgrplist = [Select Label_del__c 
                                                        from LDT_Lightning_Filter__c 
                                                        where Object_API__c = 'AT_Group__c' 
                                                        and Coverage__c = 'Include'];// and Active__c = True];
             
            for (LDT_Lightning_Filter__c g: rtgrplist)
            {
                if (rt.getName() == g.Label_del__c)
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }        
        }
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel)
    {
        Id recid = Schema.SObjectType.Library__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }
    
    @AuraEnabled
    public static List<Library__c> getTOsOfProject(ID id) 
    {
        return getCBAsofProjectwRecordType(id, 'All');
    }
    @AuraEnabled
    public static List<Library__c> getCBAsOfProjectwRecordType(ID id, String recordTypeLabel) 
    {
    
        if( recordTypeLabel != 'All'){
        return [SELECT Id,Name,dt_submittal_date__c,lu_group__c, RecordType.Name
                FROM Library__c
                WHERE lu_group__c = :id and RecordType.Name in (:recordTypeLabel)
                ];}
        else{
        return [SELECT Id,Name,dt_submittal_date__c,lu_group__c, RecordType.Name
                FROM Library__c
                WHERE lu_group__c = :id
                ];}
    }
}