/**************************************************************************************
Name: DataTable_Service
Version: 1.0 
Created Date: 07.03.2017
Function: Base component service which will deliver functionalities for Data Table controllers

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.03.2017         Original Version
* Chris Cornejo             05.22.2018         DE460 - Projects - Accounts Project Subtab - ProjectsDataTable Component and Related List Issues
*************************************************************************************/
public virtual class DataTable_Service extends BaseComponent_Service{

    private ValueOpeartorService valueOpeartorService = new ValueOpeartorService();

   /*
    * Mapping used to set proper order of the months
    */
    private final static Map<String, Integer> MONTH_MAPPING = new Map<String, Integer>{
            'January'  => 1,
            'February' => 2,
            'March'    => 3,
            'April'    => 4,
            'May'      => 5,
            'June'     => 6,
            'July'     => 7,
            'August'   => 8,
            'September'=> 9,
            'October'  => 10,
            'November' => 11,
            'December' => 12
    };

    public ValueOpeartorService getValueOpeartorService(){
        return valueOpeartorService;
    }

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    public TableData getData(
            String fieldSetName,
            Id recordId,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault,
            String additionalValueField
    ) {
        validateObjectAccesible(Schema.getGlobalDescribe().get(sObjectName).getDescribe().getName());

        List<FieldDescription> fieldDescriptions = prepareFieldDescriptions(fieldSetName, sObjectName);
        String query =
                buildQuery(
                        fieldDescriptions,
                        recordId,
                        relationField,
                        sObjectName
                );

        //Execute query
        List<sObject> records = (List<sObject>) Database.query(query);
        TableData data =
                buildTableData(
                    records,
                    fieldDescriptions,
                    totalRowPresent,
                    isSumDefault,
                    additionalValueField
                );
        return data;
    }

    protected TableData buildTableData(
            List<sObject> records,
            List<FieldDescription> fieldDescriptions,
            Boolean totalRowPresent,
            Boolean isSumDefault,
            String additionalValueField
    ){
        TableData data = new TableData();
        data.fieldDescriptions = fieldDescriptions;
        data.rows = new List<Map<String, Object>>();
        Map<String, Decimal> totalRowValues = new Map<String, Decimal>();
        List<Map<String, Object>> rows = new List<Map<String, Object>>();
        for(sObject obj : records){
            Map<String, Object> record = new Map<String, Object>();
            for(FieldDescription fieldDescription : fieldDescriptions){
                if(totalRowPresent && !totalRowValues.containsKey(fieldDescription.fieldName)){
                    totalRowValues.put(fieldDescription.fieldName, 0);
                }
                Map<String, Object> objectMap = obj.getPopulatedFieldsAsMap();
                String[] path = fieldDescription.fieldName.split('\\.');
                Map<String, Object> val = objectMap;
                for (Integer i = 0; i < path.size() - 1; i++) {
                    val = ((sObject)val.get(path[i])).getPopulatedFieldsAsMap();
                }
                Object fieldValue = val.get(path[path.size()-1]);
                if(fieldDescription.fieldType == DisplayType.DOUBLE || fieldDescription.fieldType == DisplayType.CURRENCY ) {
                    fieldValue = fieldValue != null ? (Decimal)fieldValue : 0;
                    if(totalRowPresent) {
                        totalRowValues.put(fieldDescription.fieldName, totalRowValues.get(fieldDescription.fieldName) + (Decimal) fieldValue);
                    }
                }
                //cc mod for DE460
                Object value = calculateFieldSpecificValue(fieldValue, fieldDescription, obj);
                record.put(fieldDescription.fieldName, value);
            }
            rows.add(record);
        }
        data.rows = rows;
        if(totalRowPresent) {
            data.totalRow =
                    updateTotalRowFields(totalRowValues, records.size(), fieldDescriptions, isSumDefault, additionalValueField);
        }
        return data;
    }

    protected virtual String buildQuery(
            List<FieldDescription> fieldDescriptions,
            Id recordId,
            String relationField,
            String sObjectName
    ){
        //Bulid query
        List<String> fieldNames = getFieldNames(fieldDescriptions);
        String query =
                'SELECT '+String.join(fieldNames, ',')+' ' +
                        'FROM ' + sObjectName +' '+
                        'WHERE '+relationField+ '= \''+recordId+'\'';
        System.debug('query: '+query);
        return query;
    }

   /*
    * Method creates list of fields and it's labels from field set
    */
    protected List<FieldDescription> prepareFieldDescriptions(String fieldSetName, String sObjectName){
        if(String.isBlank(fieldSetName)){
            //TODO
            throw new AuraHandledException('Field Set is not valid');
        }
        Map<String, Schema.FieldSet> fieldSets = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fieldSets.getMap();
        List<FieldDescription> fieldDescriptions = new List<FieldDescription>();
        for(FieldSetMember fsm : fieldSets.get(fieldSetName).fields){
            FieldDescription singleField = new FieldDescription();
            singleField.fieldLabel  = fsm.getLabel();
            singleField.fieldName   = fsm.getFieldPath();
            singleField.fieldType   = fsm.getType();
            fieldDescriptions.add(singleField);
        }
        return fieldDescriptions;
    }

   /*
    * Sets custom value for specific fields
    */
    protected virtual Object calculateFieldSpecificValue(Object fieldValue, FieldDescription fieldDescription, sObject sObjectName){
        Object value;
        if(fieldDescription.fieldType == DisplayType.CURRENCY){
            fieldValue = ((Decimal) fieldValue).setScale(0).format();
            value = '' + UserDataProvider.getUserCurrencySymbol() + fieldValue;
        } 
        //CC Display value as null if null
        else if (fieldDescription.fieldType == DisplayType.Reference && fieldValue == null){
            value = '';
        }
        //CC Reference Type field, get Name instead of a record ID display
        else if (fieldDescription.fieldType == DisplayType.Reference && fieldValue != null){
            //objectAPI = (String.valueof(sObjectName)).left((String.valueof(sObjectName)).indexOfChar(58));

            //Get the Object API using describe information
            Schema.SObjectType objType = sObjectName.getSobjectType();
            Schema.DescribeSObjectResult objRes = objType.getDescribe();
            String objectAPI = objres.getName();
            
            String lookupObject;     
            Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectAPI).getDescribe().fields.getMap();
           
            if(fieldMap.containsKey(fieldDescription.fieldName)){
              
              value = fieldMap.get(fieldDescription.fieldName).getDescribe().getReferenceTo();
              lookupObject = String.valueof(value).remove('(');
              lookupObject = String.valueof(lookupObject).remove(')');
            }
            value = DataTableService_noSharing.getName(lookupObject, fieldValue);
        }
       // }
        else {
        
            value = fieldValue;
        }
        return value;
    }

   /*
    * Sets footer row values. Each signle column can have different calculation logic
    * Most basic one is just SUM and is default behaviour
    */
    protected virtual Map<String, Object> updateTotalRowFields(
            Map<String, Decimal> totalRowValues,
            Integer groupAttritionCount,
            List<FieldDescription> fieldDescriptions,
            Boolean isSumDefault,
            String additionalValueField
    ){
        Map<String, Object> totalRow = new Map<String, Object>();
        for(FieldDescription fieldDescription : fieldDescriptions) {
            Object value = calculateFieldSecificTotalRowValue(
                    totalRowValues.get(fieldDescription.fieldName),
                    fieldDescription,
                    groupAttritionCount,
                    totalRowValues,
                    isSumDefault,
                    additionalValueField
            );
            totalRow.put(fieldDescription.fieldName, value);
        }
        return totalRow;
    }

   /*
    * Sets footer row value based on field name
    * Each field may have different logic behind for calculation of total row
    * Single if handles logic for known fields
    * Last else is handling standard behaviour for default logic
    */
    private Object calculateFieldSecificTotalRowValue(
            Object fieldValue,
            FieldDescription fieldDescription,
            Integer groupAttritionCount,
            Map<String, Decimal> totalRowValues,
            Boolean isSumDefault,
            String additionalValueField
    ){

        Object value;
        IFieldValueOperator valueOperator = getValueOpeartorService().getFieldSpecificValueOperator(fieldDescription.fieldName);
        if(valueOperator != null && fieldDescription.fieldName != additionalValueField){
            value = valueOperator.calculate(fieldValue, groupAttritionCount, additionalValueField, totalRowValues);
        } else {
            valueOperator = valueOpeartorService.getFieldDefaultValueOperator(fieldDescription.fieldName != additionalValueField ? isSumDefault : true);
            value = valueOperator.calculate(fieldValue, groupAttritionCount, additionalValueField, totalRowValues);
        }
        if(fieldDescription.fieldType == DisplayType.CURRENCY){
            value = '' + UserDataProvider.getUserCurrencySymbol() + ((Decimal)value).setScale(0).format();
        }

        return value;
    }

   /*
    * Rerturns field api names from list of field descriptions
    */
    public List<String> getFieldNames(List<FieldDescription> fieldDescriptions){
        List<String> fieldNames = new List<String>();
        for (FieldDescription fieldDescription : fieldDescriptions) {
            fieldNames.add(fieldDescription.fieldName);
        }
        return fieldNames;
    }

   /*
    * Total data set used to generate table
    */
    public class TableData{
        @AuraEnabled
        public Map<String, Object> totalRow {get; set;}

        @AuraEnabled
        public List<FieldDescription> fieldDescriptions {get; set;}

        @AuraEnabled
        public List<Map<String, Object>> rows {get; set;}
    }

   /*
    * Total data set used to generate table
    */
    public virtual class FieldDescription{
        @AuraEnabled
        public String fieldName {get; set;}

        @AuraEnabled
        public String fieldLabel {get; set;}

        public DisplayType fieldType;
    }

   /*
    * Strategy pattern inteface
    */
    public interface IFieldValueOperator{
        Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues);
    }

   /*
    * Default when you want to see sum and percent related to given field
    */
    public class DefaultPercentOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            Decimal totalValue = totalRowValues.get(additionalField);
            totalValue = totalValue > 0 ? totalValue : 1;
            Object value =
                    String.valueOf(decValue.setScale(0)) +
                            '(' + String.valueOf(((Decimal) decValue / totalValue * 100).setScale(1))+'%)';
            return value;
        }
    }

   /*
    * Default when you want to see only sum
    */
    public class DefaultSumOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {
            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            return decValue;
        }
    }

   /*
    * Service which will deliver calculatio method for given field
    */
    public class ValueOpeartorService{
        public final String DEFAULT_SUM_OPERATOR = 'DefaultSumOperator';
        public final String DEFAULT_PERCENT_OPERATOR = 'DefaultPercentOperator';

        public final Map<String, IFieldValueOperator> operatorsByKey = new Map<String, IFieldValueOperator>{
                DEFAULT_PERCENT_OPERATOR => new DefaultPercentOperator(),
                DEFAULT_SUM_OPERATOR => new DefaultSumOperator()
        };

        public final Map<String, String> fieldNameByOperatorKey = new Map<String, String>{};

        public IFieldValueOperator getFieldSpecificValueOperator(String fieldName){
            String operatorKey = fieldNameByOperatorKey.get(fieldName);
            return operatorsByKey.get(operatorKey);
        }

        public IFieldValueOperator getFieldDefaultValueOperator(Boolean isSumDefault){
            return isSumDefault ? operatorsByKey.get(DEFAULT_SUM_OPERATOR) : operatorsByKey.get(DEFAULT_PERCENT_OPERATOR);
        }
    }
}