/**************************************************************************************
Name: SharePoint_Service
Version: 1.0 
Created Date: 28.02.2017
Function: Complex business logic related to communication with SharePoint

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           28.02.2017         Original Version
*************************************************************************************/
public class SharePoint_Service {

   /*
    *  Not all files contains LinkingUrl parameter.
    *  For those which don't have it we compile url based on endpint and server relative path
    */
/*    public List<SharePointConfiguration.File> updateFileLink(List<SharePointConfiguration.File> files){
        List<SharePointConfiguration.File> resFiles = new List<SharePointConfiguration.File>();
        NamedCredential namedCredential = [SELECT Endpoint FROM NamedCredential WHERE DeveloperName = :SharePointConfiguration.NAMED_CREDENTIAL_DEV_NAME];

        for(SharePointConfiguration.File file : files){
            if( (file.LinkingUrl == null || file.LinkingUrl == '')
             && file.ServerRelativeUrl != null
             && file.ServerRelativeUrl != ''){
                String idValue = EncodingUtil.urlEncode(file.ServerRelativeUrl, 'UTF-8');
                String parentValue = EncodingUtil.urlEncode(file.ServerRelativeUrl.removeEndIgnoreCase('/'+file.Name), 'UTF-8');
                file.LinkingUrl = namedCredential.Endpoint + SharePointConfiguration.SHAREPOINT_ALL_ITEMS_FORM_URL + '?id=' + idvalue + '&parent=' +parentValue;
            }
            resFiles.add(file);
        }
        return resFiles;
    }

    private final static String NAME_FIELD = 'Name';
    private final static String CASE_NUMBER_FIELD = 'CaseNumber';
    private final static String ID_FIELD = 'Id';*/


   /*
    *  Easy to use method which will create root path for each single object based on it's id and cofiguration
    */
/*    public List<String> preprareRootPaths(List<Id> ids, String objectType){
        List<SharePointObjectFolderConfiguration__mdt> folderConfigurations = [
                SELECT
                        Object_API_Name__c, FolderName__c, IsReference__c, Level__c, ReferenceObjectType__c, ReferenceFieldName__c
                FROM SharePointObjectFolderConfiguration__mdt
                ORDER BY Object_API_Name__c, Level__c DESC
        ];

        Map<String, List<SharePointObjectFolderConfiguration__mdt>> folderConfigurationByObject = prepareFolderConfigurationByObject(folderConfigurations);
        LinkedFolderConfiguration firstConfigElement;
        if(folderConfigurationByObject.containsKey(objectType)) {
            firstConfigElement = prepareConfigurationChainAndGetFrist(folderConfigurationByObject, objectType);
            firstConfigElement = prepareFields(firstConfigElement);
        }
        List<String> fields = getFields(firstConfigElement, objectType);
        List<sObject> objects = makeQuery(fields, objectType, ids);
        if(objects.isEmpty() ||  objects.size() != ids.size()){
            throw new AuraHandledException(Label.SomeRecordsAreNotAccessible);
        }
        List<String> rootPaths = new List<String>();
        for(sObject obj : objects){
            rootPaths.add(createRootPath(obj, firstConfigElement));
        }
        return rootPaths;
    }*/


   /*
    *  Sorts configuration by each object
    */
/*
    private Map<String, List<SharePointObjectFolderConfiguration__mdt>> prepareFolderConfigurationByObject(
            List<SharePointObjectFolderConfiguration__mdt> folderConfigurations
    ){
        Map<String, List<SharePointObjectFolderConfiguration__mdt>> folderConfigurationByObject = new Map<String, List<SharePointObjectFolderConfiguration__mdt>>();
        for(SharePointObjectFolderConfiguration__mdt configuration : folderConfigurations){
            if(!folderConfigurationByObject.containsKey(configuration.Object_API_Name__c)){
                folderConfigurationByObject.put(configuration.Object_API_Name__c, new List<SharePointObjectFolderConfiguration__mdt>());
            }
            folderConfigurationByObject.get(configuration.Object_API_Name__c).add(configuration);
        }
        return folderConfigurationByObject;
    }
*/

   /*
    *  Prepares a chain list where first object is last level of foler path for given object type
    */
/*
    private LinkedFolderConfiguration prepareConfigurationChainAndGetFrist(
            Map<String, List<SharePointObjectFolderConfiguration__mdt>> folderConfigurationByObject,
            String objectType){
        List<SharePointObjectFolderConfiguration__mdt> singleObjectConfiguration = folderConfigurationByObject.get(objectType);
        LinkedFolderConfiguration firstEntry = new LinkedFolderConfiguration(singleObjectConfiguration[0]);
        LinkedFolderConfiguration objectEntry = firstEntry;
        for(Integer i=1; i < singleObjectConfiguration.size(); i++){
            LinkedFolderConfiguration nextEntry = new LinkedFolderConfiguration(singleObjectConfiguration[i]);
            objectEntry.setNext(nextEntry);
            objectEntry = nextEntry;
        }
        if(objectEntry.getConfiguration().IsReference__c && folderConfigurationByObject.containsKey(objectEntry.getConfiguration().ReferenceObjectType__c)){
            LinkedFolderConfiguration nextEntry =
                    prepareConfigurationChainAndGetFrist(folderConfigurationByObject, objectEntry.getConfiguration().ReferenceObjectType__c);
            objectEntry.setNext(nextEntry);
        }
        return firstEntry;
    }
*/

   /*
    *  Prepares a field names which will be used to compose SOQL query
    */
/*    private LinkedFolderConfiguration prepareFields(LinkedFolderConfiguration firstObjectEntry){
        String relation = '';
        LinkedFolderConfiguration objectEntry = firstObjectEntry;
        while(objectEntry != null){
            if(objectEntry.getConfiguration().IsReference__c){
                objectEntry.setIdFieldPath(relation + objectEntry.getConfiguration().ReferenceFieldName__c);
                String referenceField = getRelationField(objectEntry.getConfiguration().ReferenceFieldName__c);
                objectEntry.setNameFieldPath(relation + referenceField + '.' + NAME_FIELD);
                relation = relation + referenceField + '.';
            }
            objectEntry = objectEntry.getNext();
        }
        return firstObjectEntry;
    }*/

   /*
    *  Generates a list of fields for SOQL query
    */
/*    private List<String> getFields(LinkedFolderConfiguration objectEntry, String objectType){
        List<String> fields = new List<String>();
        while(objectEntry != null){
            if(objectEntry.getConfiguration().IsReference__c){
                fields.add(objectEntry.getIdFieldPath());
                fields.add(objectEntry.getNameFieldPath());
            }
            objectEntry = objectEntry.getNext();
        }

        fields.add(objectType == Case.sObjectType.getDescribe().getName() ? CASE_NUMBER_FIELD : NAME_FIELD);
        fields.add(ID_FIELD);
        return fields;
    }*/

   /*
    *  Prepares a query for given fields
    */
/*    private List<sObject> makeQuery(List<String> fields, String objectType, List<Id> ids){
        List<Id> localIds = ids;
        String query = 'SELECT '+String.join(fields, ',') + ' FROM ' + objectType + ' WHERE Id IN :localIds';
        System.debug('query: '+query);
        return Database.query(query);
    }*/

   /*
    *  Field name is pointing to Id of reference object.
    *  We need to have name of field for relation which is i.e. Opportunity__r
    */
/*    private String getRelationField(String fieldName){
        return fieldName.removeEndIgnoreCase('Id').replace('__c', '__r');
    }*/

   /*
    *  Prepares a Root Folder path for given object based on linked list
    */
/*    private String createRootPath(
            sObject obj,
            LinkedFolderConfiguration firstConfigElement){

        LinkedFolderConfiguration objectEntry = firstConfigElement;
        String rootPath = (obj instanceof Case ? obj.get(CASE_NUMBER_FIELD) : obj.get(NAME_FIELD)) + '(' + obj.Id + ')';
        while(objectEntry != null){
            if(objectEntry.getConfiguration().IsReference__c){
                String relationName = (String) getFieldValue(obj, objectEntry.getNameFieldPath());
                String relationId = (String) getFieldValue(obj, objectEntry.getIdFieldPath());
                rootPath = relationName + '(' + relationId + ')/'+rootPath;
            } else {
                rootPath = objectEntry.getConfiguration().FolderName__c + '/'  + rootPath;
            }
            objectEntry = objectEntry.getNext();
        }

        return rootPath;
    }*/

   /*
    *  Retruns a field value for given field path
    */
/*    private Object getFieldValue(sObject obj, String fieldPath){
        List<String> fields = fieldPath.split('\\.');
        Object value = obj;
        for(String field : fields){
            if(value instanceof sObject && ((sObject) value).getPopulatedFieldsAsMap().containsKey(field)){
                value = ((sObject) value).getPopulatedFieldsAsMap().get(field);
            } else {
                throw new InvalidFieldNameException(field);
            }
        }
        return value;
    }*/

   /*
    * Method prepares forlder structure foe single record
    */
/*    public List<String> createFolderPathsForRootPath(String recordRootPath, String objectType){
        List<String> folderPathList = new List<String>{SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + recordRootPath};
        for(String folderPath : SharePointConfiguration.getFolderStructureFor(objectType)){
            folderPathList.add(SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + recordRootPath+'/'+folderPath);
        }
        return folderPathList;
    }*/

   /*
    *  Forward Linked list implementation
    */
/*    public class LinkedFolderConfiguration {
        private SharePointObjectFolderConfiguration__mdt configRecord;

        private LinkedFolderConfiguration next;
        private String nameFieldPath;
        private String idFieldPath;

        public LinkedFolderConfiguration(SharePointObjectFolderConfiguration__mdt configuration){
            this.configRecord = configuration;
        }

        public SharePointObjectFolderConfiguration__mdt getConfiguration(){
            return configRecord;
        }

        public LinkedFolderConfiguration getNext(){
            return next;
        }

        public void setNext(LinkedFolderConfiguration next){
            this.next = next;
        }

        public String getNameFieldPath(){
            return nameFieldPath;
        }

        public void setNameFieldPath(String nameFieldPath){
            this.nameFieldPath = nameFieldPath;
        }


        public String getIdFieldPath(){
            return idFieldPath;
        }

        public void setIdFieldPath(String idFieldPath){
            this.idFieldPath = idFieldPath;
        }
    }

    public class InvalidFieldNameException extends Exception {}*/
}