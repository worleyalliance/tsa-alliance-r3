/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 14.03.2017
Function: Service for BookedActuals component. Makes all the logic for component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   05/05/2017      Original Version
* Pradeep Shetty    01/09/2019      DE335: Closed Won values should be 0 if the Opp is not won
*******************************************************************************************************************/
public class BookedActualsService extends BaseComponent_Service{

 /*
  *   Builds data for table for given opportunity id
  */
  public BookedActual getBookedActuals(Id opportunityId){
    validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());
    List<Opportunity> opportunities =[SELECT Revenue__c, 
                                             Budgeted_Revenue2__c, 
                                             Budgeted_Revenue_Variance__c, 
                                             Budgeted_Rev_Var_Percent__c,
                                             Amount, 
                                             Budgeted_GM2__c, 
                                             Budgeted_GM_Variance__c, 
                                             Budgeted_GM_Var_Percent__c,
                                             Sold_GM_HR__c, 
                                             Budgeted_GM_HR__c, 
                                             Budgeted_GM_HR_Variance__c, 
                                             Budgeted_GM_HR_Var_Percent__c,
                                             IsWon
                                       FROM Opportunity
                                       WHERE Id = :opportunityId
    ];
    if(opportunities.isEmpty()){
        throw new AuraHandledException(Label.NoRecords);
    }
    BookedActual bookedActual = new BookedActual(opportunities[0]);
    return bookedActual;
  }

 /*
  *   Data model for table
  */
  public class BookedActual {

      @AuraEnabled
      public String soldRevenue {get; set;}

      @AuraEnabled
      public String budgetedRevenue {get; set;}

      @AuraEnabled
      public String varianceRevenue {get; set;}

      @AuraEnabled
      public String variancePercRevenue {get; set;}


      @AuraEnabled
      public String soldGM {get; set;}

      @AuraEnabled
      public String budgetedGM {get; set;}

      @AuraEnabled
      public String varianceGM {get; set;}

      @AuraEnabled
      public String variancePercGM {get; set;}


      //@AuraEnabled
      //public String soldWorkhours {get; set;}

      //@AuraEnabled
      //public String actualWorkhours {get; set;}

      //@AuraEnabled
      //public String varianceWorkhours {get; set;}

      //@AuraEnabled
      //public String variancePercWorkhours {get; set;}


      @AuraEnabled
      public String soldGMWorkhours {get; set;}

      @AuraEnabled
      public String budgetedGMWorkhours {get; set;}

      @AuraEnabled
      public String varianceGMWorkhours {get; set;}

      @AuraEnabled
      public String variancePercGMWorkhours {get; set;}

     /*
      *   Constructor
      */
      public BookedActual(Opportunity opp){
          soldRevenue = opp.IsWon? setScale(opp.Revenue__c) : setScale(0);
          budgetedRevenue = setScale(opp.Budgeted_Revenue2__c);
          varianceRevenue = setScale(opp.Budgeted_Revenue_Variance__c);
          variancePercRevenue = setScale(opp.Budgeted_Rev_Var_Percent__c);

          soldGM = opp.IsWon? setScale(opp.Amount) : setScale(0);
          budgetedGM = setScale(opp.Budgeted_GM2__c);
          varianceGM = setScale(opp.Budgeted_GM_Variance__c);
          variancePercGM = setScale(opp.Budgeted_GM_Var_Percent__c);

          //soldWorkhours = setScale(opportunity.Work_Hours__c);
          //actualWorkhours = setScale(opportunity.Billable_Hours__c);
          //varianceWorkhours = setScale(opportunity.Variance_Hours__c);
          //variancePercWorkhours = setScale(opportunity.Percent_Variance_Work_Hours__c);

          soldGMWorkhours = opp.IsWon? setScale(opp.Sold_GM_HR__c) : setScale(0);
          budgetedGMWorkhours = setScale(opp.Budgeted_GM_HR__c);
          varianceGMWorkhours = setScale(opp.Budgeted_GM_HR_Variance__c);
          variancePercGMWorkhours = setScale(opp.Budgeted_GM_HR_Var_Percent__c);
      }

     /*
      *   Method sets decimal spaces to display
      */
      private String setScale(Decimal value){
          value = value != null ? value : 0;
          return String.valueOf(value.setScale(2));
      }

  }
}