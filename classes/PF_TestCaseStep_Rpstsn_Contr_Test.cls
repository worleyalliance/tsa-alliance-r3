/**
* Test Class for PF_TestCaseStep_Representation_Contr
**/
@isTest
private class PF_TestCaseStep_Rpstsn_Contr_Test {
    
    static testMethod void testSaveDefect() {
        Test.startTest();
        
        PF_Product__c product = new PF_Product__c(Name = 'Add Defect Product');      
        insert product;
        
        PF_Epic__c epic = new PF_Epic__c(Name = 'Add Defect Enhancement',
                                         PF_Product__c = product.Id);      
        insert epic;
        
        PF_Stories__c story = new PF_Stories__c(Name = 'Add Defect from Test Case Assignment',
                                                PF_Story_Owner__c = Userinfo.getUserId(),
                                                PF_Epic__c = epic.Id);      
        insert story;
        
        PF_TestCases__c testCase = new PF_TestCases__c(
        //RecordTypeId = Schema.SObjectType.PF_TestCases__c.getRecordTypeInfosByName().get('Unit Test Case').getRecordTypeId(),
                                                       Name = 'Test creation of Defect',
                                                       PF_Type__c = 'Unit Test',
                                                       PF_Description__c = 'Test creation of defect from Test Case Assignment screen', 
                                                       PF_Story__c = story.Id);      
        insert testCase;
        PF_Test_Case_Step__c testCaseStep = new PF_Test_Case_Step__c(Name = 'Test Case Step',
                                                                     PF_Test_Case__c = testCase.Id,
                                                                     PF_Step_Number__c = 1,
                                                                     PF_Step_Description__c = 'Click on Clone',
                                                                     PF_Expected_Result__c = 'User should be navigated to the clone screen');      
        insert testCaseStep;
        
        PF_TestCaseExecution__c testCaseAssign = new PF_TestCaseExecution__c(PF_Test_Case__c = testCase.Id,
                                                                             PF_Assigned_To__c = Userinfo.getUserId(),    
                                                                             PF_Target_Completion_Date__c = Date.today() + 10);      
        insert testCaseAssign;
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('PF_Severity__c','Critical');
        gen.writeStringField('PF_Type__c','Defect');
        gen.writeStringField('PF_Status__c','New');
        gen.writeStringField('PF_Resolution_Type__c','Configuration');
        gen.writeStringField('PF_Priority__c','High');
        gen.writeStringField('Name','test');
        gen.writeDateField('PF_Planned_Fixed_Date__c', Date.today());
        gen.writeStringField('PF_Resolution_Details__c','<p>This is a test defect</p>');
        gen.writeStringField('PF_Description__c','<p>This is a test description</p>');
        gen.writeStringField('PF_Steps_to_Reproduce__c','<p>Click on the link</p>');
        gen.writeEndObject();
        String jsonString = gen.getAsString();
        
        
        PF_TestCaseStep_Representation_Contr.getTestCaseSteps(testCaseAssign.id);
        PF_TestCaseStep_Representation_Contr.getCurrentTestCase();
        PF_TestCaseStep_Representation_Contr.createDefect(jsonString, testCaseAssign.id, testCaseStep.id);
        PF_TestCaseStep_Representation_Contr.getPicklist();
        PF_TestCaseStep_Representation_Contr.getFieldsMapPicklists();
        PF_TestCaseStep_Representation_Contr.checkPageAccess();
        Test.stopTest();
    }
}