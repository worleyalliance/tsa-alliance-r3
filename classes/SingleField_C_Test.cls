/**************************************************************************************
Name: SingleField_C_Test
Version: 1.1 
Created Date: 24.02.2017
Function: Unit test for SingleField_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           02.24.2017         Original Version
* Pradeep Shetty            07.27.2017         DE167: Edit button visibility 
* Pradeep Shetty            10.11.2017         DE270: Disable fields for Closed Won Opp
*************************************************************************************/
@isTest
private class SingleField_C_Test {

  private static final String ACCT_STATUS_VERIFIED     = 'Verified';

  @isTest
  private static void shouldRertieveAccountName(){
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    System.debug(accountBuilder.build().getRecord());
    Account acc = accountBuilder.build().save().getRecord();

    //when
    Object result = SingleField_C.retrieveFieldValue('Account', 'Name', acc.Id);

    //then
    System.assertEquals(acc.Name, (String)result);
  }

  @isTest
  private static void shouldThrowExceptionForInvalidFieldName(){
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    //when
    Object result;
    try {
        result = SingleField_C.retrieveFieldValue('Account', 'Name123', acc.Id);

        //then
        System.assert(false, 'Should throw excpetion');
    } catch (Exception ex){
        System.assert(ex instanceof AuraHandledException);
    }
  }

  @isTest
  private static void shouldThrowExceptionForInvalidFieldType(){
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    //when
    Object result;
    try {
        result = SingleField_C.retrieveFieldValue('Account', 'CreatedDate', acc.Id);

        //then
        System.assert(false, 'Should throw excpetion');
    } catch (Exception ex){
        System.assert(ex instanceof AuraHandledException);
    }
  }

  @isTest
  private static void shouldUpdateAccountField(){
    //given
    String newName = 'new name';
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().withName(newName).save().getRecord();

    //when
    SingleField_C.updateFieldValue('Account', 'Name', acc.Id, newName);

    //then
    Account result = [SELECT Name FROM Account WHERE Id = :acc.Id];
    System.assertEquals(newName, result.Name);
  }

  @isTest
  private static void shouldHideEditButton(){
      
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    Account acc;
    //Test User 1 to create Account
    User testuser1 = userBuilder.build().save().getRecord();        

    //Test User 2 to access the Account
    User testuser2 = userBuilder.build().withProfile('Inside Sales').withRole('B_I_Sales_Team').save().getRecord();
    Boolean result;

    System.runAs(testuser1){
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        acc = accountBuilder.build().save().getRecord();
    }

    Test.startTest();

    System.runAs(testUser2){

        try {
            result = SingleField_C.checkEditPermission('Account', 'Account_Strategy__c', acc.Id);
        } catch (Exception ex){
            System.assert(ex instanceof AuraHandledException);
        }            
    }

    Test.stopTest();

    System.assertEquals(false, result);
  }   
  
  //Added as part of DE270
  @isTest
  private static void shouldHideEditButtonForClosedWonOpp(){
    
    Account acc;
    Opportunity opp;

    //Test User to create Account and Opportunity
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();    
    User datasteward = userBuilder.build()
                                .withProfile('Inside Sales')
                                .withRole('B_I_Sales_Team')
                                .save()
                                .getRecord();
    User testuser = userBuilder.build()
                                .withProfile('Inside Sales')
                                .withRole('B_I_Sales_Team')
                                .save()
                                .getRecord();
    Boolean result;

    System.runAs(datasteward)
    {
      //Create Verified Account
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      acc = accountBuilder.build().withStatus(ACCT_STATUS_VERIFIED).getRecord();
      acc.ownerId = testuser.id;
      insert acc;
    }
      
    Test.startTest();
    System.runAs(testuser)
    { 
      //Create Opportunity
      TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
      opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

      try
      {
        //Call service method
        result = SingleField_C.checkEditPermission('Opportunity', 'Description', opp.Id);
      } 
      catch (Exception ex)
      {
        System.assert(ex instanceof AuraHandledException);
      } 

      System.assert(result);

      //Get the Multi Office record associated with this Opportunity
      Multi_Office_Split__c multiOffice = [Select Id,
                                                Resource_Type__c
                                         From Multi_Office_Split__c 
                                         Where Opportunity__c = :opp.Id 
                                         Limit 1];

      //Update the resource type of the multioffice
      multioffice.Resource_Type__c = 'Pass Through (No Hours)';
      update multioffice;

      //Set the stage to Closed Won
      opp.Reason_Lost__c = 'Team';
      opp.StageName = 'Closed - Won';
      
      //Add Primary location to prevent validatin rule
      opp.Primary_Country__c = 'US';

      update opp;

      try
      {
        //Call service method
        result = SingleField_C.checkEditPermission('Opportunity', 'Description', opp.Id);
      } 
      catch (Exception ex)
      {
        System.assert(ex instanceof AuraHandledException);
      } 

      System.assert(!result);

    }

    Test.stopTest();
   }
}