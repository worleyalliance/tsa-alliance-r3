/**************************************************************************************
Name: JacobsOracleSyncRetryBatch_Test
Version: 1.0 
Created Date: 05.07.2017
Function: Unit test class JacobsOracleSyncRetryBatch

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.07.2017         Original Version
*************************************************************************************/
@isTest
private class JacobsOracleSyncRetryBatch_Test {

    @isTest
    private static void shouldUseRetryLogToExecuteCalloutForAccount() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = acc.Id,
                        ObjectType__c = Account.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = 1
                );
        insert retrySyncLog;
        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 404 Not Found"';
        Test.setMock(WebServiceMock.class, mock);


        //when
        Test.startTest();
        JacobsOracleSyncRetryBatch batch = new JacobsOracleSyncRetryBatch();
        batch.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT NumberOfAttempts__c FROM RetrySyncLog__c WHERE Id = :retrySyncLog.Id];
        System.assertEquals(2, result[0].NumberOfAttempts__c);
    }

    @isTest
    private static void shouldUseRetryLogToExecuteCalloutForProject() {
        //given
        TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
        Jacobs_Project__c project = projectBuilder.build().save().getRecord();

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = project.Id,
                        ObjectType__c = Jacobs_Project__c.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = 1
                );
        insert retrySyncLog;
        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 404 Not Found"';
        Test.setMock(WebServiceMock.class, mock);

        //when
        Test.startTest();
        JacobsOracleSyncRetryBatch batch = new JacobsOracleSyncRetryBatch();
        batch.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT NumberOfAttempts__c FROM RetrySyncLog__c WHERE Id = :retrySyncLog.Id];
        System.assertEquals(2, result[0].NumberOfAttempts__c);
    }

    @isTest
    private static void shouldSkipRetryLogWhenNumberOfAttemptsIsTooBig() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = acc.Id,
                        ObjectType__c = Account.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = OracleCalloutRetryService.MAX_RETRY_ATTEMPTS+1
                );
        insert retrySyncLog;
        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 404 Not Found"';
        Test.setMock(WebServiceMock.class, mock);


        //when
        Test.startTest();
        JacobsOracleSyncRetryBatch batch = new JacobsOracleSyncRetryBatch();
        Database.executeBatch(batch);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT NumberOfAttempts__c FROM RetrySyncLog__c WHERE Id = :retrySyncLog.Id];
        System.assertEquals(retrySyncLog.NumberOfAttempts__c, result[0].NumberOfAttempts__c);
    }
}