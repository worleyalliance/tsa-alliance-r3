/**************************************************************************************
Name: ContentDocument_TriggerHandler
Version: 1.0
Created Date: 10/09/2017
Function: Handler for ContentDocument_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    12/05/2017      Original Version
*************************************************************************************/
public with sharing class ContentDocument_TriggerHandler {
    
    //Trigger Handler  
    private static ContentDocument_TriggerHandler handler;
    
    /*
	* Singleton like pattern
	*/
    public static ContentDocument_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new ContentDocument_TriggerHandler();
        }
        return handler;
    }
    
    /*
	* Actions performed after the content document is deleted
	*/
    public void onAfterDelete(List<ContentDocument> delLibraryFiles){
        ContentDocumentLink_TriggerHandler.recalcFileCount(getLibraries(delLibraryFiles));
    }
    
    /* 
	* Update the count of Files attached to a Library document
	*/
    public List<Library__c> getLibraries(List<ContentDocument> libraryFiles){

        //Must convert the list to Ids to be able to execute the query for ContentDocumentLink
        Set<Id> docIds = new Set<Id>(new Map<Id, ContentDocument>(libraryFiles).keySet());
        Set<Id> parentIds = new Set<Id>();
        
        //Get the document link records associated
        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>([SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN : docIds ALL ROWS]);
        for(ContentDocumentLink link : docLinks){
            parentIds.add(link.LinkedEntityId);
        }
        
        //Update the Library records with the new count of Files
        List<Library__c> parentLibraries = new List<Library__c>([SELECT Id, num_count_of_related_files__c  
                                            					 FROM Library__c WHERE Id IN : parentIds]);
        
        return parentLibraries;
    }
}