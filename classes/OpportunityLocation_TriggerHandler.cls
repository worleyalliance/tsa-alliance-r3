/**************************************************************************************
Name: OpportunityLocation_TriggerHandler
Version: 1.0
Created Date: 03/08/2018
Function: Handler for OpportunityLocation_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson	08/03/2018		US1789 - Require only one primary to be checked
*************************************************************************************/
public class OpportunityLocation_TriggerHandler {
    
    private static OpportunityLocation_TriggerHandler handler;
	private static boolean run = true; 
    
    /*
    * Singleton like pattern
    */
    public static OpportunityLocation_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new OpportunityLocation_TriggerHandler();
        }
        return handler;
    }
    
    /*
    * Actions performed on Opportunity Location records before insert
    */
    public void onBeforeInsert(List<Opportunity_Locations__c> locations) {
        setFirstLocationAsPrimary(locations);
    }
    
    /*
    * Actions performed on Opportunity Location records after update
    */
    public void onAfterUpdate(Map<Id, Opportunity_Locations__c> locations, Map<Id, Opportunity_Locations__c> oldLocations) {
        List<Opportunity_Locations__c> updatedPrimaryLocations = new List<Opportunity_Locations__c>();
        List<Opportunity_Locations__c> removedPrimaryLocations = new List<Opportunity_Locations__c>();
        
        for (Id i : locations.keyset()) {
            Opportunity_Locations__c newLocation = locations.get(i);
            if (newLocation.Primary__c == true && newLocation.Primary__c != oldLocations.get(i).Primary__c) {
                updatedPrimaryLocations.add(locations.get(i));
            } else if (newLocation.Primary__c == false && newLocation.Primary__c != oldLocations.get(i).Primary__c) {
                removedPrimaryLocations.add(locations.get(i));
            }
        }
        
        if (runOnce()) {
        	Utility.uniqueFieldAcrossChildren(updatedPrimaryLocations, 'Opportunity__c', 'Primary__c');
        	updateParentOppPrimaryLocation(updatedPrimaryLocations);
            clearParentOppPrimaryLocation(removedPrimaryLocations);
        }
    }
    
    /*
    * Actions performed on Opportunity Location records before delete
    */
    public void onBeforeDelete(List<Opportunity_Locations__c> locations) {
        List<Opportunity_Locations__c> deletedPrimaryLocations = new List<Opportunity_Locations__c>();
        
        for (Opportunity_Locations__c ol : locations) {
            if (ol.Primary__c == true) {
            	deletedPrimaryLocations.add(ol);
            }
        }
        System.debug(deletedPrimaryLocations);
        clearParentOppPrimaryLocation(deletedPrimaryLocations);
    }
    
    /*
    * Method to set first Opportunity Location created to primary
    */
    private void setFirstLocationAsPrimary(List<Opportunity_Locations__c> locations){
        List<Id> parentIdList = new List<id>();
        List<Opportunity_Locations__c> newPrimaryLocations = new List<Opportunity_Locations__c>();
        
        for (Opportunity_Locations__c ol : locations) {
            parentIdList.add(ol.Opportunity__c);
        }
        
        // Loop through Opportunities and check if they have any locations
        for (Opportunity parentOpp : [SELECT Id, (SELECT Id FROM Opportunity_Locations__r)
                                      FROM Opportunity WHERE Id IN : parentIdList]) {
			if (parentOpp.Opportunity_Locations__r.isEmpty()) {
                for (Opportunity_Locations__c ol : locations) {
                    if (parentOpp.Id == ol.Opportunity__c) {
                        ol.Primary__c = true;
                        newPrimaryLocations.add(ol);
                    }
                }
            }
        }
        
        updateParentOppPrimaryLocation(newPrimaryLocations);
    }
    
    /*
    * Method updates the parent Opportunity primary location fields
    */
    private void updateParentOppPrimaryLocation(List<Opportunity_Locations__c> locations) {
        List<Id> parentOppList = new List<id>();
        List<Id> parentLocList = new List<id>();
        
        for (Opportunity_Locations__c ol : locations) {
            parentOppList.add(ol.Opportunity__c);
            parentLocList.add(ol.Location__c);
        }
        
        List<Opportunity> parentOpps = [SELECT Id FROM Opportunity WHERE Id IN : parentOppList];
        Map<Id, SiteLocation__c> locationDetails = new Map<Id, SiteLocation__c>(
        	[SELECT Id, Street_Address__c, City__c, State_Province__c, Country__c, Postal_Code__c 
             FROM SiteLocation__c WHERE Id IN : parentLocList]);
        
        // Loop through Opportunities and set the primary fields
        for (Opportunity parentOpp : parentOpps) {
			for (Opportunity_Locations__c ol : locations) {
                if (parentOpp.Id == ol.Opportunity__c) {
                    parentOpp.Primary_Street__c = locationDetails.get(ol.Location__c).Street_Address__c;
                    parentOpp.Primary_City__c = locationDetails.get(ol.Location__c).City__c;
                    parentOpp.Primary_State_Province__c = locationDetails.get(ol.Location__c).State_Province__c;
                    parentOpp.Primary_Country__c = locationDetails.get(ol.Location__c).Country__c;
                    parentOpp.Primary_Zip_Postal_Code__c = locationDetails.get(ol.Location__c).Postal_Code__c;
                }
            }
        }
        
        update parentOpps;
    }
    
    /*
    * Method clears the parent Opportunity primary location fields
    */
    private void clearParentOppPrimaryLocation(List<Opportunity_Locations__c> locations) {
        
        List<Opportunity> parentOpps = [SELECT Id FROM Opportunity 
                                        WHERE Id IN (SELECT Opportunity__c FROM Opportunity_Locations__c 
                                                       WHERE Id IN : locations)];
		System.debug('parent: ' + parentOpps);
        for (Opportunity parentOpp : parentOpps) {
            parentOpp.Primary_Street__c = '';
            parentOpp.Primary_City__c = '';
            parentOpp.Primary_State_Province__c = '';
            parentOpp.Primary_Country__c = '';
            parentOpp.Primary_Zip_Postal_Code__c = '';
        }
        
        update parentOpps;
    }
    
    //Check for recursion
    public static boolean runOnce(){
        if (run){
            run = false;
            return true;
        } else {
            return run;
        }
    }
}