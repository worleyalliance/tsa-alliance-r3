/**************************************************************************************
Name: Groups_ControllerTest
Version: 
Created Date: 
Function: Test class for Groups_ControllerTest

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    7/25/2017        Changes made to accomodate new update to Controller re: DE165,DE164,US718
*************************************************************************************/
@isTest
private class Groups_ControllerTest{

    static testMethod void test_Groups() {

       Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       
       AT_Group__c ATgrp = new AT_Group__c();
       ATgrp.Name = 'testATgrp';
       insert ATgrp;
       
       Group grp = new Group();
       grp.Name = 'testGrp';
       grp.DoesIncludeBosses = false;
       grp.DoesSendEmailToMembers = false;
       insert grp;
       
       system.debug('grp.id=' + grp.Id);
       Library__c lib = new Library__c();
       lib.Name = 'testLib';
       lib.cb_active__c = true;
       lib.dt_submittal_date__c = System.today();
       lib.lu_group__c = ATgrp.Id;
       if(recordTypeMap.containsKey('Success Story')) 
       {
           lib.RecordTypeId= recordTypeMap.get('Success Story').getRecordTypeId();
       }
       insert lib;
       
       system.debug('library.id='+lib.Id);
       ApexPages.currentPage().getParameters().put('Id', ATgrp.Id);
           
       LDT_Lightning_Filter__c grpsetting = new LDT_Lightning_Filter__c();
       grpsetting.Name = 'Test Setting';
       grpsetting.Label_del__c = 'Success Story';
       grpsetting.Object_API__c = 'AT_Group__c';
       grpsetting.OBJ_RecordType_API_Name__c = 'None';
       grpsetting.Coverage__c = 'Include';
       grpsetting.Active__c = true;
       insert grpsetting;
           
       List<Library__c> l = Groups_Controller.getTOsOfProject(ATgrp.Id);
       List <String> m = Groups_Controller.fetchRecordTypeValues();
       List <Library__c> n = Groups_Controller.getCBAsOfProjectwRecordType(ATgrp.Id, 'All');
       List <Library__c> o = Groups_Controller.getCBAsOfProjectwRecordType(ATgrp.Id, 'Success Story');
       Test.StartTest();
           system.assertequals(1, 1);
       Test.StopTest();
       }
}