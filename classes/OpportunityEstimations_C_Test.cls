/**************************************************************************************
Name: OpportunityEstimations_C_Test
Version: 1.0 
Created Date: 29.02.2017
Function: Unit test class for OpportunityEstimations_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04.11.2017         Original Version
*************************************************************************************/
@isTest
private class OpportunityEstimations_C_Test {

    @isTest
    private static void shouldRetireveEstimaitonsForOpportunity() {
        //when
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();

        Decimal go = 10;
        Decimal get = 12;
        Decimal tic = 13;

        Opportunity opp =
                oppBuilder
                        .build()
                        .withAccount(acc.Id)
                        .withGet(get)
                        .withGo(go)
                        .withTotalInstalledCost(tic)
                        .withLobBu()
                        .save()
                        .getOpportunity();

        //when
        OpportunityEstimations_Service.Estimation estimation = OpportunityEstimations_C.getEstimation(opp.Id);

        //then
        System.assertEquals(go, estimation.go);
        System.assertEquals(get, estimation.get);
        System.assertEquals(tic, estimation.tic);
    }

    @isTest
    private static void shouldSaveEstimaitonsForOpportunity() {
        //when
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();

        Decimal go = 10;
        Decimal get = 12;
        Decimal tic = 13;

        Opportunity opp =
                oppBuilder
                        .build()
                        .withAccount(acc.Id)
                        .withLobBu()
                        .save()
                        .getOpportunity();

        //when
        OpportunityEstimations_Service.Estimation estimation = new OpportunityEstimations_Service.Estimation();
        estimation.go = go;
        estimation.get = get;
        estimation.tic = tic;
        OpportunityEstimations_Service.Estimation result = OpportunityEstimations_C.saveEstimations(opp.Id, JSON.serialize(estimation));

        //then
        System.assertEquals(go, result.go);
        System.assertEquals(get, result.get);
        System.assertEquals(tic, result.tic);
    }

    @isTest
    private static void shouldRetrieveForecastCategories() {
        //when
        List<BaseComponent_Service.PicklistValue> resutls = OpportunityEstimations_C.getForecastCategories();

        //then
        System.assertNotEquals(0, resutls.size());
    }

    @isTest
    private static void shouldRetrieveClasses() {
        //when
        List<BaseComponent_Service.PicklistValue> resutls = OpportunityEstimations_C.getClasses();

        //then
        System.assertNotEquals(0, resutls.size());
    }

    @isTest
    private static void shouldVerifyIfItsClassDriven() {
        //when
        Boolean resutls = OpportunityEstimations_C.isClassDriven();

        //then
        System.assertEquals(Opportunity.Opportunity_Class_A_T__c.getDescribe().isUpdateable(), resutls);
    }
}