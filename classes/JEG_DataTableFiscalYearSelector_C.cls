/**************************************************************************************
Name: JEG_DataTableFiscalYearSelector_Service
Version: 1.0 
Created Date: 07.05.2017
Function: Controller for fiscal year selector component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.05.2017         Original Version
*************************************************************************************/
public with sharing class JEG_DataTableFiscalYearSelector_C {
    private static JEG_DataTableFiscalYearSelector_Service service = new JEG_DataTableFiscalYearSelector_Service();

   /*
    * Method retrieves Fiscal years to be found in given group attition records for given group
    * Query retrieves AT_Segment_Attrition__c to find fiscal years present.
    * Set is used to make list uniqe
    */
    @AuraEnabled
    public static List<Integer> getFiscalYears(Id groupId, String fiscalYearField, String relationField, String sObjectName){
        return service.getFiscalYears(groupId, fiscalYearField, relationField, sObjectName);
    }
}