/**************************************************************************************
Name: SiteLocation_TriggerTest
Version: 1.0 
Created Date: 04.20.2017
Function: To Test SiteLocation_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni    04.20.2017         Original Version
* Madhu Shetty    08.11.2017      (US654) - Added validation to block deletion of Account if an associated 
                                               Opportunity exists for that Account.
* Scott Stone     09.11.2018        Verify that an Opportunity's Billing State and Country can be set to any value allowed on its Location.
*************************************************************************************/


@IsTest
private class SiteLocation_TriggerTest {
    
    private final static String SKIP_VALUE = '';
    
    @IsTest
    private static void testUniquePrimary() {

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();

        List<SiteLocation__c> slList = new List<SiteLocation__c >();
        for (integer i = 0; i < 40; i++) {
            TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
            SiteLocation__c sl = locationBuilder.build().withAccount(acc1.Id).getRecord();
            slList.add(sl);
        }

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c sl1 = locationBuilder.build().withAccount(acc1.Id).isPrimary().getRecord();
        slList.add(sl1);

        Test.startTest();
        insert slList;
        Test.stopTest();

        Integer primaryCount = [SELECT Count() FROM SiteLocation__c WHERE Account__c = :acc1.id and Primary__c = true];
        system.assertEquals(1, primaryCount);

    }

    @IsTest
    private static void cantDeletePrimary() {

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c sl1 = locationBuilder.build().withAccount(acc1.Id).isPrimary().save().getRecord();

        try {
            delete sl1;
        } catch (Exception e) {
            e.setMessage(System.Label.LocationDeletionError);
            system.assertEquals(System.Label.LocationDeletionError, e.getMessage());
        }


    }

    @IsTest
    private static void canDeleteNonPrimary() {

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c sl1 = locationBuilder.build().withAccount(acc1.Id).save().getRecord();

        try {
            delete sl1;
        } catch (Exception e) {
            e.setMessage(System.Label.LocationDeletionError);
            system.assertEquals(System.Label.LocationDeletionError, e.getMessage());
        }


    }


     @IsTest
    private static void shouldRestrictDeleteIfOpportunityExists() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c location = locationBuilder.build().withAccount(acc.Id).save().getRecord();

        //TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        //Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //NewOpportunityLocation_C.saveOpportunityLocation(opp.Id, location.Id);

        try {
            delete location;
        } catch (Exception e) {
            e.setMessage(System.Label.OpportunityExistsForLocation);
            system.assertEquals(System.Label.OpportunityExistsForLocation, e.getMessage());
        }
    }
    
    //US2352 - add test to verify that any country + state that can be set on a location can also be set on the Opportunity primary Country + State
    //SWS - this test fails because it needs to create / edit too many opportunities, and the automation that is triggered on create / edit of opportunities
    //causes the test to exceed SOQL / Apex CPU limits.  Commenting out the test for now.
    //
    //@IsTest
    private static void shouldBeAbleToSetAnyLocationStateProvinceCodeOnOpportunity() {
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();

        Opportunity parentOpp = opportunityBuilder.build().save().getOpportunity();
        
        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        List<Opportunity> opportunities = new List<Opportunity>();
        
        system.debug('getting dependencies');
        FieldDescribeUtil.PicklistDependency stateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(SiteLocation__c.State_Province__c, FieldDescribeUtil.ResultType.LABEL, SiteLocation__c.Country__c, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});

        FieldDescribeUtil.PicklistDependency usStateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(User.statecode, FieldDescribeUtil.ResultType.VALUE, User.Countrycode, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});
        system.debug('got dependencies');        
        List<Schema.RecordTypeInfo> opportunityRecordTypes = Schema.SObjectType.Opportunity.getRecordTypeInfos();
        Id bookingValueRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Booking Value').getRecordTypeId();
        
        Integer max = 4;
        Integer count = 0;
        for(string country : stateDependency.dependentByControling.keySet()){
            
            if(count >= max){
                break;
            }
            
            
             
            FieldDescribeUtil.PicklistDependency dependency = stateDependency;
            if(country == 'US') {
                dependency = usStateDependency;
                system.debug('using US dependencies');
            }
            
            
            
            for(string state : dependency.dependentByControling.get(country)){
                
                if(count >= max){
                    break;
                }
                
                system.debug('c=' + country + ' s=' + state);
                
                //for(Schema.RecordTypeInfo recordType : opportunityRecordTypes){
                Opportunity opp = opportunityBuilder.build().withParent(parentOpp.Id).withPrimaryCountry(country).withPrimaryState(state).getOpportunity();
                opp.RecordTypeId = bookingValueRecordTypeId;
                opp.Auto_Create_Contract_Program__c = 'No';
                
                try {
                    insert opp;
                } catch (DMLException e) {
                    string failedFields = '';
                    for(Integer i = 0; i < e.getNumDml(); i++ ){
                        Opportunity failedOpportunity = opp;
                        failedFields = failedFields + ' c=' + failedOpportunity.Primary_Country__c + ' s=' + failedOpportunity.Primary_State_Province__c + ' recType=' + failedOpportunity.RecordTypeId + ' |';
                    }
                    throw new DMLException('error adding opportunity with' + failedFields + '. ' + e.getMessage(), e);
                }
                
                count = count + 1;
                //}
                
            }
        }
    }
    
    //US2352 - add test to verify that any country + state that can be set on a location can also be set on the Opportunity primary Country + State
    //US2352 SWS commented out @IsTest so that this does not run automatically.  The country state checks create a lot of test data and can exceed APEX CPU Limits if they're run simulatenously.  
    //You may still run manually, by uncommenting @IsTest and adding the test to a new Test Run.  But please run only one country / state test per test run.
    //@IsTest
    private static void shouldBeAbleToSetAnyLocationStateProvinceCodeOnAccount() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();

        List<Account> accounts = new List<Account>();
        
        FieldDescribeUtil.PicklistDependency stateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(SiteLocation__c.State_Province__c, FieldDescribeUtil.ResultType.VALUE, SiteLocation__c.Country__c, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});

        FieldDescribeUtil.PicklistDependency usStateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(User.statecode, FieldDescribeUtil.ResultType.VALUE, User.Countrycode, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});

        for(string country : stateDependency.dependentByControling.keySet()){
            
            FieldDescribeUtil.PicklistDependency dependency = stateDependency;
            if(country == 'US') {
                dependency = usStateDependency;
            }      
            
            for(string state : dependency.dependentByControling.get(country)){
                
                system.debug('c=' + country + ' s=' + state);
                
                Account acc = accountBuilder.build().getRecord();
                acc.BillingCountryCode = country;
                acc.BillingCountry = null;
                acc.BillingStreet = null; //blanking this out to prevent Account trigger that creates locations from slowing down test
                
                if(country == 'US'){
                    acc.BillingStateCode = state;
                    acc.BillingState = null;
                } else {
                    acc.BillingState = state;    
                }
                
                accounts.add(acc);    
                
            }
        }
                
        try {
            Test.startTest();
            insert accounts;  
            Test.stopTest();
        } catch (DMLException e) {
            string failedFields = '';
            for(Integer i = 0; i < e.getNumDml(); i++ ){
                Account failedAccount = accounts.get(e.getDmlIndex(i));
                failedFields = failedFields + ' c=' + failedAccount.BillingCountryCode + ' s=' + failedAccount.BillingState + ' |';
            }
            throw new DMLException('error adding account with' + failedFields + '. ' + e.getMessage(), e);
        }
    }
    
    //US2352 - add test to verify that any country + state that can be set on a location can also be set on the Opportunity primary Country + State
    //US2352 SWS commented out @IsTest so that this does not run automatically.  The country state checks create a lot of test data and can exceed APEX CPU Limits if they're run simulatenously.  
    //You may still run manually, by uncommenting @IsTest and adding the test to a new Test Run.  But please run only one country / state test per test run.
    //@IsTest 
    private static void shouldBeAbleToSetAnyLocationStateProvinceCodeOnPerformingLocation() {
        TestHelper.PerformingLocationBuilder performingLocationBuilder = new TestHelper.PerformingLocationBuilder();

        List<Jacobs_Location__c> performingLocations = new List<Jacobs_Location__c>();
        
        FieldDescribeUtil.PicklistDependency stateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(SiteLocation__c.State_Province__c, FieldDescribeUtil.ResultType.VALUE, SiteLocation__c.Country__c, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});

        FieldDescribeUtil.PicklistDependency usStateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(User.statecode, FieldDescribeUtil.ResultType.VALUE, User.Countrycode, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});

        for(string country : stateDependency.dependentByControling.keySet()){
            
            FieldDescribeUtil.PicklistDependency dependency = stateDependency;
            if(country == 'US') {
                dependency = usStateDependency;
            }      
            
            for(string state : dependency.dependentByControling.get(country)){
                
                system.debug('c=' + country + ' s=' + state);
                
                Jacobs_Location__c loc = performingLocationBuilder.build().withCountry(country).withStateProvince(state).getRecord();      
                performingLocations.add(loc);    
                
            }
        }
                
        try {
            Test.startTest();
            insert performingLocations;  
            Test.stopTest();
        } catch (DMLException e) {
            string failedFields = '';
            for(Integer i = 0; i < e.getNumDml(); i++ ){
                Jacobs_Location__c failedLocation = performingLocations.get(e.getDmlIndex(i));
                failedFields = failedFields + ' c=' + failedLocation.Country__c + ' s=' + failedLocation.State_Province__c + ' |';
            }
            throw new DMLException('error adding performing location with' + failedFields + '. ' + e.getMessage(), e);
        }
    }
}