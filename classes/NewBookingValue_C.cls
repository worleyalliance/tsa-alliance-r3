/**************************************************************************************
Name:NewBookingValue_C
Version: 1.0 
Created Date: 03/24/2018
Function: Creates a new bookng for a parent opp

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
*Pradeep Shetty     03/24/2018      Original Version
* Pradeep Shetty    06/13/2018      US2179: Added Pursuit Type field to the layout
* Pradeep Shetty    07/07/2018      US2270: Add Selling Unit and Performance Unit
* Pradeep Shetty    09/02/2018      US2327: Add methods to support Booking Value Schedule creation
*************************************************************************************/
public with sharing class NewBookingValue_C {
  
  private static NewBookingValue_Service service = new NewBookingValue_Service();

  /*
  *  Retrieves picklist values for Scope of Services
  */
  @AuraEnabled
  public static List<BaseComponent_Service.PicklistValue> getScopeOfServices(){
      return service.getScopeOfServices();
  }

  /*
  *  Retrieves picklist values for Stages
  */
  @AuraEnabled
  public static List<BaseComponent_Service.PicklistValue> getStages(){
      return service.getStages();
  }    

  //Start: US2179
  /*
  *  Retrieves picklist values for Stages
  */
  @AuraEnabled
  public static List<BaseComponent_Service.PicklistValue> getPursuitTypes(){
      return service.getPursuitTypes();
  }
  //End: US2179

  //Start: US2270
  /*
  *  Retrieves picklist values for Stages
  */
  @AuraEnabled
  public static List<BaseComponent_Service.PicklistValue> getSellingUnits(){
      return service.getSellingUnits();
  }
  //End: US2270   

  /*
  *   Gets helptext for given object's fields
  */
  @AuraEnabled
  public static Map<String, String> getOpportunityFieldsHelpText() {
      return service.getOpportunityFieldsHelpText();
  }  

  /*
  *   Gets helptext for given object's fields
  */
  @AuraEnabled
  public static Map<String, String> getOpportunityFieldsLabel() {
      return service.getOpportunityFieldsLabel();
  }   
  
  /*
  * Create booking value based on the Opportunity information provided by lightning component
  */
  @AuraEnabled
  public static List<Opportunity> createBookingValue(List<Opportunity> opp){
      return service.createBookingValue(opp);
  }   

  //Start: US2270    
  /*
  * Check if Performance unit is active
  */
  @AuraEnabled
  public static Boolean checkPerformanceUnitState(String unitValue){
      return service.checkPerformanceUnitState(unitValue);
  }     
  //End: US2270 

  /*
  *   US2327: Gets Column headers for datatable
  */
  @AuraEnabled
  public static List<BaseComponent_Service.DataTableColumn> getColumnHeaders() {
      return service.getColumnHeaders();
  }
  /*
  *   US2327: Gets row data for data table
  */
  @AuraEnabled
  public static List<Opportunity> getBookingValues(Opportunity bvParameters, Integer duration, String frequency) {
      return service.getBookingValues(bvParameters, duration, frequency);
  } 
}