/**************************************************************************************
Name: ScheduleMilestoneChart_Service
Version: 1.0 
Created Date: 16.03.2017
Function: Service data supplier for ScheduleMilestone Lightning Component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           16.03.2017         Original Version
* Chris Cornejo             9/5/2018           US2404 - Schedule Milestone: Visual Status on Chart
* Scott Stone				03/19/2019			DE928 - Modify the structure of Tasks & milestones to allow some milestones to be grouped by task.
*************************************************************************************/
public class ScheduleMilestoneChart_Service extends BaseComponent_Service{

    private static String ONE_DAY_STATUS = 'oneDay';
    private static String MULTI_DAY_STATUS = 'multiDay';
    
   /*
    * Method retrieves Milestone records for opportunity or account by deliverd record Id
    * Retrieved data is wrapped with custom class
    * Status will represent color on the chart and is set based on length of milestone
    */
    public List<ScheduleMilestoneChart_Service.Task> getMilestones(
            Id parentId,
            String objectTypeName,
            String relationFieldName,
            String startDateFieldName,
            String endDateFieldName,
            String typeFieldName,
            //US2404 - pulling status
            String statusFieldName
    ){
        validateObjectAccesible(Schema.getGlobalDescribe().get(objectTypeName).getDescribe().getName());

        //build query based on delivered parameters
        //US2404 - added status to query
        String query = 'SELECT ' +startDateFieldName + ',' + endDateFieldName +',' +typeFieldName + ',' + statusFieldName +
                ' FROM '+objectTypeName +
                (String.isNotEmpty(relationFieldName) ? ' WHERE '+relationFieldName + '= \''+parentId+'\'' : '')+
                ' ORDER BY '+startDateFieldName;

        System.debug('query: '+query);
        List<ScheduleMilestoneChart_Service.Task> tasks = new List<ScheduleMilestoneChart_Service.Task>();

        //Execute query on DB
        List<sObject> records = Database.query(query);

        //Prepare wrapper list. Status is based on the length of milestone
        for(sObject record : records){
            ScheduleMilestoneChart_Service.Milestone m = new ScheduleMilestoneChart_Service.Milestone();
            
            String taskName = (String) record.get(typeFieldName);
            
            //DE928 - group Call Milestones on row called Calls
            if(taskName == 'Call'){
                taskName = 'Calls';
            }
            
            ScheduleMilestoneChart_Service.Task t = getOrAddTask(tasks, taskName);
            m.startDate = (Date) record.get(startDateFieldName);
            m.endDate   = (Date) record.get(endDateFieldName);
            t.taskName = taskName;
            //US2404 - commented out per story
            //t.status   = t.startDate == t.endDate ? ONE_DAY_STATUS : MULTI_DAY_STATUS;
            //US2404 - get status of record
            m.status   = (String) record.get(statusFieldName);
            t.milestones.add(m);
        }
        return tasks;
    }

    //this function adds a new task to the task list or gets an existing one if milestones should be grouped by task.
    private ScheduleMilestoneChart_Service.Task getOrAddTask(List<ScheduleMilestoneChart_Service.Task> tasks, String taskName){
        
        //for Calls, group all the milestones in a Task.  Find the existing task and return it.
        if(taskName == 'Calls'){
            for(ScheduleMilestoneChart_Service.Task t : tasks){
                if(t.taskName == taskName){
                    return t;
                }
            }
        }

	    //for everything else, return a new task.  they aren't grouped.    
        ScheduleMilestoneChart_Service.Task newTask = new ScheduleMilestoneChart_Service.Task();
        newTask.milestones = new List<ScheduleMilestoneChart_Service.Milestone>();   
        tasks.add(newTask);
        return newTask;
    }
    
	//DE928 - SWS - Create Milestone object to hold milestones for a specific type of task
    public class Task{

        @AuraEnabled
        public String taskName {get; set;}

        @AuraEnabled
        public List<Milestone> milestones {get;set;}

    }
    
    /*
    * Custom class which wrapps significant fields used to generate charts
    * Start and End dates are used on x axis and tasks names on y axis
    * Status is used to change color or shape of the element
    */
    public class Milestone {
        @AuraEnabled
        public Date startDate {get; set;}

        @AuraEnabled
        public Date endDate {get; set;}
        
        @AuraEnabled
        public String status {get; set;}
        
        //US2404 - pulling status
        @AuraEnabled
        public String statusType {get; set;}
    }
}