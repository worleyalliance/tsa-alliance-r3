/**************************************************************************************
Name: ScheduleMilestone_TriggerTest
Version: 1.1
Created Date: 04/18/2017
Function: Test Class for Schedule Milestone Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni    	04/18/2017      Original Version
* Manuel Johnson  	24/07/2017		Update with tests for Milestone roll ups (US725)
* Madhu Shetty  	15/05/2018      Added test method to check validation that checks only one Award Milestone is added for each opportunity (US1892)
* Madhu Shetty  	20/08/2018      Fixed both test methods to execute without an error (DE635)
* Madhu Shetty  	10/09/2018      Added validation to check user is not able to create Target Close date, Project Close date and 
									Client Visit date. Also, users should not be able to delete auto created milestones (US2467)
*************************************************************************************/
@isTest
private class Schedule_Milestone_TriggerTest {
 
    @isTest 
    static void milestoneRollupsSetToMaxDate() {
        //given Milestone Roll ups are defined and an Opportunity has multiple Schedule Milestones
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder(); 
        User u = userBuilder.build().withLobBuSu().save().getRecord();
        
        System.runAs (u) {
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
            
            Test.startTest();   
            TestHelper.MilestoneBuilder smBuilder = new TestHelper.MilestoneBuilder(); 
            
            //Closed Schedule Milestone
            Schedule_Milestone__c closedSM = smBuilder.build().withOpportunity(opp.id).withEndDate(date.Today() - 1)
                .withStatus('Closed').withType('RFP').withAssignedTo(u.Id).save().getRecord();
            
            //Earliest Schedule Milestone
            Schedule_Milestone__c earlySM = smBuilder.build().withOpportunity(opp.id).withEndDate(date.Today())
                .withStatus('Upcoming').withType('RFP').withAssignedTo(u.Id).save().getRecord();
            
            //Next earliest Schedule Milestone
            Schedule_Milestone__c nextSM = smBuilder.build().withOpportunity(opp.id).withEndDate(date.Today() + 1)
                .withStatus('Upcoming').withType('RFP').withAssignedTo(u.Id).save().getRecord();
            
            //Last Schedule Milestone
            Schedule_Milestone__c lastSM = smBuilder.build().withOpportunity(opp.id).withEndDate(date.Today() + 2)
                .withStatus('Upcoming').withType('RFP').withAssignedTo(u.Id).save().getRecord();
            
            //then Opportunity OCI Case Status should be set to 'Open' since another Case exists
            System.assertNotEquals(earlySM.End_Date__c, [SELECT RFP_Date__c from Opportunity where Id = :opp.Id].RFP_Date__c);
            
            //given the earliest Milestone is closed
            earlySM.Status__c = 'Closed';
            update earlySM;
            
            //then the RFP Date should update to the next earliest Schedule Milestone
            System.assertNotEquals(nextSM.End_Date__c, [SELECT RFP_Date__c from Opportunity where Id = :opp.Id].RFP_Date__c);
            
            //given the next earliest Milestone is deleted
            delete nextSM;
            
            //then the RFP Date should update to the next earliest Schedule Milestone
            System.assertEquals(lastSM.End_Date__c, [SELECT RFP_Date__c from Opportunity where Id = :opp.Id].RFP_Date__c);
            
            //given the last open Milestone is deleted
            delete lastSM;
            
            //then the RFP Date should update to last closed
            System.assertNotEquals(closedSM.End_Date__c, [SELECT RFP_Date__c from Opportunity where Id = :opp.Id].RFP_Date__c);
            
            //given that all Milestones are deleted
            delete closedSM;
            delete earlySM;
            
            //then the RFP Date should update to null
            System.assertEquals(null, [SELECT RFP_Date__c from Opportunity where Id = :opp.Id].RFP_Date__c);
            
            
            Test.stopTest();
        }
    }

    @isTest 
    static void OnlyOneAwardMilestoneforOpportunity() {
		TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
		Account acc = accountBuilder.build().save().getRecord();
		
		Opportunity opp;
		//Create Test Opportunity with a specific Selling Unit
		TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
		opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //Create Award type Milestone
		TestHelper.MilestoneBuilder milestoneBuilder = new TestHelper.MilestoneBuilder();
        Schedule_Milestone__c awmilestone =
                milestoneBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .withType('Award')
                        .withStartDate(Date.today().addDays(1))
                        .withEndDate(Date.today().addDays(3))
                        .save()
                        .getRecord();
                        
        //Check if more than one Award type Milestone can be created
		try {
			Schedule_Milestone__c awmilestone1 =
	                milestoneBuilder
	                        .build()
	                        .withOpportunity(opp.Id)
	                        .withType('Award')
	                        .withStartDate(Date.today().addDays(1))
	                        .withEndDate(Date.today().addDays(3))
	                        .save()
	                        .getRecord();
        } catch (Exception e) {
            system.assertEquals(e.getMessage().contains(string.format(System.Label.Milestone_Type_Already_Exists, new string[] {'Award'})), true);
        }

        //Create Proposal Due type Milestone
		Schedule_Milestone__c pdmilestone =
                milestoneBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .withType('Proposal Due')
                        .withStartDate(Date.today().addDays(1))
                        .withEndDate(Date.today().addDays(3))
                        .save()
                        .getRecord();
                        
        //Check if more than one Proposal Due type Milestone can be created
		try {
			Schedule_Milestone__c pdmilestone1 =
	                milestoneBuilder
	                        .build()
	                        .withOpportunity(opp.Id)
	                        .withType('Proposal Due')
	                        .withStartDate(Date.today().addDays(1))
	                        .withEndDate(Date.today().addDays(3))
	                        .save()
	                        .getRecord();
        } catch (Exception e) {
            system.assertEquals(e.getMessage().contains(string.format(System.Label.Milestone_Type_Already_Exists, new string[] {'Proposal Due'})), true);
        }

    }
    
    @IsTest
    //make sure that auto-created milestones get a record type of Read Only Milestone
    static void autoCreatedMilestonesShouldBeReadOnly(){
        
        RecordType readonlyRecordType = Utility.getRecordTypes('Schedule_Milestone__c', new List<string>{'Schedule_Milestone_Read_Only'}).get(0);
        
        TestHelper.MilestoneBuilder smBuilder = new TestHelper.MilestoneBuilder();        
        Schedule_Milestone__c milestone = smBuilder.build().withAutoCreated(true).getRecord();                
        Schedule_Milestone_TriggerHandler handler = new Schedule_Milestone_TriggerHandler();
        
        Test.startTest();  
        handler.onBeforeInsert(new List<Schedule_Milestone__c>{milestone});
        Test.stopTest();
        
        system.assertEquals(readonlyRecordType.Id, milestone.RecordTypeId);
    }
    
    @IsTest
    //make sure that auto-created milestones cannot be deleted
    static void autoCreatedMilestonesCannotBeDeleted(){
        
        TestHelper.MilestoneBuilder smBuilder = new TestHelper.MilestoneBuilder();        
        Schedule_Milestone__c milestone = smBuilder.build().withAutoCreated(true).save().getRecord();                
        Schedule_Milestone_TriggerHandler handler = new Schedule_Milestone_TriggerHandler();
        
        try {
            Test.startTest();
            delete milestone;
            Test.stopTest();
        } catch(Exception e){
            system.assertEquals(Label.CannotDeleteAutoCreatedMilestones, e.getMessage());
        }        
    }
    
    @IsTest
    //make sure that user-created milestones can be deleted
    static void userCreatedMilestonesCanBeDeleted(){
        
        TestHelper.MilestoneBuilder smBuilder = new TestHelper.MilestoneBuilder();        
        Schedule_Milestone__c milestone = smBuilder.build().withAutoCreated(false).save().getRecord();                
        Schedule_Milestone_TriggerHandler handler = new Schedule_Milestone_TriggerHandler();
        
        Id milestoneId = milestone.Id;
        
        Test.startTest();
        delete milestone;
        Test.stopTest();
        
        
        Integer count = Database.countQuery('SELECT Count() from Schedule_Milestone__c where Id = :milestoneId');
        System.assertEquals(0, count);
        
    }
}