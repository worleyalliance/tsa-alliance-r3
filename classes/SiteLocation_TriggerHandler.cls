/**************************************************************************************
Name: SiteLocation_TriggerHandler
Version: 1.2
Created Date: 04/20/2017
Function: Handler for SiteLocation_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04/20/2017      Original Version
* Madhu Shetty      08/11/2017      (US654) - Added validation to block deletion of Account if an associated 
                                    Opportunity exists for that Account.
* Pradeep Shetty    09/06/2017      US782 - Trigger Location callout on Name change
* Manuel Johnson    08/03/2018      US1789 - Moved primary checkbox logic to utility
* Scott Stone       09/10/2018      US2352 - bulkify location callout.
*************************************************************************************/
public class SiteLocation_TriggerHandler {
    
    public final static String OPERATING_UNIT_US = 'US_OU';
    private static SiteLocation_TriggerHandler handler;
    // private static boolean run = true;
    
    /*
    * Singleton like pattern
    */
    public static SiteLocation_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new SiteLocation_TriggerHandler();
        }
        return handler;
    }
    
    /*
    * Actions performed before the account is inserted
    */
    public void onBeforeInsert(List<SiteLocation__c> locations) {
        populateOperatingUnit(locations);
    }
    
    /*
    * Actions performed before the account is updated
    */
    public void onBeforeUpdate(List<SiteLocation__c> locations) {
        populateOperatingUnit(locations);
    }  
    
    /*
    * Actions performed on Location records before delete
    */
    public void onBeforeDelete(List<SiteLocation__c> delLocation) {
        throwValidation(delLocation);
        //Restrict deletion of Location if an Opportunity is associated with Location.
        restrictDeleteIfOpportunityExists(delLocation);
    }
    
    /*
    * Actions performed on Location records after upsert
    */
    public void onAfterUpsert(List<SiteLocation__c> trgNew, List<SiteLocation__c> trgOld, Boolean isInsert, Boolean isUpdate) {
        Utility.uniqueFieldAcrossChildren(trgNew, 'Account__c', 'Primary__c');
        if (!isInsert) updateOppPrimaryLocationFields(trgNew, trgOld);
        makeLocationCallout(trgNew, trgOld, isInsert);
    }
    
    /*
    * Populate operating unit before the account is inserted based on custom settings
    */
    private void populateOperatingUnit(List<SiteLocation__c> locations) {
        Map<String, Country_Operating_Unit_Mapping__c> cou = Country_Operating_Unit_Mapping__c.getAll();
        Map<string, string> operatingUnitByCountryCode = new Map<string, string>();
        for (Country_Operating_Unit_Mapping__c co : cou.values()) {
            operatingUnitByCountryCode.put(co.Country_Code__c, co.Operating_Unit__c);
        }
        
        for (SiteLocation__c loc: locations) {
            if (String.isBlank(loc.Operating_Unit__c) && loc.Address_ID__c == null) {
                if (operatingUnitByCountryCode.containsKey(loc.Country__c) ) {
                    loc.Operating_Unit__c = operatingUnitByCountryCode.get(loc.Country__c);
                } else {
                    loc.Operating_Unit__c = OPERATING_UNIT_US;
                }
            }
        }
    }
    
    /*
    * Method to validate Primary location record is not deleted
    */
    private void throwValidation(List<SiteLocation__c> locationRecords) {
        for (SiteLocation__c sl: locationRecords) {
            if (sl.Primary__c == true) {
                sl.addError(System.Label.LocationDeletionError);
            }
        }
    }
    
    /*
    * Method to make a callout if a location has been created or updated, when that location is not primary
    */
    private void makeLocationCallout(List<SiteLocation__c> trgNew, List<SiteLocation__c> trgOld, Boolean isInsert) {
        List<SiteLocation__c> nonPrimaryLocations = new List<SiteLocation__c>();
        List<Id> locAccountId = new List<Id>();
        Map<SiteLocation__c, Id> locationAccountIdMap = new Map<SiteLocation__c, Id>();
        Map<SiteLocation__c, Account> locAccountMap = new Map<SiteLocation__c, Account>();
        
        for (Integer i = 0; i < trgNew.size(); i++) {

            if(
                ((isInsert 
                  || trgNew[i].Street_Address__c != trgOld[i].Street_Address__c
                  || trgNew[i].City__c           != trgOld[i].City__c
                  || trgNew[i].State_Province__c != trgOld[i].State_Province__c
                  || trgNew[i].Postal_Code__c    != trgOld[i].Postal_Code__c
                  || trgNew[i].Country__c        != trgOld[i].Country__c
                  || trgNew[i].Name              != trgOld[i].Name) //[PS 09/06 - US782 Added Name to enable callout when name is changed]
                 && trgNew[i].Primary__c        != true
                ) || 
                (!isInsert && 
                 trgNew[i].Primary__c == true && 
                 trgNew[i].Name != trgOld[i].Name
                )
            ) 
            {
                nonPrimaryLocations.add(trgNew[i]);
                locAccountId.add(trgNew[i].Account__c);
                locationAccountIdMap.put(trgNew[i], trgNew[i].Account__c);
            }
        //US2352 - bulkify this trigger to stop SOQL 101 errors.  close the for loop before querying for account Ids to reduce SOQL statements executed. SOQL already used IN condition
        }
        if (locAccountId != null && locAccountId.size() > 0) {
            
            Map<Id, Account> accountIdMap = new Map<Id, Account>([SELECT Id, 
                                                                  Name, 
                                                                  OracleAccountID__c,
                                                                  Operating_Unit__c ,
                                                                  Status__c,
                                                                  CH2M_Status__c,
                                                                  CH2M_Oracle_Account_ID__c,
                                                                  Account_Group__c,
                                                                  Acct_Group_ID_Name__c
                                                                  FROM Account 
                                                                  WHERE ID IN :locAccountId 
                                                                  AND OracleAccountID__c != null 
                                                                  AND Do_Not_Create_Oracle_Account__c=false]);
            for (SiteLocation__c loc : locationAccountIdMap.keySet()) {
                if (accountIdMap.get(locationAccountIdMap.get(loc)) != null) {
                    locAccountMap.put(loc, accountIdMap.get(locationAccountIdMap.get(loc)));
                }
            }
            
            for (SiteLocation__c loc: locAccountMap.keySet()) {
                If (!Test.isRunningTest()) {
                    AccountQueueableHandler job = new AccountQueueableHandler(locAccountMap.get(loc));
                    job.max = 3;
                    job.locationChange = TRUE;
                    job.location = loc;
                    ID jobID = System.enqueueJob(job);
                }
            }
        }
    }
    
    //Restrict deletion of Location if an Opportunity is associated with Location.
    private void restrictDeleteIfOpportunityExists(List<SiteLocation__c> delRecords){ 
        for (SiteLocation__c sl: delRecords) {
            List<Opportunity_Locations__c> allLocations = [SELECT Id
                                                           FROM Opportunity_Locations__c
                                                           WHERE Location__c = :sl.Id];
            
            if (!allLocations.isEmpty()) {
                sl.adderror(Label.OpportunityExistsForLocation);
            }
        }
    }
    
    /*
    * When Location is related to a primary Opportunity Location, update the Opportunity
    */
    private void updateOppPrimaryLocationFields(List<SiteLocation__c> locations, List<SiteLocation__c> oldLocations) {
        List<Opportunity> oppsUpdatePrimary = new List<Opportunity>();
        List<SiteLocation__c> changedLocations = new List<SiteLocation__c>();
        Map<Id, Opportunity_Locations__c> oppLocations = new Map<Id, Opportunity_Locations__c>();
        
        // Get list of all the Site Locations where the values have been modified
        for (Integer i = 0; i < locations.size(); i++) {
			
            if (locations[i].Street_Address__c != oldLocations[i].Street_Address__c ||
                locations[i].City__c != oldLocations[i].City__c ||
                locations[i].State_Province__c != oldLocations[i].State_Province__c ||
                locations[i].Country__c != oldLocations[i].Country__c ||
                locations[i].Postal_Code__c != oldLocations[i].Postal_Code__c) {
                    changedLocations.add(locations[i]);                  
                }
        }
        
        for (Opportunity_Locations__c oppLocation : [SELECT Opportunity__c, Location__r.Street_Address__c, Location__r.City__c, 
             								   Location__r.State_Province__c, Location__r.Country__c, Location__r.Postal_Code__c 
                                               FROM Opportunity_Locations__c WHERE Location__c IN : locations AND Primary__c = true]) {
        	oppLocations.put(oppLocation.Opportunity__c, oppLocation);
        }

        for (Opportunity parentOpp : [SELECT Id FROM Opportunity WHERE Id IN : oppLocations.keySet()]) {
            
            parentOpp.Primary_Street__c = oppLocations.get(parentOpp.Id).Location__r.Street_Address__c;
            parentOpp.Primary_City__c = oppLocations.get(parentOpp.Id).Location__r.City__c;
            parentOpp.Primary_State_Province__c = oppLocations.get(parentOpp.Id).Location__r.State_Province__c;
            parentOpp.Primary_Country__c = oppLocations.get(parentOpp.Id).Location__r.Country__c;
            parentOpp.Primary_Zip_Postal_Code__c = oppLocations.get(parentOpp.Id).Location__r.Postal_Code__c;
            oppsUpdatePrimary.add(parentOpp);
        }
        
        update oppsUpdatePrimary;
    }
    
    //Check for recursion
    /*public static boolean runOnce() {
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }*/
}