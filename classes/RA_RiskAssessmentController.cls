/**************************************************************************************
Name: RA_RiskAssessmentController
Version: 1.0 
Created Date: 08.19.2018
Function: Controller for the Risk Assessment

Modification Log:
**************************************************************************************
* Developer         Date            Description
**************************************************************************************
* Blake Poutra   	08/19/2018      Original Version
*************************************************************************************/
global with sharing class RA_RiskAssessmentController {
    
    @AuraEnabled
    public static Boolean hasWriteAccess(Id reviewId){
        
        UserRecordAccess recordAccess;
        try {
            recordAccess = [SELECT RecordId, HasEditAccess, HasAllAccess
                            FROM UserRecordAccess
                            WHERE UserId = :UserInfo.getUserId() AND RecordId = :reviewId
                            LIMIT 1];
            
        } catch(Exception ex){
            throw new AuraHandledException('Query Error');
        }
        
        if(recordAccess != null && (recordAccess.HasEditAccess || recordAccess.HasAllAccess)){
            return true;
        } else {
            return false;
        }
        
    }
    
    @AuraEnabled
    public static List<RA_Step__mdt> getRASteps(){
        
        List<RA_Step__mdt> returnSteps = [SELECT Id, MasterLabel, Step_Label__c, Component_Name__c, Sort_Order__c
                                          FROM RA_Step__mdt
                                          WHERE Active__c = true
                                          ORDER BY Sort_Order__c ASC];
        
        return returnSteps;
    }
    
    @AuraEnabled
    public static Review__c getReviewRec(String recordId){
        
        // Query for the questions related to active steps, in order to query for the relevant field names
        List<RA_Question__mdt> questions = [SELECT Id, Field_Name__c, Risk_Level_Condition__c, Additional_Condition__c
                                            FROM RA_Question__mdt
                                            WHERE RA_Step__r.Active__c = true];
        Set<String> fieldNames = new Set<String>();
        fieldNames.add('Id');
        fieldNames.add('RecordType.Name');
        fieldNames.add('Account__r.Name');
        fieldNames.add('Opportunity__r.Name');
        fieldNames.add('Risk_Categories_Selected__c');
        fieldNames.add('Revenue__c');
        fieldNames.add('Execution_Risk_Score__c');
        fieldNames.add('Opportunity_Risk_Score__c');
        fieldNames.add('Client_Risk_Score__c');
        fieldNames.add('Geography_Risk_Score__c');
        fieldNames.add('Commercial_Risk_Score__c');
        fieldNames.add('Contractual_Risk_Score__c');
        fieldNames.add('Opportunity_Risk_Category__c');
        fieldNames.add('Execution_Risk_Category__c');
        fieldNames.add('Client_Risk_Category__c');
        fieldNames.add('Geography_Risk_Category__c');
        fieldNames.add('Commercial_Risk_Category__c');
        fieldNames.add('Contractual_Risk_Category__c');
        fieldNames.add('Contract_Type_s__c');
        fieldNames.add('Service_Type_Code__c');
        fieldNames.add('Line_of_Business__c');
        fieldNames.add('Scope_of_Services__c');
        fieldNames.add('Contract_Award_Type__c');
        fieldNames.add('Total_Installed_Cost_TIC__c');
        fieldNames.add('Gross_Margin__c');
        fieldNames.add('Total_Requested_B_P__c');
        fieldNames.add('Execution_Risks__c');
        fieldNames.add('Client_Risks__c');
        fieldNames.add('Geography_Risks__c');
        fieldNames.add('Commercial_Risks__c');
        fieldNames.add('Contractual_Risks__c');
        fieldNames.add('Risk_Assessment__c');
        fieldNames.add('Project_Role__c');
        fieldNames.add('Review_Status__c');
        fieldNames.add('Flagged_Execution_Questions__c');
        fieldNames.add('Flagged_Client_Questions__c');
        fieldNames.add('Flagged_Geography_Questions__c');
        fieldNames.add('Flagged_Commercial_Questions__c');
        fieldNames.add('Flagged_Contractual_Questions__c');
        fieldNames.add('Risk_Assessment_Complete__c');
        for(RA_Question__mdt q : questions){
            fieldNames.add(q.Field_Name__c);
        }
        
        List<String> fieldNameList = new List<String>();
        fieldNameList.addAll(fieldNames);
        
        String fieldNameString = String.join(fieldNameList, ', ');
        String queryString = 'SELECT ' + fieldNameString + ' FROM Review__c WHERE Id = :recordId LIMIT 1';
        List<sObject> queryResult = Database.query(queryString);
        
        Review__c theReview = (Review__c)queryResult[0];
        System.debug('theReview: ' + theReview);
        
        return theReview;
        
    }
    
    @AuraEnabled
    public static String saveReviewRec(Review__c reviewRecord){
        
        Review__c theReview = (Review__c)reviewRecord;
        
        try{
            update theReview;
            
            return 'SUCCESS';
        } catch(exception ex){
            throw new AuraHandledException('ERROR ON SAVE: ' + ex.getMessage());
        }
        
    }
    
    @AuraEnabled
    public static List<QuestionsWrapper> getQuestions(String riskType, Review__c theReview){
        
        RA_Step__mdt step = [SELECT Id
                             FROM RA_Step__mdt
                             WHERE Step_Label__c = :riskType
                             LIMIT 1];
        
        List<RA_Question__mdt> questions = new List<RA_Question__mdt>();
        String recordTypeName = theReview.RecordType.Name;
        
        if(recordTypeName.contains('ECR')){
            questions = [SELECT Id, Field_Name__c, Sort_Order__c, Risk_Level_Condition__c, Additional_Condition__c
                         FROM RA_Question__mdt
                         WHERE RA_Step__c = :step.Id
                         AND Line_of_Business__c = 'ECR'
                         ORDER BY Sort_Order__c ASC];
        } else if(recordTypeName.contains('BIAF')){
            questions = [SELECT Id, Field_Name__c, Sort_Order__c, Risk_Level_Condition__c, Additional_Condition__c
                         FROM RA_Question__mdt
                         WHERE RA_Step__c = :step.Id
                         AND Line_of_Business__c = 'BIAF'
                         ORDER BY Sort_Order__c ASC];
        }
        
        List<QuestionsWrapper> returnList = new List<QuestionsWrapper>();
        
        // Get question info based on field API names
        // and create questionsWrappers for them
        // Manual loop through the list of metadata, incrementing by 2 instead of 1
        for(Integer i = 0; i < questions.size(); i += 2){
            // Get the info for the first question of this page
            fieldInfoWrapper q1 = new fieldInfoWrapper(questions[i].Field_Name__c, theReview);
            
            //  If there is a second question (even number of questions), get the info, and create
            //  the questionsWrapper, add it to the return list
            if(i + 1 < questions.size()){
                fieldInfoWrapper q2 = new fieldInfoWrapper(questions[i+1].Field_Name__c, theReview);
                returnList.add(new QuestionsWrapper(q1.label, q1.helpText, q1.ApiName, q1.options, (i + 1), q2.label, q2.helpText, q2.ApiName, q2.options, (i + 2)));  
            } else {
                //  If there is not a 2nd question (odd number of questions)
                //  create a new questionsWrapper with null for q2
                returnList.add(new QuestionsWrapper(q1.label, q1.helpText, q1.ApiName, q1.options, (i + 1), null, null, null, null, null)); 
            }
        }
        
        return returnList;
    }
    
    private class fieldInfoWrapper{
        
        public String label {get; set;}
        public String helpText {get; set;}
        public String ApiName {get; set;}
        public List<String> options {get; set;}
        
        public fieldInfoWrapper(String apiName, Review__c theReview){
            // Set the wrapper API name
            this.ApiName = apiName;
            
            String objName = 'Review__c';
            
            // Get the base schema info
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.SObjectType sObjType= gd.get(objName);
            Schema.DescribeSObjectResult describeResult = sObjType.getDescribe();
            
            // Get the helptext from the Schema result
            this.helpText = describeResult.fields.getMap().get(apiName).getDescribe().getInlineHelpText();
            // Get the field Label (question label) from the Schema Result
            this.label = describeResult.fields.getMap().get(apiName).getDescribe().getLabel();
            
            // Get the options from the Schema result, and process into a list of Strings
            List<String> theOptions = new List<String>();
            
            
            Http theHttp = new Http();
            HttpRequest req = new HttpRequest();
            String shost = Url.getOrgDomainUrl().toExternalForm();
            String url = 'callout:RA_Picklist_Values/services/data/v44.0/ui-api/object-info/Review__c/picklist-values/' + theReview.RecordTypeId + '/' + apiName;
            system.debug(url);
            system.debug(shost);
            req.setEndpoint(url);
            req.setMethod('GET');
            //req.setHeader('Authorization', 'Bearer '+UserInfo.getSessionId());
            HttpResponse response = theHttp.send(req);
            system.debug(response.getBody());
            Map<String, Object> returnMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            
            List<Map<String, Object>> valuesMaps = new List<Map<String, Object>>();
            List<Object> mapObjects = (List<Object>)returnMap.get('values');
            for(Object o : mapObjects){
                valuesMaps.add((Map<String, Object>)o);
            }
            
            for(Object o : valuesMaps){
                Map<String, Object> value = (Map<String, Object>)o;
                String theValue = (String)value.get('value');
                
                theValue = theValue.replaceAll('&lt;', '<');
                theValue = theValue.replaceAll('&gt;', '>');
                theValue = theValue.replaceAll('&amp;', '&');
                
                theOptions.add(theValue);
            }
            
            /*List<Schema.PicklistEntry> optionEntries = describeResult.fields.getMap().get(apiName).getDescribe().getPicklistValues();
            for(Schema.PicklistEntry pe : optionEntries){
                theOptions.add(pe.getValue());
            }*/
            this.options = theOptions;
        }
        
    }    
    public class QuestionsWrapper{
        
        @auraEnabled
        public String q1Label {get; set;}
        @auraEnabled
        public String q1Help {get; set;}
        @auraEnabled
        public String q1ApiName {get; set;}
        @auraEnabled
        public List<String> q1Options {get; set;}
        @auraEnabled
        public Integer q1Number {get; set;}
        @auraEnabled
        public String q2Label {get; set;}
        @auraEnabled
        public String q2Help {get; set;}
        @auraEnabled
        public String q2ApiName {get; set;}
        @auraEnabled
        public List<String> q2Options {get; set;}
        @auraEnabled
        public Integer q2Number {get; set;}
        
        public QuestionsWrapper(String q1Label, 
                                String q1Help, 
                                String q1ApiName, 
                                List<String> q1Options, 
                                Integer q1Number,
                                String q2Label, 
                                String q2Help, 
                                String q2ApiName, 
                                List<String> q2Options,
                                Integer q2Number){
                                    this.q1Label = q1Label;
                                    this.q1Help = q1Help;
                                    this.q1ApiName = q1ApiName;
                                    this.q1Options = q1Options;
                                    this.q1Number = q1Number;
                                    this.q2Label = q2Label;
                                    this.q2Help = q2Help;
                                    this.q2ApiName = q2ApiName;
                                    this.q2Options = q2Options;
                                    this.q2Number = q2Number;
                                }
        
    }
    
    @AuraEnabled
    public static Review__c updateOpportunityBrief(List<Id> reviewIds) {
        
        ReviewService.updateOpportunityBrief(reviewIds);
        
        Review__c updatedReview = [SELECT Id, Account__r.Name, Opportunity__r.Name, Line_of_Business__c, Contract_Type_s__c,
                                          Scope_of_Services__c, Service_Type_Code__c, Contract_Award_Type__c,
                                  		  Total_Installed_Cost_TIC__c, Revenue__c, Gross_Margin__c, Total_Requested_B_P__c, Review_Status__c
                                   FROM Review__c
                                   WHERE Id = :reviewIds[0]
                                   LIMIT 1];
        
        return updatedReview;
        
    }
    
    @auraEnabled
    public static RA_RiskAssessmentService.scoreWrapper getRiskScore(Review__c theReviewRec, Map<String, String> selectedCategories){
        
        return RA_RiskAssessmentService.calcRiskScore(theReviewRec, selectedCategories);
        
    }
    
    @auraEnabled
    public static List<summaryWrapper> getSummaryData(Review__c theReview, String sectionName){
        String stepName;
        
        switch on sectionName {
            when 'Execution Risk' {
                stepName = 'Step 300';
            }
            when 'Client Risk' {
                stepName = 'Step 400';
            }
            when 'Geography Risk' {
                stepName = 'Step 500';
            }
            when 'Commercial Risk' {
                stepName = 'Step 600';
            }
            when 'Contractual Risk' {
                stepName = 'Step 700';
            }
        }
        
        List<RA_Question__mdt> questionList;
        
        String recordTypeName = theReview.RecordType.Name;
        
        List<RA_Question__mdt> activeQuestions;
        if(recordTypeName.contains('ECR')){
            questionList = [SELECT Id, RA_Step__r.Step_Label__c, Risk_Level_Condition__c, 
                                      Additional_Condition__c, Field_Name__c, RA_Step__r.MasterLabel
                               FROM RA_Question__mdt
                               WHERE RA_Step__r.MasterLabel = :stepName
                               AND Line_of_Business__c = 'ECR'];
        } else if(recordTypeName.contains('BIAF')){
            questionList = [SELECT Id, RA_Step__r.Step_Label__c, Risk_Level_Condition__c, 
                                      Additional_Condition__c, Field_Name__c, RA_Step__r.MasterLabel
                               FROM RA_Question__mdt
                               WHERE RA_Step__r.MasterLabel = :stepName
                               AND Line_of_Business__c = 'BIAF'];
        }
        
        List<summaryWrapper> returnSummaries = new List<summaryWrapper>();
        
        // Iterate through each question in the list
        for(RA_Question__mdt q : questionList){
            
            // Initialize a variable to hold the current iteration's highest lever, for comparison later.
            String thisLevel;
            String thisCategory;
            
            // If there is a condition, and the field isn't empty, evaluate it.
            if(q.Risk_Level_Condition__c != null && theReview.get(q.Field_Name__c) != null){
                //testQuestions.add(q);
                
                // Get the name of the field to be evaluated
                String theValue = (String)theReview.get(q.Field_Name__c);
                
                // Now we need to deserialize and parse the JSON for processing.
                Map<String, Object> conditionMap = (Map<String, Object>)JSON.deserializeUntyped(q.Risk_Level_Condition__c);
                thisLevel = (String)conditionMap.get('else');
                if(thisLevel == 'R2'){
                    thisCategory = 'High';
                }
                
                //  If the question metadata has an "Additional Condition"
                if(q.Additional_Condition__c != null){
                    
                    // Keep processing JSON info for processing
                    Map<String, Object> additionalMap = (Map<String, Object>)JSON.deserializeUntyped(q.Additional_Condition__c);
                    List<Map<String, Object>> someMaps = new List<Map<String, Object>>();
                    List<Object> mapObjects = (List<Object>)additionalMap.get('additionalConditions');
                    for(Object o : mapObjects){
                        someMaps.add((Map<String, Object>)o);
                    }
                    
                    // Iterate through the resulting list of conditions to check
                    for(Object o : someMaps){
                        // Cast the condition to something actionable
                        Map<String, Object> condition = (Map<String, Object>)o;
                        
                        // Check to see if the json indicates checking against a string or a decimal
                        if(condition.get('isString') == true){
                            // Extract the relevent info for easy access in processing.
                            String fieldName = (String)condition.get('FieldName');
                            String testValue = condition.get('Value') != null ? (String)condition.get('Value') : null;
                            String answer = (String)condition.get('Answer');
                            String operator = (String)condition.get('Operator');
                            String riskLevel = (String)condition.get('RiskLevel');
                            String riskCategory = (String)condition.get('RiskCategory');
                            
                            if(fieldName.contains(';')){
                                List<Map<String, String>> conditionMaps = new List<Map<String, String>>();
                                List<String> fieldNameList = fieldName.split(';');
                                List<String> testValueList = testValue.split(';');
                                for(Integer i = 0; i < fieldNameList.size(); i++){
                                    Map<String, String> m = new Map<String, String>();
                                    m.put(fieldNameList[i], testValueList[i]);
                                    conditionMaps.add(m);
                                }
                                
                                Boolean conditionPass = true;
                                for(Map<String, String> m : conditionMaps){
                                    List<String> fieldList = new List<String>();
                                    fieldList.addAll(m.keySet());
                                    String field = fieldList[0];
                                    String value = m.get(field);
                                    
                                    if(theReview.get(q.Field_Name__c) == answer){
                                        switch on operator{
                                            when 'E'{
                                                if(theReview.get(field) != value){
                                                    conditionPass = false;
                                                }
                                            }
                                        }
                                    } else {
                                        // The question's answer doesn't match criteria to check the other questions' values
                                        conditionPass = false;
                                    }
                                }
                                if(conditionPass){
                                    thisLevel = riskLevel;
                                    thisCategory = riskCategory;
                                }
                                
                            } else {
                                
                                if(theReview.get(q.Field_Name__c) == answer){
                                    switch on operator{
                                        when 'E'{
                                            if(theReview.get(fieldName) == testValue){
                                                thisLevel = riskLevel;
                                                thisCategory = riskCategory;
                                            }
                                        }
                                    }
                                }
                            }
                            
                        } else {
                            // Extract the relevant info for easy access in processing.
                            String fieldName = (String)condition.get('FieldName');
                            Decimal testValue = condition.get('Value') != null ? Decimal.valueOf((String)condition.get('Value')) : null;
                            String answer = (String)condition.get('Answer');
                            String operator = (String)condition.get('Operator');
                            String riskLevel = (String)condition.get('RiskLevel');
                            String riskCategory = (String)condition.get('RiskCategory');
                            
                            // If the answer conditions is met, process the condition
                            if(theReview.get(q.Field_Name__c) == answer){
                                // If there is a value to verify against, check it, if not, assign the risk level.
                                if(testValue != null){
                                    // Refer to the operator value, to determine which kind of comparison to perform
                                    // If the conditions are met, replace the iteration's risk level with this condition's riskLevel
                                    switch on operator{
                                        when 'E'{
                                            if(theReview.get(fieldName) == testValue){
                                                thisLevel = riskLevel;
                                                thisCategory = riskCategory;
                                            }
                                        }
                                        when 'GE'{
                                            if((Decimal)theReview.get(fieldName) >= testValue){
                                                thisLevel = riskLevel;
                                                thisCategory = riskCategory;
                                            }
                                        }
                                        when 'LT'{
                                            if((Decimal)theReview.get(fieldName) < testValue){
                                                thisLevel = riskLevel;
                                                thisCategory = riskCategory;
                                            }
                                        }
                                    }
                                } else {
                                    thisLevel = riskLevel;
                                    thisCategory = riskCategory;
                                }
                            }
                        }
                    }
                    
                    // If there are no Additional Conditions, go with the else.
                } else {
                    thisLevel = (String)conditionMap.get('else');
                    thisCategory = 'Standard';
                }
            }
            
            if(thisLevel == null){
                thisLevel = '';
                thisCategory = '';
            } else if(thisLevel == 'R7' || thisLevel == 'R5'){
                thisCategory = 'Standard';
            }
            
            String objName = 'Review__c';
            
            // Get the base schema info
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.SObjectType sObjType= gd.get(objName);
            Schema.DescribeSObjectResult describeResult = sObjType.getDescribe();
            
            String question = describeResult.fields.getMap().get(q.Field_Name__c).getDescribe().getInlineHelpText();
            String response = (String)theReview.get(q.Field_Name__c);
            String riskScore = thisLevel + ' - ' + thisCategory;
            
            summaryWrapper theWrap = new summaryWrapper(question, response, riskScore);
            theWrap.apiName = q.Field_Name__c;
            returnSummaries.add(theWrap);
        }
        
        Map<String, List<summaryWrapper>> summaryMap = new Map<String, List<summaryWrapper>>();
        for(summaryWrapper sw : returnSummaries){
            String score = sw.riskScore.substring(0,2);
            List<summaryWrapper> swList = summaryMap.get(score);
            if(swList == null){
                swList = new List<summaryWrapper>();
            }
            swList.add(sw);
            summaryMap.put(score, swList);
        }
        
        List<summaryWrapper> summaryList = new List<summaryWrapper>();
        for(String key : summaryMap.keySet()){
            List<summaryWrapper> mapList = summaryMap.get(key);
            mapList.sort();
            
            summaryMap.put(key, mapList);
        }
        
        if(summaryMap.get('R1') != null){
            summaryList.addAll(summaryMap.get('R1'));
        }
        if(summaryMap.get('R2') != null){
            summaryList.addAll(summaryMap.get('R2'));
        }
        if(summaryMap.get('R3') != null){
            summaryList.addAll(summaryMap.get('R3'));
        }
        if(summaryMap.get('R4') != null){
            summaryList.addAll(summaryMap.get('R4'));
        }
        if(summaryMap.get('R5') != null){
            summaryList.addAll(summaryMap.get('R5'));
        }
        if(summaryMap.get('R6') != null){
            summaryList.addAll(summaryMap.get('R6'));
        }
        if(summaryMap.get('R7') != null){
            summaryList.addAll(summaryMap.get('R7'));
        }        
        
        return summaryList;
        
    }
    
    global class summaryWrapper implements Comparable{
        
        @auraEnabled
        global String question {get; set;}
        @auraEnabled
        global String response {get; set;}
        @auraEnabled
        global String riskScore {get; set;}
        @auraEnabled
        global String apiName {get; set;}
        
        global summaryWrapper(String question, String response, String riskScore){
            this.question = question;
            this.response = response;
            this.riskScore = riskScore;
        }
        
        global Integer compareTo(Object objToCompare){
            
            return question.compareTo(((summaryWrapper)objToCompare).question);
            
        }
        
    }
    
    @auraEnabled
    Public Static Review__c calcReviewPercent(Review__c theReview){
        
        System.debug('theReview: ' + theReview);
        List<String> selectedSteps = new List<String>();
        Map<String, Boolean> catMap = new Map<String, Boolean>();
        if(theReview.Risk_Categories_Selected__c.contains('execution')){
            catMap.put('execution', true);
            selectedSteps.add('Execution Risks');
        }
        if(theReview.Risk_Categories_Selected__c.contains('client')){
            catMap.put('client', true);
            selectedSteps.add('Client Risks');
        }
        if(theReview.Risk_Categories_Selected__c.contains('geography')){
            catMap.put('geography', true);
            selectedSteps.add('Geography Risks');
        }
        if(theReview.Risk_Categories_Selected__c.contains('commercial')){
            catMap.put('commercial', true);
            selectedSteps.add('Commercial Risks');
        }
        if(theReview.Risk_Categories_Selected__c.contains('contractual')){
            catMap.put('contractual', true);
            selectedSteps.add('Contractual Risks');
        }
        
        String lob = theReview.Line_of_Business__c;
        if(lob == null){
            Review__c rtReview = [SELECT Id, RecordType.Name FROM Review__c WHERE Id = :theReview.Id];
            if(rtReview.RecordType.Name.contains('BIAF')){
                lob = 'BIAF';
            } else if(rtReview.RecordType.Name.contains('ECR')){
                lob = 'ECR';
            }
        }
        List<RA_Question__mdt> reviewQuestions = [SELECT Id, Field_Name__c
                                                  FROM RA_Question__mdt
                                                  WHERE Line_of_Business__c = :lob
                                                  AND RA_Step__r.Step_Label__c IN :selectedSteps];
        
        Integer numQuestions = reviewQuestions.size();
        Integer numAnswered = 0;
        if(reviewQuestions.size() > 0){
            for(RA_Question__mdt q : reviewQuestions){
                if(theReview.get(q.Field_Name__c) != null){
                    numAnswered++;
                }
            }
        }
        System.debug('numQuestions: ' + numQuestions);
        System.debug('numAnswered: ' + numAnswered);
        
        Decimal percentAnswered = Decimal.valueOf(numAnswered) / Decimal.valueOf(numQuestions);
        System.debug('percentAnswered: ' + (percentAnswered * 100).round());
        
        theReview.Risk_Assessment_Complete__c = (percentAnswered * 100).round();
        
        return theReview;
        
    }
    
}