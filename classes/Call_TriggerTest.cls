/**************************************************************************************
Name: Call_TriggerTest
Version: 1.0 
Created Date: 28.08.2018
Function: Test class for Call trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Stone       28.08.2018        Original Version
* Scott Stone		21.03.2019		 DE928 - change client visit milestone to call
*************************************************************************************/
@IsTest
public class Call_TriggerTest {

    //tests process builder - not trigger
    @IsTest
    private static void shouldCreateClientVisitMilestoneOnCreate() {
        //given
        TestHelper.CallBuilder callBuilder = new TestHelper.CallBuilder();
        Call__c call = callBuilder.build().getRecord();

        //when
        Test.startTest();
        insert call;
        Test.stopTest();

        String readonlyRecordType = Schema.SObjectType.Schedule_Milestone__c.getRecordTypeInfosByName().get('Schedule Milestone Read Only').getRecordTypeId();
        
        //then
        integer milestoneCount = [SELECT Count() FROM Schedule_Milestone__c 
                                  WHERE Milestone_Type__c = 'Call' 
                                  AND Auto_Created_Milestone__c = true 
                                  AND RecordTypeId = :readonlyRecordType 
                                  AND Call__c = :call.Id];
        
        System.assertEquals(1, milestoneCount);
    }
    
    //tests process builder - not trigger
    @IsTest
    private static void shouldUpdateClientVisitMilestoneOnUpdate() {
        //given
        TestHelper.CallBuilder callBuilder = new TestHelper.CallBuilder();
        Call__c call = callBuilder.build().save().getRecord();

        Date tomorrow = Date.today().addDays(1);
        
        call.Subject__c = 'Updated Test Subject';
        call.Date__c = tomorrow;
        call.Status__c = 'Completed';
        
        //when
        Test.startTest();
        update call;
        Test.stopTest();

        //then
        Schedule_Milestone__c milestone = [SELECT Notes__c, Start_Date__c, End_Date__c, Status__c, RecordTypeId
                                           FROM Schedule_Milestone__c 
                                           WHERE Milestone_Type__c = 'Call' 
                                           AND Auto_Created_Milestone__c = true];
        
        String expectedDescription = call.Name + ' ' + call.Subject__c;
        String readonlyRecordType = Schema.SObjectType.Schedule_Milestone__c.getRecordTypeInfosByName().get('Schedule Milestone Read Only').getRecordTypeId();
        
        
        System.debug(call.Name);
        System.debug(call.Subject__c);
        
        //System.assertEquals(expectedDescription, milestone.Notes__c);
        System.assertEquals('Closed', milestone.Status__c);
        System.assertEquals(tomorrow, milestone.End_Date__c);
        System.assertEquals(tomorrow, milestone.Start_Date__c);
        System.assertEquals(readonlyRecordType, milestone.RecordTypeId);
    }
    
    @IsTest
    private static void shouldDeleteClientVisitMilestoneOnDelete() {
        //given
        TestHelper.CallBuilder callBuilder = new TestHelper.CallBuilder();
        Call__c call = callBuilder.build().save().getRecord();
        
        //when
        Test.startTest();
        delete call;
        Test.stopTest();

        //then
        integer milestoneCount = [SELECT Count() FROM Schedule_Milestone__c 
                                  WHERE Milestone_Type__c = 'Call' 
                                  AND Auto_Created_Milestone__c = true 
                                  AND RecordTypeId = '0122C000000CreIQAS' 
                                  AND Call__c = :call.Id];
        
        System.assertEquals(0, milestoneCount);
    }
}