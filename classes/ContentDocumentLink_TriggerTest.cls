/**************************************************************************************
Name: ContentDocumentLink_TriggerTest
Version: 1.0
Created Date: 10/09/2017
Function: Test class for ContentDocumentLink_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    10/09/2017      Original Version
*************************************************************************************/
@isTest
private class ContentDocumentLink_TriggerTest {

  private static final String PROFILE_SALES_SUPER_USER  = 'Sales Super User';
  private static final String USERNAME                  = 'salescdlTest@testuser.com';
  private static final string LIBRARY_RECORDTYPE_RESUME = 'Resume';

  //Test data setup
  @testSetup
  static void setUpTestData(){

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

    System.runAs(salesUser){
      
      //Create 300 Accounts
      List<Account> accountList = new List<Account>();
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      for(Integer i=0; i<10; i++){
        accountList.add(accountBuilder.build().getRecord());
      }
       
      insert accountList;  

      //Create 300 Library records
      List<Library__c> libraryList = new List<Library__c>();
      TestHelper.LibraryBuilder libraryBuilder = new TestHelper.LibraryBuilder();
      for(Integer i=0; i<10; i++){
        libraryList.add(libraryBuilder.build().withRecordType(LIBRARY_RECORDTYPE_RESUME).getRecord());
      }

      insert libraryList;

      //Create 300 Files
      List<ContentVersion> fileList = new List<ContentVersion>();
      TestHelper.ContentVersionBuilder fileBuilder = new TestHelper.ContentVersionBuilder();
      for(Integer i=0; i<10; i++){
        fileList.add(fileBuilder.build().getRecord());
      }

      insert fileList;      

    }
  }
	
	@isTest static void test_LibraryFilesShareTypeI() {
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    test.startTest();
    //Execute tests
    System.runAs(salesUser){

      //Get all libraries created by the user
      List<Library__c> libraryList = [Select Id 
                                      From Library__c 
                                      Where CreatedBy.Id = :salesUser.Id];

      //Create files
      List<ContentVersion> fileList = [Select id, 
                                              ContentDocumentId 
                                       From ContentVersion 
                                       Where CreatedBy.Id = :salesUser.Id];

      //List for inserting contentdocumentlink
      List<ContentDocumentLink> contentdocumentlinkList = new List<ContentDocumentLink>();

      //List of Library Ids
      List<Id> libraryIds = new List<Id>();

      //Loop through the libraries and attach file to each library
      for(Integer i=0; i<10; i++)
      {
        //Prepare the contentdocument link and add it to the list
        contentdocumentlinkList.add(new ContentDocumentLink(ContentDocumentId = fileList[i].ContentDocumentId,
                                                            LinkedEntityId    = libraryList[i].Id,
                                                            ShareType = 'V'));

        libraryIds.add(libraryList[i].Id);

      } //END FOR

      //Insert ContentDocumentLink
      insert contentdocumentlinkList;

      //Retrieve the contentdocument link list
      List<ContentDocumentLink> updatedConDocLinkList = [Select Id,
                                                                ShareType
                                                         From ContentDocumentLink 
                                                         Where LinkedEntityId in :libraryIds];
      //Perform assertions
      for(ContentDocumentLink currentLink: updatedConDocLinkList)
      {
        System.assertEquals('I', currentLink.ShareType);
      } //END FOR

    }

    test.stopTest();    

	}
	
	@isTest static void test_NonLibraryFilesShareTypeV() {
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

    test.startTest();
    //Execute tests
    System.runAs(salesUser){

      //Get all libraries created by the user
      List<Account> accountList = [Select Id 
                                      From Account 
                                      Where CreatedBy.Id = :salesUser.Id];

      //Create files
      List<ContentVersion> fileList = [Select id, 
                                              ContentDocumentId 
                                       From ContentVersion 
                                       Where CreatedBy.Id = :salesUser.Id];

      //List for inserting contentdocumentlink
      List<ContentDocumentLink> contentdocumentlinkList = new List<ContentDocumentLink>();

      //List of Library Ids
      List<Id> acctIds = new List<Id>();

      //Loop through the libraries and attach file to each library
      for(Integer i=0; i<10; i++)
      {
        //Prepare the contentdocument link and add it to the list
        contentdocumentlinkList.add(new ContentDocumentLink(ContentDocumentId = fileList[i].ContentDocumentId,
                                                            LinkedEntityId    = accountList[i].Id,
                                                            ShareType = 'V'));

        acctIds.add(accountList[i].Id);

      } //END FOR

      //Insert ContentDocumentLink
      insert contentdocumentlinkList;

      //Retrieve the contentdocument link list
      List<ContentDocumentLink> updatedConDocLinkList = [Select Id,
                                                                ShareType
                                                         From ContentDocumentLink 
                                                         Where LinkedEntityId in :acctIds];
      //Perform assertions
      for(ContentDocumentLink currentLink: updatedConDocLinkList)
      {
        System.assertEquals('V', currentLink.ShareType);
      } //END FOR

    }

    test.stopTest();   
	}
	
}