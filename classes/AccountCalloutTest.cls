/**************************************************************************************
Name: AccountCalloutTest
Version: 1.0 
Created Date: 25.04.2017
Function: This class is used to initiate all the variables and classes in NewAccount_WS_XSD and generate the mock response
          that is set by the AccountCalloutMock class, in order to meet code coverage.  

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      25.04.2017      Original Version
* Scott Walker      15.02.2019      ECR updates to testAccountCalloutError and shouldDeleteRetryLogWhenNoError methods to
                                    support manual Account verification process
***************************************************************************************/

@isTest
private class AccountCalloutTest {

    @isTest static void testAccountCalloutSuccess() {

        //PartySetup Parameters
        List<NewAccount_WS_XSD.PartySetup_element> partyList = new List<NewAccount_WS_XSD.PartySetup_element>();
        //PartyRecordParameters
        NewAccount_WS_XSD.PartySetup_element partyRecordParameters = new NewAccount_WS_XSD.PartySetup_element();
        partyRecordParameters.partyName = '';
        partyRecordParameters.flexattribute1 = '';
        partyRecordParameters.flexattribute2 = '';
        partyRecordParameters.flexattribute3 = '';
        partyRecordParameters.flexattribute4 = '';
        partyRecordParameters.flexattribute5 = '';
        partyRecordParameters.accountinfo = new List<NewAccount_WS_XSD.Accountinfo_element>();
        //AccountInfoRecordParameters
        NewAccount_WS_XSD.Accountinfo_element accountInfoRecordParameters = new NewAccount_WS_XSD.Accountinfo_element();
        accountInfoRecordParameters.accountName = 'Test Acc Name';
        //accountInfoRecordParameters.accountId = '124';
        accountInfoRecordParameters.JBaccountId= '124';
        accountInfoRecordParameters.sfAccountId = '0010000000afgdh';
        accountInfoRecordParameters.flexattribute1 = '';
        accountInfoRecordParameters.flexattribute2 = '';
        accountInfoRecordParameters.flexattribute3 = '';
        accountInfoRecordParameters.flexattribute4 = '';
        accountInfoRecordParameters.flexattribute5 = '';
        accountInfoRecordParameters.partycontactInfo = new List<NewAccount_WS_XSD.PartycontactInfo_element>();
        accountInfoRecordParameters.addressinfo = new List<NewAccount_WS_XSD.Addressinfo_element>();
        //PartyContactParameters
        NewAccount_WS_XSD.PartycontactInfo_element partyContactParameters = new NewAccount_WS_XSD.PartycontactInfo_element();
        partyContactParameters.prefix = '';
        partyContactParameters.firstName = '';
        partyContactParameters.middleName = '';
        partyContactParameters.lastName = '';
        partyContactParameters.jobTitle = '';
        partyContactParameters.contactRoles = '';
        partyContactParameters.phonenumber = '';
        partyContactParameters.emailAddress = '';
        partyContactParameters.urls = '';
        partyContactParameters.contactId = '123';
        partyContactParameters.sfCountactId = '';
        partyContactParameters.flexattribute1 = '';
        partyContactParameters.flexattribute2 = '';
        partyContactParameters.flexattribute3 = '';
        partyContactParameters.flexattribute4 = '';
        partyContactParameters.flexattribute5 = '';
        // Adding the record to the List
        accountInfoRecordParameters.partycontactInfo.add(partyContactParameters);
        //AddressInfoRecord
        NewAccount_WS_XSD.Addressinfo_element addressInfoRecord = new NewAccount_WS_XSD.Addressinfo_element();
        addressInfoRecord.orgName = '';
        //   addressInfoRecord.orgName = acc.Operating_Unit__c;
        addressInfoRecord.address1 = '123 Alameda';
        //   addressInfoRecord.address1 = loc.Street_Address__c;
        addressInfoRecord.address2 = 'Street 4';
        addressInfoRecord.address3 = 'Unit G';
        addressInfoRecord.address4 = '';
        addressInfoRecord.city = 'Pasadena';
        //    addressInfoRecord.city = loc.City__c;
        addressInfoRecord.state = 'CA';
        //     addressInfoRecord.state = loc.State_Province__c;
        addressInfoRecord.province = '';
        addressInfoRecord.county = '';
        addressInfoRecord.country = 'US';
        //     addressInfoRecord.country = loc.Country__c;
        addressInfoRecord.postalCode = '91007';
        //      addressInfoRecord.postalCode = loc.Postal_Code__c;
        addressInfoRecord.addressId = '';
   /*     if (loc.Address_ID__c != null){
         addressInfoRecord.addressId = loc.Address_ID__c;
        }
    */
        addressInfoRecord.sfAddressId = '178427';
        //     addressInfoRecord.sfAddressId = loc.id;
    /*    if (loc.Primary__c == TRUE){
         addressInfoRecord.flexattribute1 = 'Y';
        } else {
          addressInfoRecord.flexattribute1 = 'N';  
        }
     */
        addressInfoRecord.flexattribute1 = 'Y';
        addressInfoRecord.flexattribute2 = '';
        addressInfoRecord.flexattribute3 = '';
        addressInfoRecord.flexattribute4 = '';
        addressInfoRecord.flexattribute5 = '';
        //Adding the record to the list
        accountInfoRecordParameters.addressinfo.add(addressInfoRecord);
        //Adding all accountInfo records to the PartyRecordParameter list
        partyRecordParameters.accountinfo.add(accountInfoRecordParameters);
        //Adding everything to the main list
        partyList.add(partyRecordParameters);

        NewAccount_WS.ProcessAccountReqABCS_pt request = new NewAccount_WS.ProcessAccountReqABCS_pt();
        //setting the timeout          
        request.timeout_x = 120000;

        //Auth
        request.Header = new NewAccount_WS.Security();
        request.Header.UserNameToken = new NewAccount_WS.UsernameToken();
        request.Header.UserNameToken.UserName = 'SALESFORCE';
        request.Header.UserNameToken.password = 'salesforce123';

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        SiteLocation__c sl = [SELECT Id, Address_ID__c, Country__c FROM SiteLocation__c WHERE Primary__c = TRUE LIMIT 1];
        system.debug('*** ' + sl);
        sl.Address_ID__c = '12345';
        update sl;

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new AccountCalloutMock());
 /*       // Call the method that invokes a callout
        integer x = 123;
        integer y = 456;
        string sp = 'attribute1';
  */

        Test.startTest();
        AccountQueueableHandler job = new AccountQueueableHandler(acc);
        job.max = 1;
        job.execute(null);
        Test.stopTest();

        List<Log__c> errors = [SELECT Id FROM Log__c];
        System.assertEquals(0, errors.size());
    }

    @isTest static void testAccountCalloutError() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().getRecord();
        //Commenting out next line  in support of manual ECR Account verification process
        //acc.OracleAccountID__c = null;
        acc.Status__c = 'Verified';
        insert acc;

        SiteLocation__c sl = [SELECT Id, Operating_Unit__c, Account__c, Street_Address__c FROM SiteLocation__c WHERE Account__c = :acc.Id LIMIT 1];
        sl.Street_Address__c = 'Some different value';
        sl.Name ='Testing name';
        update sl;

        AccountCalloutMock mock = new AccountCalloutMock();
        mock.accountCreationError = true;
        Test.setMock(WebServiceMock.class, mock);

        Test.startTest();
        AccountQueueableHandler job = new AccountQueueableHandler(acc);
        job.max = 1;
        job.execute(null);
        Test.stopTest();

        List<Log__c> errors = [SELECT Id, Exception__c, EventType__c, status__c FROM Log__c];
        System.debug('errors: '+errors);
        System.assertEquals(1, errors.size());

    }

    @isTest static void testAccountCalloutAddressError() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        AccountCalloutMock mock = new AccountCalloutMock();
        mock.addressExistsError = true;
        Test.setMock(WebServiceMock.class, mock);

        Test.startTest();
        AccountQueueableHandler job = new AccountQueueableHandler(acc);
        job.max = 1;
        job.execute(null);
        Test.stopTest();

        List<Log__c> errors = [SELECT Id FROM Log__c];
        System.assertEquals(1, errors.size());

    }

    @IsTest
    private static void shouldCreateRetryLogForSubscribedErrorCodes() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withOracleId(null).save().getRecord();

        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 404 Not Found"';
        Test.setMock(WebServiceMock.class, mock);

        //when
        Test.startTest();
        AccountQueueableHandler job = new AccountQueueableHandler(acc);
        job.max = 1;
        job.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT RecordId__c, ObjectType__c, NumberOfAttempts__c FROM RetrySyncLog__c];
        System.assertEquals(1, result.size());
        System.assertEquals(acc.Id, result[0].RecordId__c);
        System.assertEquals(Account.getSObjectType().getDescribe().getName(), result[0].ObjectType__c);
        System.assertEquals(1, result[0].NumberOfAttempts__c);
    }

    @isTest
    private static void shouldUpdateRetryLogForSubscribedErrorCodes() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withOracleId(null).save().getRecord();

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = acc.Id,
                        ObjectType__c = Account.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = 1
                );
        insert retrySyncLog;

        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 500"';
        Test.setMock(WebServiceMock.class, mock);

        //when
        Test.startTest();
        AccountQueueableHandler handler = new AccountQueueableHandler(acc);
        handler.max = 1;
        handler.setRetrySyncLog(retrySyncLog);
        handler.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT RecordId__c, ObjectType__c, NumberOfAttempts__c FROM RetrySyncLog__c];
        System.assertEquals(1, result.size());
        System.assertEquals(acc.Id, result[0].RecordId__c);
        System.assertEquals(Account.getSObjectType().getDescribe().getName(), result[0].ObjectType__c);
        System.assertEquals(2, result[0].NumberOfAttempts__c);
    }

    @isTest
    private static void shouldDeleteRetryLogWhenNoError() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        //Comment out following acc variable line and replace to support ECR manual Account verification process
        //Account acc = accountBuilder.build().withOracleId(null).save().getRecord();
        Account acc = accountBuilder.build().save().getRecord();
        SiteLocation__c sl = [SELECT Address_ID__c FROM SiteLocation__c WHERE Primary__c = true LIMIT 1];
        sl.Address_ID__c = '12345';
        update sl;

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = acc.Id,
                        ObjectType__c = Account.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = 1
                );
        insert retrySyncLog;

        AccountCalloutMock mock = new AccountCalloutMock();
        Test.setMock(WebServiceMock.class, mock);

        //when
        Test.startTest();
        AccountQueueableHandler handler = new AccountQueueableHandler(acc);
        handler.max = 1;
        handler.setRetrySyncLog(retrySyncLog);
        handler.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT RecordId__c, ObjectType__c, NumberOfAttempts__c FROM RetrySyncLog__c];
        System.assertEquals(0, result.size());
    }

    @isTest
    private static void shouldNotUpdateRetryLogWhenNumberOfAttemptsItTooBig() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withOracleId(null).save().getRecord();

        RetrySyncLog__c retrySyncLog =
                new RetrySyncLog__c(
                        RecordId__c = acc.Id,
                        ObjectType__c = Account.getSObjectType().getDescribe().getName(),
                        NumberOfAttempts__c = OracleCalloutRetryService.MAX_RETRY_ATTEMPTS+1
                );
        insert retrySyncLog;

        ResponseExceptionMock mock = new ResponseExceptionMock();
        mock.message = 'Unable to tunnel through proxy. Proxy returns "HTTP/1.0 500"';
        Test.setMock(WebServiceMock.class, mock);

        //when
        Test.startTest();
        AccountQueueableHandler handler = new AccountQueueableHandler(acc);
        handler.max = 1;
        handler.setRetrySyncLog(retrySyncLog);
        handler.execute(null);
        Test.stopTest();

        //then
        List<RetrySyncLog__c> result = [SELECT RecordId__c, ObjectType__c, NumberOfAttempts__c FROM RetrySyncLog__c];
        System.assertEquals(1, result.size());
        System.assertEquals(acc.Id, result[0].RecordId__c);
        System.assertEquals(Account.getSObjectType().getDescribe().getName(), result[0].ObjectType__c);
        System.assertEquals(retrySyncLog.NumberOfAttempts__c, result[0].NumberOfAttempts__c);
    }

}