/**************************************************************************************
Name: ContentDocumentLink_TriggerHandler
Version: 1.0
Created Date: 10/09/2017
Function: Handler for ContentDocumentLink_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    10/09/2017      Original Version
*************************************************************************************/
public with sharing class ContentDocumentLink_TriggerHandler {
    
    //Trigger Handler  
    private static ContentDocumentLink_TriggerHandler handler;
    
    /*
	* Singleton like pattern
	*/
    public static ContentDocumentLink_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new ContentDocumentLink_TriggerHandler();
        }
        return handler;
    }  
    
    /*
	* Actions performed before the content document link is inserted
	*/
    public void onBeforeInsert(List<ContentDocumentLink> newLibraryFiles){
        changeLibraryFileShareType(newLibraryFiles);
    }
    
   /*
	* Actions performed after the content document link is inserted
	*/
    public void onAfterUpsert(List<ContentDocumentLink> newLibraryFiles){
        recalcFileCount(getLibraries(newLibraryFiles));
    }
    
    /* 
	* Change the File Sharing Setting of Files attached to LIbrary document to 'Set By Record' 
	*/
    public void changeLibraryFileShareType(List<ContentDocumentLink> newLibraryFiles){
        
        List<Id> linkedEntityIdList = new List<Id>();
        
        //Get the LinkedEntity Id for uploaded files
        for(ContentDocumentLink currentFile: newLibraryFiles)
        {
            linkedEntityIdList.add(currentFile.LinkedEntityId);
        } //END FOR
        
        //Get the map of linked Entity Id
        Map<Id, Library__c> libraryMap = new Map<Id, Library__c>([Select Id 
                                                                  From Library__c
                                                                  Where Id in :linkedEntityIdList]);
        
        //Perform the update if map is not empty
        if(libraryMap!=null && 
           !libraryMap.isEmpty())
        {
            //Loop through files and set sharetype for library attachment
            for(ContentDocumentLink currentFile: newLibraryFiles)
            {
                if(libraryMap.containsKey(currentFile.LinkedEntityId))
                {
                    currentFile.ShareType = 'I';          
                }
            } //END FOR      
        }
    }
    
        
    /* 
	* Return list of Libraries associated to File
	*/
    public List<Library__c> getLibraries(List<ContentDocumentLink> libraryFiles){
        Set<Id> parentIds = new Set<Id>();
        Set<Id> fileIds = new Set<Id>(new Map<Id, ContentDocumentLink>(libraryFiles).keySet());
        
        //Get list of File Linked Entity Ids
        for(ContentDocumentLink cdl : [SELECT LinkedEntityId FROM ContentDocumentLink WHERE Id IN : fileIds]){
            parentIds.add(cdl.LinkedEntityId);
        }
        
        //Update the Library records with the new count of Files
        List<Library__c> parentLibraries = new List<Library__c>([SELECT Id, num_count_of_related_files__c  
                                            					 FROM Library__c WHERE Id IN : parentIds]);
        
        return parentLibraries;
    }
        
   /* 
	* Recalculate the count of Files associated to a Library
	*/
    public static void recalcFileCount(List<Library__c> libraries){

        //Must convert the list to Ids to be able to execute the query for ContentDocumentLink
        Map<Id, Decimal> mapFileCountToLibrary = new Map<Id, Decimal>();
        Set<Id> libraryIds = new Set<Id>(new Map<Id, Library__c>(libraries).keySet());
        
        //Get a count of all Files for each Linked Entity Id
        if(libraryIds.isEmpty() == false){
            
            for(AggregateResult agr : [SELECT LinkedEntityId, count(Id) fileCount FROM ContentDocumentLink
                                       WHERE LinkedEntityId IN : libraryIds GROUP BY LinkedEntityId]){
                                           mapFileCountToLibrary.put((Id) agr.get('LinkedEntityId'), (Decimal) agr.get('fileCount'));
                                       }
        }
        
        for(Library__c library : libraries){
            if(mapFileCountToLibrary.get(library.Id) != null){
            	library.num_count_of_related_files__c = mapFileCountToLibrary.get(library.Id);
            } else{
            	library.num_count_of_related_files__c = 0;
            }
        }
        
        update libraries;
        
    }
    
}