/**************************************************************************************
Name: Contact_TriggerTest
Version: 1.0 
Created Date: 06.06.2017
Function: Contact Trigger handler unit test class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           06.06.2017        Original Version
*************************************************************************************/
@isTest
private class Contact_TriggerTest {

    @isTest
    private static void shouldUpdateValuesInResourceContact() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().getRecord();
        acc.Employee_Contact_Account__c = true;
        insert acc;

        TestHelper.ContactBuilder contactBuilder = new TestHelper.ContactBuilder();

        //Build Contact without Account
        Contact con = contactBuilder.build().getRecord();
        RecordType contactRecordType =
            [
                    SELECT Id
                    FROM RecordType
                    WHERE SobjectType = :Contact_TriggerHandler.CONTACT_OBJECT_NAME AND DeveloperName != :Contact_TriggerHandler.RESOURCE_RECORD_TYPE_NAME
                    LIMIT 1];
        //when
        con.Jacobs_Employee_ID__c = 'Some value';
        con.RecordTypeId = contactRecordType.Id;
        insert con;

        //then
        List<Contact> result = [SELECT Id,
                                       AccountId 
                                FROM Contact 
                                WHERE RecordType.DeveloperName = :Contact_TriggerHandler.RESOURCE_RECORD_TYPE_NAME];

        System.assertEquals(1, result.size());
        System.assertEquals(acc.Id, result[0].AccountId);

    }

    @isTest
    private static void shouldNotUpdateValuesInResourceContact() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.ContactBuilder contactBuilder = new TestHelper.ContactBuilder();
        Contact con = contactBuilder.build().getRecord();
        RecordType contactRecordType =
        [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = :Contact_TriggerHandler.CONTACT_OBJECT_NAME AND DeveloperName != :Contact_TriggerHandler.RESOURCE_RECORD_TYPE_NAME
                LIMIT 1];

        //when
        con.Jacobs_Employee_ID__c = null;
        con.RecordTypeId = contactRecordType.Id;
        insert con;

        //then
        List<Contact> result = [SELECT Id FROM Contact WHERE RecordType.DeveloperName = :Contact_TriggerHandler.RESOURCE_RECORD_TYPE_NAME];
        System.assertEquals(0, result.size());
    }
}