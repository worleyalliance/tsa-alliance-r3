/**************************************************************************************
Name: ProjectCalloutMock
Version: 1.0 
Created Date: 17.04.2017
Function: This class is used to set the Mock response that is used in test class for Project Service Callout (Project_WS)

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      17.04.2017      Original Version
***************************************************************************************/

@isTest
global class ProjectCalloutMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        // start - specify the response you want to send
        Project_WS.processResponse_element response_x = 
            new Project_WS.processResponse_element();
        response_x.result = 'S';
        // end
        response.put('response_x', response_x); 
   }
}