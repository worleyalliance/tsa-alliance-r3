/**************************************************************************************
Name: OpportunityForecast_C_Test
Version: 1.0 
Created Date: 04.21.2017
Function: Unit test for OpportunityForecast_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04/21/2018      Original Version
* Pradeep Shetty    04/22/2018      US1424: Changes related to Monthly Spread
* Pradeep Shetty    09/13/2018      US2406: Updated test class based on US changes
*************************************************************************************/
@isTest
private class OpportunityForecast_C_Test {

    @isTest
    private static void shouldRetrieveSpreadingFormulas() {
        //when
        List<BaseComponent_Service.PicklistValue> results = OpportunityForecast_C.getSpreadingFormulas();

        //then
        System.assertNotEquals(0, results.size());
    }

    //US2406: Commenting out this method since we are not getting standard deviation
    //@isTest
    //private static void shouldRetrieveStandardDeviations() {
    //    //when
    //    List<BaseComponent_Service.PicklistValue> results = OpportunityForecast_C.getStandardDeviations();

    //    //then
    //    System.assertNotEquals(0, results.size());
    //}

    @isTest
    private static void shouldRetrieveForecastsWithSpread() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 1000;
        Date startDate = Date.newInstance(2017, 7, 3);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder
                                                .build()
                                                .withRevenue(revenue)
                                                .withGM(revenue)
                                                .withStartDate(startDate)
                                                .withNumberOfMonths(11)
                                                .withOpportunity(opp.Id)
                                                .save()
                                                .getRecord();

        String fiscalYearSignature = 'FY 2018';
        
        //when
        List<OpportunityForecast_Service.OpportunityForecast> results = OpportunityForecast_C.retrieveForecasts(opp.Id);

        //then
        OpportunityForecast_Service.OpportunityForecast resultForecast;
        for(OpportunityForecast_Service.OpportunityForecast forecast : results){
            if(forecast.id == multiOfficeSplit.Id){
                resultForecast = forecast;
            }
        }
        System.assertNotEquals(null, resultForecast);
        System.assertEquals(1, resultForecast.fiscalYears.size());
        System.assertEquals(fiscalYearSignature, resultForecast.fiscalYears[0].year);
        System.assertNotEquals(null, resultForecast.fiscalYears[0].q1Revenue); //There is some recalculation done
        System.assertNotEquals(null, resultForecast.fiscalYears[0].q2Revenue);
        System.assertNotEquals(null, resultForecast.fiscalYears[0].q3Revenue);
        System.assertNotEquals(null, resultForecast.fiscalYears[0].q4Revenue);
    }

    @isTest
    private static void shouldRetrieveQuartersForMultiOffice() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 1000;
        Date startDate = Date.newInstance(2017, 7, 3);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder
                                                .build()
                                                .withRevenue(revenue)
                                                .withGM(revenue)
                                                .withStartDate(startDate)
                                                .withNumberOfMonths(11)
                                                .withOpportunity(opp.Id)
                                                .save()
                                                .getRecord();

        String fiscalYearSignature = 'FY 2018';
        String quarterName = 'Q1 FY18';

        //when
        List<OpportunityForecast_Service.Quarter> results = OpportunityForecast_C.retrieveQuartersForMultiOffice(multiOfficeSplit.Id);

        //then
        System.assertEquals(4, results.size());
        System.assertEquals(quarterName, results[0].quaterName);
        System.assertEquals(fiscalYearSignature, results[0].year);
    }

    //Commenting out test methods that deal with Manual
    /*@isTest
    private static void shouldUpdateQuarterValues() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withGet(100).withGo(100).withAccount(acc.Id).save().getOpportunity();
        Decimal revenue = 1000;
        Date startDate = Date.newInstance(2030, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit =
                multiOfficeSplitBuilder
                        .build()
                        .withRevenue(revenue)
                        .withGM(revenue)
                        .withStartDate(startDate)
                        .withNumberOfMonths(10)
                        .withOpportunity(opp.Id)
                        .save()
                        .getRecord();
        Multi_Office_Split__c multiOfficeSplitRefresh =
            [SELECT Probable_GM__c FROM Multi_Office_Split__c WHERE Id = :multiOfficeSplit.Id];

        //Get the Revenue Schedule that was generated    
        List<Revenue_Schedule__c> rsList = [Select Id, 
                                                   Probable_GM__c,
                                                   Reporting_Quarter__C, 
                                                   Spread_Percent__c,
                                                   Fiscal_Year__c 
                                            from Revenue_Schedule__c 
                                            where Multi_Office_Split__c = :multiOfficeSplit.Id 
                                            and RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];

        //Prepare quarters list to mimic data coming from the UI.
        List<OpportunityForecast_Service.Quarter> quarters = new List<OpportunityForecast_Service.Quarter>();

        //Variable for total percent
        Decimal totalpercent = 0;

        //Variable to get the highest percent
        Integer highestSpreadPercentInd = 0;

        for(Integer i=0; i< rsList.size(); i++){
            if(rsList[i].Probable_GM__c!=0){
                
                //Add the percent to the total
                totalpercent += rsList[i].Spread_Percent__c;

                //Get the highest percent. We will deduct the percent from here if total > 100
                if(rsList[i].Spread_Percent__c > rsList[highestSpreadPercentInd].Spread_Percent__c){
                    highestSpreadPercentInd = i;
                }

                OpportunityForecast_Service.Quarter quar = new OpportunityForecast_Service.Quarter();
                quar.id = rsList[i].Id;
                quar.quaterName = rsList[i].Reporting_Quarter__c;
                quar.revenue = rsList[i].Probable_GM__c;
                quar.year = rsList[i].Fiscal_Year__c;
                quar.spreadPercent = rsList[i].Spread_Percent__c;
                quarters.add(quar);  


            }
        }

        if(totalpercent > 100){
            //Deduct the difference from the highst spread percent
            quarters[highestSpreadPercentInd].spreadPercent = quarters[highestSpreadPercentInd].spreadPercent - (totalpercent - 100);

        }
        else if(totalpercent < 100){
            //Deduct the difference from the highst spread percent
            quarters[highestSpreadPercentInd].spreadPercent = quarters[highestSpreadPercentInd].spreadPercent + (100 - totalpercent);
        }
        else{
            //Update some quarters data
            quarters[0].spreadPercent += 0.1;
            quarters[0].revenue += 100;



            quarters[1].spreadPercent -= 0.1;
            quarters[1].revenue -= 100;
        }

        //Loop through all 
        for(OpportunityForecast_Service.Quarter q: quarters){
            system.debug('Percent: ' + q.spreadPercent + ' Prob GM: ' + q.revenue);
        }
        
        //when
        OpportunityForecast_C.saveQuarters(multiOfficeSplit.Id, 'Manual', JSON.serialize(quarters));

        //then
        //Loop through all quarters and add the revenue. This should match Multi Office
        Decimal rsTotal = 0;
        for(OpportunityForecast_Service.Quarter q: quarters){
            rsTotal += q.revenue;
        }
        System.assertEquals(multiOfficeSplitRefresh.Probable_GM__c, rsTotal);
    }*/
    
    //Commenting out test methods that deal with Manual
    /*@isTest
    private static void shouldNotUpdateQuarterValuesWhenSumeNotEqualsGM() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 20000;
        Date startDate = Date.newInstance(2030, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit =
                multiOfficeSplitBuilder
                        .build()
                        .withRevenue(revenue)
                        .withGM(revenue)
                        .withStartDate(startDate)
                        .withNumberOfMonths(10)
                        .withOpportunity(opp.Id)
                        .save()
                        .getRecord();

        //Get the Revenue Schedule that was generated  
        List<Revenue_Schedule__c> rsList = [Select Id, 
                                                   Probable_GM__c,
                                                   Reporting_Quarter__C, 
                                                   Spread_Percent__c,
                                                   Fiscal_Year__c 
                                            from Revenue_Schedule__c 
                                            where Multi_Office_Split__c = :multiOfficeSplit.Id];

        //Prepare quarters list to mimic data coming from the UI. 
        List<OpportunityForecast_Service.Quarter> quarters = new List<OpportunityForecast_Service.Quarter>();
        for(Revenue_Schedule__c currentRS: rsList){
            if(currentRS.Probable_GM__c!=0){
                OpportunityForecast_Service.Quarter quar = new OpportunityForecast_Service.Quarter();
                quar.id = currentRS.Id;
                quar.quaterName = currentRS.Reporting_Quarter__c;
                quar.revenue = currentRS.Probable_GM__c;
                quar.year = currentRS.Fiscal_Year__c;
                quar.spreadPercent = currentRS.Spread_Percent__c+10;
                quarters.add(quar);       
            }

        }

        //when
        try {
            OpportunityForecast_C.saveQuarters(multiOfficeSplit.Id, 'Manual', JSON.serialize(quarters));

            //then
            System.assert(false, 'Sum is not equal to GM so there should be an exception');
        } catch (Exception ex){
            //System.assert(ex instanceof AuraHandledException);
            System.assert(ex instanceof AuraHandledException);
        }
    }*/

    @isTest
    private static void shouldUpdateSpreadingFormulaToLinearAndCalculateQuarters() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 1000;
        Date startDate = Date.newInstance(2020, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder
                                                .build()
                                                .withRevenue(revenue)
                                                .withGM(revenue)
                                                .withStartDate(startDate)
                                                .withNumberOfMonths(10)
                                                .withOpportunity(opp.Id)
                                                .save()
                                                .getRecord();


        //when
        OpportunityForecast_C.saveSpreadingFormula(multiOfficeSplit.Id, ForecastSpread_Service.LINEAR);

        //then
        List<Revenue_Schedule__c> results = [SELECT Probable_GM__c 
                                             FROM Revenue_Schedule__c 
                                             WHERE Multi_Office_Split__c = :multiOfficeSplit.Id 
                                             AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];
        System.assertNotEquals(0, results.size());
    }

    @isTest
    private static void shouldUpdateSpreadingFormulaToStandardAndCalculateQuarters() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 2000000;
        Date startDate = Date.newInstance(2020, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder
                                                .build()
                                                .withRevenue(revenue)
                                                .withGM(0.5 * revenue)
                                                .withStartDate(startDate)
                                                .withNumberOfMonths(10)
                                                .withOpportunity(opp.Id)
                                                .save()
                                                .getRecord();

        //when
        OpportunityForecast_C.saveSpreadingFormula(multiOfficeSplit.Id, ForecastSpread_Service.STANDARD);

        //then
        List<Revenue_Schedule__c> results = [SELECT Probable_GM__c 
                                             FROM Revenue_Schedule__c 
                                             WHERE Multi_Office_Split__c = :multiOfficeSplit.Id 
                                             AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];
        System.assertNotEquals(0, results.size());
    }

    @isTest
    private static void shouldUpdateSpreadingFormulaToFrontLoadAndCalculateQuarters() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 2000000;
        Date startDate = Date.newInstance(2020, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit =
                multiOfficeSplitBuilder
                        .build()
                        .withRevenue(revenue)
                        .withGM(0.5 * revenue)
                        .withStartDate(startDate)
                        .withNumberOfMonths(10)
                        .withOpportunity(opp.Id)
                        .save()
                        .getRecord();


        //when
        OpportunityForecast_C.saveSpreadingFormula(multiOfficeSplit.Id, ForecastSpread_Service.FRONTLOAD);

        //then
        List<Revenue_Schedule__c> results = [SELECT Probable_GM__c 
                                             FROM Revenue_Schedule__c 
                                             WHERE Multi_Office_Split__c = :multiOfficeSplit.Id 
                                             AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];
        System.assertNotEquals(0, results.size());
    }

    @isTest
    private static void shouldUpdateSpreadingFormulaToBackLoadAndCalculateQuarters() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        Decimal revenue = 2000000;
        Date startDate = Date.newInstance(2020, 1, 1);
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder
                                                .build()
                                                .withRevenue(revenue)
                                                .withGM(0.5 * revenue)
                                                .withStartDate(startDate)
                                                .withNumberOfMonths(10)
                                                .withOpportunity(opp.Id)
                                                .save()
                                                .getRecord();

        //when
        OpportunityForecast_C.saveSpreadingFormula(multiOfficeSplit.Id, ForecastSpread_Service.BACKLOAD);

        //then
        List<Revenue_Schedule__c> results = [SELECT Probable_GM__c 
                                             FROM Revenue_Schedule__c 
                                             WHERE Multi_Office_Split__c = :multiOfficeSplit.Id 
                                             AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];
        System.assertNotEquals(0, results.size());
    }

    //US2406: Test class for OpportunitySpreadCalculation class
    @isTest
    private static void shouldUpdateForecastOnOppChange() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder
                          .build()
                          .withAccount(acc.Id)
                          .withRevenue(200000)
                          .withGrossMargin(100000)
                          .withDuration(12)
                          .withGo(100)
                          .withGet(100)
                          .save()
                          .getOpportunity();

        //Get Revenue schedule for the multi office                  
        List<Revenue_Schedule__c> spreadBefore = [SELECT Probable_GM__c 
                                                  FROM Revenue_Schedule__c 
                                                  WHERE Multi_Office_Split__r.Opportunity__c = :opp.Id 
                                                  AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];

        //Update Opportunity Probability
        opp.Go__c = 50;
        Update opp;

        //Get updated rev schedule for multi office
        List<Revenue_Schedule__c> spreadAfter = [SELECT Probable_GM__c 
                                                  FROM Revenue_Schedule__c 
                                                  WHERE Multi_Office_Split__r.Opportunity__c = :opp.Id 
                                                  AND RecordType.DeveloperName = :ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD];
        
        //Assertion
        for(Integer i = 0; i< spreadBefore.size(); i++){
            System.assertEquals(spreadBefore[i].Probable_GM__c / 2.0, spreadAfter[i].Probable_GM__c);
        }
    }    
}