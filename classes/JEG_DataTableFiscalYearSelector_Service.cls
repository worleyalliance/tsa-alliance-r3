/**************************************************************************************
Name: JEG_DataTableFiscalYearSelector_Service
Version: 1.0 
Created Date: 07.03.2017
Function: Service for fiscal year selector component


Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.03.2017         Original Version
*************************************************************************************/
public virtual class JEG_DataTableFiscalYearSelector_Service extends BaseComponent_Service{

   /*
    * Method retrieves Fiscal years to be found in given group attition records for given group
    * Query retrieves AT_Segment_Attrition__c to find fiscal years present.
    * Set is used to make list uniqe
    */
    public List<Integer> getFiscalYears(Id groupId, String fiscalYearField, String relationField, String sObjectName){
        Set<Integer> years = new Set<Integer>();
        String query = 'SELECT '+fiscalYearField+' FROM '+sObjectName+' WHERE '+relationField+ '= \''+groupId+'\'';
        System.debug('query: '+query);
        List<sObject> results = Database.query(query);
        for(sObject result : results){
            years.add(Integer.valueOf(result.get(fiscalYearField)));
        }
        List<Integer> fiscalYears = new List<Integer>(years);
        fiscalYears.sort();
        return fiscalYears;
    }
}