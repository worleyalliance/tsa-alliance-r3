/**************************************************************************************
Name:ForecastSpread_Service
Version: 1.0 
Created Date: 04.08.2017
Function: Revenue Schedule (Forecast Spread ) service class. This class contains methods used by revenuespread classes

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04/14/2017      Original Version     
* Pradeep Shetty    04/23/2018      US1424: Updates based on the Monthly spread requirement
* Pradeep Shetty    04/25/2018      US1424: Combined Spread calculation into one single method
* Pradeep Shetty    09/11/2018      US2406: Adding Front Load and Back load option for spreading formula
                                    Removing Standard Deviation.
* Chris Cornejo     09/18/2018      Fiscal months sort alphabetically instead of chronologically
*************************************************************************************/

public class ForecastSpread_Service {

  public final static String FRONTLOAD                     = 'Frontload';
  public final static String STANDARD                      = 'Standard';
  public final static String BACKLOAD                      = 'Backload';
  public final static String LINEAR                        = 'Linear';
  public final static String MANUAL                        = 'Manual';
  public static final String QUARTER                       = 'Quarter';                  //Quarter
  public static final String MONTH                         = 'Month';                    //Month
  public static final String REC_TYPE_QUARTERLY_SPREAD     = 'Quarterly_Spread';         //Quarterly Spread Record Type
  public static final String REC_TYPE_MONTHLY_SPREAD       = 'Monthly_Spread';           //Monthly Spread Record Type
  public static final String OBJ_NAME_REVENUE_SCHEDULE     = 'Revenue_Schedule__c';      //Multi Office Split Object API Name
  public static final String SPACE                         = ' ';                        //Space

  private static List<Period> fiscalPeriodValues;

  //Variables
  private static Map<String, Id> recordTypeMap = new Map<String, Id>(); //Map for recordTypes

  /*
  * Generic method to get Fiscal Periods
  * Params:
  * List<String> types - List of Period Types (Year, Quarter, Month)
  * Date startDate     - This method fetches period types 1 year before the startdate
  * Date endDate       - This method fetches period types 1 year after the enddate
  */
  public static List<Period> getFiscalPeriods(List<String> types, Date startDate, Date endDate){

    //Initiate the query string
    String queryString = 'Select EndDate, FiscalYearSettings.Name, FullyQualifiedLabel, Id, IsForecastPeriod, Number, PeriodLabel, QuarterLabel, StartDate, Type ' + 
                   'From Period ';

    //If types is not null then get all periods corresponding to the types provided                   
    if(types!=null){
      queryString = queryString + 'Where Type in :types ';
    }

    //If StartDate is provided, subtract a year from start date and use it for querying
    //Prepare the query accordingly
    if(startDate!=null){

      Integer queryStartYear = startDate.addYears(-1).year();
          //system.debug('startDate: ' + String.valueOf(queryStartYear));

      if(queryString.contains('Where')){
        queryString += 'And CALENDAR_YEAR(StartDate) >= :queryStartYear ';
      }
      else{
        queryString += 'Where CALENDAR_YEAR(StartDate) >= :queryStartYear ';
      }
    }
    
    //If EndDate is provided, add a year to end date and use it for querying
    //Prepare the query accordingly
    if(endDate!=null){

      Integer queryEndYear = endDate.addYears(1).year();
          //system.debug('End: ' + String.valueOf(queryEndYear));    

      if(queryString.contains('Where')){
        queryString += 'And CALENDAR_YEAR(EndDate) <= :queryEndYear ';
      }
      else{
        queryString += 'Where CALENDAR_YEAR(EndDate) <= :queryEndYear '; 
      }
    }

    //Order the results first by Type and then by the startdate. This is important. Do not change this.
    //Changingthis will impact the way the revenue schedules are created in prepareRevenueSchedules method. 
    //That method is based on the assumption that the fiscal periods are in ascending order of StartDate
    queryString += 'ORDER BY Type, StartDate';

    //Execute the query
    fiscalPeriodValues = Database.query(queryString);

    //Return the query result
    return fiscalPeriodValues;
  }

  /*
  * Deletes existing Revenue Schedule records for given multioffice records
  */
  public static void deleteRevenue(List<Id> mosIDList){

    //Query Revenue Schedule Object to get existing records
    List<Revenue_Schedule__c> existingSpread = [Select Id
                                                From Revenue_Schedule__c
                                                Where Multi_Office_Split__c IN :mosIDList];

    //Check if it is null. If not null/empty then delete it.
    if (!existingSpread.isEmpty()) 
    {
      delete existingSpread;
    }
  }

  /*
  * From a given set of fiscal periods, this method filters out periods that fall out of the given duration
  * Params:
  * List<Period> fiscalPeriods - List of fiscal periods that need to be filtered
  * Date startDate             - Start date of the duration
  * Date endDate               - End Date of the duration
  * String periodType          - Quarter or Month or Year
  */
  public static List<Period> filterFiscalPeriods(List<Period> fiscalPeriods, Date startDate, Date endDate, String periodType){
    List<Period> periods = new List<Period>();

    //Index to identify the position of startDatePeriod
    Integer i = -1;
      //System.debug('fiscalPeriods: ' + fiscalPeriods);
    for(Period p : fiscalPeriods)
    {
      i++;

      if(p.type == periodType){
        //system.debug('FY-' + p.FiscalYearSettings.Name + ' PeriodLabel-' + p.FullyQualifiedLabel + ' Number-' + p.Number);
        //Add remaing valid periods
        if(p.StartDate <= endDate && p.EndDate >= startDate)
        {
          if(periods!=null && periods.isEmpty()){

            //Number of levels up to go for leading periods
            Integer numOfLevelsUp;

            //For quarters
            if(periodType == QUARTER){
              numOflevelsUp = p.Number - 1;                 
            }

            //For Months
            else if(periodType == MONTH){

              if(p.Number>=7){
                numOfLevelsUp = p.Number - 7;
              }
              else{
                numOflevelsUp = p.Number + 5;
              }
            }

            //Add leading periods if the start date period is not the first period of its kind for a given FY
            for(Integer j=numOflevelsUp;  j > 0; j-- ){
      //system.debug('Value of i: ' + i + '--Value of j: ' + j);
             if(i-j>=0){
                periods.add(fiscalPeriods[i-j]);  
              }
              //system.debug('***Added***' + 'FY-' + fiscalPeriods[i-j].FiscalYearSettings.Name + ' PeriodLabel-' + fiscalPeriods[i-j].FullyQualifiedLabel + ' Number-' + fiscalPeriods[i-j].Number);              
            }              
       
          }
          periods.add(p); 
          system.debug('***Added***' + 'FY-' + p.FiscalYearSettings.Name + ' PeriodLabel-' + p.FullyQualifiedLabel + ' Number-' + p.Number);
          //Move to the next Period
          continue;

        }

        //Add trailing periods if the end date period is not the last period of its kind for a given FY
        if(p.StartDate > endDate ){

          if((periodType == QUARTER && p.Number == 1) || 
             (periodType == MONTH && p.Number == 7)){

            //If start date of the period is beyond the given end date and the period is the first of it's kind in the fiscal year, 
            //then  break the loop. If not, then proceed to add this period
            break;  
          }

          periods.add(p);
          //system.debug('***Added Trailing***' + 'FY-' + p.FiscalYearSettings.Name + ' PeriodLabel-' + p.FullyQualifiedLabel + ' Number-' + p.Number);

          //September is 9, so the end month is compared to 9
          if((periodType == QUARTER && p.Number == 4) || 
             (periodType == MONTH && p.Number == 6)){

            //If start date of the period is beyond the given end date and the period is the last of it's kind in the fiscal year, 
            //then  break the loop
            break;  
          }
        }

      }
    } //END FOR
    return periods;
  }

  /*
  * Invocable method to recalculate the spread for a list of MultiOffices. 
  * Used by the 'Create Spread on Multi Office Upsert' process builder on Multi Office create/edit. 
  */
  @InvocableMethod(label='Calculate spread' description='Calculates spread')
  public static void calculateSpread(List<Multi_Office_Split__c> multiOfficeSplits){

    //list of Opportunity Ids
    list<Id> mosIds = new List<Id>();

    //Get the Opportunity Ids 
    for(Multi_Office_Split__c currentMOS: multiOfficeSplits)
    {
      mosIds.add(currentMOS.Id);
    }

    //Delete existing spreads
    ForecastSpread_Service.deleteRevenue(mosIds);

    //Re-create the spread
    createSpread(multiOfficeSplits, 
                 new Set<String>{ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD, ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD});
  }   

  /*
   * Method to create revenue schedule
   * Params:
   * List<Multi_Office_Split__c> multiOfficeRecords - List of Multi Office records that need revenue schedules
   * String spreadFormula                           - Spreading Formula for the Multi Office
   */
  public static void createSpread(List<Multi_Office_Split__c> multiOfficeRecords, Set<String> periodType) {

    Date earliestMOStartDate; //Earliest start date from the list of Multi Office start dates
    Date latestMOStartDate;   //Latest end date from the list of Multi Office end dates

    //Loop Through Multi Office to determine the earliest start date and the latest end date
    for(Multi_Office_Split__c mos: multiOfficeRecords){
      
      //Set Start Date
      if(earliestMOStartDate==null){
        earliestMOStartDate = mos.Start_Date__c;
      }
      else if(earliestMOStartDate > mos.Start_Date__c){
        earliestMOStartDate = mos.Start_Date__c;
      }

      //Set End Date
      if(latestMOStartDate==null){
        latestMOStartDate = mos.End_Date__c;
      }
      else if(latestMOStartDate < mos.End_Date__c){
        latestMOStartDate = mos.End_Date__c;
      }

    }

    //Get Fiscal Periods for earliest start date and latest end date
    List<Period> fiscalPeriodsList = ForecastSpread_Service.getFiscalPeriods(new List<String>{ForecastSpread_Service.QUARTER, ForecastSpread_Service.MONTH}, 
                                                                             earliestMOStartDate,
                                                                             latestMOStartDate);

    //Get recordtypes for Quarter and Monthly
    for(RecordType rt: Utility.getRecordTypes(ForecastSpread_Service.OBJ_NAME_REVENUE_SCHEDULE, 
                                              new List<String>{ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD, 
                                                               ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD})){
      recordTypeMap.put(rt.DeveloperName, rt.Id);
    } //END FOR


    //List to add Revenue Schedule records
    List<Revenue_Schedule__c> newSpread = new List<Revenue_Schedule__c>();

    for(Multi_Office_Split__c multiOfficeRecord : multiOfficeRecords) {

      if(periodType.contains(ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD)){
        //Add revenue schedules for Quarter
        newSpread.addAll(prepareNewRevenueSchedules(fiscalPeriodsList,
                                                    ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD, 
                                                    multiOfficeRecord));        
      }

      if(periodType.contains(ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD)){
        //Add revenue schedules for Months
        newSpread.addAll(prepareNewRevenueSchedules(fiscalPeriodsList,
                                                    ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD, 
                                                    multiOfficeRecord));   
      }

    }//END FOR

    //Insert th new spread
    insert newSpread;
  } 

  /*
  * Method to prepare the list of RevenueSchedules to be inserted
  * Params:
  * List<Period> fiscalPeriodsList - Filtered list of Periods for the given start and end date
  * String rtDevName               - Revenue Schedule record type dev name
  * Multi_Office_Split__c mos      - Multi Office split that needs revenue schedules calculated
  * String spreadFormula           - Spreading Formula for the Multi Office
  */
  public static List<Revenue_Schedule__c> prepareNewRevenueSchedules(List<Period> fiscalPeriodsList,String rtDevName, Multi_Office_Split__c mos){

    //List of Revenue Schedule to be returned
    List<Revenue_Schedule__c> revenueSchedulesList = new List<Revenue_Schedule__c>();

    String periodType; //PeriodTYpe based on the recordtype provided

    if(rtDevname == REC_TYPE_QUARTERLY_SPREAD){
      periodType = QUARTER;
    }
    else if(rtDevname == REC_TYPE_MONTHLY_SPREAD){
      periodType = MONTH;
    }

    //Get Fiscal Periods for the multi Office Split based on the Start and End Date
    List<Period> mosFiscalPeriods = ForecastSpread_Service.filterFiscalPeriods(fiscalPeriodsList, 
                                                                               mos.Start_Date__c, 
                                                                               mos.End_Date__c, 
                                                                               periodType);

    //Loop through Fiscal Periods and spread the budget evenly
    for (Period currentPeriod: mosFiscalPeriods) {

        //Initiate a new record
        Revenue_Schedule__c newScheduleRecord = new Revenue_Schedule__c();
        //Set Fiscal Year
        newScheduleRecord.Fiscal_Year__c      = 'FY ' + currentPeriod.FiscalYearSettings.Name;
        //Set the MOS
        newScheduleRecord.Multi_Office_Split__c = mos.Id;
        //Set the Period Label
        if(rtDevName == ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD){
          newScheduleRecord.Reporting_Quarter__c  = 'Q' + 
                                                    currentPeriod.Number + ForecastSpread_Service.SPACE + 
                                                    currentPeriod.FullyQualifiedLabel.right(7).left(2) +
                                                    currentPeriod.FullyQualifiedLabel.right(2);  
        }
        else if(rtDevName == ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD){

          newScheduleRecord.Reporting_Month__c    = currentPeriod.FullyQualifiedLabel.left(3).toUpperCase() + 
                                                    ForecastSpread_Service.SPACE + 
                                                    currentPeriod.FullyQualifiedLabel.right(7).left(2) +
                                                    currentPeriod.FullyQualifiedLabel.right(2);

          //US2497 - Populating the new custom field reporting month pl to be able to sort on reports
          newScheduleRecord.Reporting_Month_pl__c    = currentPeriod.FullyQualifiedLabel.left(3).toUpperCase();
          Integer quarterNumber;           //Quarter Number

          //Fiscal Months are numbered as per calendar year month. 
          //So July is 7. However October is in Q1, hence the number is set as 1.
          //Similar logic applies to other months.
           if(currentPeriod.Number >= 7 && currentPeriod.Number <=9){
            quarterNumber = 1;
          }
          else if(currentPeriod.Number >= 10 && currentPeriod.Number <=12){
            quarterNumber = 2;
          }
          else if(currentPeriod.Number >= 1 && currentPeriod.Number <=3){
            quarterNumber = 3;
          }
          else if(currentPeriod.Number >= 4 && currentPeriod.Number <=6){
            quarterNumber = 4;
          }
            
          //Set reporting quarter label
          newScheduleRecord.Reporting_Quarter__c  = 'Q' + 
                                                    quarterNumber +
                                                    ForecastSpread_Service.SPACE + 
                                                    currentPeriod.FullyQualifiedLabel.right(7).left(2) +
                                                    currentPeriod.FullyQualifiedLabel.right(2);
        }

        //Set Period End Date
        newScheduleRecord.Quarter_End_Date__c = currentPeriod.EndDate;

        //Set Record Type
        newScheduleRecord.RecordTypeId = recordTypeMap.get(rtDevName);

        //If the periods fall out of the Multi Office start and end period then the null dates. 
        //This will make Probable GM = 0 for Linear spread. 
        //Probable GM needs to be made 0 explicitly for Front Load, Standard and Back Load
        if(currentPeriod.EndDate < mos.Start_Date__c || 
           currentPeriod.StartDate > mos.End_Date__c){

          //Null the dates so that the Probable GM calcualtion will result in 0
          newScheduleRecord.Start_Date__c = null;
          newScheduleRecord.End_Date__c   = null;

          if(mos.Spreading_Formula__c != LINEAR){
            newScheduleRecord.Probable_GM__c = 0;
          }

          revenueSchedulesList.add(newScheduleRecord); 
          continue;
        }
        //Logic for setting revenu schedule field values that fall in the Multi Office date range
        else{

          //If the MOS Start Date is greater than or equal to the period startdate then set that as the start date of Revenue Schedule
          //Else the start date of the Period is the start date for revenue schedule
          if(currentPeriod.StartDate <= mos.Start_Date__c){
            newScheduleRecord.Start_Date__c = mos.Start_Date__c;            
          }
          else{
            newScheduleRecord.Start_Date__c = currentPeriod.StartDate;
          }

          //If the MOS endDate is less than or equal to the period endDate then set that as the endDate of Revenue Schedule
          //Else the end date of the Period is the end date for revenue schedule
          if (currentPeriod.EndDate >= mos.End_Date__c) {
            newScheduleRecord.End_Date__c = mos.End_Date__c;
          } else {
            newScheduleRecord.End_Date__c = currentPeriod.EndDate;
          }          
        }

        //Logic to calculate Probable GM for Frontload, Standard and Backload spread
        if(mos.Spreading_Formula__c != LINEAR){

          /* US2406: Commenting out following code. New function is being used to calculated CDF
          //Variables needed for S-Curve Spread
          //Standard deviation in days
          Double sdDays = mos.Standard_Deviation_in_Days__c;

          //Variance
          Double variance = sdDays * sdDays;

          //Mean
          Double mean = Double.valueOf(mos.Number_of_Days__c / 2); 
                   
          //Calculate CDF for currentPeriod Start Date
          Double startX = 0;
          if (currentPeriod.StartDate >= mos.Start_Date__c) {
              startX = mos.Start_Date__c.daysBetween(currentPeriod.StartDate);

          } else {
              startX = 0;
          }

          //Cumulative Distribution Function value for number of days corresponding to the Quarter Start Date
          Double quarterStartDateCDF = cdf(startX,mean,variance);

          Double endX = 0;
          if (mos.End_Date__c > currentPeriod.EndDate) {
              endX = mos.Start_Date__c.daysBetween(currentPeriod.EndDate) + 1;
          } else {
              endX = mos.Start_Date__c.daysBetween(mos.End_Date__c) + 1;
          }

          //Cumulative Distribution Function value for number of days corresponding to the Quarter End Date
          Double quarterEndDateCDF = cdf(endX, mean, variance);

          Double denominator = 1 - 2 * cdf(0, mean, variance);

          //Calculate the revenue by determining the area between start and end cumulative functions
          newScheduleRecord.Probable_GM__c = (quarterEndDateCDF - quarterStartDateCDF) * mos.probable_GM__c / denominator;
          
          END - US2406 commenting*/

          //US2406: New logic to calculate the Probable GM for each period.
          //Constants used in the Beta Distribution for calculation of Cumulative Distribution Function(CDF)
          Double constantA;
          Double constantB;

          //Constants used in Beta Distribution to indicate period fraction
          Double startT;
          Double endT;          

          if(mos.Spreading_Formula__c == FRONTLOAD){
            //Constants for Frontload
            constantA = 1.00;
            constantB = 0.00;            
          }
          else if(mos.Spreading_Formula__c == STANDARD){
            //Constants for Standard
            constantA = 0.00;
            constantB = 1.00;             
          }
          else if(mos.Spreading_Formula__c == BACKLOAD){
            //Constants for Backload 
            constantA = 0.00;
            constantB = 0.00;             
          }

          //Period start date is before the MOS start date and period end date is after MOS start date but before MOS end date
          if(currentPeriod.StartDate < mos.Start_Date__c && 
             currentPeriod.EndDate >= mos.Start_Date__c &&
             currentPeriod.EndDate <= mos.End_Date__c){
            startT = 0.00;
            endT = (mos.Start_Date__c.daysBetween(currentPeriod.EndDate) + 1) / (mos.Number_Of_Days__c);
          } 
          
          //Period start date is after MOS start date or equal to MOS start date
          //Period end date is before or equal to MOS end date
          else if( currentPeriod.StartDate >= mos.Start_Date__c && 
                   currentPeriod.EndDate <= mos.End_Date__c){
            startT = (mos.Start_Date__c.daysBetween(currentPeriod.StartDate)) / (mos.Number_Of_Days__c);
            endT   = (mos.Start_Date__c.daysBetween(currentPeriod.EndDate) + 1) / (mos.Number_Of_Days__c);
          }

          //Period start date is greater than MOS  start date but less than MOS end date
          //Period end date is greater than MOS end date
          else if(currentPeriod.StartDate >= mos.Start_Date__c && 
                  currentPeriod.StartDate <= mos.End_Date__c && 
                  currentPeriod.EndDate >= mos.End_Date__c){
            startT = (mos.Start_Date__c.daysBetween(currentPeriod.StartDate)) / (mos.Number_Of_Days__c);
            endT   = 1.00;
          }

          //Period start date is less that MOS start date and Period End Date is greater than the MOS End Date
          //MOS is totally contained by a single period
          else if(currentPeriod.StartDate < mos.Start_Date__c && 
                  currentPeriod.EndDate > mos.End_Date__c){
            startT = 0.00;
            endT   = 1.00;
          }
 
          //Get the probable GM for the selected formula
          newScheduleRecord.Probable_GM__c = (betaCDF(constantA, constantB, endT) - betaCDF(constantA, constantB, startT)) * mos.probable_GM__c; 

        }       

        //Insert it to the revenue schedule list
        revenueSchedulesList.add(newScheduleRecord);      
    }//END FOR

    return revenueSchedulesList;
  }  

  //Method to calculate the error function for calculating the Normal value
  //US2406: Commenting out this method since new function is used for calculation
  /*public static Double erf(Double x) {

      //Save the sign for x
      Integer sign = x >= 0 ? 1 : -1;

      //Get the Absolute value of x
      Double absoluteX = Math.abs(x);

      // constants
      Double a1 = 0.254829592;
      Double a2 = -0.284496736;
      Double a3 = 1.421413741;
      Double a4 = -1.453152027;
      Double a5 = 1.061405429;
      Double p = 0.3275911;

      //Calculate Error Function based on the formula
      Double t = 1.0 / (1.0 + (p * absoluteX));

      Double erfValue = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.exp(-1.0 * absoluteX * absoluteX);

      return sign * erfValue;

  }*/

  //Method to calculate Cumulative Distribution Function
  //US2406: Commenting out this method since new function is used for calculation
  /*public static Double cdf(Double x, Double mean, Double variance) {
      return 0.5 * (1 + erf((x - mean) / (Math.sqrt(2 * variance))));
  }*/

  /* US2406: New method to calculate CDF using the Polynomial approximation of the Beta Distribution
  * https://en.wikipedia.org/wiki/Beta_distribution
  * Params: 
  * A - Beta Distribution Shape Parameter. 0 <= A <= 1
  * B - Beta Distribution Shape Parameter. 0 <=B <= 1
  * T - Fraction of the total period. 
  *     For example, if total period is 10 days, T for the first day is 0.1.
  *     T for the second day is 0.2 and so on. 
  */
  public static Double betaCDF(Double A, Double B, Double T) {
    return 10 * T * T * (1 - T) * (1 - T) * (A + B * T) + (T * T * T * T * (5 - 4 * T)); 
  } 

}