Public with sharing class PF_AddEditTCSteps {  
    public static List<PF_Test_Case_Step__c> testCaseStpList = new List<PF_Test_Case_Step__c>();
    public static List<PF_Test_Case_Step__c> existingTestCaseStps = new List<PF_Test_Case_Step__c>();
    public static List<PF_Test_Case_Step__c> testCaseAssStpDel = new List<PF_Test_Case_Step__c>();
    
    @AuraEnabled
    public static String upsertTestCaseSteps(List<PF_Test_Case_Step__c> testCaseStps, List<Id> delAssigne){  
         System.debug('------ Entered saveRecords method');
        system.debug('testCaseStps'+testCaseStps.Size());
        system.debug('delAssigne'+delAssigne.Size());
        
        string msg;
       /*if(delAssigne.size() > 0){
             testCaseAssStpDel =  [Select id from PF_Test_Case_Step__c where id IN : delAssigne];
            delete testCaseAssStpDel;
             msg='SUCCESS';
        }*/
        if(testCaseStps.size() > 0){            
            try{
                system.debug('pff1'+testCaseStps);
                for (PF_Test_Case_Step__c pf : testCaseStps) {
                    system.debug('pf'+pf);
                    PF_Test_Case_Step__c pf1 = new PF_Test_Case_Step__c(PF_Step_Description__c=pf.PF_Step_Description__c, 
                                                                             PF_Expected_Result__c=pf.PF_Expected_Result__c, 
                                                                             PF_Actual_Result__c= pf.PF_Actual_Result__c, 
                                                                             PF_Test_Case__c=pf.PF_Test_Case__c,Id=pf.id,
                                                                             PF_Step_Number__c =pf.PF_Step_Number__c);
                    testCaseStpList.add(pf1);
                }
                system.debug('pff'+testCaseStpList);
                upsert testCaseStpList;
                msg='SUCCESS';
            }catch(Exception ex){
                msg='Error';
                system.debug('Here is Exception ' + ex.getMessage());
            }
        }
        return msg;
    }
  @AuraEnabled
    public static String deleteMiddleTestCaseSteps(List<PF_Test_Case_Step__c> testCaseStps, List<Id> delAssigne,string indexvalue){  
         System.debug('------ Entered saveRecords method');
        system.debug('testCaseStps'+testCaseStps.Size());
        system.debug('delAssigne'+delAssigne.Size());
        system.debug('delAssigne'+indexvalue);
        
       return null;
    }
    @AuraEnabled
    public static List<PF_Test_Case_Step__c> getRecords (Id testCase) {
        System.debug('----- Entered getRecords method');
        existingTestCaseStps = [Select id,name,PF_Step_Description__c,PF_Expected_Result__c,PF_Test_Case__c,PF_Step_Number__c from PF_Test_Case_Step__c where PF_Test_Case__c =: testCase order By PF_Step_Number__c];
        System.debug('----- Entered testCaseAssList : ' + existingTestCaseStps.size());
        system.debug('Entered testCaseAssList : ' + existingTestCaseStps);
        return existingTestCaseStps;            
    }
    @AuraEnabled
    public static string deleteSeleted (List<Id> testStpsDel,Id testCase) {
        system.debug('testStpsDel'+testStpsDel);
        system.debug('testCaseStps'+testCase);
        list<PF_Test_Case_Step__c> checkValueTest = new list<PF_Test_Case_Step__c>();
        list<PF_Test_Case_Step__c> checkValueTestMax = new list<PF_Test_Case_Step__c>();

        integer totlCount = [select count() from PF_Test_Case_Step__c where id IN : testStpsDel ];
        system.debug('totlCount'+totlCount);
        AggregateResult[] result = [select min(PF_Step_Number__c)stpno from PF_Test_Case_Step__c where id IN : testStpsDel ];
        system.debug('result'+result);
        String str = String.valueOf(result[0].get('stpno')) ;
			Integer I = Integer.valueOf(str) ;
        system.debug('stepno'+i);
        AggregateResult[] resultMax = [select max(PF_Step_Number__c)stpno1 from PF_Test_Case_Step__c where id IN : testStpsDel ];
        system.debug('result'+resultMAx);
        String strMAX = String.valueOf(resultMAx[0].get('stpno1')) ;
			Integer IMAX = Integer.valueOf(strMAX) ;
        system.debug('stepno'+IMAX);
   list<PF_Test_Case_Step__c> checkValueMax = [select id,PF_Step_Number__c from PF_Test_Case_Step__c where PF_Test_Case__c=:testCase AND PF_Step_Number__c >: i AND PF_Step_Number__c <:IMAX ];
		system.debug('checkValueMax'+checkValueMax);
        for(PF_Test_Case_Step__c pf:checkValueMax)
        {
            pf.PF_Step_Number__c= pf.PF_Step_Number__c -1;
            pf.PF_Test_Case_Step_Unique__c =testCase + String.ValueOf(pf.PF_Step_Number__c);
            system.debug('pf.PF_Test_Case_Step_Unique__c'+pf.PF_Test_Case_Step_Unique__c);
            checkValueTestMax.add(pf);
            system.debug('checkValueTestMax'+checkValueTestMax);
        }
        update checkValueTestMax;
        system.debug('count delete'+[select PF_Step_Number__c from PF_Test_Case_Step__c where id IN : testStpsDel ]);

        system.debug('count delete'+[select max(PF_Step_Number__c) from PF_Test_Case_Step__c where PF_Test_Case__c=:testCase ]);
	list<PF_Test_Case_Step__c> checkValue = [select id,PF_Step_Number__c from PF_Test_Case_Step__c where PF_Test_Case__c=:testCase AND PF_Step_Number__c >: IMAX];
       system.debug('check aftr delete'+checkValue) ;
        for(PF_Test_Case_Step__c pf:checkValue)
        {
            pf.PF_Step_Number__c= pf.PF_Step_Number__c -totlCount;
            system.debug('testCase'+testCase);
            system.debug('String.ValueOf(pf.PF_Step_Number__c)'+String.ValueOf(pf.PF_Step_Number__c));
            pf.PF_Test_Case_Step_Unique__c = testCase + String.ValueOf(pf.PF_Step_Number__c);
            system.debug('pf.PF_Test_Case_Step_Unique__c'+pf.PF_Test_Case_Step_Unique__c);
            checkValueTest.add(pf);
            system.debug('checkValueTest'+checkValueTest);
        }
        update checkValueTest;
        
            string msg;
       if(testStpsDel.size() > 0){
            testCaseAssStpDel =  [Select id,PF_Step_Number__c from PF_Test_Case_Step__c where id IN : testStpsDel];
           system.debug('testCaseAssStpDel'+testCaseAssStpDel);
            delete testCaseAssStpDel;
             msg='SUCCESS';
        }
        return msg;            
    }
  @AuraEnabled
    public static string deleteSeletedSingle (List<Id> testStpsDel,Id testCase) {
        system.debug('testStpsDel'+testStpsDel);
        system.debug('testCaseStps'+testCase);
        Integer I;
        list<PF_Test_Case_Step__c> checkValueTest = new list<PF_Test_Case_Step__c>();
        integer totlCount = [select count() from PF_Test_Case_Step__c where id IN : testStpsDel ];
        system.debug('totlCount'+totlCount);
        AggregateResult[] result = [select max(PF_Step_Number__c)stpno from PF_Test_Case_Step__c where id IN : testStpsDel ];
        system.debug('result'+result);
       
        String str = String.valueOf(result[0].get('stpno')) ;
		I = Integer.valueOf(str) ;
        system.debug('stepno'+i);
       
        system.debug('count delete'+[select PF_Step_Number__c from PF_Test_Case_Step__c where id IN : testStpsDel ]);

        system.debug('count delete'+[select max(PF_Step_Number__c) from PF_Test_Case_Step__c where PF_Test_Case__c=:testCase ]);
	list<PF_Test_Case_Step__c> checkValue = [select id,PF_Step_Number__c from PF_Test_Case_Step__c where PF_Test_Case__c=:testCase AND PF_Step_Number__c >: i];
       system.debug('check aftr delete'+checkValue) ;
        for(PF_Test_Case_Step__c pf:checkValue)
        {
            pf.PF_Step_Number__c= pf.PF_Step_Number__c -totlCount;
             pf.PF_Test_Case_Step_Unique__c = testCase + String.ValueOf(pf.PF_Step_Number__c);
            system.debug('pf.PF_Test_Case_Step_Unique__c'+pf.PF_Test_Case_Step_Unique__c);
            checkValueTest.add(pf);
            system.debug('checkValueTest'+checkValueTest);
        }
        update checkValueTest;
        
            string msg;
       if(testStpsDel.size() > 0){
            testCaseAssStpDel =  [Select id,PF_Step_Number__c from PF_Test_Case_Step__c where id IN : testStpsDel];
           system.debug('testCaseAssStpDel'+testCaseAssStpDel);
            delete testCaseAssStpDel;
             msg='SUCCESS';
        }
        return msg;            
    }
    
  
    
    @AuraEnabled
    public static boolean checkPageAccess(){
        boolean notHavingAccess = false;
        notHavingAccess = PF_Utility.checkPageAccess('PF_ProjectForce_Read_Only');
        return notHavingAccess;    
    }
     @AuraEnabled
    public static List<PF_Test_Case_Step__c> getTestCaseWithStep (Id tcsId) {
        System.debug('----- Entered getRecords method');
        existingTestCaseStps = [Select id,name, PF_Step_Description__c,PF_Expected_Result__c,PF_Actual_Result__c, PF_Test_Case__c,PF_Step_Number__c from PF_Test_Case_Step__c where PF_Test_Case__c =:tcsId];
        System.debug('----- Entered testCaseAssList : ' + existingTestCaseStps.size());
        System.debug('----- Entered testCaseAssList : ' + existingTestCaseStps);
        return existingTestCaseStps;            
    }
     @AuraEnabled
    public static void updateTestCaseStepInMiddle (Id tcsId, string IndexVar) {
        System.debug('----- Entered getRecords method');
        list<PF_Test_Case_Step__c> tcsList = new list<PF_Test_Case_Step__c>();
        existingTestCaseStps = [Select id,name, PF_Step_Description__c,PF_Expected_Result__c,PF_Actual_Result__c, PF_Test_Case__c,PF_Step_Number__c from PF_Test_Case_Step__c where id =:tcsId];
      	//existingTestCaseStps.PF_Step_Number__c = 
      for(PF_Test_Case_Step__c ptcs:existingTestCaseStps)
      {
          ptcs.PF_Step_Number__c = Integer.valueof(IndexVar.trim());
          //ptcs.PF_Test_Case_Step_Unique__c = ptcs.PF_Test_Case__c+IndexVar;
              system.debug('ptcs.PF_Test_Case_Step_Unique__c'+ptcs.PF_Test_Case_Step_Unique__c);
          tcsList.add(ptcs);
      }
      System.debug('----- Entered testCaseAssListtcsList : ' + tcsList);
       update tcsList;
        System.debug('----- Entered testCaseAssList : ' + existingTestCaseStps);
        System.debug('----- Entered testCaseAssListIndexVar : ' + IndexVar);
    }
}