public with sharing class LibraryRecordTypes_Controller{

    @AuraEnabled
    //public static List<RecordType> getLibRecordTypes(){
      public static List<String> getpickval() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = RecordType.Name.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }       
        return options;
    }
       //List<RecordType> options = new List<RecordType>();
    
        
        //  options = [SELECT Name FROM RecordType WHERE SObjectType = 'Library__c' order by Name
        //        ];
        //      return options;
          
    //}
    }