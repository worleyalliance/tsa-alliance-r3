@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint and method.
        //System.assertEquals('/services/data/v44.0/ui-api/object-info/Review__c/picklist-values/0122C000000Crz6QAC/', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
		res.setBody('{"controllerValues":{},"defaultValue":null,"eTag":"84ebd0e093959f3e5ded0b1c45f3faae","url":"/services/data/v44.0/ui-api/object-info/Review__c/picklist-values/0122C000000CrzBQAS/Decision_authority_on_behalf_of_client__c","values":[{"attributes":null,"label":"Yes","validFor":[],"value":"Yes"},{"attributes":null,"label":"No","validFor":[],"value":"No"}]}');
        res.setStatusCode(200);
        return res;
    }
}