public class RA_ReviewToCSVController {
    
    @AuraEnabled
    public static csvReturnData fetchReview(Id reviewId){
        
        //Review__c returnReview = new Review__c();
        Review__c returnReview = [SELECT Id, RecordType.Name FROM Review__c WHERE Id = :reviewId];
        System.debug('returnReview: ' + returnReview);
        List<Map<String, String>> fieldNames = getAPINames(returnReview);
        List<String> fieldNameList = new List<String>();
        for(Map<String, String> m : fieldNames){
            fieldNameList.add(m.get('apiName'));
        }
        String queryString = 'SELECT ';
        String fieldString = String.join(fieldNameList, ', ');
        queryString += fieldString + ' FROM Review__c WHERE Id = :reviewId';
        System.debug('queryString: ' + queryString);
        returnReview = Database.query(queryString);
        System.debug('returnReview: ' + returnReview);
        
        for(Map<String, String> m: fieldNames){
            System.debug('m: ' + m);
            String thisValue;
            
            if(m.get('apiName').contains('.')){
                String apiName = m.get('apiName');
                //apiName = apiName.replace('__r', '__c');
                String api1 = apiName.subString(0, apiName.indexOf('.'));
                String api2 = apiName.substring(apiName.indexOf('.') + 1, apiName.length());
                sObject parent = returnReview.getSObject(api1);
                if(parent != null){
                    thisValue = String.valueOf(parent.get(api2));
                } else {
                    thisValue = '';
                }
                
            } else {
                String apiName = m.get('apiName');
                thisValue = String.valueOf(returnReview.get(apiName));
            }
            
            
            if(thisValue == null){
                thisValue = '';
            }
            if(thisValue.contains('"')){
                thisValue = thisValue.replaceAll('"', '\'');
            }
            m.put('value', thisValue);
            
        }
        
        csvReturnData returnData = new csvReturnData();
        returnData.theReview = returnReview;
        returnData.fieldNames = fieldNames;
        
        System.debug('returnData: ' + returnData);
        return returnData;
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getAPINames(Review__c rev){
        
        List<Map<String, String>> returnList = new List<Map<String, String>>();
        
        SObjectType reviewType = Schema.getGlobalDescribe().get('Review__c');
        Map<String, Schema.SObjectField> rFields = reviewType.getDescribe().fields.getMap();
        
        for(String s : rFields.keySet()){
            if(rFields.get(s).getDescribe().isAccessible()){
                String apiName = rFields.get(s).getDescribe().getName();
                Map<String, String> newMap = new Map<String, String>();
                String label = rFields.get(s).getDescribe().getLabel();
                System.debug(s + ' type: ' + rFields.get(s).getDescribe().getType());
                if(rFields.get(s).getDescribe().getType() == Schema.DisplayType.REFERENCE){
                    String newS = rFields.get(s).getDescribe().getRelationshipName();
                    newS += '.Name';
                    newMap.put('apiName', newS);
                } else {
                    newMap.put('apiName', apiName);
                }
                newMap.put('Name', label);
                returnList.add(newMap);
            }
        }
        
        System.debug('returnList: ' + returnList);
        return returnList;
        
    }
    
    public class csvReturnData{
        
        @AuraEnabled
        Public Review__c theReview {get; set;}
        @AuraEnabled
        Public List<Map<String, String>> fieldNames {get; set;}
        
        public csvReturnData(){
            
        }
        
    }
    
}