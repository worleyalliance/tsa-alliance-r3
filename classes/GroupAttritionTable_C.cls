/**************************************************************************************
Name: GroupAttritionTable_C
Version: 1.0 
Created Date: 07.03.2017
Function: Table component to display group attrition object elements for given group id.
            COMPONENT WORKS ONLY WITH FiscalYearSelector component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.03.2017         Original Version
*************************************************************************************/
public with sharing class GroupAttritionTable_C {

    private static GroupAttritionTable_Service service = new GroupAttritionTable_Service();

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    @AuraEnabled
    public static DataTable_Service.TableData getData(
            String fieldSetName,
            Id groupId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault,
            String additionalValueField
    ) {
        return service.getSegmentAttritionData(
                fieldSetName,
                groupId,
                fiscalYear,
                fiscalYearField,
                relationField,
                sObjectName,
                totalRowPresent,
                isSumDefault,
                additionalValueField
        );
    }

}