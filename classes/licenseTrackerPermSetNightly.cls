/**************************************************************************************
Name: licenseTrackerPermSetNightly                
Version: 1.0 
Created Date: March 10, 2018
Function: Update License Request record with related user's Permission Set Assignments.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Ray Zhao                  03/10/2018          Original
* Ray Zhao                  03/21/2018          Bugfix on Syncing up Global Value Set
* Jignesh Suvarna           07/30/2018          Code Optimization to avoid
                                                CPU time limit error #DE590
*************************************************************************************/
global class licenseTrackerPermSetNightly implements Schedulable {

    public void execute(SchedulableContext SC){
        /* JS Moving below code to Batch Class licenseTrackerPermSetNightlyBatch Class's Execute method
    
        // Initialize Log Entries and licenseTrackerUtilities class
        List<Log__c> logEntries = new List<Log__c>();
        licenseTrackerUtilities ltu = new licenseTrackerUtilities();
        
        //Build PermissionSetAssignment Map<AssigneeId, String - List of PermSet delimited by ';'> 
        List<PermissionSetAssignment> psaList = [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSet.IsOwnedByProfile = FALSE ORDER BY AssigneeId];
        System.debug(loggingLevel.INFO, 'Number Of Assignements =' + psaList.size());
        Map<Id, String> userPermSetNamesMap = new Map<Id, String>();
        for (PermissionSetAssignment psa : psaList){
            if (userPermSetNamesMap.containsKey(psa.AssigneeId)){
                userPermSetNamesMap.put(psa.AssigneeId,(userPermSetNamesMap.get(psa.AssigneeId) + ';' + ltu.getPermSetNameById(psa.PermissionSetId)));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Had =' + userPermSetNamesMap.get(psa.AssigneeId));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Add =' + ltu.getPermSetNameById(psa.PermissionSetId));
            }
            else {
                userPermSetNamesMap.put(psa.AssigneeId,ltu.getPermSetNameById(psa.PermissionSetId));
                //System.debug(loggingLevel.INFO, psa.AssigneeId + ' - ' + ' Empty, Adding =' + ltu.getPermSetNameById(psa.PermissionSetId));            
            }
        }
        System.debug(loggingLevel.INFO, 'userPermSetNamesMap =' + userPermSetNamesMap);
        System.debug(loggingLevel.INFO, 'userPermSetNamesMap size =' + userPermSetNamesMap.size());
    
        //Build License Request List<License_Request__c>
        List<License_Request__c> lrList = [SELECT Id, Requested_Permission_Sets__c, Provisioned_User__c FROM License_Request__c 
                                           WHERE Provisioned_User_Status__c = TRUE AND Provisioned_User__c in :userPermSetNamesMap.keySet() AND
                                           RecordTypeId = :Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('Master License Request').getRecordTypeId()];
        System.debug(loggingLevel.INFO, 'LR List size =' + lrList.size());
        
        //Loop through the License Request Maps, Set Permission Set Values
        for (License_Request__c lr : lrList){
            if(userPermSetNamesMap.get(lr.Provisioned_User__c) != NULL){
                if(lr.Requested_Permission_Sets__c != userPermSetNamesMap.get(lr.Provisioned_User__c))
                    lr.Requested_Permission_Sets__c = userPermSetNamesMap.get(lr.Provisioned_User__c);
            }
            else{
                System.debug(loggingLevel.INFO,'That\'s the null pointer: ' + lr.Provisioned_User__c);            
                logEntries.add(new Log__c(AppName__c = 'License Management',
                                          ClassName__c = 'licenseTrackerPermSetNightly',
                                          Event__c = 'Night Scheduleable',
                                          EventType__c = 'AppException',
                                          Operation__c = 'Lookup User Id in the userPermSet Map',
                                          SFID__c = lr.Id,
                                          Exception__c = 'User Id ' + lr.Provisioned_User__c + ' not found in userPermSetmap'));
            }
        }
        System.debug(loggingLevel.INFO,'Number of unfound users: ' + logEntries.size());            
        
        try {
            
            // JS 07/30/2018 Using Database.Update instead of update
            //Update lrList;
            Database.SaveResult[] srList = Database.update(lrList, false);
            
            //Insert log entries
            if (logEntries.size()>0) insert logEntries;
        }
        catch (Exception e){
            System.Debug(e);
            logEntries.add(new Log__c(AppName__c = 'License Management',
                                      ClassName__c = 'licenseTrackerPermSetNightly',
                                      Event__c = 'Night Scheduleable',
                                      EventType__c = 'AppException',
                                      Operation__c = 'Unknown exception occurred, Logging UserId',
                                      SFID__c = userInfo.getUserId(),
                                      Exception__c = 'Exception: ' + e));
            insert logEntries;
        }
        /**************************************************************************************************************************************************/
        
        //JS 07/30/2018 - Calling Batch class to execute the Jobs in batches of 200 to avoid CPU time limit
        Id batchJobId = Database.executeBatch(new licenseTrackerPermSetNightlyBatch(), 200);
        system.debug('Batch Job Id '+batchJobId);
    }
}