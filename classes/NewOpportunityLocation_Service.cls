/**************************************************************************************
Name: NewOpportunityLocation_Service
Version: 1.0 
Created Date: 26.04.2017
Function: Adds new Location to Oppotunity base on Account locations

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   26.04.2017         Original Version
* Chris Cornejo     8/28/2017          US823 - Added filter to only show active account locations on the new Opp Location dropdown lightning component.
* Chris Cornejo     12/11/2017         US1354 - Added end client active locations to Opportunity Locations dropdown list.
*************************************************************************************/
public with sharing class NewOpportunityLocation_Service extends BaseComponent_Service{

   /*
    * Returns list of locations attached to account related to given opportunity
    */
    public List<PicklistValue> retrieveAccountLocations(Id opportunityId){
        validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());
        validateObjectAccesible(Account.sObjectType.getDescribe().getName());
        validateObjectAccesible(SiteLocation__c.sObjectType.getDescribe().getName());

        List<PicklistValue> picklistValues = getAccountLocationsPicklistValues(opportunityId);
        return picklistValues;
    }

    private List<PicklistValue> getAccountLocationsPicklistValues(Id opportunityId){
        List<PicklistValue> picklistValues = new List<PicklistValue>();
        List<SiteLocation__c> siteLocations = [
                SELECT Name
                FROM SiteLocation__c
                WHERE Inactive__c = false AND Account__c IN
                    (SELECT
                        AccountId
                    FROM Opportunity
                    WHERE Id = :opportunityId)
                ORDER BY CreatedDate
        ];
        
        //US1354
        List<SiteLocation__c> siteLocationsEndClient = [
                SELECT Name
                FROM SiteLocation__c
                WHERE Inactive__c = false AND Account__c IN
                    (SELECT
                        End_Client__c
                    FROM Opportunity
                    WHERE Id = :opportunityId)
                ORDER BY CreatedDate
        ];
        
        siteLocations.addAll(siteLocationsEndClient);
        
        for(SiteLocation__c location : siteLocations){
            picklistValues.add(new PicklistValue(location.Name, location.Id));
        }
        return picklistValues;
    }

   /*
    * It returns only opportunity name for given opportunity id
    */
    public String retrieveOpportunityName(Id opportunityId){
        validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());
        List<Opportunity> opportunities = [SELECT Name FROM Opportunity WHERE Id = :opportunityId];
        if(opportunities.isEmpty()){
            throw new AuraHandledException(Label.RecordIsNotAvailable + ' : '+opportunityId);
        }
        return opportunities[0].Name;
    }

   /*
    * Saves new junction relation between opportunity and account location
    */
    public Id saveOpportunityLocation(Id opportunityId, Id locationId){
        validateObjectCreateable(Opportunity_Locations__c.sObjectType.getDescribe().getName());

        Opportunity_Locations__c opportunityLocation =
                new Opportunity_Locations__c(
                        Opportunity__c = opportunityId,
                        Location__c = locationId
                );
        try{
            insert opportunityLocation;
        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
        return opportunityLocation.Id;
    }
}