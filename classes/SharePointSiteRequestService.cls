/**************************************************************************************
Name:SharePointSiteRequestService
Version: 1.0 
Created Date: 09/20/2018
Function: Request SharePoint Site

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Stone	   09/20/2018       Original Version
* Scott Stone	   01/07/2019		DE859 - allow users who have edit rights via opportunity share to request sharepoint site.
*************************************************************************************/
public class SharePointSiteRequestService {
    
    public void requestSharePointSite(Id oppId) { 
        if(userCanEditOpportunity(oppId) || isUserAnAdministrator()){
            Opportunity opp = [Select Id, 
                           SharePoint_Status__c, 
                           SharePoint_Url__c, 
                           SharePoint_Site_Requested_On__c,
                           OwnerId
                           From Opportunity 
                           Where id=:oppId];
            
            opp.SharePoint_URL__c = null;
            opp.Sharepoint_Status__c = 'Requested';
            opp.Sharepoint_Site_Requestor__c = Userinfo.getUserId();
            opp.SharePoint_Site_Requested_On__c = datetime.now();
            try{
                update opp;
            }catch(Exception e){
                throw new AuraHandledException(e.getMessage());
            }
        } else {
            throw new AuraHandledException('A Sharepoint Site cannot be requested for opportunity ' + oppId);
        }
    }
    
    public Boolean userCanEditOpportunity(Id oppId) { 
        UserRecordAccess recordAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :Userinfo.getUserId() AND RecordId = :oppId];
        if(recordAccess.HasEditAccess) {
            return true;
        }
        //DE859 - check opportunity sharing if they don't have direct record access.
        OpportunityShare opportunityShare = [SELECT OpportunityAccessLevel, RowCause FROM OpportunityShare WHERE UserOrGroupId = :Userinfo.getUserId() and OpportunityId = :oppId];
		return (opportunityShare.OpportunityAccessLevel == 'Edit'
                || opportunityShare.OpportunityAccessLevel == 'All');
    }
    
    public Boolean isUserAnAdministrator(){
        String userProfileName = [select Name from profile where id =: Userinfo.getProfileId()].Name;
		return (userProfileName == 'System Administrator');
    }
    
    public Opportunity getSharePointSiteDetails(Id oppId){
        //Get the opportunity based on the ID  
        Opportunity opp = [Select Id, 
                           SharePoint_Status__c, 
                           SharePoint_Url__c, 
                           SharePoint_Site_Requested_On__c,
                           OwnerId
                           From Opportunity 
                           Where id=:oppId];
        return opp;
    }
}