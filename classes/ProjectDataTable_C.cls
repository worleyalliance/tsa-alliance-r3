/**************************************************************************************
Name: ProjectDataTable_C
Version: 1.0 
Created Date: 05.05.2017
Function: Controller of ProjectDataTable component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   05/05/2017      Original Version
* Pradeep Shetty    09/12/2017      Added Project Type parameter
*************************************************************************************/
public with sharing class ProjectDataTable_C {

    private static ProjectDataTable_Service service = new ProjectDataTable_Service();

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    @AuraEnabled
    public static DataTable_Service.TableData getData(
            String fieldSetName,
            Id recordId,
            String recordTypeNames,
            String projectTypeNames,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault
    ) {
        return service.getProjectData(
                fieldSetName,
                recordId,
                recordTypeNames,
            	projectTypeNames,
                relationField,
                sObjectName,
                totalRowPresent,
                isSumDefault
        );
    }
}