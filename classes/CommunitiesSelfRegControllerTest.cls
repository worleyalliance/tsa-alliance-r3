/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CommunitiesSelfRegControllerTest {
    @IsTest(SeeAllData=true) 
    public static void testCommunitiesSelfRegController() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        Profile communityProfile = [SELECT ID FROM PROFILE WHERE ID = '00e1U000001B0zj'];
        controller.profileIdValue = communityProfile.Id;
        
        controller.AccountIdValue = acc.id;
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'Qwerty#1234';
        controller.confirmPassword = 'Qwerty#1234';
        System.assert(controller.registerUser() == null);  
    }  
    
     @IsTest(SeeAllData=true) 
    public static void testCommunitiesSelfRegControllerIvalidPassword() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().getRecord();
        
                
        Profile communityProfile = [SELECT ID FROM PROFILE WHERE ID = '00e1U000001B0zj'];
		controller.profileIdValue = communityProfile.Id;
        
        controller.AccountIdValue = acc.id;
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'Qwerty#1234';
        controller.confirmPassword = 'Qwerty#123';
        System.assert(controller.registerUser() == null);  
    }  
      static testMethod void testRegistration() {
       SiteRegisterController controller = new SiteRegisterController();
       controller.username = 'test@force.com';
       controller.email = 'test@force.com';
       controller.communityNickname = 'test';
       // registerUser will always return null when the page isn't accessed as a guest user
       System.assert(controller.registerUser() == null);    
       
       controller.password = 'abcd1234';
       controller.confirmPassword = 'abcd1234';
       System.assert(controller.registerUser() == null);  
   }
}