/**************************************************************************************
Name: SalesPlanQueueableHandler
Version: 1.0 
Created Date: 12/23/2018
Function: This handler updates the Sales Plan field on Opportunities to ensure that new sales plan
          or updated sales plans have the correct opportunities in the related list.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    12/23/2018      Original Version
***************************************************************************************/
public class SalesPlanQueueableHandler implements Queueable{
   
    //List of new sales plans
    private List<Campaign> newSalesPlans;

    //List of old sales plans
    private Map<Id, Campaign> oldSalesPlansMap;

    //Set for Sales Plan Keys
    private Set<String> salesPlanKeys = new Set<String>(); 

    //Set for Old Sales Plan Keys
    private Set<String> oldSalesPlanKeys = new Set<String>();   

    //Set for Sales Plan Keys for non existant sales plans after update
    private Set<String> nonExistingSalesPlanKeys = new Set<String>();       

    //Map of Selling Unit (BU in case of ATN) + Fiscal Year as a key and Sales Plan Id as the value
    private Map<String, Id> salesPlanMap = new Map<String, Id>();   

    //Constant for Line Of Business
    private final String LOB_ATN = 'ATN';
    
    //Constant for Selling Unit Sales Plan record type developer name
    private final String REC_TYPE_SU_SALES_PLAN = 'Sales_Plan';    

    //Variable holding the Selling Unit sales plan record type
    private RecordType suSalesPlanRecordType = [Select Id 
                                                From RecordType 
                                                Where DeveloperName = :REC_TYPE_SU_SALES_PLAN
                                                Limit 1];    

    public SalesPlanQueueableHandler(List<Campaign> newSalesPlans, Map<Id, Campaign> oldSalesPlans) {
        this.newSalesPlans = newSalesPlans;
        this.oldSalesPlansMap = oldSalesPlans;
    }

    public void execute(QueueableContext context) {

        //Ensure all collections are empty
        salesPlanKeys.clear();
        oldSalesPlanKeys.clear();
        nonExistingSalesPlanKeys.clear();
        salesPlanMap.clear();

        //Prepare salesPlanMap
        prepareSalesPlanMap();

        //Update Opportunities
        updateOpportunities();
    }

    //Method to prepare the list of keys to query Opportunity
    private void prepareSalesPlanMap(){
        //Variable for Old Sales Plan
        Campaign oldSalesPlan;

        //Loop through sales plan to build collections
        for(Campaign salesPlan: newSalesPlans){
            if(salesPlan.RecordTypeId == suSalesPlanRecordType.Id &&
               salesPlan.cb_sales_plan_locked__c == False && 
               salesPlan.IsActive == True){
                if (oldSalesPlansMap!=null && !oldSalesPlansMap.isEmpty()) {
                    //Get old sales plan
                    oldSalesPlan = oldSalesPlansMap.get(salesPlan.Id);

                    //Fiscal year changed. Get the key for both new and old sales plan
                    if(salesPlan.pl_fiscal_year__c != oldSalesPlan.pl_fiscal_year__c){
                        //Populate collections for old and new values of salesplans
                        populateCollections(salesPlan, oldSalesPlan);
                    }

                    //Selling Unit changed. Get the key for both new and old sales plan
                    if(salesPlan.pl_selling_unit__c != oldSalesPlan.pl_selling_unit__c){
                        //Populate collections for old and new values of salesplans
                        populateCollections(salesPlan, oldSalesPlan);                
                    }   

                    //Business Unit changed. Get the old and new sales plan key only if the old and new LOB is ATN. 
                    //Other conditions will be captured by above 2 scenarios.
                    if(salesPlan.pl_line_of_business__c == LOB_ATN && 
                       oldSalesPlan.pl_line_of_business__c == LOB_ATN && 
                       salesPlan.pl_business_unit__c != oldSalesPlan.pl_business_unit__c){
                        //Populate collections for old and new values of salesplans
                        populateCollections(salesPlan, oldSalesPlan);                
                    }                                                      
                }
                else{
                    //Populate collections
                    populateCollections(salesPlan, null);  
                }                   
            }            
        }//END FOR

        if(oldSalesPlanKeys!=null & !oldSalesPlanKeys.isEmpty()){
            //Query Sales Plan to get Sales Plans related to Old key values and add it to the map
            for(Campaign salesPlan: [Select Id, 
                                            Sales_Plan_Key__c 
                                    From Campaign 
                                    Where Sales_Plan_Key__c in :oldSalesPlanKeys]){
                salesPlanMap.put(salesPlan.Sales_Plan_Key__c, salesPlan.Id);
            }//END FOR

            //Identify non existing sales plan keys. 
            //This will be used to blank out the Sales Plan Id on Opportunities that don't meet the criteria for any sales plan
            for(String oldSalesPlanKey: oldSalesPlanKeys){
                if(!salesPlanMap.containsKey(oldSalesPlanKey)){
                    nonExistingSalesPlanKeys.add(oldSalesPlanKey);
                }
            } //END FOR  
        }         
    }

    //Method to populate collections
    private void populateCollections(Campaign salesPlan, Campaign oldSalesPlan){
        //Populate collections for new value of Sales Plans
        salesPlanKeys.add(salesPlan.Sales_Plan_Key__c);
        salesPlanMap.put(salesPlan.Sales_Plan_Key__c, salesPlan.Id);
        
        //Populate collections for old value of Sales Plans only if that key does not exist in salesPlanKeys
        if(oldSalesPlan!=null && !salesPlanKeys.contains(oldSalesPlan.Sales_Plan_Key__c)){
            oldSalesPlanKeys.add(oldSalesPlan.Sales_Plan_Key__c);
        }          
    }

    //Method to update Opportunities with salesplan
    private void updateOpportunities(){
        //List of Opps to be updated
        List<Opportunity> oppUpdateList = new List<Opportunity>();

        for(Opportunity currentOpp: [Select Id, 
                                            CampaignId,
                                            Sales_Plan_key__c 
                                        From Opportunity 
                                        Where Sales_Plan_Key__c in :salesPlanMap.keySet()]){

            //get the sales plan Id
            currentOpp.CampaignId = salesPlanMap.get(currentOpp.Sales_Plan_key__c);
            //Add to the update list
            oppUpdateList.add(currentOpp);            
        } //END FOR

        if(nonExistingSalesPlanKeys!=null && !nonExistingSalesPlanKeys.isEmpty()){
            for(Opportunity currentOpp: [Select Id, 
                                            CampaignId,
                                            Sales_Plan_key__c 
                                        From Opportunity 
                                        Where Sales_Plan_Key__c in :nonExistingSalesPlanKeys]){
                currentOpp.CampaignId = null;
                //Add to the update list
                oppUpdateList.add(currentOpp);                 
            }
        }

        //If the list of Opps is not empty, update Opps
        if(oppUpdateList!=null && !oppUpdateList.isEmpty()){
            update oppUpdateList;
        }
    }    
}