/**************************************************************************************
Name: DataTableService_noSharing
Version: 1.0 
Created Date: 2018 06 12
Function: Service to pull Name of record user do not have access to to support the DataTableService class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo            2018 06 12        DE460 - Projects - Accounts Project Subtab - ProjectsDataTable Component and Related List Issues
*************************************************************************************/
public without sharing class DataTableService_noSharing{
    
    static public Object getName(String lookupObject, Object fieldValue){
        
        Object value;
        String soqlQuery = 'Select Name from '+ lookupObject+ ' where id = ' + '\'' + fieldValue + '\'';
            
        List<sObject> newObj = Database.query(soqlQuery);
            
            if(newObj.isEmpty()){
               //throw new AuraHandledException(Label.FolderNotFound);
               system.debug('Can not find Name of related object');
            }    
            value = ((String)newObj[0].get('Name')) ;
    
        return value;
    
    }
}