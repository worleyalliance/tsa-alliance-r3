/**************************************************************************************
Name: SingleField_Service
Version: 1.1 
Created Date: 24.04.2017
Function: Service class for SingleFiled Lightning Component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           04.24.2017         Original Version
* Pradeep Shetty            07.27.2017         DE167: Set edit button visibility
* Pradeep Shetty            10.11.2017         DE270: Prevent field edits for closed won opp 
* Jignesh Suvarna           03.12.2018         US1509: Data Stewards/Sales Super User - Edit Access - Custom Components
*************************************************************************************/
public class SingleField_Service extends BaseComponent_Service{

  private final String OBJECTTYPE_OPPORTUNITY = 'Opportunity';

  /*
   * Retrieves given field value of give record
   */
  public Object retrieveFieldValue(String objectType, String fieldName, Id recordId) {
    validateObjectAccesible(Schema.getGlobalDescribe().get(objectType).getDescribe().getName());

    Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();

    if(!fieldMap.containsKey(fieldName))
    {
      throw new AuraHandledException(Label.FieldDontExist);
    } else if(fieldMap.get(fieldName).getDescribe().getType() != Schema.DisplayType.STRING &&
              fieldMap.get(fieldName).getDescribe().getType() != Schema.DisplayType.TEXTAREA)
    {
      throw new AuraHandledException(Label.FieldNotSupported);
    }

    String query = 'SELECT '+fieldName+' FROM '+objectType+' WHERE Id=\''+recordId+'\'';
    List<sObject> result = Database.query(query);
    sObject record = result[0];

    return record.get(fieldName);
  }

  /*
   * Updates given field value of give record
   */
  public void updateFieldValue(String objectType, String fieldName, Id recordId, String value) {
    validateObjectUpdateable(Schema.getGlobalDescribe().get(objectType).getDescribe().getName());

    sObject obj = (sObject) Type.forName(objectType).newInstance();
    obj.Id = recordId;
    obj.put(fieldName, value);
    try
    {
      update obj;
    }
    catch (DmlException ex)
    {
      throw new AuraHandledException(ex.getDmlMessage(0));
    }
  }

  /*
   * Checks edit access to the record and field to determine Edit button visibility
   */
  public Boolean checkEditPermission(String objectType, String fieldName, Id recordId){

    //Check if this component is used in Opportunity record. If yes, check if the Opportunity is closed won. 
    //If yes, then prevent this field from being edit (Added as part of DE270)
    //Initialize the variable
    Boolean isWon = false;
    
    // 20180313_JS - Added as part of US1509 - allow Admin/Data stewards Edit access
    Boolean hasEditPermission = Utility.checkAdminPermissions();
    Boolean havRecordAccess = Utility.havRecordAccess(objectType, fieldName, recordId);

    //Get the correct value of isWon
    if(String.isNotEmpty(objectType) && 
       objectType.equalsIgnoreCase(OBJECTTYPE_OPPORTUNITY)){
      //Get the status of this Opportunity
      isWon = [Select IsWon 
               From Opportunity 
               Where Id = :recordId 
               Limit 1].IsWon;

    }

    // 20180313_JS - Added as part of US1509
    if(hasEditPermission && isWon && havRecordAccess)
    {
      return true;   
    }
    else if(isWon)
    {
      return false;   
    }
    //Check if the object is updateable by the user. 
    else if(isObjectUpdatable(Schema.getGlobalDescribe().get(objectType).getDescribe().getName()))
    {
      return havRecordAccess;
    }
    else{
      return false;
    } 
  } 
}