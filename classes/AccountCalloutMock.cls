/**************************************************************************************
Name: AccountCalloutMock
Version: 1.0 
Created Date: 25.04.2017
Function: This class is used to set the Mock response that is used in test class for Account Service Callout (NewAccount_WS)

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      25.04.2017      Original Version
***************************************************************************************/
@isTest
global class AccountCalloutMock implements WebServiceMock {
    public Boolean addressExistsError    = false;
    public Boolean addressNotExistsError = false;
    public Boolean accountExistssError   = false;
    public Boolean accountCreationError  = false;

    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        // start - specify the response you want to send
        NewAccount_WS_XSD.AccountSetupResponse_element response_x =
                new NewAccount_WS_XSD.AccountSetupResponse_element();
        response_x.ErrorMessage = new List<NewAccount_WS_XSD.ErrorMessage_element>();
        response_x.AccountSetup1 = new List<NewAccount_WS_XSD.AccountSetup1_element>();

        NewAccount_WS_XSD.AccountSetup_element req = new NewAccount_WS_XSD.AccountSetup_element();
        req = (NewAccount_WS_XSD.AccountSetup_element) request;

        NewAccount_WS_XSD.ErrorMessage_element errorMessageParams = new NewAccount_WS_XSD.ErrorMessage_element();
        //errorMessageParams.ErrorId = '';
        //errorMessageParams.ErrorMessage = '';
        //errorMessageParams.Status = 'S';
        errorMessageParams.JBErrorId = '';
        errorMessageParams.JBErrorMessage= '';
        errorMessageParams.JBstatus= 'S';
        NewAccount_WS_XSD.AccountSetup1_element accountSetup1Params = new NewAccount_WS_XSD.AccountSetup1_element();
        accountSetup1Params.Location = new List<NewAccount_WS_XSD.Location_element>();
        NewAccount_WS_XSD.Location_element location = new NewAccount_WS_XSD.Location_element();
        accountSetup1Params.Contact = new List<NewAccount_WS_XSD.contact_element>();
        NewAccount_WS_XSD.contact_element contact = new NewAccount_WS_XSD.contact_element();
        if (addressExistsError) {
            //errorMessageParams.JBErrorMessage= System.Label.AddressExists;
            //errorMessageParams.Status = 'E';
            errorMessageParams.JBErrorMessage= System.Label.AddressExists;
            errorMessageParams.JBstatus= 'E';
        } else if (accountExistssError) {
            errorMessageParams.JBErrorMessage= System.Label.AccountExists;
            errorMessageParams.JBstatus= 'E';
        } else if (addressNotExistsError) {
            errorMessageParams.JBErrorMessage= 'Address Line 1 Does not Exist';
            errorMessageParams.JBstatus= 'E';
        }
        else if(accountCreationError){
            errorMessageParams.JBErrorMessage= 'Oracle Account creation error';
            errorMessageParams.JBstatus= 'E';           
        }
        else{

            //NewAccount_WS_XSD.AccountSetup1_element accountSetup1Params = new NewAccount_WS_XSD.AccountSetup1_element();
            //accountSetup1Params.AccountId = '654321';
            accountSetup1Params.JBaccountId = '654321';
            accountSetup1Params.SFPartyId = '';
            accountSetup1Params.SfAccountId = '';
            accountSetup1Params.Flexattribute1 = '';
            accountSetup1Params.Flexattribute2 = '';
            accountSetup1Params.Flexattribute2 = '';
            accountSetup1Params.Flexattribute2 = '';
            accountSetup1Params.Flexattribute2 = '';

            //accountSetup1Params.Location = new List<NewAccount_WS_XSD.Location_element>();
            //NewAccount_WS_XSD.Location_element location = new NewAccount_WS_XSD.Location_element();
            location.AddressID = '123456';
            location.SFAddressId = '';  // Requires String
            location.Flexattribute1 = '';
            location.Flexattribute2 = '';
            location.Flexattribute3 = '';
            location.Flexattribute4 = '';
            location.Flexattribute5 = '';
            accountSetup1Params.Location.add(location);

            //accountSetup1Params.Contact = new List<NewAccount_WS_XSD.contact_element>();
            //NewAccount_WS_XSD.contact_element contact = new NewAccount_WS_XSD.contact_element();
            contact.ContactID = '';
            contact.SFCountactId = ''; // Requires String
            contact.Flexattribute1 = '';
            contact.Flexattribute2 = '';
            contact.Flexattribute3 = '';
            contact.Flexattribute4 = '';
            contact.Flexattribute5 = '';
            accountSetup1Params.Contact.add(contact);

        }
        errorMessageParams.Flexattribute1 = '';
        errorMessageParams.Flexattribute1 = '';
        errorMessageParams.Flexattribute1 = '';
        response_x.ErrorMessage.add(errorMessageParams);
        response_x.AccountSetup1.add(accountSetup1Params);

        // end
        response.put('response_x', response_x);
    }
}