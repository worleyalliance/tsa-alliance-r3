/**************************************************************************************
Name: GroupAttritionTable_C_Test
Version: 1.0 
Created Date: 05.05.2017
Function: Unit test class for GroupAttritionTable_C functionalities

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
@isTest
private class GroupAttritionTable_C_Test {

    @IsTest
    private static void shouldCalculateDataForGroupAttritionTable() {
        //given
        String fieldSetName =
                Schema.getGlobalDescribe().get('AT_Segment_Attrition__c').getDescribe().fieldSets.getMap().values()[0].name;

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .save()
                        .getRecord();
        AT_Segment_Attrition__c groupAttrition2 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                GroupAttritionTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        2015,
                        'Fiscal_Year__c',
                        'Group__c',
                        'AT_Segment_Attrition__c',
                        true,
                        true,
                        null
                );

        //then
        System.assertNotEquals(0, result.fieldDescriptions.size());
        System.assertEquals(2, result.rows.size());
        System.assertNotEquals(0, result.totalRow.values().size());
    }

    @IsTest
    private static void shouldCalculateDataMonthlyAttritionRate() {
        //given
        Map<String, FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('AT_Segment_Attrition__c').getDescribe().fieldSets.getMap();
        String fieldSetName =
                fieldSetMap.values()[0].name;
        for(FieldSet fs : fieldSetMap.values()){
            for(FieldSetMember fsm : fs.fields){
                if(fsm.getFieldPath() == 'Monthly_Attrition_Rate__c'){
                    fieldSetName = fs.getName();
                }
            }
        }

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withControllable(2)
                        .withSemiControllable(3)
                        .withStartingWorkforce(10)
                        .save()
                        .getRecord();
        AT_Segment_Attrition__c groupAttrition2 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withControllable(5)
                        .withSemiControllable(6)
                        .withStartingWorkforce(20)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                GroupAttritionTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        2015,
                        'Fiscal_Year__c',
                        'Group__c',
                        'AT_Segment_Attrition__c',
                        true,
                        true,
                        null
                );

        //then
        System.assertNotEquals(null, result.totalRow.get('Monthly_Attrition_Rate__c'));
    }

    @IsTest
    private static void shouldCalculateControllable() {
        //given
        Map<String, FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('AT_Segment_Attrition__c').getDescribe().fieldSets.getMap();
        String fieldSetName =
                fieldSetMap.values()[0].name;
        for(FieldSet fs : fieldSetMap.values()){
            for(FieldSetMember fsm : fs.fields){
                if(fsm.getFieldPath() == 'Hire_F_Hispanic__c'){
                    fieldSetName = fs.getName();
                }
            }
        }

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withHireFHispanic(10)
                        .save()
                        .getRecord();
        AT_Segment_Attrition__c groupAttrition2 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withHireFHispanic(20)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                GroupAttritionTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        2015,
                        'Fiscal_Year__c',
                        'Group__c',
                        'AT_Segment_Attrition__c',
                        true,
                        true,
                        null
                );

        //then
        System.assertEquals(30, result.totalRow.get('Hire_F_Hispanic__c'));
    }

    @IsTest
    private static void shouldCalculateDefaultSumPlusPercentValue() {
        //given
        Map<String, FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('AT_Segment_Attrition__c').getDescribe().fieldSets.getMap();
        String fieldSetName =
                fieldSetMap.values()[0].name;
        for(FieldSet fs : fieldSetMap.values()){
            for(FieldSetMember fsm : fs.fields){
                if(fsm.getFieldPath() == 'Hire_F_Hispanic__c'){
                    fieldSetName = fs.getName();
                }
            }
        }

        TestHelper.GroupBuilder builder = new TestHelper.GroupBuilder();
        AT_Group__c atGroup = builder.build().save().getRecord();
        TestHelper.GroupAttritionBuilder attritionBuilder = new TestHelper.GroupAttritionBuilder();
        AT_Segment_Attrition__c groupAttrition1 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withHireFHispanic(1)
                        .withHireMHispanic(2)
                        .save()
                        .getRecord();
        AT_Segment_Attrition__c groupAttrition2 =
                attritionBuilder
                        .build()
                        .withGroupId(atGroup.Id)
                        .withFiscalYear(2015)
                        .withHireFHispanic(1)
                        .withHireMHispanic(2)
                        .save()
                        .getRecord();

        //when
        DataTable_Service.TableData result =
                GroupAttritionTable_C.getData(
                        fieldSetName,
                        atGroup.Id,
                        2015,
                        'Fiscal_Year__c',
                        'Group__c',
                        'AT_Segment_Attrition__c',
                        true,
                        false,
                        'Hire_M_Hispanic__c'
                );

        //then
        System.assertEquals('2(50.0%)', result.totalRow.get('Hire_F_Hispanic__c'));
    }
}