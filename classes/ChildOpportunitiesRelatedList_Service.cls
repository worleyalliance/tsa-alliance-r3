/**************************************************************************************
Name: ChildOpportunitiesRelatedList_Service
Version: 1.0 
Created Date: 08.05.2017
Function: Related list of opportunity child opportunities

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   05/08/2017      Original Version
* Pradeep Shetty    03/07/2018      US1636: Add Grand Total row and additional columns.
* Pradeep Shetty    06/13/2018      US2180: Added column indicating BV or regular Opp
* Chris Cornejo     09/27/2018      DE739: Regression 10/05 - US2327: Program and Task Order fields not populated/updated on BV creation
* Scott Stone 		01/09/2019		US2678: add method to get fields copied in ehanced clone + their values
*************************************************************************************/
public class ChildOpportunitiesRelatedList_Service extends BaseComponent_Service{

 /*
  *   Retrieves list of child opportunities
  */
  public List<OpportunityWrapper> getChildOpportunities(Id recordId){
    validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());

    List<Opportunity> childOpportunities = [SELECT Name, 
                                                   AccountId,
                                                   StageName, 
                                                   Total_Probable_GM__c, 
                                                   Amount,
                                                   Work_Hours__c,
                                                   Factored_Workhours__c, 
                                                   CloseDate,
                                                   Forecast_Category__c,            //US1636
                                                   Revenue__c,                      //US1636
                                                   Pipeline_Factored_Revenue__c,
                                                   RecordType.DeveloperName         //US2180
                                            FROM Opportunity
                                            WHERE Parent_Opportunity__c = :recordId
    ];
    List<OpportunityWrapper> opportunities = new List<OpportunityWrapper>();
    for(Opportunity opportunity : childOpportunities){
        opportunities.add(new OpportunityWrapper(opportunity));
    }
    return opportunities;
  }

 /*
  *   Retrieves opportunity record with needed data
  */
  public Opportunity getOpportunity(Id recordId){
    validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());
    Opportunity opp = [SELECT Name,
                              Account.Name,
                              AccountId,
                              Amount,
                              Auto_Create_Contract_Program__c,
                              Business_Unit__c,
                              CampaignId,
                              CloseDate,
                              Contract_Award_Type__c,
                              ContractId, 
                              Duration_Months__c,
                              End_Client__c,
                              Executing_Business_Unit__c,
                              Executing_Line_of_Business__c,
                              Factored_Workhours__c, 
                              Forecast_Category__c,         //US1636
                              Jacobs_Industry_Group__c,
                              //Lead_Performance_Unit__c,  Replaced it with the new Lookup Field
                              Lead_Performance_Unit_PU__c,
                              Line_of_Business__c,
                              Markets__c,
                              OCI_Indicator__c,
                              Opportunity_Class_A_T__c,
                              Opportunity_Structure__c,
                              Opportunity_Tier__c,
                              OwnerId,
                              Parent_Opportunity__c,
                              Program_New__c,  //DE739 - Replace Program__c to Program_New__c
                              Program_Renewal_Rebid__c,
                              Project_Categories__c,
                              Project_Role__c,
                              Pipeline_Factored_Revenue__c, //US1636
                              Revenue__c,
                              SEC_Codes_and_Categories__c,
                              Selling_Unit__c,
                              StageName, 
                              Task_Order_New__c, //DE739 - Replace Task_Order__c to Task_Order_New__c
                              Total_Probable_GM__c, 
                              Work_Hours__c,
                              UserRecordAccess.HasAllAccess,
                              UserRecordAccess.HasReadAccess, 
                              UserRecordAccess.HasEditAccess,
                              UserRecordAccess.HasTransferAccess,
                       		  Sales_Plan_Unit__c,
                       		  Sales_Plan_Unit__r.Name
                      FROM Opportunity
                      WHERE Id = :recordId
    ];
    return opp;
  }

 /*
  *   Deletes record with given id
  */
  public void deleteOpportunity(Id recordId){
    validateObjectDeletable(Opportunity.sObjectType.getDescribe().getName());
    try{
        delete new Opportunity(Id = recordId);
    }catch (DmlException ex){
        throw new AuraHandledException(ex.getDmlMessage(0));
    }
  }

 /*
  *   Gives update regarding user permissions to opportunity object
  */
  public CRUDpermissions getUserOpportunityPermissios(){
    return getUserPermissionsForObject(Opportunity.sObjectType.getDescribe().getName());
  } 
   
   /*
    *  US2698 - get list of fields that get cloned in enhanced clone with their values for the opportunityId
    */
    public List<OpportunityCloneField> getClonedOpportunityFieldsAndValues(Id opportunityId){
        List<String> opportunityFields = getCloneFields();
        String query = 
            'SELECT ' + String.join(opportunityFields, ',') +
            	' FROM Opportunity' +
            	' WHERE Id = \''+opportunityId+'\'';
        
        List<Opportunity> opportunities = Database.query(query);
        if(opportunities.isEmpty()){
            System.debug('No opportunity with given Id: '+opportunityId);
            throw new AuraHandledException(Label.ErrorOccurredDuringOpportunityCopy);
        }
        
        Opportunity opportunity = opportunities[0];
        
        List<OpportunityCloneField> fieldsWithValues = new List<OpportunityCloneField>();
        for(String fieldName : opportunityFields){
            OpportunityCloneField cloneField = new OpportunityCloneField();
            cloneField.apiName = fieldName;
            cloneField.value = opportunity.get(fieldName);
            fieldsWithValues.add(cloneField);
        }
        
        return fieldsWithValues;
    }
    
    /* 
    *  US2698 - get list of clone fields as defined by custom metadata
    */
    private List<String> getCloneFields(){
        
        List<String> opportunityFields = new List<String>();
        List<ChildOpportunityFieldClone__mdt> childOpportunityFields = [
            SELECT API_Name__c
            FROM ChildOpportunityFieldClone__mdt
        ];
        
        for(ChildOpportunityFieldClone__mdt field : childOpportunityFields){
            opportunityFields.add(field.API_Name__c);
        }
        
        return opportunityFields;
    }
    
    public class OpportunityCloneField {
        
        @AuraEnabled
        public string apiName;
        
        @AuraEnabled
        public Object value;
        
    }
      
  public class OpportunityWrapper{

    public OpportunityWrapper(Opportunity opportunity){
      Decimal expectedRevenue        = opportunity.Total_Probable_GM__c != null        ? opportunity.Total_Probable_GM__c         : 0;
      Decimal amount                 = opportunity.Amount != null                      ? opportunity.Amount                       : 0;
      Decimal revenueValue           = opportunity.Revenue__c !=null                   ? opportunity.Revenue__c                   : 0;
      Decimal factoredRevenueValue   = opportunity.Pipeline_Factored_Revenue__c !=null ? opportunity.Pipeline_Factored_Revenue__c : 0;
      Decimal factoredWorkHoursValue = opportunity.Factored_Workhours__c !=null        ? opportunity.Factored_Workhours__c        : 0;
      id                             = opportunity.Id;
      name                           = opportunity.Name;
      accountId                      = opportunity.AccountId;
      stage                          = opportunity.StageName;
      gm                             = amount.setScale(2);
      factoredGM                     = expectedRevenue.setscale(2);
      workHours                      = opportunity.Work_Hours__c != null ? opportunity.Work_Hours__c : 0;
      factoredWorkHours              = factoredWorkHoursValue;
      closeDate                      = opportunity.CloseDate;
      forecastCategory               = opportunity.Forecast_Category__c;
      revenue                        = revenueValue.setScale(2);
      factoredRevenue                = factoredRevenueValue.setScale(2);
      //Start: US2180
      opportunityRecordType          = Opportunity.Recordtype.DeveloperName.contains(Utility.BOOKING) ? Utility.BOOKING_VALUE : Utility.FULL_OPPORTUNITY;
      //End: US2180
    }

    @AuraEnabled
    public String name;

    @AuraEnabled
    public Id id;

    @AuraEnabled
    public Id accountId;

    @AuraEnabled
    public String stage;

    @AuraEnabled
    public Decimal gm;

    @AuraEnabled
    public Decimal factoredGM;

    @AuraEnabled
    public Decimal workHours;

    @AuraEnabled
    public Decimal factoredWorkHours;    

    @AuraEnabled
    public Date closeDate;

    @AuraEnabled
    public String forecastCategory;

    @AuraEnabled
    public Decimal revenue;

    @AuraEnabled
    public Decimal factoredRevenue;

    //Start: US2180
    @AuraEnabled
    public String opportunityRecordType;    
    //End: US2180

  }
}