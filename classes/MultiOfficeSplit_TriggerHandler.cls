/**************************************************************************************
Name: MultiOfficeSplit_TriggerHandler
Version: 1.1
Created Date: 03/15/2017
Function: Handler for MultiOfficeSplit_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni    	04/03/2017      Original Version
* Manuel Johnson	  01/08/2017      Changed from Selling LOB/BU to Executing LOB/BU for US694
* Pradeep Shetty    04/23/2018      US2010: Replaced existing Performance Unit picklist with new lookup
* Pradeep Shetty    08/21/2018      US2396: Automatically adjust MOS start dates
*******************************************************************************************************************/
public class MultiOfficeSplit_TriggerHandler {
    
  private static MultiOfficeSplit_TriggerHandler handler;
  
  private final static String SKIP_VALUE = '';
  
  // Singleton like pattern
  public static MultiOfficeSplit_TriggerHandler getHandler() {
    if (handler == null) {
        handler = new MultiOfficeSplit_TriggerHandler();
    }
    return handler;
  }
  
  // Actions performed on MultiOffice Split records to update the opportunity Totals on all 'After' operations
  public void onBeforeDelete(List<Multi_Office_Split__c> delMultiOfficeSplit) {
    throwValidation(delMultiOfficeSplit);
  }
  
  // Actions performed on MultiOffice Split records on before insert
  public void onBeforeInsert(List<Multi_Office_Split__c> multiOfficeSplits) {
    updateLOBandBUpicklist(multiOfficeSplits);
  }
  
  // Actions performed on MultiOffice Split records on before update
  public void onBeforeUpdate(List<Multi_Office_Split__c> multiOfficeSplits, Map<Id, Multi_Office_Split__c> oldMultiOfficeSplitsByIds) {
    updateLOBandBUpicklistWhenPUChanged(multiOfficeSplits, oldMultiOfficeSplitsByIds);
  }
  
  /* US2396: Commented out afterUpsert. Separated it into AfterInsert and AfterUpdate*/
  // Actions performed on MultiOffice Split records to update the opportunity Totals on all 'After' operations
  //public void onAfterUpsert(List<Multi_Office_Split__c> MultiOfficeSplitRecs) {
  //  uniqueLead(MultiOfficeSplitRecs);
  //}

  //US2396: Actions performed on MultiOffice Split records after insert
  public void onAfterInsert(List<Multi_Office_Split__c> MultiOfficeSplitRecs) {
      //Ensure that there is only one lead MOS
      uniqueLead(MultiOfficeSplitRecs);
  }

  //US2396: Actions performed on MultiOffice Split records after update
  public void onAfterUpdate(Map<Id, Multi_Office_Split__c> oldMOSMap, Map<Id,Multi_Office_Split__c> newMOSMap) {
    //Ensure that there is only one lead MOS
    uniqueLead(newMOSMap.values());
    autoAdjustDates(oldMOSMap, newMOSMap);
  }   
  
  // Method to validate before delete
  private void throwValidation(List<Multi_Office_Split__c> delMultiOfficeSplit) {
    for (Multi_Office_Split__c ms: delMultiOfficeSplit) {
      if (ms.Lead__c == true)
        ms.addError(System.Label.MosLeadDeletionError);
    }
  }
  
  // Method to make sure there is only one unique lead per opportunity
  private void uniqueLead(List<Multi_Office_Split__c> multiOfficeSplitRecs) {
    List<Id> msLeadIds = new List<Id>();
    List<Multi_Office_Split__c> msList = new List<Multi_Office_Split__c>();
    List<id> oppIdList = new List<id>();
    for (Multi_Office_Split__c ms : multiOfficeSplitRecs) {
      if (ms.Lead__c == true) {
        oppIdList.add(ms.Opportunity__c);
        msLeadIds.add(ms.Id);
      }
    }
    for (Multi_Office_split__c ms : [SELECT Lead__c FROM Multi_Office_split__c WHERE Opportunity__c IN :oppIdList AND Id NOT IN :msLeadIds AND Lead__c = true]) {
      ms.Lead__c = false;
      msList.add(ms);
    }
    if (!msList.isEmpty()) {
      update msList;
    }
  }
  
  private void updateLOBandBUpicklistWhenPUChanged(List<Multi_Office_Split__c> multiOfficeSplits, Map<Id, Multi_Office_Split__c> oldMultiOfficeSplits){
    List<Multi_Office_Split__c> changedPUvalues = new List<Multi_Office_Split__c>();
    System.debug('match: ' + multiOfficeSplits);
    for(Multi_Office_Split__c split : multiOfficeSplits){
      if(!split.Performance_Unit_PU__c.equals(oldMultiOfficeSplits.get(split.Id).Performance_Unit_PU__c)){
        changedPUvalues.add(split);
      }
    }
    updateLOBandBUpicklist(changedPUvalues);
  }

  private void updateLOBandBUpicklist(List<Multi_Office_Split__c> multiOfficeSplits){

    /*US2010: Method commented out. BU and LOB are derived using custom object

    FieldDescribeUtil.PicklistDependency puBUDependency =
        FieldDescribeUtil.getDependentOptionsImpl(
            Opportunity.Lead_Performance_Unit__c,
            Opportunity.Executing_Business_Unit__c, //Updated to executing BU for US694
            new Set<String>{SKIP_VALUE}
        );
    FieldDescribeUtil.PicklistDependency buLOBDependency =
        FieldDescribeUtil.getDependentOptionsImpl(
            Opportunity.Executing_Business_Unit__c, //Updated to executing BU for US694
            Opportunity.Executing_Line_of_Business__c, //Updated to executing LOB for US694
            new Set<String>{SKIP_VALUE}
        );*/
    Map<Id, Unit__c> unitNames = new Map<Id, Unit__c>([Select Id, Business_Unit__c, Line_of_Business__c From Unit__c]);
    
    for(Multi_Office_Split__c multiOfficeSplit : multiOfficeSplits){
      if(String.isNotBlank(multiOfficeSplit.Performance_Unit_PU__c)){
        multiOfficeSplit.Executing_BU__c = unitNames.get(multiOfficeSplit.Performance_Unit_PU__c).Business_Unit__c;
        multiOfficeSplit.Executing_LOB__c = unitNames.get(multiOfficeSplit.Performance_Unit_PU__c).Line_of_Business__c;
      }
    }
  }

  /*
  * US2396: Method to adjust dates of MOS records when the lead MOS' date is changed
  * @Params
  * oldMOS: Map of Id and Old values of Multi Office rows
  * newMOS: List of new Multi Office rows
  */
  private void autoAdjustDates(Map<Id,Multi_Office_Split__c> oldMOSMap, Map<Id,Multi_Office_Split__c> newMOSMap){

    //Map of Opportunity Id and date difference
    Map<Id, Integer> oppDateAdjustmentMap = new Map<Id, Integer>();

    //List for updating Multi Office
    List<Multi_Office_Split__c> mosDateUpdateList = new List<Multi_Office_Split__c>();

    for(Multi_Office_Split__c mos : newMOSMap.values()){
      if(mos.Lead__c && 
        mos.Lead__c == oldMOSMap.get(mos.Id).Lead__c && 
        mos.Start_Date__c!=oldMOSMap.get(mos.Id).Start_Date__c){
        //Add the Opportunity ID and date difference
        //Note that if the new date is after the old date, the date difference is positive
        //If the new date is before the old date, the date difference is negative
        oppDateAdjustmentMap.put(mos.Opportunity__c, oldMOSMap.get(mos.Id).Start_Date__c.daysBetween(mos.Start_Date__c));
      }
    }

    //Get MOS records with Auto Adjust = true for opportunities identified in the previous step
    for(Multi_Office_Split__c mos: [Select Id, 
                                           Start_Date__c,
                                           Opportunity__c
                                    from Multi_Office_Split__c 
                                    where Opportunity__c in :oppDateAdjustmentMap.keyset() 
                                    and Auto_Adjust_Dates__c = true 
                                    and Lead__c!=true]){

      //Add the date difference
      mos.Start_Date__c = mos.Start_Date__c + oppDateAdjustmentMap.get(mos.Opportunity__c);
      mosDateUpdateList.add(mos);

    }

    //Update MOS records
    Database.SaveResult[] srList = Database.update(mosDateUpdateList);  
    
    // Iterate through each returned result
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        String errorMessage = 'Error';
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            errorMessage+= '- ' + err.getMessage();                
        }

        //Add error to the record
        newMOSMap.get(sr.getId()).addError(errorMessage);
      }
    }
  }    
}