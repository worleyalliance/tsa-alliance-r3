/**************************************************************************************
Name: OpportunityNumber_Test
Version: 1.0 
Created Date: 12/06/2018
Function: Test class to test the Opportunity number generation on creation

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty   12/06/2018       US1855: Original Version
*************************************************************************************/
@IsTest
public with sharing class OpportunityNumber_Test {
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME                 = 'salesOpp@testuser.com';  

    //Test data setup
    @testSetup
    static void setUpTestData(){

        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

        System.runAs(salesUser){

        //Account required for the Opportunity
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acct = accountBuilder.build().save().getRecord(); 

        }
    }

    /* Scenario 1: Test that Opportunity number is generated on creation
    * Creating 36 opportunity to make sure that base36 code is generated properly
    */
    @isTest
    private static void test_generateOpportunityNumber(){

        //Get the user
        User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];

        //Get the Account created by the sales user
        Account acct = [Select Id from Account where CreatedById = :salesUser.Id Limit 1];

        List<Opportunity> newOppList = new List<Opportunity>();

        test.startTest();
        //Execute tests
        System.runAs(salesUser){

            //Create multiple Opportunities

            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            for(Integer i=0; i<36; i++){
                newOppList.add(oppBuilder.build().withAccount(acct.Id).getOpportunity());
            }

            insert newOppList;  
        }
        test.stopTest();

        //Validate results
        //Fetch newly created opp
        for(Opportunity opp: [Select System_Number__c, Opportunity_Number__c from Opportunity where Id in :newOppList]){
            system.assertEquals(Utility.generateAlphaNumericCode(Integer.valueOf(opp.System_Number__c)), opp.Opportunity_Number__c);
        }
    }
}