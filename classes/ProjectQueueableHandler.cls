/**************************************************************************************
Name: ProjectQueueableHandler
Version: 1.0 
Created Date: 17.04.2017
Function: This handler actually makes the callout to the Project service of Oracle 

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      17.04.2017      Original Version
* Madhu Shetty      22.08.2017      (US871) - code for proposal location commented
* Raj Vakati        22.08.2017      (US769)& US781 - B&P Project workhrs and Cross Change flag
* Pradeep Shetty    22.08.2017      US778 & US902: Send IWA Letterhead Class CAtegory for Cross Charge
* Pradeep Shetty    01/11/2018      US1314: Sending Org Id instead of Org Name for legal entity
* Scott Walker      02/09/2018      US1625:  Send B&P start date instead of B&P creation date for role start date
* Jignesh Suvarna   17/04/2018      DE404: avoid sending key member start dates to Oracle
***************************************************************************************/
public class ProjectQueueableHandler implements OracleQueueableHandler, Database.AllowsCallouts {
    
    /*
* Project passed on class creation (the actual project from the Trigger)
*/
    private Jacobs_Project__c proj { get; set; }
    public Integer max;
    public Boolean updateFlag;
    private Integer counter = 1;
    public String result;
    
    private OracleCalloutRetryService retryService = new OracleCalloutRetryService();
    
    /*
* Constructor
*/
    public ProjectQueueableHandler(Jacobs_Project__c proj) {
        this.proj = proj;
    }
    
    public ProjectQueueableHandler() {
    }
    
    public void setObject(sObject obj) {
        this.proj = (Jacobs_Project__c) obj;
    }
    
    public void setRetrySyncLog(RetrySyncLog__c retrySyncLog) {
        retryService.setRetryLog(retrySyncLog);
    }
    
    public void execute(QueueableContext context) {
        //  String result;
        Project_WS.JEG_PROJECTS_REQUEST_PROCESS_pt request = new Project_WS.JEG_PROJECTS_REQUEST_PROCESS_pt();
        
        try {
            //Project Parameters 
            Project_XSD_WS.APPS_JEG_PROJECT_IN_REC_TYPE projectParameters = new Project_XSD_WS.APPS_JEG_PROJECT_IN_REC_TYPE();

            //PS 08/31/2017 START US772 Send the name as Id only during creation. For updates send the project code present in the name
            if(String.isNotBlank(String.valueOf(proj.Project_ID__c))){
                //[PS 09/12 US715] Project name for FY project contains the FY as well. When sending the name to Project, send only the first 8 characters. 
                projectParameters.PROJECT_NAME = proj.Name.left(8);                
            }
            else{
                projectParameters.PROJECT_NAME = proj.Id;
            }
            //PS 08/31/2017 END US772 Send the name as Id only during creation. For updates send the project code present in the name

            //[PS 01/11/2018] US1314: Set Legal Entity value to CARRYING_OUT_ORGANIZATION_ID instead of name
            if(String.isNotBlank(proj.Legal_Entity__c)){
                projectParameters.CARRYING_OUT_ORGANIZATION_ID = Decimal.valueOf(proj.Legal_Entity__c);    
            }

            //projectParameters.CARRYING_OUT_ORG_NAME = proj.Legal_Entity__c;
            projectParameters.PUBLIC_SECTOR_FLAG = 'N';
            // Start -Added by Raj - US779
            if(proj.txt_description__c!=null ){
                if(!String.isEmpty(proj.Project_Code__c)){
                    projectParameters.LONG_NAME = proj.Project_Code__c + '-' + proj.txt_description__c;    
                }
                else{
                    projectParameters.LONG_NAME = proj.txt_description__c;   
                }
                
                projectParameters.DESCRIPTION = proj.txt_description__c;
            }
            // End -Added by Raj - US779
            // Start -Added by Raj - US778
            //Request Re-Open - This action should change the Jacobs_Project__c.Status__c to 'Request Re-Open'. When the Status is changed to this value, the integration call should send the status 'Active' to Oracle.
            //Request Pending Closure - This action should change the Jacobs_Project__c.Status__c to 'Request Pending Closure'. When the Status is changed to this value, the integration call should send the status 'Pending Close' to Oracle
            //Request Closure - This action should change the Jacobs_Project__c.Status__c to 'Request Closure'. When the Status is changed to this value, the integration call should send the status 'Closed' to Oracle
            if(proj.Project_Status__c =='Request Re-Open' ){
                projectParameters.PROJECT_STATUS_CODE = 'Active';
            }
            if(proj.Project_Status__c =='Request Pending Closure' ){
                projectParameters.PROJECT_STATUS_CODE ='Pending Close';
            }
            if(proj.Project_Status__c =='Request Closure' ){
                projectParameters.PROJECT_STATUS_CODE = 'Closed';
            }
             System.debug('projectParameters'+projectParameters);
            // End -Added by Raj - US778
            
            projectParameters.START_DATE = proj.Project_Start_Date__c;
            if (proj.Oracle_Account_ID__c != null) {
                projectParameters.CUSTOMER_ID = Decimal.valueof(proj.Oracle_Account_ID__c);
            }
            ///     projectParameters.OPERATING_UNIT_NAME = 'US_OU';
            projectParameters.OPERATING_UNIT_NAME = proj.Operating_Unit__c;
            //     projectParameters.LONG_NAME = 'SFDC0003';
            if (proj.Request_Type__c != 'Sales PU B&P') {
                projectParameters.CREATED_FROM_PROJECT = 'BID/PROPOSAL';
            } else if (proj.Request_Type__c == 'Sales PU B&P') {
                projectParameters.CREATED_FROM_PROJECT = 'BID/PROPOSAL GENERAL';
            }
            if (updateFlag == true && proj.Project_ID__c != null) {
                projectParameters.PA_PROJECT_ID = Decimal.valueOf(proj.Project_ID__c);
            }
            
            //Customer Parameters
            Project_XSD_WS.APPS_JEG_CUSTOMER_TBL_TYPE customerParameters = new Project_XSD_WS.APPS_JEG_CUSTOMER_TBL_TYPE();
            customerParameters.P_CUSTOMERS_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE>();
            //Customer Record Parameters
            Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE customerRecord = new Project_XSD_WS.APPS_JEG_CUSTOMER_IN_REC_TYPE();
            if (proj.Oracle_Account_ID__c != null) {
                customerRecord.CUSTOMER_ID = Decimal.valueof(proj.Oracle_Account_ID__c);
            }

            customerParameters.P_CUSTOMERS_IN_ITEM.add(customerRecord);
            
            //Key Members Parameters
            //DE404 - Required only for creation and not for update
            Project_XSD_WS.APPS_JEG_PROJECT_ROLE_TBL_TYPE keyMemberParameters = new Project_XSD_WS.APPS_JEG_PROJECT_ROLE_TBL_TYPE();
            if(updateFlag != true){
                keyMemberParameters.P_KEY_MEMBERS_ITEM = new List<Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE>();
                
                //Org Role record1
                Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE orgRecord1 = new Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE();
                orgRecord1.PERSON_EMP_NUMBER = proj.Jacobs_Sales_Lead_Employee_Number__c;
                orgRecord1.PROJECT_ROLE_MEANING = System.Label.Jacobs_Sales_Lead;
                //JS 17/04/2018 DE404 commenting START_DATE
                //orgRecord1.START_DATE = proj.Project_Start_Date__c;
                
                //Org Role record2
                Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE orgRecord2 = new Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE();
                orgRecord2.PERSON_EMP_NUMBER = proj.Project_Accountant_Employee_Number__c;
                orgRecord2.PROJECT_ROLE_MEANING = System.Label.Project_Accountant;
                //JS 17/04/2018 DE404 commenting START_DATE
                //orgRecord2.START_DATE = proj.Project_Start_Date__c;
                
                //Org Role record3
                Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE orgRecord3 = new Project_XSD_WS.APPS_JEG_PROJECT_ROLE_REC_TYPE();
                orgRecord3.PERSON_EMP_NUMBER = proj.Project_Manager_Employee_Number__c;
                orgRecord3.PROJECT_ROLE_MEANING = System.Label.Project_Manager;
                //JS 17/04/2018 DE404 commenting START_DATE
                //orgRecord3.START_DATE = proj.Project_Start_Date__c;
                
                keyMemberParameters.P_KEY_MEMBERS_ITEM.add(orgRecord1);
                keyMemberParameters.P_KEY_MEMBERS_ITEM.add(orgRecord2);
                keyMemberParameters.P_KEY_MEMBERS_ITEM.add(orgRecord3);                
            }
            
            //Class Category Parameters
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_TBL_TYPE classCategoryParameters = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_TBL_TYPE();
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM = new List<Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE>();
            
            //ClassCategoryRecord1 Parameters - Task Validation
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord1 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
            classCategoryRecord1.CLASS_CATEGORY = System.Label.Task_Validation;
            classCategoryRecord1.CLASS_CODE = proj.TasK_Validation__c;
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord1);
            
            //ClassCategoryRecord2 Parameters -  Market Sector
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord2 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
            classCategoryRecord2.CLASS_CATEGORY = System.Label.Market_Sector;
            classCategoryRecord2.CLASS_CODE = proj.Market_Sector__c;
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord2);
            
            //ClassCategoryRecord3 Parameters - Proposal Location
            //Start - US871 Do not send Proposal Location
            /*Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord3 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
classCategoryRecord3.CLASS_CATEGORY = System.Label.Proposal_Location;
classCategoryRecord3.CLASS_CODE = proj.Proposal_Location__c;
classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord3);*/
            //End - US871 Do not send Proposal Location
            
            //ClassCategoryRecord4 Parameters - Area of Business
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord4 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
            classCategoryRecord4.CLASS_CATEGORY = System.Label.Area_of_Business;
            classCategoryRecord4.CLASS_CODE = proj.Area_of_Business__c;
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord4);
            
            //ClassCategoryRecord5 Parameters - Controlling Performance Unit
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord5 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
            classCategoryRecord5.CLASS_CATEGORY = System.Label.Controlling_Performance_Unit;
            classCategoryRecord5.CLASS_CODE = proj.Controlling_Performance_Unit__c;
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord5);
            
            //ClassCategoryRecord6 Parameters - Request Type
            Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord6 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();
            classCategoryRecord6.CLASS_CATEGORY = System.Label.B_P_Request_Type;
            classCategoryRecord6.CLASS_CODE = proj.Request_Type__c;
            classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord6);
            
            //ClassCategoryRecord7 parameters - China Surcharge
            if (proj.Operating_Unit__c == 'CN_OU') {
                Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord7 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();                
                classCategoryRecord7.CLASS_CATEGORY = 'China Surcharge';
                classCategoryRecord7.CLASS_CODE = 'No';
                classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord7);
            }
            
            //ClassCategoryREcord8 parameters - Cross Charge
            //Start --  US778 and US902 Cross Charge logic
            if(proj.Cross_Charge__c){
                projectParameters.ALLOW_CROSS_CHARGE_FLAG =  'Y';
                Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE classCategoryRecord8 = new Project_XSD_WS.APPS_JEG_CLASS_CATEGORY_REC_TYPE();                
                classCategoryRecord8.CLASS_CATEGORY = system.Label.IWA_Letterhead;
                classCategoryRecord8.CLASS_CODE = proj.Office_Receiving_Cross_Charge__c;
                classCategoryParameters.P_CLASS_CATEGORIES_ITEM.add(classCategoryRecord8);
            }
            // End - US778 and US902 Cross Charge logic 
            
            system.debug('*****CC' + classCategoryParameters.P_CLASS_CATEGORIES_ITEM);
            
            //Task Parameters
            Project_XSD_WS.APPS_JEG_TASK_IN_TBL_TYPE taskParameters = new Project_XSD_WS.APPS_JEG_TASK_IN_TBL_TYPE();
            
            //Org Roles Table
            Project_XSD_WS.APPS_JEG_PROJECT_ROLE_TBL_TYPE orgRoles = new Project_XSD_WS.APPS_JEG_PROJECT_ROLE_TBL_TYPE();
            
            //Structure
            Project_XSD_WS.APPS_JEG_STRUCTURE_IN_REC_TYPE structure = new Project_XSD_WS.APPS_JEG_STRUCTURE_IN_REC_TYPE();
            
            //External Attributes Table
            Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_TABLE_TYPE extAttributes = new Project_XSD_WS.APPS_JEG_PA_EXT_ATTR_TABLE_TYPE();
            
            //Deliverables
            Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_TBL_TYPE deliverables = new Project_XSD_WS.APPS_JEG_DELIVERABLE_IN_TBL_TYPE();
            
            //Deliverable Actions
            Project_XSD_WS.APPS_JEG_ACTION_IN_TBL_TYPE deliverableActions = new Project_XSD_WS.APPS_JEG_ACTION_IN_TBL_TYPE();
            
            //Agreement
            Project_XSD_WS.APPS_JEG_AGREEMENT_REC_IN_TYPE agreement = new Project_XSD_WS.APPS_JEG_AGREEMENT_REC_IN_TYPE();
            agreement.TEMPLATE_FLAG = 'N';
            
            //Funding
            Project_XSD_WS.APPS_JEG_FUNDING_IN_TBL_TYPE funding = new Project_XSD_WS.APPS_JEG_FUNDING_IN_TBL_TYPE();
            
            //Budget
            Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_TBL_TYPE budget = new Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_TBL_TYPE();
            
            budget.P_BUDGET_IN_ITEM = new List<Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE>();
            //BudgetRecordParameters Parameters
            Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE budgetRecord = new Project_XSD_WS.APPS_JEG_BUDGET_LINE_IN_REC_TYPE();
            budgetRecord.RAW_COST = proj.B_P_Budget_Local__c;
            budgetRecord.BURDENED_COST = proj.B_P_Budget_Local__c;
            //Start - Added by Raj -US 769 
            budgetRecord.QUANTITY = proj.nu_workhours__c ;
            //End - Added by Raj -US 769 
            //budgetRecord.QUANTITY = 1;
            
            budget.P_BUDGET_IN_ITEM.add(budgetRecord);
            
            
            //setting the timeout to Maximum Value          
            request.timeout_x = 120000;
            
            //Callout String response
            result = request.process('SFDC', proj.id, projectParameters, customerParameters, keyMemberParameters, classCategoryParameters,
                                     taskParameters, orgRoles, structure, extAttributes, deliverables, deliverableActions, agreement, funding, budget);
            System.debug('result'+result);
        }
        
        // All the system.calloutExceptions are handled by the below Catch block. 
        // The callout is re-tried 3 times before logging an error
        catch (System.CalloutException ex) {
            ProjectQueueableHandler job = new ProjectQueueableHandler(proj);
            job.max = max;
            job.updateFlag = updateFlag;
            job.counter = counter + 1;
            if (max >= job.counter && !test.isRunningTest()) {
                system.debug('***' + job.counter);
                System.enqueueJob(job);
            } else {
                system.debug('***' + job.counter);
                String message = ex.getMessage().length() > 4000 ?  ex.getMessage().subString(0, 4000) : ex.getMessage();
                retryService.logEntry(
                    proj.Id,
                    Jacobs_Project__c.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'ProjectQueueableHandler',
                    'B&P Approved',
                    'InfraException',
                    message,
                    'Callout Exception'
                );
            }
            system.debug('Callout Exception: ' + ex);
        }
        
        // All other exceptions are handled in the below catch block. 
        // Here also the callout is retried 3 times before logging an error.
        catch (Exception ex) {
            ProjectQueueableHandler job = new ProjectQueueableHandler(proj);
            job.max = max;
            job.updateFlag = updateFlag;
            job.counter = counter + 1;
            if (max >= job.counter) {
                system.debug('***' + job.counter);
                System.enqueueJob(job);
            } else {
                system.debug('***' + job.counter);
                String message = ex.getMessage().length() > 4000 ?  ex.getMessage().subString(0, 4000) : ex.getMessage();
                retryService.logEntry(
                    proj.Id,
                    Jacobs_Project__c.getSObjectType().getDescribe().getName(),
                    'SFDC Sales',
                    'ProjectQueueableHandler',
                    'B&P Approved',
                    'InfraException',
                    message,
                    'Other Exception'
                );
            }
            system.debug('Exception from callout:' + ex);
        }
        
        system.debug(result);
        retryService.deleteRetryLogIfNeeded();
    }
    
}