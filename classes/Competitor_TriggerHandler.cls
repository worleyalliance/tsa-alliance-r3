/**************************************************************************************
Name: Partner_TriggerHandler
Version: 1.0
Created Date: 05/03/2018
Function: Handler for Partner Trigger

Modification Log:
-- -- -- -- -- -- -- -onBeforeInsert- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    05/03/2018      Original Version
* Chris Cornejo      01/02/2019      US2749 - Key Wins / Losses Report Requirements
*************************************************************************************/

public with sharing class Competitor_TriggerHandler {
private static Competitor_TriggerHandler handler;
    
    /*
    * Singleton like pattern
    */
    public static Competitor_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Competitor_TriggerHandler();
        }
        return handler;
    }
    
   /*
    * Actions performed on competitor records after insert
    */
    public void onAfterInsert(List<Competitor__c > newCompetitor){
       recalculateOppCompetitorFields(newCompetitor,null);
    }
    /*
    * Actions performed on competitor records after update
    */
    public void onAfterUpdate(Map<Id,Competitor__c> newCompetitor, Map<Id,Competitor__c> oldCompetitor) {
        List<Competitor__c > lstCompetitor = new List <Competitor__c>(); 
        List<Id> oldOppIds = new List <Id>(); 
        for(Competitor__c competitor :newCompetitor.values()){
            lstCompetitor.add(competitor);
            oldOppIds.add(oldCompetitor.get(competitor.Id).opportunity__c);
        }
        system.debug('lstCompetitor'+lstCompetitor);
        resetOppCompetitorFields(lstCompetitor,oldOppIds);
    }
    /*
    * Actions performed on competitor records after delete
    */
    public void onAfterDelete(List<Competitor__c > oldCompetitor) {
        List<Competitor__c > lstCompetitor = new List <Competitor__c>();
        List<Id> oldOppIds = new List <Id>();
        for(Competitor__c competitor :oldCompetitor){
            lstCompetitor.add(competitor);
            oldOppIds.add(competitor.opportunity__c);
        }
        system.debug('lstCompetitor'+lstCompetitor);
        system.debug('oldOppIds'+oldOppIds);
        resetOppCompetitorFields(lstCompetitor,oldOppIds);
    }
    
    public void recalculateOppCompetitorFields(List<Competitor__c > newCompetitor,List<ID> oldOpportunityIDs){
        List<ID> competitorIDs = new List<ID>();
        List<ID> opportunityIDs = new List<ID>();
        Map<ID,Set<String>> mapCompetitors = new Map<ID,Set<String>>();
        //US2749 - add map for Opportunity Winners
        Map<ID,Set<String>> mapWinners = new Map<ID,Set<String>>();
       
        for(Competitor__c comp :newCompetitor){
            {
                competitorIDs.add(comp.ID); 
                opportunityIDs.add(comp.Opportunity__c);
            }
        }
        // merge Opportunites from OldOpportunity
        if(oldOpportunityIDs != Null)
            opportunityIDs.addAll(oldOpportunityIDs);
        
        //Query to get opp and account details
        Map<ID, Competitor__c> mapIDCompetitor = new Map<ID, Competitor__c>([Select ID, Opportunity__r.Competitors__c, Competitor__r.Name
                                                                           from Competitor__c
                                                                           where Opportunity__c in :opportunityIDs]);                                                                                                          
        system.debug('competitorIDs'+mapIDCompetitor);
        for(Competitor__c comp : mapIDCompetitor.values()){
        
                if(mapCompetitors.containsKey(comp.Opportunity__c)) {
                    Set<String> competitors = mapCompetitors.get(comp.Opportunity__c);
                    competitors.add(comp.Competitor__r.Name);
                    mapCompetitors.put(comp.Opportunity__c, competitors);
                } else {
                    mapCompetitors.put(comp.Opportunity__c, new Set<String> { comp.Competitor__r.Name });
                }    
        }
        system.debug('mapCompetitors '+mapCompetitors);
    
        // Calling update method
        updateOpportunity(mapCompetitors);

        //US2749 - add mapping for opportunity winners
        Map<ID, Competitor__c> mapIDWinners = new Map<ID, Competitor__c>([Select ID, Opportunity__r.Competitors__c, Competitor__r.Name, Opportunity__r.opportunity_winner__c
                                                                           from Competitor__c
                                                                           where Opportunity__c in :opportunityIDs and Opportunity_winner__c = TRUE]);

        if (!mapIDWinners.isEmpty()){
            for(Competitor__c comp : mapIDWinners.values()){
                if(mapWinners.containsKey(comp.Opportunity__c)) {
                    Set<String> competitors = mapWinners.get(comp.Opportunity__c);
                    competitors.add(comp.Competitor__r.Name);
                    mapWinners.put(comp.Opportunity__c, competitors);
                } else {
                    mapWinners.put(comp.Opportunity__c, new Set<String> { comp.Competitor__r.Name });
                }    
            }
        
        // Calling update opportunity winner
        updateOpportunityWinners(mapWinners);
        }
    
    }
    public void updateOpportunity(Map<ID,Set<String>> mapToProcess){
        List<Opportunity> updateOppList = new List<Opportunity>();
        for(ID oppId :mapToProcess.keyset()){
            Opportunity opp = new opportunity();
            opp.Id = oppId;
            String competitorName = '';
            for(String s:maptoProcess.get(oppId)) {
                if((competitorName.length() + s.length()) < 255)
                    competitorName += (competitorName==''?'':',')+s;
                else{
                   competitorName += '..';  
                }
            }
            opp.Competitors__c = competitorName;
            updateOppList.add(opp);
        }
        if(updateOppList !=null){
            try{
                update updateOppList;
            }catch(Exception e){
                system.debug('Exception '+e);
            }
        } 
    }
    //method to update opportunity winner
    public void updateOpportunityWinners(Map<ID,Set<String>> mapToProcess){
        List<Opportunity> updateOppList = new List<Opportunity>();
        for(ID oppId :mapToProcess.keyset()){
            Opportunity opp = new opportunity();
            opp.Id = oppId;
            String competitorName = '';
            for(String s:maptoProcess.get(oppId)) {
                if((competitorName.length() + s.length()) < 255)
                    competitorName += (competitorName==''?'':',')+s;
                else{
                   competitorName += '..';  
                }
            }
            opp.Opportunity_winner__c = competitorName;
            updateOppList.add(opp);
        }
        if(updateOppList !=null){
            try{
                update updateOppList;
            }catch(Exception e){
                system.debug('Exception '+e);
            }
        } 
    }
    public void resetOppCompetitorFields(List<Competitor__c > newCompetitor, List<ID> oldOppIds){
        List<Opportunity> resetOpp = new List<Opportunity>();
        for(ID oppID : oldOppIds){
            Opportunity opp = new Opportunity();
            opp.ID= oppID;
            opp.Competitors__c='';
            //US2749 - adding opportunity winner
            opp.Opportunity_winner__c='';
            if(!resetOpp.contains(opp))
                resetOpp.add(opp);
        } 
        for(Competitor__c comp : newCompetitor){
            Opportunity opp = new Opportunity();
            opp.ID= comp.Opportunity__c;
            opp.Competitors__c='';
            //US2749 - adding opportunity winner
            opp.opportunity_winner__c='';
            if(!resetOpp.contains(opp) && opp.ID !=null){
                resetOpp.add(opp);
                system.debug('Delete '+resetOpp);
            }
        }  
        system.debug('Delete list in Reset method'+resetOpp);
        if(resetOpp !=null){
            try{
              update resetOpp;
            }catch(Exception e){
               system.debug('Exception '+e);
            }
        }
        // recalculating Competitor values        
        recalculateOppCompetitorFields(newCompetitor,oldOppIds);
    }
}