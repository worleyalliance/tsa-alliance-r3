/**************************************************************************************
Name:BatchGenerateOpportunityNumber
Version: 1.0 
Created Date: 12.28.2018
Function: Batch class to create Opportunity number

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                   
* Pradeep Shetty    12/28/2018      Original Version
*************************************************************************************/
global class BatchGenerateOpportunityNumber implements Database.Batchable<sObject> {
	
	String query;
	
	global BatchGenerateOpportunityNumber() {
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		//Get all Multi Office
		query = 'Select Id, System_Number__c, Opportunity_Number__c from Opportunity where Opportunity_Number__c=null and IsDeleted=false Order by AccountId';
		return Database.getQueryLocator(query);
	}

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Opportunity> oppList = new List<Opportunity>();

        for(sobject s : scope){
            oppList.add((Opportunity)s);
        }
        
        OpportunityNumber.generateOpportunityNumber(oppList);
    }
	
	global void finish(Database.BatchableContext BC) {
	}
	
}