/**************************************************************************************
Name:BPProjectReopenQuickActionCntrl_Test
Version: 1.0 
Created Date: 08/15/2017
Function: test class to test the Request Closure and Request ReOpen functionality

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    08/15/2017       Original Version
* Madhu Shetty      10/06/2017       (US1076) Updated the testCheckRequestAllowed function to check if a request owner 
                                     or B&P Coordinator can close or re-open a B&P request. 
*************************************************************************************/
@isTest
private class BPProjectReopenQuickActionCntrl_Test {

  //Constants
  private static final String BP_STATUS_ACTIVE         = 'Active';
  private static final String BP_STATUS_CLOSED         = 'Closed';
  private static final String BP_STATUS_REOPEN         = 'Request Re-Open';
  private static final String BP_STATUS_CLOSURE        = 'Request Closure';
  private static final String BP_CLOSE                 = 'Close';
  private static final String BP_REOPEN                = 'ReOpen';
  private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
  private static final String PROJECT_FIN_RECTYPE      = 'Finance';

  private static final String USERNAME                 = 'salesBPRequestActions@testuser.com'; 


  //Test data setup
  @testSetup
  static void setUpTestData(){

    //Create B&P records and assign it to respective variables

    //Create test user
    TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

    
    System.runAs(salesUser){

      //Account required for contacts
      TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
      Account acct = accountBuilder.build().save().getRecord(); 

      //Create records required for creating a B&P
      TestHelper.ContactBuilder contactBuilder = new TestHelper.ContactBuilder();
      Contact con = contactBuilder.build().withAccount(acct.Id).save().getRecord();

      //List of B&Ps to be created
      List<B_P_Request__c> bpList = new List<B_P_Request__c>();

      //List of Projects to be created
      List<Jacobs_Project__c> projectList = new List<Jacobs_Project__c>();

      //Create B&P Builder
      TestHelper.BPRequestBuilder bpRequestBuilder = new TestHelper.BPRequestBuilder();

      //Create project builder
      TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();

      //Active B&P
      bpList.add(bpRequestBuilder.build(con.Id,
                                         con.Id,
                                         con.Id,
                                         null)
                                 .withBudget(100)
                                 .withSellingUnit('BIAF - Americas S-South Central')
                                 .withSalesBPUnit('019243')
                                 .withOperatingUnit('US_OU')
                                 .withLegalEntity('2466')
                                 .withStatus(BP_STATUS_ACTIVE)
                                 .getRecord());

      //Closed B&P
      bpList.add(bpRequestBuilder.build(con.Id,
                                         con.Id,
                                         con.Id,
                                         null)
                                 .withBudget(100)
                                 .withSellingUnit('BIAF - Americas S-South Central')
                                 .withSalesBPUnit('019243')
                                 .withOperatingUnit('US_OU')
                                 .withLegalEntity('2466')
                                 .withStatus(BP_STATUS_CLOSED)
                                 .getRecord());     

      //Legacy B&P
      bpList.add(bpRequestBuilder.build(con.Id,
                                         con.Id,
                                         con.Id,
                                         null)
                                 .withBudget(100)
                                 .withSellingUnit('BIAF - Americas S-South Central')
                                 .withSalesBPUnit('019243')
                                 .withOperatingUnit('US_OU')
                                 .withLegalEntity('2466')
                                 .withStatus(BP_STATUS_ACTIVE)
                                 .withLegacyBP(false)
                                 .withProjectAccountant(con.Id)
                                 .withJacobsSalesLead(con.Id)
                                 .withProjectManager(con.Id)
                                 .getRecord()); 

      //Insert BPs
      insert bpList;

      //Create Projects and associate them with B&P
      for(B_P_Request__c currentBP: bpList)
      {
        if(currentBP.Status__c == BP_STATUS_ACTIVE)
        {
          //Create an active Project tied to this B&P
          projectList.add(projectBuilder.build()
                                        .withBPRequest(currentBP.Id)
                                        .withRecordType(PROJECT_FIN_RECTYPE)
                                        .getRecord());
        }
        else if(currentBP.Status__c == BP_STATUS_CLOSED)
        {
          //Create a Closed Project tied to this B&P
          projectList.add(projectBuilder.build()
                                        .withBPRequest(currentBP.Id)
                                        .withRecordType(PROJECT_FIN_RECTYPE)
                                        .withStatus(BP_STATUS_CLOSED)
                                        .getRecord()); 
        }
      } //END FOR

      //Insert Projects
      insert projectList;
    }
  }

  /*Scenario 1: test the checkRequestAllowed method
  * Create an active B&P and test if the request owner or the B&P coordinator can request for closure.
  * Create a closed B&P and check if the request owner or the B&P coordinator can request for reopening
  * Create a legacy B&P record and check if the request owner or the B&P coordinator can reopen or close it. 

  * Create an active B&P and test if a user who is not the request owner or the B&P coordinator can request for closure.
  * Create a closed B&P and check if a user who is not the request owner or the B&P coordinator can request for reopening
  * Create a legacy B&P record and check if a user who is not the request owner or the B&P coordinator can reopen or close it. 
  */
  private static testmethod void testCheckRequestAllowed(){

    //Get the user
    User salesUser = [Select Id, Name, Approval_Level__c from User where UserName = :USERNAME Limit 1];

    //US2648 - check to see if user is an administrator or a B&P Coodinator
    Boolean isAdministrator = UserDataProvider.isUserAnAdministrator(salesUser.Id);
    Boolean isBpCoordinator = salesUser.Approval_Level__c == 'B&P Coordinator';
      
    test.startTest();
    //Execute tests
    System.runAs(salesUser){

      //Run test for B&P's where the current user is the B&P owner or the B&P Coordinator
      for(B_P_Request__c currentBP: [Select Id, 
                                            Status__c,
                                            Legacy_B_P__c 
                                     From B_P_Request__c 
                                     Where OwnerId = :salesUser.Id
                                     OR   B_P_Coordinator__c = :salesUser.Id])
      {
        if(currentBP.Status__c == BP_STATUS_ACTIVE && 
           !currentBP.Legacy_B_P__c)
        {
          //Close an active B&P
          System.assert(BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close'));

          //Open an active B&P
          System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'ReOpen')); 
        }

        else if(currentBP.Status__c == BP_STATUS_CLOSED && 
           !currentBP.Legacy_B_P__c)
        {
          //Open a closed B&P
          System.assert(BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'ReOpen'));

          //Close a closed B&P
          System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close')); 
        }

        else if(currentBP.Legacy_B_P__c)
        {
          //Close active Legacy B&P
          System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close'));
        }
      }
      
      //Run test for B&P's where the current user is neither the B&P owner nor the B&P Coordinator
      for(B_P_Request__c currentBP: [Select Id, 
                                            Status__c,
                                            Legacy_B_P__c 
                                     From B_P_Request__c 
                                     Where OwnerId != :salesUser.Id
                                     AND   B_P_Coordinator__c != :salesUser.Id LIMIT 25])
      {
          //US2648 - if the current user is not an administrator or a B&P Coordinator, they should not be able to close or reopen.
          if(!(isAdministrator || isBpCoordinator)){
              System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close'));
              System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'ReOpen'));
          }
          
          else if(currentBP.Status__c == BP_STATUS_ACTIVE && !currentBP.Legacy_B_P__c)
          {
              //Close an active B&P
              System.assert(BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close'));
              
              //Open an active B&P
              System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'ReOpen')); 
          }
          
          else if(currentBP.Status__c == BP_STATUS_CLOSED && 
                  !currentBP.Legacy_B_P__c)
          {
              //Open a closed B&P
              System.assert(BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'ReOpen'));
              
              //Close a closed B&P
              System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close')); 
          }
          
          else if(currentBP.Legacy_B_P__c)
          {
              //Close active Legacy B&P
              System.assert(!BPProjectReopenQuickActionCntrl.checkRequestAllowed(currentBP.Id, 'Close'));
          }
      }
    }
    test.stopTest();
  }  

  /*Scenario 2: test the updateBPStatus method
  * Create an active B&P and request for closure. Related project status should change.
  * Create a closed B&P and request for reopen. Related project status should change. 
  */    
  private static testmethod void updateBP(){
    //Get the user
    User salesUser = [Select Id from User where UserName = :USERNAME Limit 1];  

    test.startTest();
    //Execute tests
    System.runAs(salesUser){

      for(B_P_Request__c currentBP: [Select Id, 
                                            Status__c,
                                            Legacy_B_P__c 
                                     From B_P_Request__c 
                                     Where CreatedById = :salesUser.Id 
                                     And Legacy_B_P__c!=True])
      {
        if(currentBP.Status__c == BP_STATUS_ACTIVE)
        {
          BPProjectReopenQuickActionCntrl.updateBPStatus(currentBP.Id, BP_STATUS_CLOSURE);  
        }
        else if(currentBP.Status__c == BP_STATUS_CLOSED)
        {
          BPProjectReopenQuickActionCntrl.updateBPStatus(currentBP.Id, BP_STATUS_REOPEN);           
        }
      } //END FOR  

      //Validate if project status changed
      for(Jacobs_Project__c currentProject: [Select Id, 
                                                    B_P_Request__r.Status__c,
                                                    B_P_Request__r.Legacy_B_P__c,
                                                    Project_Status__c 
                                             From Jacobs_Project__c 
                                             Where CreatedById = :salesUser.Id 
                                             And B_P_Request__r.Legacy_B_P__c!=True])
      {

        if(currentProject.B_P_Request__r.Status__c == BP_STATUS_ACTIVE)
        {
          System.assertEquals(BP_STATUS_CLOSURE,currentProject.Project_Status__c); 
        }
        else if(currentProject.B_P_Request__r.Status__c == BP_STATUS_CLOSED)
        {
          System.assertEquals(BP_STATUS_REOPEN,currentProject.Project_Status__c);      
        }
      } //END FOR                                                 

    } 

    test.stopTest();
      
  }
    
}