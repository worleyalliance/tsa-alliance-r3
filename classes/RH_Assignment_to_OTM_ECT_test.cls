/**************************************************************************************
Name: RH_Assignment_to_OTM_ECT_test
Function: Resource Hero - Test class related to RH_Assignment_to_OTM_ECT

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-11-09          Original Version
* Chris Cornejo             2018-11-20          Fixed OppTeamBuilder method due to change on TestHelper
*************************************************************************************/
@isTest
public class RH_Assignment_to_OTM_ECT_test { 
    
    /*
    //Constants
    private static final String PROFILE_SALES_SUPER_USER    = 'Sales Super User';
    private static final String USERNAME1                   = 'teamMemberTriggerTest1@jacobstestuser.net';
    private static final String USERNAME2                   = 'teamMemberTriggerTest2@jacobstestuser.net';
    private static final String USERNAME3                   = 'teamMemberTriggerTest3@jacobstestuser.net';
    private static final String USERNAME4                   = 'teamMemberTriggerTest4@jacobstestuser.net';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        //Ensure that our default opportunity lookup is configured
        ResourceHeroApp__RHA_Object_Translation__c ot = ResourceHeroApp__RHA_Object_Translation__c.getInstance('ResourceHeroApp__opportunity__c');
        if(ot == null) {
            ResourceHeroApp__RHA_Object_Translation__c opp_ot = new ResourceHeroApp__RHA_Object_Translation__c();
            opp_ot.Name = 'ResourceHeroApp__opportunity__c';
            opp_ot.ResourceHeroApp__Object_Name__c = 'opportunity';
            opp_ot.ResourceHeroApp__Name_Field__c = 'Name';
            
            insert opp_ot;
        }
        
        //Add key settings for RHA operation
        List<ResourceHeroApp__RHA_Configuration__c> rhaconfig = new List<ResourceHeroApp__RHA_Configuration__c>();
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Days in Work Week', ResourceHeroApp__Value__c = '5')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Start of Week', ResourceHeroApp__Value__c = '2')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Over', ResourceHeroApp__Value__c = '#fa8072')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Target', ResourceHeroApp__Value__c = '#62b762')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Under', ResourceHeroApp__Value__c = '#d3d3d3'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Name for Resource Not Assigned', ResourceHeroApp__Value__c = 'Not yet assigned'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Sort Type', ResourceHeroApp__Value__c = 'CREATEDDATE'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Sort Order', ResourceHeroApp__Value__c = 'DESC'));
        insert rhaconfig;
        
        //Create matrix enabled field custom settings
        List<ResourceHeroApp__RHA_Matrix_Enabled_Fields__c> settings = new List<ResourceHeroApp__RHA_Matrix_Enabled_Fields__c>();
        ResourceHeroApp__RHA_Matrix_Enabled_Fields__c forecast = new ResourceHeroApp__RHA_Matrix_Enabled_Fields__c();
        forecast.Name = 'forecast';
        forecast.ResourceHeroApp__Field_Name__c = 'ResourceHeroApp__Forecast__c';
        forecast.ResourceHeroApp__Notes_Field_Name__c = 'ResourceHeroApp__Forecast_Notes__c';
        forecast.ResourceHeroApp__Save_Button_Text__c = 'Save Forecast';
        forecast.ResourceHeroApp__Dropdown_Text__c = 'Forecast';
        forecast.ResourceHeroApp__Use_Resource_Targets__c = true;
        settings.add(forecast);
        
        ResourceHeroApp__RHA_Matrix_Enabled_Fields__c actual = new ResourceHeroApp__RHA_Matrix_Enabled_Fields__c();
        actual.Name = 'actual';
        actual.ResourceHeroApp__Field_Name__c = 'ResourceHeroApp__Actual__c';
        actual.ResourceHeroApp__Notes_Field_Name__c = 'ResourceHeroApp__Actual_Notes__c';
        actual.ResourceHeroApp__Save_Button_Text__c = 'Save Actual';
        actual.ResourceHeroApp__Dropdown_Text__c = 'Actual';
        actual.ResourceHeroApp__Use_Resource_Targets__c = true;
        settings.add(actual);
        insert settings;
        
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser1 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME1).save().getRecord();
        User testUser2 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME2).save().getRecord();
        User testUser3 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME3).save().getRecord();
        User testUser4 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME4).save().getRecord();
        
        
        //Create test records
        system.runAs(testUser1){    
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            //add new OpportunityTeamMembember method
            TestHelper.OpportunityTeamMemberBuilder oppteamMemberBuilder = new TestHelper.OpportunityTeamMemberBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            //Opportunity opp = oppBuilder.build().withAccount(acc.Id).withTeamMember(testUser2.Id).save().getOpportunity();
            Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
            OpportunityTeamMember otm = oppteamMemberBuilder.build().withOpportunity(opp.id).withUser(testUser2.id).save().getRecord();
        }
    }
    
    
    
    
    @isTest
    private static void OTM_Confirm_Duplicate_OTM_Not_Created() {       
        //Verify we our standard OTMs
        List<OpportunityTeamMember> verify_before = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(2, verify_before.size());
        
        //Get all users
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2];
        User testUser3 = [SELECT Id FROM User WHERE Username = :USERNAME3];
        User testUser4 = [SELECT Id FROM User WHERE Username = :USERNAME4];
        
        //Create test resource records
        List<ResourceHeroApp__Resource__c> res = new List<ResourceHeroApp__Resource__c>();
        res.add(new ResourceHeroApp__Resource__c(Name = 'Test Res 1', ResourceHeroApp__User__c = testUser3.Id, ResourceHeroApp__Weekly_Target_Min_Hours__c = 30, ResourceHeroApp__Weekly_Target_Max_Hours__c = 40));
        insert res;
        
        //Get the parent Opportunity
        Opportunity opp = [Select Id, 
                           AccountId 
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        //Create assignment
        ResourceHeroApp__Resource_Assignment__c ra = new ResourceHeroApp__Resource_Assignment__c();
        ra.ResourceHeroApp__Opportunity__c = opp.Id;
        ra.ResourceHeroApp__Resource__c = res[0].Id;
        ra.ResourceHeroApp__Role__c = 'BD Coordinator';
        insert ra;
        
        //Verify our new OTM has been added
        List<OpportunityTeamMember> verify_middle = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(3, verify_middle.size());
        
        
        //Make a change to our RA
        ra.ResourceHeroApp__Role__c = 'Construction Manager';
        update ra;
        
        
        //Verify no new OTMs have been added
        List<OpportunityTeamMember> verify_end = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(3, verify_end.size());
        
        
        // Add team members to the Parent Opportunity
        List<OpportunityTeamMember> lstNewTm = new List<OpportunityTeamMember>();
        OpportunityTeamMember newTm1 = new OpportunityTeamMember();
        newTm1.OpportunityAccessLevel ='Edit';
        newTm1.OpportunityId          = opp.Id;
        newTm1.TeamMemberRole         ='BD Coordinator';
        newTm1.UserId                 = testUser3.ID;
        lstNewTm.add(newTm1);
        
        // Insert team members in Parent Opportunity
        insert lstNewTm;
        
        //Confirm that the assignment record has been created
        List<ResourceHeroApp__Resource_Assignment__c> verify_after = [SELECT Id, ResourceHeroApp__Resource__c, ResourceHeroApp__Role__c FROM ResourceHeroApp__Resource_Assignment__c];
        system.assertEquals(1, verify_after.size());
        system.assertEquals(res[0].Id, verify_after[0].ResourceHeroApp__Resource__c);
        system.assertEquals('BD Coordinator', verify_after[0].ResourceHeroApp__Role__c);
        
    }
    
    
    @isTest
    private static void OTM_Confirm_Duplicate_ETC_Not_Created() {       
        //Verify we our standard OTMs
        List<OpportunityTeamMember> verify_before = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(2, verify_before.size());
        
        //Get all users
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2];
        User testUser3 = [SELECT Id FROM User WHERE Username = :USERNAME3];
        User testUser4 = [SELECT Id FROM User WHERE Username = :USERNAME4];
        
        List<Account> acc = [SELECT Id FROM Account];
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', AccountId = acc[0].Id, User__c = System.userInfo.getUserId(), Utilize_in_Resource_Hero__c = true));
        insert cons;
        
        List<ResourceHeroApp__Resource__c> res = [SELECT Id, ResourceHeroApp__User__c FROM ResourceHeroApp__Resource__c];
        for(ResourceHeroApp__Resource__c re : res) {
            re.ResourceHeroApp__User__c = null;
        }
        update res;
                
        //Get the parent Opportunity
        Opportunity opp = [Select Id, 
                           AccountId 
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        //Create assignment
        ResourceHeroApp__Resource_Assignment__c ra = new ResourceHeroApp__Resource_Assignment__c();
        ra.ResourceHeroApp__Opportunity__c = opp.Id;
        ra.ResourceHeroApp__Resource__c = res[0].Id;
        ra.ResourceHeroApp__Role__c = 'BD Coordinator';
        insert ra;
        
        //Verify that ETC was created
        List<Extended_Capture_Team__c> verify_ect = [SELECT Id FROM Extended_Capture_Team__c];
        system.assertEquals(1, verify_ect.size());
        
        //Make a change to our RA
        ra.ResourceHeroApp__Role__c = 'Construction Manager';
        update ra;
        
        
        //Verify no new ETCs have been added
        List<Extended_Capture_Team__c> verify_end = [SELECT Id FROM Extended_Capture_Team__c];
        system.assertEquals(1, verify_end.size());
        
    }
    
    
    
    @isTest
    private static void OTM_Confirm_OppTeam_Create() {       
        //Verify we only have one OTM
        List<OpportunityTeamMember> verify_before = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(2, verify_before.size());
        
        //Get all users
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2];
        User testUser3 = [SELECT Id FROM User WHERE Username = :USERNAME3];
        User testUser4 = [SELECT Id FROM User WHERE Username = :USERNAME4];
        
        //Create test resource records
        List<ResourceHeroApp__Resource__c> res = new List<ResourceHeroApp__Resource__c>();
        res.add(new ResourceHeroApp__Resource__c(Name = 'Test Res 1', ResourceHeroApp__User__c = testUser3.Id, ResourceHeroApp__Weekly_Target_Min_Hours__c = 30, ResourceHeroApp__Weekly_Target_Max_Hours__c = 40));
        insert res;
        
        //Get the parent Opportunity
        Opportunity opp = [Select Id, 
                           AccountId 
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        //Create assignment
        ResourceHeroApp__Resource_Assignment__c ra = new ResourceHeroApp__Resource_Assignment__c();
        ra.ResourceHeroApp__Opportunity__c = opp.Id;
        ra.ResourceHeroApp__Resource__c = res[0].Id;
        ra.ResourceHeroApp__Role__c = 'BD Coordinator';
        insert ra;
        
        //Verify we only have one new OTM
        List<OpportunityTeamMember> verify_otm = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
        system.assertEquals(3, verify_otm.size());
        
        //Verify we do not have any ETM
        List<Extended_Capture_Team__c> verify_ect = [SELECT Id FROM Extended_Capture_Team__c];
        system.assertEquals(0, verify_ect.size());
    }
    
    
    @isTest
    private static void OTM_Confirm_ETC_Create() {       
        //Verify we only have one OTM
        List<OpportunityTeamMember> verify_before = [SELECT Id FROM OpportunityTeamMember];
        system.assertEquals(2, verify_before.size());
        
        //Get all users
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2];
        User testUser3 = [SELECT Id FROM User WHERE Username = :USERNAME3];
        User testUser4 = [SELECT Id FROM User WHERE Username = :USERNAME4];
        
        List<Account> acc = [SELECT Id FROM Account];
        
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(FirstName = 'Bill', LastName = 'Kuehler', AccountId = acc[0].Id, User__c = System.userInfo.getUserId(), Utilize_in_Resource_Hero__c = true));
        insert cons;
        
        List<ResourceHeroApp__Resource__c> res = [SELECT Id, ResourceHeroApp__User__c FROM ResourceHeroApp__Resource__c];
        for(ResourceHeroApp__Resource__c re : res) {
            re.ResourceHeroApp__User__c = null;
        }
        update res;
        
        //Get the parent Opportunity
        Opportunity opp = [Select Id, 
                           AccountId 
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        //Create assignment
        ResourceHeroApp__Resource_Assignment__c ra = new ResourceHeroApp__Resource_Assignment__c();
        ra.ResourceHeroApp__Opportunity__c = opp.Id;
        ra.ResourceHeroApp__Resource__c = res[0].Id;
        ra.ResourceHeroApp__Role__c = 'BD Coordinator';
        insert ra;
        
        //Verify we only have original OTM
        List<OpportunityTeamMember> verify_otm = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
        system.assertEquals(2, verify_otm.size());
        
        //Verify that ETC was created
        List<Extended_Capture_Team__c> verify_ect = [SELECT Id FROM Extended_Capture_Team__c];
        system.assertEquals(1, verify_ect.size());
    }

*/
}