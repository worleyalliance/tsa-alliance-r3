@isTest
public class licenseTrackerNightlyTests {

    @testSetup
    static void setupTestData(){
        Id acctRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The AccountRecordTypeId: ' + acctRecordTypeId);
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Resource Contact').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The ContactRecordTypeId: ' + conRecordTypeId);
        Id lrRecordTypeId1 = Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('New License Request').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The LicenceRequestRecordTypeId1: ' + lrRecordTypeId1);
        Account tAcct = new Account(Name='TestAccount', RecordTypeId=acctRecordTypeId);
        Insert tAcct;
        System.assertNotEquals(tAcct.Id, null);
        Contact tCon = new Contact(FirstName='TestFN1', Lastname='TestLN1', Email='tu1@example.com', RecordTypeId = conRecordTypeId, AccountId=tAcct.Id);
        Insert tCon;
        System.assertNotEquals(tCon.Id, null);
        System.Debug(LoggingLevel.INFO,'The Contact Id: ' + tCon.Id);
        Licenses__c tLic = new Licenses__c(Name='TestLic1', License_Type__c='Full', Line_of_Business__c='BIAF', Business_Unit__c='BIAF - Europe', Number_of_Licenses_Allocated__c=10);
        Insert tLic;
        System.assertEquals(10, tLic.Number_of_Licenses_Allocated__c);
        System.Debug(LoggingLevel.INFO,'The License Id: ' + tLic.Id);
        License_Request__c tLicReq = new License_Request__c(RecordTypeId = lrRecordTypeId1,
                                                            License_Type_Requested__c='Full', 
                                                            Requested_For__c = tCon.Id,
                                                            Line_of_Business__c = 'BIAF',
                                                            Business_Unit__c = 'BIAF - Europe',
                                                            Selling_Unit__c = 'BIAF - Europe UK Aviation',
                                                            License__c = tLic.Id,
                                                            Requested_Profile__c = 'Sales Super User',
                                                            Requested_Roles__c = SObjectType.License_Request__c.Fields.Requested_Roles__c.PicklistValues[0].getValue() );
        Insert tLicReq;
        /*User tu1 = new User(ProfileId = UserInfo.getProfileId(),
                          LastName = 'TestLN1',
                          Email = 'tu1@test.com',
                          Username = 'tu1@licensetracker.test.com' + System.currentTimeMillis(),
                          Alias = 'tu1alias',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          IsActive = TRUE,
                          //Line_of_Business__c ='Buildings & Infrastructure',
                          //Business_Unit__c = 'B&I-Americas',
                          //Selling_Unit__c = 'B&I - Americas Central',
                          UserRoleId = UserInfo.getUserRoleId());    
        Insert tu1;*/
    }
    
    public static testMethod void testUserNightly() {
        Test.StartTest();
        Insert testCreateNewUser();
        licenseTrackerUserNightly licUser = new licenseTrackerUserNightly();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test UserNightly', sch, licUser); 
        Test.stopTest(); 
    }

    public static testMethod void testPermSetNightly() {
        Test.StartTest();
        Insert testCreateNewUser();
        licenseTrackerPermSetNightly licPermSet = new licenseTrackerPermSetNightly();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test PermSetNightly', sch, licPermSet); 
        Test.stopTest(); 
    }    

    public static testMethod void testLicenseTrackerUtilities() {
        Test.StartTest();
        licenseTrackerUtilities ltu = new licenseTrackerUtilities();
        map<Id,PermissionSet> psMap = new map<Id, PermissionSet>([SELECT Id, Name FROM PermissionSet LIMIT 10]);
        List<String> permSetNames = ltu.getPermSetNamesByIdSet(psMap.keySet()).split(';',15);
        System.assertEquals(10, permSetNames.size());
        System.assertEquals('View Only', ltu.getLicenseTypeByProfile('LIMITED Jacobs Security'));
        System.assertEquals('Limited', ltu.getLicenseTypeByProfile('Limited User With Financials'));
        System.assertEquals('Full', ltu.getLicenseTypeByProfile(ltu.getProfileNameById(UserInfo.getProfileId())));
        Test.stopTest(); 
    }   
    
    public static testMethod User testCreateNewUser(){
        User tu1 = new User(ProfileId = UserInfo.getProfileId(),
                          LastName = 'TestLN1',
                          Email = 'tu1@example.com',
                          Username = 'tu1@licensetracker.example.com' + System.currentTimeMillis(),
                          Alias = 'tu1alias',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          IsActive = TRUE,
                          //Line_of_Business__c ='Buildings & Infrastructure',
                          //Business_Unit__c = 'B&I-Americas',
                          //Selling_Unit__c = 'B&I - Americas Central',
                          UserRoleId = UserInfo.getUserRoleId());    
        return tu1;        
    }
}