/**************************************************************************************
Name: recordtypeControllerTest
Version: 
Created Date: 
Function: Test class for recordtypeController

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    7/25/2017        Uses a custom setting to easily maintain. DE165,DE164, US718
* Jignesh Suvarna	9/7/2018		Updated hardcodded Record Type Name "Success Story" with "Success Stories" to fix test failures
*************************************************************************************/
@isTest
private class recordtypeControllerTest{

    static testMethod void test_recordtype() {

       Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       Map <String,Schema.RecordTypeInfo> recordTypeMapProj = Jacobs_Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       
       Jacobs_Project__c proj = new Jacobs_Project__c();
       proj.Name = 'testProj';
       proj.Project_Start_Date__c = System.today();
       insert proj;
       
       system.debug('proj.id=' + proj.Id);
       Library__c lib = new Library__c();
       lib.Name = 'testLib';
       lib.cb_active__c = true;
       lib.dt_submittal_date__c = System.today();
       lib.lr_project__c = proj.Id;
       if(recordTypeMap.containsKey('Other')) 
       {
           lib.RecordTypeId= recordTypeMap.get('Other').getRecordTypeId();
       }
       insert lib;
       
       system.debug('library.id='+lib.Id);
       ApexPages.currentPage().getParameters().put('Id', proj.Id);
       List<String> l = recordtypeController.fetchRecordTypeValues(proj.Id, proj.getSObjectType().getDescribe().getName());
       
       //A&T Program Test
       Jacobs_Project__c projAT = new Jacobs_Project__c();
       projAT.Name = 'testProjAT';
       projAT.Project_Start_Date__c = System.today();
       if(recordTypeMapProj.containsKey('ATEN Program')) 
       {
           projAT.RecordTypeId= recordTypeMapProj.get('ATEN Program').getRecordTypeId();
       }
       insert projAT;
       
       system.debug('projAT.id=' + projAT.Id);
       Library__c libAT = new Library__c();
       libAT.Name = 'testLibAT';
       libAT.cb_active__c = true;
       libAT.dt_submittal_date__c = System.today();
       libAT.lr_project__c = projAT.Id;
       if(recordTypeMap.containsKey('Awards')) 
       {
           libAT.RecordTypeId= recordTypeMap.get('Awards').getRecordTypeId();
       }
       insert libAT;
        
       LDT_Lightning_Filter__c setting = new LDT_Lightning_Filter__c();
       setting.Name = 'Test Setting';
       setting.Label_del__c = 'Awards';
       setting.Object_API__c = 'Jacobs_Project__c';
       setting.OBJ_RecordType_API_Name__c = 'A_T_Program';
       setting.Coverage__c = 'Include';
       insert setting;
        
       List<String> at = recordtypeController.fetchRecordTypeValues(projAT.Id, projAT.getSObjectType().getDescribe().getName());
           
       //Task Order Test
       Jacobs_Project__c projTO = new Jacobs_Project__c();
       projTO.Name = 'testProjTO';
       projTO.Project_Start_Date__c = System.today();
       if(recordTypeMapProj.containsKey('Task Orders')) 
       {
           projTO.RecordTypeId= recordTypeMapProj.get('Task Orders').getRecordTypeId();
       }    
       insert projTO;
       
       system.debug('projTO.id =' + projTO.Id);
       Library__c libTO = new Library__c();
       libTO.Name = 'testLibTO';
       libTO.cb_active__c = true;
       libTO.dt_submittal_date__c = System.today();
       libTO.lr_project__c = projTO.Id;
       if(recordTypeMap.containsKey('PWS')) 
       {
           libTO.RecordTypeId= recordTypeMap.get('PWS').getRecordTypeId();
       }
       insert libTO;
       
       system.debug('libTO.id = '+libTO.Id);
       ApexPages.currentPage().getParameters().put('Id', projTO.Id);
          
       LDT_Lightning_Filter__c TOsetting = new LDT_Lightning_Filter__c();
       TOsetting.Name = 'Test Setting';
       TOsetting.Label_del__c = 'PWS';
       TOsetting.Object_API__c = 'Jacobs_Project__c';
       TOsetting.OBJ_RecordType_API_Name__c = 'task_orders';
       TOsetting.Coverage__c = 'Include';
       TOsetting.Active__c = true;
       insert TOsetting;
           
       system.debug('tosetting.name = ' + TOsetting.name);
       List<String> m = recordtypeController.fetchRecordTypeValues(projTO.Id, projTO.getSObjectType().getDescribe().getName());
       system.debug('projTO.id = ' + projTO.Id );
       system.debug('recordtype = ' + projTO.getSObjectType().getDescribe().getName());
       system.debug('m' + m);
       system.debug('m.size = ' + m.size());
           
       //Group Test
       AT_Group__c grp = new AT_Group__c();
       grp.Name = 'testGrp';
       insert grp;
       
       system.debug('grp.id=' + grp.Id);
       Library__c libgrp = new Library__c();
       libgrp.Name = 'testLibgrp';
       libgrp.cb_active__c = true;
       libgrp.dt_submittal_date__c = System.today();
       libgrp.lu_group__c = grp.Id;
       if(recordTypeMap.containsKey('Success Stories')) 
       {
           libgrp.RecordTypeId= recordTypeMap.get('Success Stories').getRecordTypeId();
       }
       insert libgrp;
       
       system.debug('libgrp.id='+libgrp.Id);
       ApexPages.currentPage().getParameters().put('Id', grp.Id);
       
       LDT_Lightning_Filter__c grpsetting = new LDT_Lightning_Filter__c();
       grpsetting.Name = 'Test Setting';
       grpsetting.Label_del__c = 'Success Stories';
       grpsetting.Object_API__c = 'AT_Group__c';
       grpsetting.OBJ_RecordType_API_Name__c = 'None';
       grpsetting.Coverage__c = 'Include';
       grpsetting.Active__c = true;
       
       insert grpsetting;
           
       List<String> n = recordtypeController.fetchRecordTypeValues(grp.Id,grp.getSObjectType().getDescribe().getName());
           
       Test.StartTest();
           system.assert(l.size() > 0);
           system.assertequals(TOsetting.name, 'Test Setting');
           system.assert(n.size() > 0);
           system.assert(at.size() > 0);
           system.assertequals(libgrp.RecordTypeId, recordtypeController.getRecTypeId('Success Stories'));
       Test.StopTest();
       }
}