/**************************************************************************************
Name: BP_Request_TriggerHandler
Version: 1.1
Created Date: 03/15/2017
Function: Handler for BP_Request_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      03/15/2017      Original Version
* Pradeep Shetty    08/15/2017      US772 - Update B&P process
* Manuel Johnson      08/16/2017          Updates per US768 and US780
* Pradeep Shetty    08/21/2017      US902: Added Cross Charge logic
* Madhu Shetty      08/22/2017      (US871) - code for proposal location commented 
* Raj Vakati        08/22/2017      (US769) - code for workhours
* Manuel Johnson      08/30/2017          US765 - Removed validation that BP OU matches Account Location OU
* Pradeep Shetty    09/08/2017      US715 - YTD Project values 
* Pradeep Shetty    01/09/2018      US1435 - Changed A_T_B_P__C field name to Manual_Approval__c
* Pradeep Shetty    04/25/2018      US1828: Create a CH2M Project when the Project number is manually added to the B&P
* Jignesh Suvarna   05/17/2018      US2080 - added validation checks Do Not Create Oracle Account field in verifyIfAccountIsVerifyToSubmitRequest()
* Pradeep Shetty    07/20/2018      DE580 - Check for Account and Opportunity Account combination
* Scott Walker      01/15/2019      STY-00237 - Updated CH2M methods to leverage project creation for manual BP approval.  Removed Oracle sync check on approval due to Account manual workaround
*************************************************************************************/
public class BP_Request_TriggerHandler {
    
    public final static String VERIFIED_ACCOUNT_STATUS = 'Verified';
    public final static String UNERIFIED_ACCOUNT_STATUS = 'Unverified';
    public final static String OPPORTUNITY_BP_REQUEST_TYPE = 'Opportunity B&P';
    public final static String CLIENT_BP_REQUEST_TYPE = 'Client B&P';
    public final static String SALES_UNIT_BP_REQUEST_TYPE = 'Sales PU B&P';
    public final static String REVISION_BP_RECORD_TYPE = 'B_P_Revision';
    //public final static String B_P_PROJECT_RECORD_TYPE = 'B_P_Project'; //[PS 09/08/2017 US715] Commented out this variable. It's not needed. Removing the BP Project record type
    public final static String BP_OBJECT_NAME = B_P_Request__c.sObjectType.getDescribe().getName();
    public final static String PROJECT_OBJECT_NAME = Jacobs_Project__c.sObjectType.getDescribe().getName();

    //PS 08/31/2017: START US772 Added status constants for Active, Closed and Pending Closed
    public final static String STATUS_APPROVED = 'Approved';                    //Approved status
    public final static String STATUS_ACTIVE = 'Active';                        //Active status
    public final static String STATUS_CLOSED = 'Closed';                        //Closed Status
    public final static String STATUS_PENDING_CLOSE = 'Pending Close';          //Pending Close Status
    public final static String REVISION_BP_REQUEST_TYPE = 'Revision';           //Revision Request Type
    public final static String STATUS_DRAFT = 'Draft';                          //Draft status
    public final static String STATUS_SUBMITTED = 'Submitted';                  //Submitted status
    //PS 08/31/2017: END US772 Added status constants for Closed and Pending Closed
    
    //[PS 09/08/2017 US715] Added a variable for Finance Project record type
    public final static String FIN_PROJECT_RECORD_TYPE_NAME = 'Finance';
    public final static String B_P_PROJECT_TYPE_NAME        = 'BID/PROPOSAL';
    public final static String CH2M = 'CH2M';

    private static RecordType finProjectRecordType = getRecordType(PROJECT_OBJECT_NAME, FIN_PROJECT_RECORD_TYPE_NAME);

    //[PS 09/08/2017 US715] Commented out this variable. It's not needed. Removing the BP Project record type
    private static RecordType bPProjectRecordType;

    // Get a map of the record type developer names
    private static Map<Id, RecordType> recordTypes = new Map<Id, RecordType>([SELECT Id, 
                                                                                    DeveloperName 
                                                                              FROM RecordType 
                                                                              WHERE SobjectType = :BP_OBJECT_NAME]);
    
    @TestVisible
    private List<String> folderPaths;
    
    private SharePoint_Service sharePointServiceservice = new SharePoint_Service();
    private BP_Request_TriggerService service = new BP_Request_TriggerService();
    
    private static BP_Request_TriggerHandler handler;

 
  /*
    * Singleton like pattern
    */
    public static BP_Request_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new BP_Request_TriggerHandler();
        }
        return handler;
    }
    
  /*
    * Actions performed on before B_P_Request__c is inserted
    */
    public void onBeforeInsert(List<B_P_Request__c> newBPRequests) {
        //Set the request type
        setRequestType(newBPRequests);
    }
    
  /*
  * Actions performed on before B_P_Request__c is updated
  */
    public void onBeforeUpdate(List<B_P_Request__c> newBPRequests, Map<Id, B_P_Request__c> oldBPMap) {
        //Set the request type
        setRequestType(newBPRequests);

        //updateApproversWhenBudgetChanged(newRequests, oldBPRequestsByIds);
        verifyIfAccountIsVerifyToSubmitRequest(newBPRequests);        

        //Update the Original B&P for the Approved revision
        updateBPOnApproval(newBPRequests, oldBPMap);     

        //Set B&P Status to Active for CH2M B&P when project number is added
        setCH2MBPActive(newBPRequests, oldBPMap);

    }  

  /*
    * Actions performed on after B_P_Request__c being inserterd
    */
    public void onAfterInsert(List<B_P_Request__c> newBPRequests) {
        //updateApproversForNewRequests(newPRequests);
        createProject(newBPRequests, null, true, false);
    }
    
  /*
    * Actions performed on after B_P_Request__c being updated
    */
    public void onAfterUpdate(List<B_P_Request__c> newRequests, Map<Id, B_P_Request__c> oldBPRequestsByIds) {
        //Manuel: START US765 - Remove the OU validation
        //verifyOperatingUnitIsOneOfAccountLocations(newRequests);
        //Manuel: END US765 - Remove the OU validation
        createProject(newRequests, oldBPRequestsByIds, false, true);

        //US1828: Create a CH2M Project when the Project number is manually added to the B&P
        createCH2MFinanceProject(newRequests, oldBPRequestsByIds);   
        
    }

  /*
  * Getter for object record type
  */
  private static RecordType getRecordType(String objectName, String recordTypeDevName){

    return [SELECT Id FROM RecordType WHERE SobjectType = :objectName AND DeveloperName = :recordTypeDevName];
    
  }    
 
  //[PS 09/08/2017 US715] Replacing this method with the above generic method
  /*
    * Getter for object record type
    */
  /*  public RecordType getBPProjectRecordType() {
        if (bPProjectRecordType == null) {
            bPProjectRecordType = [
                SELECT Id
                FROM Recordtype
                WHERE SobjectType = :PROJECT_OBJECT_NAME AND DeveloperName = :B_P_PROJECT_RECORD_TYPE
                LIMIT 1
            ];
        }
        return bPProjectRecordType;
    } */
    
  /*
    * Method to create Project record on B&P Approval
    */
    private void createProject(List<B_P_Request__c> trgNew, Map<Id, B_P_Request__c> trgOldMap, Boolean isInsert, Boolean isUpdate) {
        
        Set<Id> BpIds = new Set<Id>();
        Set<Id> BpWithProjectIds = new Set<Id>();
        // Set<Id> BpWithNoProjectIds = new Set<Id>();

        for (B_P_Request__c bp: trgNew) {      
            //PS 08/15/2017 : US772 Changed from 'Closed-Approved' to 'Approved'
            //PS 08/31/2017: START US772 Added Closed and Pending Closed status to condition
            //Project needs to be created/updated only in the following status scenarios
            if (bp.Status__c == STATUS_APPROVED || 
                bp.Status__c == STATUS_ACTIVE || 
                bp.Status__c == STATUS_PENDING_CLOSE || 
                bp.Status__c == STATUS_CLOSED) {
                BpIds.add(bp.id);
            }
            //PS 08/31/2017: END US772 Added Closed and Pending Closed status to condition
        }
        
        if (BpIds != null && BpIds.size() > 0) 
        { 
          //List of projects to update when a B&P budget or status is updated
          List<Jacobs_Project__c> projectUpdateList = new List<Jacobs_Project__c>();

          //Get the currency exchange rates from the custom setting
          Map<String, Currency_Exchange_Rates__c> exchangeRatesByCodeAndFiscalYear = Currency_Exchange_Rates__c.getAll();          

          //Get all parent projects for the updated B&P
          for (Jacobs_Project__c project: [Select Id, 
                                                  B_P_Budget_Local__c, 
                                                  B_P_Request__r.BP_Budget__c, 
                                                  B_P_Request__r.B_P_Budget_Local_Currency__c,
                                                  B_P_Request__c,
                                                  nu_workhours__c,
                                                  B_P_Request__r.Work_Hours__c 
                                           From Jacobs_Project__c 
                                           Where B_P_Request__c In :BpIds 
                                           And RecordTypeId = :finProjectRecordType.Id]) 
          {
            //Update the Project budget only if the B&P budget has changed
            if(project.B_P_Budget_Local__c != project.B_P_Request__r.B_P_Budget_Local_Currency__c || 
               project.nu_workhours__c != project.B_P_Request__r.Work_Hours__c)
            {
                project.B_P_Budget_Local__c     = project.B_P_Request__r.B_P_Budget_Local_Currency__c;

                //Update the corporate currency value 
                String exchangeRateKey = project.Local_Currency__c +'-'+ project.Fiscal_Year__c;
                Decimal exchangeRate = 1;
                
                if(exchangeRatesByCodeAndFiscalYear.containsKey(exchangeRateKey)){
                  exchangeRate = exchangeRatesByCodeAndFiscalYear.get(exchangeRateKey).Conversion_Rate__c;
                }                

                Decimal bpBudgetLocal = project.B_P_Request__r.B_P_Budget_Local_Currency__c != null ? project.B_P_Request__r.B_P_Budget_Local_Currency__c : 0;

                project.B_P_Budget__c = bpBudgetLocal * exchangeRate;

                //Update work hours if the work hours have been updated
                project.nu_workhours__c   = project.B_P_Request__r.Work_Hours__c;

                projectUpdateList.add(project);
                BpWithProjectIds.add(project.B_P_Request__c);
            }

          } //END FOR
          
          //Update the Project budget and commit it to database
          if (projectUpdateList != null && projectUpdateList.size() > 0) {
              update projectUpdateList;
          }
        }
        
        BpIds.removeAll(BpWithProjectIds);
        
        List<B_P_Request__c> approvedRequests = new List<B_P_Request__c>();
        List<Jacobs_Project__c> insertList = new List<Jacobs_Project__c>();
        //List<Id> clientIds = new List<Id>();
        //List<Id> oppIds = new List<Id>();

        for (B_P_Request__c bp : trgNew) {
            //PS 08/15/2017 : US772 Changed from 'Closed-Approved' to 'Approved'            
            if (bp.Status__c == STATUS_APPROVED 
                && (isInsert || bp.Status__c != trgOldMap.get(bp.id).Status__c) 
                //&& !atUnitsSet.contains(bp.Sales_B_P_Unit__c) 
                && !bp.Manual_Approval__c
                && BpIds.contains(bp.id) 
                && bp.Request_Type__c!= REVISION_BP_REQUEST_TYPE) {
                approvedRequests.add(bp);
                /*clientIds.add(bp.Client__c);
                clientIds.add(bp.Client__c);
                if (bp.Request_Type__c == OPPORTUNITY_BP_REQUEST_TYPE) {
                    oppIds.add(bp.Opportunity__c);
                }*/
            }
        }
        
        // Map<Id,Account> accountDataMap = new Map<Id,Account>([SELECT Id, OracleAccountID__c, Name FROM Account WHERE Id IN :clientIds]);
        Map<Id, B_P_Request__c> bpDataMap = new Map<Id, B_P_Request__c>([ SELECT Id,
                                                                                Client__r.OracleAccountID__c, 
                                                                                Client__r.Name,
                                                                                Project_Accountant__r.Jacobs_Employee_ID__c,
                                                                                Project_Manager__r.Jacobs_Employee_ID__c,
                                                                                Jacobs_Sales_Lead__r.Jacobs_Employee_ID__c
                                                                          FROM B_P_Request__c
                                                                          WHERE Id 
                                                                          In:approvedRequests]);
                
        //Set<ID> RTIds = new Set<ID>();
        
        //[PS 09/08/2017 US715] Removing the BP project record type and replacing it with Finance record type
        //Id bPProjectRecordTypeId = getBPProjectRecordType().Id;
        
        for (B_P_Request__c bp : approvedRequests) {
            Jacobs_Project__c proj                     = new Jacobs_Project__c();
            proj.Name                                  = bp.Name + ' - ' + Label.Pending;
            proj.Opportunity__c                        = bp.Opportunity__c;
            //PS 10/27/2017 US1211 Changed the date value from Today to the value entered by the user
            proj.Project_Start_Date__c                 = bp.B_P_Start_Date__c;
            proj.B_P_Request__c                        = bp.id;
            proj.Project_Status__c                     = 'Active';
            proj.Request_Type__c                       = bp.Request_Type__c;
            proj.Operating_Unit__c                     = bp.Operating_Unit__c;
            proj.Legal_Entity__c                       = bp.Legal_Entity__c;
            proj.Task_Validation__c                    = bp.Task_Validation__c;
            proj.Market_Sector__c                      = bp.Market_Sector__c;
            
            //Start: US871 Commented for removing proposal location field
            //proj.Proposal_Location__c                  = bp.Proposal_Location__c;
            //End: US871 Commented for removing proposal location field

            proj.Area_of_Business__c                   = 'F'; //We will always be sending F-Proposal for project creation
            proj.Controlling_Performance_Unit__c       = bp.Sales_B_P_Unit__c;
            proj.B_P_Budget_Local__c                   = bp.B_P_Budget_Local_Currency__c;
            proj.B_P_Budget__c                         = bp.BP_Budget__c;
            proj.Local_Currency__c                     = bp.Currency__c;
            //proj.RecordTypeId                          = bPProjectRecordTypeId;
            proj.RecordTypeId                          = finProjectRecordType.Id; //[PS 09/08/2017 US715] Removing the BP project record type and replacing it with Finance record type
            proj.lr_client__c                          = bpDataMap.get(bp.id).Client__c;
            proj.B_P_Request_Created_Date__c           = Date.newInstance(bp.CreatedDate.year(), bp.CreatedDate.month(), bp.CreatedDate.day());
            proj.Project_Accountant_Employee_Number__c = bpDataMap.get(bp.id).Project_Accountant__r.Jacobs_Employee_ID__c;
            proj.Project_Manager_Employee_Number__c    = bpDataMap.get(bp.id).Project_Manager__r.Jacobs_Employee_ID__c;
            proj.Jacobs_Sales_Lead_Employee_Number__c  = bpDataMap.get(bp.id).Jacobs_Sales_Lead__r.Jacobs_Employee_ID__c;
            proj.Jacobs_Sales_Lead__c                  = bp.Jacobs_Sales_Lead__c;
            proj.Project_Manager__c                    = bp.Project_Manager__c;
            proj.Project_Accountant__c                 = bp.Project_Accountant__c;
            proj.Project_Type__c                       = B_P_PROJECT_TYPE_NAME;
            
            //Added Project Description field based on the discussion during SIT
            proj.txt_description__c                    = bp.Description__c;

            // Added by Raj -US 769 
            proj.nu_workhours__c                       = bp.Work_Hours__c ;

            //START: PS 08/21/2017: US902 Add Cross Charge in the integration
            proj.Cross_Charge__c                       = bp.Cross_Charge__c;
            proj.Office_Receiving_Cross_Charge__c      = bp.Office_Receiving_Cross_Charge__c;
            //END: PS 08/21/2017: US902 Add Cross Charge in the integration            

            insertList.add(proj);

        }
        
        insert insertList;
    }
    
  /*
  *   Requests can be submitted only when account is verified
  */
    private void verifyIfAccountIsVerifyToSubmitRequest(List<B_P_Request__c> requests) {
        for (B_P_Request__c request : requests) {
            // JS 17/05/2018 :US2080 - Added a condition to prevent B&P submission when users try to submit a B&P with Account
            // that is verified, but has been marked as 'Do Not Create Oracle Account'
            //PS 07/20/2018: DE580 - Adding a check to see the Account and Opportunity Account match
            //Start DE580
            //Run this block of code only if this is an update as part of approval process. 
            //Account_Verification_Check__c is set to true only as part of initial submission action of the approval process
            if(request.Account_Verification_Check__c == true){
              //Initialise error message
              String errorMessage = 'Please address following errors before submitting the B&P: ';

              //Indicates that Account does not match
              Boolean accountDoesNotMatch = false;
              //Indicates that Account in not verified
              Boolean accountNotVerified  = false;
              //Indicates that salesB&PUnit is empty
              Boolean salesBPUnitEmpty    = false;              
              //This condition checks for the correct Account and Opp combination
              //It does this for Opp request type and if the Project code is blank. 
              //As an additional precaution we also check fot status
              //Note that this condition is also checked as a validation rule. we are doing this in trigger as well to catch 
              //this condition when the B&P is submitted for approval. In such case the validation rule is triggered after 
              //Before trigger is executed and the error message is not user friendly.
              if(request.Request_Type__c == OPPORTUNITY_BP_REQUEST_TYPE && 
                 String.isBlank(request.Project_Code__c) && 
                 (request.Status__c == STATUS_DRAFT || request.Status__c == STATUS_SUBMITTED || request.Status__c == STATUS_APPROVED) &&
                 request.Client__c != request.Opportunity_Account__c){
                accountDoesNotMatch = true;
              }
              //End DE580
              //If the Opportunity and Account combination is correct, then we check if the Account is verified. 
              //This condition is also used for Account B&P
              //STY0037-change the else if block below for ECR.  Due to manual account workaround, do not sync to Oracle flag will always 
              //be set to true therefore the Oracle account creation check must be removed. 
              /*else if ((request.Request_Type__c == OPPORTUNITY_BP_REQUEST_TYPE ||
                   request.Request_Type__c == CLIENT_BP_REQUEST_TYPE) &&
                  request.Client__c != null &&
                  (request.Account_Status__c != VERIFIED_ACCOUNT_STATUS || 
                   (request.Account_Status__c == VERIFIED_ACCOUNT_STATUS && request.Oracle_Account_Creation__c))) 
              */                                         
              else if ((request.Request_Type__c == OPPORTUNITY_BP_REQUEST_TYPE ||
                   request.Request_Type__c == CLIENT_BP_REQUEST_TYPE) &&
                  request.Client__c != null &&
                  request.Account_Status__c != VERIFIED_ACCOUNT_STATUS)                                  
              {
                  accountNotVerified = true;
              }
              //Start DE560
              //Here we check if the Sales B&P Unit is populated. Sometimes the Sales B&P Unit may be blank due to 
              //changes in Selling Unit dependencies. This ensures that this condition is checked during submission.
              //Note that this condition is also checked as a validation rule. we are doing this in trigger as well to catch 
              //this condition when the B&P is submitted for approval. In such case the validation rule is triggered after 
              //Before trigger is executed and the error message is not user friendly.
              //Only applies to non-CH2M B&Ps
              if(!recordTypes.get(request.RecordTypeId).DeveloperName.contains(CH2M) && 
                 String.isNotBlank(request.Selling_Unit__c) && 
                 String.isBlank(request.Sales_B_P_Unit__c)){
                salesBPUnitEmpty = true; 
              }
              //End DE560

              if(accountDoesNotMatch || accountNotVerified || salesBPUnitEmpty){
                if(accountDoesNotMatch){
                  errorMessage = errorMessage + Label.AccountDoesNotMatch + ' ';
                }
                else if(accountNotVerified){
                  errorMessage = errorMessage + Label.AccountNotVerified + ' ';
                } 
                if(salesBPUnitEmpty){
                  errorMessage = errorMessage + Label.SalesBPUnitRequired + ' ';
                } 

                request.addError(errorMessage); 
                //throw new AuraHandledException(errorMessage);             
              }
            }
        }
    }

  /*
  * Method to set the Request Type based on the Client, Opportunity and Sales B&P Unit
  */
    private void setRequestType(List<B_P_Request__c> bpRequests){

      for(B_P_Request__c bp : bpRequests){
          
        // Should only update the Request Type if it is a New B&P Request
        if(!recordTypes.get(bp.RecordTypeId).DeveloperName.contains(REVISION_BP_RECORD_TYPE)){
          if(!String.isEmpty(bp.Sales_B_P_Unit__c) && String.isEmpty(bp.Opportunity__c) && String.isEmpty(bp.Client__c)){
              bp.Request_Type__c = SALES_UNIT_BP_REQUEST_TYPE; // Set to Sales PU B&P if the Sales B&P Unit is populated
          } else if(!String.isEmpty(bp.Opportunity__c)){
              bp.Request_Type__c = OPPORTUNITY_BP_REQUEST_TYPE; // Set to Opportunity B&P if the Opportunity is populated
          } else if(!String.isEmpty(bp.Client__c)){
              bp.Request_Type__c = CLIENT_BP_REQUEST_TYPE; // Set to Client B&P if the Client is populated
          }
        }
      } //END FOR
    }

  /*
  * Method for updating Original B&P when B&P revision is approved
  */ 
  private void updateBPOnApproval(List<B_P_Request__c> newBPRequests, Map<Id, B_P_Request__c> oldBPMap){  

    Map<Id, B_P_Request__c> originalBPAdditionalBPMap = new Map<Id, B_P_Request__c>();

    //If this is an approved revision then update the B&P Budget and work hours of the Original B&P and capture approval level and amount at approval
    for(B_P_Request__c bp: newBPRequests){

      if(bp.Status__c == STATUS_APPROVED){
        if(bp.Approval_Level_Amount__c >2000000){
          bp.Approval_Level_At_B_P_Approval__c = 'LEVEL 1';
        }
        else if(bp.Approval_Level_Amount__c > 500000){
          bp.Approval_Level_At_B_P_Approval__c = 'LEVEL 2';
        }
        else if(bp.Approval_Level_Amount__c > 25000){
          bp.Approval_Level_At_B_P_Approval__c = 'LEVEL 3';
        }
        else{
          bp.Approval_Level_At_B_P_Approval__c = 'LEVEL 4';
        }
      }

        if(recordTypes.get(bp.RecordTypeId).DeveloperName.contains(REVISION_BP_RECORD_TYPE) && 
           bp.Status__c == STATUS_APPROVED && 
           bp.Status__c != oldBPMap.get(bp.Id).Status__c){
               originalBPAdditionalBPMap.put(bp.Original_B_P__c, bp);
           }


    } //END FOR

    //Get the Original B&P Objects
    List<B_P_Request__c> originalBPList = [Select Id, 
                                                  B_P_Budget_Local_Currency__c,
                                                  Work_Hours__c 
                                           From B_P_Request__c 
                                           Where Id 
                                           In :originalBPAdditionalBPMap.keySet()];

    for(B_P_Request__c originalBP: originalBPList){
      originalBP.B_P_Budget_Local_Currency__c      = originalBP.B_P_Budget_Local_Currency__c + originalBPAdditionalBPMap.get(originalBP.Id).B_P_Budget_Local_Currency__c;
      originalBP.Work_Hours__c                     = originalBP.Work_Hours__c + originalBPAdditionalBPMap.get(originalBP.Id).Work_Hours__c;
      originalBP.Approval_Level_At_B_P_Approval__c = originalBPAdditionalBPMap.get(originalBP.Id).Approval_Level_At_B_P_Approval__c;
    } //END FOR

    try{
      update originalBPList;          
    }
    catch(DmlException exc){
      throw new BPTriggerException(Label.Original_BP_Update_Error);
    }  
  }

  //US1828: Create a CH2M Project when the Project number is manually added to the B&P
  //STY-00237:  Added the Manual Approved BP request for ECR divest
  private void createCH2MFinanceProject(List<B_P_Request__c> trgNew, Map<Id, B_P_Request__c> trgOldMap){

    //List of Projects to be inserted
    List<Jacobs_Project__c> ch2mProjects = new List<Jacobs_Project__c>();

    for (B_P_Request__c bp: trgNew) {      
        if (bp.Status__c == STATUS_ACTIVE && 
            (bp.RecordTypeId == getRecordType('B_P_Request__c', 'CH2M_Approved_B_P_Request').Id ||
            bp.RecordTypeId == getRecordType('B_P_Request__c', 'Manual_Approved_B_P_Request').Id) && 
            String.isNotBlank(bp.Project_Code__c) &&
            bp.Project_Code__c != trgOldMap.get(bp.Id).Project_Code__c) {

          //Create a new Project record
          Jacobs_Project__c proj                     = new Jacobs_Project__c();
          proj.B_P_Budget__c                         = bp.BP_Budget__c;
          proj.B_P_Request__c                        = bp.id;
          proj.B_P_Budget_Local__c                   = bp.B_P_Budget_Local_Currency__c;
          proj.Local_Currency__c                     = bp.Currency__c;
          proj.Name                                  = bp.Project_Code__c;
          proj.Opportunity__c                        = bp.Opportunity__c;
          proj.Project_Code__c                       = bp.Project_Code__c;
          //Add check for CH2M Approved else manual BP
            if (bp.RecordTypeId == getRecordType('B_P_Request__c', 'CH2M_Approved_B_P_Request').Id) {
                    proj.Project_ID__c               = 'CH' + bp.Project_Code__c;}
            else{
                    proj.Project_ID__c               = bp.Project_ID__c;
                }
          
          proj.Project_Manager__c                    = bp.Project_Manager__c;
          proj.Project_Status__c                     = STATUS_ACTIVE;
          proj.Project_Type__c                       = B_P_PROJECT_TYPE_NAME;
          proj.RecordTypeId                          = finProjectRecordType.Id;
          proj.Request_Type__c                       = bp.Request_Type__c;
          proj.nu_workhours__c                       = bp.Work_Hours__c ;

          ch2mProjects.add(proj);

        }
        //PS 08/31/2017: END US772 Added Closed and Pending Closed status to condition
    }

    insert ch2mProjects;

  }

    //STY-00237:  Added the Manual Approved BP request for ECR divest
  private void setCH2MBPActive(List<B_P_Request__c> trgNew, Map<Id, B_P_Request__c> trgOldMap){
    for (B_P_Request__c bp: trgNew) {      
      if (bp.Status__c == STATUS_APPROVED && 
          (bp.RecordTypeId == getRecordType('B_P_Request__c', 'CH2M_Approved_B_P_Request').Id ||
            bp.RecordTypeId == getRecordType('B_P_Request__c', 'Manual_Approved_B_P_Request').Id) && 
          String.isNotBlank(bp.Project_Code__c) &&
          bp.Project_Code__c != trgOldMap.get(bp.Id).Project_Code__c) {

        bp.Status__c = STATUS_ACTIVE;
      }
    }
  }

  public class BPTriggerException extends Exception {}  

}