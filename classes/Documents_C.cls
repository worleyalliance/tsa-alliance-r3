/**************************************************************************************
Name: Documents_C
Version: 1.0 
Created Date: 24.02.2017
Function: Controller class for Documents Lightning Component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/

public with sharing class Documents_C {

   /*
    * Retrieves files placed in given folder in SharePoint
    */
/*    @AuraEnabled
    public static List<SharePointConfiguration.File> retrieveDocuments(String folderPath, String recordId, String objectType) {
        SharePoint_Service service = new SharePoint_Service();
        folderPath = service.preprareRootPaths(new List<Id>{recordId}, objectType).get(0) + '/'+ folderPath;
        SharePointClient client = new SharePointClient();
        List<SharePointConfiguration.File> files;
        try {
            files = client.retrieveFolderFiles(folderPath);
            files = service.updateFileLink(files);
        } catch (SharePointClient.SharpointCommunicationException ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return files;
    }*/

   /*
    * Uploads file to give folder in SharePoint
    */
/*
    @AuraEnabled
    public static Integer uploadDocument(
            String fileName, String base64Data, String folderPath,
            String recordId, String objectType,
            String guid,
            Decimal offset,
            Boolean isFirstChunk,
            Boolean isSingleChunk,
            Boolean isLastChunk) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        NamedCredential namedCredential = [SELECT Endpoint FROM NamedCredential WHERE DeveloperName = :SharePointConfiguration.NAMED_CREDENTIAL_DEV_NAME];
        SharePointClient client = new SharePointClient();
        SharePoint_Service service = new SharePoint_Service();
        folderPath = service.preprareRootPaths(new List<Id>{recordId}, objectType).get(0) + '/'+ folderPath;
        Integer responseOffset;
        try {
            responseOffset =
                    client.uploadDocument(
                            fileName,
                            EncodingUtil.base64Decode(base64Data),
                            folderPath,
                            namedCredential.Endpoint,
                            guid,
                            offset,
                            isFirstChunk,
                            isSingleChunk,
                            isLastChunk);
        } catch (SharePointClient.SharpointCommunicationException ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return responseOffset;
    }

   */
/*
    * Deletes file from given folder in SharePoint
    *//*

    @AuraEnabled
    public static void deleteDocument(String fileName, String folderPath, String recordId, String objectType) {
        SharePointClient client = new SharePointClient();
        SharePoint_Service service = new SharePoint_Service();
        folderPath = service.preprareRootPaths(new List<Id>{recordId}, objectType).get(0) + '/'+ folderPath;
        try {
            client.deleteDocument(fileName, folderPath);
        } catch (SharePointClient.SharpointCommunicationException ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
*/

   /*
    * Each record based folder structure is compsed with record Name and Id
    * Given method prepares root path for given record
    */
/*    private static String composeRecordRootPath(String recordId, String objectType){
        if(recordId == null || objectType == null){
            throw new AuraHandledException(Label.FolderNotFound);
        }
        String fields = objectType == 'Opportunity' ? ',AccountId, Account.Name' : '';

        String query = 'SELECT Id, Name'+fields+' FROM '+objectType+' WHERE Id=\''+recordId+'\'';
        List<sObject> result = Database.query(query);
        if(result.isEmpty()){
            throw new AuraHandledException(Label.FolderNotFound);
        }
        String rootPath = '';
        if(objectType == 'Opportunity'){
            Opportunity opp = (Opportunity) result[0];
            rootPath = opp.Account.Name +'('+opp.AccountId+')/Opportunities/';
        }
        rootPath += ((String)result[0].get('Name')) +'('+result[0].Id+')/';

        return rootPath;
    }*/

}