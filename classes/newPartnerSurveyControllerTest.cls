/**************************************************************************************
Name: newPartnerSurveyControllerTest
Version:
Created Date: 8/4/2017
Function: Test class for newPartnerSurveyController

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo        8/4/2017    Test Class for newPartnerSurveyController
*************************************************************************************/
@isTest
private class newPartnerSurveyControllerTest{

    static testMethod void test_newPartnerSurveyController() {
        
        Integer IntTest;
        
        Partner_Survey__c partnerSurvey = new Partner_Survey__c();
        
        insert partnerSurvey;
        
        partnerSurvey.us_cb_billing_approved__c = True;
        
        update partnerSurvey;
        
        Partner_Survey__c a = new Partner_Survey__c();
        
        insert a;
        
        a.us_cb_accounting_approved__c   = True;
        a.us_cb_annual_incurred_cost__c = True;
        a.us_cb_billing_approved__c = True;
        a.us_dt_accounting_approved__c = System.today();
        a.us_dt_date_of_last_report__c = System.today();
        a.us_dt_billing_approved__c   = System.today();
        
        update a;
        
        
        Test.StartTest();
        //    system.assert(.size() > 0);
            PageReference pageRef = Page.dcaa_cas_surveys;
            Test.setCurrentPage(pageRef);
            
            pageRef.getParameters().put('id',partnerSurvey.id);
            ApexPages.StandardController sc = new ApexPages.standardController(partnerSurvey);
             
            newPartnerSurveyController  getPartnerSurvey = new newPartnerSurveyController(sc);
           
            getPartnerSurvey.save();
            
            getPartnerSurvey.getPartnerSurvey();
            
            pageRef.getParameters().put('id',a.id);
            ApexPages.StandardController sc1 = new ApexPages.standardController(a);
             
            newPartnerSurveyController  getPartnerSurvey1 = new newPartnerSurveyController(sc1);
           
            getPartnerSurvey1.save();
            
            //System.debug('chck = ' + a.us_cb_accounting_approved__c);
            
             try{
                 if(Test.isRunningTest())
                    IntTest = 20/0;
                    System.debug('intest = ' + IntTest);
             }
             catch(Exception ex){
                 ApexPages.addMessages(ex);
                 for (ApexPages.Message msg : ApexPAges.getMessages()){
                 System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
                 }
             }
            

        Test.StopTest();

       }
}