/**************************************************************************************
Name: MultiOfficeSplit_TriggerTest
Version: 1.0 
Created Date: 03.15.2017
Function: To Test MultiOfficeSplit_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04/03/2017      Original Version
* Pradeep Shetty    08/22/2018      US2396: Added a test classes to test date adjustment    
*************************************************************************************/

@IsTest
private class MultiOfficeSplit_TriggerTest {
    
  @IsTest
  private static void shouldSetOnlyOneAsLead() {

    // given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();
    
    TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();

    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c DIGIPU = unitBuilder.build().save().getRecord();
    Unit__c JESAPU = unitBuilder.build().withLOB('JESA').withBU('JESA - Joint Venture').withName('020342 (JESA MS) JT-I2S').save().getRecord();

    String resourceType = Multi_Office_Split__c.Resource_Type__c.getDescribe().getPicklistValues()[0].getValue();

    Multi_Office_Split__c leadMultiOffice1 = new Multi_Office_Split__c( Start_Date__c = Date.today(),
                                                                        Number_of_Months__c = 2,
                                                                        Performance_Unit_PU__c = DIGIPU.Id,
                                                                        Resource_Type__c = resourceType,
                                                                        Lead__c = true,
                                                                        Gross_Margin__c = 200,
                                                                        Revenue__c = 1000,
                                                                        Hours__c = 100,
                                                                        Opportunity__c = opp.Id
                                                                      );
    insert leadMultiOffice1;

    // when
    Multi_Office_Split__c leadMultiOffice2 = new Multi_Office_Split__c( Start_Date__c = Date.today(),
                                                                        Number_of_Months__c = 2,
                                                                        Performance_Unit_PU__c = JESAPU.Id,
                                                                        Resource_Type__c = resourceType,
                                                                        Lead__c = true,
                                                                        Gross_Margin__c = 100,
                                                                        Revenue__c = 1000,
                                                                        Hours__c = 100,
                                                                        Opportunity__c = opp.Id
                                                                      );
    Test.startTest();
    insert leadMultiOffice2;
    Test.stopTest();

    List<Multi_Office_Split__c> result = [SELECT Id FROM Multi_Office_Split__c WHERE Opportunity__c = :opp.Id AND Lead__c = true];
    system.assertEquals(1, result.size());
    system.assertEquals(leadMultiOffice2.Id, result[0].Id);
  }

  @IsTest
  private static void shouldNotAllowToDeleteLead() {
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();

    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c DIGIPU = unitBuilder.build().save().getRecord();

    String resourceType = Multi_Office_Split__c.Resource_Type__c.getDescribe().getPicklistValues()[0].getValue();

    Multi_Office_Split__c ms1 = new Multi_Office_Split__c(Start_Date__c = Date.today(),
                                                          Number_of_Months__c = 2,
                                                          Performance_Unit_PU__c = DIGIPU.Id,
                                                          Resource_Type__c = resourceType,
                                                          Lead__c = true,
                                                          Gross_Margin__c = 100,
                                                          Revenue__c = 1000,
                                                          Hours__c = 100,
                                                          Opportunity__c = opp.Id
                                                          );
    insert ms1;

    //when
    try {
      delete ms1;

      //then
      System.assert(false, 'Should throw an exception');
    } catch (DmlException e) {
      System.assertEquals(Label.MosLeadDeletionError, e.getDmlMessage(0));
    }
  }

  @isTest
  private static void shouldSetExecutingBUandExecutingLOBvaluesOnInsert() {
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

    Decimal revenue = 20;
    TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
    Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withRevenue(revenue).withGM(10).withOpportunity(opp.Id).getRecord();

    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c DIGIPU = unitBuilder.build().save().getRecord();

    //when
    multiOfficeSplit.Performance_Unit_PU__c = DIGIPU.Id;
    insert multiOfficeSplit;

    //then
    List<Multi_Office_Split__c> result =[SELECT Executing_BU__c, 
                                                Executing_LOB__c 
                                         FROM Multi_Office_Split__c 
                                         WHERE Id =:multiOfficeSplit.Id];
    System.assert(String.isNotBlank(result[0].Executing_BU__c));
    System.assert(String.isNotBlank(result[0].Executing_LOB__c));
  }

  @isTest
  private static void shouldSetExecutingBUandExecutingLOBvaluesOnUpdate() {
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

    Decimal revenue = 20;
    TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
    Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withRevenue(revenue).withGM(10).withOpportunity(opp.Id).save().getRecord();

    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c JESAPU = unitBuilder.build().withLOB('JESA').withBU('JESA - Joint Venture').withName('020342 (JESA MS) JT-I2S').save().getRecord();

    //when
    multiOfficeSplit.Performance_Unit_PU__c = JESAPU.Id;
    update multiOfficeSplit;

    //then
    List<Multi_Office_Split__c> result = [SELECT Executing_BU__c, 
                                                 Executing_LOB__c 
                                          FROM Multi_Office_Split__c 
                                          WHERE Id =:multiOfficeSplit.Id];
    System.assert(String.isNotBlank(result[0].Executing_BU__c));
    System.assert(String.isNotBlank(result[0].Executing_LOB__c));
  }

  /*US2396: Test class to check that when the Lead PU date changes, other dates change as well*/
  @isTest
  private static void shouldChangeDateOnLeadPUDateChange() {
    //Prepare data
    //Account
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    //Opportunity
    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

    //Unit
    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c JESAPU = unitBuilder.build().withLOB('JESA').withBU('JESA - Joint Venture').withName('020342 (JESA MS) JT-I2S').save().getRecord();

    //Multi Offices
    List<Multi_Office_Split__c> mosTestData = new List<Multi_Office_Split__c>();
    TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();


    //Start test
    Test.startTest();

    //Multi Office with Auto Adjust = True
    Multi_Office_Split__c mosAutoAdjust = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).getRecord();
    mosAutoAdjust.Auto_Adjust_Dates__c   = true;

    //Add MOS to the list
    mosTestData.add(mosAutoAdjust);

    //Multi Office with Auto Adjust = True
    Multi_Office_Split__c mosNoAutoAdjust = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).getRecord();
    mosAutoAdjust.Auto_Adjust_Dates__c   = false;
     
    //Add MOS to the list  
    mosTestData.add(mosNoAutoAdjust);

    insert mosTestData;

    //Requery Multi Office and ensure that the lag is calculated for MOS with AutoAdjust = true 
    List<Multi_Office_Split__c> afterInsertMOSList = [Select Id, 
                                                         Lag__c,
                                                         Start_Date__c,
                                                         Auto_Adjust_Dates__c 
                                                  From Multi_Office_Split__c 
                                                  Where Opportunity__c = :opp.Id 
                                                  And Lead__c=false];

    //Get the Lead MOS
    Multi_Office_Split__c leadMOS = [Select Id,
                                            Start_Date__c 
                                     From Multi_Office_Split__c 
                                     Where Opportunity__c = :opp.Id 
                                     And Lead__c = true];

    //Update the date
    leadMOS.Start_Date__c = leadMOS.Start_Date__c.addDays(2);

    //Requery Multi Office and ensure that the dates have changed for auto adjust and not for auto adjust = false
    List<Multi_Office_Split__c> afterUpdateMOSList = [Select Id, 
                                                         Lag__c,
                                                         Start_Date__c,
                                                         Auto_Adjust_Dates__c
                                                  From Multi_Office_Split__c 
                                                  Where Opportunity__c = :opp.Id 
                                                  And Lead__c=false];    

    Test.stopTest();

    //Asserts
    //Check the lag before updating Lead MOS date
    for(Multi_Office_Split__c currentMOS: afterInsertMOSList){
      if(currentMOS.Auto_Adjust_Dates__c){
        //Lag should be calculated
        System.assert(String.isNotBlank(currentMOS.Lag__c));
        //Comparing Lag value
        System.assertEquals(String.valueOf(currentMOS.Start_Date__c.daysBetween(leadMOS.Start_Date__c)), currentMOS.Lag__c);
      }
      else{
        //Lag should be blank if date is not auto adjusted
        System.assert(String.isBlank(currentMOS.Lag__c));
      }
    }

    //Check dates after updating Lead MOS date
    for(Multi_Office_Split__c currentMOS: afterUpdateMOSList){
      if(currentMOS.Auto_Adjust_Dates__c){
        //Lag should not be blank if date is auto adjusted
        System.assert(String.isNotBlank(currentMOS.Lag__c));
        //Lag should have changed
        System.assertEquals(String.valueOf(currentMOS.Start_Date__c.daysBetween(leadMOS.Start_Date__c)), currentMOS.Lag__c);
        //Date should have changed
        System.assertEquals(Date.today().addDays(4),currentMOS.Start_Date__c);
      }
      else{
        //Lag should be blank
        System.assert(String.isBlank(currentMOS.Lag__c));
        //Date should not have changed
        System.assertEquals(Date.today(),currentMOS.Start_Date__c);
      }
    }
  }  
  /*US2396: Test class to check that when the Lead PU changes, other MOS dates do not change*/
  @isTest
  private static void shouldNotChangeDateOnLeadPUChange() {
    //Prepare data
    //Account
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    //Opportunity
    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

    //Unit
    TestHelper.UnitBuilder unitBuilder = new TestHelper.UnitBuilder();
    Unit__c JESAPU = unitBuilder.build().withLOB('JESA').withBU('JESA - Joint Venture').withName('020342 (JESA MS) JT-I2S').save().getRecord();

    //Multi Offices
    List<Multi_Office_Split__c> mosTestData = new List<Multi_Office_Split__c>();
    TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();

    //Multi Office with Auto Adjust = True
    Multi_Office_Split__c mosAutoAdjust = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).getRecord();
    mosAutoAdjust.Auto_Adjust_Dates__c   = true;

    //Add MOS to the list
    mosTestData.add(mosAutoAdjust);

    //Multi Office with Auto Adjust = True
    Multi_Office_Split__c mosNoAutoAdjust = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).getRecord();
    mosAutoAdjust.Auto_Adjust_Dates__c   = false;
     
    //Add MOS to the list  
    mosTestData.add(mosNoAutoAdjust);

    insert mosTestData;

    //Prepare a map of all MOS tied to the Opp
    Map<Id, Multi_Office_Split__c> mosMap = new Map<Id, Multi_Office_Split__c>([Select Id, 
                                                                                       Lead__c,
                                                                                       Lag__c,
                                                                                       Start_Date__c,
                                                                                       Auto_Adjust_Dates__c
                                                                                From Multi_Office_Split__c 
                                                                                Where Opportunity__c = :opp.Id]);

    //Start test
    Test.startTest();

    //Make one of the newly added MOS the lead MOS
    mosTestData[0].Lead__c = true;
    mosTestData[0].Start_Date__c = mosTestData[0].Start_Date__c.addDays(10);

    update mosTestData[0];

    //Requery Multi Office and ensure that the dates have changed for auto adjust and not for auto adjust = false
    List<Multi_Office_Split__c> afterUpdateMOSList = [Select Id, 
                                                             Lead__c,
                                                             Lag__c,
                                                             Start_Date__c,
                                                             Auto_Adjust_Dates__c
                                                      From Multi_Office_Split__c 
                                                      Where Opportunity__c = :opp.Id];    

    Test.stopTest();

    //Asserts
    //Check dates after changing Lead MOS
    for(Multi_Office_Split__c currentMOS: afterUpdateMOSList){
      if(currentMOS.Id == mosTestData[0].Id){
        //The date should have changed for the MOS for which the date was changed in the test
        System.assertEquals(mosMap.get(currentMOS.Id).Start_Date__c.addDays(10),currentMOS.Start_Date__c);
        //This should be the new lead now
        System.assert(currentMOS.Lead__c);
      }
      else {
        //Other MOS dates should not have changed
        System.assertEquals(mosMap.get(currentMOS.Id).Start_Date__c,currentMOS.Start_Date__c);
      }
    }
  }  
}