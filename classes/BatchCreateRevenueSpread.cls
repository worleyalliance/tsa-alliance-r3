/**************************************************************************************
Name:BatchCreateRevenueSpread
Version: 1.0 
Created Date: 12.04.2017
Function: Batch class to create revenue spread

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                   
* Pradeep Shetty    07/01/2018      Original Version
* Pradeep Shetty    09/25/2018      DE713: Remove compilation errors
*************************************************************************************/
global class BatchCreateRevenueSpread implements Database.Batchable<sObject> {
	
	String query;
  Map<String, Id> recordTypeMap = new Map<String, Id>(); //Map for recordTypes
	
	global BatchCreateRevenueSpread() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		//Get all Multi Office
		query = 'select Average_Gross_Margin__c, Contains_Legacy_PUs__c, CreatedById, CreatedDate, End_Date__c, Executing_BU__c, Executing_LOB__c, ' + 
		        'GM_Per_Day__c, Gross_Margin__c, Hours__c, Id, IsDeleted, LastActivityDate, LastModifiedById, LastModifiedDate, Lead__c, Mean_Date__c, ' +
		        'Name, Number_of_Days__c, Number_of_Months__c, Opportunity__c,  Performance_Unit_PU__c, Probable_GM__c, ' +
		        'Probable_GM_Per_Day__c, Resource_Type__c, Revenue__c, Revenue_Spread_Total_GM__c, Spreading_Formula__c, Standard_Deviation__c, ' +
		        'Standard_Deviation_in_Days__c, Standard_Deviation_Months__c, Start_Date__c, SystemModstamp from Multi_Office_Split__c WHERE isDeleted=false';
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope) {

  	List<Multi_Office_Split__c> mosList = new List<Multi_Office_Split__c>();

	  for(sobject s : scope){
    		mosList.add((Multi_Office_Split__c)s);
    }
  	ForecastSpread_Service.createSpread(mosList,new Set<String>{ForecastSpread_Service.REC_TYPE_QUARTERLY_SPREAD, ForecastSpread_Service.REC_TYPE_MONTHLY_SPREAD});
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}