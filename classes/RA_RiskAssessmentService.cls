public class RA_RiskAssessmentService {

    public static scoreWrapper calcRiskScore(Review__c theReviewRec, Map<String, String> selectedCategories){
        
        System.debug('theReviewRec: ' + theReviewRec);
        System.debug('selectedCategories: ' + selectedCategories);
        
        // Create a set of active categories, with which to narrow query results on
        Set<String> theCategories = new Set<String>();
        if(selectedCategories.get('execution') == 'true'){
            theCategories.add('Execution Risks');
        }
        if(selectedCategories.get('client') == 'true'){
            theCategories.add('Client Risks');
        }
        if(selectedCategories.get('geography') == 'true'){
            theCategories.add('Geography Risks');
        }
        if(selectedCategories.get('commercial') == 'true'){
            theCategories.add('Commercial Risks');
        }
        if(selectedCategories.get('contractual') == 'true'){
            theCategories.add('Contractual Risks');
        }
        if(selectedCategories.get('opportunity') == 'true'){
            theCategories.add('Confirm Opportunity Info');
        }
        
        String recordTypeName = Schema.SObjectType.Review__c.getRecordTypeInfosById().get(theReviewRec.RecordTypeId).getName();
        System.debug('recordTypeName: ' + recordTypeName);
        if(!(recordTypeName.contains('ECR') || recordTypeName.contains('BIAF'))){
            String LOB = [SELECT Id, Selling_Line_of_Business__c
                          FROM Opportunity
                          WHERE Id = :theReviewRec.Opportunity__c
                          LIMIT 1][0].Selling_Line_of_Business__c;
            recordTypeName = LOB;
        }
        System.debug('recordTypeName: ' + recordTypeName);
        
        
        
        List<RA_Question__mdt> activeQuestions;
        if(recordTypeName.contains('ECR')){
            activeQuestions = [SELECT Id, RA_Step__r.Step_Label__c, Risk_Level_Condition__c, 
                                      Additional_Condition__c, Field_Name__c
                               FROM RA_Question__mdt
                               WHERE RA_Step__r.Step_Label__c IN :theCategories
                               AND Line_of_Business__c = 'ECR'];
        } else if(recordTypeName.contains('BIAF')){
            activeQuestions = [SELECT Id, RA_Step__r.Step_Label__c, Risk_Level_Condition__c, 
                                      Additional_Condition__c, Field_Name__c
                               FROM RA_Question__mdt
                               WHERE RA_Step__r.Step_Label__c IN :theCategories
                               AND Line_of_Business__c = 'BIAF'];
        }
        
        // Query for a list of question metadata pertaining to only the active categories
        /*activeQuestions = [SELECT Id, RA_Step__r.Step_Label__c, Risk_Level_Condition__c, 
                                                  Additional_Condition__c, Field_Name__c
                                                  FROM RA_Question__mdt
                                                  WHERE RA_Step__r.Step_Label__c IN :theCategories
                                                  AND Line_of_Business__c = 'ECR'];*/
        
        //  Setting a default high level (lowest possible), might need to be changed later
        String highLevel;
        if(recordTypeName.contains('ECR')){
            highLevel = 'R5';
        }else if(recordTypeName.contains('BIAF')){
            highLevel = 'R7';
        }
        String highCategory = 'Standard';
        System.debug('highLevel: ' + highLevel);
        
        // Iterate through each question in the list
        if(activeQuestions != null){
            for(RA_Question__mdt q : activeQuestions){
            
                // Initialize a variable to hold the current iteration's highest lever, for comparison later.
                String thisLevel;
                String thisCategory;
            
                // If there is a condition, and the field isn't empty, evaluate it.
                if(q.Risk_Level_Condition__c != null && theReviewRec.get(q.Field_Name__c) != null){
                    //testQuestions.add(q);
                    
                    // Get the name of the field to be evaluated
                    String theValue = (String)theReviewRec.get(q.Field_Name__c);
                    
                    // Now we need to deserialize and parse the JSON for processing.
                    Map<String, Object> conditionMap = (Map<String, Object>)JSON.deserializeUntyped(q.Risk_Level_Condition__c);
                    thisLevel = (String)conditionMap.get('else');
                    if(thisLevel == 'R2'){
                        thisCategory = 'High';
                    }
                    
                    //  If the question metadata has an "Additional Condition"
                    if(q.Additional_Condition__c != null){
                        
                        // Keep processing JSON info for processing
                        Map<String, Object> additionalMap = (Map<String, Object>)JSON.deserializeUntyped(q.Additional_Condition__c);
                        List<Map<String, Object>> someMaps = new List<Map<String, Object>>();
                        List<Object> mapObjects = (List<Object>)additionalMap.get('additionalConditions');
                        for(Object o : mapObjects){
                            someMaps.add((Map<String, Object>)o);
                        }
                        
                        // Iterate through the resulting list of conditions to check
                        for(Object o : someMaps){
                            // Cast the condition to something actionable
                            Map<String, Object> condition = (Map<String, Object>)o;
                            
                            // Check to see if the json indicates checking against a string or a decimal
                            if(condition.get('isString') == true){
                                // Extract the relevent info for easy access in processing.
                                String fieldName = (String)condition.get('FieldName');
                                String testValue = condition.get('Value') != null ? (String)condition.get('Value') : null;
                                String answer = (String)condition.get('Answer');
                                String operator = (String)condition.get('Operator');
                                String riskLevel = (String)condition.get('RiskLevel');
                                String riskCategory = (String)condition.get('RiskCategory');
                                
                                if(fieldName.contains(';')){
                                    List<Map<String, String>> conditionMaps = new List<Map<String, String>>();
                                    List<String> fieldNameList = fieldName.split(';');
                                    List<String> testValueList = testValue.split(';');
                                    List<String> operatorList;
                                    if(operator.contains(';')){
                                        operatorList = operator.split(';');
                                    } else {
                                        operatorList = new List<String>();
                                        for(String s : testValueList){
                                            operatorList.add(operator);
                                        }
                                    }
                                    
                                    Boolean conditionPass = true;
                                    for(Integer i = 0; i < fieldNameList.size(); i++){
                                        Map<String, String> m = new Map<String, String>();
                                        m.put(fieldNameList[i], testValueList[i]);
                                        conditionMaps.add(m);
                                        
                                        List<String> fieldList = new List<String>();
                                        fieldList.addAll(m.keySet());
                                        String field = fieldNameList[i];
                                        //updated to set null string to null
                                        String value = testValueList[i] != 'null' ? testValueList[i] : null;
                                        String thisOperator = operatorList[i];
                                        
                                        if(theReviewRec.get(q.Field_Name__c) == answer){
                                            switch on thisOperator{
                                                when 'E'{
                                                    if(theReviewRec.get(field) != value){
                                                        conditionPass = false;
                                                    }
                                                }
                                                when 'GE'{
                                                    if((Decimal)theReviewRec.get(field) < Decimal.valueOf(value)){
                                                        conditionPass = false;
                                                    }
                                                }
                                            }
                                        } else {
                                            // The question's answer doesn't match criteria to check the other questions' values
                                            conditionPass = false;
                                        }
                                    }
                                    if(conditionPass){
                                        thisLevel = riskLevel;
                                        thisCategory = riskCategory;
                                    }
                                    
                                } else {
                                    
                                    if(theReviewRec.get(q.Field_Name__c) == answer){
                                        switch on operator{
                                            when 'E'{
                                                if(theReviewRec.get(fieldName) == testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            } else {
                                // Extract the relevant info for easy access in processing.
                                String fieldName = (String)condition.get('FieldName');
                                Decimal testValue = condition.get('Value') != null ? Decimal.valueOf((String)condition.get('Value')) : null;
                                String answer = (String)condition.get('Answer');
                                String operator = (String)condition.get('Operator');
                                String riskLevel = (String)condition.get('RiskLevel');
                                String riskCategory = (String)condition.get('RiskCategory');
                                
                                // If the answer conditions is met, process the condition
                                if(theReviewRec.get(q.Field_Name__c) == answer && selectedCategories.get('opportunity') != 'true'){
                                    // If there is a value to verify against, check it, if not, assign the risk level.
                                    if(testValue != null){
                                        // Refer to the operator value, to determine which kind of comparison to perform
                                        // If the conditions are met, replace the iteration's risk level with this condition's riskLevel
                                        switch on operator{
                                            when 'E'{
                                                if(theReviewRec.get(fieldName) == testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                            when 'GE'{
                                                if((Decimal)theReviewRec.get(fieldName) >= testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                            when 'LT'{
                                                if((Decimal)theReviewRec.get(fieldName) < testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                        }
                                    } else {
                                        thisLevel = riskLevel;
                                        thisCategory = riskCategory;
                                    }
                                } else if(theReviewRec.get(q.Field_Name__c) == answer && selectedCategories.get('opportunity') == 'true'){
                                    // If there is a value to verify against, check it, if not, assign the risk level.
                                    if(testValue != null){
                                        // Refer to the operator value, to determine which kind of comparison to perform
                                        // If the conditions are met, replace the iteration's risk level with this condition's riskLevel
                                        switch on operator{
                                            when 'E'{
                                                if(theReviewRec.get(fieldName) == testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                            when 'GE'{
                                                if((Decimal)theReviewRec.get(fieldName) >= testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                            when 'LT'{
                                                if((Decimal)theReviewRec.get(fieldName) < testValue){
                                                    thisLevel = riskLevel;
                                                    thisCategory = riskCategory;
                                                }
                                            }
                                        }
                                    } else {
                                        thisLevel = riskLevel;
                                        thisCategory = riskCategory;
                                    }
                                }
                            }
                        }
                        
                        // If there are no Additional Conditions, go with the else.
                    } else {
                        thisLevel = (String)conditionMap.get('else');
                        thisCategory = 'Standard';
                    }
                }
                
                // Process this iterations risk level, compare it to the current highest level.
                // If this one is higher, it becomes the new highest.
                // This is assuming R1 is highest, R7 is lowest.
                if(thisLevel != null && thisLevel != highLevel && highLevel != 'R1'){
                    
                    // if highest level is R7, and this level is not R7, this level is automatically higher.
                    if(highLevel == 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    } else if(highLevel == 'R6' && thisLevel != 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    } else if(highLevel == 'R5' && thisLevel != 'R6' && thisLevel != 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    } else if(highLevel == 'R4' && thisLevel != 'R5' && thisLevel != 'R6' && thisLevel != 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    } else if(highLevel == 'R3' && thisLevel != 'R4' && thisLevel != 'R5' && thisLevel != 'R6' && thisLevel != 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    } else if(highLevel == 'R2' && thisLevel != 'R3' && thisLevel != 'R4' && thisLevel != 'R5' && thisLevel != 'R6' && thisLevel != 'R7'){
                        highLevel = thisLevel;
                        highCategory = thisCategory;
                    }
                    
                }
            }
        }
        
        // Instantiate a wrapper for return payload, fill it with the values we evaluated.
        String color;
        if(highCategory == 'Standard'){
            color = '#2FC9A1';
        } else if(highCategory == 'Elevated'){
            color = '#ffc52d';
        } else if(highCategory == 'High'){
            color = '#c92a2e';
        } else {
            color = '#2FC9A1';
            highCategory = 'Standard';
        }
        scoreWrapper returnWrapper = new scoreWrapper(highLevel, highCategory, color);
        
        System.debug('returnWrapper: ' + returnWrapper);
        
        return returnWrapper;
        
    }
    
    public class scoreWrapper{
        
        @auraEnabled
        Public String riskLevel {get; set;}
        @auraEnabled
        Public String category {get; set;}
        @auraEnabled
        Public String iconColor {get; set;}
        
        Public scoreWrapper(String risk, String cat, String icon){
            this.riskLevel = risk;
            this.category = cat;
            this.iconColor = icon;
        }
    }
    
    public static String getRecordTypeName(Review__c theReviewRec){
        
        List<RecordType> reviewRTs = [SELECT Name, Id
                                      FROM RecordType
                                      WHERE sObjectType = 'Review__c'];
        Map<String, Set<String>> rtIdMap = new Map<String, Set<String>>();
        for(RecordType rt : reviewRTs){
            if(rt.Name.contains('ECR')){
                Set<String> IDList = rtIdMap.get('ECR');
                if(IDList == null){
                    IDList = new Set<String>();
                }
                IDList.add(rt.Id);
                rtIdMap.put('ECR', IDList);
            } else if(rt.Name.contains('BIAF')){
                Set<String> IDList = rtIdMap.get('BIAF');
                if(IDList == null){
                    IDList = new Set<String>();
                }
                IDList.add(rt.Id);
                rtIdMap.put('BIAF', IDList);
            }
            
        }
        
        if(rtIdMap.get('ECR').contains(theReviewRec.recordTypeId)) {
            return 'ECR';
        } else if(rtIdMap.get('BIAF').contains(theReviewRec.recordTypeId)) {
            return 'BIAF';  
        } else {
            return null;
        }
        
    }
    
}