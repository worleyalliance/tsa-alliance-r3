/**************************************************************************************
Name: SharePointClient
Version: 1.0 
Created Date: 24.02.2017
Function: Client class use to perfom callouts for SharePoint

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/
public class SharePointClient {

   /*
    * Method retrieves files places under foler of given path
    */
/*    public List<SharePointConfiguration.File> retrieveFolderFiles(String folderPath){

        folderPath = SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + folderPath;
        folderPath = EncodingUtil.urlEncode(folderPath, 'UTF-8');
        folderPath = folderPath.replaceAll('\\+','%20');
        String folderURL = SharePointConfiguration.GET_FOLDER_FILES_URL.replace(SharePointConfiguration.FOLDER_PATH, folderPath);
        HttpRequest req = new HttpRequest();
        req.setEndpoint(SharePointConfiguration.SHARE_POINT_ENDPOINT_URL + folderURL);
        req.setMethod('GET');
        req.setHeader('accept', 'application/json;odata=verbose');

        HttpResponse resp = makeCallout(req);
        SharePointConfiguration.FilesResponse response;
        try {
            response = (SharePointConfiguration.FilesResponse) JSON.deserialize(resp.getBody(), SharePointConfiguration.FilesResponse.class);
        } catch (JSONException ex){
            throw new SharpointCommunicationException(ex.getMessage());
        }
        System.debug(response);
        return response != null ? response.d.results : new List<SharePointConfiguration.File>();
    }*/

   /*
    * Method create new file or overwrites existing in SharePoint
    * If file is too big it will be send to SP in chunks
    * https://msdn.microsoft.com/en-us/library/office/dn450841.aspx#bk_FileStartUpload
    */
/*    public Integer uploadDocument(
            String fileName,
            Blob fileBody,
            String folderPath,
            String sitePath,
            String guid,
            Decimal offset,
            Boolean isFirstChunk,
            Boolean isSingleChunk,
            Boolean isLastChunk){
        String filePath = '/sites/SharePointChampions/SalesForceIntegration/'+ SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + folderPath +'/'+ fileName;
        filePath = EncodingUtil.urlEncode(filePath, 'UTF-8');
        filePath = filePath.replaceAll('\\+','%20');

        folderPath = SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + folderPath;
        folderPath = EncodingUtil.urlEncode(folderPath, 'UTF-8');
        folderPath = folderPath.replaceAll('\\+','%20');

        guid = EncodingUtil.urlEncode(guid, 'UTF-8');

        String url = SharePointConfiguration.SHARE_POINT_ENDPOINT_URL;
        if(isFirstChunk || isSingleChunk){
            fileName = EncodingUtil.urlEncode(fileName, 'UTF-8');
            fileName = fileName.replaceAll('\\+','%20');
            String addNewURL =
                    SharePointConfiguration.SHARE_POINT_ENDPOINT_URL +
                            SharePointConfiguration.CREATE_FILE_URL
                                    .replace(SharePointConfiguration.FOLDER_PATH, folderPath)
                                    .replace(SharePointConfiguration.FILE_NAME, fileName);
            System.debug('addNewURL: '+addNewURL);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(addNewURL);
            req.setMethod('POST');
            req.setHeader('accept', 'application/json;odata=verbose');

            //when the file is smaller than chunk just save it and finish action
            if(isSingleChunk) {
                req.setHeader('content-length', String.valueOf(fileBody.size()));
                req.setBodyAsBlob(fileBody);
                makeCallout(req);
                return null;
            }

            // when there file have to be splitted just create empty file and start uplaoding
            req.setHeader('content-length', String.valueOf(0));
            makeCallout(req);
            url += SharePointConfiguration.FILE_STARTUPLOAD_URL
                    .replace(SharePointConfiguration.FILE_PATH, filePath)
                    .replace(SharePointConfiguration.GUID, guid);
        } else if(isLastChunk){
            url += SharePointConfiguration.FILE_FINISHUPLOAD_URL
                    .replace(SharePointConfiguration.FILE_PATH, filePath)
                    .replace(SharePointConfiguration.GUID, guid)
                    .replace(SharePointConfiguration.OFFSET, String.valueOf(offset));
        } else {
            url += SharePointConfiguration.FILE_CONTINUEUPLOAD_URL
                    .replace(SharePointConfiguration.FILE_PATH, filePath)
                    .replace(SharePointConfiguration.GUID, guid)
                    .replace(SharePointConfiguration.OFFSET, String.valueOf(offset));
        }
        System.debug('URL: '+url);
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('accept', 'application/json;odata=verbose');
        req.setHeader('content-length', String.valueOf(fileBody.size()));
        req.setBodyAsBlob(fileBody);

        HttpResponse resp = makeCallout(req);
        SharePointConfiguration.FilesResponse response;
        try {
            response = (SharePointConfiguration.FilesResponse) JSON.deserialize(resp.getBody(), SharePointConfiguration.FilesResponse.class);
        } catch (JSONException ex){
            throw new SharpointCommunicationException(ex.getMessage());
        }
        System.debug(response);
        return response.d.ContinueUpload != null ? response.d.ContinueUpload : response.d.StartUpload;
    }*/

   /*
    * Method create new file or overwrites existing in SharePoint
    */
/*    public void deleteDocument(String fileName, String folderPath){
        String filePath = SharePointConfiguration.ROOT_SHAREPOINT_FOLDER + folderPath + '/' + fileName;
        filePath = EncodingUtil.urlEncode(filePath, 'UTF-8');
        filePath = filePath.replaceAll('\\+','%20');

        String url =
                SharePointConfiguration.SHARE_POINT_ENDPOINT_URL +
                        SharePointConfiguration.GET_FOLDER_PATH_URL.replace(SharePointConfiguration.FILE_PATH, filePath);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('accept', 'application/json;odata=verbose');
        req.setHeader('X-HTTP-Method', 'DELETE');
        req.setHeader('IF-MATCH', '*');
        req.setHeader('content-length', '0');

        makeCallout(req);
    }*/

   /*
    * Method which performs actual callout and handles errors
    */
/*    private HTTPResponse makeCallout(HttpRequest req){
        Http http = new Http();
        HTTPResponse res;
        try {
            res = http.send(req);
            if (res.getStatusCode() >= 400 ) {
                try {
                    ErrorResponse errorResp = (ErrorResponse) JSON.deserialize(res.getBody(), ErrorResponse.class);
                    throw new SharpointCommunicationException(errorResp.error.message.value);
                } catch (JSONException ex){
                    throw new SharpointCommunicationException(ex.getMessage());
                }
            }
        } catch (CalloutException ex){
            System.debug(ex.getStackTraceString());
            System.debug(ex.getMessage());
            throw new SharpointCommunicationException(ex.getMessage());
        }
        return res;
    }*/

   /*
    * Wrapper for error message response
    */
/*    public class ErrorResponse{
        public ErrorData error {get; set;}
    }

    public class ErrorData{
        public String code {get; set;}
        public ErrorMessage message {get; set;}
    }

    public class ErrorMessage{
        public String lang {get; set;}
        public String value {get; set;}
    }*/

   /*
    * Custom SharePoint communication exception
    */
/*
    public class SharpointCommunicationException extends Exception{}
*/

}