@isTest
global class ResponseExceptionMock implements WebServiceMock {

    public String message = 'Callout Exception Occured';

    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNs,
        String responseName,
        String responseType
    ){
            CalloutException ex = new CalloutException();
            ex.setMessage(message);
            throw ex;
    }
}