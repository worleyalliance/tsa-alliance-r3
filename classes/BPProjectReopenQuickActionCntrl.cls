/**************************************************************************************
Name:BPProjectReopenQuickActionCntrl
Version: 1.0 
Created Date: 08/15/2017
Function: Updated B&P Status to active

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Rajamohan Vakati   08/15/2017       Original Version
*************************************************************************************/

public class BPProjectReopenQuickActionCntrl {
    
    @AuraEnabled
    public static void updateBPStatus(Id bpId , String statusName) {
        BPProjectReopenQuickActionService service = new BPProjectReopenQuickActionService();
        service.updateBPStatus(bpId, statusName);
    }
    @AuraEnabled
    public static Boolean checkRequestAllowed(Id bpId, String changeRequested) {
        BPProjectReopenQuickActionService service = new BPProjectReopenQuickActionService();
        return service.checkRequestAllowed(bpId, changeRequested);
        
    }
    
    
}