/**************************************************************************************
Name: TriggerUtil
Version: 1.0 
Created Date: 06.24.2017
Function: Functionalities related to proper handling triggers

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           06.24.2017         Original Version
*************************************************************************************/
public class TriggerUtil {
    public static Boolean isTriggerActive(String triggerName){
        triggerName = triggerName + '__c';
        Technical_Configuration__c config = Technical_Configuration__c.getInstance(UserInfo.getUserId());
        if(config != null){
            Boolean isTriggerActive = config.get(triggerName)!= null ? (Boolean) config.get(triggerName) : true;
            return isTriggerActive;
        }
        return true;
    }
}