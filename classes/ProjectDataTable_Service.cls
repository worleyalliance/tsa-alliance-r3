/**************************************************************************************
Name: ProjectDataTable_Service
Version: 1.0 
Created Date: 05.05.2017
Function: Controller of ProjectDataTable component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
public class ProjectDataTable_Service extends DataTable_Service{

    private final Map<String, String> totalRowFieldMapping = new Map<String, String>{
            Jacobs_Project__c.Project_Status__c.getDescribe().getName() => 'ProjectStatusOperator',
            Jacobs_Project__c.Name.getDescribe().getName() => 'NameOperator'
    };

    private final Map<String, IFieldValueOperator> fieldValueOperatorMap = new Map<String, IFieldValueOperator>{
            'ProjectStatusOperator' => new ProjectStatusOperator(),
            'NameOperator' => new NameOperator()
    };

   /*
    * Method retrieves Project records for opportunity
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    public TableData getProjectData(
            String fieldSetName,
            Id recordId,
            String recordTypeNames,
            String projectTypeNames,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault
    ) {
        validateObjectAccesible(Schema.getGlobalDescribe().get(sObjectName).getDescribe().getName());

        List<FieldDescription> fieldDescriptions = prepareFieldDescriptions(fieldSetName, sObjectName);
        String query =
                buildProjectQuery(
                        fieldDescriptions,
                        recordId,
                        recordTypeNames,
                        projectTypeNames,
                        relationField,
                        sObjectName
                );

         
        //Execute query
        List<sObject> records = (List<sObject>) Database.query(query);   
        updateValueOperatorService();
        String additionalValueField = null;
        system.debug('12345');
        TableData data = buildTableData(records, fieldDescriptions, totalRowPresent, isSumDefault, additionalValueField);
        system.debug('67890');
        return data;
    }

   /*
    * Method prepares a query based on delivered data
    */
    private String buildProjectQuery(
            List<DataTable_Service.FieldDescription> fieldDescriptions,
            Id recordId,
            String recordTypeNames,
            String projectTypeNames,
            String relationField,
            String sObjectName
    ){
        String query = super.buildQuery(
                fieldDescriptions,
                recordId,
                relationField,
                sObjectName
        );
        if(String.isNotBlank(recordTypeNames)){
            List<String> recordTypeDevNames = recordTypeNames.split(',');
            String recordTypesPart = '';
            for(String recordTypeDevName : recordTypeDevNames){
                recordTypesPart += '\'' + recordTypeDevName  + '\'' + ',';
            }
            recordTypesPart = recordTypesPart.removeEnd(',');
            query += ' AND RecordType.DeveloperName IN (' + recordTypesPart + ')';
        }
        System.debug('query: '+query);
        
        //String projectTypesPart = '';
        
        //projectTypeNames = '';
        
        System.debug('projectTypeNames = ' +projectTypeNames);
        
        if(String.isNotBlank(projectTypeNames)){
            List<String> projectTypes = projectTypeNames.split(',');
            String projectTypesPart = '';
            for(String projectTypeName : projectTypes){
                projectTypesPart += '\'' + projectTypeName + '\'';
            }
            query += ' AND Project_Type__c IN (' + projectTypesPart + ')';

        }
        System.debug('query: '+query);         
        return query;
    }

   /*
    * Method updates configuration for total row calculation for Group Attrition
    */
    private void updateValueOperatorService(){
        getValueOpeartorService().operatorsByKey.putAll(fieldValueOperatorMap);
        getValueOpeartorService().fieldNameByOperatorKey.putAll(totalRowFieldMapping);
    }

   /*
    * Simple action for Project status column
    */
    public class ProjectStatusOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Object value = Label.Total;
            return value;
        }
    }

   /*
    * Simple action Name field
    */
    public class NameOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Object value = '';
            return value;
        }
    }
}