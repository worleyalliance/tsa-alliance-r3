/**************************************************************************************
Name: UserDataProvider
Version: 1.0 
Created Date: 05.05.2017
Function: An extenstion of UserInfo class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
* Scott Stone				12.10.2018		   US2648 - 
*************************************************************************************/
public with sharing class UserDataProvider {

    public static String getUserCurrencySymbol(){
        if(Test.isRunningTest()){
            return '$';
        } else {
            return ConnectApi.Organization.getSettings().userSettings.currencySymbol;
        }
    }
    
    public static Boolean isUserAnAdministrator(String userId) {
        //Set containing Permission sets containing 'Administrators' custom permission
        Set<Id> permissionSetIdList = new Set<Id>();
    
        //Set of Permission sets tied to the SetUpEntityAccess 
        for(SetupEntityAccess sea: [Select Id, 
                                           ParentId 
                                    From SetupEntityAccess 
                                    Where SetupEntityId in (Select Id 
                                                            From CustomPermission 
                                                            Where DeveloperName = 'Administrators')])
        {
          permissionSetIdList.add(sea.ParentId);
        }
    
        List<PermissionSetAssignment> lstcurrentUserPerSet = [Select Id, 
                                                                     PermissionSetId
                                                              From PermissionSetAssignment
                                                              Where AssigneeId = :userId];
      
        //Check if the user is an administrator 
        for(PermissionSetAssignment psa : [Select Id, 
                                                  PermissionSetId
                                           From PermissionSetAssignment
                                           Where AssigneeId = :Userinfo.getUserId()])
        {
          if(permissionSetIdList.contains(psa.PermissionSetId))
          {
            return true;
          }
        } //END FOR
        return false;
    }
}