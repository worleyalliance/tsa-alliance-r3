/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 14.03.2017
Function: Controller for CustomRelatedList component. Handles data read, save and delete

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   05/05/2017      Original Version
* Pradeep Shetty    01/09/2018      De335: Closed won values should be 0 when Opp is not won
*******************************************************************************************************************/
@isTest
private class BookedActuals_C_Test {

  @isTest
  private static void shouldBuildAndReturnBookedActualComparisionData() {
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();
    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opportunity = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

    //when
    BookedActualsService.BookedActual bookedActual = BookedActuals_C.getBookedActual(opportunity.Id);

    //then
    System.assertEquals('0.00', bookedActual.soldRevenue); //Revenue should be 0 since Opportunity is not won
    System.assertEquals('0.00', bookedActual.budgetedGMWorkhours); //there is no child records so value should be 0
    System.assertEquals('0.00', bookedActual.varianceGMWorkhours); //there is no child records so value should be 0
  }

  @isTest
  private static void shouldSetClosedWonValuesToOpportunityValues() {
    //given
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();
    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opportunity = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

    //Set Opportunity Status to Closed Won
    opportunity.StageName = 'Closed - Won';

    update opportunity;

    //when
    BookedActualsService.BookedActual bookedActual = BookedActuals_C.getBookedActual(opportunity.Id);

    //then
    System.assertEquals('1000.00', bookedActual.soldRevenue); //Revenue should be 1000 since Opportunity is won
    System.assertEquals('0.00', bookedActual.budgetedGMWorkhours); //there is no child records so value should be 0
    System.assertEquals('0.00', bookedActual.varianceGMWorkhours); //there is no child records so value should be 0
  }

  @isTest
  private static void shouldThrowErrorWhenThereIsNoOpprotunityWithGivenId() {

    //when
    try {
      BookedActualsService.BookedActual bookedActual = BookedActuals_C.getBookedActual(null);

      //then
      System.assert(false, 'There is no opportunity with Id null so should not reach this line');
    } catch (Exception ex){
      System.assert(ex instanceof AuraHandledException);
    }
  }
}