/**************************************************************************************
Name: ProjectDataTable_C_Test
Version: 1.0 
Created Date: 05.05.2017
Function: Unit test class of ProjectDataTable_C controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
@isTest
private class ProjectDataTable_C_Test {

  //Constants
  private static final String PROJECT_STATUS_ACTIVE   = 'Active';
  private static final String PROJECT_TYPE_CONTRACT   = 'CONTRACT';
  private static final String PROJECT_RECTYPE_FINANCE = 'Finance';
  private static final String PROJECT_RECTYPE_FIN_YTD = 'Finance_YTD';


  @IsTest
  private static void shouldCalculateDataForProjectRecords() {
    //given
    Map<String, FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('Jacobs_Project__c').getDescribe().fieldSets.getMap();
    String fieldSetName =  fieldSetMap.values()[0].name;

    for(FieldSet fs : fieldSetMap.values())
    {
      for(FieldSetMember fsm : fs.fields)
      {
        if(fsm.getFieldPath() == 'Actual_Revenue__c')
        {
            fieldSetName = fs.getName();
            break;
        }
      } //END FOR
    } //END FOR

    //Create an Account
    TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
    Account acc = accountBuilder.build().save().getRecord();

    //Create an Opportunity
    TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
    Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

    //Revenue
    Decimal revenue = 20;

    //Project Status
    String status = PROJECT_STATUS_ACTIVE;

    //Create Project
    TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
    //Create Parent Project
    Jacobs_Project__c ptdProject = projectBuilder.build()
                                              .withOpportunity(opp.Id)
                                              .withStatus(status)
                                              .withActualLocalRevenue(revenue)
                                              .withProjectType(PROJECT_TYPE_CONTRACT)
                                              .withRecordType(PROJECT_RECTYPE_FINANCE)
                                              .withProjectId('12345')
                                              .save()
                                              .getRecord();

    //Create 
    Jacobs_Project__c ytdProject = projectBuilder.build()
                                              .withOpportunity(opp.Id)
                                              .withStatus(status)
                                              .withActualLocalRevenue(revenue)
                                              .withProjectType(PROJECT_TYPE_CONTRACT)
                                              .withRecordType(PROJECT_RECTYPE_FIN_YTD)
                                              .withProjectId('12345')
                                              .withFiscalYear('FY2017')
                                              .getRecord();  

    //Associate child with parent
    ytdProject.PTD_Project__c = ptdProject.Id;

    //Insert the child project
    insert ytdProject;                                                                                        

    //when
    DataTable_Service.TableData result = ProjectDataTable_C.getData(fieldSetName,
                                                                    opp.Id,
                                                                    PROJECT_RECTYPE_FINANCE,
                                                                    PROJECT_TYPE_CONTRACT,
                                                                    Jacobs_Project__c.Opportunity__c.getDescribe().getName(),
                                                                    Jacobs_Project__c.sObjectType.getDescribe().getName(),
                                                                    true,
                                                                    true);

    //then
    System.assertEquals('$'+revenue, result.totalRow.get('Actual_Revenue__c'));
    }
}