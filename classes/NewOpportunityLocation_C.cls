/**************************************************************************************
Name: NewOpportunityLocation_C
Version: 1.0 
Created Date: 26.04.2017
Function: Adds new Location to Oppotunity base on Account locations

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   26.04.2017         Original Version
*************************************************************************************/
public with sharing class NewOpportunityLocation_C {
    private static NewOpportunityLocation_Service service = new NewOpportunityLocation_Service();

   /*
    * Returns list of locations attached to account related to given opportunity
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> retrieveAccountLocations(Id opportunityId){
        return service.retrieveAccountLocations(opportunityId);
    }

   /*
    * Saves new junction relation between opportunity and account location
    */
    @AuraEnabled
    public static Id saveOpportunityLocation(Id opportunityId, Id locationId){
        return service.saveOpportunityLocation(opportunityId, locationId);
    }


   /*
    * It returns only opportunity name for given opportunity id
    */
    @AuraEnabled
    public static String retrieveOpportunityName(Id opportunityId){
        return service.retrieveOpportunityName(opportunityId);
    }
}