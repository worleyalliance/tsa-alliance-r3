public with sharing class PF_TestCasesCloningAuraController {
    
    @AuraEnabled
    Public static List<childObjectWrapper> getWorkTaskList(String parentRecordId){
        Map<String, childObjectWrapper> childObjectMap = new Map<String, childObjectWrapper>();
        List<childObjectWrapper> childObjectList = new List<childObjectWrapper>();
        Map<String, Integer> childObjNameSizeMap = new Map<String, Integer>();
        try{
            childObjNameSizeMap.put('PF_TestCases__c', [select count() from PF_TestCases__c where PF_Predecessor_Test_Case__c =:parentRecordId]);
            childObjNameSizeMap.put('PF_Test_Case_Step__c', [select count() from PF_Test_Case_Step__c where PF_Test_Case__c =:parentRecordId]);
            childObjNameSizeMap.put('PF_TestCaseExecution__c', [select count() from PF_TestCaseExecution__c where PF_Test_Case__c =:parentRecordId]);	
            
            for(Schema.ChildRelationship childObj : PF_Utility.getChildRelationshipsFromId(parentRecordId)){
                if(childObj.getChildSObject().getDescribe().isQueryable() && childObjNameSizeMap.containsKey(childObj.getChildSObject().getDescribe().getName()) && childObj.getField().getDescribe().isCreateable()){
                    childObjectMap.put(childObj.getChildSObject().getDescribe().getName(), new childObjectWrapper(
                        childObj.getChildSObject().getDescribe().getLabel(),
                        childObj.getChildSObject().getDescribe().getName(),
                        false,
                        childObjNameSizeMap.get(childObj.getChildSObject().getDescribe().getName())
                    ));
                } 
            }
            for(String eachObj :  childObjNameSizeMap.keySet()){/* This for loop is written to order: Test Case, Test Case Step, Test Case Execution, as Comparable Interface is not applicable here*/
                if(childObjectMap.containsKey(eachObj)){
                    childObjectList.add(childObjectMap.get(eachObj));
                }
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            System.debug('The following exception has occurred: ' + e.getLineNumber());
        }
        
        return childObjectList;
    }
    @AuraEnabled
    Public static string getCloneRecord(List<String> relatedChildsToClone, String newPfStoriesId, String newPfStoriesName){
        try{
            Set<String> relatedChildsToCloneSet = new Set<String>();
            relatedChildsToCloneSet.addAll(relatedChildsToClone);		
            List<sObject> recordsToBeInserted = new List<sObject>();
            sObject parentRecord;
            List<sObject> sObjList = new List<sObject>();
            String sobjQuery = '';
            If(newPfStoriesId != null && newPfStoriesId != ''){
                sobjQuery = PF_Utility.sQlQueryCreator('PF_TestCases__c', newPfStoriesId, True, '');
                sObjList = Database.query(sobjQuery);
                parentRecord = sObjList[0];
                parentRecord = parentRecord.clone(false, true, false, false);
                If(newPfStoriesName != null && newPfStoriesName != '')
                    parentRecord.put('Name',newPfStoriesName);
                
                parentRecord.put('PF_Status__c', 'New');
                Insert parentRecord;
                sObjList = new List<sObject>();
                sobjQuery = '';
            }
            If(!relatedChildsToCloneSet.isEmpty()){
                If(relatedChildsToCloneSet.contains('PF_TestCases__c')){
                    sobjQuery = PF_Utility.sQlQueryCreator('PF_TestCases__c', newPfStoriesId, false, 'PF_Predecessor_Test_Case__c');
                    sObjList = Database.query(sobjQuery);
                    
                    for(sObject eachRecord : sObjList){
                        sObject clonedEachChild = eachRecord.clone(false, true, false, false);
                        clonedEachChild.put('PF_Predecessor_Test_Case__c',parentRecord.id);
                        recordsToBeInserted.add(clonedEachChild);
                    }
                    sObjList = new List<sObject>();
                    sobjQuery = '';
                }
                
                If(relatedChildsToCloneSet.contains('PF_Test_Case_Step__c')){
                    sobjQuery = PF_Utility.sQlQueryCreator('PF_Test_Case_Step__c', newPfStoriesId, false, 'PF_Test_Case__c');
                    sObjList = Database.query(sobjQuery);
                    
                    for(sObject eachRecord : sObjList){
                        sObject clonedEachChild = eachRecord.clone(false, true, false, false);
                        clonedEachChild.put('PF_Test_Case__c',parentRecord.id);
                        recordsToBeInserted.add(clonedEachChild);
                    }
                    sObjList = new List<sObject>();
                    sobjQuery = '';
                }
                
                If(relatedChildsToCloneSet.contains('PF_TestCaseExecution__c')){
                    sobjQuery = PF_Utility.sQlQueryCreator('PF_TestCaseExecution__c', newPfStoriesId, false, 'PF_Test_Case__c');
                    sObjList = Database.query(sobjQuery);
                    
                    for(sObject eachRecord : sObjList){
                        sObject clonedEachChild = eachRecord.clone(false, true, false, false);
                        clonedEachChild.put('PF_Test_Case__c',parentRecord.id);
                        recordsToBeInserted.add(clonedEachChild);
                    }
                    sObjList = new List<sObject>();
                    sobjQuery = '';
                }
                Database.insert(recordsToBeInserted,false); 
            }
            return parentRecord.id;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            System.debug('The following exception has occurred: ' + e.getLineNumber());
        }
        return null;
    }
    
    //wrapper class to hold the value of related child records
    public class childObjectWrapper{
        @AuraEnabled
        public String childObjLabel {get;set;}
        @AuraEnabled
        public String childObjName {get;set;}
        @AuraEnabled
        public boolean isSelected {get;set;}
        @AuraEnabled
        public Integer numberOfChildRecord {get;set;}
        
        public childObjectWrapper(String childObjLabel, String childObjName, Boolean isSelected, Integer numberOfChildRecord){
            this.childObjLabel = childObjLabel;
            this.childObjName = childObjName;
            this.isSelected = isSelected;
            this.numberOfChildRecord = numberOfChildRecord;
        }
        
    }
}