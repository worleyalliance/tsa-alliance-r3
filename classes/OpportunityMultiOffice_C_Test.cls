/**************************************************************************************
Name: OpportunityMultiOffice_C_Test
Version: 1.0 
Created Date: 26.04.2017
Function: Unit test for OpportunityMultiOffice_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   26.04.2017      Original Version
* Pradeep Shetty    05/02/2018      US2010: changes related to PU Lookup
*************************************************************************************/
@isTest
private class OpportunityMultiOffice_C_Test {

    //Picklist changed to lookup as part of US2010
    /*@isTest
    private static void shouldRetrievePerformanceUnits() {
        //when
        List<BaseComponent_Service.PicklistValue> resutls = OpportunityMultiOffice_C.getPerformanceUnits();

        //then
        System.assertNotEquals(0, resutls.size());
    }*/

    @isTest
    private static void shouldRetrieveResourceTypes() {
        //when
        List<BaseComponent_Service.PicklistValue> resutls = OpportunityMultiOffice_C.getResourceTypes();

        //then
        System.assertNotEquals(0, resutls.size());
    }

    @isTest
    private static void shouldRetrieveMultiofficeRecordForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        Decimal revenue = 20;
        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withRevenue(revenue).withGM(10).withOpportunity(opp.Id).save().getRecord();

        //when
        List<OpportunityMultiOffice_Service.MultiOffice> resutls = OpportunityMultiOffice_C.getMultiOffices(opp.Id);

        //then
        OpportunityMultiOffice_Service.MultiOffice multiOfficeResult;
        for(OpportunityMultiOffice_Service.MultiOffice multiOffice : resutls){
            if(multiOffice.id == multiOfficeSplit.Id){
                multiOfficeResult = multiOffice;
            }
        }

        System.assertNotEquals(null, multiOfficeResult);
        System.assertEquals(false,multiOfficeResult.lead);
        System.assertEquals(revenue, multiOfficeResult.revenue);
    }

    @isTest
    private static void shouldSaveMultiofficeRecordForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).getRecord();
        OpportunityMultiOffice_Service.MultiOffice multiOffice = new OpportunityMultiOffice_Service.MultiOffice(multiOfficeSplit);

        OpportunityMultiOffice_Service.OpportunityData opportunityRecord = new OpportunityMultiOffice_Service.OpportunityData();
        opportunityRecord.id = opp.Id;
        opportunityRecord.targetProjectStartDate = Date.today().addDays(10);

        //when
        Multi_Office_Split__c result = OpportunityMultiOffice_C.saveMultiOffice(JSON.serialize(opportunityRecord), JSON.serialize(multiOffice), false);

        //then
        System.assertNotEquals(null, result);
    }

    @isTest
    private static void shouldUpdateMultiofficeRecordForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).save().getRecord();
        OpportunityMultiOffice_Service.MultiOffice multiOffice = new OpportunityMultiOffice_Service.MultiOffice(multiOfficeSplit);

        OpportunityMultiOffice_Service.OpportunityData opportunityRecord = new OpportunityMultiOffice_Service.OpportunityData();
        opportunityRecord.id = opp.Id;
        opportunityRecord.targetProjectStartDate = Date.today().addDays(10);

        //when
        Multi_Office_Split__c result = OpportunityMultiOffice_C.saveMultiOffice(JSON.serialize(opportunityRecord), JSON.serialize(multiOffice), false);

        //then
        System.assertNotEquals(null, result);
    }

    @isTest
    private static void shouldDeleteMultiofficeRecord() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).withLobBu().save().getOpportunity();

        TestHelper.MultiOfficeSplitBuilder multiOfficeSplitBuilder = new TestHelper.MultiOfficeSplitBuilder();
        Multi_Office_Split__c multiOfficeSplit = multiOfficeSplitBuilder.build().withOpportunity(opp.Id).save().getRecord();

        //when
        OpportunityMultiOffice_C.deleteMultiOffice(multiOfficeSplit.Id);

        //then
        List<Multi_Office_Split__c> results = [SELECT Id FROM Multi_Office_Split__c WHERE Id = :multiOfficeSplit.Id];
        System.assertEquals(0, results.size());
    }
}