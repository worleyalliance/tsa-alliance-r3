/**************************************************************************************
Name:LVtest_Controller
Version: 
Created Date: 
Function: LDT table for Projects for Library records

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo                   Original Version
* Madhu Shetty     31-Jul-2018      Updated condition to show inactive record types in filters
*************************************************************************************/
public with sharing class LVtest_Controller{
public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues(ID id)
    {
        List<Schema.RecordTypeInfo> recordtypes = Library__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        String recordType;
        
        recordType = [Select Recordtype.DeveloperName from Jacobs_Project__c where Id = :id LIMIT 1].Recordtype.DeveloperName;
        if(recordType != 'A_T_Program')
        {
            for(RecordTypeInfo rt : recordtypes)
            {
                if(rt.getName() != 'Master'&& rt.getName() != 'Professional Photo' && rt.isAvailable())
                    recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }
        }
        else
        {
            for(RecordTypeInfo rt : recordtypes)
            {
            List <LDT_Lightning_Filter__c> rtlist1 = [Select Label_del__c from LDT_Lightning_Filter__c where Object_API__c = 'Jacobs_Project__c' and OBJ_RecordType_API_Name__c = 'A_T_Program' and Coverage__c = 'Include'];// and Active__c = True];

                 for (LDT_Lightning_Filter__c k: rtlist1)
                 {
                     if (rt.getName() == k.Label_del__c && rt.getName() != 'Master' && rt.IsAvailable())
                         recordtypemap.put(rt.getRecordTypeId(), rt.getName());
                 }
            }
        }       
             
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel)
    {
        Id recid = Schema.SObjectType.Library__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }

    @AuraEnabled
    public static List<Library__c> getCBAsOfProject(ID id) 
    {
    
        return [SELECT Id,Name,dt_submittal_date__c,lr_project__c, RecordType.Name
                FROM Library__c
                WHERE lr_project__c = :id
                ];
          
    }
    
    @AuraEnabled
    public static List<Library__c> getCBAsOfProjectwRecordType(ID id, String recordTypeLabel) 
    {
         if( recordTypeLabel != 'All')
         {
             return [SELECT Id,Name,dt_submittal_date__c,lr_project__c, RecordType.Name
                FROM Library__c
                WHERE lr_project__c = :id and RecordType.Name in (:recordTypeLabel)
                ];
         }
        else
        {
            return [SELECT Id,Name,dt_submittal_date__c,lr_project__c, RecordType.Name
                FROM Library__c
                WHERE lr_project__c = :id
                ];
        }
    }
   
    
        
         

    
    
    }