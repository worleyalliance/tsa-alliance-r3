/**************************************************************************************
Name: Partner_TriggerHandler
Version: 1.0
Created Date: 05/03/2018
Function: Handler for Partner Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    05/03/2018      Original Version
*************************************************************************************/

public with sharing class Partner_TriggerHandler {
private static Partner_TriggerHandler handler;
    
    /*
    * Singleton like pattern
    */
    public static Partner_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Partner_TriggerHandler();
        }
        return handler;
    }
    
    // insert and update actions are handled in Process Builder - Auto Populate Teammates/Subcontractors on Opportunity
	 /*
    * Actions performed on Partner records after insert
    */
    public void onAfterInsert(List<Partner__c > newPartner){
        recalculateOppPartnerFields(newPartner);
    }
    
    /*
    * Actions performed on Partner records after update
    */
    public void onAfterUpdate(Map<Id,Partner__c > newPartner,Map<Id,Partner__c > oldPartner) {
        List<Partner__c > lstPartners = new List <Partner__c>();  
        List<Partner__c > lstOldPartners = new List <Partner__c>();  
        for(Partner__c partner :newPartner.values()){
            if(partner.Role_New__c =='Subcontractor' || partner.Role_New__c =='Teammate'){
                lstPartners.add(partner);
            }
        }
        for(Partner__c partner :oldPartner.values()){
            if(oldPartner.get(partner.ID).Role_New__c =='Subcontractor' ||
               oldPartner.get(partner.ID).Role_New__c =='Teammate'){
                lstPartners.add(partner);
            }
        }
        system.debug('lstPartners'+lstPartners);
        resetOppPartnerFields(lstPartners);
    }
    /*
    * Actions performed on Partner records before delete
    */
    public void onAfterDelete(List<Partner__c > oldPartner) {
        List<Partner__c > lstPartners = new List <Partner__c>();
        for(Partner__c partner :oldPartner){
            if(partner.Role_New__c =='Subcontractor' ||
               partner.Role_New__c =='Teammate'){
               lstPartners.add(partner);
            }
        }
        system.debug('lstPartners'+lstPartners);
        resetOppPartnerFields(lstPartners);
    }
    
    public void recalculateOppPartnerFields(List<Partner__c > newPartner){
        List<ID> partnerIDs = new List<ID>();
        List<ID> opportunityIDs = new List<ID>();
        Map<ID,Set<String>> mapSubcontractors = new Map<ID,Set<String>>();
        Map<ID,Set<String>> mapTeammates = new Map<ID,Set<String>>();
       
        for(Partner__c partner :newPartner){
            {
                partnerIDs.add(partner.ID); 
                opportunityIDs.add(partner.Opportunity__c);
            }
        }
        //Query to get opp and account details
        Map<ID, Partner__c> mapIDPartner = new Map<ID, Partner__c>([Select ID, Role_New__c, Opportunity__r.Teammates__c, Opportunity__r.Subcontractors__c,Partner__r.Name
                                                                           from Partner__c
                                                                           where Opportunity__c in :opportunityIDs]);                                                                                                          
        system.debug('partnerIDs'+mapIDPartner);
        for(Partner__c partner : mapIDPartner.values()){
            // for Subcontractors
            if(partner.Role_New__c == 'Subcontractor'){
                system.debug('Test 1');
                if(mapSubcontractors.containsKey(partner.Opportunity__c)) {
                    Set<String> subcontractors = mapSubcontractors.get(partner.Opportunity__c);
                    subcontractors.add(partner.Partner__r.Name);
                    mapSubcontractors.put(partner.Opportunity__c, subcontractors);
                    system.debug('Test 1 if');
                } else {
                    system.debug('Test 1 else');
                    mapSubcontractors.put(partner.Opportunity__c, new Set<String> { partner.Partner__r.Name });
                }    
            }
            // for Teammate
            if(partner.Role_New__c == 'Teammate'){ 
                
                if(mapTeammates.containsKey(partner.Opportunity__c)) {
                    Set<String> teamMates = mapTeammates.get(partner.Opportunity__c);
                    teamMates.add(partner.Partner__r.Name);
                    mapTeammates.put(partner.Opportunity__c, teamMates);
                } else {
                    mapTeammates.put(partner.Opportunity__c, new Set<String> { partner.Partner__r.Name });
                }
            }
        }
        system.debug('mapSubcontractors '+mapSubcontractors);
        system.debug('mapTeammates '+mapTeammates);
        
        // Calling update method
        updateOpportunity(mapSubcontractors,'Subcontractors');
        updateOpportunity(mapTeammates,'Teammates');
        
    }
    public void updateOpportunity(Map<ID,Set<String>> mapToProcess,String fieldToUpdate){
        List<Opportunity> updateOppList = new List<Opportunity>();
        for(ID oppId :mapToProcess.keyset()){
            Opportunity opp = new opportunity();
            opp.Id = oppId;
            String partnerName = '';
            for(String s:maptoProcess.get(oppId)) {
                if((partnerName.length() + s.length()) < 255)
                	partnerName += (partnerName==''?'':',')+s;
                else{
                   partnerName += '..';  
                }
            }
            if(fieldToUpdate =='Teammates'){
                opp.Teammates__c = partnerName;
            }else if(fieldToUpdate =='Subcontractors'){
                opp.Subcontractors__c = partnerName;
            }
            updateOppList.add(opp);
        }
        
        if(updateOppList !=null){
            try{
                update updateOppList;
            }catch(Exception e){
                system.debug('Exception '+e);
            }
        } 
    }
    public void resetOppPartnerFields(List<Partner__c > newPartner){
        List<Opportunity> resetOpp = new List<Opportunity>();
        for(Partner__c partner : newPartner){
                Opportunity opp = new Opportunity();
                opp.ID= partner.Opportunity__c;
                opp.Teammates__c ='';
                opp.Subcontractors__c='';
                if(!resetOpp.contains(opp))
                    resetOpp.add(opp);
            }
        system.debug('Delete list in Reset method'+resetOpp);
        if(resetOpp !=null){
            try{
            	update resetOpp;
            }catch(Exception e){
                system.debug('Exception '+e);
            }
        }
        // recalculating Partner values        
        recalculateOppPartnerFields(newPartner);
    }
}