/**************************************************************************************
Name:OpportunitySpreadCalculation
Version: 1.0 
Created Date: 09/13/2018
Function: Class to calculate spread when Opportunity values change
Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty   09/13/2018      Original Version     
*************************************************************************************/

public with sharing class OpportunitySpreadCalculation {

  @InvocableMethod (label='Calculate Spread On Opp Changes' description='Calculates spread for all MOS of Opportunities')
  public static void calculateSpreadForOpportunity(List<Id> opportunityIDList){
    List<Multi_Office_Split__c> multiOfficeSplits = [Select Id,
                                                            Spreading_Formula__c,
                                                            Start_Date__c,
                                                            End_Date__c,
                                                            Revenue__c,
                                                            Number_of_Days__c,
                                                            Gross_Margin__c,
                                                            Probable_GM__c
                                                    From Multi_Office_Split__c
                                                    Where Opportunity__c in :opportunityIDList];      
      ForecastSpread_Service.calculateSpread(multiOfficeSplits);
  }
  
}