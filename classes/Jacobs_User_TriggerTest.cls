/**************************************************************************************
Name: Jacobs_Project_TriggerTest
Version: 1.0 
Created Date: 04.20.2017
Function: Jacobs Project Trigger Test Class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Ray Zhao          03/10/2018      Lost Original, Making a new test class
*************************************************************************************/
@isTest
public class Jacobs_User_TriggerTest {

    @testSetup
    static void setupTestData(){
        Id acctRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The AccountRecordTypeId: ' + acctRecordTypeId);
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Resource Contact').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The ContactRecordTypeId: ' + conRecordTypeId);
        Id lrRecordTypeId1 = Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('New License Request').getRecordTypeId();
        System.Debug(LoggingLevel.INFO,'The LicenceRequestRecordTypeId1: ' + lrRecordTypeId1);
        Account tAcct = new Account(Name='TestAccount', RecordTypeId=acctRecordTypeId);
        Insert tAcct;
        System.assertNotEquals(tAcct.Id, null);
        Contact tCon = new Contact(FirstName='TestFN1', Lastname='TestLN1', Email='tu1@example.com', RecordTypeId = conRecordTypeId, AccountId=tAcct.Id);
        Insert tCon;
        System.assertNotEquals(tCon.Id, null);
        System.Debug(LoggingLevel.INFO,'The Contact Id: ' + tCon.Id);
        Licenses__c tLic = new Licenses__c(Name='TestLic1', License_Type__c='Full', Line_of_Business__c='NA', Business_Unit__c='NA', Number_of_Licenses_Allocated__c=10);
        Insert tLic;
        System.assertEquals(10, tLic.Number_of_Licenses_Allocated__c);
        System.Debug(LoggingLevel.INFO,'The License Id: ' + tLic.Id);
        License_Request__c tLicReq = new License_Request__c(RecordTypeId = lrRecordTypeId1,
                                                            License_Type_Requested__c='Full', 
                                                            Requested_For__c = tCon.Id,
                                                            Line_of_Business__c = 'NA',
                                                            Business_Unit__c = 'NA',
                                                            Selling_Unit__c = 'Sales Center of Excellence',
                                                            License__c = tLic.Id,
                                                            Requested_Profile__c = 'Sales Super User',
                                                            Requested_Roles__c = SObjectType.License_Request__c.Fields.Requested_Roles__c.PicklistValues[0].getValue());
        Insert tLicReq;
        System.assertNotEquals(tLic.Id, null);
        System.Debug(LoggingLevel.INFO,'The License Request Id: ' + tLicReq.Id);
    }

    @isTest 
	static void insertActiveUser() {
        // Implement test code
        User tu1 = new User(ProfileId = UserInfo.getProfileId(),
                          LastName = 'TestLN1',
                          Email = 'tu1@example.com',
                          Username = 'tu1@licensetracker.test.com' + System.currentTimeMillis(),
                          Alias = 'tu1alias',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          IsActive = TRUE);    
        Test.startTest();
        Insert tu1;
        System.assertNotEquals(tu1.Id, null);
        Test.stopTest();
        Licenses__c[] tLicList = [SELECT No_of_Licenses_In_Use__c FROM Licenses__c WHERE Name = 'TestLic1'];
        System.assertEquals(1, tLicList.size());
        System.assertEquals(1, tLicList[0].No_of_Licenses_In_Use__c);
    }    

    @isTest 
	static void insertUserThenDeactivate() {
        // Implement test code
        User tu1 = new User(ProfileId = UserInfo.getProfileId(),
            			  //ProfileId = '00e1U0000018Hjz',
                          LastName = 'TestLN1',
                          Email = 'tu1@example.com',
                          Username = 'tu1@licensetracker.test.com' + System.currentTimeMillis(),
                          Alias = 'tu1alias',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          IsActive = TRUE);    
        Test.startTest();
        Insert tu1;
        System.assertNotEquals(tu1.Id, null);
        tu1.IsActive=FALSE;
        Update tu1;
        Test.stopTest();
        Licenses__c[] tLicList = [SELECT No_of_Licenses_In_Use__c FROM Licenses__c WHERE Name = 'TestLic1'];
        System.assertEquals(1, tLicList.size());
        System.assertEquals(0, tLicList[0].No_of_Licenses_In_Use__c);
    }    
}