/**************************************************************************************
Name: JEG_DataTable_C
Version: 1.0 
Created Date: 07.03.2017
Function: Table component to display group attrition object elements for given group id.
            COMPONENT WORKS ONLY WITH AT_SegmentAttritionFiscalYearSelector component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.03.2017         Original Version
*************************************************************************************/
public with sharing class JEG_DataTable_C {

    private static DataTable_Service service = new DataTable_Service();
    
   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    @AuraEnabled
    public static DataTable_Service.TableData getData(
            String fieldSetName,
            Id recordId,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault,
            String additionalValueField
    ) {
        return service.getData(
                fieldSetName,
                recordId,
                relationField,
                sObjectName,
                totalRowPresent,
                isSumDefault,
                additionalValueField
        );
    }
}