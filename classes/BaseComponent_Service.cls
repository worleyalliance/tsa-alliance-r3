/**************************************************************************************
Name: BaseComponent_Service
Version: 1.0 
Created Date: 12.04.2017
Function: Set of utils and data models used across various components

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04/12/2017      Original Version
* Pradeep Shetty    03/27/2017      US1641: Added method for getting field helptext   
* Pradeep Shetty    09/02/2018      US2327: Added method for getting table column headers
*************************************************************************************/
public abstract class BaseComponent_Service {

    protected List<PicklistValue> getPicklistValues(String objectName, String fieldName){
        List<PicklistValue> options = new List<PicklistValue>();
        Schema.DescribeFieldResult fieldResult =
                Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            options.add(new PicklistValue(f.getLabel(), f.getValue()));
        }
        return options;
    }

    public class PicklistValue{
        @AuraEnabled
        public String label {get; set;}

        @AuraEnabled
        public String value {get; set;}

        public PicklistValue(String label, String value){
            this.value = value;
            this.label = label;
        }
    }

    public class CRUDpermissions{
        @AuraEnabled
        public Boolean hasRead {get; set;}

        @AuraEnabled
        public Boolean hasUpdate {get; set;}

        @AuraEnabled
        public Boolean hasCreate {get; set;}

        @AuraEnabled
        public Boolean hasDelete {get; set;}

        public CRUDpermissions(
                Boolean hasRead,
                Boolean hasUpdate,
                Boolean hasCreate,
                Boolean hasDelete
        ){
            this.hasRead = hasRead;
            this.hasUpdate = hasUpdate;
            this.hasCreate = hasCreate;
            this.hasDelete = hasDelete;
        }
    }

    public static CRUDpermissions getUserPermissionsForObject(String objectName){
        CRUDpermissions permissions = new CRUDpermissions(
                isObjectAccessible(objectName),
                isObjectUpdatable(objectName),
                isObjectCreateable(objectName),
                isObjectDeletable(objectName)
        );
        return permissions;
    }

    public static void validateObjectUpdateable(String objectName){
        if(!isObjectUpdatable(objectName)){
            throw new AuraHandledException(Label.SecurityError + objectName);
        }
    }

    public static void validateObjectDeletable(String objectName){
        if(!isObjectDeletable(objectName)){
            throw new AuraHandledException(Label.SecurityError + objectName);
        }
    }

    public static  void validateObjectCreateable(String objectName){
        if(!isObjectCreateable(objectName)){
            throw new AuraHandledException(Label.SecurityError + objectName);
        }
    }

    public static void validateObjectAccesible(String objectName){
        if(!isObjectAccessible(objectName)){
            throw new AuraHandledException(Label.SecurityError + objectName);
        }
    }

    public static Boolean isObjectUpdatable(String objectName){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().isUpdateable();
    }

    public static Boolean isObjectDeletable(String objectName){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().isDeletable();
    }

    public static  Boolean isObjectCreateable(String objectName){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().isCreateable();
    }

    public static  Boolean isObjectAccessible(String objectName){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().isAccessible();
    }

    /*
    * US1641: Generic method to return a map of field names and their helptext
    */
    public static Map<String, String> getHelpText(String objectAPIName, String fieldSetName){

        Map<String, String> fieldHelpTextMap = new Map<String, String>();

        DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectAPIName).getDescribe();

        if(String.isNotBlank(fieldSetName)){
            //Get the list of fields for the given object and fieldset
            for(Schema.FieldSetMember fsm: Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fieldsets.getMap().get(fieldSetName).fields){
                fieldHelpTextMap.put(Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fsm.getFieldPath()).getDescribe().getName(), 
                                     Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fsm.getFieldPath()).getDescribe().getInlineHelpText());
            }
        }
        else{
            //Get the list of fields for the given object
            for(String fieldName: Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().keyset()){
                fieldHelpTextMap.put(Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fieldName).getDescribe().getName(), 
                                     Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fieldName).getDescribe().getInlineHelpText());
            }

        }

        return fieldHelpTextMap;
    }

    /*
    * US1641: Generic method to return a map of field names and their Label
    */
    public static Map<String, String> getFieldLabel(String objectAPIName, String fieldSetName){

        Map<String, String> fieldHelpTextMap = new Map<String, String>();

        DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectAPIName).getDescribe();

        if(String.isNotBlank(fieldSetName)){
            //Get the list of fields for the given object and fieldset
            for(Schema.FieldSetMember fsm: Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fieldsets.getMap().get(fieldSetName).fields){
                fieldHelpTextMap.put(Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fsm.getFieldPath()).getDescribe().getName(), 
                                     Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fsm.getFieldPath()).getDescribe().getLabel());
            }
        }
        else{
            //Get the list of fields for the given object
            for(String fieldName: Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().keyset()){
                fieldHelpTextMap.put(Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fieldName).getDescribe().getName(), 
                                     Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel());
        }

        }

        return fieldHelpTextMap;
    }

    /*US2327: Method to get the tabel column headers for a given object with a fieldset*/
    public static List<DataTableColumn> getColumnHeaders(String strObjectName, String strFieldSetName){                
        
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //List holding table column headers
        List<DataTableColumn> ldtColumnsList = new List<DataTableColumn>();
        
        for( Schema.FieldSetMember currentFSM : fieldSetObj.getFields() ){
            //Get the column data type
            String dataType = String.valueOf(currentFSM.getType()).toLowerCase();
            //Schema object data type does not exactly match lightning:datatabl component's allowed values. 
            //Change it to match the value
            if(dataType == 'datetime' || dataType == 'date'){
                dataType = 'date-local';
            }
            else if(dataType == 'double'){
                dataType = 'number';
            }            
            //Create a wrapper instance and store label, fieldname and type.
            DataTableColumn datacolumns = new DataTableColumn( String.valueOf(currentFSM.getLabel()) , 
                                                               String.valueOf(currentFSM.getFieldPath()), 
                                                               String.valueOf(dataType ));
            ldtColumnsList.add(datacolumns);
        }
        
        return ldtColumnsList;
    }

    //US2327: Wrapper class to hold Columns with headers
    public class DataTableColumn {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumn(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }

}