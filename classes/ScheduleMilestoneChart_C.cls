/**************************************************************************************
Name: ScheduleMilestoneChart_C
Version: 1.0 
Created Date: 16.03.2017
Function: Controller data supplier for ScheduleMilestone Lightning Component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   16.03.2017         Original Version
* Chris Cornejo     9/5/2018           US2404 - Schedule Milestone: Visual Status on Chart
* Blake Poutra		2/25/2019		   US2868 - Recreate Milestone Chart
* Scott Stone		3/20/2019		   DE928  - Group Client Visit Milestones on the same line and label them 'Call'
*************************************************************************************/
public with sharing class ScheduleMilestoneChart_C {
    
    private static ScheduleMilestoneChart_Service service = new ScheduleMilestoneChart_Service();
    
    /*
* Method retrieves Milestone records for opportunity or account by deliverd record Id
* Retrieved data is wrapped with custom class
* Status will represent color on the chart and is set based on length of milestone
*/
    @AuraEnabled
    public static List<ScheduleMilestoneChart_Service.Task> getMilestones(
        Id parentId,
        String objectTypeName,
        String relationFieldName,
        String startDateFieldName,
        String endDateFieldName,
        String typeFieldName,
        //US2404 - pull status
        String statusFieldName
    ){
        return service.getMilestones(
            parentId,
            objectTypeName,
            relationFieldName,
            startDateFieldName,
            endDateFieldName,
            typeFieldName,
            //US2404 - return status
            statusFieldName
        );
    }
    
    // New version of get milestones for new version of chart
    // This version gets the milestones just like before, but
    // passes back to the Lighting Component a Map (JSON) of data
    // for the Lightning Component to render
    @AuraEnabled
    public static Map<String, List<Object>> newGetMilestones(Id parentId, 
                                                             String scale, 
                                                             String objectName, 
                                                             String relationFieldName,
                                                             String startDateFieldName,
                                                             String endDateFieldName,
                                                             String typeFieldName){
        
        System.debug('scale: ' + scale);
        
        // Get list of Milestones
        List<ScheduleMilestoneChart_Service.Task> scheduleTaskList = new List<ScheduleMilestoneChart_Service.Task>();
        scheduleTaskList = service.getMilestones(
            parentId,
            objectName,
            relationFieldName,
            startDateFieldName,
            endDateFieldName,
            typeFieldName,
            'Status__c');
        System.debug('scheduleTaskList: ' + scheduleTaskList);
        if(scheduleTaskList.size() > 0){
            // Setup return Map
            Map<String, List<Object>> returnMap = new Map<String, List<Object>>();
            
            // Find the lowest and highest date for the date range.
            Date minDate = scheduleTaskList[0].milestones[0].startDate;
            Date maxDate = scheduleTaskList[0].milestones[0].endDate;
            for(ScheduleMilestoneChart_Service.Task t : scheduleTaskList){
                for(ScheduleMilestoneChart_Service.Milestone m : t.milestones){
                	if(m.endDate > maxDate){
                    	maxDate = m.endDate;
                    }
                    if(m.startDate < minDate){
                        minDate = m.startDate;
                    }    
                }
            }
            // Add a step to each limit, for chart padding
            // Step determined by scale
            Switch on scale{
                when 'Day'{
                    minDate = minDate.addDays(-1);
                    maxDate = maxDate.addDays(1);
                }
                when 'Week'{
                    minDate = minDate.addDays(-7);
                    maxDate = maxDate.addDays(7);
                }
                when 'Month'{
                    minDate = minDate.addMonths(-1);
                    maxDate = Date.newInstance(maxDate.year(), maxDate.month(), minDate.day());
                    maxDate = maxDate.addMonths(1);
                }
                when 'Year'{
                    minDate = minDate.addYears(-1);
                    maxDate = Date.newInstance(maxDate.year(), minDate.month(), minDate.day());
                    maxDate = maxDate.addYears(1);
                }
            }
            
            // Create a list of Dates used for the column headers, spacing determined by scale
            List<Date> dateList = new List<Date>();
            Date currentDate = minDate;
            System.debug('minDate: ' + minDate);
            System.debug('maxDate: ' + maxDate);
            System.debug('first currentDate: ' + currentDate);
            while(currentDate <= maxDate){
                
                if(scale == 'week'){
                    dateList.add(currentDate.toStartOfWeek());
                } else {
                    dateList.add(currentDate);
                }
                
                Switch on scale{
                    when 'Day'{
                        currentDate = currentDate.addDays(1);
                    }
                    when 'Week'{
                        currentDate = currentDate.addDays(7);
                    }
                    when 'Month'{
                        currentDate = currentDate.addMonths(1);
                    }
                    when 'Year'{
                        currentDate = currentDate.addYears(1);
                    }
                }
                System.debug('currentDate: ' + currentDate);
                System.debug('maxDate: ' + maxDate);
                System.debug('currentDate <= maxDate: ' + (currentDate <= maxDate));
            }
            System.debug('dateList: ' + dateList);
            
            // Create data portion of return data.  Each element of the list
            // contains a Map(JSON) of date info to display, and a value to test against
            List<Map<String, String>> dates = new List<Map<String, String>>();
            for(Date d : dateList){
                Map<String, String> dateMap = new Map<String, String>();
                
                dateMap.put('value', d.format());
                Switch on scale{
                    when 'Day'{
                        dateMap.put('text', dayDates(d));
                    }
                    when 'Week'{
                        dateMap.put('text', weekDates(d));
                    }
                    when 'Month'{
                        dateMap.put('text', monthDates(d));
                    }
                    when 'Year'{
                        dateMap.put('text', yearDates(d));
                    }
                }
                dates.add(dateMap);
            }
            
            // Iterate through tasks, creating a milestone element
            // Result will be an element for each row, containing row information
            // and a list of display properties for each date.
            List<Map<String, Object>> milestones = new List<Map<String, Object>>();
            for(ScheduleMilestoneChart_Service.Task t : scheduleTaskList){
                // Create new milestone, set taskname, status, statusType, start and end dates
                Map<String, Object> milestone = new Map<String, Object>();
                milestone.put('taskName', t.taskName);
                List<String> dayList = new List<String>();
                System.debug('next Row: ' + t.taskName);
                Integer milestoneIndex = 0;
                Integer dateIndex = 0;
                // Iterate though dates, create display info for each day, and add to list
                while(dateIndex < dateList.size()){
                    
                    Date d = convertDateByScale(dateList[dateIndex], scale);
                    
                    
                    
                    ScheduleMilestoneChart_Service.Milestone m = t.milestones[milestoneIndex];
                    //SWS (DE928) - Since there can be multiple Milestone dates per Task now (such as grouped calls), and the Milestone Dates are sorted smallest to largest,
                    //we grab the next milestone date once we've added the current milestone date.
                    if(milestoneIndex < t.milestones.size()){
                                                
                        milestone.put('status', m.status);
                        milestone.put('statusType', m.statusType);
                        String startDate = m.startDate.format();
                        String endDate = m.endDate.format();
                        milestone.put('startDate', startDate);
                        milestone.put('endDate', endDate);
                        
                        m.startDate = convertDateByScale(m.startDate, scale);
                        m.endDate = convertDateByScale(m.endDate, scale);
                        
                        
                        System.debug('d: ' + d);
                        System.debug('m.startDate: ' + m.startDate);
                        System.debug('m.endDate: ' + m.endDate);
                        
                        // Determine if the current cell should be empty, a diamond, or filled in
                        if(m.startDate == d && m.endDate == d){
                            dayList.add('diamond');
                            System.debug('diamond');
                        } else if(m.startDate <= d && m.endDate >= d && m.startDate != m.endDate){
                            dayList.add('fill');
                            System.debug('fill');
                        } else {
                            dayList.add('nothing');
                            System.debug('nothing');
                        }
                                              
                        //SWS (DE928) - catch the milestones up to the current day.
                        //for example, if there are 3 dates in a month, print the first one and skip the rest until we get to the next month.
                        while(convertDateByScale(m.endDate, scale) <= convertDateByScale(d, scale) && (milestoneIndex < t.milestones.size() -1)){
                            System.debug('-- next milestone --');
                            milestoneIndex++;
                            m = t.milestones[milestoneIndex];
                            system.debug('next milestone - m.endDate: ' + convertDateByScale(m.endDate, scale));
                        }
                        
                    } else {
                        //if we're at the end of the milestone list, the rest of the days have to be nothing.
                        dayList.add('nothing');
                    }
                    dateIndex++;
                }
                
                // Add list to milestone
                milestone.put('dayList', dayList);
                
                // Add milestone to list of milestones
                milestones.add(milestone);
            }
            
            //  Add data to returning Map(JSON).
            returnMap.put('dates', dates);
            returnMap.put('milestones', milestones);
            return returnMap;
        }
        
        Map<String, List<Object>> returnMap = new Map<String, List<Object>>();
        returnMap.put('dates', null);
        returnMap.put('milestones', null);
        return returnMap;
        
    }
    
    private static Date convertDateByScale(Date d, String scale){
        if(scale == 'Week'){
            return d.toStartOfWeek();
        }
        if(scale == 'Month'){
            return d.toStartOfMonth();
        }
        if(scale == 'Year'){
            return Date.newInstance(d.year(), 1, 1);
        }
        return d;
    }
    
    // Return display formatted version of Date for Day scale
    private static String dayDates(Date d){
        return d.format();
    }
    
    // Return display formatted version of Date for Week scale
    private static String weekDates(Date d){
        return d.format();
    }
    
    // Return display formatted version of Date for Month scale
    private static String monthDates(Date d){
        
        String month = getMonthName(d);
        return (month + ' ' + d.year());
    }
    
    // Return display formatted version of Date for Year scale
    private static String yearDates(Date d){
        return String.valueOf(d.year());
    }
    
    // Take in month number, return String of month name
    private static String getMonthName(Date d){
        Switch on d.month(){
            when 1{
                return 'January';
            }
            when 2{
                return 'February';
            }
            when 3{
                return 'March';
            }
            when 4{
                return 'April';
            }
            when 5{
                return 'May';
            }
            when 6{
                return 'June';
            }
            when 7{
                return 'July';
            }
            when 8{
                return 'August';
            }
            when 9{
                return 'September';
            }
            when 10{
                return 'October';
            }
            when 11{
                return 'November';
            }
            when 12{
                return 'December';
            }
        }
        return null;
    }
    
}