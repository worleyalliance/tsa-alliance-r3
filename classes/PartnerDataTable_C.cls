/**************************************************************************************
Name: PartnerDataTable_C
Version: 1.0 
Created Date: 05.05.2017
Function: Controller of PartnerDataTable for Review page component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
public with sharing class PartnerDataTable_C {
    private static PartnerDataTable_Service service = new PartnerDataTable_Service();

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    @AuraEnabled
    public static DataTable_Service.TableData getData(
            String fieldSetName,
            Id recordId
    ) {
        return service.getPartnerData(
                fieldSetName,
                recordId
        );
    }
}