/**************************************************************************************
Name: Partner_TriggerTest
Version: 1.0
Created Date: 15/05/2018
Function: Test class for Partner_TriggerTest

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer           Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Jignesh Suvarna    15/05/2018      Original Version
* Pradeep Shetty     09/25/2018      DE713: Updated to remove compilation errors after changing Opportunity Builder class
*************************************************************************************/
@IsTest
public class Partner_TriggerTest {
//Constants
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME1 = 'teamMemberTriggerTest1@jacobstestuser.net';
    private static final String USERNAME2 = 'teamMemberTriggerTest2@jacobstestuser.net';
    private static final String USERNAME3 = 'teamMemberTriggerTest3@jacobstestuser.net';
    private static final String USERNAME4 = 'teamMemberTriggerTest4@jacobstestuser.net';
    private static final String COUNTRY1 = 'US';
    private static final String COUNTRY2 = 'CA';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        
      //Get user builder
      TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();

      //List for user creation
      List<User> testUserList = new List<User>();

      //Create test user records. These are not saved as yet.
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME1).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME2).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME3).getRecord());
      testUserList.add(userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME4).getRecord());
      
      //Insert Users
      insert testUserList;          
        
      //Create test records
      
      system.runAs(testUserList[0]){    
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
        TestHelper.OpportunityTeamMemberBuilder otmBuilder = new TestHelper.OpportunityTeamMemberBuilder();
        TestHelper.PartnerBuilder partnerBuilder = new TestHelper.PartnerBuilder();
        
        Account acc = accountBuilder.build().save().getRecord();
        Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
        OpportunityTeamMember otm = otmBuilder.build().withOpportunity(opp.Id).withUser(testUserList[1].Id).getRecord();
        Partner__c partner = partnerBuilder.build().withOpportunity(opp.Id).withPartner(opp.AccountId).withTeammate().save().getRecord();
        Partner__c partner2 = partnerBuilder.build().withOpportunity(opp.Id).withPartner(opp.AccountId).withSubcontractor().save().getRecord();
      }
    }

    @isTest
    private static void testInsertPartner() {
      //Get the test running user
      User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1];

      TestHelper.PartnerBuilder partnerBuilder = new TestHelper.PartnerBuilder();      

      Test.startTest();
      System.runAs(testUser){
        Opportunity opp = [SELECT Id, 
                                  AccountId, 
                                  Teammates__c, 
                                  Subcontractors__c 
                           FROM Opportunity 
                           LIMIT 1];
        Account acc = [SELECT Id,
                              Name 
                       FROM Account 
                       LIMIT 1];
        
        // create new Partner records
        Partner__c partner = partnerBuilder.build().withOpportunity(opp.Id).withPartner(opp.AccountId).withTeammate().save().getRecord();
        Partner__c partner2 = partnerBuilder.build().withOpportunity(opp.Id).withPartner(opp.AccountId).withSubcontractor().save().getRecord();
        
        Opportunity opp2 = [SELECT Id, 
                                   AccountId, 
                                   Teammates__c, 
                                   Subcontractors__c 
                            FROM Opportunity 
                            LIMIT 1];

        System.assertEquals(opp2.Teammates__c, acc.Name);
      }
      Test.stopTest();
    }
    @isTest
    private static void testUpdatePartner() {
      //Get the test running user
      User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1];    

      Test.startTest();
      System.runAs(testUser){
        Opportunity opp = [SELECT Id, 
                                  AccountId, 
                                  Teammates__c, 
                                  Subcontractors__c 
                           FROM Opportunity 
                           LIMIT 1];
        Account acc = [SELECT Id,
                              Name 
                       FROM Account 
                       LIMIT 1];
        Partner__c partner1 = [SELECT Id, 
                                      Role_New__c 
                               FROM Partner__c 
                               where Role_New__c = 'Teammate' 
                               LIMIT 1];
        
        System.assertEquals(opp.Subcontractors__c, acc.Name);

        // Update Partner records  
        partner1.Role_New__c = 'Subcontractor';
        update partner1;
        
        Opportunity opp2 = [SELECT Id, 
                                   AccountId, 
                                   Teammates__c, 
                                   Subcontractors__c 
                            FROM Opportunity 
                            where Id=:opp.ID 
                            LIMIT 1];

        System.assertEquals(opp2.Subcontractors__c, acc.Name);
        System.assertEquals(opp2.Teammates__c, null);        
      }      
      Test.stopTest(); 
        
    }
    @isTest
    private static void testDeletePartner() {
      //Get the test running user
      User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1];    

      Test.startTest();
      System.runAs(testUser){
        Opportunity opp = [SELECT Id, 
                                  AccountId, 
                                  Teammates__c, 
                                  Subcontractors__c 
                           FROM Opportunity 
                           LIMIT 1];
        Account acc = [SELECT Id,
                              Name 
                       FROM Account 
                       LIMIT 1];
        Partner__c partner1 = [SELECT Id, 
                                      Role_New__c 
                               FROM Partner__c 
                               where Role_New__c = 'Teammate' 
                               LIMIT 1];
        
        System.assertEquals(opp.Subcontractors__c, acc.Name);
        // Delete Partner records  
        delete partner1;
        
        Opportunity opp2 = [SELECT Id, 
                                   AccountId, 
                                   Teammates__c,
                                   Subcontractors__c 
                            FROM Opportunity 
                            where Id=:opp.ID 
                            LIMIT 1];

        System.assertEquals(opp2.Teammates__c, null);        
      }
      Test.stopTest();       
    }
}