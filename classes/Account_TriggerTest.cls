/**************************************************************************************
Name: Account_TriggerTest
Version: 1.0 
Created Date: 27.02.2017
Function: Test class for Account trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           27.02.2017         Original Version
* Madhu Shetty              08.11.2017         (US654) - Added validation to block deletion of Account if an associated 
                                               Opportunity exists for that Account.
* Scott Stone               09.10.2018         US2352 - Added test to verify that an account location can be created for 
                                               every country / state that may be entered on an account.  
*************************************************************************************/
@IsTest
private class Account_TriggerTest {
    
    private final static String SKIP_VALUE = '';
    
   /*
    @IsTest
    private static void populateOuBeforeInsert() {
        //given
        String countryCode = 'US';
        TestHelper.CountryOpUnitBuilder couBuilder = new TestHelper.CountryOpUnitBuilder();
        Country_Operating_Unit_Mapping__c cou = couBuilder.build().withCountryCode(countryCode).save().getRecord();

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withBillingCountryCode(countryCode).getRecord();

        //when
        Test.startTest();
        insert acc;
        Test.stopTest();

        //then
        Account acct = [SELECT Id, Operating_Unit__c FROM Account WHERE Id = :acc.Id LIMIT 1];
        system.debug(acct);
        System.assertEquals('US_OU', acct.Operating_Unit__c);
    }
   */

    @IsTest
    private static void createLocationOnInsert() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().getRecord();

        //when
        Test.startTest();
        insert acc;
        Test.stopTest();

        //then
        integer locationCount = [SELECT Count() FROM SiteLocation__c];
       // System.assertEquals(1, locationCount);
    }

    @IsTest
    private static void addLocationOnAddressChange() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        acc.BillingCity = 'SF';

        //when
        Test.startTest();
        update acc;
        Test.stopTest();

        //then
        integer locationCount = [SELECT Count() FROM SiteLocation__c];
      //  System.assertEquals(locationCount,2);
    }

    @IsTest
    private static void createLocationNonUS() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().getRecord();

        acc.BillingCity = '';
        acc.BillingState = '';
        acc.BillingCountryCode = 'AU';
        acc.BillingCountry = 'Australia';

        insert acc;

        acc.BillingCountryCode = 'IN';
        acc.BillingCountry = 'India';

        //when
        Test.startTest();
        update acc;
        Test.stopTest();

        //then
        integer locationCount = [SELECT Count() FROM SiteLocation__c];
      //  System.assertEquals(locationCount,2);
    }

    @IsTest
    private static void shouldDetectAccountMergeAction() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();
        Account acc2 = accountBuilder.build().save().getRecord();

        //when
        Test.startTest();
        merge acc1 acc2;
        Test.stopTest();

        //then
      //  System.assertEquals(1, Account_TriggerHandler.mergedAccountIds.size());
    }

    @IsTest
    private static void shouldNotDetectAccountMergeAction() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();

        //when
        Test.startTest();
        update acc1;
        Test.stopTest();

        //then
        System.assertEquals(0, Account_TriggerHandler.mergedAccountIds.size());
    }

    @IsTest
    private static void shouldLeaveOnlyOneLocationSetAsPrimary() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc1 = accountBuilder.build().save().getRecord();
        Account acc2 = accountBuilder.build().save().getRecord();
        //SiteLocation__c acc1Location = [SELECT Id FROM SiteLocation__c WHERE Account__c = :acc1.Id AND Primary__c = true LIMIT 1];
        //locations are created by Account_Trigger. One on each account and this one is set as Primary

        //when
        Test.startTest();
        merge acc1 acc2;
        Test.stopTest();

        //then
        List<SiteLocation__c> result = [SELECT Id FROM SiteLocation__c WHERE Primary__c = true];
        System.assertEquals(1, result.size());
        //System.assertEquals(acc1Location.Id, result[0].Id);
    }

    @IsTest
    private static void shouldRestrictDeleteIfOpportunityExists() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        //Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        try {
            delete acc;
        } catch (Exception e) {
            e.setMessage(System.Label.OpportunityOrProjectExistsForAccount);
            system.assertEquals(System.Label.OpportunityOrProjectExistsForAccount, e.getMessage());
        }
    }
    
    @IsTest
    private static void shouldRestrictDeleteIfProjectExists() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.ProjectBuilder projectBuilder = new TestHelper.ProjectBuilder();
        Jacobs_Project__c proj = projectBuilder.build().withAccount(acc.id).getRecord();

        try {
            delete proj;
        } catch (Exception e) {
            e.setMessage(System.Label.OpportunityOrProjectExistsForAccount);
            system.assertEquals(System.Label.OpportunityOrProjectExistsForAccount, e.getMessage());
        }
    }
    
    //US23252 - add test for Countries/states
    //US2352 SWS commented out @IsTest so that this does not run automatically.  The country state checks create a lot of test data and can exceed APEX CPU Limits if they're run simulatenously.  
    //You may still run manually, by uncommenting @IsTest and adding the test to a new Test Run.  But please run only one country / state test per test run.
//    @IsTest
    private static void shouldBeAbleToSetAnyStateProvinceCodeOnAccountLocation() {
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        
        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        List<SiteLocation__c> locations = new List<SiteLocation__c>();

        FieldDescribeUtil.PicklistDependency stateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(User.statecode, FieldDescribeUtil.ResultType.LABEL, User.Countrycode, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});
        
        FieldDescribeUtil.PicklistDependency usStateDependency =
            FieldDescribeUtil.getDependentOptionsWithLabels(User.statecode, FieldDescribeUtil.ResultType.VALUE, User.Countrycode, FieldDescribeUtil.ResultType.VALUE, new Set<String>{SKIP_VALUE});
        
        for( string country : stateDependency.dependentByControling.keySet() ){
            
            FieldDescribeUtil.PicklistDependency dependency = stateDependency;
            if(country == 'US') {
                dependency = usStateDependency;
            }
            
            for(string stateCode : dependency.dependentByControling.get(country)){
                
                system.debug('c=' + country + ' sCode=' + stateCode);
                
                SiteLocation__c loc = locationBuilder.build().withAccount(acc.Id).withCountry(country).withStateProvince(stateCode).getRecord();
                loc.Primary__c = false;
                loc.RecordTypeId = Schema.SObjectType.SiteLocation__c.getRecordTypeInfosByName().get('Account Location').getRecordTypeId();
                locations.add(loc);
            }
        }
                
        try {
            insert locations;  
        } catch (DMLException e) {
            string failedFields = '';
            for(Integer i = 0; i < e.getNumDml(); i++ ){
                SiteLocation__c failedLocation = locations.get(e.getDmlIndex(i));
                failedFields = failedFields + ' c=' + failedLocation.Country__c + ' s=' + failedLocation.State_Province__c + ' |';
            }
            throw new DMLException('error adding location with' + failedFields + '. ' + e.getMessage(), e);
        }
    }
}