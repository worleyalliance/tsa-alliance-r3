/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_CPCTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_CPCTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new CPC__c());
    }
}