/*****************************************************************************************************************
Name: Campaign_TriggerHandler
Version: 1.0
Created Date: 11/21/2017
Function: Handler for Campaign_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    11/21/2017      Original Version
* Madhu & Pradeep 	12/15/2018      US2537:Rearchitect trigger handler for plan week decommissioning changes
*******************************************************************************************************************/
public with sharing class Campaign_TriggerHandler {
    
    private static Campaign_TriggerHandler handler;
    
    //Constant for Selling Unit Sales Plan record type developer name
    private final String REC_TYPE_SU_SALES_PLAN = 'Sales_Plan';

    //Constant for Line Of Business
    private final String LOB_ATN = 'ATN';

    //Variable holding the Selling Unit sales plan record type
    private RecordType suSalesPlanRecordType = [Select Id 
                                                From RecordType 
                                                Where DeveloperName = :REC_TYPE_SU_SALES_PLAN
                                                Limit 1];
                                      
    //Singleton like pattern
    public static Campaign_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Campaign_TriggerHandler();
        }
        return handler;
    }
    
    //Actions performed on Campaign records on after insert
    public void onBeforeInsert(List<Campaign> salesPlans) {
        //Activate sales plan
        for(Campaign salesPlan: salesPlans){
            salesPlan.isActive = true;
        }
    }

    //Actions performed on Campaign records on after insert
    public void onAfterInsert(List<Campaign> salesPlans) {
        //Queue the Opportunity Sales Plan Assignment process
        queueSalesPlanAssignment(salesPlans, null);
    }
    
    //Actions performed on Campaign records on after update
    public void onAfterUpdate(List<Campaign> salesPlans, Map<Id, Campaign> oldsalesPlansByIds) {
        //Queue the Opportunity Sales Plan Assignment process
        queueSalesPlanAssignment(salesPlans, oldsalesPlansByIds);
    }

    //Method to queue up sales plan assignment
    private void queueSalesPlanAssignment(List<Campaign> salesPlans, Map<Id, Campaign> oldsalesPlansByIds){
        //Queue the Opportunity Sales Plan Assignment process
        Id salesPlanAssignmentJobId = System.enqueueJob(new SalesPlanQueueableHandler(salesPlans, oldsalesPlansByIds));

        //Log the job Id
        system.debug('Sales Plan Assignment Job Id: ' + salesPlanAssignmentJobId);
    }
   
    public class CampaignTriggerException extends Exception {}
}