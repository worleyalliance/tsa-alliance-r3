/**************************************************************************************
Name: OCICase_TriggerTest
Version: 1.0 
Created Date: 20.07.2017
Function: OCI Case Trigger handler unit test class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    20.07.2017      Original Version
* Manuel Johnson    12.13.2017      Updated to switch to OCI Case instead of Case object
*************************************************************************************/
@isTest
private class OCICase_TriggerTest {

    @isTest
    private static void shouldUpdateOCICaseStatusToOpen() {
        //given an Opportunity with multiple OCI Cases
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.OCICaseBuilder caseBuilder = new TestHelper.OCICaseBuilder();
        OCI_Case__c cas1 = caseBuilder.build().withOpportunity(opp.Id).save().getRecord();
        OCI_Case__c cas2 = caseBuilder.build().withOpportunity(opp.Id).save().getRecord();
        
        //when the most recent Case is deleted
        Delete cas2;
        system.debug(cas1.status__c);
        //then Opportunity OCI Case Status should be set to 'Open' since another Case exists
        System.assertEquals('Open - Potential Issues', [SELECT OCI_Case_Status__c from Opportunity where Id = :opp.Id].OCI_Case_Status__c);
    }

    @isTest
    private static void shouldUpdateOCICaseStatusToNone() {
        //given an Opportunity with only one OCI Case
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();

        TestHelper.OCICaseBuilder caseBuilder = new TestHelper.OCICaseBuilder();
        OCI_Case__c cas = caseBuilder.build().withOpportunity(opp.Id).save().getRecord();
        
        //when the Case is deleted
        Delete cas;
        
        //then Opportunity OCI Case Status should be set to 'None' since no other Case exists
        System.assertEquals('None', [SELECT OCI_Case_Status__c from Opportunity where Id = :opp.Id].OCI_Case_Status__c);
    }
}