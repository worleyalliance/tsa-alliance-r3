/**************************************************************************************
Name:ldtLibraryListViewControllers
Version: 
Created Date: 
Function: LDT table for Projects for Library records

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Madhu shetty      01-Jul-2018     Original Version
* Chris Cornejo     22-Aug-2018     DE637 - Error found in US623: ATEN: Issues for Show Record of Type Field on new Library component for Closed won and Oppty not closed
* Chris Cornejo     23-Aug 2018     DE615 - Lightning Data Table - Success Story Consolidation
* Chris Cornejo     02-Oct-2018     US1890 - ATEN: (LDT) Lightning Data Table - Display number of files and allow Download All
*************************************************************************************/

public class ldtLibraryListViewController {
    
    public static Map<Id, String> recordtypemap {get;set;}
    
    //Function returns the recordtypes for the filter and New record creation Dropdown.
    @AuraEnabled
    public static List<RecordType> fetchRecordTypeValues(ID iRecordID, string sObjectName, string RecordtypeFor)
    {
        List<Schema.RecordTypeInfo> lstlibRecordTypeInfos = Library__c.SObjectType.getDescribe().getRecordTypeInfos();    
        
        String strrecordType = getObjectRecTypeDevName(iRecordID, sObjectName);
        
        if (strrecordType == null)
            strrecordType = 'None';
            
            system.debug('strrecordType = ' + strrecordType );
        
        //Get list of all library record types available for the current object
        List <LibraryListviewConfiguration__mdt> ldtRecTypeAPINames;
        if(RecordtypeFor == 'FILTER'){
            ldtRecTypeAPINames = [Select Library_RecType_API__c from LibraryListviewConfiguration__mdt 
                      where Obj_API__c =: sObjectName 
                      and Obj_RecType_API__c =: strrecordType
                      and For_Filter__c = true];
        }
        else if(RecordtypeFor == 'NEW'){
            ldtRecTypeAPINames = [Select Library_RecType_API__c from LibraryListviewConfiguration__mdt 
                      where Obj_API__c =: sObjectName 
                      and Obj_RecType_API__c =: strrecordType
                      and For_New__c = true];
        }
        
        system.debug('ldtRecTypeAPINames =' + ldtRecTypeAPINames );
        
        list <string> lstlibRecTypeAPINames = new list<string>();
        for(LibraryListviewConfiguration__mdt ldtRecType : ldtRecTypeAPINames){
            lstlibRecTypeAPINames.add(ldtRecType.Library_RecType_API__c);
        }
        
        //Get list of matching recordtype objects
        List<RecordType> lstrecordTypeList = new List<RecordType>();  
        lstrecordTypeList = [Select Name, 
                          Id,
                          Description,
                          DeveloperName 
                          From RecordType 
                          Where SobjectType = 'Library__c' 
                          And DeveloperName in :lstlibRecTypeAPINames
                          Order by Name desc];
                          
         system.debug('lstrecordTypeList = ' + lstrecordTypeList );
        
        //Retrieve list of recordtypes accessible to the user for record creation
        List<RecordType> lstLibRecordTypes = new List<RecordType>();  
        for(RecordTypeInfo lstlibRecordTypeInfo : lstlibRecordTypeInfos)
        {
            for (RecordType lstrecordType: lstrecordTypeList)
            {
                system.debug('lstlibRecordTypeInfo.getDeveloperName() = ' + lstlibRecordTypeInfo.getDeveloperName());
                system.debug('lstrecordType.DeveloperName = ' + lstrecordType.DeveloperName);
                system.debug('lstlibRecordTypeInfo.isAvailable() = ' + lstlibRecordTypeInfo.isAvailable());
                if(lstlibRecordTypeInfo.getDeveloperName() == lstrecordType.DeveloperName && lstlibRecordTypeInfo.isAvailable()){
                    lstLibRecordTypes.add(lstrecordType);
                }
            }
        }
        
        system.debug('lstLibRecordTypes' + lstLibRecordTypes);
        
        return lstLibRecordTypes;
    }
    
    //Function for fetching the record type api name of the record for which we are showing the library records.
    private static string getObjectRecTypeDevName(ID iRecordID, string sObjectName){
        String queryString;
        string strRecordType;
        
        /*try{
            // Dynamic SOQL
            queryString = 'SELECT Recordtypeid FROM ' + sObjectName + ' WHERE Id =: iRecordID LIMIT 1';
            
            SObject dynamicSObj = Database.query(queryString);
            strRecordType = iRecordID.getSObjectType().getDescribe().getRecordTypeInfosById().get((string)dynamicSObj.get('Recordtypeid')).getDeveloperName();
        }
        catch(Exception e){
            strRecordType = Null;
        }
        */
        if (sObjectName <> 'AT_Group__c')
        {
            // Dynamic SOQL
            queryString = 'SELECT Recordtypeid FROM ' + sObjectName + ' WHERE Id =: iRecordID LIMIT 1';
            
            SObject dynamicSObj = Database.query(queryString);
            strRecordType = iRecordID.getSObjectType().getDescribe().getRecordTypeInfosById().get((string)dynamicSObj.get('Recordtypeid')).getDeveloperName();
        }
        else
            strRecordType = Null;
        
        return strRecordType;
    }
    
    //Function for fetching the recordtypeid of the Library record type for which the new record is being created
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel)
    {
        Id rectypeid = Schema.SObjectType.Library__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return rectypeid;
    }
    
    //Function for fetching the library records of the current object
    @AuraEnabled
    public static List<Library__c> getLibraryRecords(ID iRecordID, String recordTypeLabel, string sLookupFieldName) 
    {
        list<Library__c> libRecords;
        
        //DE615 - If Success Stories, consolidated all Success Stories record types to display
        if (recordTypeLabel == 'Success_Stories')
            recordTypeLabel = '\'Success_Stories_Continuous_Imp\', \'success_stories_value_sustainability\', \'Success_Story\', \'Success_Stories\'';
        else if (recordTypeLabel <> 'All')
            recordTypeLabel = '\'' + recordTypeLabel + '\'';
        //DE637 - Modified query to match RecordType.DeveloperName instead of RecordType.Name
        //US1890 - Adding num_count_of_related_files__c field to query for Files Count
        String queryString = 'SELECT Id, Name, dt_submittal_date__c, RecordType.Name, num_count_of_related_files__c '
                            + 'FROM Library__c WHERE ' + sLookupFieldName + ' = :iRecordID'
                            + ((recordTypeLabel == 'All') ? '' : ' and RecordType.DeveloperName in (' + recordTypeLabel + ')' );
        
        system.debug('recordTypeLabel = ' + recordTypeLabel );                    
        system.debug('queryString = ' + queryString );
        libRecords = Database.query(queryString);
        
        return libRecords;
    }
        //US1890 - Concatenate all related files on a library record
        @AuraEnabled
        public static String getdownloadFiles(String libId){
        
         List<ContentDocumentLink> CLList = new List<ContentDocumentLink>();  
         CLList = [Select ContentDocumentId
                          From ContentDocumentLink 
                          Where LinkedEntityId = :libId];
        
        String IdList;
        IdList = ''; 
        
        for( ContentDocumentLink CL: CLList)
        {
            IdList = IdList + '/';
            IdList = IdList + CL.ContentDocumentId;
        }
        
        return IdList;
    }
 }