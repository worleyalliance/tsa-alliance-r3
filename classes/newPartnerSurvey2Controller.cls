/**************************************************************************************
Name: newPartnerSurvey2Controller
Version:
Created Date: 8/4/2017
Function: Apex Class to update CAS Survey

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo        8/4/2017    Apex Class to update CAS Survey
*************************************************************************************/  
    
public class newPartnerSurvey2Controller {
   Partner_Survey2__c partnerSurvey2;
   public Partner_Survey2__c currentRecord{get; set;}

   
    public newPartnerSurvey2Controller(ApexPages.StandardController controller) {
    //this.controller = controller;
    this.partnerSurvey2 = (Partner_Survey2__c)controller.getRecord();
    
   //currentRecord = [SELECT Id, Name, lr_partner__c FROM Partner_Survey2__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
   currentRecord = [SELECT Id, Name FROM Partner_Survey2__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }


   // These four class variables maintain the state of the wizard.  
    
   // When users enter data into the wizard, their input is stored  
    
   // in these variables.  
    


   // The next four methods return one of each of the four class  
    
   // variables. If this is the first time the method is called,  
    
   // it creates an empty record for the variable.  
    
   public Partner_Survey2__c getPartnerSurvey2() {
      if(partnerSurvey2 == null) partnerSurvey2 = new Partner_Survey2__c();
      return partnerSurvey2;
      }


   // This method performs the final save for all four objects, and  
    
   // then navigates the user to the detail page for the new  
    
   // opportunity.  
    
   public PageReference save() {

      // Create the account. Before inserting, copy the contact's  
    
      // phone number into the account phone number field.  
    
      //account.phone = contact.phone;
      //insert account;
      //partnerSurvey.frm_myaccount__c = ApexPages.currentPage().getParameters().get('id');
      
      String nextPage;
      
      //system.debug('id = ' + partnerSurvey2.id);
      //system.debug('name = ' + partnerSurvey2.name);
      //system.debug('us_dt_accounting_approved__c = ' + partnerSurvey2.us_dt_accounting_approved__c);
      //system.debug('us_lta_accounting_incomplete__c = ' + partnerSurvey2.us_lta_accounting_incomplete__c);
      //system.debug('us_txt_accounting_system__c = ' + partnerSurvey2.us_txt_accounting_system__c);
      //system.debug('us_txt_aco_approver__c = ' + partnerSurvey2.us_txt_aco_approver__c);
      //system.debug('us_lta_true_ups_reason__c = ' + partnerSurvey2.us_lta_true_ups_reason__c);
      
      try{
      update partnerSurvey2;
      
      //currentRecord = [SELECT Id, Name, lr_partner__c, lr_cas_survey__c, us_fm_risk_rating__c FROM Partner_Survey2__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
      currentRecord = [SELECT Id, Name, mdr_partner_survey__c, mdr_partner_survey__r.lr_partner__c FROM Partner_Survey2__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
      
      //system.debug('lr_partner__c = ' + partnerSurvey2.lr_partner__c);
      
      //if (currentRecord.us_fm_risk_rating__c == 'Low Risk')
          //nextPage = currentRecord.lr_partner__c;
      //else
          //nextPage = currentRecord.lr_cas_survey__c; 
          
          nextPage = currentRecord.mdr_partner_survey__r.lr_partner__c; 
          
          system.debug('nextPage =' + nextPage);
    
      PageReference newPartnerSurveyPage = new PageReference('/' +
                                                 nextPage);
                               
      //newPartnerSurveyPage.setRedirect(true);
      
      //system.debug('Old lr_partner__c = ' + currentRecord.lr_cas_survey__c);

      system.debug('New page = ' + newPartnerSurveyPage);
      return newPartnerSurveyPage;
      }
      
      catch(Exception ex){
      ApexPages.addMessages(ex);
      }
      return null;
   }

}