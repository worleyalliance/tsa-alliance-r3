/**************************************************************************************
Name:OpportunityEstimations_Service
Version: 1.0 
Created Date: 12.04.2017
Function: Batch class for converting financial data in opportunities

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           04/12/2017      Original Version
* Pradeep Shetty            10/11/2017      DE270: Disable edits for closed Won Opp
* Jignesh Suvarna			      03/04/2018		  US1922: Factored Revenue/GM Calculation
* Pradeep Shetty            09/13/2018      US2406: Commented out spread recalculation.
                                            It is being triggered as a process builder flow. 
*************************************************************************************/
public class OpportunityEstimations_Service extends BaseComponent_Service{

  /*
   *   Retrieves estimation data for opportunity record
   */
  public Estimation getEstimation(Id recordId){
    Estimation estimationEntry = createEstimationEntry(recordId);
    return estimationEntry;
  }

  /*
   *   Retrieves Forecast picklist values
   */
  public List<PicklistValue> getForecastCategories(){
    return getPicklistValues(Opportunity.sObjectType.getDescribe().getName(),
                             Opportunity.Forecast_Category__c.getDescribe().getName());
  }

  /*
   *   Retrieves Class picklist values
   */
  public List<PicklistValue> getClasses(){
    List<PicklistValue> classes = new List<PicklistValue>();
    classes.add(new PicklistValue('', ''));
    classes.addAll(getPicklistValues(Opportunity.sObjectType.getDescribe().getName(),
                                     Opportunity.Opportunity_Class_A_T__c.getDescribe().getName())
    );
    return classes;
  }

  /*
   *   Returns current user update permission for Opportunity_Class_A_T__c field
   */
  public Boolean isClassFieldEditable(){
    return Opportunity.Opportunity_Class_A_T__c.getDescribe().isUpdateable() || 
           Opportunity.Opportunity_Class_A_T__c.getDescribe().isAccessible();
  }

  /*
   *   Updates estimation data on opportunity
   */
  public Estimation saveEstimations(Id recordId, String estimationJSON){
    validateObjectUpdateable(Opportunity.sObjectType.getDescribe().getName());
    Estimation estimationValues = (Estimation) JSON.deserialize(estimationJSON, Estimation.class);
    Opportunity opp = new Opportunity(Id                          = recordId,
                                      Total_Installed_Cost_TIC__c = estimationValues.tic,
                                      Opportunity_Class_A_T__c    = estimationValues.classAT,
                                      Go__c                       = estimationValues.go,
                                      Get__c                      = estimationValues.get,
                                      Forecast_Category__c        = estimationValues.forecastCategory);
    try
    {
      update opp;
      //US2406 commented out since this logic is in the Process Builder
      //ForecastSpread_Service.recalculateSpread(new List<Opportunity>{opp});
    } 
    catch (DmlException ex)
    {
      throw new AuraHandledException(ex.getDmlMessage(0));
    }
    Estimation estimationEntry = createEstimationEntry(recordId);
    return estimationEntry;
  }

  private Estimation createEstimationEntry(Id opportunityId){
    validateObjectAccesible(Opportunity.sObjectType.getDescribe().getName());
    List<Opportunity> opportunities = [Select Total_Installed_Cost_TIC__c, 
                                              Go__c, 
                                              Get__c, 
                                              Probability, 
                                              Forecast_Category__c,
                                              Amount,
                                       		  Total_Probable_GM__c,
                                              Opportunity_Class_A_T__c,
                                              IsWon
                                       From Opportunity
                                      Where Id = :opportunityId];
    if(opportunities.isEmpty())
    {
      throw new AuraHandledException(Label.NoRecords);
    }

    Estimation estimationValues = new Estimation();
    estimationValues.tic              = opportunities[0].Total_Installed_Cost_TIC__c;
    estimationValues.go               = opportunities[0].Go__c;
    estimationValues.get              = opportunities[0].Get__c;
    //estimationValues.probability      = opportunities[0].Probability;
    estimationValues.probability      = opportunities[0].Go__c * opportunities[0].Get__c * .01;
    estimationValues.totalGM          = opportunities[0].Amount;
    // US1922: JS replaced ExpectedRevenue with Total_Probable_GM__c 
    estimationValues.totalProbableGM  = opportunities[0].Total_Probable_GM__c;
    estimationValues.forecastCategory = opportunities[0].Forecast_Category__c;
    estimationValues.classAT          = opportunities[0].Opportunity_Class_A_T__c;
    //DE270: Added isWon to determine if the Opportunity was won or not
    estimationValues.isWon            = opportunities[0].IsWon;
    
    return estimationValues;
  }

 /*
  *   Data structure to transpord estimatio data
  */
  public class Estimation {

      @AuraEnabled
      public Decimal tic {get; set;}

      @AuraEnabled
      public Decimal totalGM {get; set;}

      @AuraEnabled
      public Decimal totalProbableGM {get; set;}

      @AuraEnabled
      public Decimal go {get; set;}

      @AuraEnabled
      public Decimal get {get; set;}

      @AuraEnabled
      public Decimal probability {get; set;}

      @AuraEnabled
      public String forecastCategory {get; set;}

      @AuraEnabled
      public String classAT {get; set;}

      //DE270: Adding IsWon Attribute to be sent along with other Opportunity fields
      @AuraEnabled
      public Boolean isWon {get; set;}
  }

}