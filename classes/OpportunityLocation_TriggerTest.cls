/**************************************************************************************
Name: OpportunityLocation_TriggerTest
Version: 1.0 
Created Date: 03.08.2018
Function: To Test OpportunityLocation_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel Johnson    03.08.2018      Original Version
*************************************************************************************/

@IsTest
private class OpportunityLocation_TriggerTest {
    
    //Constants
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
    private static final String USERNAME = 'salesTriggerTest@jacobstestuser.net';
    private static final String COUNTRY1 = 'US';
    private static final String COUNTRY2 = 'CA';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();
        
        //Create test records
        system.runAs(testUser){
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            
            TestHelper.LocationBuilder locBuilder = new TestHelper.LocationBuilder();
            SiteLocation__c loc = locBuilder.build().withAccount(acc.Id).withCountry(COUNTRY1).withStateProvince('').save().getRecord();
            SiteLocation__c loc2 = locBuilder.build().withAccount(acc.Id).withCountry(COUNTRY2).withStateProvince('').save().getRecord();
            
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
        }
    }
    
    @isTest
    private static void testFirstLocationIsPrimary() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SiteLocation__c loc = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY1 LIMIT 1];
        
        System.runAs(testUser){
            
            TestHelper.OppLocationBuilder oppLocationBuilder = new TestHelper.OppLocationBuilder();
            Opportunity_Locations__c oppLocation1 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc.Id).save().getRecord();
        
            // Validate that the first location created is set to primary
            System.assertEquals(true, [SELECT Primary__c FROM Opportunity_Locations__c WHERE Id = :oppLocation1.Id].Primary__c);
            
            // Validate that the parent Opportunity primary field is updated
            Opportunity oppAfter = [SELECT Id, Primary_Country__c FROM Opportunity LIMIT 1];
            System.assertEquals(loc.Country__c, oppAfter.Primary_Country__c);
        }
    }
    
    @isTest
    private static void testFirstLocationStillPrimary() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME];
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        SiteLocation__c loc = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY1 LIMIT 1];
        SiteLocation__c loc2 = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY2 LIMIT 1];
        
        System.runAs(testUser){
            
            TestHelper.OppLocationBuilder oppLocationBuilder = new TestHelper.OppLocationBuilder();
            Opportunity_Locations__c oppLocation1 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc.Id).save().getRecord();
        	Opportunity_Locations__c oppLocation2 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc2.Id).save().getRecord();
            
            // Validate that the first location created is set to primary
            System.assertEquals(true, [SELECT Primary__c FROM Opportunity_Locations__c WHERE Id = :oppLocation1.Id].Primary__c);
            
            // Validate that the parent Opportunity primary field is updated
            Opportunity oppAfter = [SELECT Id, Primary_Country__c FROM Opportunity LIMIT 1];
            System.assertEquals(loc.Country__c, oppAfter.Primary_Country__c);
        }
    }
    
    @isTest
    private static void testUpdateToPrimary() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME];
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        SiteLocation__c loc = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY1 LIMIT 1];
        SiteLocation__c loc2 = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY2 LIMIT 1];
        
        System.runAs(testUser){
            
            TestHelper.OppLocationBuilder oppLocationBuilder = new TestHelper.OppLocationBuilder();
            Opportunity_Locations__c oppLocation1 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc.Id).save().getRecord();
        	Opportunity_Locations__c oppLocation2 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc2.Id).save().getRecord();
            oppLocation2.Primary__c = true;
            update OppLocation2;
            
            // Validate that the updated location is set to primary and the first is no longer primary
            System.assertEquals(true, [SELECT Primary__c FROM Opportunity_Locations__c WHERE Id = :oppLocation2.Id].Primary__c);
            System.assertEquals(false, [SELECT Primary__c FROM Opportunity_Locations__c WHERE Id = :oppLocation1.Id].Primary__c);
            
            // Validate that the parent Opportunity primary field is updated
            Opportunity oppAfter = [SELECT Id, Primary_Country__c FROM Opportunity LIMIT 1];
            System.assertEquals(loc2.Country__c, oppAfter.Primary_Country__c);
        }
    }
    
    @isTest
    private static void testDeletePrimary() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME];
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        SiteLocation__c loc = [SELECT Id, Country__c FROM SiteLocation__c WHERE Country__c = :COUNTRY1 LIMIT 1];
        
        System.runAs(testUser){
            
            TestHelper.OppLocationBuilder oppLocationBuilder = new TestHelper.OppLocationBuilder();
            Opportunity_Locations__c oppLocation1 = oppLocationBuilder.build().withOpportunity(opp.Id).withLocation(loc.Id).save().getRecord();
            
            // Validate that the parent Opportunity primary field is updated
            Opportunity oppAfterInsert = [SELECT Id, Primary_Country__c FROM Opportunity LIMIT 1];
            System.assertEquals(loc.Country__c, oppAfterInsert.Primary_Country__c);
            
            delete oppLocation1;
            
            // Validate that the parent Opportunity primary field is blank
            Opportunity oppAfterDelete = [SELECT Id, Primary_Country__c FROM Opportunity LIMIT 1];
            System.assertEquals(null, oppAfterDelete.Primary_Country__c);
        }
    }
}