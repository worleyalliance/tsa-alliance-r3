/**************************************************************************************
Name: OpportunityForecast_C
Version: 1.0 
Created Date: 04.14.2017
Function: Controller for operaiton on Multioffice and Quarter records of OpportunityForecast LC

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04.14.2017         Original Version
* Jignesh Suvarna   03/12/2018      US1509: Data Stewards/Sales Super User - Edit Access
* Pradeep Shetty    09/11/2018      US2406: Adding Front Load and Back load option for spreading formula
                                    Removing Standard Deviation.
*************************************************************************************/
public with sharing class OpportunityForecast_C {

    private static OpportunityForecast_Service service = new OpportunityForecast_Service();

    /*
    * Retrieves picklist values for Spreading_Formula__c  
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getSpreadingFormulas(){
        return service.getSpreadingFormulas();
    }

   /*
    * Retrieves picklist values for Standard_Deviation__ c
    * US2406: Standard deviation is not used anymore
    */
    //@AuraEnabled
    //public static List<BaseComponent_Service.PicklistValue> getStandardDeviations(){
    //    return service.getStandardDeviations();
    //}

   /*
    *   Retrieves forecast for a given Opportunity
    */
    @AuraEnabled
    public static List<OpportunityForecast_Service.OpportunityForecast> retrieveForecasts(Id opportunityId){
        return service.retrieveForecasts(opportunityId);
    }

   /*
    *   Retrieves revenue schedules sorted by multioffice record.
    */
    @AuraEnabled
    public static List<OpportunityForecast_Service.Quarter> retrieveQuartersForMultiOffice(Id multiofficeId){
        return service.retrieveQuartersForMultiOffice(multiofficeId);
    }

    /*
    *   Updates quarter value for given records
    *   Validation: Sum of all records value have to be equal to multioffice GM value
    * Retrieves revenue schedules for given multioffice record
    */
    @AuraEnabled
    public static void saveQuarters(Id multiofficeId, String spreadFormula, String quartersJSON){
        service.saveQuarters(multiofficeId, spreadFormula, quartersJSON);
    }

    /*
    * Save Multi Office Spreading Formula and updates REvenue Schedules
    * US2406: Removed Standard Deviation parameter from the method. Standard Deviation is not used anymore
    */
    @AuraEnabled
    public static void saveSpreadingFormula(Id multiofficeId, String spreadingFormula){
        //US2406: Removed standard deviation from method call
        service.saveSpreadingFormula(multiofficeId, spreadingFormula);
    }

   /*
    *   Retrieves Opportunity (Added for DE270)
    */
    @AuraEnabled
    public static Opportunity getOpportunity(Id recordId){
        return service.getOpportunity(recordId);
    } 

    /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean hasEditPermission(){
        return Utility.checkAdminPermissions();
    }
    /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean isEditable(Id recordId){
        String objectName = recordId.getSObjectType().getDescribe().getName();
        return Utility.havRecordAccess(null,null,recordId);
    }       
}