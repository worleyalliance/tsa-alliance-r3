/**************************************************************************************
Name: Contact_TriggerHandler
Version: 1.0 
Created Date: 06.06.2017
Function: Contact Trigger handler

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           06.06.2017        Original Version
*************************************************************************************/
public class Contact_TriggerHandler {

    private static Contact_TriggerHandler handler;
    private static RecordType resourceRecordType;
    private static List<Account> accountList;
    public static String RESOURCE_RECORD_TYPE_NAME = 'Resource_Contact';
    public static String CONTACT_OBJECT_NAME = Contact.sObjectType.getDescribe().getName();

   /*
    * Singleton like pattern
    */
    public static Contact_TriggerHandler getHandler(){
        if(handler == null){
            handler = new Contact_TriggerHandler();
        }
        return handler;
    }

   /*
    * Getter for Resource_Contact record type
    */
    public RecordType getResourceRecordType(){
        if(resourceRecordType == null){
            resourceRecordType = [SELECT Id FROM RecordType WHERE SobjectType = :CONTACT_OBJECT_NAME AND DeveloperName = :RESOURCE_RECORD_TYPE_NAME];
        }
        return resourceRecordType;
    }

   /*
    * Getter for Employee Contact Account
    */
    public List<Account> getAccount(){
        if(accountList == null){
            accountList = [SELECT Id 
                           FROM Account 
                           WHERE Employee_Contact_Account__c = true];
        }
        return accountList;
    }    

   /*
    * Actions performed on before contacts is being created
    */
    public void onBeforeInsert(List<Contact> contacts){
        setValuesForResourceContacts(contacts);
    }


   /*
    * If Jacobs_Employee_ID__c is not empty then contact is comming from Oracle and it should have proper record type
    */
    private List<Contact> setValuesForResourceContacts(List<Contact> contacts){

        //Get Account for setting Contact's Account
        getAccount();

        //Variable for Account Id
        Id accountId;

        //Check if the Account is present
        if(accountList!=null && 
           !accountList.isEmpty() )
        {
            accountId = accountList[0].Id;
        }

        //Get the record type
        Id recordTypeId = getResourceRecordType().Id;

        //Set values
        for(Contact c : contacts){
            if(String.isNotBlank(c.Jacobs_Employee_ID__c)){
                c.RecordTypeId = recordTypeId;
                c.AccountId    = accountId;
            }
        }
        return contacts;
    }
}