/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 14.03.2017
Function: Controller for BookedActuals component. Generates data for Booked & Actual table

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   05.05.2017      Original Version
*************************************************************************************/
public with sharing class BookedActuals_C {

   /*
    *   Service class
    */
    private static BookedActualsService service = new BookedActualsService();

   /*
    *   Builds data for table for given opportunity id
    */
    @AuraEnabled
    public static BookedActualsService.BookedActual getBookedActual(Id opportunityId){
        return service.getBookedActuals(opportunityId);
    }
}