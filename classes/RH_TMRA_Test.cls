/**************************************************************************************
Name: RH_TMRA_Test
Function: Resource Hero - Test class related to RH_TMRA

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer                 Date                Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* William Kuehler           2018-09-24          Original Version
* Chris Cornejo             2018-11-20          Fixed OppTeamBuilder method due to change on TestHelper
*************************************************************************************/
@isTest
public class RH_TMRA_Test {
    /*
    
    //Constants
    private static final String PROFILE_SALES_SUPER_USER    = 'Sales Super User';
    private static final String USERNAME1                   = 'teamMemberTriggerTest1@jacobstestuser.net';
    private static final String USERNAME2                   = 'teamMemberTriggerTest2@jacobstestuser.net';
    private static final String USERNAME3                   = 'teamMemberTriggerTest3@jacobstestuser.net';
    private static final String USERNAME4                   = 'teamMemberTriggerTest4@jacobstestuser.net';
    
    //Test data setup
    @testSetup
    static void setUpTestData(){
        //Ensure that our default opportunity lookup is configured
        ResourceHeroApp__RHA_Object_Translation__c ot = ResourceHeroApp__RHA_Object_Translation__c.getInstance('ResourceHeroApp__opportunity__c');
        if(ot == null) {
            ResourceHeroApp__RHA_Object_Translation__c opp_ot = new ResourceHeroApp__RHA_Object_Translation__c();
            opp_ot.Name = 'ResourceHeroApp__opportunity__c';
            opp_ot.ResourceHeroApp__Object_Name__c = 'opportunity';
            opp_ot.ResourceHeroApp__Name_Field__c = 'Name';
            
            insert opp_ot;
        }
        
        //Add key settings for RHA operation
        List<ResourceHeroApp__RHA_Configuration__c> rhaconfig = new List<ResourceHeroApp__RHA_Configuration__c>();
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Days in Work Week', ResourceHeroApp__Value__c = '5')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Start of Week', ResourceHeroApp__Value__c = '2')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Over', ResourceHeroApp__Value__c = '#fa8072')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Target', ResourceHeroApp__Value__c = '#62b762')); 
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Matrix Utilization Color - Under', ResourceHeroApp__Value__c = '#d3d3d3'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Name for Resource Not Assigned', ResourceHeroApp__Value__c = 'Not yet assigned'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Sort Type', ResourceHeroApp__Value__c = 'CREATEDDATE'));
        rhaconfig.add(new ResourceHeroApp__RHA_Configuration__c(Name = 'Default Sort Order', ResourceHeroApp__Value__c = 'DESC'));
        insert rhaconfig;
        
        //Create matrix enabled field custom settings
        List<ResourceHeroApp__RHA_Matrix_Enabled_Fields__c> settings = new List<ResourceHeroApp__RHA_Matrix_Enabled_Fields__c>();
        ResourceHeroApp__RHA_Matrix_Enabled_Fields__c forecast = new ResourceHeroApp__RHA_Matrix_Enabled_Fields__c();
        forecast.Name = 'forecast';
        forecast.ResourceHeroApp__Field_Name__c = 'ResourceHeroApp__Forecast__c';
        forecast.ResourceHeroApp__Notes_Field_Name__c = 'ResourceHeroApp__Forecast_Notes__c';
        forecast.ResourceHeroApp__Save_Button_Text__c = 'Save Forecast';
        forecast.ResourceHeroApp__Dropdown_Text__c = 'Forecast';
        forecast.ResourceHeroApp__Use_Resource_Targets__c = true;
        settings.add(forecast);
        
        ResourceHeroApp__RHA_Matrix_Enabled_Fields__c actual = new ResourceHeroApp__RHA_Matrix_Enabled_Fields__c();
        actual.Name = 'actual';
        actual.ResourceHeroApp__Field_Name__c = 'ResourceHeroApp__Actual__c';
        actual.ResourceHeroApp__Notes_Field_Name__c = 'ResourceHeroApp__Actual_Notes__c';
        actual.ResourceHeroApp__Save_Button_Text__c = 'Save Actual';
        actual.ResourceHeroApp__Dropdown_Text__c = 'Actual';
        actual.ResourceHeroApp__Use_Resource_Targets__c = true;
        settings.add(actual);
        insert settings;
        
        
        //Create test user
        TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
        User testUser1 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME1).save().getRecord();
        User testUser2 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME2).save().getRecord();
        User testUser3 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME3).save().getRecord();
        User testUser4 = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME4).save().getRecord();
        
        
        //Create test records
        system.runAs(testUser1){    
            TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            //add new OpportunityTeamMemberBuilder method
            TestHelper.OpportunityTeamMemberBuilder oppteamMemberBuilder = new TestHelper.OpportunityTeamMemberBuilder();
            Account acc = accountBuilder.build().save().getRecord();
            //Opportunity opp = oppBuilder.build().withAccount(acc.Id).withTeamMember(testUser2.Id).save().getOpportunity();
            Opportunity opp = oppBuilder.build().withAccount(acc.Id).save().getOpportunity();
            OpportunityTeamMember otm = oppteamMemberBuilder.build().withOpportunity(opp.id).withUser(testUser2.id).save().getRecord();
        }
    }
    
    
    
    
    @isTest
    private static void OTM_Insert_CreateAssignment() {       
        //Get all users
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2];
        User testUser3 = [SELECT Id FROM User WHERE Username = :USERNAME3];
        User testUser4 = [SELECT Id FROM User WHERE Username = :USERNAME4];
        
        //Create test resource records
        List<ResourceHeroApp__Resource__c> res = new List<ResourceHeroApp__Resource__c>();
        res.add(new ResourceHeroApp__Resource__c(Name = 'Test Res 1', ResourceHeroApp__User__c = testUser3.Id, ResourceHeroApp__Weekly_Target_Min_Hours__c = 30, ResourceHeroApp__Weekly_Target_Max_Hours__c = 40));
        insert res;
        
        
        //Get the parent Opportunity
        Opportunity opp = [Select Id, 
                           AccountId 
                           From Opportunity 
                           where Parent_Opportunity__c = Null 
                           Limit 1];
        
        // Add team members to the Parent Opportunity
        List<OpportunityTeamMember> lstNewTm = new List<OpportunityTeamMember>();
        
        OpportunityTeamMember newTm1 = new OpportunityTeamMember();
        newTm1.OpportunityAccessLevel ='Edit';
        newTm1.OpportunityId          = opp.Id;
        newTm1.TeamMemberRole         ='BD Coordinator';
        newTm1.UserId                 = testUser3.ID;
        lstNewTm.add(newTm1);
        
        // Insert team members in Parent Opportunity
        insert lstNewTm;
        
        //Confirm that the assignment record has been created
        List<ResourceHeroApp__Resource_Assignment__c> verify = [SELECT Id, ResourceHeroApp__Resource__c, ResourceHeroApp__Role__c FROM ResourceHeroApp__Resource_Assignment__c];
        system.assertEquals(1, verify.size());
        system.assertEquals(res[0].Id, verify[0].ResourceHeroApp__Resource__c);
        system.assertEquals('BD Coordinator', verify[0].ResourceHeroApp__Role__c);
    }
*/
}