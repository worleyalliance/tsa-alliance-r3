/**************************************************************************************
Name: TaskOrderDocs_Controller
Version: 
Created Date: 
Function: LDT table to display library records for Projects where record type = Task Orders

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo    7/25/2017        DE 164, modified to show associated library records for Task Orders
* Chris Cornejo        8/4/2017     Task Order Document Library Record LDT Table populated when Project/Contract/Task Order Name field on the LIbrary record has the TO id
* Madhu Shetty     31-Jul-2018      Updated condition to show inactive record types in filters
*************************************************************************************/
public with sharing class TaskOrderDocs_Controller
{
public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues()
    {
        List<Schema.RecordTypeInfo> recordtypes = Library__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes){
            List <LDT_Lightning_Filter__c> rtlist = [Select Label_del__c 
                                                     from LDT_Lightning_Filter__c 
                                                     where Object_API__c  = 'Jacobs_Project__c' 
                                                     and OBJ_RecordType_API_Name__c = 'task_orders' 
                                                     and Coverage__c = 'Include'];// and Active__c = True];
            for (LDT_Lightning_Filter__c l: rtlist){
                 if (rt.getName() == l.Label_del__c)
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }
        }        
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel)
    {
        Id recid = Schema.SObjectType.Library__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }
    @AuraEnabled
    public static List<Library__c> getTOsOfProject(ID id) 
    {
        
        return getCBAsofProjectwRecordType(id, 'All');
    }
    
    @AuraEnabled
    public static List<Library__c> getCBAsOfProjectwRecordType(ID id, String recordTypeLabel) 
    {
        if( recordTypeLabel != 'All'){
        return [SELECT Id,Name,dt_submittal_date__c,lr_project__c, RecordType.Name
                FROM Library__c
                WHERE lr_project__c = :id and RecordType.Name in (:recordTypeLabel)
                ];}
        else{
        return [SELECT Id,Name,dt_submittal_date__c,lr_project__c, RecordType.Name
                FROM Library__c
                WHERE lr_project__c = :id
                ];}
    }
    
}