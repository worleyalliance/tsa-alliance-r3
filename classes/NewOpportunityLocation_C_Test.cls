/**************************************************************************************
Name: NewOpportunityLocation_C_Test
Version: 1.0 
Created Date: 26.04.2017
Function: NewOpportunityLocation_C unit test class

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   26.04.2017         Original Version
*************************************************************************************/
@isTest
private class NewOpportunityLocation_C_Test {

    @isTest
    private static void shouldRetrieveAccountLocations() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c location = locationBuilder.build().withAccount(acc.Id).save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //when
        List<BaseComponent_Service.PicklistValue> locations = NewOpportunityLocation_C.retrieveAccountLocations(opp.Id);

        //then
        System.assertEquals(2, locations.size()); //one is created by trigger
        Set<Id> locationIds = new Set<Id>{locations[0].value, locations[1].value};
        System.assert(locationIds.contains(location.Id), 'Location is missing');
    }

    @isTest
    private static void shouldAddOpportunityLocation() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.LocationBuilder locationBuilder = new TestHelper.LocationBuilder();
        SiteLocation__c location = locationBuilder.build().withAccount(acc.Id).save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //when
        NewOpportunityLocation_C.saveOpportunityLocation(opp.Id, location.Id);

        //then
        List<Opportunity_Locations__c> opportunityLocations =
            [SELECT Location__c FROM Opportunity_Locations__c WHERE Opportunity__c = :opp.Id];
        System.assertEquals(1, opportunityLocations.size());
        System.assertEquals(location.Id, opportunityLocations[0].Location__c);
    }

    @isTest
    private static void shouldRetrieveOpportunityName() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //when
        String opportunityName = NewOpportunityLocation_C.retrieveOpportunityName(opp.Id);

        //then
        System.assertEquals(opp.Name, opportunityName);
    }

    @isTest
    private static void shouldThrowExcpetionWhenOpportunityIsNotFound() {

        //when
        try {
            String opportunityName = NewOpportunityLocation_C.retrieveOpportunityName(null);

            //then
            System.assert(false, 'Should throw exception of missing parent opportunity');
        } catch (Exception ex){
            System.assert( ex instanceof AuraHandledException, 'Wrong exception type');
        }

    }
}