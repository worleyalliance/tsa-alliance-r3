@IsTest
public class Review_TriggerHandlerTest {
    
    private static final String PROFILE_SALES_SUPER_USER = 'Sales Super User';
  	private static final String USERNAME = 'salesBpRevision@testuser.com';
    
	//Test data setup
    @testSetup
    static void setUpTestData(){
        //Create test user
    	TestHelper.UserBuilder userBuilder = new TestHelper.UserBuilder();
    	User salesUser = userBuilder.build().withProfile(PROFILE_SALES_SUPER_USER).withUserName(USERNAME).save().getRecord();

        System.runAs(salesUser){
            
            //Create Opportunity
            TestHelper.OpportunityBuilder oppBuilder = new TestHelper.OpportunityBuilder();
            Opportunity opp = oppBuilder.build().withGo(50).save().getOpportunity();
            
            //Create Review
            TestHelper.ReviewBuilder reviewBuilder = new TestHelper.ReviewBuilder();
            Review__c review = reviewBuilder.build().withOpportunity(opp.Id).recordType('Go/No-Go Review (BIAF)').save().getRecord();
            
    	}
    }
    
   @isTest
   private static void testReviewTrigger(){
       
       //Get the user
    	User salesUser = [SELECT Id FROM User WHERE UserName = :USERNAME LIMIT 1];
        
        //Get the parent Opportunity
        Opportunity opp = [SELECT Id, Go__c FROM Opportunity WHERE OwnerId = :salesUser.Id LIMIT 1];
       
       //Getting ECR RecordType for the Review
        RecordType reviewECRType = [SELECT Id FROM RecordType 
                                    WHERE SObjectType = 'Review__c' AND Name = 'Bid/No-Bid Review (ECR)' LIMIT 1];
        
        //Getting BIAF RecordType for the Review
        RecordType reviewBIAFType = [SELECT Id FROM RecordType 
                                    WHERE SObjectType = 'Review__c' AND Name = 'Bid/No-Bid Review (BIAF)' LIMIT 1];
        
        //Get Review record before updating the Opportunity
        Review__c reviewBefore = [SELECT Id, Go__c, Refresh_Opportunity_Brief__c, RecordType.Name, Contract_Type_s__c, Revenue__c, 
                                  Account__r.Name, Opportunity__r.Name, Risk_Categories_Selected__c, Execution_Risk_Score__c, 
                                  Opportunity_Risk_Score__c, Client_Risk_Score__c, Geography_Risk_Score__c, Commercial_Risk_Score__c, 
                                  Contractual_Risk_Score__c, Opportunity_Risk_Category__c, Execution_Risk_Category__c, Client_Risk_Category__c, 
                                  Geography_Risk_Category__c, Commercial_Risk_Category__c, Contractual_Risk_Category__c, Service_Type_Code__c, 
                                  Line_of_Business__c, Scope_of_Services__c, Contract_Award_Type__c, Total_Installed_Cost_TIC__c, 
                                  Gross_Margin__c, Total_Requested_B_P__c, Execution_Risks__c, Client_Risks__c, Geography_Risks__c, 
                                  Commercial_Risks__c, Contractual_Risks__c, Risk_Assessment__c, Project_Role__c, Review_Status__c, 
                                  Hazardous_materials_involved__c, Gross_margin_health__c, Indemnification_beyond_our_negligence__c, 
                                  Sub_consultant_involvement__c, Does_client_require_exclusivity__c, 
                                  Client_in_business_for_5_years__c, Project_Joint_Venture_required__c, Ongoing_dispute_with_this_client__c, 
                                  Boots_on_the_ground_required__c, Negative_sustainability_impact__c, Third_Party_Liability__c, 
                                  Payment_terms_less_than_favorable__c, Funding_by_development_bank_or_Jacobs__c, 
                                  Increased_human_rights_risk__c, Purchasing_on_our_paper__c, Cross_LOB_multi_office_exec_required__c, 
                                  Non_operating_country__c, Are_there_sourcing_constraints__c, Payment_issues_with_this_client__c, 
                                  Payment_terms_unfavorable__c, Previously_approved_by_LOB_president__c, Public_Private_Partnership_required__c, 
                                  Payment_secured_by_LOC__c, Revamp_in_live_plant_or_aged_facility__c, Inadequate_design_basis__c, 
                                  Is_part_of_scope_uninsured__c, Liability_for_Defects__c, 
                                  Has_corporate_tax_dept_approved__c, Legal_issues_with_working_in_country__c, 
                                  Does_project_involve_new_technology__c, Client_with_doubtful_creditworthiness__c, 
                                  Other_funding_limitations_concerns__c, Unusual_suspicious_T_Cs__c, 
                                  Public_or_private_sector_client__c, Does_executing_office_lack_expertise__c, Force_Majeure__c, 
                                  Direct_H_S_services__c, T_Cs_different_than_Jacobs_standard__c, Is_the_project_for_a_new_client__c, 
                                  Corruption_index_CPI_50__c, Does_client_have_experience_in_country__c, 
                                  Is_the_client_a_broker_front_company__c, Environmental_site_assessment__c, Bankable_Feasibility_Study__c, 
                                  Local_content_requirements__c, Liability_for_Liquidated_Damages__c,  
                                  Decision_authority_on_behalf_of_client__c, Country_with_high_risk_security_rating__c,  
                                  Project_specific_insurance_required__c, Does_client_lack_skills_for_the_project__c, 
                                  Total_Limit_of_Contract_Liability__c, Defects_Warranty_Period_2_Years__c, Client_poses_high_safety_security_risk__c, 
                                  Is_the_country_on_an_embargo_list__c, Tunnel_dam_or_stand_alone_geotech__c, 
                                  Do_we_have_to_endorse_rely_upon_info__c, Is_the_client_a_newly_formed_JV__c, 
                                  Is_the_client_on_a_corruption_list__c, Change_Order_approval_exposure_TCV_5M__c, 
                                  Liability_for_Consequential_Damages__c, Performance_Guarantees__c, Financial_securities_required__c, 
                                  Is_client_s_project_approach_a_concern__c, High_Risk_HSE_Rating__c
                                  FROM Review__c WHERE Opportunity__c = :opp.Id LIMIT 1];
        
        reviewBefore.RecordTypeId = reviewBIAFType.Id;
        reviewBefore.Revenue__c = 25000001;
        reviewBefore.Decision_authority_on_behalf_of_client__c = 'Yes';
        reviewBefore.Corruption_index_CPI_50__c = 'Yes';
        reviewBefore.Previously_approved_by_LOB_president__c = 'No';
        reviewBefore.Non_operating_country__c = 'Yes';
        reviewBefore.Boots_on_the_ground_required__c = 'No';
        reviewBefore.Has_corporate_tax_dept_approved__c = 'Yes';
        reviewBefore.Justification_Comments__c = 'Fake comment.';
        update reviewBefore;
        
        //Update the Opportunity Go%
        opp.Go__c = 75;
        update opp;
        
        test.startTest();
        
        //Execute tests
    	System.runAs(salesUser){
            
            //Create Review
            TestHelper.ReviewBuilder reviewBuilder = new TestHelper.ReviewBuilder();
            Review__c review = reviewBuilder.build().withOpportunity(opp.Id).recordType('Go/No-Go Review (BIAF)').save().getRecord();
            
            reviewBefore.Refresh_Opportunity_Brief__c = TRUE;
            update reviewBefore;
            
            Review_TriggerHandler.submitForApproval(reviewBefore);
            
            // Create another review
            Review__c blankReview = new Review__c(Opportunity__c = opp.Id, Risk_Categories_Selected__c = 'execution;client;geography;commercial;contractual');
            insert blankReview;
        }
        
        test.stopTest();
        
        //Get the Review after updating the Opportunity
        Review__c reviewAfter = [SELECT Id, Go__c, Refresh_Opportunity_Brief__c 
                       FROM Review__c WHERE Opportunity__c = :opp.Id LIMIT 1];
        
        //Validate results
    	System.assertEquals(50, reviewBefore.Go__c);
    	System.assertEquals(75, reviewAfter.Go__c);
    	System.assertEquals(FALSE, reviewAfter.Refresh_Opportunity_Brief__c);
       
   }

}