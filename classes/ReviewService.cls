/**************************************************************************************
Name: ReviewService
Version: 1.0 
Created Date: 08.31.2018
Function: Review services

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Manuel			08.31.2018		US2364 - Refresh Opportunity Brief Action
*************************************************************************************/
public with sharing class ReviewService {

    /*
  	* Updates Review records with latest parent Opportunity information
  	*/
    @InvocableMethod
    public static void updateOpportunityBrief(List<Id> reviewIds) {

        Map<String, Review_Opp_Field_List__mdt> reviewFieldMap = new Map<String, Review_Opp_Field_List__mdt>();
        Set<String> reviewFields = new Set<String>();
        Set<String> oppFields = new Set<String>();
        Set<Id> reviewIdSet = new Set<Id>(reviewIds);
        Set<Id> oppIdSet = new Set<Id>();
        
        //Get map of fields to refresh
        for(Review_Opp_Field_List__mdt reviewOppField : [
        	SELECT MasterLabel, Opportunity_Field_API_Name__c FROM Review_Opp_Field_List__mdt WHERE Refresh__c = TRUE
        ]) {
            reviewFieldMap.put(reviewOppField.MasterLabel, reviewOppField);
        }
        
        //Convert map to string list of fields to be used in query
        for(Review_Opp_Field_List__mdt fieldList : reviewFieldMap.values()) {
            reviewFields.add(fieldList.MasterLabel);
            oppFields.add(fieldList.Opportunity_Field_API_Name__c);
        }
        reviewFields.add('Opportunity__c');
        
        // Get fields to query that are needed to calculate risk score
        SObjectType accountType = Schema.getGlobalDescribe().get('Review__c');
		Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
        Set<String> reviewQueryFields = new Set<String>();
        for(Schema.SObjectField f : mfields.values()){
            reviewQueryFields.add(String.valueOf(f));
        }
        
        //Retrieve Review records to update
        List<Review__c> reviews = Database.query(prepareQuery('Review__c', reviewQueryFields, reviewIdSet));
        
        //Get Opportunity Ids
        for(Review__c review : reviews) {
            oppIdSet.add(review.Opportunity__c);
        }
        
        //Retrieve map of Opportunity records to copy values from
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(
            (List<Opportunity>)Database.query(prepareQuery('Opportunity', oppFields, oppIdSet)));

        //Update Review Opportunity fields
        for(Review__c review : reviews) {
            for(String reviewField : reviewFieldMap.keySet()) {
                review.put(reviewField, oppMap.get(review.Opportunity__c).get(reviewFieldMap.get(reviewField).Opportunity_Field_API_Name__c));
            }
            
            // Update the risk score based on the new values
            // Construct a map of selected categories to be used to calculate the risk score
            Map<String, String> selectedCategories = new Map<String, String>();
            selectedCategories.put('opportunity', 'true');
            if(review.Risk_Categories_Selected__c != null){
                if(review.Risk_Categories_Selected__c.contains('execution')){
                    selectedCategories.put('execution', 'true');
                }
                if(review.Risk_Categories_Selected__c.contains('client')){
                    selectedCategories.put('client', 'true');
                }
                if(review.Risk_Categories_Selected__c.contains('geography')){
                    selectedCategories.put('geography', 'true');
                }
                if(review.Risk_Categories_Selected__c.contains('commercial')){
                    selectedCategories.put('commercial', 'true');
                }
                if(review.Risk_Categories_Selected__c.contains('contractual')){
                    selectedCategories.put('contractual', 'true');
                }
            }
            RA_RiskAssessmentService.scoreWrapper score = RA_RiskAssessmentService.calcRiskScore(review, selectedCategories);
            review.Risk_Assessment__c = score.riskLevel + ' - ' + score.category;
        }
        
        update reviews;
    }
    
    /*
  	* Returns query string given object name, set of fields and Ids
  	*/
    private static String prepareQuery(String sobjectName, Set<String> fieldSet, Set<Id> ids) {
        String idList = '';
        List<String> fields = new List<String>(fieldSet);
        
        //Convert Id list to comma separated string to be used in query
        for(Id i : ids) {
            idList += (idList==''?'\'':',\'') + i + '\'';
        }
        
        //Prepare the review query string
        String query = 'SELECT ' + String.join(fields, ',') +
                       ' FROM ' + sobjectName +
                       ' WHERE Id IN ('+ idList +')';
        
        return query;
    }
    
    /*
    * Returns a map of Revenue values for a given set of Opportunity IDs
    */
    public static Map<Id, Decimal> getOppRevenue(Set<Id> oppIds){
        
        List<Opportunity> opps = [SELECT Id, Revenue__c
                                  FROM Opportunity
                                  WHERE Id IN :oppIds];
        
        Map<Id, Decimal> oppRevenueMap = new Map<Id, Decimal>();
        for(Opportunity o : opps){
            oppRevenueMap.put(o.Id, o.Revenue__c);
        }
        
        return oppRevenueMap;
        
    }
    
}