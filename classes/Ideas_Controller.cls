public with sharing class Ideas_Controller{

//public static Map<Id, String> recordtypemap {get;set;}

    
   @AuraEnabled        
    public static List<String> fetchIdeasCategories(){
       
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Ideas__c.Idea_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
           for( Schema.PicklistEntry f : ple)
           {
              options.add(f.getLabel());
           }       
           return options;
    }
    
    @AuraEnabled
    public static List<Ideas__c> getallIdeas() {
     
        return [SELECT Id,Name,Idea_Status__c,Idea_Category__c, Count_of_Up_Votes__c
                FROM Ideas__c];
       
    }     
    
    @AuraEnabled
    public static List<Ideas__c> getideasCategoryType(String recordTypeLabel) {
    
        if( recordTypeLabel != 'All'){
        return [SELECT Id,Name,Idea_Status__c,Idea_Category__c, Count_of_Up_Votes__c
                FROM Ideas__c
                WHERE Idea_Category__c = :recordTypeLabel
                ];}
        else{
        return [SELECT Id,Name,Idea_Status__c,Idea_Category__c, Count_of_Up_Votes__c
                FROM Ideas__c
                ];}
    }

    }