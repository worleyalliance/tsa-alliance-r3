/**************************************************************************************
Name: ChildOpportunitiesRelatedList_C_Test
Version: 1.0 
Created Date: 08.05.2017
Function: Unit test class for ChildOpportunitiesRelatedList_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           08.05.2017         Original Version
*************************************************************************************/
@isTest
private class ChildOpportunitiesRelatedList_C_Test {

    @isTest
    private static void shouldRetrieveChildOpportunities() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity parentOpportunity = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();
        Opportunity childOpportunity1 = opportunityBuilder.build().withAccount(acc.Id).withParent(parentOpportunity.Id).getOpportunity();
        Opportunity childOpportunity2 = opportunityBuilder.build().withAccount(acc.Id).withParent(parentOpportunity.Id).getOpportunity();

        insert new List<Opportunity>{childOpportunity1, childOpportunity2};

        //when

        List<ChildOpportunitiesRelatedList_Service.OpportunityWrapper> opportunities =
                ChildOpportunitiesRelatedList_C.getChildOpportunities(parentOpportunity.Id);

        //then
        System.assertEquals(2, opportunities.size());
        
        ChildOpportunitiesRelatedList_C.getClonedOpportunityFieldsAndValues(parentOpportunity.Id);
    }

    @isTest
    private static void shouldRetrieveOpportunityById() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity parentOpportunity = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();

        //when

        Opportunity retrievedParentOpp = ChildOpportunitiesRelatedList_C.getOpportunity(parentOpportunity.Id);

        //then
        System.assertEquals(parentOpportunity.Id, retrievedParentOpp.Id);
    }

    @isTest
    private static void shouldDeleteChildOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity parentOpportunity = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();
        Opportunity childOpportunity1 = opportunityBuilder.build().withAccount(acc.Id).withParent(parentOpportunity.Id).getOpportunity();
        Opportunity childOpportunity2 = opportunityBuilder.build().withAccount(acc.Id).withParent(parentOpportunity.Id).getOpportunity();

        insert new List<Opportunity>{childOpportunity1, childOpportunity2};

        //when
        ChildOpportunitiesRelatedList_C.deleteOpportunity(childOpportunity1.Id);

        //then
        List<Opportunity> result = [SELECT Id FROM Opportunity WHERE Parent_Opportunity__c = :parentOpportunity.Id];
        System.assertEquals(1, result.size());
    }

    @isTest
    private static void shouldValidateUserPermissions() {
        //given
        Boolean hasRead = BaseComponent_Service.isObjectAccessible(Opportunity.sObjectType.getDescribe().getName());
        Boolean hasCreate = BaseComponent_Service.isObjectCreateable(Opportunity.sObjectType.getDescribe().getName());
        Boolean hasEdit = BaseComponent_Service.isObjectUpdatable(Opportunity.sObjectType.getDescribe().getName());
        Boolean hasDelete = BaseComponent_Service.isObjectDeletable(Opportunity.sObjectType.getDescribe().getName());

        //when
        BaseComponent_Service.CRUDpermissions cruDpermissions = ChildOpportunitiesRelatedList_C.getUserPermissions();

        //then
        System.assertEquals(hasCreate, cruDpermissions.hasCreate);
        System.assertEquals(hasDelete, cruDpermissions.hasDelete);
        System.assertEquals(hasEdit, cruDpermissions.hasUpdate);
        System.assertEquals(hasRead, cruDpermissions.hasRead);
    }
}