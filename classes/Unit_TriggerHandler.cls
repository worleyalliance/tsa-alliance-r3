/**************************************************************************************
Name: Unit_TriggerHandler
Version: 1.0
Created Date: 12.04.2018
Function: Handler for Unit_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Scott Walker      12.04.2018      Original Version
*******************************************************************************************************************/
public class Unit_TriggerHandler {

    private static Unit_TriggerHandler handler;
    
    // Singleton like pattern
    public static Unit_TriggerHandler getHandler() {
     if (handler == null) {
     handler = new Unit_TriggerHandler();
     }
     return handler;
    }
    
    // Actions performed on Unit records on after update
    public void onAfterUpdate(List<Unit__c> units,Map<Id, Unit__c> oldUnitsByIds) {
      updateLOBandBUpicklists(units,oldUnitsByIds);
    }
    
    private void updateLOBandBUpicklists(List<Unit__c> units,Map<Id, Unit__c> oldUnits){
      
        //Create and populate a map with the unit records whose BU and/or LOB have been updated
        map<ID, Unit__c> UnitsMap = new map<ID, Unit__c>();
        for (Unit__c unit:  units)
            if(!unit.Business_Unit__c.equals(oldUnits.get(unit.Id).Business_Unit__c) || 
               !unit.Line_of_Business__c.equals(oldUnits.get(unit.Id).Line_of_Business__c))
                {
                    UnitsMap.put(unit.ID, unit);
                }
        
        //List of opportunities, multi office splits, projects, and libraries which will be updated due to Unit BU/LOB changes
        List<Opportunity> opportunityUpdateList = new List<Opportunity>();
        List<Multi_Office_Split__c> multiOfficeUpdateList = new List<Multi_Office_Split__c>();
        List<Project_Descriptions__c> projectUpdateList = new List<Project_Descriptions__c>();
        List<Library__c> libraryUpdateList = new List<Library__c>();
        
        //add a list for each object to be used for logging
        List<Log__c> opportunityLog = new List<Log__c>();
        List<Log__c> multiOfficeLog = new List<Log__c>();
        List<Log__c> projectLog = new List<Log__c>();
        List<Log__c> libraryLog = new List<Log__c>();
                
        //Loop through the opportunities matching the changed Units.  On each loop, create a unit variable and match the
        //opportunity with the unit varible-from the UnitsMap-using the PU Id.  Set the Opportunity BU and LOB from the value in the unit variable 
        for (Opportunity o: [SELECT Id, Lead_Performance_Unit_PU__c, Executing_Business_Unit__c, Executing_Line_of_Business__c
                                          FROM Opportunity WHERE Lead_Performance_Unit_PU__c IN :UnitsMap.KeySet()])
        {
            Unit__c unit = UnitsMap.get(o.Lead_Performance_Unit_PU__c);
            o.Executing_Business_Unit__c = unit.Business_Unit__c;
            o.Executing_Line_of_Business__c = unit.Line_of_Business__c;
            opportunityUpdateList.add(o);
        }
        
        //Loop through the multi office records.....
        for (Multi_Office_Split__c m: [SELECT Id, Performance_Unit_PU__c, Executing_BU__c, Executing_LOB__c
                                          FROM Multi_Office_Split__c WHERE Performance_Unit_PU__c IN :UnitsMap.KeySet()])
        {
            Unit__c unit = UnitsMap.get(m.Performance_Unit_PU__c);
            m.Executing_BU__c = unit.Business_Unit__c;
            m.Executing_LOB__c = unit.Line_of_Business__c;
            multiOfficeUpdateList.add(m);
        }
        
        //Loop through the project records.....
        for (Project_Descriptions__c p: [SELECT Id, Performance_Unit__c, Business_Unit__c, Line_of_Business__c
                                          FROM Project_Descriptions__c WHERE Performance_Unit__c IN :UnitsMap.KeySet()])
        {
            Unit__c unit = UnitsMap.get(p.Performance_Unit__c);
            p.Business_Unit__c = unit.Business_Unit__c;
            p.Line_of_Business__c = unit.Line_of_Business__c;
            projectUpdateList.add(p);
        }
        
        //Loop through the library records.....
        for (Library__c l: [SELECT Id, Performance_Unit__c, Business_Unit__c, Line_of_Business__c
                                          FROM Library__c WHERE Performance_Unit__c IN :UnitsMap.KeySet()])
        {
            Unit__c unit = UnitsMap.get(l.Performance_Unit__c);
            l.Business_Unit__c = unit.Business_Unit__c;
            l.Line_of_Business__c = unit.Line_of_Business__c;
            libraryUpdateList.add(l);
        }
                      
        //run DML updates
        if(opportunityUpdateList.size() > 0)
        {
            List<Database.SaveResult> opportunityresult = Database.Update(opportunityUpdateList, false);
            
            // Iterate through each returned result           
            for (Integer i=0; i < opportunityUpdateList.size(); i++) {
                Database.SaveResult s = opportunityresult[i];
                Opportunity origRecord = opportunityUpdateList[i];
                
                if (s.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    Log__c ol = new Log__c();
                    System.debug('Successfully updated Opportunity. ID: ' + origRecord.Id);
                    //add successes to the OpportunityLog list
                    ol.SFID__c = origRecord.Id;
                    //ol.GUID__c = sr.xxxxx;  //research method to access unit ID
                    ol.AppName__c = 'SFDC';
                    ol.EventType__c = 'UnitOpportunitySuccess';
                    opportunityLog.add(ol);					
                }
                
                else
                {
                    // Operation failed, so get all errors
                    System.debug('Error updating Opportunity. ID: ' + origRecord.Id);                
                    for(Database.Error err : s.getErrors()) {
                        Log__c ol = new Log__c();
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());
                        //add exceptions to OpportunityLog list
                        ol.SFID__c = origRecord.Id;
                        ol.AppName__c = 'SFDC';
                        ol.EventType__c = 'UnitOpportunityException';
                        ol.Exception__c = 'Error status code: ' + err.getStatusCode() + 'Error message: ' + err.getMessage();
                        opportunityLog.add(ol);                        
                 }

            }
                
           }    
            Database.SaveResult[] opportunitylogresult = Database.Insert(opportunityLog, false);    
        }
            //debugging log inserts
            /* for (Database.SaveResult sr : opportunitylogresult) {
                if (sr.isSuccess()) {
                    // Log operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated log with Log ID: ' + sr.getId());
                }
                else {
                    // Log operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred when writing to the log.');                    
                        System.debug('The log error status code is: ' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this log error: ' + err.getFields());
                    }
                }
            } 
            */
        
        if(multiOfficeUpdateList.size() > 0)
        {
            List<Database.SaveResult> multiofficeresult = Database.Update(multiOfficeUpdateList, false);
            
            for (Integer i=0; i < multiOfficeUpdateList.size(); i++) {
                Database.SaveResult s = multiofficeresult[i];
                Multi_Office_Split__c origRecord = multiOfficeUpdateList[i];

                
                if (s.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    Log__c ml = new Log__c();
                    System.debug('Successfully updated MultiOfficeSplit. ID: ' + origRecord.Id);
                    //add successes to the multiOfficeLog list
                    ml.SFID__c = origRecord.Id;
                    ml.AppName__c = 'SFDC';
                    ml.EventType__c = 'UnitMultiOfficeSuccess';
                    multiOfficeLog.add(ml);
                }
                else {
                    // Operation failed, so get all errors
                    System.debug('Error updating Multi Office. ID: ' + origRecord.Id);                
                    for(Database.Error err : s.getErrors()) {
                        Log__c ml = new Log__c();
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('MultiOfficeSplit fields that affected this error: ' + err.getFields());
                        //add exceptions to OpportunityLog list
                        ml.SFID__c = origRecord.Id;
                        ml.AppName__c = 'SFDC';
                        ml.EventType__c = 'UnitMultiOfficeException';
                        ml.Exception__c = 'Error status code: ' + err.getStatusCode() + 'Error message: ' + err.getMessage();
                        multiOfficeLog.add(ml); 
                    }
                }
            } 
           Database.SaveResult[] multiofficelogresult = Database.Insert(multiOfficeLog, false);
        } 
        
        if(projectUpdateList.size() > 0)
        {  
            List<Database.SaveResult> projectresult = Database.Update(projectUpdateList, false);
            
            for (Integer i=0; i < projectUpdateList.size(); i++) {
                Database.SaveResult s = projectresult[i];
                Project_Descriptions__c origRecord = projectUpdateList[i];

                
                
                if (s.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    Log__c pl = new Log__c();
                    System.debug('Successfully updated Project. ID: ' + origRecord.Id);
                    //add successes to the OpportunityLog list
                    pl.SFID__c = origRecord.Id;
                    pl.AppName__c = 'SFDC';
                    pl.EventType__c = 'UnitProjectSuccess';
                    projectLog.add(pl); 
                }
                else {
                    // Operation failed, so get all errors
                    System.debug('Error updating Project. ID: ' + origRecord.Id);                
                    for(Database.Error err : s.getErrors()) {
                        Log__c pl = new Log__c();
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Project fields that affected this error: ' + err.getFields());
                        //add exceptions to OpportunityLog list
                        pl.SFID__c = origRecord.Id;
                        pl.AppName__c = 'SFDC';
                        pl.EventType__c = 'UnitProjectException';
                        pl.Exception__c = 'Error status code: ' + err.getStatusCode() + 'Error message: ' + err.getMessage();
                        projectLog.add(pl);
                    }
                }
            } 
            Database.SaveResult[] projectlogresult = Database.Insert(projectLog, false);
        } 
        
        if(libraryUpdateList.size() > 0)
        {
            List<Database.SaveResult> libraryresult = Database.Update(libraryUpdateList, false);
            
            for (Integer i=0; i < libraryUpdateList.size(); i++) {
                Database.SaveResult s = libraryresult[i];
                Library__c origRecord = libraryUpdateList[i];

                
                
                if (s.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    Log__c ll = new Log__c();
                    System.debug('Successfully updated Library. ID: ' + origRecord.Id);
                    //add successes to the OpportunityLog list
                    ll.SFID__c = origRecord.Id;
                    //ol.GUID__c = sr.xxxxx;  //research method to access unit ID
                    ll.AppName__c = 'SFDC';
                    ll.EventType__c = 'UnitLibrarySuccess';
                    libraryLog.add(ll);
                }
                else {
                    // Operation failed, so get all errors 
                    System.debug('Error updating Library. ID: ' + origRecord.Id);                
                    for(Database.Error err : s.getErrors()) {
                        Log__c ll = new Log__c();
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Library fields that affected this error: ' + err.getFields());
                        ll.SFID__c = origRecord.Id;
                        ll.AppName__c = 'SFDC';
                        ll.EventType__c = 'UnitLibraryException';
                        ll.Exception__c = 'Error status code: ' + err.getStatusCode() + 'Error message: ' + err.getMessage();
                        libraryLog.add(ll);
                    }
                }
            } 
            Database.SaveResult[] librarylogresult = Database.Insert(libraryLog, false);
        } 
  } 
}