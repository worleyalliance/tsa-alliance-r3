/************************************************************************************
* Name: Jacobs_User_TriggerHandler
* Version: 1.0 
* Created Date: 11.30.2017
* Function: Handles user inserts and updates. Locate corresponding License Request 
*     records and make updates to it according to the user attributes.
*
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                 
* Ray Zhao          11.30.2017      Original Version
* Pradeep Shetty    01.09.2018      US1287 - Add new users to Platform Alerts and Notices Chatter group
* Manuel Johnson	04.26.2018		US2038 - When user role changes, update all Accounts they own with new Role
*************************************************************************************/
public class Jacobs_User_TriggerHandler {
    
    public static final String PLATFORM_ALERTS_NOTICES = 'Platform Alerts and Notices';
    
    @future
    public static void onAfterInsert(Map<String, Id> usermap) {
        
        List<License_Request__c> LRList = [Select Id, Requested_For__r.Email, Provisioned_User__c, Provisioned_User_Status__c FROM License_Request__c WHERE Requested_For__r.Email IN :usermap.keySet()]; 
        
        for (Integer i=0; i<LRList.size(); i++){
            if (usermap.get(LRList[i].Requested_For__r.Email) != null) {
                LRList[i].Provisioned_User__c = usermap.get(LRList[i].Requested_For__r.Email);
                LRList[i].Provisioned_User_Status__c = TRUE;
                LRList[i].Status__c = 'Provisioned in Prod';
                usermap.remove(LRList[i].Requested_For__r.Email);
            }
        }
        Update LRList;
        if (usermap.size()>0){
            List<ErrorLog__c> logEntries = new List<ErrorLog__c>();
            logEntries.add(new ErrorLog__c(ErrorMessage__c = 'UserTriggerInsert'+ ' ' + usermap.values()));
            insert logEntries;
        }
        
        //US1287: Add users to the chatter group
        addUserToChatterGroup(usermap.values());  
        
    }

	//Actions performed on User records on after update    
    public static void onAfterUpdate(Map<Id, User> users, Map<Id, User> oldUsers) {

        Map<Id, Boolean> c = new Map<Id, Boolean>();
        List<Id> usersWithNewRole = new List<Id>();
        
        for (Id userId : users.keySet()){
            if (oldUsers.get(userId).isActive != users.get(userId).isActive){
                c.put(userId, users.get(userId).isActive);
            }
            if (oldUsers.get(userId).UserRoleId != users.get(userId).UserRoleId){
                usersWithNewRole.add(userId);
            }
        }
        
        if (!c.isEmpty()) {
            updateLog(c);
        }
        if(!usersWithNewRole.isEmpty()){
            updateUserRole(usersWithNewRole);
        }

    }
    
    @future
    private static void updateLog(Map<Id, Boolean> uu) {

        List<License_Request__c> LRList = [Select Id, Status__c, Provisioned_User__c, Provisioned_User_Status__c FROM License_Request__c WHERE Provisioned_User__c IN :uu.keySet()]; 
		
        for (Integer i=0; i<LRList.size(); i++){
            if (uu.get(LRList[i].Provisioned_User__c)){
                LRList[i].Provisioned_User_Status__c = TRUE;
                LRList[i].Status__c = 'Provisioned in Prod';
            }
            else {
                LRList[i].Provisioned_User_Status__c = FALSE;
                LRList[i].Status__c = 'Prod User Inactive';
            }                
            
            uu.remove(LRList[i].Provisioned_User__c);
        }
        
        Update LRList;
        
        if (uu.size()>0){
            List<ErrorLog__c> logEntries = new List<ErrorLog__c>();
            logEntries.add(new ErrorLog__c(ErrorMessage__c = 'UserTriggerUpdate'+ ' ' + uu.keySet()));
            insert logEntries;
        }
    }
    
    // Update all Accounts owned by users with their new Role
    @future
    private static void updateUserRole(List<Id> userIds){

        List<Account> accounts = [SELECT Id, Owner.UserRole.DeveloperName, Account_Lead_User_Role__c FROM Account WHERE OwnerId IN : userIds];
        List<Call__c> calls = [SELECT Id, Owner.UserRole.DeveloperName, Owner_Role__c FROM Call__c WHERE OwnerId IN : userIds];
        
        for (Account acc : accounts){
            acc.Account_Lead_User_Role__c = acc.Owner.UserRole.DeveloperName;
        }
        update accounts;
        
        for (Call__c call : calls){
            call.Owner_Role__c = call.Owner.UserRole.DeveloperName;
        }
        update calls;
    }
    
    //Method to add new user to Platform Alerts and Notices chatter group
    private static void addUserToChatterGroup(List<Id> userIdList){
        
        //Get the Collaboration Group Id. Using list to prevent SOQL exception if record is not found
        List<CollaborationGroup> chatterGroupIdList = [Select Id
                                                       From CollaborationGroup 
                                                       Where Name = :PLATFORM_ALERTS_NOTICES 
                                                       Limit 1];
        
        if(chatterGroupIdList!=null && !chatterGroupIdList.isEmpty()){
            //List for CollaborationGroupMember records
            List<CollaborationGroupMember> collGrpMemList = new List<CollaborationGroupMember>();
            
            //Add all the members to the Chatter group
            for(Id userId: userIdList){
                CollaborationGroupMember collGrpMem = new CollaborationGroupMember();
                collGrpMem.CollaborationGroupId = chatterGroupIdList[0].Id;
                collGrpMem.memberId = userId;
                collGrpMemList.add(collGrpMem);
            }
            
            //Insert Collaboration Group Members 
            Database.SaveResult[] srList = Database.insert(collGrpMemList, false);
            
            List<ErrorLog__c> logEntries = new List<ErrorLog__c>();
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        logEntries.add(new ErrorLog__c(ErrorMessage__c = 'Error adding user to Chatter Group'+ 
                                                       ' ' + 
                                                       err.getStatusCode() + 
                                                       ': ' + 
                                                       err.getMessage()));
                    }
                }
            }
        }
    }
}