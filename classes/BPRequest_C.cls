/**************************************************************************************
Name: BPRequest_C
Version: 1.0 
Created Date: 08/14/2017 
Function: Controller for lightning component creating B&P Revision

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty   08/14/2017       Original Version
*************************************************************************************/
public with sharing class BPRequest_C {

    //Variable to access the service class
    private static BPRequest_Service service = new BPRequest_Service();

   /*
    * Returns original B&P for which revision is being created
    */
    @AuraEnabled
    public static BPRequest_Service.BPRequestData retrieveOriginalBAndP(Id bpId){
        return service.retrieveOriginalBAndP(bpId);
    }

   /*
    * Saves the B&P revision
    */
    @AuraEnabled
    public static Id createBPRevision(Id originalBPId, String bpRevisionJSON){
        return service.saveBPRevision(originalBPId, bpRevisionJSON);
    }

    /*
     * Constructor to enable using VF page as a content source for custom list button
     */
    public BPRequest_C(ApexPages.StandardSetController stdSetController){

    }


}