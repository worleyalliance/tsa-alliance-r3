/**************************************************************************************
Name: PartnerDataTable_Service
Version: 1.0 
Created Date: 05.05.2017
Function: Service of PartnerDataTable for Review page component

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
public class PartnerDataTable_Service extends DataTable_Service{

   /*
    * Method retrieves Project records for opportunity
    * Fields retreived are based on field set.
    */
    public TableData getPartnerData(
            String fieldSetName,
            Id recordId
    ) {
        validateObjectAccesible('Partner__c');
        validateObjectAccesible('Review__c');

        List<FieldDescription> fieldDescriptions = prepareFieldDescriptions(fieldSetName, 'Partner__c');
        String query =
                buildPartnerQuery(
                        fieldDescriptions,
                        recordId
                );

        //Execute query
        List<sObject> records = (List<sObject>) Database.query(query);
        String additionalValueField = null;
        Boolean totalRowPresent = false;
        Boolean isSumDefault = false;
        TableData data = buildTableData(records, fieldDescriptions, totalRowPresent, isSumDefault, additionalValueField);
        return data;
    }

   /*
    * Method prepares a query based on delivered data
    */
    private String buildPartnerQuery(
            List<DataTable_Service.FieldDescription> fieldDescriptions,
            Id recordId
    ){
        List<String> fieldNames = getFieldNames(fieldDescriptions);
        String query =
                'SELECT '+String.join(fieldNames, ',')+' ' +
                'FROM Partner__c ' +
                'WHERE Opportunity__c IN (SELECT Opportunity__c FROM Review__c WHERE Id = \''+recordId+'\')';

        System.debug('query: '+query);
        return query;
    }
}