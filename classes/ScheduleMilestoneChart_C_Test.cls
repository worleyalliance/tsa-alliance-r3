/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 21.03.2017
Function: Unit test class for ScheduleMilestoneChart_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   21.03.2017      Original Version
* Blake Poutra		2/25/2019		Updated for newGetMilestones method
* Scott Stone		3/21/2019		updated for DE928
*************************************************************************************/
@IsTest
private class ScheduleMilestoneChart_C_Test {

    @IsTest
    private static void shouldRetrieveMilestonesForAccounts() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withTeamMember().save().getRecord();
        TestHelper.MilestoneBuilder milestoneBuilder = new TestHelper.MilestoneBuilder();
        Schedule_Milestone__c milestone =
                milestoneBuilder
                        .build()
                        .withAccount(acc.Id)
                        .withStartDate(Date.today())
                        .withEndDate(Date.today().addDays(2))
                        .save()
                        .getRecord();

        //when
        List<ScheduleMilestoneChart_Service.Task> tasks =
                ScheduleMilestoneChart_C.getMilestones(
                        acc.Id,
                        'Schedule_Milestone__c',
                        'Account__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Name',
                        'Status__c');

        //then
        System.assertEquals(1, tasks.size());
        System.assertEquals(Date.today(), tasks[0].milestones[0].startDate);
        System.assertEquals(Date.today().addDays(2), tasks[0].milestones[0].endDate);
    }

    @IsTest
    private static void shouldRetrieveMilestonesForAccountsWithoutCondition() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().withTeamMember().save().getRecord();
        TestHelper.MilestoneBuilder milestoneBuilder = new TestHelper.MilestoneBuilder();
        Schedule_Milestone__c milestone =
                milestoneBuilder
                        .build()
                        .withAccount(acc.Id)
                        .withStartDate(Date.today())
                        .withEndDate(Date.today().addDays(2))
                        .save()
                        .getRecord();

        //when
        List<ScheduleMilestoneChart_Service.Task> tasks =
                ScheduleMilestoneChart_C.getMilestones(
                        acc.Id,
                        'Schedule_Milestone__c',
                        null,
                        'Start_Date__c',
                        'End_Date__c',
                        'Name',
                        'Status__c');

        //then
        System.assertEquals(1, tasks.size());
        System.assertEquals(Date.today(), tasks[0].milestones[0].startDate);
        System.assertEquals(Date.today().addDays(2), tasks[0].milestones[0].endDate);
    }

    @IsTest
    private static void shouldRetrieveMilestonesForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();
        TestHelper.MilestoneBuilder milestoneBuilder = new TestHelper.MilestoneBuilder();
        Schedule_Milestone__c milestone =
                milestoneBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .withStartDate(Date.today())
                        .withEndDate(Date.today())
                        .save()
                        .getRecord();

        //when
        List<ScheduleMilestoneChart_Service.Task> tasks =
                ScheduleMilestoneChart_C.getMilestones(
                        opp.Id,
                        'Schedule_Milestone__c',
                        'Opportunity__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Milestone_Type__c',
                        'Status__c');

        //then
        //two of the three milestones added by Create or Update Opportunity Start Date or Close Date Schedule Milestones Process Builder
        System.assertEquals(3, tasks.size());
        //test class milestone
        System.assertEquals(Date.today(), tasks[0].milestones[0].startDate);
        System.assertEquals(Date.today(), tasks[0].milestones[0].endDate);
        //process builder close date milestone        
        System.assertEquals(Date.today(), tasks[1].milestones[0].startDate);
        System.assertEquals(Date.today(), tasks[1].milestones[0].endDate);
        //process builder project start date milestone
        System.assertEquals(Date.today().addDays(2), tasks[2].milestones[0].startDate);
        System.assertEquals(Date.today().addDays(2), tasks[2].milestones[0].endDate);
    }
    
    @IsTest
    private static void newShouldRetrieveMilestonesForOpportunity() {
        //given
        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();
        TestHelper.OpportunityBuilder opportunityBuilder = new TestHelper.OpportunityBuilder();
        Opportunity opp = opportunityBuilder.build().withAccount(acc.Id).save().getOpportunity();
        TestHelper.MilestoneBuilder milestoneBuilder = new TestHelper.MilestoneBuilder();
        Schedule_Milestone__c milestone =
                milestoneBuilder
                        .build()
                        .withOpportunity(opp.Id)
                        .withStartDate(Date.today())
                        .withEndDate(Date.today())
                        .save()
                        .getRecord();
        Call__c theCall = new Call__c(Opportunity__c = opp.Id, Date__c = Date.today(), Call_Type__c = 'Telephone', Meeting_Location__c = 'test',
                                      Status__c = 'Planned', Subject__c = 'test', Call_Objectives__c = 'test');
        insert theCall;

        //when
        Map<String, List<Object>> tasks1 =
                ScheduleMilestoneChart_C.newGetMilestones(
                        opp.Id,
                        'Day',
                    	'Schedule_Milestone__c',
                        'Opportunity__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Milestone_Type__c');
        Map<String, List<Object>> tasks2 =
                ScheduleMilestoneChart_C.newGetMilestones(
                        opp.Id,
                        'Week',
                    	'Schedule_Milestone__c',
                        'Opportunity__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Milestone_Type__c');
        Map<String, List<Object>> tasks3 =
                ScheduleMilestoneChart_C.newGetMilestones(
                        opp.Id,
                        'Month',
                    	'Schedule_Milestone__c',
                        'Opportunity__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Milestone_Type__c');
        Map<String, List<Object>> tasks4 =
                ScheduleMilestoneChart_C.newGetMilestones(
                        opp.Id,
                        'Year',
                    	'Schedule_Milestone__c',
                        'Opportunity__c',
                        'Start_Date__c',
                        'End_Date__c',
                        'Milestone_Type__c');

        
    }

}