/**
* Test Class for PF_StoryCloningAuraController
**/
@isTest
public class PF_TestCasesCloningAuraController_Test {
    
    static testMethod void executeTestClone() {
        Test.startTest();
            
            PF_Product__c product = new PF_Product__c(
                Name = 'Test Case Product');      
            insert product;
            
            PF_Epic__c epic = new PF_Epic__c(
                Name = 'Test Case Enhancement',
                PF_Product__c = product.Id);      
            insert epic;
            
            PF_Stories__c story = new PF_Stories__c(
                Name = 'Clone Test Case',
                PF_Story_Owner__c = Userinfo.getUserId(),
                PF_Epic__c = epic.Id);      
            insert story;
            
            PF_Deployment__c Deployment = new PF_Deployment__c(
                Name = 'Deployment1',
                PF_API_Name__c = 'Deployment1',
                PF_Object_Name__c = 'epic',
                PF_Story__c = story.id);      
            insert Deployment;
            
            PF_Project_Item__c ProjectItem = new PF_Project_Item__c(
                Name = 'ProjectItem1',
                PF_Status__c = 'New',
                PF_Assigned_To__c = Userinfo.getUserId(),
                PF_Type__c = 'Risk',
                PF_Level__c = 'Story',
                PF_Impact__c = 'Medium',
                PF_Probability__c = 'Moderate',
                PF_Identified_Date__c = System.today(),
                PF_Story__c = story.id);
            insert ProjectItem;
            PF_Tasks__c task = new PF_Tasks__c(
                Name = 'Task',
                PF_Assigned_To__c = Userinfo.getUserId(),
                PF_Work_Task_Status__c = 'New',
                PF_Type__c = 'Integration',
                PF_Complexity__c = 'Simple',
                PF_Story__c = story.Id);      
            insert task;
            
            PF_TestCases__c testCase = new PF_TestCases__c(
                //RecordTypeId = Schema.SObjectType.PF_TestCases__c.getRecordTypeInfosByName().get('Unit Test Case').getRecordTypeId(),
                Name = 'Test Case Cloning',
                PF_Type__c = 'Unit Test',
                PF_Description__c = 'Test successful cloning of test case with its child objects', 
                PF_Story__c = story.Id);      
            insert testCase;
            
            PF_TestCases__c testCase1 = new PF_TestCases__c(
                //RecordTypeId = Schema.SObjectType.PF_TestCases__c.getRecordTypeInfosByName().get('Unit Test Case').getRecordTypeId(),
                Name = 'Test Case Cloning',
                PF_Type__c = 'Unit Test',
                PF_Description__c = 'Test successful cloning of test case with its child objects', 
                PF_Story__c = story.Id,
                PF_Predecessor_Test_Case__c = testCase.id);      
            insert testCase1;
            
            PF_TestCaseExecution__c testCaseAssign = new PF_TestCaseExecution__c(
                PF_Test_Case__c = testCase.Id,
                PF_Assigned_To__c = Userinfo.getUserId(),    
                PF_Target_Completion_Date__c = Date.today() + 10);      
            insert testCaseAssign;
            
            PF_Test_Case_Step__c testCaseStep = new PF_Test_Case_Step__c(
                Name = 'Test Case Step',
                PF_Test_Case__c = testCase.Id,
                PF_Step_Number__c = 1,
                PF_Step_Description__c = 'Click on Clone',
                PF_Expected_Result__c = 'User should be navigated to the clone screen');      
            insert testCaseStep;
            PF_Defects__c testDefect = new PF_Defects__c(
                Name = 'Test Defect',
                PF_Test_Case__c = testCase.Id,
                PF_Status__c = 'New',
                PF_Type__c = 'Defect',
                PF_Severity__c = 'Low');      
            insert testDefect;
            
            PF_TestCasesCloningAuraController.getWorkTaskList(testCase.id);
            PF_TestCasesCloningAuraController.getCloneRecord(new List<String>{'PF_TestCases__c', 'PF_Defects__c', 'PF_Test_Case_Step__c', 'PF_TestCaseExecution__c'}, testCase.id, 'Copy Of TestTestCase');
            System.assertNotEquals('Copy Of TestTestCase',[SELECT Name FROM PF_TestCases__c where Id =: testCase.id LIMIT 1].name);
        
        Test.stopTest();
    }
    
}