/**************************************************************************************
Name:BPRequest_Service
Version: 1.0 
Created Date: 08/14/2017
Function: Creates B&P revision

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty   	08/14/2017      Original Version
* Manuel Johnson	04/23/2018		US2006 Updates to allow for CH2M Revision Type
* Manuel Johnson	07/18/2018		Added Project Code to list of values to copy over to the revision
*************************************************************************************/
public with sharing class BPRequest_Service extends BaseComponent_Service {
    
    //Request Type Constant
    public final String REQUEST_TYPE_REVISION = 'Revision';
    
    /*
	* Returns B&P Request for which the revision needs to be created
	*/
    public BPRequestData retrieveOriginalBAndP(Id bpId){
        //Check if user has access to the B&P Request object
        validateObjectAccesible(B_P_Request__c.sObjectType.getDescribe().getName());
        
        //Create an object of BPRequestData
        BPRequestData bpReq = new BPRequestData();
        
        //Get details of the Original B&P
        B_P_Request__c bpRequest = [Select Id,
                                    B_P_Budget_Local_Currency__c,
                                    BP_Budget__c,
                                    Currency__c,
                                    Description__c,
                                    Operating_Unit__c,
                                    Reason_for_Request_Justification__c,
                                    Status__c,
                                    Legacy_B_P__c,
                                    RecordType.Name
                                    From B_P_Request__c 
                                    Where Id = :bpId];
        
        //Set BPREquestData Value
        bpReq.bpBudgetLocalCurrency = bpRequest.B_P_Budget_Local_Currency__c;
        bpReq.status                = bpRequest.Status__c;
        bpReq.legacyBP              = bpRequest.Legacy_B_P__c;
        bpReq.localCurrency	  		= bpRequest.Currency__c;
        bpReq.stdRecordType	  	  	= !bpRequest.RecordType.Name.contains('CH2M');
        
        //Get Picklist label for the Operating Unit field
        Schema.DescribeFieldResult fresult = B_P_Request__c.Operating_Unit__c.getDescribe();
        
        //Loop through the picklist entry to get the label for Operating Unit value
        for(Schema.PicklistEntry plEntry: fresult.getPicklistValues()){
            if(plEntry.getValue().equalsIgnoreCase(bpRequest.Operating_Unit__c)){
                bpReq.operatingUnitLabel = plEntry.getLabel();
            }
        }
        
        return bpReq;
    }
        
    /*
	* Saves B&P Revision
	*/
    public Id saveBPRevision(Id originalBPId, String bpRevisionJSON){
        validateObjectCreateable(B_P_Request__c.sObjectType.getDescribe().getName());
        
        //Get the information entered on the screen and convert JSON to object
        BPRequestData revisionData = (BPRequestData) JSON.deserialize(bpRevisionJSON, BPRequestData.class);
        
        //Get B&P information to copy over to the revision record. Expecially the approvers
        B_P_Request__c bpRequest = [Select Id,
                                    Client__c,
                                    Opportunity__c,
                                    BP_Budget__c,
                                    Level_1_Ops_Manager__c,
                                    Level_1_Sales_Manager__c,
                                    Regional_VP_of_Ops__c,
                                    Regional_VP_of_Sales__c,
                                    LOB_President__c,
                                    SVP_GM_Ops__c,
                                    SVP_Sales__c,
                                    Chairman_CEO__c,
                                    B_P_Coordinator__c,
                                    Sales_Lead__c,
                                    Operating_Unit__c,
                                    Currency__c,
                                    CH2M_Org_Code__c,
                                    RecordType.DeveloperName,
                                    Project_Code__c
                                    From B_P_Request__c 
                                    Where Id = :originalBPId];
        
        //Get the record type associated with B&P revision
        String recordTypeName = bpRequest.RecordType.DeveloperName == 'CH2M_Approved_B_P_Request' ? 'CH2M_B_P_Revision' : 'B_P_Revision';
        RecordType revisionRT = [Select Id from RecordType where DeveloperName = :recordTypeName limit 1];
        
        //Create B&P revision
        B_P_Request__c bpRev = new B_P_Request__c(B_P_Budget_Local_Currency__c = revisionData.additionalBudgetLocal,
                                                  Client__c = bpRequest.Client__c,
                                                  Opportunity__c = bpRequest.Opportunity__c,
                                                  Original_B_P__c = originalBPId,
                                                  Original_B_P_Budget_USD__c = bpRequest.BP_Budget__c,
                                                  Operating_Unit__c = bpRequest.Operating_Unit__c,
                                                  Currency__c = bpRequest.Currency__c,
                                                  CH2M_Org_Code__c = bpRequest.CH2M_Org_Code__c,
                                                  Reason_for_Request_Justification__c = revisionData.requestJustification,
                                                  Status__C = Label.Revision_Initial_Status,
                                                  RecordTypeId = revisionRT.Id,
                                                  Level_1_Ops_Manager__c = bpRequest.Level_1_Ops_Manager__c,
                                                  Level_1_Sales_Manager__c = bpRequest.Level_1_Sales_Manager__c,
                                                  Regional_VP_of_Ops__c = bpRequest.Regional_VP_of_Ops__c,
                                                  Regional_VP_of_Sales__c = bpRequest.Regional_VP_of_Sales__c,
                                                  LOB_President__c = bpRequest.LOB_President__c,
                                                  SVP_GM_Ops__c = bpRequest.SVP_GM_Ops__c,
                                                  SVP_Sales__c = bpRequest.SVP_Sales__c,
                                                  Chairman_CEO__c = bpRequest.Chairman_CEO__c,
                                                  B_P_Coordinator__c =bpRequest.B_P_Coordinator__c,
                                                  Sales_Lead__c = bpRequest.Sales_Lead__c,
                                                  Request_Type__c = REQUEST_TYPE_REVISION,
                                                  Work_Hours__c = revisionData.additionalWorkHours,
                                                  Project_Code__c = bpRequest.Project_Code__c);
        
        try{
            insert bpRev;
        } catch (DmlException ex){
            throw new AuraHandledException(ex.getDmlMessage(0));
        }
        
        return bpRev.Id;
    } 
    
    
    /*
	*   Data structure to create B&P revision
	*/
    public class BPRequestData {
        
        @AuraEnabled
        public Decimal bpBudgetLocalCurrency {get; set;}
        
        @AuraEnabled
        public String operatingUnitLabel {get; set;}    
        
        @AuraEnabled
        public String status {get; set;}        
        
        @AuraEnabled
        public Decimal additionalBudgetLocal {get; set;}
        
        @AuraEnabled
        public Decimal additionalWorkHours {get; set;}      
        
        @AuraEnabled
        public String requestJustification {get; set;}
        
        @AuraEnabled
        public Boolean legacyBP {get; set;}
        
        @AuraEnabled
        public Boolean stdRecordType {get; set;}
        
        @AuraEnabled
        public String localCurrency {get; set;}

    }   
}