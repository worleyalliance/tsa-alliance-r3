/**************************************************************************************
Name: licenseTrackerUtilities                
Version: 1.0 
Created Date: March 10, 2018
Function: Provide various utility functions to support License Tracker app.

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Ray Zhao                  03/10/2018         Original
*************************************************************************************/
public class licenseTrackerUtilities {

    public Map<Id, Profile> profileMap;
    public Map<Id, PermissionSet> permSetMap;
    
    public licenseTrackerUtilities(){
        profileMap = new Map<Id, Profile>([Select Id, Name From Profile]); 
        permSetMap = new Map<Id, PermissionSet>([Select Id, Label From PermissionSet]); 
    }      
    public String getProfileNameById(Id profId){
        return profileMap.get(profId).Name; 
    }
    public String getPermSetNameById(Id permSetId){
		if(permSetMap.containsKey(permSetId)){
            return permSetMap.get(permSetId).Label; 
        }  
        else{
            return null;
        }
    }
    public String getPermSetNamesByIdSet(Set<Id> IdSet){
        String permSetNames =null;
        for (Id i : IdSet){
            permSetNames = permSetNames + ';' + permSetMap.get(i).Label;
        }
        return permSetNames.subString(5); 
    }
    public String getLicenseTypeByProfile(String varProfileName) {
        //return 'Full';
        if( varProfileName == 'View Only User' || varProfileName == 'LIMITED Jacobs Security' )
        	return 'View Only';
        else if( varProfileName == 'Limited User With Financials' || varProfileName == 'Limited User Without Financials')
        	return 'Limited';  
        else return 'Full'; 
    }    
}