/**************************************************************************************
Name: GroupAttritionTable_Service
Version: 1.0 
Created Date: 07.03.2017
Function: Service class used to deliver functionalities for GroupAttrition controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           07.03.2017         Original Version
*************************************************************************************/
public class GroupAttritionTable_Service extends DataTable_Service{

   /*
    * Mapping used to set proper order of the months
    */
    private final Map<String, Integer> MONTH_MAPPING = new Map<String, Integer>{
            'January'  => 1,
            'February' => 2,
            'March'    => 3,
            'April'    => 4,
            'May'      => 5,
            'June'     => 6,
            'July'     => 7,
            'August'   => 8,
            'September'=> 9,
            'October'  => 10,
            'November' => 11,
            'December' => 12
    };

    private final Map<String, String> totalRowFieldMapping = new Map<String, String>{
            AT_Segment_Attrition__c.Monthly_Attrition_Rate__c.getDescribe().getName() => 'MonthlyAttritionRateOperator',

            AT_Segment_Attrition__c.Starting_Workforce__c.getDescribe().getName() => 'StartingWorkforceOperator',

            AT_Segment_Attrition__c.Hires_Rehires_Transfers_In__c.getDescribe().getName() => getValueOpeartorService().DEFAULT_SUM_OPERATOR,

            AT_Segment_Attrition__c.Transfers_Out__c.getDescribe().getName() => 'PercentForTotalSeparationOperator',
            AT_Segment_Attrition__c.Non_Controllable__c.getDescribe().getName() => 'PercentForTotalSeparationOperator',
            AT_Segment_Attrition__c.Semi_Controllable__c.getDescribe().getName() => 'PercentForTotalSeparationOperator',
            AT_Segment_Attrition__c.Controllable__c.getDescribe().getName() => 'PercentForTotalSeparationOperator',

            AT_Segment_Attrition__c.Cont_Reason_Location__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Personal__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Job__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Compensation__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Promotion__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Retirement__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Supervisor__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Training__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Reason_Transfer__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.Cont_Gender_Male__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Gender_Female__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.NonCont_Gender_Male__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Gender_Female__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.Cont_Service_LT1__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_1To5__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_5To10__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_10To15__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_15To20__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_20To25__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Service_GT25__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.Cont_Age_LT20__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Age_20s__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Age_30s__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Age_40s__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Age_50s__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_Age_GTE60__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.Cont_EEO_AmIndian__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_Asian__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_Black__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_Hispanic__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_Hawaiian__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_Multi__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',
            AT_Segment_Attrition__c.Cont_EEO_White__c.getDescribe().getName() => 'PercentForCalculableSeparationOperator',

            AT_Segment_Attrition__c.NonCont_Reason_Contract__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_Temp__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_Health__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_Military__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_Misc__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_OtherTransfer__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_RIF__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_School__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_Cause__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Reason_CivilServ__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',

            AT_Segment_Attrition__c.NonCont_Gender_Female__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Gender_Male__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',

            AT_Segment_Attrition__c.NonCont_Age_LT20__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Age_20s__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Age_30s__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Age_40s__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Age_50s__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_Age_GTE60__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',

            AT_Segment_Attrition__c.NonCont_EEO_AmIndian__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_Asian__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_Black__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_Hispanic__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_Hawaiian__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_Multi__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',
            AT_Segment_Attrition__c.NonCont_EEO_White__c.getDescribe().getName() => 'PercentForNonControllableSeparationOperator',

            AT_Segment_Attrition__c.Month__c.getDescribe().getName() => 'MonthOperator'
    };

    private final Map<String, IFieldValueOperator> fieldValueOperatorMap = new Map<String, IFieldValueOperator>{
            'MonthlyAttritionRateOperator' => new MonthlyAttritionRateOperator(),
            'StartingWorkforceOperator' => new StartingWorkforceOperator(),
            'PercentForTotalSeparationOperator' => new PercentForTotalSeparationOperator(),
            'PercentForCalculableSeparationOperator' => new PercentForCalculableSeparationOperator(),
            'PercentForNonControllableSeparationOperator' => new PercentForNonControllableSeparationOperator(),
            'MonthOperator' => new MonthOperator(),
            'NameOperator' => new NameOperator()
    };

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    public TableData getSegmentAttritionData(
            String fieldSetName,
            Id recordId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName,
            Boolean totalRowPresent,
            Boolean isSumDefault,
            String additionalValueField
    ) {
        validateObjectAccesible(Schema.getGlobalDescribe().get(sObjectName).getDescribe().getName());

        List<FieldDescription> fieldDescriptions = prepareFieldDescriptions(fieldSetName, sObjectName);
        String query =
                buildSegmentAtritionQuery(
                        fieldDescriptions,
                        recordId,
                        fiscalYear,
                        fiscalYearField,
                        relationField,
                        sObjectName
                );

        //Execute query
        List<sObject> records = (List<sObject>) Database.query(query);
        updateValueOperatorService();
        TableData data = buildTableData(records, fieldDescriptions, totalRowPresent, isSumDefault, additionalValueField);
        if(!records.isEmpty()) {
            data.rows = sortMonthsBasedOnFiscalYear(data.rows, fiscalYear);
        }
        return data;
    }

   /*
    * Method prepares a query based on delivered data
    */
    private String buildSegmentAtritionQuery(
            List<DataTable_Service.FieldDescription> fieldDescriptions,
            Id recordId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName
    ){
        String query = super.buildQuery(
                fieldDescriptions,
                recordId,
                relationField,
                sObjectName
        );
        if(String.isNotBlank(fiscalYearField)) {
            query += ' AND ' + fiscalYearField + ' = ' + fiscalYear;
        }
        System.debug('query: '+query);
        return query;
    }

   /*
    * Method updates configuration for total row calculation for Group Attrition
    */
    private void updateValueOperatorService(){
        getValueOpeartorService().operatorsByKey.putAll(fieldValueOperatorMap);
        getValueOpeartorService().fieldNameByOperatorKey.putAll(totalRowFieldMapping);
    }

   /*
    * Months have to be sorted based on fiscal year start
    */
    private List<Map<String, Object>> sortMonthsBasedOnFiscalYear(List<Map<String, Object>> rows, Decimal fiscalYear){
        Integer orgFiscalMonth = findFiscalYearStartMonth(fiscalYear);
        List<Map<String, Object>> sortedRows = new List<Map<String, Object>>(rows);
        if(rows.size()==12) { // to prevent calculation when data is invalid
            for (Map<String, Object> row : rows) {
                if (row.containsKey('Month__c') && row.get('Month__c') != null) {
                    Integer rowMonth = MONTH_MAPPING.get((String) row.get('Month__c'));
                    Integer rowPosition = rowMonth - orgFiscalMonth;
                    if (rowPosition < 0) {
                        rowPosition += 12;
                    }
                    sortedRows.set(rowPosition, row);
                }
            }
        }
        return sortedRows;
    }

   /*
    * Find star month of given fiscal year and return it
    */
    private Integer findFiscalYearStartMonth(Decimal fiscalYear){
        Date firstOfGivenYear = Date.newInstance(fiscalYear.intValue(), 1, 1);
        Integer firstMonthOnFiscalYear = 10;
        if(fiscalYear >= 2017){
            List<Period> fiscalMonths = [
                    SELECT EndDate
                    FROM Period
                    WHERE   Type = 'Month'
                    AND Number = 1
                    AND EndDate >= :firstOfGivenYear
            ];
            firstMonthOnFiscalYear = fiscalMonths.isEmpty() ? firstMonthOnFiscalYear : fiscalMonths[0].EndDate.month();
        }

        return firstMonthOnFiscalYear;
    }

   /*
    * Sets custom value for specific fields
    */
    public override Object calculateFieldSpecificValue(Object fieldValue, FieldDescription fieldDescription, sObject obj){
        Object value;

        if(fieldDescription.fieldName == 'Monthly_Attrition_Rate__c'){
            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            value = String.valueOf(decValue.setScale(1)) + '%';
        } else {
        //CC for DE460, adding 3rd parameter to calculateFieldSpecificValue
            value = super.calculateFieldSpecificValue(fieldValue, fieldDescription, obj);
        }
        return value;
    }



   /*
    * Calculation specify for Monthtly Attrition field
    */
    public class MonthlyAttritionRateOperator implements DataTable_Service.IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer recordCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            Object value = String.valueOf(decValue.setScale(1)) + '%';

            Decimal calculableSeparationsTotal = totalRowValues.get('Calculable_Separations__c');
            calculableSeparationsTotal = calculableSeparationsTotal != null ? calculableSeparationsTotal : 0;

            Decimal startingWorkforceAvg = totalRowValues.get('Starting_Workforce__c');
            startingWorkforceAvg = startingWorkforceAvg != null ? startingWorkforceAvg : 1;

            recordCount = recordCount > 0 ? recordCount : 1;
            startingWorkforceAvg = (startingWorkforceAvg / recordCount).setScale(0);
            Decimal annualAttritionRate = ((calculableSeparationsTotal * 100) / startingWorkforceAvg) * (12 / recordCount);
            value += '(' + annualAttritionRate.setScale(2) + '%)';
            return value;
        }
    }

   /*
    * Calculation specify for Starting workforce field
    */
    public class StartingWorkforceOperator implements DataTable_Service.IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer recordCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            recordCount = recordCount > 0 ? recordCount : 1;
            Object value = (decValue/recordCount).setScale(0);
            return value;
        }
    }

   /*
    * Calculation specify for field that are compared to Total Seperation value
    */
    public class PercentForTotalSeparationOperator implements DataTable_Service.IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer recordCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            Decimal totalSeparations = totalRowValues.get('Total_Separations__c');
            totalSeparations = totalSeparations > 0 ? totalSeparations : 1;
            Object value =
                    String.valueOf(decValue.setScale(0)) +
                            '(' + String.valueOf(((Decimal) decValue / totalSeparations * 100).setScale(1))+'%)';
            return value;
        }
    }

   /*
    * Calculation specify for field that are compared to Caculable Seperation value
    */
    public class PercentForCalculableSeparationOperator implements DataTable_Service.IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer recordCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal calculableSeparationsTotal = totalRowValues.get('Calculable_Separations__c');
            calculableSeparationsTotal = calculableSeparationsTotal > 0 ? calculableSeparationsTotal : 1;
            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            Object value =
                    String.valueOf(decValue.setScale(0)) +
                            '(' + ((Decimal) decValue/calculableSeparationsTotal * 100).setScale(1)+'%)';
            return value;
        }
    }

   /*
    * Calculation specify for field that are compared to Non Controllable
    */
    public class PercentForNonControllableSeparationOperator implements DataTable_Service.IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer recordCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Decimal nonControllableSeparationsTotal = totalRowValues.get('Non_Controllable__c');
            nonControllableSeparationsTotal = nonControllableSeparationsTotal > 0 ? nonControllableSeparationsTotal : 1;
            Decimal decValue = (Decimal) fieldValue;
            decValue = decValue != null ? decValue : 0;
            Object value =
                    String.valueOf(decValue.setScale(0)) +
                            '(' + ((Decimal) decValue/nonControllableSeparationsTotal * 100).setScale(1)+'%)';
            return value;
        }
    }
   /*
    * Simple action for month column
    */
    public class MonthOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Object value = Label.Total;
            return value;
        }
    }

   /*
    * Simple action Name field
    */
    public class NameOperator implements IFieldValueOperator{
        public Object calculate(
                Object fieldValue,
                Integer groupAttritionCount,
                String additionalField,
                Map<String, Decimal> totalRowValues) {

            Object value = '';
            return value;
        }
    }

}