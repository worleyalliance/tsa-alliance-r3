/**************************************************************************************
Name:BPProjectReopenQuickActionService
Version: 1.0 
Created Date: 08/24/2017
Function: Updated B&P Status to active

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Rajamohan Vakati   08/24/2017       Original Version
* Madhu Shetty       10/06/2017       (US1076) Updated the CheckRequestAllowed function to allow only a request owner 
									                    or B&P Coordinator to close or re-open a B&P request. 
* Scott Stone		 10/25/2018		  (De809)  Updated Quick Action Service to check for null to avoid retrieving all B&P 
* Scott Stone		 12/10/2018		  (US2648) Updated the CheckRequestAllowed function to allow any user with a B&P Coordinator
*                                                       approval level to close or re-open a B&P request.
*************************************************************************************/

public class BPProjectReopenQuickActionService {

    private final String BP_STATUS_ACTIVE        = 'Active';
    private final String BP_STATUS_PENDING_CLOSE = 'Pending Close';
    private final String BP_STATUS_CLOSED        = 'Closed';
    private final String BP_REOPEN               = 'ReOpen';
    private final String BP_CLOSE                = 'Close';
    
    public void updateBPStatus(Id bpId , String statusName) { 

      if(bpId == null) {
          throw new AuraHandledException('Error - B&P Id is null');
      }
      if(statusName == null){
          return;
      }
        
      List<Jacobs_Project__c> projectUpdateList = new List<Jacobs_Project__c>();
      for (Jacobs_Project__c project: [SELECT Id, Project_Status__c FROM Jacobs_Project__c where B_P_Request__c =:bpId]){
          project.Project_Status__c =statusName;
          projectUpdateList.add(project);
      }   
      try{
          update projectUpdateList ;
      }catch(Exception e){
          throw new AuraHandledException(e.getMessage());
      }
        
    }
    
  public Boolean checkRequestAllowed(Id bpId, String changeRequested) { 

    if(bpId == null) {
        throw new AuraHandledException('Error - B&P Id is null');
    }
    if(changeRequested == null){
        return false;
    }
      
    //Get the B&P request based on the ID  
    B_P_Request__c bp = [Select Id, 
                                Status__c, 
                                Legacy_B_P__c, 
                                OwnerId, 
                                B_P_Coordinator__c 
                         From B_P_Request__c 
                         Where id=:bpId];

    //Boolean variable determinng if the change is allowed or not
    Boolean changeAllowed = true;

    //Boolean variable to indicate that the user has Administrators custom permission
    Boolean isAdministrator = UserDataProvider.isUserAnAdministrator(UserInfo.getUserId());
        
    //US2648: get approval level of user
    User currentUser = [Select Approval_Level__c From User Where Id = :Userinfo.getUserId()];
    
    //If the B&P is past history, no change is allowed. 
    if(bp.Legacy_B_P__c){
        changeAllowed = false;
    }
    else{
      //Check if the current logged in person is either the B&P owner or a B&P Coordinator (US2648) or 
      //a user with Profile/Permission Set containing 'Administrators' custom permission
      if((currentUser != null && currentUser.Approval_Level__c == 'B&P Coordinator') 
         	|| UserInfo.getUserId() == bp.OwnerId || isAdministrator){
        //If the B&P is not past history, but user is requesting to reopen an active B&P then change is not allowed
        if(bp.Status__c == BP_STATUS_ACTIVE && 
           changeRequested == BP_REOPEN)
        {
            changeAllowed = false;
        }
        //If the B&P is not past history, but user is requesting to close an closed/pending close B&P then change is not allowed
        else if(bp.Status__c == BP_STATUS_CLOSED && 
                changeRequested == BP_CLOSE)
        {
            changeAllowed = false;
        }
      }
      else{
        changeAllowed = false;
      }
    }

    return changeAllowed;
  }
       
}