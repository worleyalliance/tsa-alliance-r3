/**************************************************************************************
Name: classes
Version: 1.0 
Created Date: 05.05.2017
Function: Batch class for converting financial data in opportunities

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.05.2017         Original Version
*************************************************************************************/
public with sharing class ReachbackDataTable_C {

    private static ReachbackDataTable_Service service = new ReachbackDataTable_Service();

   /*
    * Method retrieves Group Attrition records for group and fiscal year
    * Fields retreived are based on field set.
    * Footer row is calculated. Total is default calculation any other is based on field name
    */
    @AuraEnabled
    public static DataTable_Service.TableData getData(
            String fieldSetName,
            Id recordId,
            Decimal fiscalYear,
            String fiscalYearField,
            String relationField,
            String sObjectName
    ) {
        return service.getReachbackData(
                fieldSetName,
                recordId,
                fiscalYear,
                fiscalYearField,
                relationField,
                sObjectName
        );
    }
}