/**************************************************************************************
Name: Jacobs_Project_TriggerHandler
Version: 1.0 
Created Date: 04.20.2017
Function: Jacobs Project Trigger handler

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Siva Katneni      04.20.2017      Original Version
* Raj Vakati        24.08.2017      US788
* Pradeep Shetty    07.09.2017      US715 - YTD project values
* Pradeep Shetty    04/25/2018      US1828: Prevent Child FY project creation when B&P creates the Project
* Scott Stone		01/10/2019		DE891: rollup Budgeted_Gross_Margin__c
*************************************************************************************/

public class Jacobs_Project_TriggerHandler {
    
    
  private static Jacobs_Project_TriggerHandler handler;
  //public final static String B_P_PROJECT_RECORD_TYPE_NAME     = 'B_P_Project';
  public final static String FIN_YTD_PROJECT_RECORD_TYPE_NAME = 'Finance_YTD';
  public final static String FIN_PROJECT_RECORD_TYPE_NAME     = 'Finance';  
  public final static String B_P_PROJECT_TYPE_NAME            = 'BID/PROPOSAL';
  public static String PROJECT_OBJECT_NAME                    = Jacobs_Project__c.sObjectType.getDescribe().getName();

  public final static String PROJECT_STATUS_ACTIVE            = 'Active';
  public final static String PROJECT_STATUS_REQ_REOPEN        = 'Request Re-Open';
  public final static String PROJECT_STATUS_REQ_CLOSURE       = 'Request Closure';
  
  //[PS 09/06 US715] Commenting out the method call. Not needed anymore.
  //private static RecordType bPProjectRecordType;
  
  //Record Type for Finance YTD
  private static RecordType finYTDProjectRecordType = getRecordType(PROJECT_OBJECT_NAME, FIN_YTD_PROJECT_RECORD_TYPE_NAME);

  //Record Type for Finance YTD
  private static RecordType finProjectRecordType = getRecordType(PROJECT_OBJECT_NAME, FIN_PROJECT_RECORD_TYPE_NAME);

  ////Private variable to ensure that the trigger recursion does not happen
  //private static boolean run = true;
  
  ////Method to set the run variable
  //public static boolean runOnce(){
  //  if(run)
  //  {
  //   run = false;
  //   return true;
  //  }
  //  else
  //  {
  //    return run;
  //  }
  //}
    
  /*
  * Singleton like pattern
  */
  public static Jacobs_Project_TriggerHandler getHandler(){
      if(handler == null){
          handler = new Jacobs_Project_TriggerHandler();
      }
      return handler;
  }
    
  /*
  * Getter for object record type
  */
  private static RecordType getRecordType(String objectName, String recordTypeDevName){

    return [SELECT Id FROM RecordType WHERE SobjectType = :objectName AND DeveloperName = :recordTypeDevName];
    
  }
    
    
  /*
  * Actions performed on Project before insert
  */
  public void onBeforeInsert(List<Jacobs_Project__c> trgNew){
    //[PS 09/06 US715] Commenting out the method call. Not needed anymore.
    //setRecordTypeForBPprojects(projects);
    //[PS 09/06 US715] Adding a method call to create parent PTD project if it is not present already      
    assignParentPTDProjects(trgNew);

    //calculate USD values based on local currency and exchange rates
    recalculatedValuesBasedOnChangeRates(trgNew);           
  }
    
  /*
  * Actions performed on Project before update
  */
  public void onBeforeUpdate(List<Jacobs_Project__c> trgNew){

    //calculate USD values based on local currency and exchange rates
    recalculatedValuesBasedOnChangeRates(trgNew);
      
  }
    
  /*
  * Actions performed on Project after insert
  */
  public void onAfterInsert(List<Jacobs_Project__c> trgNew){

    //[PS 09/06 US715] Calculate Amounts on the parent projects
    calculateParentProjectAmount(trgNew);

    //[PS 09/06 US715] Insert the first child project for B&P
    insertBPFYProject(trgNew);

    //Callout for B&P project creation
    projectCallout(trgNew);   

  }
  
  /*
  * Actions performed on Project after update
  */
  public void onAfterUpdate(List<Jacobs_Project__c> trgNew, List<Jacobs_Project__c> trgOld, Map<Id, Jacobs_Project__c> trgNewMap, Map<Id, Jacobs_Project__c> trgOldMap){

    //Calculate Amounts on the parent projects
    calculateParentProjectAmount(trgNew);

    //Update latest B&P child project when the parent project is updated
    updateBPFYProject(trgNew,trgOldMap);

    //Callout for B&P project update
    projectUpdateCallout(trgNew, trgOld, trgNewMap, trgOldMap);
        
  }
    
  /*
  * Actions performed on Project after delete
  */
  public void onAfterDelete(List<Jacobs_Project__c> trgOld){
    
    //Recalculate the Parent project amount
    calculateParentProjectAmount(trgOld);
      
  }

  /*
  * Actions performed on Project after undelete
  */
  public void onAfterUnDelete(List<Jacobs_Project__c> trgNew){
    
    //Recalculate the Parent project amount
    calculateParentProjectAmount(trgNew);      

  }

  /*
  * Method to make a project service callout
  */
  private void projectCallout(List<Jacobs_Project__c> trgNew){

    
    //[PS 09/08/2017 US715] Commenting this code. Using the finYTDProjectRecordType for triggering integration
    /*
    Set<ID> RTIds = new Set<ID>(); 
    for(RecordType rt : [select id from Recordtype where SobjectType='Jacobs_Project__c' and DeveloperName in ('B_P_Project')]){
        RTIds.add(rt.id);
    }*/

    system.debug('***Inside project call out');

    for(Jacobs_Project__c currentProject : trgNew){
      if(currentProject.Project_Status__c == PROJECT_STATUS_ACTIVE 
         && !test.isRunningTest() 
         && currentProject.RecordTypeId == finYTDProjectRecordType.Id
         && String.isBlank(currentProject.Project_ID__c))
      {
          projectQueueableHandler job = new projectQueueableHandler(currentProject);
          job.max = 3;
          ID jobID = System.enqueueJob(job);
          // ID jobID = System.enqueueJob(new projectQueueableHandler(Trigger.new[i]));  
      }
        
    }
  }
    
  /*
  * Method to make a project service callout on update of B&P Budget
  */
  private void projectUpdateCallout(List<Jacobs_Project__c> trgNew, List<Jacobs_Project__c> trgOld, Map<Id, Jacobs_Project__c> trgNewMap, Map<Id, Jacobs_Project__c> trgOldMap){

    system.debug('***Inside project Update call out');

    for(Jacobs_Project__c project : trgNew){
      //System.debug('====project in  update call '+project);

      //Call Integration service if Budget Changes or Status change is requested. Integration is triggered only for FY Projects
      if((project.B_P_Budget_Local__c != trgOldMap.get(project.id).B_P_Budget_Local__c || 
          project.nu_workhours__c != trgOldMap.get(project.id).nu_Workhours__c ||
          (project.Project_Status__c == PROJECT_STATUS_REQ_REOPEN && trgOldMap.get(project.id).Project_Status__c != PROJECT_STATUS_REQ_REOPEN) || 
          (project.Project_Status__c == PROJECT_STATUS_REQ_CLOSURE && trgOldMap.get(project.id).Project_Status__c!=PROJECT_STATUS_REQ_CLOSURE)) && 
         project.Project_Code__c != null && 
         project.Project_ID__c != null && 
         !project.Project_ID__c.startsWith('CH')&& 
         project.Project_ID_FY_Key__c !=null && 
         project.RecordTypeId == finYTDProjectRecordType.Id &&
         !test.isRunningTest())
      {   
        //system.debug(project); 
        projectQueueableHandler job = new projectQueueableHandler(project);
        job.max = 3;
        job.updateFlag = true;
        ID jobID = System.enqueueJob(job);
        System.debug('job==>'+jobID);
        // ID jobID = System.enqueueJob(new projectQueueableHandler(Trigger.new[i]));  
      }
    }
  }
  
  /*
  * Method to create/assign Parent PTD project to Finance YTD projects
  */
  private void assignParentPTDProjects(List<Jacobs_Project__c> jacobsProjects){


    system.debug('***Inside Assign parent PTD Projects');

    //Identify if this is a child or parent insert
    Boolean isChildInsert = true;

    //Map to contain all parent Project Ids and related Finance YTD projects
    Map<String, List<Jacobs_Project__c>> parentProjectMap = new Map<String, List<Jacobs_Project__c>>();

    if(!parentProjectMap.isEmpty()){
      parentProjectMap.clear();
    }  

    //Map of existing project's project Id and salesforce Id
    Map<String, Jacobs_Project__c> existingProjectMap = new Map<String, Jacobs_Project__c>();

    if(!existingProjectMap.isEmpty()){
      existingProjectMap.clear();
    }

    //List of parent projects to be created
    List<Jacobs_Project__c> parentProjectInsertList = new List<Jacobs_Project__c>();

    if(!parentProjectInsertList.isEmpty()){
      parentProjectInsertList.clear();
    }

    //Loop through all projects and get the Project Id.
    for(Jacobs_Project__c project : jacobsProjects){
        
      //Get the Project Id from the Project ID FY key
      if(String.isNotBlank(project.Project_ID_FY_Key__c))
      {
        project.Project_Id__c = project.Project_ID_FY_Key__c.substringBefore('F');
      }
      
      //Check that this is a FY project and is not the project inserted by B&P Approval
      if( project.RecordTypeId == finYTDProjectRecordType.Id && 
          String.isBlank(project.PTD_Project__c) ){ 

        if(parentProjectMap.containsKey(project.Project_ID__c)){
          parentProjectMap.get(project.Project_Id__c).add(project);
        }
        //For non B&P projects we need to create the parent if they don't exist and then assign them to the newly created parent
        else if(!parentProjectMap.containsKey(project.Project_ID__c)){
          //Create a parent Project record and add it to the map
          system.debug('**** before adding it to the map' + project);
          parentProjectMap.put(project.Project_ID__c, new List<Jacobs_Project__c> {project});
          system.debug('**** before adding it to the map' + parentProjectMap);
        }
        
      }
      else{
        isChildInsert = false;
      }
    } //END FOR

    //Execute the following only if the parentProjectMap is not empty
    if(!parentProjectMap.isEmpty() && isChildInsert){
      /* 
      * Get Projects corresponding to the IDs in the parentProjectMap
      */
      for(Jacobs_Project__c parentProject: [Select Id, 
                                                   Project_ID__c,
                                                   B_P_Budget_Local__c,
                                                   B_P_Budget__c
                                                   From Jacobs_Project__c
                                                   Where Project_ID__c in :parentProjectMap.keySet() 
                                                   And RecordTypeId = :finProjectRecordType.Id]){

        //Prepare the map of existing Project Id and it's corresponding record
        existingProjectMap.put(parentProject.Project_ID__c, parentProject);
        system.debug('*** Existing Prject map' + existingProjectMap);
      } //END FOR
        
      //Loop through the ParentProject Map and identify the projects that need to be created
      for(String projectId: parentProjectMap.keySet()){
        
        //If the project exists then assign the parent to the child
        if(existingProjectMap.containsKey(projectId)){
          for(Jacobs_Project__c currentFYProject: parentProjectMap.get(projectId)){
            currentFYProject.PTD_Project__c = existingProjectMap.get(projectId).Id;

            //Assign B&P Budget if the FY proejct is B&P type
            if(currentFYProject.Project_Type__c == B_P_PROJECT_TYPE_NAME){
              currentFYProject.B_P_Budget_Local__c = existingProjectMap.get(projectId).B_P_Budget_Local__c;
              currentFYProject.B_P_Budget__c = existingProjectMap.get(projectId).B_P_Budget__c;
            }
          }
        }
        //Else create a new Project and add it to the insert list
        else{
          system.debug('**** Preparing Insert List');
          //Get one of the child Finance YTD records to populate parent values
          Jacobs_Project__c financeYTDProject = parentProjectMap.get(projectId)[0];
          parentProjectInsertList.add(new Jacobs_Project__c(Name              = financeYTDProject.Project_Code__c,
                                                            Opportunity__c    = financeYTDProject.Opportunity__c,
                                                            lr_client__c      = financeYTDProject.lr_client__c,
                                                            Project_ID__c     = financeYTDProject.Project_ID__c,
                                                            Project_Code__c   = financeYTDProject.Project_Code__c,
                                                            RecordTypeId      = finProjectRecordType.id,
                                                            Local_Currency__c = financeYTDProject.Local_Currency__c,
                                                            Project_Type__c   = financeYTDProject.Project_Type__c));
        }
      } //END FOR

      //Insert all new projects remaining in the map
      if(parentProjectInsertList.size()>0)
      {
        try{
          insert parentProjectInsertList;

          //Loop through newly created project list and assign it to the related children
          for(Jacobs_Project__c currentParentProject: parentProjectInsertList){
            for(Jacobs_Project__c currentChildProject: parentProjectMap.get(currentParentProject.Project_Id__c)){
              currentChildProject.PTD_Project__c = currentParentProject.Id;
            }
          } //END FOR

        }
        catch(DmlException de){
          throw new ProjectTriggerException(de.getMessage());
        }        
      }
    }

  }

  /*
  * Calculates USD value for Finance YTD project fields based on the local currency and exchange rates 
  */
  private List<Jacobs_Project__c> recalculatedValuesBasedOnChangeRates(List<Jacobs_Project__c> jacobsProjects){

    system.debug('*** Inside recalculatedValuesBasedOnChangeRates');


    //Get the currency exchange rates from the custom setting
    Map<String, Currency_Exchange_Rates__c> exchangeRatesByCodeAndFiscalYear = Currency_Exchange_Rates__c.getAll();

    //Loop through the projects to calculate USD. This conversion should be done only for Finance YTD projects
    for(Jacobs_Project__c project : jacobsProjects){

      //Check the type of this project
      if(project.RecordTypeId == finYTDProjectRecordType.Id)
      {
        String exchangeRateKey = project.Local_Currency__c +'-'+ project.Fiscal_Year__c;
        Decimal exchangeRate = 1;
        
        if(exchangeRatesByCodeAndFiscalYear.containsKey(exchangeRateKey)){
          exchangeRate = exchangeRatesByCodeAndFiscalYear.get(exchangeRateKey).Conversion_Rate__c;
        }

        Decimal actualRevenueLocal      = project.Actual_Revenue_Local__c != null      ? project.Actual_Revenue_Local__c : 0;
        Decimal actualCostLocal         = project.Actual_Cost_Local__c != null         ? project.Actual_Cost_Local__c : 0;
        Decimal budgetedCostLocal       = project.Budgeted_Cost_Local__c != null       ? project.Budgeted_Cost_Local__c : 0;
        Decimal budgetedRevenueLocal    = project.Budgeted_Revenue_Local__c != null    ? project.Budgeted_Revenue_Local__c : 0;
        Decimal costPTDLocal            = project.Cost_PTD_Local__c != null            ? project.Cost_PTD_Local__c : 0;
        Decimal costYTDLocal            = project.Cost_YTD_Local__c != null            ? project.Cost_YTD_Local__c : 0;
        Decimal costCurrentMonthLocal   = project.Cost_Current_Month_Local__c != null  ? project.Cost_Current_Month_Local__c : 0;
        Decimal actualGrossMarginLocal  = project.Actual_Gross_Margin_Local__c != null ? project.Actual_Gross_Margin_Local__c : 0;
        
        project.Actual_Revenue__c      = actualRevenueLocal      * exchangeRate;
        project.Actual_Cost__c         = actualCostLocal         * exchangeRate;
        project.Budgeted_Cost__c       = budgetedCostLocal       * exchangeRate;
        project.Budgeted_Revenue__c    = budgetedRevenueLocal    * exchangeRate;
        project.Cost_PTD__c            = costPTDLocal            * exchangeRate;
        project.Cost_YTD__c            = costYTDLocal            * exchangeRate;
        project.Cost_Current_Month__c  = costCurrentMonthLocal   * exchangeRate;
        project.Actual_Gross_Margin__c = actualGrossMarginLocal  * exchangeRate;
      }
    }

    return jacobsProjects;
  }

  /*
  * Calculate the aggregate values in the PTD project
  */
  private void calculateParentProjectAmount(List<Jacobs_Project__c> jacobsProjects){ 

    system.debug('*** Inside calculateParentProjectAmount');    

    //Boolean to decide if this is a child upsert or parent upsert
    Boolean isChildUpsert = true;

    //List of Parent Project Ids
    List<Id> parentProjectIdList = new List<Id>();
    
    //List of Parent Project for update
    Set<Jacobs_Project__c> parentProjectUpdateList = new Set<Jacobs_Project__c>();
    
    //Loop through jacobs projects and get only Finance projects
    for(Jacobs_Project__c currentProject: jacobsProjects){
      //Check the record type id  
      if(currentProject.RecordTypeId == finYTDProjectRecordType.Id && 
         String.isNotBlank(currentProject.PTD_Project__c)){

        //Add it to the list. This list will be used to query Parent Projects
        parentProjectIdList.add(currentProject.PTD_Project__c);        

      }
      else{
        isChildUpsert = false;
      }
    } //END FOR

    if(isChildUpsert && !parentProjectIdList.isEmpty()){
      //Map of Parent projects
      Map<Id,Jacobs_Project__c> parentProjectMap = new Map<Id,Jacobs_Project__c>();

      //Loop through all these parent projects and reset the value to 0
      for(Jacobs_Project__c parentProject: [Select Id,
                                                   Actual_Cost__c,
                                                   Actual_Cost_Local__c,
                                                   Actual_Gross_Margin__c, 
                                                   Actual_Gross_Margin_Local__c,
                                                   Actual_Revenue__c,
                                                   Actual_Revenue_Local__c,
                                                   Budgeted_Cost__c,
                                                   Budgeted_Cost_Local__c,
                                                   Budgeted_Revenue__c,
                                                   Budgeted_Revenue_Local__c,
                                            	   Budgeted_Gross_Margin__c, //DE891 - roll up Budgeted_Gross_Margin__c
                                                   Cost_PTD__c,
                                                   Cost_PTD_Local__c,
                                                   Cost_YTD__c,
                                                   Cost_YTD_Local__c,
                                                   Fiscal_Year__c,
                                                   Billable_Hours__c,
                                                   Nonbillable_Hours__c
                                            From Jacobs_Project__c 
                                            Where Id in :parentProjectIdList])
      {
          //Reset all Amount values before adding up all child amount values
          parentProject.Actual_Cost__c               = 0;
          parentProject.Actual_Cost_Local__c         = 0;
          parentProject.Actual_Gross_Margin__c       = 0;
          parentProject.Actual_Gross_Margin_Local__c = 0;
          parentProject.Actual_Revenue__c            = 0;
          parentProject.Actual_Revenue_Local__c      = 0;
          parentProject.Billable_Hours__c            = 0;
          parentProject.Nonbillable_Hours__c         = 0;
          parentProject.Budgeted_Gross_Margin__c     = 0; //DE891 - roll up Budgeted_Gross_Margin__c
          parentProject.Budgeted_Hours__c		     = 0; //DE891 - roll up Budgeted_Hours__c
          
          //Add it to a map
          parentProjectMap.put(parentProject.Id, parentProject);
      }

      //Get Finance YTD projects for all these Parent Projects and loop through them to add all values up
      for(Jacobs_Project__c currentChildProject: [Select Id,
                                                         PTD_Project__c,
                                                         Actual_Cost__c,
                                                         Actual_Cost_Local__c,
                                                         Actual_Gross_Margin__c, 
                                                         Actual_Gross_Margin_Local__c,
                                                         Actual_Revenue__c,
                                                         Actual_Revenue_Local__c,
                                                         Budgeted_Cost__c,
                                                         Budgeted_Cost_Local__c,
                                                         Budgeted_Revenue__c,
                                                         Budgeted_Revenue_Local__c,
                                            	   		 Budgeted_Gross_Margin__c, //DE891 - roll up Budgeted_Gross_Margin__c
                                                  		 Budgeted_Hours__c, //DE891 - roll up Budgeted_Hours__c
                                                         Cost_PTD__c,
                                                         Cost_PTD_Local__c,
                                                         Cost_YTD__c,
                                                         Cost_YTD_Local__c,
                                                         Fiscal_Year__c,
                                                         Billable_Hours__c,
                                                         Nonbillable_Hours__c 
                                                  From Jacobs_Project__c 
                                                  Where PTD_Project__c in :parentProjectIdList])
      {
        system.debug('currentChildProject is null? ' + (currentChildProject == null));
        //Get the related Parent Project
        Jacobs_Project__c parentProject = parentProjectMap.get(currentChildProject.PTD_Project__c);
		system.debug('parentProject is null? ' + (parentProject == null));
        //Check for null before adding the value. Following fields need to be aggregated.
        parentProject.Actual_Cost__c               += currentChildProject.Actual_Cost__c               != null? currentChildProject.Actual_Cost__c               : 0;
        parentProject.Actual_Cost_Local__c         += currentChildProject.Actual_Cost_Local__c         != null? currentChildProject.Actual_Cost_Local__c         : 0;
        parentProject.Actual_Gross_Margin__c       += currentChildProject.Actual_Gross_Margin__c       != null? currentChildProject.Actual_Gross_Margin__c       : 0;
        parentProject.Actual_Gross_Margin_Local__c += currentChildProject.Actual_Gross_Margin_Local__c != null? currentChildProject.Actual_Gross_Margin_Local__c : 0;
        parentProject.Actual_Revenue__c            += currentChildProject.Actual_Revenue__c            != null? currentChildProject.Actual_Revenue__c            : 0;
        parentProject.Actual_Revenue_Local__c      += currentChildProject.Actual_Revenue_Local__c      != null? currentChildProject.Actual_Revenue_Local__c      : 0;
        parentProject.Billable_Hours__c            += currentChildProject.Billable_Hours__c            != null? currentChildProject.Billable_Hours__c            : 0;   
        parentProject.Nonbillable_Hours__c         += currentChildProject.Nonbillable_Hours__c         != null? currentChildProject.Nonbillable_Hours__c         : 0;
		//DE891 - roll up Budgeted_Gross_Margin__c
        if(parentProject.Budgeted_Gross_Margin__c == null) { parentProject.Budgeted_Gross_Margin__c = 0; }
        parentProject.Budgeted_Gross_Margin__c	   += currentChildProject.Budgeted_Gross_Margin__c     != null? currentChildProject.Budgeted_Gross_Margin__c     : 0;
		//DE891 - roll up Budgeted_Hours__c
        if(parentProject.Budgeted_Hours__c == null) { parentProject.Budgeted_Hours__c = 0; }
        parentProject.Budgeted_Hours__c	   += currentChildProject.Budgeted_Hours__c     			   != null? currentChildProject.Budgeted_Hours__c     : 0;

          
          
        //This logic ensures that the following fields always contain values from the latest FY project record.
        if(String.isBlank(parentProject.Fiscal_Year__c) || 
           (String.isNotBlank(parentProject.Fiscal_Year__c) && 
           String.isNotBlank(currentChildProject.Fiscal_Year__c) &&
           Integer.valueOf(parentProject.Fiscal_Year__c.right(4)) < Integer.valueOf(currentChildProject.Fiscal_Year__c.right(4)))){

          //Following fields should be updated from the latest FY record
          parentProject.Budgeted_Cost__c             = currentChildProject.Budgeted_Cost__c;
          parentProject.Budgeted_Cost_Local__c       = currentChildProject.Budgeted_Cost_Local__c;
          parentProject.Budgeted_Revenue__c          = currentChildProject.Budgeted_Revenue__c;
          parentProject.Budgeted_Revenue_Local__c    = currentChildProject.Budgeted_Revenue_Local__c;
          parentProject.Cost_PTD__c                  = currentChildProject.Cost_PTD__c;
          parentProject.Cost_PTD_Local__c            = currentChildProject.Cost_PTD_Local__c;
          parentProject.Cost_YTD__c                  = currentChildProject.Cost_YTD__c;
          parentProject.Cost_YTD_Local__c            = currentChildProject.Cost_YTD_Local__c;
          //DE891 - roll up Budgeted_Gross_Margin__c
		  parentProject.Budgeted_Gross_Margin__c     = currentChildProject.Budgeted_Gross_Margin__c;
          //DE891 - roll up Budgeted_Hours__c
		  parentProject.Budgeted_Hours__c     = currentChildProject.Budgeted_Hours__c;
        }


        ////Add it to the update list
        //if(!parentProjectUpdateList.contains(parentProject))
        //{
        //  parentProjectUpdateList.add(parentProject);
        //}  
      }

      //Update parent projects
      try{
        update parentProjectMap.values();
      }
      catch(DmlException de){
        throw new ProjectTriggerException(de.getMessage());
      }
    }
  }

  /*
  * Method to insert B&P Fiscal year project when the B&P is approved and creates the Parent Project
  */
  public void insertBPFYProject(List<Jacobs_Project__c> jacobsProjects){

    system.debug('*** Inside insertBPFYProject');    

    //List of child FY projects to be added
    List<Jacobs_Project__c> fyProjectInsertList = new List<Jacobs_Project__c>();

    if(!fyProjectInsertList.isEmpty()){
      fyProjectInsertList.clear();      
    }

    //This method should run only when the Parent B&P Project is created
    for(Jacobs_Project__c currentParentProject: jacobsProjects)
    {
      if(currentParentProject.Project_Type__c == B_P_PROJECT_TYPE_NAME && 
         currentParentProject.RecordTypeId == finProjectRecordType.Id && 
         String.isNotBlank(currentParentProject.B_P_Request__c) && 
         !String.isNotBlank(currentParentProject.Project_ID__c)) //US1828: Prevent Child FY project from getting created for CH parent projects

      {
          Jacobs_Project__c newFYProject = currentParentProject.clone();
          newFYProject.PTD_Project__c = currentParentProject.Id;
          newFYProject.RecordTypeId   = finYTDProjectRecordType.Id;
          newFYProject.B_P_Request__c = null;

          //Add it to the list of projects to be inserted
          fyProjectInsertList.add(newFYProject);
      }
    } //END FOR

    //Insert the list
    if(fyProjectInsertList.size() > 0){
      try
      {
        insert fyProjectInsertList;
      }
      catch(DmlException de)
      {
        throw new ProjectTriggerException(de.getMessage());
      }
    }

  }

  /*
  * Update latest B&P child project when the parent project is updated
  */
  public void updateBPFYProject(List<Jacobs_Project__c> jacobsNewProjects, Map<Id, Jacobs_Project__c> jacobsOldProjectsMap){

        system.debug('*** Inside updateBPFYProject');

    //List for updating child FY Projects
    List<Jacobs_Project__c> fyProjectUpdateList = new List<Jacobs_Project__c>();

    if(!fyProjectUpdateList.isEmpty()){
      fyProjectUpdateList.clear();      
    }

    //List of updated B&P Parent Projects with the latest FY project
    List<Jacobs_Project__c> bpParentProject = [SELECT Id,
                                                      B_P_Budget_Local__c,
                                                      B_P_Budget__c,
                                                      Project_Status__c,
                                                      Project_Type__c,
                                                      RecordTypeId,
                                                      B_P_Request__c,
	                                                  nu_Workhours__c,
                                                      (SELECT Id, 
                                                              B_P_Budget_Local__c,
                                                              B_P_Budget__c,
                                                              nu_Workhours__c,
                                                              Project_Status__c,
                                                              Fiscal_Year__c
                                                       From PTD_Project__r 
                                                       Order By Fiscal_Year__c Desc
                                                       Limit 1)
                                                From Jacobs_Project__c 
                                                Where Id In :jacobsNewProjects 
                                                And RecordTypeId = :finProjectRecordType.Id]; 

    //DEBUG
    // system.debug('****Parent Project' + bpParentProject);
    // system.debug('****Parent Project size' + bpParentProject[0].PTD_Project__r.size());
    // system.debug('****Parent Project value' + bpParentProject[0].PTD_Project__r.get(0));
    //This method should run only when the Parent B&P Project Budget or status (Request ReOpen/ Request Closure)is updated
    for(Jacobs_Project__c currentParentProject: bpParentProject)
    {
      if(currentParentProject.Project_Type__c == B_P_PROJECT_TYPE_NAME && 
         currentParentProject.RecordTypeId == finProjectRecordType.Id && 
         String.isNotBlank(currentParentProject.B_P_Request__c) && 
         (currentParentProject.B_P_Budget_Local__c != jacobsOldProjectsMap.get(currentParentProject.id).B_P_Budget_Local__c ||
          currentParentProject.nu_workhours__c != jacobsOldProjectsMap.get(currentParentProject.id).nu_Workhours__c || 
          (currentParentProject.Project_Status__c == PROJECT_STATUS_REQ_REOPEN && 
           jacobsOldProjectsMap.get(currentParentProject.id).Project_Status__c != PROJECT_STATUS_REQ_REOPEN) || 
          (currentParentProject.Project_Status__c == PROJECT_STATUS_REQ_CLOSURE && 
           jacobsOldProjectsMap.get(currentParentProject.id).Project_Status__c!=PROJECT_STATUS_REQ_CLOSURE)) && 
         !currentParentProject.PTD_Project__r.isEmpty())
      {
          Jacobs_Project__c childFYProject   = currentParentProject.PTD_Project__r.get(0);
          childFYProject.B_P_Budget_Local__c = currentParentProject.B_P_Budget_Local__c;
          childFYProject.B_P_Budget__c       = currentParentProject.B_P_Budget__c;
          childFYProject.nu_Workhours__c     = currentParentProject.nu_Workhours__c;
          childFYProject.Project_Status__c   = currentParentProject.Project_Status__c;

          //Add to the update list
          fyProjectUpdateList.add(childFYProject);
          system.debug('***Child Project***' +  childFYProject);
      }
    } //END FOR

    //Update the child projects
    if(!fyProjectUpdateList.isEmpty()){
      try
      {
        update fyProjectUpdateList;
      }
      catch (DmlException de)
      {
        throw new ProjectTriggerException(de.getMessage());
      }
    }   

  }

  public class ProjectTriggerException extends Exception {}   
  
  //[PS 09/06 US715] Commenting out the method call. Not needed anymore.
  /*private List<Jacobs_Project__c> setRecordTypeForBPprojects(List<Jacobs_Project__c> jacobsProjects){
    for(Jacobs_Project__c project : jacobsProjects){
      if(project.Project_Type__c == B_P_PROJECT_TYPE_NAME){
          project.RecordTypeId = getBPProjectRecordType().Id;
      }
    }
    return jacobsProjects;
  }*/
}