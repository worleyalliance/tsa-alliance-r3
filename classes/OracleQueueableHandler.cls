/**************************************************************************************
Name: OracleQueueableHandler
Version: 1.0 
Created Date: 05.07.2017
Function: Inteface for QueueableHandler objects

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.07.2017         Original Version
*************************************************************************************/
public interface OracleQueueableHandler extends Queueable{
    void setObject(sObject obj);
    void setRetrySyncLog(RetrySyncLog__c retrySyncLog);
}