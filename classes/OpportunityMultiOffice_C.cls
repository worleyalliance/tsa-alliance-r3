/**************************************************************************************
Name: OpportunityMultiOffice_C
Version: 1.0 
Created Date: 29.02.2017
Function: Retrieves/updates financial data from opportunity

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04.11.2017         Original Version
* Jignesh Suvarna   23/04/2018      US1509: Added method to check Admin permissions
*************************************************************************************/
public with sharing class OpportunityMultiOffice_C {

    private static OpportunityMultiOffice_Service service = new OpportunityMultiOffice_Service();

   /*
    *   Retrieves picklist values for Performance_Unit__c - picklist changed to lookup as part of US2010
    
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getPerformanceUnits(){
        return service.getPerformanceUnits();
    }*/

   /*
    *   Retrieves picklist values for Resource_Type__c
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getResourceTypes(){
        return service.getResourceTypes();
    }

   /*
    *   Retrieves picklist values for Worley_Capability_Subsector__c
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getSubSectors(){
        return service.getSubSectors();
    }    
    
   /*
    *   Retrieves MultiOffice split records for opportunity
    */
    @AuraEnabled
    public static List<OpportunityMultiOffice_Service.MultiOffice> getMultiOffices(Id opportunityId){
        return service.getMultiOffices(opportunityId);
    }

   /*
    *   Upates/inserts record of MutiOffice split for Oppotunity
    */
    @AuraEnabled
    public static Multi_Office_Split__c saveMultiOffice(String opportunityJSON, String multiOfficeJSON, Boolean shouldRecalculateSpread){
        return service.saveMultiOffice(opportunityJSON, multiOfficeJSON, shouldRecalculateSpread);
    }

   /*
    *   Removes MultiOffice record
    */
    @AuraEnabled
    public static void deleteMultiOffice(Id multiofficeId){
        service.deleteMultiOffice(multiofficeId);
    }

   /*
    *   Retrieves Revenue Type settings
    */
    @AuraEnabled
    public static Map<String, Resource_Type_Setting__mdt> getResourceTypeSettings(){
        return service.getResourceTypeSettings();
    }

   /*
    *   Retrieves Opportunity (Added for DE270)
    */
    @AuraEnabled
    public static Opportunity getOpportunity(Id recordId){
        return service.getOpportunity(recordId);
    }

    /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean hasEditPermission(){
        return Utility.checkAdminPermissions();
    }
     /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean isEditable(Id recordId){
        String objectName = recordId.getSObjectType().getDescribe().getName();
        return Utility.havRecordAccess(null,null,recordId);
    }    
}