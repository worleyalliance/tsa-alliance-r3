/**************************************************************************************
Name: ScheduleMilestone_TriggerHandler
Version: 1.1
Created Date: 03/04/2017
Function: Handler for Schedule Milestone Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Pradeep Shetty    03.04.2017      Original Version
* Manuel Johnson    20.07.2017      Update to set the Opportunity RFP and Proposal Due Date fields on create, edit or delete (US725)
* Madhu Shetty      15.05.2018      Added validation to check only one Award Milestone is added for each opportunity (US1892)
* Madhu Shetty      10.09.2018      Added validation to check user is not able to create Target Close date, Project Close date and 
                                    Client Visit date. Also, users should not be able to delete auto created milestones (US2467)
* Scott Stone       13.09.2018      Removedd validation to check that user cannot create Target Close date or Project Close Date.  Added
                                    function to set the Record Type to Read Only if the read only checkbox is checked.  Moved this to a trigger from a flow
                                    to make sure the Record Type is set correctly before Process Builder runs.  Renamed Madhu's function to ValidateDelete (US2467)
* Scott Stone       14.09.2018      Fix delete validation so that only auto created milestones cannot be deleted. (DE703)
*************************************************************************************/
public class Schedule_Milestone_TriggerHandler {
    
    private static Schedule_Milestone_TriggerHandler handler;
    
    /*
    * Singleton like pattern
    */
    public static Schedule_Milestone_TriggerHandler getHandler(){
        if(handler == null){
            handler = new Schedule_Milestone_TriggerHandler();
        }
        return handler;
    }
    
    /*
    * Actions performed after the Schedule Milestone record is inserted, updated or deleted
    */
    public void onAllChanges(List<Schedule_Milestone__c> milestones){
        resetOpportunityFields(milestones);
    }
    
    /*
    * Actions performed before the Schedule Milestone is inserted or updated
    */
    public void onBeforeUpsert(List<Schedule_Milestone__c> milestones) {
        AllowOnlyOneMilestoneType(milestones);
    }
    
    /*
     * Actions performed before the Schedule Milestone is inserted
     */
    public void onBeforeInsert(List<Schedule_Milestone__c> milestones){
        SetReadOnlyRecordTypeOnAutoCreatedMilestones(milestones);
    }
    
    /*
    * Actions performed before the Schedule Milestone is deleted
    */
    public void onBeforeDelete(List<Schedule_Milestone__c> milestones) {
        ValidateDelete(milestones);
    }
    
    /*
     * Method to set the Milestone's Record Type to 'Read Only Milestone' if the milestone is auto created
     */ 
    private void SetReadOnlyRecordTypeOnAutoCreatedMilestones(List<Schedule_Milestone__c> milestones){
        
        List<RecordType> recordTypes = Utility.getRecordTypes(milestones.getSObjectType().getDescribe().getName(), new List<string>{'Schedule_Milestone_Read_Only'});
        RecordType readonlyRecordType = recordTypes.get(0);
        
        if(readonlyRecordType != null){      
            for(Schedule_Milestone__c sm : milestones){
                if(sm.Auto_Created_Milestone__c){
                    sm.RecordTypeId = readonlyRecordType.Id;
                }
            }    
        }
    }
    
    /*
    * Method to check that users should not be able to delete Auto-Created Milestones.
    */
    private void ValidateDelete(List<Schedule_Milestone__c> milestones){
        for(Schedule_Milestone__c sm : milestones){
            //DE703 - allow deletion of user created milestones
            if((sm.Auto_Created_Milestone__c) &&
               !userinfo.getprofileid().equals('00e1U000001B0zjQAC')
            ){
                //Show a validation error if the user is trying to delete an auto create milestone
                sm.adderror(Label.CannotDeleteAutoCreatedMilestones);
            }
        }
    }
    
    /*
    * Method to set the parent Opportunity RFP and Proposal Due Date fields
    */
    private void resetOpportunityFields(List<Schedule_Milestone__c> milestones){
        Map<String, String> opportunityFieldMap = new Map<String, String>();
        List<Id> opportunityIds = new List<Id>();
        List<AggregateResult> milestoneAR = new List<AggregateResult>();
        List<Opportunity> oppsToUpdateList = new List<Opportunity>();
        
        //Get a map of Milestone Types and corresponding Opportunity field name that we need to update
        for(Opportunity_Milestone_Rollup__mdt field : [
            SELECT Aggregate_Result_Field__c, Milestone_Type__c 
            FROM Opportunity_Milestone_Rollup__mdt
        ]) {
            opportunityFieldMap.put(String.valueOf(field.Milestone_Type__c), String.valueOf(field.Aggregate_Result_Field__c));
        }
        
        //Get the list of Schedule Milestone Parent Opportunities to update
        if(opportunityFieldMap != null && !opportunityFieldMap.isEmpty()){
            for(Schedule_Milestone__c sm : milestones){
                if(sm.Opportunity__c != null && opportunityFieldMap.get(sm.Milestone_Type__c) != null){
                    opportunityIds.add(sm.Opportunity__c);
                }
            }
        }
        
        //Find the min date value for each Milestone Type per Opportunity
        for(AggregateResult ar : [
            SELECT Opportunity__c, Milestone_Type__c, IsClosed__c, MAX(End_Date__c)endDate
            FROM Schedule_Milestone__c
            WHERE Opportunity__c IN :opportunityIds
            GROUP BY Opportunity__c, Milestone_Type__c, IsClosed__c
        ]){
            //Only add Milestone Types that have been defined
            if(opportunityFieldMap.get((String)ar.get('Milestone_Type__c')) != null){
                milestoneAR.add(ar);
            }
        }

        //Update the Opportunity fields with the min date value        
        for(Opportunity o : [SELECT Id FROM Opportunity WHERE Id IN :opportunityIds]){
            
            //Need to loop through Milestones defined to see if any are missing and set them to null
            for(String milestoneType : opportunityFieldMap.keySet()){
                String fieldName = opportunityFieldMap.get(milestoneType);
                Boolean milestoneExists = false;
                
                for(AggregateResult nextMilestone : milestoneAR){
                    Id milestoneOppId = (Id)nextMilestone.get('Opportunity__c');
                    String nextMilestoneType = (String)nextMilestone.get('Milestone_Type__c');
                    Boolean nextMilestoneClosed = (Boolean)nextMilestone.get('IsClosed__c');
                    
                    if(o.Id == milestoneOppId && milestoneType == nextMilestoneType && nextMilestoneClosed == false){
                        o.put(fieldName, (Date)nextMilestone.get('endDate'));
                        milestoneExists = true;
                    } else if(o.Id == milestoneOppId && milestoneType == nextMilestoneType && milestoneExists == false){
                        o.put(fieldName, (Date)nextMilestone.get('endDate'));
                        milestoneExists = true;
                    }
                }
                if(milestoneExists == false) o.put(fieldName, null);
            }
            oppsToUpdateList.add(o);
        }
        
        update oppsToUpdateList;
    }
    
    /*
    * Method to validate that Opportunity has only one Award Milestone
    */
    private void AllowOnlyOneMilestoneType(List<Schedule_Milestone__c> milestoneRecords) {
        //Loop through the list of Milestone Types for which we need to check uniqueness
        for(Opportunity_Milestone_Rollup__mdt field : [
            SELECT Occurs_Only_Once__c, Milestone_Type__c 
            FROM Opportunity_Milestone_Rollup__mdt]) 
        {
            if(field.Occurs_Only_Once__c){
                SET<Id> opportunityIds = new SET<Id>();
                //Get the list of Schedule Milestone Parent Opportunities to update
                for(Schedule_Milestone__c sm : milestoneRecords){
                    if(sm.Opportunity__c != null && sm.Milestone_Type__c == String.valueOf(field.Milestone_Type__c)){
                        if(opportunityIds.contains(sm.Opportunity__c)){
                            sm.addError(string.format(System.Label.Milestone_Type_Already_Exists, new string[] {String.valueOf(field.Milestone_Type__c)}));
                        }
                        opportunityIds.add(sm.Opportunity__c);
                    }
                }
                
                //Loop through milestones for each opportunity to check uniquness 
                for(Schedule_Milestone__c  sl : [SELECT Id, Opportunity__c FROM Schedule_Milestone__c 
                                                 WHERE Milestone_Type__c = :String.valueOf(field.Milestone_Type__c) 
                                                 and Opportunity__c in :opportunityIds]){
                    for(Schedule_Milestone__c sm : milestoneRecords){
                        if(sl.Opportunity__c == sm.Opportunity__c && sl.ID != sm.id){
                            sm.addError(string.format(System.Label.Milestone_Type_Already_Exists, new string[] {String.valueOf(field.Milestone_Type__c)}));
                        }
                    }
                }
            }
        }
    }

}