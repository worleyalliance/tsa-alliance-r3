/**************************************************************************************
Name: Documents_C_Test
Version: 1.0 
Created Date: 24.02.2017
Function: Test class for Documents_C

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Stefan Abramiuk           24.02.2017         Original Version
*************************************************************************************/
@isTest
private class Documents_C_Test {
/*
    @isTest
    private static void shouldRetrieveFilesFromGivenFolderPath() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = false;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        Test.startTest();
        List<SharePointConfiguration.File> results = Documents_C.retrieveDocuments('test', acc.Id, 'Account');
        Test.stopTest();

        //then
        NamedCredential namedCredential = [SELECT Endpoint FROM NamedCredential WHERE DeveloperName = :SharePointConfiguration.NAMED_CREDENTIAL_DEV_NAME];
        System.assertEquals(1, results.size());
        System.assertNotEquals(null, results[0].LinkingUrl);
        System.assert(results[0].LinkingUrl.contains(namedCredential.Endpoint));
    }

    @isTest
    private static void shouldFailToRetrieveFilesFromFolder() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        List<SharePointConfiguration.File> results;
        try{
            Test.startTest();
            results = Documents_C.retrieveDocuments('test', acc.Id, 'Account');
            Test.stopTest();
            //then
            System.assert(false, 'Should throw an exception');

        }catch (Exception ex){
            System.debug('ex: '+ex);
            System.assert(ex instanceof AuraHandledException, 'Wrong exception type');
        }
    }

    @isTest
    private static void shouldFailToRetrieveFilesFromFolderWhenRecordIdIsNull() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        List<SharePointConfiguration.File> results;
        try{
            Test.startTest();
            results = Documents_C.retrieveDocuments('test', null, 'Account');
            Test.stopTest();

            //then
            System.assert(false, 'Should throw an exception');

        }catch (Exception ex){
            System.assert(ex instanceof AuraHandledException, 'Wrong exception type');
        }
    }

    @isTest
    private static void shouldFailToRetrieveFilesFromFolderWhenRecordDontExist() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        //when
        List<SharePointConfiguration.File> results;
        try{
            Test.startTest();
            results = Documents_C.retrieveDocuments('test', '001n000000CeOpqAAF', 'Account');
            Test.stopTest();

            //then
            System.assert(false, 'Should throw an exception');

        }catch (Exception ex){
            System.assert(ex instanceof AuraHandledException, 'Wrong exception type');
        }
    }


    @isTest
    private static void shouldUploadDocumentViaSharePointClient() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = false;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        try {
            Test.startTest();
            Documents_C.uploadDocument(
                    'test.txt',
                    'some value',
                    'test/test123',
                    acc.Id,
                    'Account',
                    'notImportant',
                    0,
                    true,
                    true,
                    false);
            Test.stopTest();

            //then
        }catch (Exception ex){
            System.assert(false, 'Should not throw any exception');
        }
    }

    @isTest
    private static void shouldFailToUploadDocumentViaSharePointClient() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        try {
            Test.startTest();
            Documents_C.uploadDocument(
                    'test.txt',
                    'some value',
                    'test/test123',
                    acc.Id,
                    'Account',
                    'notImportant',
                    0,
                    true,
                    true,
                    false);
            Test.stopTest();

            //then
            System.assert(false, 'Should throw an exception');

        }catch (Exception ex){
            System.assert(ex instanceof AuraHandledException, 'Wrong exception type');
        }
    }

    @isTest
    private static void shouldDeleteDocumentViaSharePointClient() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = false;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        try {
            Test.startTest();
            Documents_C.deleteDocument('test.txt', 'test/test123', acc.Id, 'Account');
            Test.stopTest();

            //then
        }catch (Exception ex){
            System.assert(false, 'Should not throw any exception');
        }
        System.assertEquals(1, mock.executionCounter);
    }

    @isTest
    private static void shouldFailToDeleteDocumentViaSharePointClient() {
        //given
        SharePoint_Mock mock = new SharePoint_Mock();
        mock.isError = true;
        Test.setMock(HttpCalloutMock.class, mock);

        TestHelper.AccountBuilder accountBuilder = new TestHelper.AccountBuilder();
        Account acc = accountBuilder.build().save().getRecord();

        //when
        try {
            Test.startTest();
            Documents_C.deleteDocument('test.txt', 'test/test123', acc.Id, 'Account');
            Test.stopTest();

            //then
            System.assert(false, 'Should throw an exception');

        }catch (Exception ex){
            System.assert(ex instanceof AuraHandledException, 'Wrong exception type');
        }
        System.assertEquals(1, mock.executionCounter);
    }*/
}