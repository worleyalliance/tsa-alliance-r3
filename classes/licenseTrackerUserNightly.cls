/***************************************************************************************
Name: licenseTrackerUserNightly                original Name: updateUserOnLicenseReq
Version: 1.0 
Created Date: March 6, 2018
Function: Update License Request record with values from User record.

Modification Log:
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
* Developer                 Date               Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
* Linda Reinholtz           03/06/2018         Original Version
* Ray Zhao                  03/10/2018         Changed name, changed first map to list
* Linda Reinholtz           04/13/2018         Updated to accomodate BIAF License by Business Unit
*****************************************************************************************/

global class licenseTrackerUserNightly implements Schedulable {

    public void execute (SchedulableContext SC)  {
    
        // Initialize Log Entries and licenseTrackerUtilities class
        List<Log__c> logEntries = new List<Log__c>();  
        licenseTrackerUtilities ltu = new licenseTrackerUtilities();
        
        // License Request List to work with
        List<License_Request__c> lrList = [SELECT Id, Name, Business_Unit__c, Line_of_Business__c, Selling_Unit__c, Requested_Profile__c, Requested_Roles__c, Provisioned_User__c, License__c, License_Type_Requested__c FROM License_Request__c WHERE Provisioned_User_Status__c = TRUE AND Provisioned_User__r.LastModifiedDate >= YESTERDAY AND RecordTypeId = :Schema.SObjectType.License_Request__c.getRecordTypeInfosByName().get('Master License Request').getRecordTypeId()]; 
        System.debug(loggingLevel.INFO,'Number of Record LR List:' + lrList.Size());
        // Adding Related UserId's in a Set
        Set<Id> sUids = new Set<Id>();
        for (License_Request__c lr1 : lrList){
            if (!sUids.contains(lr1.Provisioned_User__c)) {
                sUids.add(lr1.Provisioned_User__c);
            }
            else {
                System.debug(loggingLevel.INFO,'This LR duped: <' + lr1.Name + '>');
            }             
        }
        System.debug(loggingLevel.INFO,'Number of Ids in sUids set:' + sUids.Size());   
        // USER MAP (um) 
        Map<ID, User> um = new Map<ID, User> (
            [SELECT ID, Business_Unit__c, Line_of_Business__c, Selling_Unit__c, ProfileId, Profile.Name, UserRoleId, UserRole.Name FROM User WHERE Id IN :sUids AND isActive = True AND LastModifiedDate >= YESTERDAY]); 
        System.debug(loggingLevel.INFO,'Number of Record User Map:' + um.Size());
        // LICENSES MAP (lm) 
        List<Licenses__c> licList = [SELECT Name, Id FROM Licenses__c];
        Map<String, Id> lm = new Map<String, Id>();
        for(Licenses__c lic : licList) {
            lm.put(lic.Name, lic.Id);
        }
        System.debug(loggingLevel.INFO,'Number of Record License Map:' + lm.Size());
        
        // LOOP OVER RECORDS - License Request Lists
        for( License_Request__c lr: lrList) {
            if (um.get(lr.Provisioned_User__c) != NULL) {
                IF(lr.Business_Unit__c <> um.get(lr.Provisioned_User__c).Business_Unit__c){
                    lr.Business_Unit__c = um.get(lr.Provisioned_User__c).Business_Unit__c;
                }
                IF(lr.Line_of_Business__c <> um.get(lr.Provisioned_User__c).Line_of_Business__c){
                    lr.Line_of_Business__c = um.get(lr.Provisioned_User__c).Line_of_Business__c;
                }
                IF(lr.Selling_Unit__c <> um.get(lr.Provisioned_User__c).Selling_Unit__c){
                    lr.Selling_Unit__c = um.get(lr.Provisioned_User__c).Selling_Unit__c;
                }
                IF(lr.Requested_Profile__c <> um.get(lr.Provisioned_User__c).Profile.Name){
                    lr.Requested_Profile__c = um.get(lr.Provisioned_User__c).Profile.Name;
                }
                IF(lr.Requested_Roles__c <> um.get(lr.Provisioned_User__c).UserRole.Name){
                    lr.Requested_Roles__c = um.get(lr.Provisioned_User__c).UserRole.Name;
                }
                IF(lr.License_Type_Requested__c <> ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name)){
                    lr.License_Type_Requested__c = ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name);
                }
                IF(um.get(lr.Provisioned_User__c).Line_of_Business__c == 'BIAF')
                    {
                        IF(lr.License__c <> lm.get(ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name) + ' - ' + um.get(lr.Provisioned_User__c).Business_Unit__c))
                            {
                                lr.License__c = lm.get(ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name) + ' - ' + um.get(lr.Provisioned_User__c).Business_Unit__c);
                            }
                     }
                     else {
                        IF(lr.License__c <> lm.get(ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name) + ' - ' + um.get(lr.Provisioned_User__c).Line_of_Business__c))
                            {
                                lr.License__c = lm.get(ltu.getLicenseTypeByProfile(um.get(lr.Provisioned_User__c).Profile.Name) + ' - ' + um.get(lr.Provisioned_User__c).Line_of_Business__c);
                            }
                    } 
                }
            else {
                System.debug(loggingLevel.INFO,'That\'s the null pointer: ' + lr.Provisioned_User__c);            
                logEntries.add(new Log__c(AppName__c = 'License Management',
                                          ClassName__c = 'licenseTrackerUserNightly',
                                          Event__c = 'Night Scheduleable',
                                          EventType__c = 'AppException',
                                          Operation__c = 'Lookup User Id in the User Map',
                                          SFID__c = lr.Id,
                                          Exception__c = 'User Id ' + lr.Provisioned_User__c + ' not found in Usermap'));
            }
        }
        
        /* UPDATE RECORDS */
        try {
            update lrList;
            //Insert log entries
            if (logEntries.size()>0) insert logEntries;
        }
        catch (Exception e){
            System.Debug(e);
            logEntries.add(new Log__c(AppName__c = 'License Management',
                                      ClassName__c = 'licenseTrackerUserNightly',
                                      Event__c = 'Night Scheduleable',
                                      EventType__c = 'AppException',
                                      Operation__c = 'Unknown exception occurred',
                                      Exception__c = 'Exception: ' + e));
            insert logEntries;
        }
    }
    /// SCHEUDULE - BATCH JOB - RUNS AT 1AM 
    /// updateUserOnLicenseReq DailyBatch = new updateUserOnLicenseReq();
    /// String cronStr = '0 0 1 * * ?';
    /// System.schedule('Daily Update', cronStr,DailyBatch);
}