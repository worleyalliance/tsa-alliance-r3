/**************************************************************************************
Name: JacobsOracleConfigurationHelper
Version: 1.0 
Created Date: 05.07.2017
Function: Helper class used to manage Oracle sync service configuration

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk           05.07.2017         Original Version
*************************************************************************************/
public with sharing class JacobsOracleConfigurationHelper {

    private static Map<String, String> valueByType;

    private static void buildConfigMap(){
        List<Jacobs_Oracle_Configuration__mdt> configurations = [SELECT Value__c, DeveloperName FROM Jacobs_Oracle_Configuration__mdt];
        valueByType = new Map<String, String>();
        for(Jacobs_Oracle_Configuration__mdt config : configurations){
            valueByType.put(config.DeveloperName, config.Value__c);
        }
    }

    public static String getConfiguration(String key){
        if(valueByType == null){
            buildConfigMap();
        }
        return valueByType.get(key);
    }
}