/**************************************************************************************
Name: TaskOrderDocs_ControllerTest
Version:
Created Date: 
Function: Test class for TaskOrderDocs_Controller

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Chris Cornejo        7/25/2017    Updated to test new updates to Controller, mainly addition of custom setting
* Chris Cornejo        8/4/2017     Task Order Document Library Record LDT Table populated when Project/Contract/Task Order Name field on the LIbrary record has the TO id
*************************************************************************************/
@isTest
private class TaskOrderDocs_ControllerTest{

    static testMethod void test_TaskOrderDocs() {

       Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       Map <String,Schema.RecordTypeInfo> recordTypeMapProj = Jacobs_Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       
       Jacobs_Project__c proj = new Jacobs_Project__c();
       proj.Name = 'testProj';
       proj.Project_Start_Date__c = System.today();
       if(recordTypeMapProj.containsKey('Task Orders')) 
       {
           proj.RecordTypeId= recordTypeMapProj.get('Task Orders').getRecordTypeId();
       }
       insert proj;
       system.debug('proj.id=' + proj.Id);
       
       Library__c lib = new Library__c();
       lib.Name = 'testLib';
       lib.cb_active__c = true;
       lib.dt_submittal_date__c = System.today();
       lib.lr_project__c = proj.Id;
       if(recordTypeMap.containsKey('Other')) 
       {
           lib.RecordTypeId= recordTypeMap.get('Other').getRecordTypeId();
       }
       insert lib;
       system.debug('library.id='+lib.Id);
       
        ApexPages.currentPage().getParameters().put('Id', proj.Id);
        LDT_Lightning_Filter__c TOsetting = new LDT_Lightning_Filter__c();
        TOsetting.Name = 'Test Setting';
        TOsetting.Label_del__c = 'Other';
        TOsetting.Object_API__c = 'Jacobs_Project__c';
        TOsetting.OBJ_RecordType_API_Name__c = 'task_orders';
        TOsetting.Coverage__c = 'Include';
        TOsetting.Active__c = true;
        insert TOsetting;
           

        List<Library__c> l = TaskOrderDocs_Controller.getTOsOfProject(proj.Id);
        List <String> m = TaskOrderDocs_Controller.fetchRecordTypeValues();
        List<Library__c> n = TaskOrderDocs_Controller.getCBAsOfProjectwRecordType(proj.Id,'All');
        List<Library__c> o = TaskOrderDocs_Controller.getCBAsOfProjectwRecordType(proj.Id,'Other');
           
        Test.StartTest();
           system.assert(l.size() > 0);
           system.assert(m.size() > 0);
           system.assert(n.size() > 0);
           system.assert(o.size() > 0);
           system.assertequals(lib.RecordTypeId, TaskOrderDocs_Controller.getRecTypeId('Other'));
        Test.StopTest();
       }
}