/**************************************************************************************
Name:NewRecordCreation_Service
Version: 1.0 
Created Date: 03/13/2018
Function: Service class for RecordTypeSelection_C class for creating new records

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                
* Pradeep Shetty    03/13/2018      Original Version
* Pradeep Shetty   08/20/2018      DE633: Sorting order of Record types is now a parameter
*************************************************************************************/
public with sharing class RecordTypeSelection_Service {
  /*
  * Returns list of recordtypes to be shown on recordtype selection
  */
  public List<RecordType> retrieveRecordTypes(String objectAPI, String recordTypeNames, String sortOrder){
    
    //List of recordType developer names
    List<String> recordTypeNameList = new List<String>();

    //List of recordtypes
    List<RecordType> recordTypeList = new List<RecordType>();  

    //DE633: Initialise the query
    String queryString  = 'Select Name, Id, Description, DeveloperName From RecordType Where SobjectType = :objectAPI';

    //If recordTypeNames is not empty then split the string
    if(String.isNotBlank(recordTypeNames)){
      recordTypeNameList = recordTypeNames.split(',');

      queryString = queryString + ' ' + 'And DeveloperName in :recordTypeNameList';

      //recordTypeList = [Select Name, 
      //                         Id,
      //                         Description,
      //                         DeveloperName 
      //                  From RecordType 
      //                  Where SobjectType = :objectAPI 
      //                  And DeveloperName in :recordTypeNameList
      //                 	Order by Name desc];
    }
    if(String.isNotBlank(sortOrder)){
      queryString = queryString + ' ' + 'Order by Name' + ' ' + sortOrder;
    }
    else{
      queryString = queryString + ' ' + 'Order by Name ASC';
    }
    //else{
    //  recordTypeList = [Select Name, 
    //                           Id, 
    //                           Description,
    //                           DeveloperName 
    //                    From RecordType 
    //                    Where SobjectType = :objectAPI];
    //}

    //Execute the query
    recordTypeList = Database.query(queryString);

    return recordTypeList;
  }

  /*
  * Get the object name for a given Object API Name
  */
  public String getObjectName(String objectAPI){

    try{
      // Make the describe call based on the objectAPI
      Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new String[]{objectAPI});

      if(results!=null && results.size() > 0){
        return results[0].getLabel();      
      }
      else{
        return null;
      }

    }
    catch (Exception e){
      //Return null
      return null;
    }

  }
}