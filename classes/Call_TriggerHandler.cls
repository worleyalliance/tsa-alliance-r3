/**************************************************************************************
Name: Call_TriggerHandler
Version: 1.0
Created Date: 08/27/2018
Function: Handler for Call_Trigger

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  

*************************************************************************************/
public class Call_TriggerHandler {
    
    private static Call_TriggerHandler handler;
	// private static boolean run = true; 
    
    /*
    * Singleton like pattern
    */
    public static Call_TriggerHandler getHandler() {
        if (handler == null) {
            handler = new Call_TriggerHandler();
        }
        return handler;
    }
    
    /*
    * Actions performed on Call records before delete
    */
    public void onBeforeDelete(List<Call__c> calls) {      
        System.debug(calls);
        deleteGeneratedClientVisits(calls);
    }
        
    /*
    * Method deletes auto generated Client Visit milestones associated witht he calls.
    */
    private void deleteGeneratedClientVisits(List<Call__c> calls) {
        
        List<Schedule_Milestone__c> generatedClientVisits = [SELECT Id FROM Schedule_Milestone__c 
                                        					WHERE Auto_Created_Milestone__c = true 
                                                          	AND Milestone_Type__c = 'Client Visit' 
                                                            AND Call__c IN : calls];
        
		System.debug('generated client visit Milestones: ' + generatedClientVisits);
        delete generatedClientVisits;
    }
    
    //Check for recursion
    // public static boolean runOnce(){
    //     if (run){
    //         run = false;
    //         return true;
    //     } else {
    //         return run;
    //     }
    // }
}