/**************************************************************************************
Name: OpportunityEstimations_C
Version: 1.0 
Created Date: 29.02.2017
Function: Retrieves/updates financial data from opportunity

Modification Log:
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
* Developer         Date            Description
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --                  
* Stefan Abramiuk   04.11.2017         Original Version
* Jignesh Suvarna   03/12/2018      US1509: Data Stewards/Sales Super User - Edit Access
* Jignesh Suvarna 	0529/2018		Modifed Utilty Method call as per DE498 to just allow edit access for Admins
*************************************************************************************/
public with sharing class OpportunityEstimations_C {

    private static OpportunityEstimations_Service service = new OpportunityEstimations_Service();

   /*
    *   Retrieves estimation data for opportunity record
    */
    @AuraEnabled
    public static OpportunityEstimations_Service.Estimation getEstimation(Id recordId){
        return service.getEstimation(recordId);
    }

   /*
    *   Retrieves Forecast picklist values
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getForecastCategories(){
        return service.getForecastCategories();
    }

   /*
    *   Retruns flag to define if probablity is class driven or not
    */
    @AuraEnabled
    public static Boolean isClassDriven(){
        return service.isClassFieldEditable();
    }

   /*
    *   Retrieves Class picklist values
    */
    @AuraEnabled
    public static List<BaseComponent_Service.PicklistValue> getClasses(){
        return service.getClasses();
    }

   /*
    *   Updates estimation data on opportunity
    */
    @AuraEnabled
    public static OpportunityEstimations_Service.Estimation saveEstimations(Id recordId, String estimationJSON){
        return service.saveEstimations(recordId, estimationJSON);
    }
    
    /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean hasEditPermission(){
       //JS 29/05/2018 Commenting below as per DE498
        //return Utility.checkAdminPermissions();
        return Utility.checkSysAdminPermissions();
    }
    /*
    *   Returns Boolean to check for current logged in users permissions - US1509
    */
    @AuraEnabled
    public static Boolean isEditable(Id recordId){
        String objectName = recordId.getSObjectType().getDescribe().getName();
        return Utility.havRecordAccess(null,null,recordId);
    }    
}