@isTest
private class Ideas_ControllerTest{

    static testMethod void test_Ideas() {

      // Map <String,Schema.RecordTypeInfo> recordTypeMap = Library__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       
       Ideas__c ideas = new Ideas__c();
       ideas.Name = 'testideas';
       ideas.Idea_Category__c = 'Integrations';
       insert ideas;
       system.debug('ideas.id=' + ideas.Id);

       
       //if(recordTypeMap.containsKey('Other')) {
       //  lib.RecordTypeId= recordTypeMap.get('Other').getRecordTypeId();
       //}

        

           
           List<Ideas__c> l = Ideas_Controller.getAllIdeas();
           List<Ideas__c> m = Ideas_Controller.getideasCategoryType('All');
           List<Ideas__c> n = Ideas_Controller.getideasCategoryType('Integrations');
           List<String> o = Ideas_Controller.fetchIdeasCategories();
           
           Test.StartTest();
           system.assert(l.size() > 0);
           system.assert(m.size() > 0);
           system.assert(n.size() > 0);
           system.assert(o.size() > 0);

        
       Test.StopTest();
       }
}