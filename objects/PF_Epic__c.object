<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override updated by Lightning App Builder during activation.</comment>
        <content>PF_Epic_Lightning_page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>PF_Epic</compactLayoutAssignment>
    <compactLayouts>
        <fullName>PF_Epic</fullName>
        <fields>Name</fields>
        <fields>PF_Epic_Number__c</fields>
        <fields>PF_Epic_Complexity__c</fields>
        <fields>PF_Assigned_To__c</fields>
        <fields>PF_Product__c</fields>
        <label>Epic</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is where project epics will be captured within the ProjectForce application.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>PF_Assigned_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Identify the business user who&apos;s responsible for this epic.</inlineHelpText>
        <label>Assigned To</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Epics</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PF_Epic_Complexity__c</fullName>
        <description>A T-shirt size that represents the complexity of the user stories within an epic</description>
        <externalId>false</externalId>
        <formula>IF(PF_Sum_of_Story_Points__c &gt; 375, &quot;Extra Large&quot;, 
IF(PF_Sum_of_Story_Points__c &gt; 110, &quot;Large&quot;, 
IF (PF_Sum_of_Story_Points__c &gt; 30, &quot;Medium&quot;, 
IF (PF_Sum_of_Story_Points__c &gt; 0, &quot;Small&quot;,Null))))</formula>
        <inlineHelpText>T-shirt size that represents the complexity of the user stories within the Epic, based on the sum of story points: 1 to 30 - Small, 31 to 110 - Medium , 111 to 375 - Large, 376+ is Extra Large</inlineHelpText>
        <label>Epic Complexity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PF_Epic_Current_User__c</fullName>
        <externalId>false</externalId>
        <formula>IF( PF_Assigned_To__r.Id = $User.Id, &quot;Yes&quot;, &quot;No&quot;)</formula>
        <inlineHelpText>Leverage this field to help with filtering list views for those identified in the assigned to field.</inlineHelpText>
        <label>Epic Current User</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PF_Epic_Description__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Provide a brief description of the epic.</inlineHelpText>
        <label>Epic Description</label>
        <length>131072</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>PF_Epic_Number__c</fullName>
        <displayFormat>EPIC-{00000}</displayFormat>
        <externalId>false</externalId>
        <inlineHelpText>This is the auto-generated identifier.</inlineHelpText>
        <label>Epic Number</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>PF_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Select the relevant product this epic is associated to.</inlineHelpText>
        <label>Product</label>
        <referenceTo>PF_Product__c</referenceTo>
        <relationshipLabel>Epics</relationshipLabel>
        <relationshipName>Epics</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PF_Sum_of_Story_Points__c</fullName>
        <description>The sum of all story points of stories in the epic</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all story points in the epic</inlineHelpText>
        <label>Sum Of Story Points</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Epic</label>
    <listViews>
        <fullName>PF_All</fullName>
        <columns>NAME</columns>
        <columns>PF_Epic_Number__c</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Product__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>PF_All_Epics</fullName>
        <columns>PF_Product__c</columns>
        <columns>NAME</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Sum_of_Story_Points__c</columns>
        <columns>PF_Epic_Complexity__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Epics</label>
    </listViews>
    <listViews>
        <fullName>PF_All_Epics_Assigned_To_Me</fullName>
        <columns>NAME</columns>
        <columns>PF_Epic_Number__c</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Product__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>PF_Epic_Current_User__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </filters>
        <label>All Epics Assigned To Me</label>
    </listViews>
    <listViews>
        <fullName>PF_All_Epics_by_Complexity</fullName>
        <columns>PF_Product__c</columns>
        <columns>NAME</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Sum_of_Story_Points__c</columns>
        <columns>PF_Epic_Complexity__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Epics by Complexity</label>
    </listViews>
    <listViews>
        <fullName>PF_All_Epics_by_Story_Points</fullName>
        <columns>PF_Product__c</columns>
        <columns>NAME</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Epic_Complexity__c</columns>
        <columns>PF_Sum_of_Story_Points__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Epics by Story Points</label>
    </listViews>
    <listViews>
        <fullName>PF_My_Epics</fullName>
        <columns>PF_Product__c</columns>
        <columns>NAME</columns>
        <columns>PF_Epic_Description__c</columns>
        <columns>PF_Assigned_To__c</columns>
        <columns>PF_Sum_of_Story_Points__c</columns>
        <columns>PF_Epic_Complexity__c</columns>
        <filterScope>Mine</filterScope>
        <label>My Epics</label>
    </listViews>
    <nameField>
        <label>Epic Name</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Epics</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>PF_Epic</fullName>
        <active>true</active>
        <description>Standard Project Force record type</description>
        <label>Epic</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>PF_Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PF_Epic_Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PF_Assigned_To__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PF_Sum_of_Story_Points__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PF_Epic_Complexity__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>PF_Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PF_Epic_Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PF_Assigned_To__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PF_Sum_of_Story_Points__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PF_Epic_Complexity__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PF_Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PF_Epic_Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PF_Assigned_To__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PF_Sum_of_Story_Points__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PF_Epic_Complexity__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>PF_Product__c</searchFilterFields>
        <searchFilterFields>PF_Assigned_To__c</searchFilterFields>
        <searchFilterFields>PF_Sum_of_Story_Points__c</searchFilterFields>
        <searchFilterFields>PF_Epic_Complexity__c</searchFilterFields>
        <searchResultsAdditionalFields>PF_Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PF_Epic_Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PF_Assigned_To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PF_Sum_of_Story_Points__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PF_Epic_Complexity__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
