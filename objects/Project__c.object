<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Project Overview</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Burn_Report_Last_Update__c</fullName>
        <description>Date burn report was last updated</description>
        <externalId>false</externalId>
        <inlineHelpText>Date burn report was last updated</inlineHelpText>
        <label>Burn Report - Last Update</label>
        <summarizedField>Burn_Report__c.LastModifiedDate</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>max</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Change_Orders__c</fullName>
        <externalId>false</externalId>
        <label>Change Orders</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Complete__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Total_Budget_Hours__c &lt;&gt; 0, Total_Actual_Hours__c  /  Total_Budget_Hours__c, 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>% Complete</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Customer_Name__c</fullName>
        <description>Name of customer</description>
        <externalId>false</externalId>
        <label>Customer Name</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Diff_w_Budget__c</fullName>
        <description>Total budget hrs - Total forecast hours</description>
        <externalId>false</externalId>
        <formula>Total_Budget_Hours__c - Total_Forecast_Hours__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Negative value indicates the project is forecasted to be over budget</inlineHelpText>
        <label>Est. Diff w/ Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Est_Hrs_to_Complete__c</fullName>
        <description>Estimated hours left in the project</description>
        <externalId>false</externalId>
        <inlineHelpText>Estimated hours left in the project</inlineHelpText>
        <label>Est. Hrs to Complete</label>
        <summarizedField>Burn_Report__c.Est_Hrs_to_Complete__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_To_Date__c</fullName>
        <externalId>false</externalId>
        <label>Expenses To Date</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Forecast_Budget_Diff__c</fullName>
        <description>Negative amt indicates OVER budget</description>
        <externalId>false</externalId>
        <inlineHelpText>Negative amt indicates OVER budget</inlineHelpText>
        <label>Forecast Budget Diff</label>
        <summarizedField>Burn_Report__c.Diff_w_Budget_Dollars__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Forecast_Budget_Difference__c</fullName>
        <externalId>false</externalId>
        <formula>Total_Approved_Budget_NEW__c - Forecast_Final_Dollars__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Forecast Budget Difference</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Forecast_Final_Dollars_OLD__c</fullName>
        <externalId>false</externalId>
        <formula>Total_Assigned_Budget_c__c  + Change_Orders__c - Forecast_Budget_Diff__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Forecast Final $ (OLD)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Forecast_Final_Dollars__c</fullName>
        <externalId>false</externalId>
        <label>Forecast Final $</label>
        <summarizedField>Burn_Report__c.Forecast_Final_Dollars__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Go_Live__c</fullName>
        <description>Expected Go live date</description>
        <externalId>false</externalId>
        <inlineHelpText>Expected Go live date</inlineHelpText>
        <label>Go Live</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Opportunity_Id__c</fullName>
        <description>Oppty ID from the 62 Org</description>
        <externalId>false</externalId>
        <inlineHelpText>Oppty ID from the 62 Org</inlineHelpText>
        <label>Opportunity Id</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Overall_Project_Status__c</fullName>
        <externalId>false</externalId>
        <label>Overall Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Green</fullName>
                    <default>false</default>
                    <label>Green</label>
                </value>
                <value>
                    <fullName>Yellow</fullName>
                    <default>false</default>
                    <label>Yellow</label>
                </value>
                <value>
                    <fullName>Red</fullName>
                    <default>false</default>
                    <label>Red</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Overall_Status__c</fullName>
        <externalId>false</externalId>
        <formula>IMAGE(
CASE(  Overall_Project_Status__c  ,
&quot;Green&quot;, &quot;/servlet/servlet.FileDownload?file=01541000000H41k&quot;,
&quot;Yellow&quot;, &quot;/servlet/servlet.FileDownload?file=01541000000H41n&quot;,
&quot;Red&quot;, &quot;/servlet/servlet.FileDownload?file=01541000000H41m&quot;,
&quot;/servlet/servlet.FileDownload?file=01541000000H41l&quot;),
&quot;status flag&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Overall Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Id__c</fullName>
        <description>Project reference identifier</description>
        <externalId>false</externalId>
        <label>Project Id</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Stage__c</fullName>
        <externalId>false</externalId>
        <label>Project Stage</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Prepare</fullName>
                    <default>true</default>
                    <label>Prepare</label>
                </value>
                <value>
                    <fullName>Architect</fullName>
                    <default>false</default>
                    <label>Architect</label>
                </value>
                <value>
                    <fullName>Construct</fullName>
                    <default>false</default>
                    <label>Construct</label>
                </value>
                <value>
                    <fullName>Validate</fullName>
                    <default>false</default>
                    <label>Validate</label>
                </value>
                <value>
                    <fullName>Deploy</fullName>
                    <default>false</default>
                    <label>Deploy</label>
                </value>
                <value>
                    <fullName>Support</fullName>
                    <default>false</default>
                    <label>Support</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Project Start Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Remaining_Expense_Budget__c</fullName>
        <externalId>false</externalId>
        <formula>Total_Expense_Budget__c -  Expenses_To_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remaining Expense Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Actual_Hours__c</fullName>
        <externalId>false</externalId>
        <label>Total Actual Hours</label>
        <summarizedField>Burn_Report__c.Actual_Hours_to_date__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Approved_Budget_NEW__c</fullName>
        <externalId>false</externalId>
        <label>Total Approved Budget*</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Approved_Budget__c</fullName>
        <description>Includes all PO&apos;s, CO&apos;s and Management Fees</description>
        <externalId>false</externalId>
        <formula>Total_Assigned_Budget_c__c  +  Change_Orders__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Includes all PO&apos;s, CO&apos;s and Management Fees</inlineHelpText>
        <label>Total Approved Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Assigned_Budget_c__c</fullName>
        <description>calculated from the Burn Report line items</description>
        <externalId>false</externalId>
        <inlineHelpText>Total of approved budget $</inlineHelpText>
        <label>Total Assigned Budget - hrs</label>
        <summarizedField>Burn_Report__c.Approved_Budget__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Budget_Hours__c</fullName>
        <externalId>false</externalId>
        <label>Total Budget Hours</label>
        <summarizedField>Burn_Report__c.Budget_Hrs__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Expense_Budget__c</fullName>
        <externalId>false</externalId>
        <formula>0.15* Total_Approved_Budget__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Expense Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Forecast_Hours__c</fullName>
        <externalId>false</externalId>
        <label>Total Forecast Hours</label>
        <summarizedField>Burn_Report__c.Forecast_Final_Hours__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_to_date__c</fullName>
        <description>amount expended to date based on actual hours logged</description>
        <externalId>false</externalId>
        <inlineHelpText>amount expended to date based on actual hours logged</inlineHelpText>
        <label>Total $ to date</label>
        <summarizedField>Burn_Report__c.Actual_dollars_to_date__c</summarizedField>
        <summaryForeignKey>Burn_Report__c.Project_Overview__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Trend__c</fullName>
        <description>Rate how this project is doing overall as compared to last week</description>
        <externalId>false</externalId>
        <inlineHelpText>Rate how this project is doing overall as compared to last week</inlineHelpText>
        <label>Trend</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Stable</fullName>
                    <default>true</default>
                    <label>Stable</label>
                </value>
                <value>
                    <fullName>Up</fullName>
                    <default>false</default>
                    <label>Up</label>
                </value>
                <value>
                    <fullName>Down</fullName>
                    <default>false</default>
                    <label>Down</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Project (PM Toolkit)</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Project Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Projects (PM Toolkit)</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Project_Id__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Project_Stage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Go_Live__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Project_Id__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Project_Stage__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Go_Live__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Customer_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Project_Id__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>Project_Id__c</searchFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Go_Live__c</searchFilterFields>
        <searchFilterFields>Project_Stage__c</searchFilterFields>
        <searchFilterFields>Project_Start_Date__c</searchFilterFields>
        <searchResultsAdditionalFields>Project_Id__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Project_Stage__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Go_Live__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
