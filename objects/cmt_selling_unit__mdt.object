<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Selling Unit mapping to Business Unit and Line of Business</description>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Record is Active when True, otherwise Inactive</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Record is Active when True, otherwise Inactive</inlineHelpText>
        <label>Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <description>Business Units associated with a Line of Business and Performance Unit</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Business Units associated with a Line of Business and Performance Unit</inlineHelpText>
        <label>Business Unit</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>NA</fullName>
                    <default>true</default>
                    <label>NA</label>
                </value>
                <value>
                    <fullName>UK Nuclear &amp; Defence</fullName>
                    <default>false</default>
                    <label>UK Nuclear &amp; Defence</label>
                </value>
                <value>
                    <fullName>Advanced Engineering, Research and Operations (AERO)</fullName>
                    <default>false</default>
                    <label>Advanced Engineering, Research and Operations (AERO)</label>
                </value>
                <value>
                    <fullName>Mission Solutions (MS)</fullName>
                    <default>false</default>
                    <label>Mission Solutions (MS)</label>
                </value>
                <value>
                    <fullName>B&amp;I-Americas</fullName>
                    <default>false</default>
                    <label>B&amp;I-Americas</label>
                </value>
                <value>
                    <fullName>B&amp;I-APAC</fullName>
                    <default>false</default>
                    <label>B&amp;I-APAC</label>
                </value>
                <value>
                    <fullName>B&amp;I-EU/UK/ME</fullName>
                    <default>false</default>
                    <label>B&amp;I-EU/UK/ME</label>
                </value>
                <value>
                    <fullName>Field Services</fullName>
                    <default>false</default>
                    <label>Field Services</label>
                </value>
                <value>
                    <fullName>JESA</fullName>
                    <default>false</default>
                    <label>JESA</label>
                </value>
                <value>
                    <fullName>Life Sciences and Consumer Goods &amp; Manufacturing</fullName>
                    <default>false</default>
                    <label>Life Sciences and Consumer Goods &amp; Manufacturing</label>
                </value>
                <value>
                    <fullName>Mining &amp; Minerals and Specialty Chemicals</fullName>
                    <default>false</default>
                    <label>Mining &amp; Minerals and Specialty Chemicals</label>
                </value>
                <value>
                    <fullName>P&amp;C-Americas</fullName>
                    <default>false</default>
                    <label>P&amp;C-Americas</label>
                </value>
                <value>
                    <fullName>P&amp;C-Europe &amp; South Africa</fullName>
                    <default>false</default>
                    <label>P&amp;C-Europe &amp; South Africa</label>
                </value>
                <value>
                    <fullName>P&amp;C-India/Asia</fullName>
                    <default>false</default>
                    <label>P&amp;C-India/Asia</label>
                </value>
                <value>
                    <fullName>P&amp;C-Middle East</fullName>
                    <default>false</default>
                    <label>P&amp;C-Middle East</label>
                </value>
                <value>
                    <fullName>US Home Office Operations (HO)</fullName>
                    <default>false</default>
                    <label>US Home Office Operations (HO)</label>
                </value>
                <value>
                    <fullName>A&amp;T-US</fullName>
                    <default>false</default>
                    <label>A&amp;T-US</label>
                </value>
                <value>
                    <fullName>A&amp;T-UK</fullName>
                    <default>false</default>
                    <label>A&amp;T-UK</label>
                </value>
                <value>
                    <fullName>P&amp;C-M&amp;A</fullName>
                    <default>false</default>
                    <label>P&amp;C-M&amp;A</label>
                </value>
                <value>
                    <fullName>P&amp;C-Asia</fullName>
                    <default>false</default>
                    <label>P&amp;C-Asia</label>
                </value>
                <value>
                    <fullName>P&amp;C-India</fullName>
                    <default>false</default>
                    <label>P&amp;C-India</label>
                </value>
                <value>
                    <fullName>A&amp;T - Government Relations</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>A&amp;T - Government Relations</label>
                </value>
                <value>
                    <fullName>A&amp;T - US Home Office</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>A&amp;T - US Home Office</label>
                </value>
                <value>
                    <fullName>IND - M&amp;A</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>IND - M&amp;A</label>
                </value>
                <value>
                    <fullName>P&amp;C - M&amp;A</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>P&amp;C - M&amp;A</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>End Date for the Performance Unit record</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>End Date for the Performance Unit record</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Line_of_Business__c</fullName>
        <description>Line of Business associated with a Business Unit and ultimate parent of a Performance Unit</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Line of Business associated with a Business Unit and ultimate parent of a Performance Unit</inlineHelpText>
        <label>Line of Business</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Aerospace &amp; Technology</fullName>
                    <default>false</default>
                    <label>Aerospace &amp; Technology</label>
                </value>
                <value>
                    <fullName>Buildings &amp; Infrastructure</fullName>
                    <default>false</default>
                    <label>Buildings &amp; Infrastructure</label>
                </value>
                <value>
                    <fullName>Industrial</fullName>
                    <default>false</default>
                    <label>Industrial</label>
                </value>
                <value>
                    <fullName>Petroleum &amp; Chemicals</fullName>
                    <default>false</default>
                    <label>Petroleum &amp; Chemicals</label>
                </value>
                <value>
                    <fullName>Corporate</fullName>
                    <default>false</default>
                    <label>Corporate</label>
                </value>
                <value>
                    <fullName>NA</fullName>
                    <default>true</default>
                    <label>NA</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Selling_Unit__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Performance Unit is a Selling Unit when checked TRUE, otherwise FALSE = non-selling Performance Unit</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Performance Unit is a Selling Unit when checked TRUE, otherwise FALSE = non-selling Performance Unit</inlineHelpText>
        <label>Selling Unit</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <defaultValue>DATE(2015,9,1)</defaultValue>
        <description>Start Date for the Performance Unit</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Start Date</label>
        <required>true</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>txt_pu_number__c</fullName>
        <description>Performance Unit Number</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Performance Unit Number</inlineHelpText>
        <label>PU Number</label>
        <length>15</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>txt_selling_unit_full_name__c</fullName>
        <description>Full selling unit name used in global picklist values</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Full selling unit name used in global picklist values</inlineHelpText>
        <label>Selling Unit Full Name</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Selling Unit</label>
    <listViews>
        <fullName>All</fullName>
        <columns>MasterLabel</columns>
        <columns>txt_selling_unit_full_name__c</columns>
        <columns>Business_Unit__c</columns>
        <columns>Line_of_Business__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <pluralLabel>Selling Units</pluralLabel>
    <validationRules>
        <fullName>vr_Selling_Unit_End_Date_Active</fullName>
        <active>true</active>
        <description>For Active records, Selling Unit End Date must be greater than or equal to start date. For inactive records, a Selling Unit End Date must be specified</description>
        <errorConditionFormula>Active__c = TRUE &amp;&amp;
ISBLANK( End_Date__c )= FALSE &amp;&amp;
End_Date__c &lt;  Start_Date__c 
||
Active__c = FALSE &amp;&amp;
ISBLANK( End_Date__c )= True
||
Active__c = TRUE &amp;&amp;
ISBLANK(  Start_Date__c) = TRUE 
||
Active__c = TRUE &amp;&amp;
Start_Date__c &gt; TODAY()</errorConditionFormula>
        <errorMessage>For Active records, Performance Unit End Date must be greater than or equal to start date. For inactive records, a Performance Unit End Date must be specified. A Performance Unit must have a start date if active and that start date must be  &lt;= today.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
